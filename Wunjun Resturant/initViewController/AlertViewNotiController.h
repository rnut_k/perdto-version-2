//
//  AlertViewNotiController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/31/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
#import "LocalizationSystem.h"

@interface AlertViewNotiController : UITableViewController

-(void)createAlertView:(NSDictionary *)data ;
-(void)setDataAlertView:(NSDictionary *)dataAlert;
@end
