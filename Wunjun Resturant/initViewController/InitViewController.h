//
//  InitViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Member.h"
#import "MenuDB.h"
#import "LoadMenuAll.h"
#import "DeviceData.h"
#import "NSDictionary+DictionaryWithString.h"

@interface InitViewController : UIViewController

@property (nonatomic, strong) Member *member;
@property (nonatomic, strong) MenuDB *menuDB;
@end
