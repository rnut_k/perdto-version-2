//
//  RoleViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/17/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "RoleViewController.h"
#import "LoginViewController.h"
#import "KitchenViewController.h"
#import "CashTableStatus.h"
#import "AdminNavigationController.h"
#import "AppDelegate.h"
#import "CheckerItemsOrdered.h"
#import "LoadProcess.h"
#import "SelectBill.h"
#import "ReConstructionViewController.h"
#import "OutcheckerController.h"

@interface RoleViewController ()<UIAlertViewDelegate>
{
    NSDictionary *userRole;
    LoadProcess *loadProcess;
    
    BOOL isNoti;
    NSDictionary *dataNoti;
    NSString *notiType;
}
@end

@implementation RoleViewController

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    NSString *Lang2;
    if (self) {
        NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        Lang2 = [languages objectAtIndex:0]; //NSString
//        LocalizationSetIs(NO);
//        LocalizationSetLanguage(Lang2);
    }
    DDLogInfo(@"RoleViewController load coder %@", Lang2);
    return self;
}

- (void)viewDidLoad {
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    loadProcess = [LoadProcess sharedInstance];
    isNoti = NO;
//    [loadProcess.loadView showWaitProgress];
    
    self.member = [Member getInstance];
    [self.member loadData];
//    NSLog(@"member code: %@", self.member.code);
    
    self.titleLbl.text = self.member.titleRes;
    self.conAdminH.constant = 0;
    if(![self.member isUserLogin])
        [self showLoginView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
//    [center postNotificationName:@"UIApplicationDisconnectSocket"
//                          object:self
//                        userInfo:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNotification:)
                                                 name:@"NotiupdataRoel" object:nil];
    
    [loadProcess.loadView showWaitProgress];
    [self setHidesBottom];
    
    userRole = [NSDictionary dictionaryWithString:self.member.role];
    self.adminBtn.enabled = YES;
    for(NSString *role in userRole){
        if([role isEqualToString:@"admin"]){
           [self.mLayoutAdminBtn setConstant:60];
           [self.adminBtn setHidden:NO];
        }
        else if([role isEqualToString:@"staff"]){
            [self.mLayoutStaffBtn setConstant:60];
            [self.staffBtn setHidden:NO];
        }
        else if([role isEqualToString:@"cashier"]){
            [self.mLayoutCashierBtn setConstant:60];
            self.cashierBtn.hidden = NO;
        }
        else if([role isEqualToString:@"checker"]){
            [self.mLayoutCheckerBtn setConstant:60];
            [self.checkerBtn setHidden:NO];
        }
        else if([role isEqualToString:@"kitchen"]){
            [self.mLayoutKitchenBtn setConstant:60];
            [self.kitchenBtn setHidden:NO];
        }
        else if([role isEqualToString:@"mama"]){
            [self.mLayoutMamaBtn setConstant:60];
            [self.mamaBtn setHidden:NO];
        }
    }
}
-(void)setHidesBottom{
    [self.mLayoutAdminBtn setConstant:0];
    [self.mLayoutStaffBtn setConstant:0];
    [self.mLayoutCashierBtn setConstant:0];
    [self.mLayoutCheckerBtn setConstant:0];
    [self.mLayoutKitchenBtn setConstant:0];
    [self.mLayoutMamaBtn setConstant:0];
    
    [self.adminBtn setHidden:YES];
    [self.staffBtn setHidden:YES];
    [self.cashierBtn setHidden:YES];
    [self.checkerBtn setHidden:YES];
    [self.kitchenBtn setHidden:YES];
    [self.mamaBtn setHidden:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    if (isNoti) {
        if ([notiType isEqual:@"staff"]) {
            DDLogInfo(@"staff");
            [self notiBillTable:dataNoti];
        }
        else if ([notiType isEqual:@"admin"]) {
            DDLogInfo(@"admin");
            [self setNotiData:dataNoti and:@"NotiupdataPending"];
        }
        else if ([notiType isEqual:@"cashier"]) {
            DDLogInfo(@"cashier");
            [self setNotiData:dataNoti and:@"updateCheckBillNoti"];
        }
        else if ([notiType isEqual:@"checker"]) {
            DDLogInfo(@"checker");
            [self setNotiData:nil and:@"updateInvoiceList"];
        }
        else if ([notiType isEqual:@"kitchen"]) {
            DDLogInfo(@"kitchen");
            [self setNotiData:nil and:@"NotisetInsertKitchen"];
        }
        else if ([notiType isEqual:@"reloadDataOrder"]) {
            DDLogInfo(@"reloadDataOrder");
            [self setNotiData:dataNoti and:@"reloadDataOrder"];
        }
    }
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (void)showLoginView
{
//    
//    LoginViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    [app.window makeKeyAndVisible];
//    [app.window.rootViewController presentViewController:navCon animated:YES completion:NULL];
    
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app.window makeKeyAndVisible];
    app.window.rootViewController = [app.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
}

- (IBAction)clickAdmin:(id)sender
{
    [self postNotificationName:@"admin"];
    
//    
//    AdminNavigationController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"AdminMain"];
//    [navCon setActive:@"home"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [self presentViewController:navCon animated:YES completion:NULL];
}
- (IBAction)clickStaff:(id)sender
{
    [self postNotificationName:@"staff"];
    
//    
//    UIViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"TableZone"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [self presentViewController:navCon animated:YES completion:NULL];

}

- (IBAction)clickMama:(id)sender
{
    [self postNotificationName:@"mama"];
    
//    
//    UIViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"MamaNav"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [self presentViewController:navCon animated:YES completion:NULL];
}

- (IBAction)clickCashier:(id)sender
{
   
    [self postNotificationName:@"cashier"];
    
//    
//    CashTableStatus *navCon = [storyboard instantiateViewControllerWithIdentifier:@"CashTableStatusMain"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [self presentViewController:navCon animated:YES completion:NULL];
}
- (IBAction)clickChecker:(id)sender
{
    [self postNotificationName:@"checker"];
    
//    
//    CheckerItemsOrdered *navCon = [storyboard instantiateViewControllerWithIdentifier:@"CheckerInit"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
    
//    [self presentViewController:navCon animated:YES completion:NULL];
}
- (IBAction)clickKitchen:(id)sender
{
    [self postNotificationName:@"kitchen"];
    
//    
//    KitchenViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"Kitchen"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [self presentViewController:navCon animated:YES completion:NULL];
}
- (IBAction)clickPrinterSeting:(id)sender
{
//    
//    UIViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"CNPrinterSeting"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
//    [app.window.rootViewController  presentViewController:navCon animated:YES completion:nil];
//    [self presentViewController:navCon animated:YES completion:NULL];
}
- (IBAction)clickLogout:(id)sender{
    UIAlertView *aler = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                   message:AMLocalizedString(@"คุณต้องการออกจากระบบใช่หรือไม่", nil)
                                                  delegate:self
                                         cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                         otherButtonTitles:AMLocalizedString(@"ใช่", nil),nil];
    [aler show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
         [LoadMenuAll logoutView:@{@"error":@"101"}];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)postNotificationName:(NSString *)type{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setValue:type forKey:@"role"];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:@"UIApplicationConnectSocket"
                          object:self
                        userInfo:nil];
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(void) updateNotification:(NSNotification *)notification{
    dataNoti = notification.userInfo;
    NSString *dataAert = [NSString stringWithFormat:@"%@",dataNoti[@"a"]];
    NSArray *position = [self getStatus:[dataAert intValue]];
    if ([position count] > 1) {
        isNoti = YES;
        notiType = position[1];
        [self performSegueWithIdentifier:position[0] sender:self];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"The segue identifier is: %@", [segue identifier]);
    if ([[segue identifier] isEqualToString:@"AdminMain"]) {
        NSLog(@"AdminMain");
        [self postNotificationName:@"admin"];
        AdminNavigationController *navCon = (AdminNavigationController *)segue.destinationViewController;
        [navCon setActive:@"home"];
    }
    else  if ([[segue identifier] isEqualToString:@"staffMain"]) {
        NSLog(@"staffMain");
        [self postNotificationName:@"staff"];
    }
    else  if ([[segue identifier] isEqualToString:@"manaMain"]) {
        NSLog(@"ManaMain");
         [self postNotificationName:@"mama"];
    }
    else  if ([[segue identifier] isEqualToString:@"cashierMain"]) {
//        NSLog(@"cashierMain");
//        [self postNotificationName:@"cashier"];
    }
    else  if ([[segue identifier] isEqualToString:@"checkerMain"]) {
        NSLog(@"checker");
        [self postNotificationName:@"checker"];
    }
    else  if ([[segue identifier] isEqualToString:@"kitchenMain"]) {
//        NSLog(@"kitchenMain");
//       [self postNotificationName:@"kitchen"];
    }
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//    ReConstructionViewController
    if ([identifier isEqualToString:@"kitchenMain"]) {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Reconstructor" bundle:nil];
        ReConstructionViewController *myViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReConstructionViewController"];
        [self presentViewController:myViewController animated:YES completion:nil];
        
        NSLog(@"kitchenMain");
        return NO;
        
    }
    else  if ([identifier isEqualToString:@"cashierMain"]) {
                NSLog(@"cashierMain");
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Reconstructor" bundle:nil];
        ReConstructionViewController *myViewController = [storyboard instantiateViewControllerWithIdentifier:@"ReConstructionViewController"];
        [self presentViewController:myViewController animated:YES completion:nil];
        return NO;
        
    }
    return YES;
}
-(IBAction)unwindToViewRoot:(UIStoryboardSegue *)segue {
    //do stuff
    NSLog(@"UnwindFromRoleView");
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:@"UIApplicationDisconnectSocket"
                          object:self
                        userInfo:nil];
}
-(void)setNotiData:(NSDictionary *)data and:(NSString *)name{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:name
                          object:self
                        userInfo:data];
}
-(NSArray *)getStatus:(int)num{
    switch (num) {
            // ไม่ต้อง
        case 1: // เปิดโต๊ะ (ไม่ส่ง)
        case 2: // ปิดโต๊ะ cashier เช็คบิล => staff อัพเดท table status
        case 3: // ย้ายโต๊ะ (ไม่ส่ง)
            return nil;//@"โต๊ะ";
            // kitchen
        case 10: // staff สั่งอาหาร => kitchen เพิ่มหรืออัพเดทรายการ order
        case 11: // staff ยกเลิกรายการอาหาร => kitchen ลบรายการนั้นออก
            return @[@"kitchenMain",@"kitchen"];
            // staff
        case 20: // admin อนุมัติโปรโมชั่น => staff แสดง popup notification และแสดงปุ่ม promotion นั้น
            return @[@"staffMain",@"staff"];
        case 12: // kitchen เปลี่ยนสถานะรายการอาหาร เช่น เสร็จแล้ว ยกเลิก => staff ไม่ต้องทำอะไรถ้าเป็นการยกเลิก staff แสดง popup notification และอัพเดทรายการอาหารนั้น
            return @[@"nil",@"reloadDataOrder"];
        case 13: // kitchen ยกเลิกรายการอาหาร => staff แสดง popup notification และอัพเดทรายการอาหารนั้น
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([[prefs objectForKey:@"role"] isEqualToString:@"staff"]){
                return @[@"staffMain",@"staff"];
            }else{
                return @[@"kitchenMain",@"kitchen"];
            }
        }
        case 60:  // kitchen block menu => staff อัพเดทรายการเมนู
        {
//            [self updateMenu:nil];
            return nil;
        }
            // admin
        case 21: // staff   ร้องขออนุมัติโปรโมชัน => admin แสดง notification หรือ อัพเทด list pending
        case 31: // checker ร้องขออนุมัติใบสั่งสินค้า => admin แสดง notification หรือ อัพเทด list pending
        case 40: // cashier ส่งยอด => admin แสดง notification หรือ อัพเทด list pending
        case 41: // cashier ร้องขอการอนุมัติส่วนลดเพิ่มเติม => admin แสดง notification หรือ อัพเทด list pending
        case 50: // checker ส่งรายงาน stock => admin แสดง notification หรือ อัพเทด list pending
            return @[@"AdminMain",@"admin"];
            // checker
        case 30: // admin อนุบัติใบสั่งสินค้า => checker แสดง notification หรืออัพเทด list invoice
            return @[@"checkerMain",@"checker"];
        case 42: // admin อนุบัติใบสั่งสินค้า => checker แสดง notification หรืออัพเทด list invoice
            return @[@"cashierMain",@"cashier"];
        default:
            return nil;
    }
}

-(void)notiBillTable:(NSDictionary *)data{
    [LoadMenuAll retrieveData:@"" success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *dataListTables = [NSArray arrayWithArray:dataTables];
            NSDictionary *filteredArray = (NSDictionary *)[NSDictionary searchDictionary:data[@"t_id"]
                                                                                andvalue:@"(id = %@)"
                                                                                 andData:dataListTables];
            [LoadMenuAll loadDataOpenTable:filteredArray[@"id"] success:^(NSDictionary *dictionary) {
                
                AppDelegate *app = [[UIApplication sharedApplication] delegate];
                [app.window makeKeyAndVisible];
                UINavigationController *navcon = (UINavigationController *)app.window.rootViewController;
                [SelectBill selectBill:navcon andData:dictionary andDataTable:filteredArray fromRole:nil];
            }];
        });
    }];
}

-(IBAction)clickOutchecker:(id)sender{
    
    [self postNotificationName:@"outchecker"];
    OutcheckerController *view2 = [[OutcheckerController alloc] initWithNibName:nil bundle:nil];
    [self presentViewController:view2 animated:YES completion:NULL];
}

@end
