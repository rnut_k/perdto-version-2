//
//  LoginViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Constants.h"
#import "Member.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadMenuAll.h"
#import "WYPopoverController.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,WYPopoverControllerDelegate>{
    
    int *countTouch;
    
}

@property (weak, nonatomic) IBOutlet UITextField *mNameRestaurant;
@property (weak, nonatomic) IBOutlet UITextField *mUsername;
@property (weak, nonatomic) IBOutlet UITextField *mPassword;
@property (weak, nonatomic) IBOutlet UIImageView *mImageBG;
@property (nonatomic,strong) Member *member;
@property (weak, nonatomic) IBOutlet UIImageView *mImageLogo;
@property (weak, nonatomic) IBOutlet UIImageView *mImageDemo;
@property (weak, nonatomic) IBOutlet UIView *mViewDemo;
@property (weak, nonatomic) IBOutlet UIButton *mBtnDemo;
@property (weak, nonatomic) IBOutlet UIButton *mBtnLogo;
@property (weak, nonatomic) IBOutlet UIButton *mBtnClose;
@property (weak, nonatomic) IBOutlet UILabel *mLabelRemember;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLaoutDemoBtn;
@property (nonatomic,assign) int *countTouch;

- (IBAction)btnLogin:(id)sender;
- (IBAction)btnCloseDemo:(id)sender;
- (IBAction)btnTagGest:(id)sender;
- (IBAction)btnTestDemo:(id)sender;

- (IBAction)btnImageLogo:(id)sender;

@end
