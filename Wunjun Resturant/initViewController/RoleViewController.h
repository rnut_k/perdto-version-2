//
//  RoleViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/17/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "Constants.h"
#import "Member.h"
#import "DeviceData.h"
#import "LoadProcess.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"


@interface RoleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *adminBtn;
@property (weak, nonatomic) IBOutlet UIButton *staffBtn;
@property (weak, nonatomic) IBOutlet UIButton *cashierBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkerBtn;
@property (weak, nonatomic) IBOutlet UIButton *kitchenBtn;
@property (weak, nonatomic) IBOutlet UIButton *mamaBtn;
@property (weak, nonatomic) IBOutlet UIButton *PrinterBtn;

@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLayoutAdminBtn;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLayoutStaffBtn;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLayoutCashierBtn;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLayoutCheckerBtn;
@property (weak, nonatomic) IBOutlet UIButton *outchecker;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *mLayoutKitchenBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutMamaBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutPrinterBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutOutcheckerBtn;


@property (nonatomic,strong) Member *member;
@property (weak, nonatomic) IBOutlet UIScrollView *mScrloViewRole;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *conAdminH;
@property (weak, nonatomic) IBOutlet UILabel *titleLbl;

- (IBAction)clickAdmin:(id)sender;
- (IBAction)clickStaff:(id)sender;
- (IBAction)clickCashier:(id)sender;
- (IBAction)clickChecker:(id)sender;
- (IBAction)clickKitchen:(id)sender;
- (IBAction)clickMama:(id)sender;
- (IBAction)clickLogout:(id)sender;
- (IBAction)clickPrinterSeting:(id)sender;
-(IBAction)unwindToViewRoot:(UIStoryboardSegue *)segue;
- (IBAction)clickOutchecker:(id)sender;
@end
