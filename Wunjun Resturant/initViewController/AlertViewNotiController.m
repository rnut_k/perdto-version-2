//
//  AlertViewNotiController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/31/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AlertViewNotiController.h"
#import "TableAllZoneCollectionView.h"
#import "CustomNavigationController.h"

#import "WYPopoverController.h"
#import "AppDelegate.h"
#import "TableOpenViewController.h"
#import "CNPPopupController.h"
#import "Constants.h"
#import "UIColor+HTColor.h"
#import "Check.h"
#import "Member.h"
#import "MenuDB.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"
#import "SelectBill.h"
#import "NSString+GetString.h"

@interface AlertViewNotiController()<UITableViewDataSource,UITableViewDelegate,WYPopoverControllerDelegate>{
    NSMutableArray *dataOrderAlert;
    WYPopoverController *popoverAlertController;
    AppDelegate *appDelegate;
    
}
@property (nonatomic,strong) Member *member;
@property (nonatomic,strong) MenuDB *menuDB;

@end
@implementation AlertViewNotiController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
-(MenuDB *)menuDB{
    if (!_menuDB) {
        _menuDB = [MenuDB getInstance];
        [_menuDB loadData:@"menu_0"];
    }
    return _menuDB;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    
    appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate.window makeKeyAndVisible];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
}
-(void)createAlertView:(NSDictionary *)data {
    dataOrderAlert = [[NSMutableArray alloc] init];
    [self setDataAlertView:data];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int num = (int)[dataOrderAlert count];
    return  num;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenfine = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenfine];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:idenfine];
    }
    
    NSDictionary *data = dataOrderAlert[indexPath.row];
    NSDictionary *dataMenu = data[@"dataMenu"];
    NSDictionary *dataTable = data[@"dataTable"];
    int num = [data[@"num"] intValue];
    NSString *titleMenu = [NSString stringWithFormat:@"%@ %@",[self getStatus:num],dataMenu[@"name"]];
    
    NSString *subbill = @"";
    if (![data[@"subbill"] isEqual:@"0"]) {
        NSString *strBill = AMLocalizedString(@"บิลย่อยที่", nil);
        subbill = [NSString stringWithFormat:@"%@ %@",strBill,data[@"subbill"]];
    }
    NSString *table = AMLocalizedString(@"โต๊ะ", nil);
    NSString *titleTable = [NSString stringWithFormat:@" %@ %@ %@ ",table,dataTable[@"label"],subbill];
    
    [cell.textLabel setText:titleMenu];
    [cell.detailTextLabel setText:titleTable];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *data = dataOrderAlert[indexPath.row];
    NSDictionary *dataTable = data[@"dataTable"];
    
    UINavigationController *navcon = (UINavigationController *)appDelegate.mainViewController;
    [appDelegate btnClosePop:self];
    
    [LoadMenuAll loadDataOpenTable:dataTable[@"id"] success:^(NSDictionary *dictionary) {
        [SelectBill selectBill:navcon andData:dictionary andDataTable:dataTable fromRole:nil];
    }];
}
-(void)setDataAlertView:(NSDictionary *)dataAlert{
    
    NSDictionary *dataNoti = [NSDictionary dictionaryWithObject:dataAlert[@"data"]];
    NSDictionary *dType = [NSDictionary dictionaryWithObject:dataNoti[@"d"]];
    [self setMenu:dType and:dataNoti[@"a"]];
}
-(void)setMenu:(NSDictionary *)dType and:(NSString *)num{
    NSDictionary *orderData = [NSDictionary dictionaryWithObject:dType[@"order_data"]];
    NSDictionary *promotionData = [NSDictionary dictionaryWithObject:dType[@"promotion_data"]];
   
    NSDictionary *dataMenu_1 = nil;
    NSString *subbill_id = @"0";
    if (orderData) {
        NSDictionary *json = [NSDictionary dictionaryWithString:self.menuDB.valueMenu];
        NSDictionary *dataMenu  = [NSDictionary dictionaryWithObject:json[@"menu"]];
        NSString *key = [NSString stringWithFormat:@"id_%@",orderData[@"menu_id"]];
        dataMenu_1 = [NSDictionary dictionaryWithObject:dataMenu[key]];
        subbill_id = [NSString checkNull:orderData[@"subbill_id"]];
    }
    else{
        dataMenu_1 = [promotionData copy];
    }
    
    if ((![Check checkNull:promotionData] || ![Check checkNull:orderData]) && ![Check checkNull:dataMenu_1]){
        [LoadMenuAll retrieveData:@"" success:^(NSArray *dataTables){
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *dataListTables = [NSArray arrayWithArray:dataTables];
                NSDictionary *filtered = (NSDictionary *)[NSDictionary searchDictionary:dType[@"table_id"]
                                                                               andvalue:@"(id = %@)"
                                                                                andData:dataListTables];
                [dataOrderAlert addObject:@{@"dataMenu":dataMenu_1,
                                            @"dataTable":filtered,
                                            @"subbill":subbill_id,
                                            @"num":num}];
                [self.tableView reloadData];
            });
        }];
    }
    else{
         [appDelegate btnClosePop:self];
    }
}

-(NSString *)getStatus:(int)num{
    switch (num) {
        case 1:{ // เปิดโต๊ะ (ไม่ส่ง)
            NSLog(@"kACTION_TABLE_OPEN");
            return  AMLocalizedString(@"เปิดโต๊ะ", nil);
        }
            break;
        case 2:{ // ปิดโต๊ะ (ไม่ส่ง)
            NSLog(@"kACTION_TABLE_CLOSE");
            return AMLocalizedString(@"ปิดโต๊ะ", nil);
        }
            break;
        case 3:{ // ย้ายโต๊ะ (ไม่ส่ง)
            NSLog(@"kACTION_TABLE_MOVE");
            return AMLocalizedString(@"ย้ายโต๊ะ", nil);
        }
            break;
            // ORDER
        case 10:{ // staff สั่งอาหาร => kitchen เพิ่มหรืออัพเดทรายการ order
            NSLog(@"kACTION_ORDER_COMMIT");
            return AMLocalizedString(@"สั่งอาหาร", nil);
        }
            break;
        case 11:{ // staff ยกเลิกรายการอาหาร => kitchen ลบรายการนั้นออก
            NSLog(@"kACTION_ORDER_STAFF_STATUS");
            return AMLocalizedString(@"ยกเลิกอาหาร", nil);
        }
            break;
        case 12:{ // kitchen เปลี่ยนสถานะรายการอาหาร เช่น เสร็จแล้ว ยกเลิก => staff ไม่ต้องทำอะไรถ้าเป็นการยกเลิก staff แสดง popup notification และอัพเดทรายการอาหารนั้น
            NSLog(@"kACTION_ORDER_KITCHEN_STATUS");
            return AMLocalizedString(@"รายการ", nil);
        }
            break;
        case 13:{ // kitchen ยกเลิกรายการอาหาร => staff แสดง popup notification และอัพเดทรายการอาหารนั้น
            NSLog(@"kACTION_ORDER_CANCEL");
            return AMLocalizedString(@"ยกเลิกรายการ", nil);
        }
            break;
            // PROMOTION
        case 20:{ // admin อนุมัติโปรโมชั่น => staff แสดง popup notification และแสดงปุ่ม promotion นั้น
            NSLog(@"kACTION_PRO_ADMIN_APPROVE");
            return AMLocalizedString(@"อนุมัติโปรโมชั่น", nil);
        }
            break;
        case 21:{ // staff ร้องขออนุมัติโปรโมชัน => admin แสดง notification หรือ อัพเทด list pending
            NSLog(@"kACTION_PRO_STAFF_REQ_APPROVE");
            return AMLocalizedString(@"ร้องขออนุมัติโปรโมชัน", nil);
        }
            break;
            // INVOCE
        case 30:{ // admin อนุบัติใบสั่งสินค้า => checker แสดง notification หรืออัพเทด list invoice
            NSLog(@"kACTION_INV_ADMIN_APPROVE");
            return AMLocalizedString(@"อนุมัติใบสั่งสินค้า", nil);
        }
            break;
        case 31:{ // checker ร้องขออนุมัติใบสั่งสินค้า => admin แสดง notification หรือ อัพเทด list pending
            NSLog(@"kACTION_INV_CHECKER_REQ_APPROV");
            return AMLocalizedString(@"ร้องขออนุมัติใบสั่งสินค้า", nil);
        }
            break;
            // CASHIER_REQ
        case 40:{ // cashier ส่งยอด => admin แสดง notification หรือ อัพเทด list pending
            NSLog(@"kACTION_INV_CASHIER_REQ_APPROV");
            return AMLocalizedString(@"ส่งยอด", nil);
        }
            break;
        case 41:{ // cashier ร้องขอการอนุมัติส่วนลดเพิ่มเติม => admin แสดง notification หรือ อัพเทด list pending
            NSLog(@"kACTION_DISCOUNT_CASHIER_REQ_APPROV");
            return AMLocalizedString(@"ร้องขอการอนุมัติส่วนลดเพิ่มเติม", nil);
        }
            break;
            // STOCK
        case 50:{ // checker ส่งรายงาน stock => admin แสดง notification หรือ อัพเทด list pending
            NSLog(@"kACTION_STOCK_CHECKER_REQ_APPROV");
            return AMLocalizedString(@"ส่งรายงาน", nil);
        }
            break;
            // MENU
        case 60:{ //  kitchen block menu => staff อัพเดทรายการเมนู
            NSLog(@"kACTION_MENU_KITCHEN_BLOCK");
            return AMLocalizedString(@"อัพเดทรายการเมนู", nil);
        }
            break;
        default:
            return @"";
            break;
    }
}
-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}
@end
