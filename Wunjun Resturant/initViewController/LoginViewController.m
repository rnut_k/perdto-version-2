//
//  LoginViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "LoginViewController.h"
#import "IQKeyboardManager.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "AppDelegate.h"
#import "TablesViewController.h"
#import "GlobalBill.h"
#import "Member.h"
#import "GlobalSocket.h"
#import "LoadProcess.h"
#import "Check.h"

#import "UIColor+HTColor.h"

@implementation LoginViewController{
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    GlobalBill *_globalBill;
    
    LoadProcess *loadProgress;
    int countLoginAdmin;
    int countDemonShow;
    
    WYPopoverController *popoverController;
    UITextField *textFieldNum;
    
}

@synthesize mBtnDemo,mLaoutDemoBtn,mLabelRemember,mImageLogo,countTouch;

struct CTResult
{
    int flag;
    int a;
};
-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    NSString *Lang2;
    if (self) {
        NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        Lang2 = [languages objectAtIndex:0];        //NSString
//        LocalizationSetIs(NO);
//        LocalizationSetLanguage(Lang2);
    }
    DDLogInfo(@"LoginViewController load coder %@", Lang2);
    return self;
    
}
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    countLoginAdmin = 0;
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:@"UIApplicationDisconnectSocket"
                          object:self
                        userInfo:nil];
    
    [self.mLaoutDemoBtn setConstant:0];
    [self.mBtnDemo setHidden:NO];
    [self.mBtnDemo setTitle:NSLocalizedString(@"", @"") forState:UIControlStateNormal];
    countDemonShow = 0;

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
//    [LoadProcess dismissLoad];
}
- (IBAction)btnLogin:(id)sender {
    DDLogInfo(@"%@ ",MAPI_URL);
    DDLogInfo(@"%@ ",MAPI_URL_PIC);
    
    loadProgress   = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
    
    NSString *strUsername = [[self.mUsername text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strPassword = [[self.mPassword text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *strNameRestaurant = [[self.mNameRestaurant text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    DDLogInfo(@"username %@",self.mUsername.text);
    DDLogInfo(@"password %@ ",self.mPassword.text);
    DDLogInfo(@"NameRestaurant %@ ",self.mNameRestaurant.text);
    
    if([strUsername isEqualToString:@""] || [strPassword isEqualToString:@""] || [strNameRestaurant isEqualToString:@""])
    {
        UIAlertView * msgAlert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:AMLocalizedString(@"กรุณากรอบข้อมูลให้ครบ", nil)
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [msgAlert show];
    }
    else
    {
        
        loadProgress   = [LoadProcess sharedInstance];
        [loadProgress.loadView showWithStatus];
        DeviceData *device = [DeviceData getInstance];
        if([Check checkNull:[device  token]])
        {
            device.token = @"123";
        }
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer =[AFHTTPResponseSerializer serializer];
        NSDictionary *parameters = @{ @"username":strUsername,
                                      @"password":strPassword,
                                      @"dvi":device.token,
                                      @"dvt":@"ios",
                                      @"lang":[LocalizationSystem getLanguage]
                                      };
        
        DDLogInfo(@"login %@",parameters);
        NSString *url = [NSString stringWithFormat:LOGIN,strNameRestaurant];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if(responseObject != nil) {
               
                NSError *error;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
                NSDictionary * strData = [json objectForKey:@"data"];
                bool login = [[strData objectForKey:@"login"]boolValue];
                
                NSLog(@" member  %@",self.member.code);
                if(login) {
                    [self.member insertLoginData:responseObject];
                    if(self.member.id != nil)  {
                        NSDictionary *parameter = @{@"code":self.member.code,
                                                    @"device_id":self.member.UDID};
                        NSString *url = [NSString stringWithFormat:LOAD_DATA,self.member.nre];
                        [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
                            DeviceData *deviceData = [DeviceData getInstance];
                            deviceData.resturant   = json[@"data"][@"resturant"];
                            deviceData.staff       = json[@"data"][@"staff"];
                            deviceData.menu        = json[@"data"][@"menu"];
                            deviceData.tables      = json[@"data"][@"tables"];
                            deviceData.goods       = json[@"data"][@"good"];
                            deviceData.vendor      = json[@"data"][@"vendor"];
                            deviceData.staffList   = json[@"data"][@"staff_list"];
                            deviceData.outchecker = json[@"data"][@"menu"][@"outchecker"];
                            NSLog(@"out: %@",deviceData.outchecker);
                            
                            deviceData.url_conn   = json[@"data"][@"conn_url"];
                            
                            [deviceData updateDb:json[@"data"]];
                            [LoadMenuAll sortIndexMenu:deviceData.menu withKey:@"0"];
                            
                            [self showMenuView];
                            [LoadProcess dismissLoad];
                        }];
                        _globalBill               = [GlobalBill sharedInstance];
                        _globalBill.billResturant = json[@"resturant"];
                        bool blu                  = [json[@"resturant"][@"use_sub_bill"] boolValue];
                        _globalBill.billType      = blu ? kSubBill:kSingleBill;
                       
                    }
                    else{
                        [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"เข้าสู่ระบบผิดพลาด", nil)];
                    }
                }
                else  {
                     [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"username หรือ password ไม่ถูกต้อง", nil)];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
            DDLogError(@"ERROR btnLogin :%@",newStr);
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"เข้าสู่ระบบผิดพลาด", nil)];
        }];
    }
}
- (IBAction)btnLoginAdmin:(id)sender{
   
    [self.mNameRestaurant setText:@"redgorilla"];
    [self.mUsername setText:@"admin"];
    [self.mPassword setText:@"123456"];
    
    countLoginAdmin ++;
    if (countLoginAdmin == 5) {
        [self showAlertViewLoginAdmin];
    }
}
-(void)showAlertViewLoginAdmin{
        
        UIViewController  *viewController = [[UIViewController alloc] init];
        viewController.title = AMLocalizedString(@"ยอดเงินก่อนชำระ", nil);
        viewController.preferredContentSize = CGSizeMake(300,200);
        viewController.modalInPopover = NO;
        
        UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
        popoverController.delegate = self;
        [popoverController presentPopoverFromRect:self.view.frame
                                           inView:self.view
                         permittedArrowDirections:WYPopoverArrowDirectionNone
                                         animated:NO];
        CGSize size = popoverController.popoverContentSize;
        
        textFieldNum = [[UITextField alloc] initWithFrame:CGRectMake(20,10,size.width-40,50)];
        [textFieldNum setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        textFieldNum.textAlignment = NSTextAlignmentCenter;
        [textFieldNum setBackgroundColor:[UIColor redColor]];
        textFieldNum.layer.cornerRadius = 10.0f;
        textFieldNum.layer.borderWidth = 1.0f;
        textFieldNum.layer.borderColor = [UIColor ht_silverColor].CGColor;
        textFieldNum.clipsToBounds = NO;
        textFieldNum.backgroundColor = [UIColor whiteColor];
        textFieldNum.layer.masksToBounds = YES;
        [textFieldNum setKeyboardType:UIKeyboardTypeNumberPad];
        [viewController.view addSubview:textFieldNum];
        
        UIButton *btnReply = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnReply setFrame:CGRectMake(5,90,size.width-10,40)];
        [[btnReply imageView] setContentMode: UIViewContentModeScaleToFill];
        [btnReply setTitle:AMLocalizedString(@"ยืนยัน", nil) forState:UIControlStateNormal];
        [btnReply  setBackgroundImage:[UIImage imageNamed:@"btn_green"] forState:UIControlStateNormal];
        [btnReply addTarget:self action:@selector(btnCbtnReply:) forControlEvents:UIControlEventTouchUpInside];
        [viewController.view addSubview:btnReply];
}
-(void)btnCbtnReply:(id )sender{
    if ([textFieldNum.text isEqual:@"admin"]) {
        
    }
    [popoverController dismissPopoverAnimated:YES];
}

- (IBAction)btnTestDemo:(id)sender {
    //[self.mNameRestaurant setText:@"redgorilla"];
    //[self.mUsername setText:@"owner"];
    //[self.mPassword setText:@"123456"];
    
    //[self btnLogin:sender];
    if(!self.mViewDemo.hidden){
        [self.mViewDemo setHidden: true];
        [self.mImageDemo setHidden: true];
        [self.mBtnClose setHidden: true];
    }else{
        [self.mViewDemo setHidden: false];
        [self.mImageDemo setHidden: false];
        [self.mBtnClose setHidden: false];
    }
}
- (void)showMenuView
{
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app.window makeKeyAndVisible];
    app.window.rootViewController = [app.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"RoleView"];
}
- (IBAction)btnTagGest:(id)sender {
    [self.mPassword resignFirstResponder];
    [self.mUsername resignFirstResponder];
    [self.mNameRestaurant resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)dealloc
{
    returnKeyHandler = nil;
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(IBAction)btnImageLogo:(id)sender{
    NSLog(@"countTouch: %d",countDemonShow);
    if(countDemonShow == 4){
        [self.mLaoutDemoBtn setConstant:30];
        [self.mBtnDemo setHidden:NO];
        [self.mBtnDemo setTitle:NSLocalizedString(@"Demo", @"Demo") forState:UIControlStateNormal];
        [self.mLabelRemember setText:@""];
    }else{
        countDemonShow = countDemonShow+1;
        NSLog(@"countTouch: %d",countDemonShow);
    }
}

-(IBAction)btnCloseDemo:(id)sender{
    [self.mViewDemo setHidden: true];
    [self.mImageDemo setHidden: true];
    [self.mBtnClose setHidden: true];
}

@end
