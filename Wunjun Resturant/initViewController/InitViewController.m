//
//  InitViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "InitViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "GlobalSocket.h"


@interface InitViewController(){
   LoadProcess *loadProgress;
    
    NSString *lang;
}
@end

@implementation InitViewController

-(id) initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
        lang = [languages objectAtIndex:0]; //NSString
//        LocalizationSetIs(YES);
        LocalizationgetNSBundle;
    }
    DDLogInfo(@"InitViewController load coder %@", lang);
    return self;
    
}

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     loadProgress = [LoadProcess sharedInstance];
    [loadProgress.loadView showWaitProgress];
}
- (void)viewDidUnload {
    [super viewDidUnload];
}
- (void)viewDidAppear:(BOOL)animated
{
    
    sleep(1);
    if([self.member isUserLogin]){
        DeviceData *device = [DeviceData getInstance];
        [device loadData];
        [self performSegueWithIdentifier:kidRoleView sender:nil];
    }
    else{
        [self performSegueWithIdentifier:kidLoginView sender:nil];
    }
    
}
- (void)showMenuView
{
    [self performSegueWithIdentifier:kidRoleView sender:nil];
//    idRoleViewController
//    NSBundle *bnd = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"]];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    UINavigationController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"RoleView"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
}
- (void)showLoginView
{
     [self performSegueWithIdentifier:kidLoginView sender:nil];
//    NSBundle *bnd = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"]];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
//    LoginViewController *navCon = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
//    AppDelegate *app = [[UIApplication sharedApplication] delegate];
//    app.window.rootViewController = navCon;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    NSLog(@"The segue identifier is: %@", [segue identifier]);
    //if ([[segue identifier] isEqualToString:@"YOUR_SEGUE_NAME_HERE"])
}

@end
