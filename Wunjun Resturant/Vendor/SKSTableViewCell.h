//
//  SKSTableViewCell.h
//  SKSTableView
//
//  Created by Sakkaras on 26/12/13.
//  Copyright (c) 2013 Sakkaras. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  SKSTableViewCell is a custom table view cell class extended from UITableViewCell class. This class is used to represent the
 *  expandable rows of the SKSTableView object.
 */

@interface SKSTableViewCell : UITableViewCell

/**
 * The boolean value showing the receiver is expandable or not. The default value of this property is NO.
 */
@property (nonatomic,weak) IBOutlet UILabel *titleCell;
@property (nonatomic,weak) IBOutlet UILabel *selesCell;

@property (weak, nonatomic) IBOutlet UILabel *mLabel;
@property (weak, nonatomic) IBOutlet UILabel *mCount;
@property (weak, nonatomic) IBOutlet UILabel *mTotalFood;
@property (weak, nonatomic) IBOutlet UILabel *mTimeDrink;
@property (weak, nonatomic) IBOutlet UILabel *mCountAndPrice;

@property (nonatomic, assign, getter = isExpandable) BOOL expandable;


//label
@property (weak, nonatomic) IBOutlet UILabel *labelDrinkGirl;
@property (weak, nonatomic) IBOutlet UILabel *labelDrinkSales;
@property (weak, nonatomic) IBOutlet UILabel *labelUsePromotion;
@property (weak, nonatomic) IBOutlet UILabel *labelRequestDiscount;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalSalesTax;


/**
 * The boolean value showing the receiver is expanded or not. The default value of this property is NO.
 */
@property (nonatomic, assign, getter = isExpanded) BOOL expanded;

/**
 * Adds an indicator view into the receiver when the relevant cell is expanded.
 */
- (void)addIndicatorView;

/**
 * Removes the indicator view from the receiver when the relevant cell is collapsed.
 */
- (void)removeIndicatorView;

/**
 * Returns a boolean value showing if the receiver contains an indicator view or not.
 *
 *  @return The boolean value for the indicator view.
 */
- (BOOL)containsIndicatorView;

- (void)accessoryViewAnimation;

@end
