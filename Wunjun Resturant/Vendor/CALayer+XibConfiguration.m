#import "CALayer+XibConfiguration.h"

@implementation CALayer(XibConfiguration)

//-(void)setBorderUIColor:(UIColor*)color
//{
//    self.borderColor = color.CGColor;
//}
//
//-(UIColor*)borderUIColor
//{
//    return [UIColor colorWithCGColor:self.borderColor];
//}
-(void)setBorderIBColor:(UIColor*)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*)borderIBColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

-(void)setShadowIBColor:(UIColor*)color
{
    self.shadowColor = color.CGColor;
}

-(UIColor*)shadowIBColor
{
    return [UIColor colorWithCGColor:self.shadowColor];
}
@end
