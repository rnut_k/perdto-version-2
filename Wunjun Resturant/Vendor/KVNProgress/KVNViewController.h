//
//  KVNViewController.h
//  KVNProgress
//
//  Created by Kevin Hirsch on 24/05/14.
//  Copyright (c) 2014 Pinch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KVNProgress.h"

@interface KVNViewController : UIViewController

- (IBAction)showSuccess;
- (IBAction)showWaitProgress;
- (IBAction)showProgress;
- (IBAction)showWithStatus;
- (IBAction)showWithStatusTitle;
- (IBAction)showWithStatusSignature;
-(IBAction)showError;
- (IBAction)showWithStatusAddError:(NSString *)erro;
- (IBAction)showWithMessage:(NSString *)msg success:(void (^) (BOOL isComplete))completion;
- (IBAction)showWithStatusTitleSet:(NSString *)str;

+(void)dismissProcess;

@end
