//
//  ReConstructionViewController.m
//  Wunjun Resturant
//
//  Created by Rnut on 8/8/2559 BE.
//  Copyright © 2559 AgeNt. All rights reserved.
//

#import "ReConstructionViewController.h"

@interface ReConstructionViewController ()
@property(nonatomic,weak)IBOutlet UIImageView *imgView;
@property(nonatomic,weak)IBOutlet UILabel *label;
@property(nonatomic,weak)IBOutlet UIButton *buttonClose;

- (IBAction)buttonClosePressed:(id)sender;

@end

@implementation ReConstructionViewController
@synthesize label;

- (IBAction)buttonClosePressed:(id)sender{
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *image = [UIImage imageNamed:@"Default.png"];
    [self.imgView setImage:image];
    [self.view addSubview:self.imgView];
    
        if (!UIAccessibilityIsReduceTransparencyEnabled()) {
            self.view.backgroundColor = [UIColor clearColor];
            UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
            blurEffectView.frame = self.view.bounds;
            blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
            [self.view addSubview:blurEffectView];
        } else {
            self.view.backgroundColor = [UIColor blackColor];
        }
//
    label.center = self.view.center;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 3;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = AMLocalizedString(@"reconstruction_message", nil);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    [self.view addSubview:label];
    
    [self.view addSubview:self.buttonClose];
    
}
@end
