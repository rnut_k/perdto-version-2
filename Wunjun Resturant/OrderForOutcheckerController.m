//
//  OrderForOutcheckerController.m
//  Wunjun Resturant
//
//  Created by Nites on 4/26/2559 BE.
//  Copyright © 2559 AgeNt. All rights reserved.
//

#import "OrderForOutcheckerController.h"
#import "Member.h"
#import "DeviceData.h"
#import "SimpleTableCell.h"

@interface OrderForOutcheckerController (){
    NSMutableArray *tableData;
    NSDictionary *menu;
    NSDictionary *tables;
}

@end
@implementation OrderForOutcheckerController;
@synthesize title,member,data,kitchens,outchecker_id,tableList;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.barNavigationBar.topItem.title = title;
    self.member = [Member getInstance];
    [self.member loadData];
    
    [self setTableView];
    
}

- (IBAction)clickBarButtonItem:(id)sender{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void) setTableView{
    
    tableData = [NSMutableArray array];
    NSDictionary *orders = data[@"data"][@"orders"];
    
    if([orders count]>0){
        menu = data[@"data"][@"menu"];
        tables = data[@"data"][@"tables"][@"tables"];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateTable:)
                                                     name:@"reloadDataOrder" object:nil];
        
        for(NSString *key in orders){
            
            Boolean is_add = false;
            for(int i=0;i<self.kitchens.count;i++){
                if([self.kitchens[i] isEqualToString:orders[key][@"kitchen_id"]]){
                    is_add = true;
                }
                NSLog(@"count: %@",self.kitchens[i]);
            }
            
            if(is_add)
                [tableData addObject:orders[key]];
            
            
        }
    }
    
    //NSLog(@"count: %d",[tableData count]);
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

// set view cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    SimpleTableCell *cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    NSString *key = @"id_";
    key = [key stringByAppendingString: [tableData objectAtIndex:indexPath.row][@"menu_id"]];
    NSDictionary *m = menu[key];
    
    NSString *keyt = @"id_";
    keyt = [keyt stringByAppendingString: [tableData objectAtIndex:indexPath.row][@"table_id"]];
    NSDictionary *tt = tables[keyt];
    
    cell.mName.text = m[@"name"];
    cell.mPrice.text = m[@"price"];
    cell.mCount.text = [tableData objectAtIndex:indexPath.row][@"n"];
    cell.mKitchen.text = tt[@"label"];
    return cell;
}


// clcik cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Value Selected by user
    //NSLog(@"click cell: %d",indexPath.row);
    NSDictionary *order = [tableData objectAtIndex:indexPath.row];
    
    // Load order
    LoadProcess *load = [LoadProcess sharedInstance];
    [load.loadView showWithStatus];
    NSDictionary *parameter = @{@"code":self.member.code,
                                @"order_id":order[@"order_id"],
                                @"kitchen_id":order[@"kitchen_id"],
                                @"deliver_status":@"4",
                                @"outchecker_id": outchecker_id};
    NSString *url = [NSString stringWithFormat:SET_DELIVER_STATUS,self.member.nre];
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        
        [tableData removeObjectAtIndex: indexPath.row];
        [tableList reloadData];
        
        [LoadProcess dismissLoad];
    }];
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 60;
    
}

#pragma mark update from socket
- (void)updateTable:(id)sender{
    
    // Load order
    LoadProcess *load = [LoadProcess sharedInstance];
    [load.loadView showWithStatus];
    NSDictionary *parameter = @{@"code":self.member.code,
                                @"device_id":self.member.UDID,
                                @"kitchen_id":@"0",
                                @"deliver_status":@"2"};
    NSString *url = [NSString stringWithFormat:GET_ORDERS_KITCHEN,self.member.nre];
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        data = json;
        [self setTableView];
        [tableList reloadData];
        [LoadProcess dismissLoad];
    }];
}

@end