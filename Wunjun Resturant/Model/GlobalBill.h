//
//  GlobalBill.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalBill : NSObject
{
    NSMutableDictionary *_billResturant;
    NSMutableDictionary *_billList;
    NSString *_billType;
}

+ (GlobalBill *)sharedInstance;

@property(strong, nonatomic, readwrite) NSMutableArray *billListTable;
@property(strong, nonatomic, readwrite) NSMutableDictionary *billStaff;
@property(strong, nonatomic, readwrite) NSMutableDictionary *billResturant;
@property(strong, nonatomic, readwrite) NSMutableDictionary *billList;
@property(strong, nonatomic, readwrite) NSString *billType;
@property(strong, nonatomic, readwrite) NSString *moneyIn;
@property(strong, nonatomic, readwrite) NSString *indexSplitBill;
- (void) reset;
@end
