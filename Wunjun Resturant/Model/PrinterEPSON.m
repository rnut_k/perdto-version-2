//
//  PrinterEPSON.m
//  Wunjun Resturant
//
//  Created by AgeNt on 11/10/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "PrinterEPSON.h"

@interface PrinterEPSON()
+ (NSString*)getEposResultText:(int)result;

@end

@implementation PrinterEPSON

@synthesize printer = _printer;
@synthesize result = _result;

static PrinterEPSON * instantce;

- (id)init {
    self = [super init];
    if (self) {
        _printer = nil;
        _result = 0;
    }
    return self;
}

+ (void)withAddress:(NSString*)address{
    if(instantce.printer != nil){
        [instantce.printer closePrinter];
    }
    
    if (instantce.printer == nil) {
        int connectionType = EPOS_OC_DEVTYPE_TCP;
        
        instantce.printer = [[EposPrint alloc] init];
        instantce.result = [instantce.printer openPrinter:connectionType
                             DeviceName:address
                                Enabled:[self getStatusMonitorEnabled]
                               Interval:1000];
        if(instantce.result != EPOS_OC_SUCCESS){
            DDLogError(@"error withAddress : %@",[self getEposResultText:instantce.result]);
            [PrinterEPSON closePrinterEPSON];
        }
    }
}

+(PrinterEPSON *)sharedInstance
{
    @synchronized(self) {
        if(instantce == nil) {
            [PrinterEPSON closePrinterEPSON];
        }
        instantce = [PrinterEPSON new];
    }
    return instantce;
}

+(BOOL) closePrinterEPSON{
    if(instantce.printer != nil){
        int result = [instantce.printer closePrinter];
        if(result != EPOS_OC_SUCCESS){
            DDLogError(@"showExceptionEpos closePrinter error  : %@",[self getEposResultText:result]);
            return false;
        }
        else{
           DDLogWarn(@"showExceptionEpos closePrinter success : %@",[self getEposResultText:result]);
        }
        instantce.printer = nil;
        instantce.result = 0;
    }
    return true;
}
+ (int)getStatusMonitorEnabled
{
    return EPOS_OC_FALSE;
}
//convert EposPrint/EposBuilder Result to text
+ (NSString*)getEposResultText:(int)result
{
    switch(result){
        case EPOS_OC_SUCCESS:
            return @"SUCCESS";
        case EPOS_OC_ERR_PARAM:
            return @"ERR_PARAM";
        case EPOS_OC_ERR_OPEN:
            return @"ERR_OPEN";
        case EPOS_OC_ERR_CONNECT:
            return @"ERR_CONNECT";
        case EPOS_OC_ERR_TIMEOUT:
            return @"ERR_TIMEOUT";
        case EPOS_OC_ERR_MEMORY:
            return @"ERR_MEMORY";
        case EPOS_OC_ERR_ILLEGAL:
            return @"ERR_ILLEGAL";
        case EPOS_OC_ERR_PROCESSING:
            return @"ERR_PROCESSING";
        case EPOS_OC_ERR_UNSUPPORTED:
            return @"ERR_UNSUPPORTED";
        case EPOS_OC_ERR_OFF_LINE:
            return @"ERR_OFF_LINE";
        case EPOS_OC_ERR_FAILURE:
            return @"ERR_FAILURE";
        default:
            return [NSString stringWithFormat:@"%d", result];
    }
}
@end
