//
//  PrinterDB.m
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "PrinterDB.h"
#import "Time.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Check.h"
#import "NSString+GetString.h"
#import "FMDBDataAccess.h" 

#define DATABASE_NAME @"printer_info.db"

@interface PrinterDB(){
    BOOL debugEnable;
}
@property (nonatomic,strong) NSString *databaseFile;
@property (nonatomic,strong) NSString *databasePath;
@end

@implementation PrinterDB{
    bool databaseIsUsed ;
}
//+ (NSArray *)columnArray
//{
//    static NSArray *columnArray;
//    @synchronized(self) {       // add attribute
//        columnArray = @[@"key",
//                        @"listPrinter",
//                        @"isOpen"];
//    }
//    return columnArray;
//}
//
//-(NSString *)databaseFile{
//    if (!_databaseFile) {
//
////        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
////        NSString *documentDir = [documentPaths objectAtIndex:0];
////        self.databasePath = [documentDir stringByAppendingPathComponent:self.databaseName];
//        
//        NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        _databasePath = [documentPaths firstObject];
//        _databaseFile = [NSString stringWithString:[_databasePath stringByAppendingPathComponent:DATABASE_NAME]];
//        [self createAndCheckDatabase];
//    }
//    return _databaseFile;
//}
//-(void) createAndCheckDatabase
//{
//    BOOL success; 
//    
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    success = [fileManager fileExistsAtPath:self.databaseFile];
//    
//    if(success) return; 
//    
//    NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DATABASE_NAME];    
//    [fileManager copyItemAtPath:databasePathFromApp toPath:self.databaseFile error:nil];
//}
//-(sqlite3 *) newConnection
//{
//    sqlite3 *database = NULL;
//    NSString *path = [self databaseFile];
//    
//    // the database file does not exist, so we need to create one.
//    if ( sqlite3_open_v2([path UTF8String], &database, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL) != SQLITE_OK) {
//        DDLogError(@"Failed to open the database with error %s", sqlite3_errmsg(database));
//    }
//    sqlite3_close(database);
//    database = NULL;
//
//    return database;
//}
////+(PrinterDB *)getInstance
////{
////    static dispatch_once_t onceToken;
////    static PrinterDB *instance = nil;
////    dispatch_once(&onceToken, ^{
////        instance = [[PrinterDB alloc] init];
////    });
////    return instance;
////}
//
//- (id)init {
//    self = [super init];
//    if (self) {
//        [self createLoginDb];
//        if (![Check checkNull:self.databaseFile]) {
//            [self checkVersionDB:kPrinterDB];
//        }
//    }
//    return self;
//}
//- (void) createLoginDb
//{
//    NSFileManager *filemgr = [NSFileManager defaultManager];
//    NSString *path = [self databaseFile];
//    if ([filemgr fileExistsAtPath:path] == NO) {
//        const char *dbpath = [self.databaseFile UTF8String];
//        if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//            char *errMsg;
//            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS printer_info (ID INTEGER PRIMARY KEY AUTOINCREMENT,key TEXT DEFAULT '1', listPrinter TEXT DEFAULT '0' ,isOpen TEXT DEFAULT '0')";            
//            if (sqlite3_exec(_printerDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
//                DDLogError(@"Failed to create table");
//            }
//            sqlite3_close(_printerDB);
//        }
//        else {
//            DDLogError(@"Failed to open/create database");
//        }
//    }
//}
//- (void) loadData:(NSString *)key
//{
//    [self createLoginDb];
//    const char *dbpath  = [self.databaseFile UTF8String];
//
//    sqlite3_stmt *statement;
//    
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM printer_info WHERE key = '1' LIMIT 1 "];
//        
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(_printerDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)  {
//            if (sqlite3_step(statement) == SQLITE_ROW)  {
//                NSError* error;
//                
//                const char *columnText = (const char *)sqlite3_column_text(statement, 1);
//                NSString *key        = [NSString stringWithUTF8String:columnText];
//                NSData *listPrinterD = [self setStatement:statement andIndex:2];
//                NSData *isOpenD      = [self setStatement:statement andIndex:3];  
//                
//                const char *columnText2 = (const char *)sqlite3_column_text(statement, 2);
//                NSString *key2        = [NSString stringWithUTF8String:columnText2];
//                
//                const char *columnText3 = (const char *)sqlite3_column_text(statement, 3);
//                NSString *key3        = [NSString stringWithUTF8String:columnText3];
//                
//                NSString *newStr = [NSString stringWithUTF8String:[isOpenD bytes]];
////                NSData *jsonPrinterD = [listPrinterD dataUsingEncoding:NSUTF8StringEncoding];
////                NSData *jsonIsOp = [isOpenD dataUsingEncoding:NSUTF8StringEncoding];
//                
//                id listPrinter = [[NSString alloc] initWithData:listPrinterD encoding:NSUTF8StringEncoding];
//                id isOpen = [[NSString alloc] initWithData:isOpenD encoding:NSUTF8StringEncoding];
//
//                _key         = [NSString checkNull:key];
//                _listPrinter = [NSString checkNull:listPrinter];
//                _isOpen      = [NSString checkNull:isOpen];   
//            }
//            else {
//                _key         = @"1";
//                _listPrinter = @"data";
//                _isOpen      = @"";
//                NSLog(@"Match not found  printerDB");
//            }
//        }
//        sqlite3_finalize(statement);
//    }
//    [self closeConnection];
//}
//-(NSData *) setStatement:(sqlite3_stmt *)statement andIndex:(int)index{
//    const void *bytes = sqlite3_column_blob(statement, index);
//    int length = sqlite3_column_bytes(statement, index);
//    NSData *myData = [NSData dataWithBytes:bytes length:length];
//    
//    NSData *content = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, index) length:sqlite3_column_bytes(statement, index)];
//    NSString *columnText =  [content base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
//    
//    if (myData == NULL)
//        return  nil;
//    else
//        return myData;
//}
//- (BOOL) isKeyDB:(NSString *)key
//{
//    [self createLoginDb];
//    const char *dbpath = [self.databaseFile UTF8String];
//    sqlite3_stmt *statement;
//    BOOL result = NO;
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK)
//    {
//        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM printer_info WHERE key = '1' LIMIT 1 "];
//        const char *query_stmt = [querySQL UTF8String];
//        
//        if (sqlite3_prepare_v2(_printerDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//        {
//            if (sqlite3_step(statement) == SQLITE_ROW)
//            {
//                result = YES;
//            }
//        }
//        sqlite3_finalize(statement);
//    }
//    [self closeConnection];
//    
//    return result;
//}
//
////- (void) insertLoginData:(NSString *)listPrinter  and:(NSString *)key andIsOpen:(NSString *)isOp {
////    
////    isOp = [NSString checkNull:isOp];
////    
////    if (key != nil) {
////        [self closeConnection];
////        [self createLoginDb];
////        
////        sqlite3_stmt *statement;
////        const char *dbpath = [self.databaseFile UTF8String];
////        if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK)
////        {
//////            const char *sql = [[NSString stringWithFormat:@"DELETE FROM printer_info WHERE key = '1'"] UTF8String];
//////            sqlite3_prepare_v2(_printerDB, sql, -1, &statement, NULL);
//////            if (sqlite3_step(statement) != SQLITE_DONE) {
//////                DDLogError(@"delete error");
//////            }
////            
////            static sqlite3_stmt *insertStmt = nil;            
////            if(insertStmt == nil)   {
////                NSString *insertSql = @"INSERT INTO printer_info(key,listPrinter,isOpen) VALUES('1','?','?')";
////                if(sqlite3_prepare_v2(_printerDB, [insertSql UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
////                    DDLogError(@"Error while insertLoginData  statement. '%s'", sqlite3_errmsg(_printerDB));
////            }
////            
////            sqlite3_bind_text(insertStmt, 2, [listPrinter UTF8String], -1, SQLITE_TRANSIENT);
////            sqlite3_bind_text(insertStmt, 3, [isOp UTF8String], -1, SQLITE_TRANSIENT);
////     
////            if(SQLITE_DONE != sqlite3_step(insertStmt))
////                DDLogError(@"Error while insertLoginData data. '%s'", sqlite3_errmsg(_printerDB));
////            else
////                NSLog(@"Inserted");
////            
////            sqlite3_reset(insertStmt);
////            insertStmt = nil;            
////            sqlite3_finalize(statement);
////            [self closeConnection];
////        }
////    }
////}
//- (void) insertLoginData:(NSString *)listPrinter andIsOpen:(NSString *)isOp {
//    
//    sqlite3_close(_printerDB);
//    
//    const char *dbpath = [self.databaseFile UTF8String];
//
////    sqlite3* db = NULL;
//    int rc=0;
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//        rc = sqlite3_open_v2(dbpath, &_printerDB, SQLITE_OPEN_READWRITE , NULL);
//        if (SQLITE_OK != rc) {
//            sqlite3_close(_printerDB);
//            NSLog(@"Failed to open db connection");
//        }
//        else  {        
//            const char *jsonList = [listPrinter UTF8String]; 
//            const char *jsonIsOp = [isOp UTF8String]; 
//            
//            //        // Bind the sqlStatement parameters
//            //        sqlite3_bind_text(compiledStatement, 1, [@"1" UTF8String], -1, NULL);
//            //        sqlite3_bind_blob(compiledStatement, 2, [jsonList bytes], (int)[jsonList length], NULL);
//            //        sqlite3_bind_blob(compiledStatement, 3, [jsonIsOp bytes], (int)[jsonIsOp length], NULL);
//            //        
//            //        if(result != SQLITE_OK) {
//            //            NSLog(@"createRecord Error:\n%s", sqlite3_errmsg(_printerDB));
//            //        }
//            //        
//            //        // Execute the prepared statement (Insert a record in the database file)
//            //        result = sqlite3_step(compiledStatement);
//            //        
//            //        // Finalize the prepared statement
//            //        sqlite3_finalize(compiledStatement);
//            
//            NSString * query  = [NSString stringWithFormat:@"INSERT INTO printer_info(key, listPrinter, isOpen) VALUES (\'%@\',\'%s\',\'%s\')",@"1",jsonList,jsonIsOp];
//            char * errMsg;
//            rc = sqlite3_exec(_printerDB, [query UTF8String] ,NULL,NULL,&errMsg);
//            if(SQLITE_OK != rc) {
//                NSLog(@"Failed to insert record  rc:%d, msg =   %s",rc,errMsg);
//            }
//        } 
//        
//        sqlite3_close(_printerDB);
//    }
//   
//    
//}
//-(NSString *) getDbFilePath
//{
//    NSString * docsPath= NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES)[0];
//    
//    return [docsPath stringByAppendingPathComponent:@"printer_info.db"];
//}
//- (void) update:(NSString *)listPrinter andIsOpen:(NSString *)isOp {
//    const char *dbpath = [self.databaseFile UTF8String];
//    sqlite3_stmt *updateStatement;
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK)
//    {
//        if(updateStatement == nil) {
//            const char *sql = "UPDATE printer_info set listPrinter = ?, isOpen = ? WHERE key = 1";
//            if(sqlite3_prepare_v2(_printerDB, sql, -1, & updateStatement, NULL) != SQLITE_OK)
//                NSAssert1(0, @"Error while creating update statement. '%s'", sqlite3_errmsg(_printerDB));
//        }
//        
//        sqlite3_bind_text(updateStatement, 1, [listPrinter UTF8String], -1, SQLITE_TRANSIENT);
//        sqlite3_bind_text(updateStatement, 2, [isOp UTF8String], -1, SQLITE_TRANSIENT);
//        
//        
//        if(SQLITE_DONE != sqlite3_step(updateStatement))
//            NSLog(@"Error while updating data. '%s'", sqlite3_errmsg(_printerDB));
//        else
//            sqlite3_reset(updateStatement);
//        
//        sqlite3_close(_printerDB);
//        updateStatement = nil;
//    }
//    else
//        sqlite3_close(_printerDB);
//}
//- (void) deleteLoginDb:(NSString *)keyDelete
//{
//    sqlite3_stmt *deleteStmt;
//    const char *dbpath = [self.databaseFile UTF8String];
//    
//    if (keyDelete != nil) {
//        [self createLoginDb];
//        if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//            const char *sql = [[NSString stringWithFormat:@"DELETE FROM printer_info WHERE key = '1' LIKE 1"] UTF8String];
//            sqlite3_prepare_v2(_printerDB, sql, -1, &deleteStmt, NULL);
//            if (sqlite3_step(deleteStmt) != SQLITE_DONE)    {
//                DDLogError(@"delete error");
//            }
//        }
//        sqlite3_finalize(deleteStmt);
//        [self closeConnection];
//    }
//}
//
//- (void) deleteLoginDb
//{
//    sqlite3_stmt *deleteStmt;
//    const char *dbpath = [self.databaseFile UTF8String];
//    
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//        NSString *insertSQL = @"DELETE FROM printer_info";
//        
//        const char *insert_stmt = [insertSQL UTF8String];
//        sqlite3_prepare_v2(_printerDB, insert_stmt, -1, &deleteStmt, NULL);
//        if (sqlite3_step(deleteStmt) != SQLITE_DONE)  {
//            DDLogError(@"insert error");
//        }
//        
//        sqlite3_finalize(deleteStmt);
//        [self closeConnection];
//    }
//}
//
//-(void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate andIsOpen:(NSString *)isOpen {
//    NSString *keyUpdate = _idUpdate;
//    NSString *val       = jsonObject;
//    
//    if (databaseIsUsed) {
//        [self updateDb:jsonObject andId:_idUpdate andIsOpen:isOpen];
//        return;
//    }
//    databaseIsUsed = TRUE;
//    
//    sqlite3_close(_printerDB);
//    [self createLoginDb];
//    const char *dbpath = [self.databaseFile UTF8String];
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK) {
//        static sqlite3_stmt *insertStmt = nil;
//        if(insertStmt == nil)   {
//            
//            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE printer_info SET key ='1', listPrinter ='11', isOpen ='%@' WHERE rowid = 1",isOpen];
//            NSLog(@"isOpen %@",updateSQL);
//            const char *update_stmt = [updateSQL UTF8String];
//            char *errMsg;             
//            sqlite3_exec(_printerDB, update_stmt, NULL, NULL, &errMsg);   
//            if (sqlite3_step(insertStmt) == SQLITE_DONE)    {
//                NSLog(@" Updated");
//            }            
//            else    {
//                NSLog(@"Error occured %s",errMsg);
//                NSLog(@"Error while updateDb updateDb statement. '%s'", sqlite3_errmsg(_printerDB));
//            }
//            
////            NSString *updateSQL = @"UPDATE printer_info SET listPrinter=555, isOpen=1 WHERE key = 1";
////            if(sqlite3_prepare_v2(_printerDB, [updateSQL UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
////                NSLog(@"Error while updateDb updateDb statement. '%s'", sqlite3_errmsg(_printerDB));
//        }
//        
//        sqlite3_bind_text(insertStmt, 1, [val UTF8String], -1, SQLITE_STATIC);
//        sqlite3_bind_text(insertStmt, 2, [isOpen UTF8String], -1, SQLITE_STATIC);
//        
//        sqlite3_step(insertStmt);
//        sqlite3_finalize(insertStmt);
//        insertStmt = nil;
//    }
//    [self closeConnection];
//    databaseIsUsed = FALSE;
//}
//- (BOOL)updatePrinter:(NSString*)jsonObject andIsOpen:(NSString *)isOpen  {
//    if (debugEnable) NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
//    
//    const char *utf8Dbpath = [self.databaseFile UTF8String];
//    static sqlite3_stmt *statement = nil;
//    
//    if (sqlite3_open(utf8Dbpath, &_printerDB) == SQLITE_OK) {
//        
//        NSString *updateQuery = [NSString stringWithFormat:@"update printer_info set listPrinter='%@', isOpen='%@' where key='1'", jsonObject, isOpen];
//        
//        const char *utf8UpdateQuery = [updateQuery UTF8String];
//        
//        sqlite3_prepare_v2(_printerDB, utf8UpdateQuery, -1, &statement, NULL);
//        
//        if (sqlite3_step(statement) == SQLITE_DONE) {
//            
//            sqlite3_reset(statement);
//            return YES;
//        } else {
//            if (debugEnable) NSLog(@"%s - %d # sqlite3_step != SQLITE_DONE", __PRETTY_FUNCTION__, __LINE__);
//            sqlite3_reset(statement);
//            return NO;
//        }
//    } else {
//        if (debugEnable) NSLog(@"%s - %d # Fail to open DB", __PRETTY_FUNCTION__, __LINE__);
//        sqlite3_reset(statement);
//        return NO;
//    }
//}
//- (BOOL)updateName:(NSString *)jsonObject uniId:(NSString *)_idUpdate gpa:(NSString *)isOpen {
//    debugEnable = true;
//    if (debugEnable) NSLog(@"%s - %d", __PRETTY_FUNCTION__, __LINE__);
//    
//    [self closeConnection];
//    [self createLoginDb];
//    const char *utf8Dbpath = [self.databasePath UTF8String];
//    _printerDB = NULL;
//    if (sqlite3_open(utf8Dbpath, &_printerDB) == SQLITE_OK) {
//        static sqlite3_stmt *statement = nil;
//        
//        NSString *updateQuery = [NSString stringWithFormat:@"update printer_info set listPrinter='%@', isOpen='%@' where key='%@'", jsonObject, isOpen, _idUpdate];
//        const char *utf8UpdateQuery = [updateQuery UTF8String];
//        
//        sqlite3_prepare_v2(_printerDB, utf8UpdateQuery, -1, &statement, NULL);
//        
//        if (sqlite3_step(statement) == SQLITE_DONE) {            
//            sqlite3_reset(statement);
//            return YES;
//        }
//        else {
//            if (debugEnable) NSLog(@"%s - %d # sqlite3_step != SQLITE_DONE", __PRETTY_FUNCTION__, __LINE__);
//            sqlite3_reset(statement);
//            return NO;
//        }
//    }
//    else {
//        if (debugEnable) NSLog(@"%s - %d # Fail to open DB", __PRETTY_FUNCTION__, __LINE__);
//        return NO;
//    }
//}
//-(BOOL) checkVersionDB:(NSString *)key{
//    
//    @try {
//        sqlite3_close(_printerDB);
//        sqlite3 *database = [self newConnection];
//        
//        NSArray *listColumn = [[self class] columnArray];
//        BOOL statusAddColumn = NO;
//        const char *dbpath  = [self.databaseFile UTF8String];
//        sqlite3_stmt *statement;
//        int error = sqlite3_open(dbpath, &database);
//        if (error == SQLITE_OK)
//        {
//            NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM printer_info WHERE key = '1' LIMIT 1 "];
//            const char *query_stmt = [querySQL UTF8String];
//            if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK)
//            {
//                if (sqlite3_step(statement) == SQLITE_ROW) {
//                    for (int i = 0; i < [listColumn count]; i++) {
//                        const char *columnText = (const char *)sqlite3_column_text(statement, i);
//                        if(columnText == NULL){
//                            sqlite3_finalize(statement);             // ปิด sqlite3_step
//                            [self closeConnection];             // ปิด sqlite3_close DB
//                            statusAddColumn = [self addComlumDB:i];
//                            break;
//                        }
//                    }
//                }
//            }
//        }
//        else {
//            [self errorDB:nil andSQL:dbpath andName:@"checkVersionDB"];
//        }
//        [self closeConnection];
//        return statusAddColumn;
//    }
//    @catch (NSException *exception) {
//        DDLogError(@"error %@",exception);
//    }
//}
//
//-(BOOL)addComlumDB:(int )vs{
//    
//    NSArray *listColumn = [[self class] columnArray];
//    const char *dbpath = [self.databaseFile UTF8String];
//
//    if (sqlite3_open(dbpath, &_printerDB) == SQLITE_OK)  {
//        for (int i = vs; i < [listColumn count]; i++) {
//            static sqlite3_stmt *insertStmt = nil;
//            NSString *addColumnSQL = [NSString stringWithString:listColumn[i]];
//            [self addColumn:addColumnSQL index:i sqlite3Stmt:insertStmt];
//        }
//        [self closeConnection];
//        return YES;
//    }
//    [self closeConnection];
//    return NO;
//}
////- (void)execSqliteWithSQL:(NSString *)sql{
////
////    const char *dbpath = [self.databaseFile UTF8String];
////    char *errMsg;
////    const char *sql_stmt = "ALTER TABLE printer_info ADD COLUMN isOpen_2 INT DEFAULT 0";
////    
////    if (sqlite3_exec(_printerDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
////    {
////        NSString *errorString = [NSString stringWithFormat:@"\n execResult error=>\n %s \n SQL:\n%@\n",errMsg,sql_stmt];
////        NSLog(@"%@",errorString);
////    }
////}
//- (void)addColumn:(NSString*)column index:(int )index sqlite3Stmt:(sqlite3_stmt *)insertStmt{
//
//    NSString *nsql = [NSString stringWithFormat:@"ALTER TABLE printer_info ADD COLUMN  '%@' TEXT DEFAULT 0", column];
//    const char *sql_stmt = [nsql UTF8String];
//    // update
//    
//    char *errMsg;
//    // execute the sql commands
//    int error = sqlite3_exec(_printerDB, sql_stmt, NULL, NULL, &errMsg);
//    if (error == SQLITE_OK) {
//        NSLog(@"successfully created database");
//    }
//    else {
//        [self errorDB:errMsg andSQL:sql_stmt andName:@"addColumn"];
//    }
//    
//    sqlite3_finalize(insertStmt);
//}
//-(void) closeConnection {
//    if(_printerDB) {
//        if (sqlite3_close(_printerDB) != SQLITE_OK )  {
//            DDLogError(@"Failed to close the database with erro : %s", sqlite3_errmsg(_printerDB));
//        }
//        _printerDB = NULL;
//    }
//}
//-(void)errorDB:(char *)errMsg andSQL:(const char *)sql_stmt andName:(NSString *)method{
//        NSString *errorString = [NSString stringWithFormat:@"\n %@ \n execResult error=>\n %s \n SQL:\n%s\n",method,errMsg,sql_stmt];
//        DDLogError(@"%@",errorString);
//}
@end



