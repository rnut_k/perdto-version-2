//
//  LoadMenuAll.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "Time.h"
#import "Member.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "GlobalBill.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "OrderDB.h"
#import "PrinterDB.h"
#import "ListDataPrinter.h"
#import "InvoiceDB.h"
#import "OrdersKitchen.h"
#import "Check.h"
#import "FMDBDataAccess.h"

#define kIsLogin (@"101")

@implementation LoadMenuAll
// type = 1 food
//        2 drink
//        3 drinkgirl

+(void)checkDataMenu:(NSString *)key{
    
    NSString *menukey = [NSString stringWithFormat:@"menu_%@",key];
    Member *_member = [Member getInstance];
    [_member loadData];
     MenuDB *_menuDB = [MenuDB getInstance];
    [_menuDB loadData:menukey];
    LoadProcess *loadProgress  = [LoadProcess sharedInstance];
//    if (![_menuDB isKeyDB:key] || ![_menuDB.date isEqual:[Time getTimeALL]]) {
    if (true) {

        [loadProgress.loadView showWithStatus];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer =[AFHTTPResponseSerializer serializer];
        NSDictionary *parameters = @{@"code":_member.code,
                                     @"device_id":_member.UDID,
                                     @"type":key,
                                     @"subtype":@"0",
                                     @"lang":_member.lg};
        
        NSString *url = [NSString stringWithFormat:GET_MENU_All,_member.nre];
        [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if(responseObject != nil)
            {
//                [KVNProgress dismiss];
//                NSDictionary *menu =[[[NSDictionary alloc]init] mutableCopy];
                NSString *received = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSDictionary dictionaryWithString:received];
         
                if([[dict[@"error"] stringValue] isEqualToString:LOGIN_ERROR])
                {
                    [LoadMenuAll logoutView:dict];
                }
                else
                {
                    [self sortIndexMenu:dict[@"data"] withKey:key];
                }
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [LoadProcess dismissLoad];
            NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
            DDLogError(@" ERROR checkDataMenu  %@",newStr);
            [self checkNetwork];
        }];
    }else{
        [LoadProcess dismissLoad];
    }
}

#pragma mark sort menu
+(void)sortIndexMenu:(NSDictionary*)dict withKey:(NSString*)key
{
    NSString *menukey = [NSString stringWithFormat:@"menu_%@",key];
    Member *_member = [Member getInstance];
    [_member loadData];
    MenuDB *_menuDB = [MenuDB getInstance];
    [_menuDB loadData:menukey];
    
    NSDictionary *menu =[[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *kitchen = [dict objectForKey:@"kitchen"];
    NSDictionary *category = [dict objectForKey:@"category"];
    NSDictionary *drink_menu = [dict objectForKey:@"drink_menu"];
    NSDictionary *drinkgirl = [dict objectForKey:@"drinkgirl"];
    NSDictionary *tag = [dict objectForKey:@"tag"];
    if([key isEqualToString:@"0"])
    {
        NSDictionary *menuEachType = [[[NSDictionary alloc]init] mutableCopy];
        if([dict objectForKey:@"menu"] != nil){
            for(NSString *k in [dict objectForKey:@"menu"]){
                NSDictionary *rowVal = [[dict objectForKey:@"menu"] objectForKey:k];
                menuEachType = [menu objectForKey:[NSString stringWithFormat:@"menu_%@", [rowVal objectForKey:@"type"]]];
                menuEachType = (menuEachType == nil)?[[[NSDictionary alloc]init] mutableCopy]:menuEachType;
                [menuEachType setValue:rowVal forKey:k];
                [menu setValue:menuEachType forKey:[NSString stringWithFormat:@"menu_%@", [rowVal objectForKey:@"type"]]];
                [menu setValue:menuEachType forKey:[NSString stringWithFormat:@"menu_0"]];
            }
        }
    }
    
    [menu setValue:[dict objectForKey:@"menu"] forKey:[NSString stringWithFormat:@"menu_%@", key]];
    
    if(menu != nil){
        NSDictionary *cache_data = [[NSDictionary alloc]init];
        for(NSString *mk in menu){
            NSError *error = nil;
            cache_data = @{
                           @"menu":[menu objectForKey:mk],
                           @"drinkgirl":drinkgirl,
                           @"drink_menu":drink_menu,
                           @"category":category,
                           @"kitchen":kitchen,
                           @"tag":tag
                           };
            NSString *received = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:cache_data
                                                                                                options:NSJSONWritingPrettyPrinted
                                                                                                  error:&error]
                                                       encoding:NSUTF8StringEncoding];
            if (![_menuDB isKeyDB:mk]){
                [_menuDB insertLoginData:received and:mk];
            }
            else{
                NSString *time = [Time getTimeALL];
                [_menuDB updateDb:@{@"valueMenu":received,@"date":time} andId:mk];
            }
        }
    }
    [LoadProcess dismissLoad];
}

+(void)loadData:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion{
    
    Member *_member = [Member getInstance];
    [_member loadData];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryMutableWithObject:parameters];
    [dic setObject:_member.UDID forKey:@"device_id"];
    [dic setObject:_member.lg forKey:@"lang"];
    parameters = [dic copy];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFHTTPResponseSerializer serializer];
    [manager POST:url  parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject != nil)
        {
            NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            if(json != nil){
                if([[json[@"error"] stringValue] isEqualToString:LOGIN_ERROR]){
                    
                    
                    NSString *url2 = [NSString stringWithFormat:LOGOUT,_member.nre];
                    
                    if ([url2 isEqualToString:url]) {
                        completion(json);
                    }else
                    [LoadMenuAll logoutView:json];
                }
                else if(completion) {
                    completion(json);
                }
            }
            else{
                 DDLogError(@"error loadData %@ \n %@",json,newStr);
                [LoadProcess dismissLoad];
            }
        }        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [LoadProcess dismissLoad];
        NSString  *newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        DDLogError(@"ERROR loadData url %@  %@",url, newStr);
        [self checkNetwork];
    }];
}
+(void)loadData:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion error:(void (^)(NSDictionary * json))_error{
    
    Member *_member = [Member getInstance];
    [_member loadData];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryMutableWithObject:parameters];
    [dic setObject:_member.UDID forKey:@"device_id"];
    [dic setObject:_member.lg forKey:@"lang"];
    parameters = [dic copy];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFHTTPResponseSerializer serializer];
    [manager POST:url  parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject != nil)
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            if(json != nil){
                if(completion) {
                    completion(json);
                }
                else{
                    [LoadMenuAll logoutView:json];
                    DDLogError(@"1. error loadData %@",json);
                }
            }
            else{
                _error(json);
                [LoadMenuAll logoutView:json];
                DDLogError(@" 2. error loadData %@",json);
            }
        }
        else{
            _error(responseObject);
            DDLogError(@" 3. error loadData %@",responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        DDLogError(@"ERROR loadData  %@",newStr);
        [self checkNetwork];
    }];
}
+(void)loadDataImage:(NSData *)imageData  andParameter:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:url   parameters:parameters   constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"image.jpg" mimeType:@"image/jpg"];
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        if(responseObject != nil)
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                 options:kNilOptions error:&error];
            if(json != nil){
                if(completion) {
                    completion(json);
                }
                else{
                    [LoadMenuAll logoutView:json];
                }
            }
            else
                DDLogError(@"error loadDataImage %@",json);
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        DDLogError(@"Failure  loadDataImage %@, %@", error, [task.response description]);
        [self checkNetwork];
    }];
}
+(void)retrieveData:(NSString *)_id  success:(void (^)(NSArray * dictionary))completion{
    Member *_member = [Member getInstance];
    [_member loadData];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFHTTPResponseSerializer serializer];
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"device_id":_member.UDID,
                                 @"zone_id":_id,
                                 @"lang":_member.lg};
    
    NSString *url = [NSString stringWithFormat:GET_TABLES,_member.nre];
    [manager POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject != nil)
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            NSDictionary  *data = [json objectForKey:@"data"];
            NSArray  *dataTables = [data objectForKey:@"tables"];
            
            if([dataTables count] != 0){
                if(completion) {
                    completion(dataTables);
                }
                else{
                    [LoadMenuAll logoutView:json];
                }
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        DDLogError(@"ERROR retrieveData %@",newStr);
        [self checkNetwork];
    }];
}
+(NSArray *) requirebillID:(NSString *)idTable {
    GlobalBill *globalBill = [GlobalBill sharedInstance];
    NSMutableDictionary *dataListSingBill = globalBill.billList;
    NSString *billID  = [dataListSingBill objectForKey:@"bill_id"];
    if (![Check checkNull:globalBill.indexSplitBill]) {
        billID = globalBill.indexSplitBill;
    }
    
    NSString *numTable = [NSString stringWithFormat:@"table_id_%@",idTable];
    NSString *usageID = [dataListSingBill valueForKey:@"usage"][numTable][@"id"];
    if (usageID == nil) {
        usageID = [dataListSingBill valueForKey:@"usage"][@"id"];
    }
    return [NSArray arrayWithObjects:usageID,billID,nil];
}
+(void)loadDataOpenTable:(NSString *)idTable  success:(void (^)(NSDictionary * dictionary))completion{
    Member *_member = [Member getInstance];
    [_member loadData];
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"action":@"in",
                                 @"table_id":idTable,
                                 @"lang":_member.lg};
    NSString *url = [NSString stringWithFormat:GET_BILL,_member.nre,idTable];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSDictionary *_data = json[@"data"];
        if([_data count] != 0){
            if(completion) {
                GlobalBill *_globalbill = [GlobalBill sharedInstance];
                _globalbill.billList = (NSMutableDictionary *)_data;
                completion(_data);
            }
            else{
                [LoadMenuAll logoutView:json];
            }
        }
    }];
}

//ออกจากระบบ
//staff/logoutSubmit{
+(void)loadDataLogout:(NSString *)idTable  success:(void (^)(BOOL status))completion{
    Member *_member = [Member getInstance];
    [_member loadData];
    DeviceData *device = [DeviceData getInstance];
    [device loadData];
    
    if ([Check checkNull:device.token]) {
        device.token = @"123";
    }
    
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"dvi":device.token,
                                 @"dvt":@"ios"
                                 };
    NSString *url = [NSString stringWithFormat:LOGOUT,_member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        BOOL status = [json[@"status"] boolValue];
        completion(status);
    }];
}
+(void)logoutView:(NSDictionary *)data{
    NSString *err = [NSString stringWithFormat:@"%@",data[@"error"]];
    if ([err isEqualToString:LOGIN_ERROR]) {
        [self loadDataLogout:@"" success:^(BOOL status) {
            //if (status) {
                [LoadMenuAll resetDataApp];
            //}
        }];
    }
}
+(void) resetDataApp{
    DeviceData *device = [DeviceData getInstance];
    if(device.token == nil)
        device.token = @"";
    
    Member *_member = [Member getInstance];
    [_member deleteLoginDb];
    
    MenuDB *menuDB = [MenuDB getInstance];
    [menuDB deleteLoginDb];
    
    OrderDB *orderDB1 = [OrderDB getInstance];
    [orderDB1 deleteLoginDb];
    
    GlobalBill *globalBill = [GlobalBill sharedInstance];
    [globalBill reset];
    
    InvoiceDB *invoice = [InvoiceDB getInstance];
    [invoice deleteLoginDb:@"delete"];
    
    OrdersKitchen *orderKit = [OrdersKitchen getInstance];
    [orderKit reSet];
    
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    [db deleteDateBase];
    
    [[self class] getDataDB];
    
    AppDelegate *app = [[UIApplication sharedApplication] delegate];
    [app.window makeKeyAndVisible];
    app.window.rootViewController = [app.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    
}
+(void) getDataDB{
    
    PrinterDB *printerDB = [[PrinterDB alloc] init];
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    printerDB = [db getCustomers];
    
    NSMutableDictionary *tempDic;
    if (printerDB.listPrinter) {
        NSDictionary *dictList = [NSJSONSerialization JSONObjectWithData:[printerDB.listPrinter dataUsingEncoding:NSUTF8StringEncoding]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
        tempDic = [NSMutableDictionary dictionaryMutableWithObject:dictList];
        int i = 0;
        for (i = 0 ; i < [tempDic  count] ; ++i) {
            NSMutableDictionary *dic = tempDic[[@(i) stringValue]];
            BOOL bl = [dic[@"isSeclect"] boolValue];
            if (bl) {
                [dic setValue:@"NO" forKey:@"isSeclect"];
            }
        }
    }
    if ([tempDic  count] != 0) {
        NSError *writeError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tempDic
                                                           options:NSJSONWritingPrettyPrinted error:&writeError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
//        [printerDB insertLoginData:jsonString andId:kPrinterDB andIsOpen:@"0"];
//        [printerDB insertLoginData:jsonString andIsOpen:@"0"];
        printerDB.listPrinter = jsonString;
        printerDB.isOpen = @"0";
        [db updateCustomer:printerDB];
    }
    
}
+(void)checkNetwork{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    //    NSString *url = [NSString stringWithFormat:MAPI_URL,self.member.nre];
    //    Reachability *reach = [Reachability reachabilityWithHostName:url];
    NetworkStatus status  = [reach currentReachabilityStatus];
    NSLog(@"status %ld ",(long)status);
    if (status == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ไม่ได้เชื่อมต่อเน็ต"
                                                        message:@"กรุณาเปิดระบบเชื่อมต่อเน็ต"
                                                       delegate:nil
                                              cancelButtonTitle:@"ใช่"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
@end
