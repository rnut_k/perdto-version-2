//
//  SelectedIndex.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SelectedIndex : NSObject

@property(nonatomic) NSInteger selectedIndex;

+(SelectedIndex *)getInstance;

@end
