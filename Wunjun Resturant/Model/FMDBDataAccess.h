//
//  FMDBDataAccess.h
//  Chanda
//
//  Created by Mohammad Azam on 10/25/11.
//  Copyright (c) 2011 HighOnCoding. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h" 
#import "FMResultSet.h" 
#import "PrinterDB.h" 

@interface FMDBDataAccess : NSObject
{
    
}
@property (nonatomic,strong) NSString *databasePath; 

+(NSString *) getDatabasePath;
-(PrinterDB *) getCustomers;
- (BOOL) isKeyDB:(NSString *)key;
-(BOOL) insertCustomer:(PrinterDB *) customer; 
-(BOOL) updateCustomer:(PrinterDB *) customer; 
-(BOOL)deleteDateBase;
@end
