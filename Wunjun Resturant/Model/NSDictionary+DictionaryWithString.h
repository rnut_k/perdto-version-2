//
//  NSDictionary+DictionaryWithString.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/13/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DictionaryWithString)
+ (NSDictionary *)dictionaryWithString:(NSString *)string;
+(NSArray *)searchDictionary:(NSString *)key andvalue:(NSString *)value andData:(NSArray *)data;
+(NSArray *)searchDictionaryALL:(NSString *)key andvalue:(NSString *)value andData:(NSArray *)data;
+(NSArray *)splitString:(NSString *)str;
+(NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array;
+(NSDictionary *)dictionaryWithObject:(id)object;
+(NSMutableDictionary *) dictionaryMutableWithObject:(id)object;
@end
