//
//  UIStoryboard+Main.h
//  Wunjun Resturant
//
//  Created by AgeNt on 10/9/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LocalizationSystem.h"

#define UIStoryboardMain \
    [[UIStoryboard sharedLocalSystem] getStoryboardMain]

@interface UIStoryboard (Main)


+ (UIStoryboard *)sharedLocalSystem;

//gets UIStoryboard
-(UIStoryboard *) getStoryboardMain;

@end
