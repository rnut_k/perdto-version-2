//
//  LocalizationSystem.h
//  Battle of Puppets
//
//  Created by Juan Albero Sanchis on 27/02/10.
//  Copyright Aggressive Mediocrity 2010. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Member.h"
#import "AppDelegate.h"
#import "LoadProcess.h"

#define LocalizationSetIs(key) \
[[LocalizationSystem sharedLocalSystem] setIs:(key)]

#define AMLocalizedString(key, comment) \
[[LocalizationSystem sharedLocalSystem] localizedStringForKey:(key) value:(comment)]

#define LocalizationSetLanguage(language) \
[[LocalizationSystem sharedLocalSystem] setLanguage:(language)]

#define LocalizationGetLanguage \
[[LocalizationSystem sharedLocalSystem] getLanguage]

#define LocalizationReset \
[[LocalizationSystem sharedLocalSystem] resetLocalization]

#define LocalizationgetNSBundle \
[[LocalizationSystem sharedLocalSystem] getNSBundle]

@interface LocalizationSystem : NSObject {
	NSString *language;
}
@property (nonatomic,strong) LoadProcess *loadProcess;
@property (nonatomic,strong) Member *member;
@property (nonatomic, readwrite) BOOL  isSet;;

//+(void)setLanguage:(NSString*)language;
//
//// you really shouldn't care about this functions and use the MACROS
+ (LocalizationSystem *)sharedLocalSystem;

//gets the string localized
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment;

//sets the language
- (void) setLanguage:(NSString*) language;

//gets the current language
+ (NSString*) getLanguage;

//resets this system.
- (void) resetLocalization;

//gets NSBundle
- (NSBundle *) getNSBundle;

- (void)setIs:(BOOL)isSet;

@end
