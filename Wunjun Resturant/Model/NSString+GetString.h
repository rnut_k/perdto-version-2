//
//  NSString+GetString.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GetString)
+(NSString *) getDrinkgirlTypeID:(NSString *)type;
+(NSString *) getDrinkgirlTypeName:(NSString *)type;
+(NSAttributedString *)checkDeliverStatus:(NSString *)text;
+(NSString *)conventJSON:(NSArray *)data;
+(NSString *)conventCurrency:(NSString*) total;
+(NSString *)checkNull:(NSString *)str;
+(float )checkNullConvertFloat:(NSString *)str;
+(NSString *) checkNullComparetT:(NSString *)strt andF:(NSString *)strf;
+(NSString *)checkNullCurrency:(NSString *)str;
@end
