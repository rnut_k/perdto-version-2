//
//  OrdersKitchenTableViewCell.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersKitchenTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *idLbl;
@property (strong, nonatomic) IBOutlet UITextView *nameLbl;
@property (strong, nonatomic) IBOutlet UITextView *optionLbl;
@property (strong, nonatomic) IBOutlet UITextView *commentLbl;
@property (strong, nonatomic) IBOutlet UILabel *tableLbl;
@property (strong, nonatomic) IBOutlet UILabel *actionLbl;

@end
