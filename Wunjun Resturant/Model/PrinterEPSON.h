//
//  PrinterEPSON.h
//  Wunjun Resturant
//
//  Created by AgeNt on 11/10/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ePOS-Print.h"
#import "ShowMsg.h"

@interface PrinterEPSON : NSObject
@property(nonatomic,retain) EposPrint *printer;
@property(nonatomic,assign) int result;

- (id)init;
+ (void)withAddress:(NSString*)address;
+ (PrinterEPSON *)sharedInstance;
+(BOOL) closePrinterEPSON;
@end
