//
//  NSString+GetString.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "NSString+GetString.h"
#import "Constants.h"
#import <UIKit/UIKit.h>
#import "Check.h"

@implementation NSString (GetString)


+(NSString *) getDrinkgirlTypeID:(NSString *)type{
    if ([type isEqual:kDrinkOne]) {
        return @"1";
    }
    else   if ([type isEqual:kDrinkTwo]) {
        return @"2";
    }
    else{
        return @"3";
    }
}
+(NSString *) getDrinkgirlTypeName:(NSString *)type{
    if ([type isEqual:@"1"]) {
        return kDrinkOne;
    }
    else   if ([type isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else{
        return kDrinkThree;
    }
}
+(NSAttributedString *)checkDeliverStatus:(NSString *)text{     // เส้นกลางตัวอักษร
    NSDictionary *attrDict2 = @{ NSStrikethroughStyleAttributeName: @(3),
                                 NSFontAttributeName: [UIFont systemFontOfSize:12]};
    return [[NSAttributedString alloc] initWithString:text attributes: attrDict2];
}
+(NSString *)conventJSON:(NSArray *)data{
    NSError *error = nil;
    NSString *_string = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:data
                                                                                         options:NSJSONWritingPrettyPrinted
                                                                                           error:&error]
                                                encoding:NSUTF8StringEncoding];
    return _string;
}

+(NSString *)checkNull:(NSString *)str{
    if ([Check checkNull:str]) {
        return @"";
    }
    return [NSString stringWithFormat:@"%@",str];
}
+(NSString *)checkNullCurrency:(NSString *)str{
    if ([Check checkNull:str]) {
        return @"0";
    }
    else{
        return [NSString conventCurrency:str];
    }
}
+(NSString *)conventCurrency:(NSString*)total {
    float floatValue = [total floatValue];
    
    NSNumber *num2 = [NSNumber numberWithDouble:floatValue];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setNegativeFormat:@"-#,##0.00"];
    [numberFormatter setPositiveFormat:@"#,##0.00"];
    
    return [numberFormatter stringFromNumber:num2];
}
+(float )checkNullConvertFloat:(NSString *)str{
    NSString *num = [NSString checkNull:str];
    return [num isEqualToString:@""] ? 0 :[num floatValue];
}
+(NSString *) checkNullComparetT:(NSString *)strt andF:(NSString *)strf{
    NSString *num     = [NSString checkNull:strf];
    NSString *comment = ([num isEqualToString:@""])?strf:num;
    return comment;
}
//+(NSString *)rep:(NSString *)strR andCheck:(NSString *)strC{
//    NSString *strTemp = [strR copy];
//    strTemp = [strTemp stringByReplacingOccurrencesOfString:@"string"
//                                         withString:strC];
//    return strTemp;
//}
@end
