//
//  MainInterface.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "MainInterface.h"

@implementation MainInterface

@synthesize isCustomer = _isCustomer;
@synthesize isMain = _isMain;

- (id)init {
    self = [super init];
    if (self) {
        _isCustomer = NO;
        _isMain = NO;
    }
    return self;
}

+ (MainInterface *)sharedInstance {
    static dispatch_once_t onceToken;
    static MainInterface *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[MainInterface alloc] init];
    });
    return instance;
}


@end
