//
//  Time.h
//  ; Resturant
//
//  Created by AgeNt on 2/6/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalizationSystem.h"

@interface Time : NSObject

@property(nonatomic,strong) NSDateComponents *components;

+(NSString *) getTime;
+(NSString *) getTimeALL;
+(NSString *) getTimeandDate;
+(NSString *) conventTime:(NSString *) time;
+(NSString *) getDateFromDatetime:(NSString *) time;
+(NSDateComponents *)getTimeDMY:(NSDate *)timeStam;
+(NSString *)getTimeDMY;
+(NSString *)getICTFormateDate:(NSString *)time;
+(NSString*)toStringFromDateTime:(NSDate*)datetime;
+(NSString *)covnetTimeDMY;
+(NSString *)covnetTimeDMY1:(NSString *)timeStamp;

+(NSMutableArray *) sortDateAscending:(NSMutableArray *)data;
+(NSMutableArray *) sortDateDescriptors:(NSMutableArray *)data;
+(NSString *)getDateFromDate:(NSString *)time;
+(NSString *)getTHTimeFromDate:(NSString *)time;
+(NSString *)getTHFullFromDate:(NSString *)time;

+(NSString *)getTHFromDate:(NSString *)time;

+(NSMutableArray *) sortDateDescriptorsDeliverDatetime:(NSMutableArray *)data;

+(NSString*)getMountTH:(NSInteger )numberMonth;

@end
