//
//  PrinterDB.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface PrinterDB : NSObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *listPrinter;
@property (nonatomic, strong) NSString *isOpen;         // status printer
@property (nonatomic, strong) NSString *isMenuSug;         // MenuSuggestion

//@property (nonatomic) sqlite3 *printerDB;
//
////+(PrinterDB *)getInstance;
//- (void) createLoginDb;
//- (void) loadData:(NSString *)key;
//- (void) insertLoginData:(NSString *)listPrinter  andIsOpen:(NSString *)isOp ;
//- (void) deleteLoginDb:(NSString *)keyDelete;
////-(void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate andIsOpen:(NSString *)isOpen ;
//- (BOOL) isKeyDB:(NSString *)key;
//- (void) deleteLoginDb;
////- (BOOL)updateName:(NSString *)jsonObject uniId:(NSString *)_idUpdate gpa:(NSString *)isOpen ;
////- (BOOL)updatePrinter:(NSString*)jsonObject andIsOpen:(NSString *)isOpen;
//- (void) update:(NSString *)listPrinter andIsOpen:(NSString *)isOp ;

@end
