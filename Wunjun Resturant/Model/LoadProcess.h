//
//  LoadProcess.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KVNViewController.h"

@interface LoadProcess : NSObject
@property(nonatomic,retain) KVNViewController *loadView;

+(LoadProcess *)sharedInstance;
+(void)dismissLoad;
@end
