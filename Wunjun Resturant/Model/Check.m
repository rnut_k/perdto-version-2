//
//  Check.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "Check.h"
#import <UIKit/UIKit.h>
#import <sys/sysctl.h>

@implementation Check

+(BOOL)checkDevice{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        // Place iPhone/iPod specific code here...
         return true;
    } else {
        // Place iPad-specific code here...
         return false;
    }
}

+(BOOL)checkNull:(id )object{
    @try {
        if ([object isKindOfClass:[NSString class]]) {
            NSString *str = (NSString *)object;
            if([str isKindOfClass:[NSNull class]] || ( str == nil ) || [str isEqual:[NSNull null]] || (!str.length))
                return YES;
            else
                return NO;
        }
        else{
            if([object isKindOfClass:[NSNull class]] ||
               ( object == nil ) ||
               [object isEqual:[NSNull null]]){
                    return YES;
            }
            else{
                if ([object isKindOfClass:[NSDictionary class]] ||
                    [object isKindOfClass:[NSMutableDictionary class]] ||
                    [object isKindOfClass:[NSArray class]] ||
                    [object isKindOfClass:[NSMutableArray class]]  ) {
                    
                    if ([object count] > 0) {
                        return NO;
                    }
                    return YES;
                }
                return NO;
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"checkNull checkObjectType %@ ",exception);
    }
}

+(id)checkObjectType:(id)object{
    if ([object count] != 0) {
        return object;
    }
    return nil;
}
+ (NSString *)platformRawString {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}
+(BOOL) checkVersioniOS8{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        return YES;
    else
        return NO;
}
@end
