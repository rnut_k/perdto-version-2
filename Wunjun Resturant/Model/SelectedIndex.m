//
//  SelectedIndex.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "SelectedIndex.h"

@implementation SelectedIndex

static SelectedIndex * instantce;
+(SelectedIndex *)getInstance
{
    @synchronized(self)
    {
        if(instantce == nil)
        {
            instantce = [SelectedIndex new];
        }
    }
    return instantce;
}

@end
