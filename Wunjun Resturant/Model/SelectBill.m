//
//  SelectBill.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/13/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "SelectBill.h"
#import "TableSubBillViewController.h"
#import "TableSingleBillViewController.h"
#import "AlertViewNotiController.h"
#import "CustomNavigationController.h"

#import "GlobalBill.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "LoadMenuAll.h"

@interface SelectBill(){
    
}

@end

static BOOL isStaffTake;
static BOOL isCashierTake;
static BOOL isCashier;
static BOOL checkOpenTable;
static BOOL iscashTableStatus;
static AppDelegate *appdelegate;

@implementation SelectBill

+(void)setCheckOpenTable:(BOOL)_checkOpenTable{         //  สถานะการเปิดโต็ะ = NO
    checkOpenTable = _checkOpenTable;
}
+(void)setIscashTableStatus:(BOOL)_iscashTableStatus{   // checker class cashTableStatus
    iscashTableStatus = _iscashTableStatus;
}
+(void)setIsCashier:(BOOL)_isCashier{                   //  class cashier
    isCashier = _isCashier;
}
+(void)setIsTake:(int)num{                              //  class takeaway
    if (num == 0) {
        isStaffTake = YES;
        isCashierTake = NO;
    }
    else if (num == 1) {
        isCashierTake = YES;
        isStaffTake = NO;
    }
    else{
        isStaffTake = NO;
        isCashierTake = NO;
    }
}

//+ (UIStoryboard *)mainStoryboard {
//    UIStoryboard *mainStoryboard;
//    if (!mainStoryboard) {
//        mainStoryboard = [UIStoryboard storyboardWithName:@"Main"  bundle: LocalizationgetNSBundle];
//    }
//    return mainStoryboard;
//}

+ (GlobalBill *)globalBill:(NSDictionary *)data{
    GlobalBill *globalBill = [GlobalBill sharedInstance];
    globalBill.billList = [NSMutableDictionary dictionaryMutableWithObject:data];
    globalBill.indexSplitBill = [NSString string];
    return globalBill;
}

+(void)selectBill:(id)_self andData:(NSDictionary *)data andDataTable:(NSDictionary *)dataTable fromRole:(NSString *)role{
    GlobalBill *_globalBill = [SelectBill globalBill:data];
    
    [LoadMenuAll retrieveData:@"" success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSArray *dataListTables = [NSArray arrayWithArray:dataTables];
            NSString *nameTable = @"[ ";
            
            if (isStaffTake || isCashierTake) {
                nameTable = [ NSString stringWithFormat:@" Q %@",data[@"no"]];
            }
            else{
                NSDictionary *dataUsage = [NSDictionary dictionaryWithObject:data[@"usage"]];
                
                if ([dataUsage count] == 1) {
                    nameTable = [NSString stringWithFormat:@" %@ ",dataTable[@"label"]];
                }
                else{
                    for (NSDictionary *dic in [dataUsage allValues]) {
                        NSDictionary *filteredArray = (NSDictionary *)[NSDictionary searchDictionary:dic[@"table_id"]
                                                                                            andvalue:@"(id = %@)"
                                                                                             andData:dataListTables];
                        nameTable = [NSString stringWithFormat:@"%@ %@ ,",nameTable, filteredArray[@"label"]];
                    }
                    nameTable = [nameTable substringToIndex:[nameTable length]-1];
                    nameTable = [nameTable stringByAppendingString:@" ]"];
                }
            }
     
            if ([_globalBill.billType  isEqual: kSingleBill] && !iscashTableStatus && !isCashierTake) {
                TableSingleBillViewController *tableSingleBill;
                if([role isEqualToString:@"admin"]){
                    tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSubBill"];
                    [tableSingleBill setRole:@"admin"];
                }
                else{
                    tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSingleBill"];
                }
                
                [tableSingleBill setTitleTable:[NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ", nil),nameTable]];
                [tableSingleBill setDataListSingBill:data];
                [tableSingleBill setNumberTable:dataTable[@"label"]];
                [tableSingleBill setDetailTable:dataTable];
                [tableSingleBill setCheckOpenTable:!checkOpenTable];
                [tableSingleBill setTableSuperClass:_self];
                [tableSingleBill setIsCashier:isCashier];
                [tableSingleBill setIsCashTableStatus:iscashTableStatus];
                [tableSingleBill setIsCashierTake:isCashierTake];
                [tableSingleBill setIsStaffTake:isStaffTake];
                
                
                if ([_self isKindOfClass:[UINavigationController class]]){
                    [_self setDataSingleBillViewController:tableSingleBill];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                         [_self pushViewController:tableSingleBill animated:YES];
                    });
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                         [[_self navigationController] pushViewController:tableSingleBill animated:YES];
                    });
                }
                
                
                NSLog(@"\n selectBill");
            }
            else{
                TableSingleBillViewController *tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSubBill"];
                [tableSingleBill setTitleTable:[NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ", nil),nameTable]];
                [tableSingleBill setDataListSingBill:data];
                [tableSingleBill setNumberTable:dataTable[@"label"]];
                [tableSingleBill setDetailTable:dataTable];
                [tableSingleBill setCheckOpenTable:!checkOpenTable];//no
                [tableSingleBill setTableSuperClass:_self];
                [tableSingleBill setIsCashier:isCashier];//no
                [tableSingleBill setIsCashTableStatus:iscashTableStatus];//yes
                [tableSingleBill setIsCashierTake:isCashierTake];//no
                [tableSingleBill setIsStaffTake:isStaffTake];//no
                
                if([role isEqualToString:@"admin"]){
                    [tableSingleBill setRole:@"admin"];
                }
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    [[_self navigationController] pushViewController:tableSingleBill animated:YES];
                });
                
            }
        });
    }];
}

@end
