//
//  DeviceData.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "DeviceData.h"
#include "LoadProcess.h"

@implementation DeviceData
static DeviceData * instantce;
+(DeviceData *)getInstance
{
    @synchronized(self)
    {
        if(instantce == nil)
        {
            instantce = [DeviceData new];
        }
    }
    return instantce;
}

- (void) createDb
{
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.databasePath = dirPaths[0];
    
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent:@"device_info.db"]];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databaseFile ] == NO)
    {
        const char *dbpath = [databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_db) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS device (data TEXT, date TEXT); INSERT INTO device VALUES ('[]','0000-00-00');";
            
            if (sqlite3_exec(_db, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create device table");
            }
            sqlite3_close(_db);
        }
        else {
            NSLog(@"Failed to open/create database device");
        }
    }
}

-(void) updateDb:(NSDictionary*)data{
    [self createDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"device_info.db"]];
    //    sqlite3_stmt *statement;
    const char *dbpath = [databaseFile UTF8String];
    if (sqlite3_open(dbpath, &_db) == SQLITE_OK)
    {
        //prepare data
        NSError *error = nil;
        NSString *dataString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:data
                                                                                            options:NSJSONWritingPrettyPrinted
                                                                                              error:&error]
                                                   encoding:NSUTF8StringEncoding];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
        [dateFormatter setTimeZone:timeZone];
        NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
        
        static sqlite3_stmt *updateStmt = nil;
        if(updateStmt == nil)
        {
            NSString *updateSQL = @"UPDATE device SET data = ? , date = ?";
            if(sqlite3_prepare_v2(_db, [updateSQL UTF8String], -1, &updateStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_db));
        }
        
        sqlite3_bind_text(updateStmt, 1, [dataString UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(updateStmt, 2, [dateString UTF8String], -1, SQLITE_TRANSIENT);
        if(SQLITE_DONE != sqlite3_step(updateStmt))
            NSAssert1(0, @"Error while update device data. '%s'", sqlite3_errmsg(_db));
        
        sqlite3_reset(updateStmt);
        updateStmt = nil;
        
        //        sqlite3_finalize(statement);
        sqlite3_close(_db);
        
    }
}

- (void) loadData
{
    [self createDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent:@"device_info.db"]];
    const char *dbpath = [databaseFile UTF8String];
    sqlite3_stmt *statement;
    
    BOOL isEmpty = YES;
    if (sqlite3_open(dbpath, &_db) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM device LIMIT 1 "];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_db, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *dataString = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                
                NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                if([dict count] > 0){
                    isEmpty = NO;
                    _resturant   = dict[@"resturant"];
                    _staff       = dict[@"staff"];
                    _menu        = dict[@"menu"];
                    _tables      = dict[@"tables"];
                    _goods       = dict[@"good"];
                    _vendor      = dict[@"vendor"];
                    _staffList   = dict[@"staff_list"];
                    _url_conn    = dict[@"conn_url"];
                    _outchecker = dict[@"menu"][@"outchecker"];
                }
            }
            else {
                NSLog(@"device not found");
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_db);
    }
    
    if(isEmpty){
        Member *member = [Member getInstance];
        NSDictionary *parameter = @{@"code":member.code};
        NSString *url = [NSString stringWithFormat:LOAD_DATA,member.nre];
        [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
            _resturant   = json[@"data"][@"resturant"];
            _staff       = json[@"data"][@"staff"];
            _menu        = json[@"data"][@"menu"];
            _tables      = json[@"data"][@"tables"];
            _goods       = json[@"data"][@"good"];
            _vendor      = json[@"data"][@"vendor"];
            _staffList   = json[@"data"][@"staff_list"];
            _url_conn    = json[@"conn_url"];
            _outchecker = json[@"outchecker"];
            
            [LoadMenuAll sortIndexMenu:_menu withKey:@"0"];
            [self updateDb:json[@"data"]];
            [LoadProcess dismissLoad];
        }];
    }
}

@end
