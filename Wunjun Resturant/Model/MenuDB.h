//
//  MenuDB.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/11/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface MenuDB : NSObject

@property (nonatomic, strong) NSString *key;   //id
@property (nonatomic, strong) NSString *valueMenu; //  ข้อมูลเมนู
@property (nonatomic, strong) NSString *date;  // วันที่

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *memberDb;

+(MenuDB *)getInstance;
- (void) createLoginDb;
- (void) loadData:(NSString *)key;
- (void) insertLoginData : (NSString *) jsonData  and:(NSString *)key;
- (void) deleteLoginDb;
- (void) updateDb:(NSDictionary*)_update andId:(NSString*)_idUpdate;
- (BOOL) isKeyDB:(NSString *)key;
@end
