//
//  Time.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/6/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "Time.h"

@implementation Time

-(NSString *)getTimeStamp
{
    return [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
}

+(NSString *) getTime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}
+(NSString *) getTimeALL{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}
+(NSString *) getTimeandDate{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    return dateString;
}
+(NSString *) conventTime:(NSString *) time{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:time];
    [dateFormat setDateFormat:@"HH:mm"];
    if (date == nil){
        return @"00:00";
    }
    return [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:date]];
}
+(NSString *) getDateFromDatetime:(NSString *) time{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:time];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    if (date == nil){
        return @"0000-00-00";
    }
    return [NSString stringWithFormat:@"%@",[dateFormat stringFromDate:date]];
}
+(NSDateComponents *)getTimeDMY:(NSString *)timeStamp
{
#ifdef __IPHONE_8_0
    #define GregorianCalendar1 NSCalendarIdentifierGregorian
#else
    #define GregorianCalendar1 NSGregorianCalendar
#endif
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:@"2011-10-05"];
    NSLog(@"date: %@", date);
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSDayCalendarUnit fromDate:date];
    NSInteger day     = [dateComponents day];
    NSLog(@"day: %ld", (long)day);
    
    
    NSCalendar *calendarTime = [[NSCalendar alloc] initWithCalendarIdentifier:GregorianCalendar1];
    NSDateComponents  *components = [calendarTime components:NSYearCalendarUnit|
                                         NSMonthCalendarUnit|
                                         NSDayCalendarUnit
                                                        fromDate:date];
    return components;
}
+(NSString *)getTimeDMY
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSDate* now = [dateFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components;
    components = [calendar components:NSYearCalendarUnit|
                   NSMonthCalendarUnit|
                   NSDayCalendarUnit|
                   NSMinuteCalendarUnit|
                   NSHourCalendarUnit
                              fromDate:now];
    return [NSString  stringWithFormat:@"  %d / %ld / %d  ",(int)components.day,(long)components.month,(int)components.year];
}
+(NSString *)getICTFormateDate:(NSString *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *localDate = [dateFormatter dateFromString:time];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    return dateString;
}
+(NSString*)toStringFromDateTime:(NSDate*)datetime
{
    // Purpose: Return a string of the specified date-time in UTC (Zulu) time zone in ISO 8601 format.
    // Example: 2013-10-25T06:59:43.431Z
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"ICT"]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:SS"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:datetime];
    return dateTimeInIsoFormatForZuluTimeZone;
}
+(NSString *)getDateFromDate:(NSDate*)datetime{
//    NSDate *date = [time date];
    NSTimeZone *tz = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:tz];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:datetime];
    NSString *tzAbbreviation = [tz abbreviationForDate:datetime];
    NSString *output = [NSString stringWithFormat:@"%@ %@", dateString, tzAbbreviation];
    return output;
}
+(NSString *)covnetTimeDMY
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *dateString = [dateFormatter stringFromDate:[NSDate date]];
    NSDate* now = [dateFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|
                   NSMonthCalendarUnit|
                   NSDayCalendarUnit|
                   NSMinuteCalendarUnit|
                   NSHourCalendarUnit
                              fromDate:now];
    
    NSInteger num = [components month];
    NSString *monthName = [Time getMountTH:num];
    return [NSString  stringWithFormat:@"  %d %@ %d  ",(int)components.day,monthName,((int)components.year + 543)];
}
+(NSString *)covnetTimeDMY1:(NSString *)timeStamp
{
    NSArray *myComponents = [timeStamp componentsSeparatedByString:@"-"];
    NSString *monthName = [Time getMountTH:[myComponents[1] intValue]];
    return [NSString  stringWithFormat:@"  %@ %@ %@  ",myComponents[2],monthName,myComponents[0]];
}
+(NSString *)getTHFromDate:(NSString *)time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *dateString = time;
    NSDate* now = [dateFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components;
    components = [calendar components:NSYearCalendarUnit|
                  NSMonthCalendarUnit|
                  NSDayCalendarUnit|
                  NSMinuteCalendarUnit|
                  NSHourCalendarUnit
                             fromDate:now];
    long fixYear = (long)components.year;
    if(fixYear < 1500)
        fixYear+=543;
    if(fixYear < 2500)
        fixYear+=543;
    return [NSString  stringWithFormat:@"%d %@ %ld",(int)components.day,[Time getMountTH:(int)components.month],fixYear];
}

+(NSString *)getTHTimeFromDate:(NSString *)time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *dateString = time;
    NSDate* now = [dateFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components;
    components = [calendar components:NSYearCalendarUnit|
                  NSMonthCalendarUnit|
                  NSDayCalendarUnit|
                  NSMinuteCalendarUnit|
                  NSHourCalendarUnit
                             fromDate:now];
    
    return [NSString  stringWithFormat:@"%02d:%02d",(int)components.hour,(int)components.minute];
}

+(NSString *)getTHFullFromDate:(NSString *)time{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"Asia/Bangkok"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *dateString = time;
    NSDate* now = [dateFormatter dateFromString:dateString];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components;
    components = [calendar components:NSYearCalendarUnit|
                  NSMonthCalendarUnit|
                  NSDayCalendarUnit|
                  NSMinuteCalendarUnit|
                  NSHourCalendarUnit
                             fromDate:now];
    long fixYear = (long)components.year;
    if(fixYear < 1500)
        fixYear+=543;
    if(fixYear < 2500)
        fixYear+=543;
    return [NSString  stringWithFormat:@"%d %@ %ld %02d:%02d",(int)components.day,[Time getMountTH:(int)components.month],fixYear,(int)components.hour,(int)components.minute];
}

#pragma mark - sort Array

+(NSMutableArray *) sortDateDescriptors:(NSMutableArray *)data{
//    NSLog(@"%@",data);
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_datetime"
                                                                 ascending:YES];
    [data sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [data sortUsingComparator:^(id dict1, id dict2) {
        NSDate *date1 = dict1[@"created_datetime"];     // create NSDate from dict1's Date;
        NSDate *date2 = dict2[@"created_datetime"];     // create NSDate from dict2's Date;
        NSLog(@"date1 : %@ date 2 : %@",date1,date2);
        return [date1 compare:date2];
    }];
    for (NSDictionary *dic in data) {
        NSDate *date2 = dic[@"created_datetime"];       // create NSDate from dict2's Date;
        NSLog(@"date : %@ ",date2);
    }
    return data;
}
+(NSMutableArray *) sortDateAscending:(NSMutableArray *)data{
    NSSortDescriptor *descriptor1 = [[NSSortDescriptor alloc] initWithKey:@"created_datetime" ascending:NO];
    NSArray *descriptors1 = [NSArray arrayWithObject:descriptor1];
    NSArray *reverseOrder = [data sortedArrayUsingDescriptors:descriptors1];
    for (NSDictionary *dic in reverseOrder) {
        NSDate *date2 = dic[@"created_datetime"];   // create NSDate from dict2's Date;
        NSLog(@"date : %@ ",date2);
    }
    return [NSMutableArray  arrayWithArray:reverseOrder];
}
+(NSMutableArray *) sortDateDescriptorsDeliverDatetime:(NSMutableArray *)data{
//    NSLog(@"%@",data);
    NSSortDescriptor *descriptor1 = [[NSSortDescriptor alloc] initWithKey:@"deliver_datetime" ascending:NO];
    NSArray *descriptors1 = [NSArray arrayWithObject:descriptor1];
    NSArray *reverseOrder = [data sortedArrayUsingDescriptors:descriptors1];
    
    for (NSDictionary *dic in reverseOrder) {
        NSDate *date2 = dic[@"deliver_datetime"];       // create NSDate from dict2's Date;
        NSLog(@"date : %@ ",date2);
    }
    
    return [NSMutableArray arrayWithArray:reverseOrder];
}
+(NSString*)getMountTH:(NSInteger )numberMonth
{
    switch (numberMonth) {
        case 1:return AMLocalizedString(@"ม.ค.", nil);break;
        case 2:return AMLocalizedString(@"ก.พ.", nil);break;
        case 3:return AMLocalizedString(@"มี.ค.", nil);break;
        case 4:return AMLocalizedString(@"เม.ย.", nil);break;
        case 5:return AMLocalizedString(@"พ.ค.", nil);break;
        case 6:return AMLocalizedString(@"มิ.ย.", nil);break;
        case 7:return AMLocalizedString(@"ก.ค.", nil);break;
        case 8:return AMLocalizedString(@"ส.ค.", nil);break;
        case 9:return AMLocalizedString(@"ก.ย.", nil);break;
        case 10:return AMLocalizedString(@"ต.ค.", nil);break;
        case 11:return AMLocalizedString(@"พ.ย.", nil);break;
        default:
            return AMLocalizedString(@"ธ.ค.", nil);
            break;
    }
}
@end
