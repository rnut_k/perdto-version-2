//
//  GlobalSocket.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/31/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SRWebSocket.h"
#import "Constants.h"
#import "Member.h"
#import "AppDelegate.h"

@interface GlobalSocket : NSObject

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readwrite) SRWebSocket *webSocket;
@property (strong, nonatomic, readwrite) NSString *status;

+ (GlobalSocket *)sharedInstance;
- (void)connectWebSocket;
-(void)close ;
@end
