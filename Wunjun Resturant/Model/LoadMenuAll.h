//
//  LoadMenuAll.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+DictionaryWithString.h"
#import "LocalizationSystem.h"
#import "UIStoryboard+Main.h"

@interface LoadMenuAll : NSObject

+(void)checkDataMenu:(NSString *)key;
+(void)sortIndexMenu:(NSDictionary*)dict withKey:(NSString*)key;
+(void)loadData:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion;
+(void)loadDataImage:(NSData *)imageData  andParameter:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion;
+(NSArray *) requirebillID:(NSString *)idTable;
+(void)retrieveData:(NSString *)_id  success:(void (^)(NSArray * dictionary))completion;
+(void)loadDataOpenTable:(NSString *)idTable  success:(void (^)(NSDictionary * dictionary))completion;
+(void)loadData:(NSDictionary *)parameters andURL:(NSString *)url success:(void (^)(NSDictionary * json))completion error:(void (^)(NSDictionary * json))_error;
+(void)logoutView:(NSDictionary *)data;
@end
