//
//  InvoiceDB.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/8/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface InvoiceDB : NSObject

@property (nonatomic, strong) NSString *key;            //  idInvoice
@property (nonatomic, strong) NSString *valueInvoice;   //  ข้อมูลใบสั่งซื้อ
//@property (nonatomic, strong) NSString *date;           //  วันที่


@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *invoiceDB;

+(InvoiceDB *)getInstance;
- (void) createLoginDb;
- (void) loadData:(NSString *)key;
- (void) insertLoginData : (NSString *) jsonData  and:(NSString *)key;
- (void) deleteLoginDb:(NSString *)keyDelete;
-(void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate;
- (BOOL) isKeyDB:(NSString *)key;

@end
