//
//  Member.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

@interface Member : NSObject

@property (nonatomic, strong, readwrite) NSString *id;      //id
@property (nonatomic, strong, readwrite) NSString *shopID;  // role list
@property (nonatomic, strong, readwrite) NSString *nre;     // ชื่อ ร้าน
@property (nonatomic, strong, readwrite) NSString *ln;      //นามแฝง
@property (nonatomic, strong, readwrite) NSString *n;       //ชื่อ
@property (nonatomic, strong, readwrite) NSString *nck;     //นามสกุล
@property (nonatomic, strong, readwrite) NSString *un;      //username
@property (nonatomic, strong, readwrite) NSString *code;
@property (nonatomic, strong, readwrite) NSString *titleRes;
@property (nonatomic, strong, readwrite) NSString *address;
@property (nonatomic, strong, readwrite) NSString *usbill;  // use_sub_bill
@property (nonatomic, strong, readwrite) NSString *isall;   // is_sign_all
@property (nonatomic, strong, readwrite) NSString *role;    // role list
@property (nonatomic, strong, readwrite) NSString *girlID;  // girl_id
@property (nonatomic, strong, readwrite) NSString *UDID;    // UDID
@property (nonatomic, strong, readwrite) NSString *lg;      // language
@property (nonatomic, strong, readwrite) NSString *ss;      // sound
@property (nonatomic, strong, readwrite) NSString *editOr;  // staff แก้ราคาตอนสั่ง
@property (nonatomic, strong, readwrite) NSString *cac;     // view เช็คบิลเงินสดและเครดิต  1  = ได้ 2 เครดิตและเงินสดพร้อมกัน
                                                            // 0 = ได้อย่างใดอย่างหนึ่ง

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *memberDb;

+(Member *)getInstance;
- (void) createLoginDb;
- (BOOL) isUserLogin;
- (void) loadData;
- (void) insertLoginData : (NSData *) json;
- (void) deleteLoginDb;
- (void) updateDb:(NSDictionary*)_update andId:(NSString*)_idUpdate;
-(void) updateDbLanguage:(NSString*)lag;
@end
