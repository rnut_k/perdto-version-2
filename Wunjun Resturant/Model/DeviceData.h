//
//  DeviceData.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "LoadProcess.h"
#import "Member.h"

@interface DeviceData : NSObject
@property(nonatomic,retain) NSString *token;
@property(nonatomic,retain) NSDictionary *resturant;
@property(nonatomic,retain) NSDictionary *staff;
@property(nonatomic,retain) NSDictionary *tables;
@property(nonatomic,retain) NSDictionary *menu;
@property(nonatomic,retain) NSDictionary *goods;
@property(nonatomic,retain) NSArray *vendor;
@property(nonatomic,retain) NSDictionary *staffList;
@property(nonatomic,retain) NSString *url_conn;
@property(nonatomic,retain) NSArray *outchecker;

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *db;
+(DeviceData *)getInstance;

- (void) createDb;
-(void) updateDb:(NSDictionary*)data;
-(void) loadData;

@end
