//
//  GlobalSocket.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/31/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "GlobalSocket.h"

@interface GlobalSocket ()<SRWebSocketDelegate>
{
    Member *member;
}
@end

@implementation GlobalSocket

@synthesize webSocket = _webSocket;
@synthesize status = _status;

+ (GlobalSocket *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalSocket *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalSocket alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _webSocket = nil;
        _status = nil;
    }
    return self;
}

#pragma mark - Connection
//web socket
- (void)connectWebSocket {
    if( self.webSocket ) {
        [self.webSocket close];
        self.webSocket.delegate = nil;
        self.webSocket = nil;
    }
    
    DeviceData *dData = [DeviceData getInstance];
    [dData loadData];
    // NSString *urlString = NOTI_URL;
    NSString *url2 = dData.url_conn;
    url2 = (@"%@/",url2 );
    self.webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:url2]];
    
}

#pragma mark - SRWebSocket delegate

- (void)webSocketDidOpen:(SRWebSocket *)newWebSocket {
    self.webSocket = newWebSocket;
    [self.webSocket send:[NSString stringWithFormat:@"Hello from %@", [UIDevice currentDevice].name]];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
    [self connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    NSLog(@"SRWebSocket  :%@",message);
    //    self.messagesTextView.text = [NSString stringWithFormat:@"%@\n%@", self.messagesTextView.text, message];
}
-(void)close {
    
    if( !self.webSocket ){
        DDLogWarn(@"SRWebSocket  : close  fail");
        return;
    }
    
    [self.webSocket close];
    self.webSocket.delegate = nil;
    self.webSocket = nil;
    DDLogWarn(@"SRWebSocket  : close  comple");
}

@end
