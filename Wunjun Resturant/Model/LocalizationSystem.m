//
//  LocalizationSystem.m
//  Battle of Puppets
//
//  Created by Juan Albero Sanchis on 27/02/10.
//  Copyright Aggressive Mediocrity 2010. All rights reserved.
//

#import "LocalizationSystem.h"
#import <objc/runtime.h>
#import "Member.h"
#import "Check.h"


@implementation LocalizationSystem
@synthesize isSet = _isSet;

//Singleton instance
static LocalizationSystem *_sharedLocalSystem = nil;

//Current application bungle to get the languages.
static NSBundle *bundle = nil;

-(LoadProcess *)loadProcess{
    if (!_loadProcess) {
        _loadProcess = [LoadProcess sharedInstance];
    }
    return _loadProcess;
}
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
    }
    [_member loadData];
    return _member;
}

+ (LocalizationSystem *)sharedLocalSystem
{
	@synchronized([LocalizationSystem class])
	{
		if (!_sharedLocalSystem){
			_sharedLocalSystem = [[self alloc] init];
		}
		return _sharedLocalSystem;
	}
	// to avoid compiler warning
	return nil;
}

+(id)alloc
{
	@synchronized([LocalizationSystem class])
	{
		NSAssert(_sharedLocalSystem == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedLocalSystem = [super alloc];
		return _sharedLocalSystem;
	}
	// to avoid compiler warning
	return nil;
}


- (id)init
{
    if ((self = [super init])) 
    {
		//empty.
		bundle = [NSBundle mainBundle];
        _isSet = NO;
	}
    return self;
}

// Gets the current localized string as in AMLocalizedString.
//
// example calls:
// AMLocalizedString(@"Text to localize",@"Alternative text, in case hte other is not find");
//- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
//{
//	return [bundle localizedStringForKey:key value:comment table:nil];
//}
- (NSString *)localizedStringForKey:(NSString *)key value:(NSString *)comment
{
    NSString *path = nil;
    if (language != nil) {
        path = [[NSBundle mainBundle] pathForResource:language ofType:@"lproj"];
    }else{
        path = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"];
    }
    
    bundle = [NSBundle bundleWithPath:path];
    
    NSString * localizedString = [bundle localizedStringForKey:key value:key table:nil];
    
    // if (value == key) and comment is not nil -> returns comment
    if([localizedString isEqualToString:key] && comment != nil)
        return @"";
    
    return localizedString;
}

// Sets the desired language of the ones you have.
// example calls:
// LocalizationSetLanguage(@"Italian");
// LocalizationSetLanguage(@"German");
// LocalizationSetLanguage(@"Spanish");
// 
// If this function is not called it will use the default OS language.
// If the language does not exists y returns the default OS language.

- (void) setLanguage:(NSString*) l{
	DDLogInfo(@"setLanguage: %@", l);
    [self.member updateDbLanguage:l];
    
	NSString *path = [[NSBundle mainBundle] pathForResource:l ofType:@"lproj"];
    if (path == nil){
        DDLogInfo(@"no: %@", l);
		[self resetLocalization];
    }
    else{
        DDLogInfo(@"yes: %@", l);
		bundle = [NSBundle bundleWithPath:path];
    }
    [self swichtLanguage:l];
    language = l;
}
-(void) swichtLanguage:(NSString*) lang{
    DDLogInfo(@"swichtLanguage: %@", lang);
//    if (_isSet) {
////        [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObject:lang] forKey:@"AppleLanguages"];
//        AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        bundle = [self getNSBundle];
//        [appdelegate switchTolanguage:bundle];
//        _isSet = NO;
//    }
    //edit by nut
    if (_isSet) {
        AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        bundle = [self getNSBundle];
        [[NSUserDefaults standardUserDefaults] setObject:lang forKey:@"AppLanguage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [appdelegate switchTolanguage:bundle];
        _isSet = NO;
    }
    //--
}

// example call:
// NSString * currentL = LocalizationGetLanguage;
+ (NSString*) getLanguage{  // index = th = 28
    DDLogInfo(@"getLanguage:");
	NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];

	NSString *preferredLang = [languages objectAtIndex:0];
    NSRange matchTH;
    NSRange matchEN;
    matchTH = [preferredLang rangeOfString:@"th"];
    matchEN = [preferredLang rangeOfString:@"en"];
    if (matchTH.location != NSNotFound){
        NSLog (@"Match not found");
        return @"th";
    }
    else if (matchEN.location != NSNotFound){
        NSLog (@"Match not found");
        return @"en";
    }
    else{
        NSLog (@"match found at index %lu : %lu", (unsigned long)matchTH.location,(unsigned long)matchEN.location);
        return @"th";
    }
    
	return preferredLang;
}

// LocalizationReset;
- (void) resetLocalization
{
    DDLogInfo(@"resetLocalization:");
	bundle = [NSBundle mainBundle];
    [self swichtLanguage:[[self class] getLanguage]];
}

- (NSBundle*) getNSBundle{
    NSArray* languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *preferredLang = [languages objectAtIndex:0];
    NSString* lang = @"";
    
    if ([self.member.lg isEqualToString:preferredLang]) {
        bundle = [NSBundle mainBundle];
    }
    else{
        if ([self.member.lg  isEqualToString:@"th"]) {
            lang = @"th";
        }
        else{
            if ([Check checkNull:self.member.lg]) {
                lang = preferredLang; // login ครั้งแรก
            }
            else
                lang = self.member.lg;
        }
        
//        NSString *path = [[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"];
        
//        bundle = [NSBundle bundleWithPath:path];
        bundle = [NSBundle mainBundle];
    }
    return  bundle;
}
- (void)setIs:(BOOL)isSet{
    _isSet = isSet;
}
@end
