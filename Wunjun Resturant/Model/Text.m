//
//  Text.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/17/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "Text.h"

@implementation Text

+(NSAttributedString *)setColor:(NSString *)emblem and:(NSString *)text andColor:(UIColor *)color{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                         initWithString:[NSString stringWithFormat:
                                                         @"%@  %@ ",
                                                         emblem, text]];
    
    [string addAttribute:NSForegroundColorAttributeName
                   value:color
                   range:[string.string rangeOfString:emblem]];
    [string addAttribute:NSFontAttributeName
                   value:[UIFont systemFontOfSize:12.0]
                   range:NSMakeRange(0,string.length)];
    
    return string;
}

+(NSString *) numberFormatt:(float)number{
    NSNumber *num2 = [NSNumber numberWithDouble:number];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setNegativeFormat:@"-#,##0.00"];
    [numberFormatter setPositiveFormat:@"#,##0.00"];
    
    return [numberFormatter stringFromNumber:num2];
}
@end
