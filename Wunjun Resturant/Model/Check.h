//
//  Check.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Check : NSObject

+(BOOL)checkDevice;
+(BOOL)checkNull:(id )object;
+(id)checkObjectType:(id)object;
+ (NSString *)platformRawString ;
+(BOOL) checkVersioniOS8;
@end
