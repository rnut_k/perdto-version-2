//
//  OrdersKitchen.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/19/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "OrdersKitchen.h"
#import "NSString+GetString.h"

@implementation OrdersKitchen
static OrdersKitchen * instantce;
+(OrdersKitchen *)getInstance
{
    @synchronized(self)
    {
        if(instantce == nil)
        {
            instantce = [OrdersKitchen new];
        }
    }
    return instantce;
}

-(void) sortOrdersData
{
    NSDictionary *pending = [[[NSDictionary alloc]init]mutableCopy];
    NSDictionary *served = [[[NSDictionary alloc]init]mutableCopy];
    NSDictionary *cancelled = [[[NSDictionary alloc]init]mutableCopy];
    
    _fetchOrders = [[[NSDictionary alloc]init]mutableCopy];
    for (NSString* k in _orders) {
        
        NSString *deliverStatus = [NSString checkNull:_orders[k][@"deliver_status"]];
        NSString *is_enabled = [NSString checkNull:_orders[k][@"is_enabled"]];
        
        if([is_enabled isEqualToString:@"0"]){
            continue;
        }
        if([deliverStatus isEqualToString:@"1"] || [deliverStatus isEqualToString:@"2"])
        {
            [pending setValue:_orders[k] forKey:k];
        }
        else if([deliverStatus isEqualToString:@"3"] || [deliverStatus isEqualToString:@"5"])
        {
            [served setValue:_orders[k] forKey:k];
        }
        else if([deliverStatus isEqualToString:@"4"])
        {
            [cancelled setValue:_orders[k] forKey:k];
        }
    }
    [_fetchOrders setValue:_orders forKey:@"1"];
    [_fetchOrders setValue:pending forKey:@"0"];
//    [_fetchOrders setValue:served forKey:@"3"];
//    [_fetchOrders setValue:cancelled forKey:@"4"];
}

-(void)reSet{
    instantce = [OrdersKitchen new];
}
@end
