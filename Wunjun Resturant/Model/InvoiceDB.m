//
//  InvoiceDB.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/8/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "InvoiceDB.h"
#import "Time.h"
#import "AFNetworking.h"
#import "Constants.h"

@implementation InvoiceDB{
    bool databaseIsUsed ;
    //    Member *member;
}

+(InvoiceDB *)getInstance
{
    static dispatch_once_t onceToken;
    static InvoiceDB *instance = nil; 
    dispatch_once(&onceToken, ^{
        instance = [[InvoiceDB alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void) loadData:(NSString *)key
{
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"invoice_info.db"]];
    const char *dbpath = [databaseFile UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM invoice_info WHERE key = '%@' LIMIT 1 ",key];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_invoiceDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                _key            = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                _valueInvoice  = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
//                _date           = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
            }
            else {
                NSLog(@"Match not found");
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_invoiceDB);
    }
}
- (BOOL) isKeyDB:(NSString *)key
{
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"invoice_info.db"]];
    
    NSLog(@" databaseFile :%@", databaseFile);
    
    const char *dbpath = [databaseFile UTF8String];
    sqlite3_stmt *statement;
    BOOL result = NO;
    if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM invoice_info WHERE key = '%@' LIMIT 1 ",key];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_invoiceDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = YES;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_invoiceDB);
    }
    
    return result;
}
- (void) createLoginDb
{
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.databasePath = dirPaths[0];
    
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent:@"invoice_info.db"]];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databaseFile ] == NO)
    {
        const char *dbpath = [databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS invoice_info (key TEXT , valueInvoice TEXT )";
            
            if (sqlite3_exec(_invoiceDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            sqlite3_close(_invoiceDB);
        }
        else {
            NSLog(@"Failed to open/create database");
        }
    }
}
- (void) insertLoginData : (NSString *) jsonData  and:(NSString *)key
{
    _key            = key;
    _valueInvoice  = jsonData;
//    _date           = [Time getTimeALL];
    
    if (_key != nil) {
        [self createLoginDb];
        NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"invoice_info.db"]];
        sqlite3_stmt *statement;
        const char *dbpath = [databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
        {
            //            char *errMsg;
            //            const char *sql = "DELETE FROM menu_info";
            //            sqlite3_exec(_memberDb, sql, NULL, NULL, &errMsg);
            //            NSString *insertSQL = [NSString stringWithFormat:
            //                                   @"INSERT INTO menu_info(key, valueMenu,date) VALUES('%@','%@','%@')",_key,_valueMenu,_date];
            //
            //            const char *insert_stmt = [insertSQL UTF8String];
            //            sqlite3_prepare_v2(_memberDb, insert_stmt, -1, &statement, NULL);
            //            if (sqlite3_step(statement) != SQLITE_DONE)
            //            {
            //                NSLog(@"insert error");
            //            }
            
            const char *sql = [[NSString stringWithFormat:@"DELETE FROM invoice_info WHERE key = '%@'", _key] UTF8String];
            sqlite3_prepare_v2(_invoiceDB, sql, -1, &statement, NULL);
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                NSLog(@"delete error");
            }
            
            static sqlite3_stmt *insertStmt = nil;
            
            if(insertStmt == nil)
            {
                NSString *insertSql = @"INSERT INTO invoice_info(key,valueInvoice) VALUES(?,?)";
                if(sqlite3_prepare_v2(_invoiceDB, [insertSql UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
                    NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_invoiceDB));
            }
            
            sqlite3_bind_text(insertStmt, 1, [_key UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 2, [_valueInvoice UTF8String], -1, SQLITE_TRANSIENT);
//            sqlite3_bind_text(insertStmt, 3, [_date UTF8String], -1, SQLITE_TRANSIENT);
            if(SQLITE_DONE != sqlite3_step(insertStmt))
                NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(_invoiceDB));
            else
                NSLog(@"Inserted");
                //Reset the add statement.
                
            sqlite3_reset(insertStmt);
            insertStmt = nil;
            
            sqlite3_finalize(statement);
            sqlite3_close(_invoiceDB);
        }
    }
}
- (void) deleteLoginDb:(NSString *)keyDelete
{
    if (keyDelete != nil) {
        [self createLoginDb];
        NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"invoice_info.db"]];
        sqlite3_stmt *statement;
        const char *dbpath = [databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
        {
            const char *sql = [[NSString stringWithFormat:@"DELETE FROM invoice_info WHERE key = '%@'", _key] UTF8String];
            sqlite3_prepare_v2(_invoiceDB, sql, -1, &statement, NULL);
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                NSLog(@"delete error");
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_invoiceDB);
    }    
}
-(void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate{
    NSString *keyUpdate = _idUpdate;
    NSString *val       = jsonObject;
//    NSString *dateVal   = jsonObject[@"date"];
    
    if (databaseIsUsed) {
        [self updateDb:jsonObject andId:_idUpdate];
        return;
    }
    databaseIsUsed = TRUE;
    
    sqlite3_close(_invoiceDB);
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent:@"invoice_info.db"]];
    //    sqlite3_stmt *statement;
    const char *dbpath = [databaseFile UTF8String];
    if (sqlite3_open(dbpath, &_invoiceDB) == SQLITE_OK)
    {
        static sqlite3_stmt *insertStmt = nil;
        if(insertStmt == nil)
        {
            NSString *updateSQL = @"UPDATE invoice_info SET valueInvoice = ? WHERE key = ?";
            if(sqlite3_prepare_v2(_invoiceDB, [updateSQL UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_invoiceDB));
        }
        
        sqlite3_bind_text(insertStmt, 1, [val UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, [keyUpdate UTF8String], -1, SQLITE_TRANSIENT);
///       sqlite3_bind_text(insertStmt, 2, [dateVal UTF8String], -1, SQLITE_TRANSIENT);
        if(SQLITE_DONE != sqlite3_step(insertStmt))
            NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(_invoiceDB));
        
        //        for (id key in [jsonObject allKeys]) {
        //            NSString *str =  [jsonObject  objectForKey:key];
        //            updateSQL = [NSString stringWithFormat:@"%@ %@ ='%s',",updateSQL,key,[str UTF8String]];
        //        }
        //        updateSQL = [updateSQL substringToIndex:updateSQL.length-1];
        //        updateSQL = [NSString stringWithFormat:@"%@ WHERE key ='%@'",updateSQL,_idUpdate];
        //
        sqlite3_reset(insertStmt);
        insertStmt = nil;
        
        //        sqlite3_finalize(statement);
        sqlite3_close(_invoiceDB);
        
    }
    databaseIsUsed = FALSE;
}
@end

