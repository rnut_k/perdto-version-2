//
//  MainInterface.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainInterface : NSObject

@property(nonatomic,retain) NSString *token;

@property(nonatomic) BOOL isCustomer;
@property(nonatomic) BOOL isMain;

+(MainInterface *)sharedInstance;

@end
