//
//  UIColor+Conventcolor.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/12/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Conventcolor)
+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str ;
@end
