//
//  UIStoryboard+Main.m
//  Wunjun Resturant
//
//  Created by AgeNt on 10/9/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "UIStoryboard+Main.h"

@implementation UIStoryboard (Main)
static UIStoryboard *_sharedLocalSystem = nil;

+ (UIStoryboard *)sharedLocalSystem
{
    @synchronized([UIStoryboard class])
    {
        if (!_sharedLocalSystem){
            _sharedLocalSystem = [[self alloc] init];
        }
        return _sharedLocalSystem;
    }
    // to avoid compiler warning
    return nil;
}

-(UIStoryboard *)getStoryboardMain{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
    return storyboard;
}
@end
