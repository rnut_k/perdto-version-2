//
//  Member.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "Member.h"
#import "Check.h"
#import "LocalizationSystem.h"
#import "NSString+GetString.h"

@implementation Member{
    bool databaseIsUsed ;
}

//@property (nonatomic, strong, readwrite) NSString *id;  //id
//@property (nonatomic, strong, readwrite) NSString *nre; // ชื่อ ร้าน
//@property (nonatomic, strong, readwrite) NSString *al;  //นามแฝง
//@property (nonatomic, strong, readwrite) NSString *n;   //ชื่อ
//@property (nonatomic, strong, readwrite) NSString *nck; //นามสกุล
//@property (nonatomic, strong, readwrite) NSString *un;  //username
//@property (nonatomic, strong, readwrite) NSString *code;

+(Member *)getInstance
{
    static dispatch_once_t onceToken;
    static Member *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[Member alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (void) loadData
{
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
    const char *dbpath = [databaseFile UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
    {
        NSString *querySQL = @"SELECT * FROM member_info LIMIT 1 ";
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_memberDb, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                _id         = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                _nre        = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                _ln         = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                _n          = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 4)];
                _nck        = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 5)];
                _un         = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 6)];
                _code       = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 7)];
                _titleRes   = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 8)];
                _address    = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 9)];
                _usbill     = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 10)];
                _isall      = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 11)];
                _role       = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 12)];
                _shopID     = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 13)];
                _girlID     = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 14)];
                _UDID       = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 15)];
                _lg         = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 16)];
                _ss         = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 17)];
                _editOr     = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 18)];
                _cac        = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 19)];
            }
            else {
                NSLog(@"Match not found");
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_memberDb);
    }
}
- (BOOL) isUserLogin
{
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
    
    const char *dbpath = [databaseFile UTF8String];
    sqlite3_stmt *statement;
    BOOL result = NO;
    if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
    {
        NSString *querySQL = @"SELECT id FROM member_info LIMIT 1 ";
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_memberDb, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = YES;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_memberDb);
    }
    
    return result;
}
- (void) createLoginDb
{
    //NSString *docsDir;
    NSArray * dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.databasePath = dirPaths[0];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent:@"member_info.db"]];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databaseFile ] == NO)
    {
        const char *dbpath = [databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS member_info (id integer primary key autoincrement, _id TEXT , nre TEXT , ln TEXT, n TEXT, nck TEXT, un TEXT, code TEXT ,title TEXT ,address TEXT,usbill TEXT,isall TEXT,role TEXT,shopID TEXT,girlID TEXT,UDID TEXT ,lg TEXT ,ss TEXT ,editOr TEXT,cac TEXT)";
            
            if (sqlite3_exec(_memberDb, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            sqlite3_close(_memberDb);
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
}
- (void) insertLoginData : (NSData *) loginData
{
    //parse out the json data
    NSError* error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:loginData
                                                         options:kNilOptions
                                                           error:&error];
    
    NSDictionary *data = [[NSDictionary alloc]init];
    NSDictionary *dataRes = [[NSDictionary alloc]init];
    NSDictionary *config = [NSDictionary dictionary];
    
    if(![[json objectForKey:@"data"] isKindOfClass:[NSString class]]){
        data = [json objectForKey:@"data"];
        dataRes = [json objectForKey:@"resturant"];
    }
    else{
        NSString *strData = [json objectForKey:@"data"];
        NSData *jsonData = [strData dataUsingEncoding:NSUTF8StringEncoding];
        data = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    }
    
    if([[dataRes objectForKey:@"config"] isKindOfClass:[NSString class]]){
        NSString *strData = [dataRes objectForKey:@"config"];
        NSData *jsonData = [strData dataUsingEncoding:NSUTF8StringEncoding];
        config = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    }

    _id       = [data objectForKey:@"id"];
    _shopID   = [data objectForKey:@"shop_id"];
    _nre      = [dataRes objectForKey:@"name"];
    _ln       = [data objectForKey:@"lastname"];
    _n        = [data objectForKey:@"name"];
    _nck      = [data objectForKey:@"nickname"];
    _un       = [data objectForKey:@"username"];
    _code     = [data objectForKey:@"code"];
    _titleRes = [dataRes objectForKey:@"title"];
    _address  = [dataRes objectForKey:@"address"];
    _usbill   = [dataRes objectForKey:@"use_sub_bill"];
    _isall    = [dataRes objectForKey:@"is_sign_all"];
    _role     = [data objectForKey:@"role"];
    _girlID   = [data objectForKey:@"girl_id"];
    _UDID     = [self getUDID];
    _editOr   = [NSString  checkNull:config[@"allow_edit"]];
    _cac      = [NSString  checkNull:config[@"is_multi_pay"]];
    [self getLanguage];
    
    if([Check checkNull:_ss])
        _ss = @"sounds-chime.mp3";
    
    if (_id != nil) {
        [self createLoginDb];
        sqlite3_close(_memberDb);
        NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
        
        const char *dbpath = [databaseFile UTF8String];
        if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
        {
            
            static sqlite3_stmt *insertStmt = nil;
            if(insertStmt == nil)
            {
                NSString *insertSql = @"INSERT INTO member_info(id, _id, nre, ln, n, nck, un, code, title, address, usbill, isall, role , shopID, girlID,UDID,lg,ss,editOr,cac) VALUES(0,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                if(sqlite3_prepare_v2(_memberDb, [insertSql UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
                    NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_memberDb));
            }
            
            sqlite3_bind_text(insertStmt, 1, [_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 2, [_nre UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 3, [_ln UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 4, [_n UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 5, [_nck UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 6, [_un UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 7, [_code UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 8, [_titleRes UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 9, [_address UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 10, [_usbill UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 11, [_isall UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 12, [_role UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 13, [_shopID UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 14, [_girlID UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 15, [_UDID UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 16, [_lg UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 17, [_ss UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 18, [_editOr UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(insertStmt, 19, [_cac UTF8String], -1, SQLITE_TRANSIENT);
            
            if(SQLITE_DONE != sqlite3_step(insertStmt)){
                sqlite3_close(_memberDb);
                NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
                
                if([[NSFileManager defaultManager] fileExistsAtPath:databaseFile]){
                    [[NSFileManager defaultManager] removeItemAtPath:databaseFile error:nil];
                }
            }else{
                sqlite3_reset(insertStmt);
                insertStmt = nil;
            }
            
            sqlite3_close(_memberDb);
        }
    }
}
- (void) deleteLoginDb
{
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
    sqlite3_stmt *statement;
    const char *dbpath = [databaseFile UTF8String];
    
    if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
    {
        NSString *insertSQL = @"DELETE FROM member_info";
        
        const char *insert_stmt = [insertSQL UTF8String];
        sqlite3_prepare_v2(_memberDb, insert_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"insert error");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(_memberDb);
    }
}
-(void) updateDb:(NSDictionary*)_update andId:(NSString*)_idUpdate{
//    NSLog(@"  databaseIsUsed = %d",(int)databaseIsUsed);
    if (databaseIsUsed) {
        [self updateDb:_update andId:_idUpdate];
        return;
    }
    databaseIsUsed = TRUE;
    
    sqlite3_close(_memberDb);
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
    sqlite3_stmt *statement;
    const char *dbpath = [databaseFile UTF8String];
    if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK)
    {
        NSString *updateSQL = [NSString stringWithFormat: @"UPDATE member_info SET"];
        
        for (id key in [_update allKeys]) {
            updateSQL = [NSString stringWithFormat:@"%@ %@='%@',",updateSQL,key,[_update  objectForKey:key]];
        }
        updateSQL = [updateSQL substringToIndex:updateSQL.length-1];
        updateSQL = [NSString stringWithFormat:@"%@ WHERE _id='%@'",updateSQL,_idUpdate];
        
        const char *insert_stmt = [updateSQL UTF8String];
        sqlite3_prepare_v2(_memberDb, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"update error %s" ,sqlite3_errmsg(_memberDb));
        }
        sqlite3_finalize(statement);
        sqlite3_close(_memberDb);
    }
    
    databaseIsUsed = FALSE;
}
-(void) updateDbLanguage:(NSString*)lag{
    
    if (databaseIsUsed) {
        [self updateDbLanguage:lag];
        return;
    }
    databaseIsUsed = TRUE;
    
    sqlite3_close(_memberDb);
    [self createLoginDb];
    NSString *databaseFile = [[NSString alloc] initWithString:[self.databasePath stringByAppendingPathComponent: @"member_info.db"]];
    sqlite3_stmt *statement;
    const char *dbpath = [databaseFile UTF8String];
    if (sqlite3_open(dbpath, &_memberDb) == SQLITE_OK){
            // update
            NSString *updateSQL = [NSString stringWithFormat: @"UPDATE member_info SET lg = '%@'",lag];
            const char *insert_stmt = [updateSQL UTF8String];
            sqlite3_prepare_v2(_memberDb, insert_stmt,-1, &statement, NULL);
            
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                NSLog(@"update error %s" ,sqlite3_errmsg(_memberDb));
            }
            sqlite3_finalize(statement);
//        }
        sqlite3_close(_memberDb);
    }
    databaseIsUsed = FALSE;
}
-(NSString *)getUDID{
    UIDevice *myDevice = [UIDevice currentDevice];
    NSString *UUID = [[myDevice identifierForVendor] UUIDString];
    return UUID;
}
-(void)getLanguage{
    if ([Check checkNull:_lg]) {
        _lg = [LocalizationSystem getLanguage];
    }
    LocalizationSetLanguage(_lg);
}
@end
