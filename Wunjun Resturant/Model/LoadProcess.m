//
//  LoadProcess.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "LoadProcess.h"

@implementation LoadProcess

@synthesize loadView = _loadView;
- (id)init {
    self = [super init];
    if (self) {
        _loadView = [[KVNViewController alloc] init];
    }
    return self;
}

+ (LoadProcess *)sharedInstance {
    static dispatch_once_t onceToken;
    static LoadProcess *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[LoadProcess alloc] init];
    });
    return instance;
}

+(void)dismissLoad{
    [KVNViewController dismissProcess];
}

@end
