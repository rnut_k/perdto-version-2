//
//  OrdersKitchen.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/19/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrdersKitchen : NSObject

@property(nonatomic,assign) NSString *maxPrevOrderId;
@property(nonatomic,retain) NSMutableDictionary *orders;
@property(nonatomic,retain) NSMutableDictionary *fetchOrders;

@property(nonatomic,retain) NSDictionary *tables;
@property(nonatomic,retain) NSDictionary *zones;
@property(nonatomic,retain) NSDictionary *menu;

+(OrdersKitchen *)getInstance;
-(void) sortOrdersData;
-(void)reSet;
@end
