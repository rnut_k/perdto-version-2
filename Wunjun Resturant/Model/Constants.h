//
//  Constants.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#ifndef Wunjun_Resturant_Constants_h
#define Wunjun_Resturant_Constants_h

#define APP_TYPE @"2"   //1 - resturant, 2 - customer
#define LOGIN_ERROR @"101"

#define kACTION_TABLE_OPEN                  (@"ACTION_TABLE_OPEN")                 //  1 - เปิดโต๊ะ
#define kACTION_TABLE_CLOSE                 (@"ACTION_TABLE_CLOSE")                //  2 - ปิดโต๊ะ
#define kACTION_TABLE_MOVE                  (@"ACTION_TABLE_MOVE")                 //  3 - ย้ายโต๊ะ
// ORDER
#define kACTION_ORDER_COMMIT                (@"ACTION_ORDER_COMMIT")               // 10
#define kACTION_ORDER_STAFF_STATUS          (@"ACTION_ORDER_STAFF_STATUS")         // 11
#define kACTION_ORDER_KITCHEN_STATUS        (@"ACTION_ORDER_KITCHEN_STATUS")       // 12
#define kACTION_ORDER_CANCEL                (@"ACTION_ORDER_CANCEL")               // 13
// PROMOTION
#define kACTION_PRO_ADMIN_APPROVE           (@"ACTION_PRO_ADMIN_APPROVE")          // 20
#define kACTION_PRO_STAFF_REQ_APPROVE       (@"ACTION_PRO_STAFF_REQ_APPROVE")      // 21
// INVOCE
#define kACTION_INV_ADMIN_APPROVE           (@"ACTION_INV_ADMIN_APPROVE")          // 30
#define kACTION_INV_CHECKER_REQ_APPROV      (@"ACTION_INV_CHECKER_REQ_APPROV")     // 31

#define kACTION_INV_CASHIER_REQ_APPROV      (@"ACTION_INV_CASHIER_REQ_APPROV")     // 40
#define kACTION_DISCOUNT_CASHIER_REQ_APPROV (@"ACTION_DISCOUNT_CASHIER_REQ_APPROV")// 41
// STOCK
#define kACTION_STOCK_CHECKER_REQ_APPROV    (@"ACTION_STOCK_CHECKER_REQ_APPROV")   // 50

/* Login define */

#define kTableOpen       AMLocalizedString(@"เปิดโต๊ะ", nil)
#define kTableOpenSum    AMLocalizedString(@"เปิดโต๊ะแบบรวม", nil)
#define kTableSum        AMLocalizedString(@"รวมโต๊ะ", nil)
#define kTableMove       AMLocalizedString(@"ย้ายโต๊ะ", nil)
#define kTablesPlitBill  AMLocalizedString(@"แยกบิล", nil)
#define kTableRevenge    AMLocalizedString(@"เช็คบิล", nil)
#define kTitleSignature  AMLocalizedString(@"ลายเซ็นต์", nil)
#define kTableOTP        @"OTP"

#define kSingleBill      @("singleBill")
#define kSubBill         @("subBill")
#define kIsSignAll       @("isSignAll")
#define kNotIsSignAll    @("notIsSignAll")

#define kCellBillTop      (@"cellBillTop")
#define kCellBill         (@"cellBill")
#define kCellBillBotton   (@"cellBillBotton")
#define kCellDrink        (@"cellDrink")
#define kCellDrinkSubmit  (@"cellDrinkSubmit")
#define kCellTotal        (@"cellTotal")
#define kCellBillCommit   (@"cellBillCommit")
#define kCellDrinkBill    (@"cellDrinkBill")
#define kCellPromotion    (@"cellPromotion")
#define kCellSignature    (@"cellSignature")
#define kCellTileSubBill  (@"cellTileSubBill")
#define kCellTotalALL     (@"cellTotalALL")
#define kCellTotalVat     (@"cellTotalVat")
#define kCellTotalSubbill (@"cellTotalSubbill")
#define kCellOptionFood   (@"CellListOptionFood")
#define kCellSumTotalBill (@"cellSumTotalBill")
#define kCellProTotalBill (@"cellProTotalBill")

#define kDrinkOne   NSLocalizedString(@"ดริงค์ ช็อตเดียว", nil)
#define kDrinkTwo   NSLocalizedString(@"ดริงค์ Star - Stop", nil)
#define kDrinkThree NSLocalizedString(@"ดริงค์ เหมาทั้งคืน", nil)


// printer
#define kPrinterDB (@"PrinterDB")
#define kIP (@"192.168.0.1")
#define kPort (@"9100")
//#define NOTI_URL_ADMIN @"http://koyacha.com:3333/n/websocket/"
//#define MAPI_URL_ADMIN @"http://koyacha.com/%@/api/"
//#define MAPI_URL_PIC_ADMIN @"http://koyacha.com/%@/"

//#define NOTI_URL @"http://notification1.perdto.com:80/n/websocket/"
#define MAPI_URL @"http://%@.perdto.com/api/"
#define MAPI_URL_PIC @"http://%@.perdto.com/"
#define WWW_URL @"http://www.perdto.com/"

//#define NOTI_URL    @"http://koyacha.com:3333/n/websocket/"
//#define MAPI_URL    @"http://koyacha.com/%@/api/"
//#define MAPI_URL_PIC @"http://koyacha.com/%@/"

//#define MAPI_URL @"http://resturant.dev/%@/index.php/api/"
//#define MAPI_URL_PIC @"http://resturant.dev/%@/index.php/"

#define LOGIN                   (MAPI_URL_PIC @"staff/loginSubmit")
#define LOGOUT                  (MAPI_URL_PIC @"staff/logoutSubmit")

#define LOAD_DATA               (MAPI_URL @"loadData")
#define GET_ZONE                (MAPI_URL @"getZone")
#define GET_TABLES              (MAPI_URL @"getTables")
#define GET_MANAGEUSAGE_TABLE   (MAPI_URL @"manageUsageTable")
#define GET_MENU_All            (MAPI_URL @"getMenu")
#define GET_BILL                (MAPI_URL @"getDetailUsageTable/%@")

// manageDrink
#define GET_MANAGE_DRINK        (MAPI_URL @"manageDrink")
#define GET_STATUS_DREINK_GIRL  (MAPI_URL @"getStatusDrinkgirl")
#define GET_REPORT_DREINK_GIRL  (MAPI_URL @"getReportDrinkgirl")

// order
#define GET_ADD_ORDER           (MAPI_URL @"addOrders")
#define SET_CANCEL_ORDER        (MAPI_URL @"cancelOrders")
#define GET_SUBMIT_ORDER        (MAPI_URL @"submitOrders")
#define UPLOAD_IMAGE            (MAPI_URL_PIC @"picture/uploadPictureMobile")
#define GET_ORDER_DETAIL        (MAPI_URL @"getOrdersDetail/%@")
#define GET_MANAGE_ORDER        (MAPI_URL @"manageOrders")

// Promotiom
#define GET_REQUIRE_PROMOTIOM   (MAPI_URL @"requirePromtion")

//admin
#define GET_PENDING_PROMOTION   (MAPI_URL @"getPendingPromotion/")
#define GET_PENDING_INVOICE     (MAPI_URL @"getPendingInvoice/")
#define GET_PENDING_INCOME      (MAPI_URL @"getPendingIncome/")
#define GET_COUNTER_PENDING     (MAPI_URL @"getCounterPending/")
#define GET_REPORT_STOCK        (MAPI_URL @"getReportStock/%@/%@")
#define APPROVE_PROMOTION       (MAPI_URL @"approvePromotion/")

//kitchen
#define GET_ORDERS_KITCHEN      (MAPI_URL @"getOrderForKitchen/")
#define SET_DELIVER_STATUS      (MAPI_URL @"setDevilerStatus/")
#define SET_BLOCK_MENU          (MAPI_URL @"setBlockMenu/")

// cash
#define GET_BILL_CASHIER        (MAPI_URL @"getAllDetailUsageTableCashier/%@")
#define GET_BILL_DETAIL         (MAPI_URL @"getBillDetailBy")
#define GET_REQUIRE_DISCOUNT    (MAPI_URL @"requireDiscount")
#define GET_ORDER_TO_GO_LIST    (MAPI_URL @"getOrderToGoList")
#define GET_ALERT_CASHIER       (MAPI_URL @"alertCashier")
#define GET_PENDING_NOTI        (MAPI_URL @"getPendingNotification")
#define GET_READ_PENDING_NOTI   (MAPI_URL @"readPendingNotification/%@") // id
#define GET_SUBMIT_COUPON       (MAPI_URL @"submitCoupon")
#define SET_SERVICE_RATE        (MAPI_URL @"setServiceRate")
#define SET_DELETE_BILL         (MAPI_URL @"deleteBill/%@")
#define GET_LIST_BILLDETAIL     (MAPI_URL_PIC @"report_bill/getListBillDetail")
#define GET_BACK_BiLL           (MAPI_URL_PIC @"orders?m=1&r=0&c=0/%@")

// จัดการรายได้ประจำวัน
#define GET_MANAGE_INCOME       (MAPI_URL @"manageIncome")

// CHECKER
#define GET_ADD_CHECKIN_GOODS     (MAPI_URL @"addCheckinGood")
#define GET_UPDATE_CHECKIN_GOODS  (MAPI_URL @"updateCheckinGoods")
#define GET_MANAGE_STOCK          (MAPI_URL @"manageStock")
#define GET_REQUIRE_INVOICE       (MAPI_URL @"requireInvoice")
#define GET_MANAGE_INVOICE        (MAPI_URL @"manageInvoice")

// MAMA
#define GET_MAMA_REPORT     (MAPI_URL @"getMamaReport")

#define GET_QRCODE          (WWW_URL @"qrcode/?qr_text=http://%@.perdto.com/mobile/?qr=%@")

#define kidRoleView   (@"idRoleViewController")
#define kidLoginView  (@"idLoginViewController")
#define kidViewCoupon (@"idViewCoupon")

#endif
