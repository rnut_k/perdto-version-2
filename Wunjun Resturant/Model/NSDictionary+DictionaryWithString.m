//
//  NSDictionary+DictionaryWithString.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/13/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "NSDictionary+DictionaryWithString.h"
#import "Check.h"

@implementation NSDictionary (DictionaryWithString)

+ (NSDictionary *)dictionaryWithString:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
//    NSString *error;
//    NSPropertyListFormat format;
//    NSDictionary *dict = [NSPropertyListSerialization
//                          propertyListFromData:data
//                          mutabilityOption:NSPropertyListImmutable
//                          format:&format
//                          errorDescription:&error];
    if(!dict){
        NSError* error;
        dict = [NSJSONSerialization  JSONObjectWithData:data
                                                options:NSJSONReadingMutableLeaves
                                                  error:&error];
    }
    return dict;
}
+(NSArray *)searchDictionary:(NSString *)key andvalue:(NSString *)value andData:(NSArray *)data{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:value,key];
    NSArray *filteredArray = [data filteredArrayUsingPredicate:predicate];
    if ([filteredArray count] != 0) {
        return filteredArray[0];
    }
    return nil;
}
+(NSArray *)searchDictionaryALL:(NSString *)key andvalue:(NSString *)value andData:(NSArray *)data{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:value,key];
    NSArray *filteredArray = [data filteredArrayUsingPredicate:predicate];
    if ([filteredArray count] != 0) {
        return filteredArray;
    }
    return nil;
}
+(NSArray *)splitString:(NSString *)str{
    NSString *first = [[str substringToIndex:[str length]-1]  substringFromIndex:1];   // LEFT
    NSArray *stringArray = [first componentsSeparatedByString:@","];
    return stringArray;
}
+(NSDictionary *) indexKeyedDictionaryFromArray:(NSArray *)array{
    id objectInstance;
    int indexKey = 0;
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    
    for (objectInstance in array)
        [mutableDictionary setObject:objectInstance forKey:[NSNumber numberWithUnsignedInt:indexKey++]];
    
    return (NSDictionary *)mutableDictionary;
    
}

+(NSDictionary *) dictionaryWithObject:(id)object{
    NSDictionary *data = [Check checkObjectType:object];
    if (data) {
        return [NSDictionary dictionaryWithDictionary:data];
    }
    return data;
}
+(NSMutableDictionary *) dictionaryMutableWithObject:(id)object{
    NSMutableDictionary *data = [Check checkObjectType:object];
    if (data) {
        return [NSMutableDictionary dictionaryWithDictionary:data];
    }
    return data;
}

@end
