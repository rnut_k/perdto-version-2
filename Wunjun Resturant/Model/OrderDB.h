//
//  OrderDB.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
@interface OrderDB : NSObject

@property (nonatomic, strong) NSString *key;         //  idInvoice
//@property (nonatomic, strong) NSString *dataOrder;   //  ข้อมูล order
@property (nonatomic, strong) NSString *orderFood;   //  ข้อมูล อาหาร
@property (nonatomic, strong) NSString *orderDrink;  //  ข้อมูล เครื่องดื่ม

//@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *orderDB;

+(OrderDB *)getInstance;
- (void) createLoginDb;
- (void) loadData:(NSString *)key;
- (void) insertLoginData:(NSString *)orderFood  andDrink:(NSString *)orderDrink and:(NSString *)key;
- (void) deleteLoginDb:(NSString *)keyDelete;
- (void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate andTypeFood:(BOOL)blFood;
- (BOOL) isKeyDB:(NSString *)key;
- (void) deleteLoginDb;
@end
