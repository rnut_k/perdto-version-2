//
//  SelectBill.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/13/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBill : UIViewController

+(void)setCheckOpenTable:(BOOL)_checkOpenTable;
+(void)setIsCashier:(BOOL)_isCashier;
+(void)setIscashTableStatus:(BOOL)_iscashTableStatus;
+(void)selectBill:(id)_self andData:(NSDictionary *)data andDataTable:(NSDictionary *)dataTable fromRole:(NSString *)role;
+(void)setIsTake:(int)num;
@end
