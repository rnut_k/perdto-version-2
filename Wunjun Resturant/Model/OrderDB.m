//
//  OrderDB.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "OrderDB.h"
#import "Time.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Check.h"

@interface OrderDB(){
    
}
@property (nonatomic,strong) NSString *databaseFile;
@end
@implementation OrderDB{
    bool databaseIsUsed ;
}

-(NSString *)databaseFile{
    if (!_databaseFile) {
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _databaseFile = [[NSString alloc] initWithString:[dirPaths[0] stringByAppendingPathComponent:@"order_info.db"]];
    }
    return _databaseFile;
}
+(OrderDB *)getInstance
{
    static dispatch_once_t onceToken;
    static OrderDB *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[OrderDB alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void) loadData:(NSString *)key
{
    [self createLoginDb];
    const char *dbpath = [self.databaseFile UTF8String];
    sqlite3_stmt *statement;
    
    if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM order_info WHERE key = '%@' LIMIT 1 ",key];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_orderDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                _key            = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                _orderFood      = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                _orderDrink     = [[NSString alloc] initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
            }
            else {
                NSLog(@"Match not found");
                _key        = @"";
                _orderFood  = @"";
                _orderDrink = @"";
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_orderDB);
    }
}
- (BOOL) isKeyDB:(NSString *)key
{
    [self createLoginDb];
    const char *dbpath = [self.databaseFile UTF8String];
    sqlite3_stmt *statement;
    BOOL result = NO;
    if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat:@"SELECT * FROM order_info WHERE key = '%@' LIMIT 1 ",key];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(_orderDB, query_stmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                result = YES;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(_orderDB);
    }
    
    return result;
}
- (void) createLoginDb
{
//    NSArray *dirPaths;
    
    // Get the documents directory
//    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    self.databasePath = dirPaths[0];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath:self.databaseFile] == NO)
    {
        const char *dbpath = [self.databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS order_info (key TEXT , orderFood TEXT , orderDrink TEXT )";
            
            if (sqlite3_exec(_orderDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            sqlite3_close(_orderDB);
        }
        else {
            NSLog(@"Failed to open/create database");
        }
    }
}
- (void) insertLoginData:(NSString *)orderFood  andDrink:(NSString *)orderDrink and:(NSString *)key
{
    _key   = key;
    
    if (_key != nil) {
        [self createLoginDb];
        sqlite3_stmt *statement;
        
        const char *dbpath = [self.databaseFile UTF8String];
        
        if (![orderFood isEqualToString:@"orderDrink"] || !_orderFood) {
            _orderFood   = orderFood;
        }
        if (![orderDrink isEqualToString:@"orderDrink"] || !_orderDrink) {
            _orderDrink    = orderDrink;
        }
   
        
        if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
        {
            const char *sql = [[NSString stringWithFormat:@"DELETE FROM order_info WHERE key = '%@'", _key] UTF8String];
            sqlite3_prepare_v2(_orderDB, sql, -1, &statement, NULL);
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                NSLog(@"delete error");
            }
            
            static sqlite3_stmt *insertStmt = nil;
            
            if(insertStmt == nil)
            {
                const char *insertSql = "INSERT INTO order_info(key,orderFood,orderDrink) VALUES(?,?,?)";
                NSInteger result = sqlite3_prepare_v2(_orderDB, insertSql, -1, &insertStmt, NULL);
                NSLog(@"upss %s\n", sqlite3_errmsg(_orderDB));
                
                if(result == SQLITE_OK)
                {
                    sqlite3_bind_text(insertStmt, 1, [_key UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(insertStmt, 2, [_orderFood UTF8String], -1, SQLITE_TRANSIENT);
                    sqlite3_bind_text(insertStmt, 3, [_orderDrink UTF8String], -1, SQLITE_TRANSIENT);
                    if(sqlite3_step(insertStmt)==SQLITE_DONE)
                    {
                        NSLog(@"done");
                    }
                    else{ NSLog(@"ERROR");
                        sqlite3_reset(insertStmt); }
                }
                else
                {
                    NSAssert1(0, @"Error . '%s'", sqlite3_errmsg(_orderDB));
                }
//                if(sqlite3_prepare_v2(_orderDB, insertSql, -1, &insertStmt, NULL) != SQLITE_OK)
//                    NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_orderDB));
            }
            
//            sqlite3_bind_text(insertStmt, 1, [_key UTF8String], -1, SQLITE_TRANSIENT);
//            sqlite3_bind_text(insertStmt, 2, [_orderFood UTF8String], -1, SQLITE_TRANSIENT);
//            sqlite3_bind_text(insertStmt, 3, [_orderDrink UTF8String], -1, SQLITE_TRANSIENT);
//            
//            if(SQLITE_DONE != sqlite3_step(insertStmt))
//                NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(_orderDB));
//            else
//                NSLog(@"Inserted");
            //Reset the add statement.
            
//            sqlite3_reset(insertStmt);
            insertStmt = nil;
            
            sqlite3_finalize(statement);
            sqlite3_close(_orderDB);
        }
    }
}
- (void) deleteLoginDb:(NSString *)keyDelete
{
    sqlite3_stmt *deleteStmt;
    const char *dbpath = [self.databaseFile UTF8String];
    
    if (keyDelete != nil) {
        [self createLoginDb];
        if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
        {
            const char *sql = [[NSString stringWithFormat:@"DELETE FROM order_info WHERE key LIKE '%@'",_key] UTF8String];
            sqlite3_prepare_v2(_orderDB, sql, -1, &deleteStmt, NULL);
            if (sqlite3_step(deleteStmt) != SQLITE_DONE)
            {
                NSLog(@"delete error");
            }
        }
        sqlite3_finalize(deleteStmt);
        sqlite3_close(_orderDB);
    }
}

- (void) deleteLoginDb
{
    sqlite3_stmt *deleteStmt;
    if (![Check checkNull:self.databaseFile]) {
        const char *dbpath = [self.databaseFile UTF8String];
        
        if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
        {
            NSString *insertSQL = @"DELETE FROM order_info";
            
            const char *insert_stmt = [insertSQL UTF8String];
            sqlite3_prepare_v2(_orderDB, insert_stmt, -1, &deleteStmt, NULL);
            if (sqlite3_step(deleteStmt) != SQLITE_DONE)
            {
                NSLog(@"insert error");
            }
            
            sqlite3_finalize(deleteStmt);
            sqlite3_close(_orderDB);
        }
    }
}

-(void) updateDb:(NSString*)jsonObject andId:(NSString*)_idUpdate andTypeFood:(BOOL)blFood{
    NSString *keyUpdate = _idUpdate;
    NSString *val       = jsonObject;
    
    if (databaseIsUsed) {
        [self updateDb:jsonObject andId:_idUpdate andTypeFood:blFood];
        return;
    }
    databaseIsUsed = TRUE;
    
   sqlite3_close(_orderDB);
    [self createLoginDb];
    const char *dbpath = [self.databaseFile UTF8String];
    if (sqlite3_open(dbpath, &_orderDB) == SQLITE_OK)
    {
        static sqlite3_stmt *insertStmt = nil;
        if(insertStmt == nil)
        {
            NSString *updateSQL = @"UPDATE order_info SET orderFood = ? WHERE key = ?";
            if (!blFood) {
                updateSQL = @"UPDATE order_info SET orderDrink = ? WHERE key = ?";
            }
            
            if(sqlite3_prepare_v2(_orderDB, [updateSQL UTF8String], -1, &insertStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating insert statement. '%s'", sqlite3_errmsg(_orderDB));
        }
        
        sqlite3_bind_text(insertStmt, 1, [val UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, [keyUpdate UTF8String], -1, SQLITE_TRANSIENT);
        if(SQLITE_DONE != sqlite3_step(insertStmt))
            NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(_orderDB));
        
        sqlite3_reset(insertStmt);
        insertStmt = nil;
        sqlite3_close(_orderDB);
    }
    databaseIsUsed = FALSE;
}
@end


