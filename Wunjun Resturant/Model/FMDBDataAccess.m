//
//  FMDBDataAccess.m
//  Chanda
//
//  Created by Mohammad Azam on 10/25/11.
//  Copyright (c) 2011 HighOnCoding. All rights reserved.
//

#import "FMDBDataAccess.h"
#import "FMDatabaseAdditions.h"
#import "Check.h"

#define DATABASE_NAME @"printerInfo.db"

@implementation FMDBDataAccess

+ (NSArray *)columnArray
{
    static NSArray *columnArray;
    @synchronized(self) {       // add attribute
        columnArray = @[@"key",
                        @"listPrinter",
                        @"isOpen",
                        @"isMenuSug"];
    }
    return columnArray;
}


+(NSString *) getDatabasePath{
      
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDir = [documentPaths objectAtIndex:0];
    NSString *databasePath = [documentDir stringByAppendingPathComponent:DATABASE_NAME];
    return databasePath;
}
- (id)init {
    self = [super init];
    if (self) {
        [self createLoginDb];
        if ([self createAndCheckDatabase]) {
            [self checkVersionDB:DATABASE_NAME];
        }
    }
    return self;
}
-(BOOL) createAndCheckDatabase
{
    BOOL success; 
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    success = [fileManager fileExistsAtPath:[FMDBDataAccess getDatabasePath]];
    
    if(success) return YES; 
    else return  NO;
}
-(void) createLoginDb{
    
    FMDatabase *database = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    if (![self createAndCheckDatabase]) {
        BOOL status =  [database open];
        if (status) {
            [database executeUpdate:@"CREATE TABLE printerInfo (ID INTEGER PRIMARY KEY AUTOINCREMENT,key TEXT DEFAULT '1',listPrinter TEXT DEFAULT '0',isOpen TEXT DEFAULT '0',isMenuSug TEXT DEFAULT '0')"];
            [database close]; 
        }  
    }  
}
- (BOOL) isKeyDB:(NSString *)key
{
    [self createLoginDb];
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    [db open];    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM printerInfo"];
    
    while([results next]) {
        [db close];  
        return YES;
        
    }
    
    [db close];  
    return NO; 
}

-(BOOL) updateCustomer:(PrinterDB *)customer
{
    [self createLoginDb];
    
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    [db open];
    
    BOOL success = [db executeUpdate:[NSString stringWithFormat:@"UPDATE printerInfo SET listPrinter = '%@', isOpen = '%@', isMenuSug = '%@' where key = '1'",customer.listPrinter,customer.isOpen,customer.isMenuSug]];
    
    [db close];
    
    return success; 
}

-(BOOL) insertCustomer:(PrinterDB *) customer 
{
    // insert customer into database
    [self createLoginDb];    
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    [db open];
    
    BOOL success =  [db executeUpdate:@"INSERT INTO printerInfo (listPrinter,isOpen,isMenuSug) VALUES (?,?,?);",
                     customer.listPrinter,customer.isOpen,customer.isMenuSug, nil];
    
    [db close];
    
    return success; 
}

-(PrinterDB *) getCustomers
{    
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    [db open];    
    FMResultSet *results = [db executeQuery:@"SELECT * FROM printerInfo"];
    PrinterDB *customer = [[PrinterDB alloc] init];
    
    while([results next]) 
    {
        customer.key = [results stringForColumn:@"key"];
        customer.listPrinter = [results stringForColumn:@"listPrinter"];
        customer.isOpen = [results stringForColumn:@"isOpen"];
        customer.isMenuSug = [results stringForColumn:@"isMenuSug"]; 
    }
    
    [db close];  
    return customer; 

}

-(BOOL) checkVersionDB:(NSString *)key{
    
    @try {        
        NSArray *listColumn = [[self class] columnArray];
        FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
        
        if ([db open]) {
            FMResultSet *results = [db executeQuery:@"SELECT * FROM printerInfo"];
            
            for (int i = 0; i < [listColumn count]; i++) {
                NSString *columnText = listColumn[i];
                int bl = [results columnIndexForName:columnText];
                if (bl == -1) {
                    [db close];
                    return  [self addComlumDB:i];
                    break;
                }
            }
                    
        }   
        
        [db close];
        return NO;
    }
    @catch (NSException *exception) {
        DDLogError(@"error %@",exception);
    }
}

-(BOOL)addComlumDB:(int )vs{
    BOOL success; 
    NSArray *listColumn = [[self class] columnArray];
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    if ([db open]) {
        for (int i = vs; i < [listColumn count]; i++) {
            NSString *addColumnSQL = [NSString stringWithString:listColumn[i]];
            success = [self addColumn:addColumnSQL];
        }
    }
    
    [db close]; 
    return success;
}
-(BOOL)addColumn:(NSString*)column {    
    [self createLoginDb];    
    
    BOOL success;    
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    if (![db open]) {
        NSLog(@"open failed");
        return NO;
    }
    [db setShouldCacheStatements:YES];
    [db setCrashOnErrors:YES];
    
    if (![db columnExists:@"printer_info" inTableWithName:column]) {
        NSString *nsql = [NSString stringWithFormat:@"ALTER TABLE printerInfo ADD COLUMN  '%@' TEXT DEFAULT '0'", column];        
        success = [db executeUpdate:nsql];
        NSAssert(success, @"alter table failed: %@", [db lastErrorMessage]);
    }    
    [db close];
    
    return success;
}

-(BOOL)deleteDateBase{
    BOOL success;    
    FMDatabase *db = [FMDatabase databaseWithPath:[FMDBDataAccess getDatabasePath]];
    
    [db open];
    success = [db executeUpdate:@"DELETE FROM printerInfo WHERE key = '1'", nil];
    [db close];
    
    return success;
}
@end
