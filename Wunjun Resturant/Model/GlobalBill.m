//
//  GlobalBill.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "GlobalBill.h"

@implementation GlobalBill

@synthesize moneyIn       = _moneyIn;
@synthesize billList      = _billList;
@synthesize billType      = _billType;
@synthesize billResturant = _billResturant;
@synthesize billStaff     = _billStaff;
@synthesize billListTable = _billListTable;
@synthesize indexSplitBill = _indexSplitBill;

+ (GlobalBill *)sharedInstance {
    static dispatch_once_t onceToken;
    static GlobalBill *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GlobalBill alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        _billListTable = [NSMutableArray array];
        _billStaff     = [[NSMutableDictionary alloc] init];
        _billResturant = [[NSMutableDictionary alloc] init];
        _billList      = [[NSMutableDictionary alloc] init];
        _billType      = [NSString string];
        _moneyIn       = [NSString string];
        _indexSplitBill   = [NSString string];
    }
    return self;
}

- (void) reset{
    _billListTable = [NSMutableArray array];
    _billStaff     = [[NSMutableDictionary alloc] init];
    _billResturant = [[NSMutableDictionary alloc] init];
    _billList      = [[NSMutableDictionary alloc] init];
    _billType      = [NSString string];
    _moneyIn       = [NSString string];
    _indexSplitBill  = [NSString string];
}

// staff
//{
//    address = "\U0e01\U0e17\U0e21";
//    age = 55;
//    code = "eyJ1c2VybmFtZSI6ImVhayIsInVzZXJfaWQiOiI2IiwibmFtZSI6ImVhayIsImxhc3RuYW1lIjoia2FjaGFpIiwiY29kZSI6IjVaRjNadWlHWEFWcFpcL1lrUDRjeW5cL3psMDZQdkgxeVwvRlc0OWtSeE1CWGRLdVNRcW1MV3ZTVVA1cGlxWTd1aUlcL0NaSHQ2aW5oUzZoeThyaWhndERTSWV2WWpQZEtJUVFpQjFGditnVEZxajJYMGNKVllaSGVBPT0ifQ==";
//    "dvi_and" = "<null>";
//    "dvi_ios" = "<null>";
//    id = 6;
//    "is_enabled" = 1;
//    lastname = kachai;
//    login = 1;
//    name = eak;
//    nickname = eak;
//    phone = 0995544525;
//    role = "[\"admin\",\"staff\",\"checker\",\"cashier\",\"kitchen\"]";
//    "shop_id" = 2;
//    "staff_position" = "<null>";
//    username = eak;
//}

//resturant =     {
//    address = "\U0e0a\U0e31\U0e49\U0e19 1 \U0e23\U0e48\U0e21\U0e40\U0e01\U0e25\U0e49\U0e32";
//    config = "{\"work_time\":{\"day\":[2,3,4,5,6,7],\"time\":{\"open\":\"17:00\",\"close\":\"2:00\"}}}";
//    description = 224;
//    "drinkgirl_config" = "{\"margin\":\"ceil\",\"min_d\":\"20\",\"min_n\":10}";
//    "has_drink" = 0;
//    id = 1;
//    "is_sign_all" = 0;
//    name = "the-im";
//    "noti_material" = "{\"is_usage\":0, \"per_of_material\":10}";
//    phone = "<null>";
//    title = "\U0e23\U0e49\U0e32\U0e19 the-im ";
//    "use_sub_bill" = 0;
//};

//"bill_id" = 6;
//orders =     (
//);
//promotion =     {
//    "is_promotion" = 0;
//    "promotion_info" =         (
//    );
//    "promotion_pool" =         (
//    );
//};
//"promotion_pool_id" = 0;
//subbill = 0;
//"table_id" = 2;
//total = 0;
//usage =     {
//    "bill_id" = 6;
//    "close_datetime" = "0000-00-00 00:00:00";
//    id = 10;
//    "is_multiple_bill" = 0;
//    "n_female" = 2;
//    "n_male" = 3;
//    "open_datetime" = "2015-02-11 14:40:42";
//    "table_id" = 2;
//};
//"usage_id" = 10;
@end

