//
//  AppDelegate.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>

#import "AlertViewNotiController.h"
#import "UIColor+HTColor.h"
#import "WYPopoverController.h"
#import "LoginViewController.h"
#import "TablesViewController.h"

#import "SRWebSocket.h"
#import "GlobalSocket.h"
#import "Member.h"
#import "Check.h"
#import "Time.h"
#import "SelectBill.h"
#import "Reachability.h"
#import "NSString+GetString.h"
#import "LocalizationSystem.h"

@interface AppDelegate ()<SRWebSocketDelegate,WYPopoverControllerDelegate,AVAudioPlayerDelegate>{
//    GlobalSocket *globalSocket;
    NSTimer *notifyTimerASKAC;
    WYPopoverController *popoverAlertController;
    AlertViewNotiController *alertView;
    AVAudioPlayer *avSound;
}
@property (nonatomic, strong) Member *member;
@property (nonatomic, strong) UITableView *tableAlert;
@property (nonatomic ,strong) GlobalSocket *globalSocket;
//@property (nonatomic ,strong) AppDelegate *appdelegate;
@end

static AppDelegate *appdelegate;
@implementation AppDelegate
void AudioServicesPlaySystemSound (
                                   SystemSoundID inSystemSoundID
                                   );
OSStatus AudioServicesCreateSystemSoundID (
                                           CFURLRef       inFileURL,
                                           SystemSoundID  *outSystemSoundID
                                           );

+(AppDelegate *)appdelegate{
    return appdelegate;
}
+ (void) setAppDelegate:(AppDelegate *)value{
    appdelegate = value;
}
-(GlobalSocket *)globalSocket{
    if (!_globalSocket) {
        _globalSocket = [GlobalSocket sharedInstance];
    }
    return _globalSocket;
}
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [AppDelegate setLogger];
     NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [self switchTolanguage:LocalizationgetNSBundle];
    [AppDelegate setAppDelegate:self];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [AppDelegate registerPush:application];

    [self handlelLaunchingFromNotification:launchOptions and:application];
    application.applicationIconBadgeNumber = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(manageNotification:)
                                                 name:@"UIApplicationReSendPRNS" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectWebSocketActiveApp:)
                                                 name:@"UIApplicationConnectSocket" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(disconnectWebSocketForegroundApp:)
                                                 name:@"UIApplicationDisconnectSocket" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateMenu:)
                                                 name:@"UIApplicationUpdateMenu" object:nil];
    
    [self checkNetwork];
    
    return YES;
}
void uncaughtExceptionHandler(NSException *exception)
{
    DDLogError(@" ### App   Crash:%@", exception);
    DDLogError(@" ### Stack Trace:%@", [exception callStackSymbols]);
}
+ (void)setLogger{
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    // Enable Colors
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:0.059 green:0.803 blue:0.000 alpha:1.000] backgroundColor:nil forFlag:LOG_FLAG_INFO];
    
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.502 alpha:1.0]
                                     backgroundColor:nil
                                             forFlag:LOG_FLAG_VERBOSE];
    
    // Initialize File Logger
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    
    // Configure File Logger
    [fileLogger setMaximumFileSize:(1024 * 1024)];
    [fileLogger setRollingFrequency:(3600.0 * 24.0)];
    [[fileLogger logFileManager] setMaximumNumberOfLogFiles:7];
    [DDLog addLogger:fileLogger];
    
    
    DDLogError(@"DDLogError");
    DDLogWarn(@"DDLogWarn") ;
    DDLogInfo(@"DDLogInfo");
    DDLogVerbose(@"DDLogVerbose");
    DDLogDebug(@"DDLogDebug");
}
-(void)checkNetwork{
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    }];
    
    NetworkStatus status  = [reach currentReachabilityStatus];
    if (status == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"ไม่ได้เชื่อมต่อเน็ต", nil)
                                                        message:AMLocalizedString(@"กรุณาเปิดระบบเชื่อมต่อเน็ต", nil)
                                                       delegate:nil
                                              cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else {
        NSLog(@"There IS internet connection");
    }
}

-(void)handlelLaunchingFromNotification:(NSDictionary *)launchOptions and:(UIApplication *)application{
    // Handle launching from a notification
    UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (locationNotification) {
        application.applicationIconBadgeNumber = 0;
    }
}
+ (void)registerPush:(UIApplication *)application {
    // Register for Push Notitications, if running iOS 8
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        // Register for Push Notifications before iOS 8
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge |
          UIRemoteNotificationTypeAlert |
          UIRemoteNotificationTypeSound)];
        
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    
    DDLogWarn(@" ### AppDelegate applicationWillResignActive");
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    DDLogWarn(@" ### AppDelegate applicationDidEnterBackground");
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
//    [self.globalSocket close];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    DDLogWarn(@" ### AppDelegate app enter foreground");
    
    if([self.member isUserLogin]){
        [self connectWebSocketActiveApp:self];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
     DDLogWarn(@" ### AppDelegate applicationWillTerminate");
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    DDLogWarn(@" ### AppDelegate app did become active");
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if([self.member isUserLogin] && ![window.rootViewController isKindOfClass:[RoleViewController class]] && ![window.rootViewController isKindOfClass:[InitViewController class]])
    {
        [self connectWebSocketActiveApp:self];
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    DDLogWarn(@" ### AppDelegate APN device token: %@", deviceToken);
    NSString *strToken = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    strToken = [strToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    DeviceData *device = [DeviceData getInstance];
    device.token = strToken;
    
    DDLogWarn(@" ### AppDelegate My token is: %@", strToken);
}
-(void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    DDLogWarn(@" ### AppDelegate Falie to get token , error : %@",error);
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DDLogWarn(@" ### AppDelegate didReceiveRemoteNotification :%@",userInfo);
    [self didReceiveApnsMessage:userInfo];
    [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}
//#pragma mark web socket
//-(void)connectWebSocket {
//    DDLogWarn(@"  ### AppDelegate connectWebSocket");
//    self.globalSocket.webSocket.delegate = nil;
//    self.globalSocket.webSocket = nil;
//    
//    NSString *urlString = NOTI_URL;
//    SRWebSocket *newWebSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:urlString]];
//    newWebSocket.delegate = self;
//    [newWebSocket open];
//}
//- (void)disConnectWebSocket:(id)sender {
//    [self.globalSocket.webSocket close];
//     DDLogWarn(@"  ### AppDelegate disConnectWebSocket");
//}

+ (void)connectWebSocketActiveAppNoti {
    [self.appdelegate connectWebSocketActiveApp:self];
    DDLogWarn(@"  ### AppDelegate connectWebSocketActiveAppNoti");
}

- (void)connectWebSocketActiveApp:(id)sender {
//    globalSocket = [GlobalSocket sharedInstance];
    [self.globalSocket connectWebSocket];
    DDLogWarn(@"  ### AppDelegate connectWebSocketActiveApp");
}
- (void)disconnectWebSocketForegroundApp:(id)sender {

    [self.globalSocket close];
    DDLogWarn(@"  ### AppDelegate disconnectWebSocketForegroundApp");
}

- (void) didReceiveApnsMessage:(NSDictionary *)userInfo
{
    NSDictionary *payload = [NSDictionary dictionaryWithObject:userInfo];
    DDLogWarn(@" ### AppDelegate apn : %@",payload);
    
    if ([payload count] != 0) {
        NSString *dataNoti = [NSString checkNull:payload[@"m"][@"t"]];
        if ([dataNoti isEqualToString:@"NOTI"]) {
            NSString *dataAert = [NSString stringWithFormat:@"%@",payload[@"m"][@"d"][@"a"]];
            NSString *position = [self getStatus:[dataAert intValue]];
            DDLogInfo(@"position %@",position);
            
            if (![Check checkNull:position]) {
                [self Vibrate];
            }
            NSDictionary *dataNoti = [NSDictionary dictionaryWithObject:payload[@"m"][@"d"]];
            if ([position isEqual:@"staff"]) {
                DDLogInfo(@"staff");
                [self notiBillTable:dataNoti];
            }
            else if ([position isEqual:@"admin"]) {
                DDLogInfo(@"admin");
                [self setNotiData:dataNoti and:@"NotiupdataPending"];
                [self setNotiData:dataNoti and:@"NotiupdataRoel"];
            }
            else if ([position isEqual:@"cashier"]) {
                DDLogInfo(@"cashier");
                [self setNotiData:dataNoti and:@"updateCheckBillNoti"];
            }
            else if ([position isEqual:@"checker"]) {
                DDLogInfo(@"checker");
                [self setNotiData:nil and:@"updateInvoiceList"];
                [self setNotiData:dataNoti and:@"NotiupdataRoel"];
            }
            else if ([position isEqual:@"kitchen"]) {
                DDLogInfo(@"kitchen");
                [self setNotiData:nil and:@"NotisetInsertKitchen"];
            }
        }
    }
}
#pragma mark SRWebSocketDelegate protocol
- (void)webSocketDidOpen:(SRWebSocket *)newWebSocket {
    NSLog(@" ## webSocketOpened");
    self.globalSocket.webSocket = newWebSocket;
    NSDictionary *setData = @{@"type":@"PBLK",
                              @"data":@{@"shop_id":self.member.shopID,
                                        @"staff_id":self.member.id
                                      },
                              @"dvt":@"ios",
                              @"code":self.member.code,
                              @"device_id":@"imei"
                              };
    NSError *error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:setData
                                                       options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [self.globalSocket.webSocket send:jsonString];
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
     [self checkNetwork];
     DDLogError(@" ### AppDelegate didFailWithError %@",error);
    [self.globalSocket connectWebSocket];
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean {
     DDLogError(@" ### AppDelegate didCloseWithCode");
    if(code != 0){
        [self.globalSocket connectWebSocket];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message {
    @try
    {
        DDLogWarn(@" ### AppDelegate didReceiveMessage");
       
        NSError *error;
        NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dataResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        NSString *type = dataResult[@"type"];
        DDLogInfo(@" didReceiveMessage : %@",dataResult);
        
        if (![type isEqual:@"PRSN"] && ![type isEqual:@"PBLK"]) {
            @try {
                 [self manageReceiveMessage:dataResult];
            }
            @catch (NSException *exception) {
                DDLogError(@" AppDelegate error manageReceiveMessage :%@",exception);
            }
        }
        else if([dataResult[@"type"] isEqualToString:@"PRSN"])
        {
            DDLogInfo(@" PRSN %@",dataResult);
        }
        else if([dataResult[@"type"] isEqualToString:@"PBLK"])
        {
            DDLogInfo(@" PBLK %@",dataResult);
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *role = [prefs objectForKey:@"role"];
            NSString *kitchen = [prefs objectForKey:@"kitchen_id"];
            if ([Check checkNull:kitchen]) {
                kitchen = @"";
            }
            
            NSDictionary *kitchen_id = ![role isEqualToString:@"kitchen"]?@{@"role":role}:@{
                                                                                            @"role":role,
                                                                                            @"kitchen":kitchen,
                                                                         };
            NSDictionary *data = @{
                           @"type":@"PRSN",
                           @"data":kitchen_id,
                           @"dvt":@"ios",
                           @"code":self.member.code
                           
                           };
            [self manageReceiveMessage:data];
        }
    }
    @catch (NSException *ex)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[NSString stringWithFormat:@"%@",ex]
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}
-(void)manageNotification:(NSNotification *)sender{
    NSDictionary *dataResult = [[sender userInfo] mutableCopy];
    DDLogInfo(@" call %@",dataResult);
    [self manageReceiveMessage:dataResult];
}
-(void)manageReceiveMessage:(NSDictionary *)dataResult
{
    if([dataResult[@"type"] isEqualToString:@"PRSN"])
    {
        DDLogInfo(@" PRSN");
//    role: string - [admin,cashier,checker,staff,kitchen] **ให้ส่งไปแค่ role เดียวเช่นถ้าเข้าหน้าแอพของ staff ก็ให้ส่ง staff
//    Receive: Not
        @try {
            NSError *error;
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dataResult
                                                               options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [self.globalSocket.webSocket send:jsonString];
        }
        @catch (NSException *exception) {
            DDLogError(@" AppDelegate error manageReceiveMessage  PRSN : %@",exception);
        }
    }
    else if([dataResult[@"type"] isEqualToString:@"NOTI"])
    {
        DDLogInfo(@" Noti");
//    Send: Not ไม่ต้องส่งอะไรกลับไป ให้รับข้อมูลมาแสดงอย่างเดียว
        DeviceData *device = [DeviceData getInstance];
//        NSString *createID = dataResult[@"data"][@"d"][@"creator"];
        NSString *device_id = dataResult[@"data"][@"d"][@"device_id"];
        
//        NSString *memberID = self.member.id;
        if (![device_id isEqualToString:device.token]) {
            [self Vibrate];
            [self createAlertView:dataResult];
        }
    }
    else if([dataResult[@"type"] isEqualToString:@"ASKA"])
    {
          DDLogInfo(@" ASKA");
//    Receive: { **ถ้ามีข้อความ type ASKA มาให้ตอบกลับไปทันทีเพราะทาง server จะเช็คการเชื่อมต่อหากไม่มีการตอบกลับ server จะตัดการเชือมต่อทันที
        @try {
            NSDictionary *data = @{@"type":@"ASKA",
                                   @"dvt":@"ios",
                                   @"code":self.member.code};
            NSError *error;
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                               options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [self.globalSocket.webSocket send:jsonString];
        }
        @catch (NSException *exception) {
            DDLogError(@" AppDelegate error manageReceiveMessage  ASKAC : %@",exception);
        }
    }
    else if ([dataResult[@"type"] isEqualToString:@"ASKAC"]){
        DDLogInfo(@" ASKAC");
//    ASKAC **ในกรณีครัวให้ส่ง ASKAC ไปทุก 5นาทีเพื่อเช็คการเชื่อมต่อกับ server หากไม่มีการตอบกลับให้ต่อใหม่หากต่อไม่สำเร็จให้แจ้งเตือนหรือ reload order
//    Receive: { ** ไม่ต้องตอบกลับทันทีให้ set time 5นาทีแล้วคอยตอบกลับ
        notifyTimerASKAC = [NSTimer timerWithTimeInterval:300.0      // set time
                                                   target:self
                                                 selector:@selector(countDownASKAC:)
                                                 userInfo:nil repeats:YES];     //7200.0
        [[NSRunLoop mainRunLoop] addTimer:notifyTimerASKAC forMode:NSDefaultRunLoopMode];
    }
    else
        DDLogError(@" Not type");
}
-(void)createAlertView:(NSDictionary *)data{
    DDLogInfo(@" createAlertView %@",data);
    NSString *num = [self getStatus:[data[@"data"][@"a"] intValue]];
    DDLogWarn(@"status %@",num);
    if ([num isEqual:@"staff"]) {
        DDLogInfo(@"staff");
        if (popoverAlertController)
            [alertView setDataAlertView:data];
        else
            [self showUserDataEntryForm:data];
    }
    else if ([num isEqual:@"โต๊ะ"]) {
        [self setNotiData:nil and:@"NotiupdataStatusTable"];
    }
    else if ([num isEqual:@"admin"]) {
        DDLogInfo(@"admin");
        NSDictionary *dataInvoice = data[@"data"];
        [self setNotiData:dataInvoice and:@"NotiupdataPending"];
    }
    else if ([num isEqual:@"cashier"]) {
        DDLogInfo(@"cashier");
        NSDictionary *dataInvoice = data[@"data"][@"d"];
        [self setNotiData:dataInvoice and:@"updateCheckBillNoti"];
    }
    else if ([num isEqual:@"checker"]) {
        DDLogInfo(@"checker");
        NSDictionary *dataInvoice = data[@"data"][@"d"][@"data"];
        [self setNotiData:dataInvoice and:@"updateInvoiceList"];
    }
    else if ([num isEqual:@"kitchen"]) {
        DDLogInfo(@"kitchen");
        NSDictionary *dataDict =@{@"data":data};
        [self setNotiData:dataDict and:@"NotisetOrdersKitchen"];
    }
    else if ([num isEqual:@"reloadDataOrder"]) {
        DDLogInfo(@"reloadDataOrder");
        NSDictionary *dataInvoice = data[@"data"][@"d"];
        [self setNotiData:dataInvoice and:@"reloadDataOrder"];
    }
}
-(void)setNotiData:(NSDictionary *)data and:(NSString *)name{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:name
                          object:self
                        userInfo:data];
}
- (IBAction)showUserDataEntryForm:(NSDictionary *)type {
    NSLog(@" showUserDataEntryForm");
    UINavigationController *navcon = (UINavigationController *)self.window.rootViewController;
    
    alertView = [navcon.storyboard instantiateViewControllerWithIdentifier:@"AlertViewAppdelegate"];
    [alertView createAlertView:type];
    alertView.preferredContentSize = CGSizeMake(300,300);
    alertView.modalInPopover = NO;
    
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:alertView];
    contentViewController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor ht_alizarinColor]};
    alertView.title = AMLocalizedString(@"แจ้งเตือน", nil);
    
   [self createViewPoppver:contentViewController];
}
-(void)setDatapopoverAlertControlle:(NSDictionary *)type{
    @try {
        NSLog(@" setDatapopoverAlertControlle");
        UINavigationController *navcon = (UINavigationController *)self.window.rootViewController;
        alertView = [navcon.storyboard instantiateViewControllerWithIdentifier:@"AlertViewAppdelegate"];
        alertView.title = AMLocalizedString(@"แจ้งเตือน", nil);
        [alertView createAlertView:type];
        
        alertView.preferredContentSize = CGSizeMake(300,300);
        alertView.modalInPopover = NO;
        
        UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:alertView];
        contentViewController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor ht_alizarinColor]};
        [self createViewPoppver:contentViewController];
    }
    @catch (NSException *exception) {
        NSLog(@" error createAlertView  : %@",exception);
    }
}
-(void)createViewPoppver:(UIViewController *)viewController{
    UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(270,3,30, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [viewController.view addSubview:btnOpiton];
    
    UIView * topView = [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];
    popoverAlertController = [[WYPopoverController alloc] initWithContentViewController:viewController];
    popoverAlertController.delegate = self;
    popoverAlertController.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
    popoverAlertController.wantsDefaultContentAppearance = NO;
    
    [popoverAlertController presentPopoverFromRect:CGRectZero
                                            inView:topView
                           permittedArrowDirections:WYPopoverArrowDirectionNone
                                           animated:YES
                                            options:WYPopoverAnimationOptionFadeWithScale];
}
- (UIView *)loadCustomObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    UIView *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController{
    popoverAlertController = nil;
}
-(void)countDownASKAC:(NSTimer *)timer {
    @try {
        NSDictionary *data = @{@"type":@"ASKAC",
                               @"dvt":@"ios",
                               @"code":self.member.code};
        NSError *error;
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                           options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [self.globalSocket.webSocket send:jsonString];
        [self stopTimer];
    }
    @catch (NSException *exception) {
        DDLogError(@" AppDelegate error manageReceiveMessage  ASKAC : %@",exception);
    }
}
- (void)stopTimer
{
    if (notifyTimerASKAC) {
        [notifyTimerASKAC invalidate];
        notifyTimerASKAC = nil;
    }
}
- (IBAction)btnClosePop:(id)sender {
    [popoverAlertController dismissPopoverAnimated:YES];
    popoverAlertController = nil;
}
-(IBAction)Vibrate {
    NSURL *fileURL;
    NSString *sound = [self.member.ss lastPathComponent];
    if ([sound isEqualToString:@"sounds-chime.mp3"]) {
        NSArray *explode = [sound componentsSeparatedByString:@"."];
        NSString *soundType = [explode objectAtIndex:[explode count]-1];
        NSString *soundName = [sound stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@".%@",soundType] withString:@""];
        
        
        fileURL = [NSURL fileURLWithPath:
                          [[NSBundle mainBundle] pathForResource:soundName ofType:soundType]];
    }
    else{
       fileURL = [NSURL URLWithString:self.member.ss];
    }
  
    CFURLRef url = (CFURLRef) CFBridgingRetain(fileURL);
    SystemSoundID PlaySoundID;
    OSStatus errorCode  = AudioServicesCreateSystemSoundID(url,&PlaySoundID);
//    NSLog(@"ERROR %d: Failed to initialize UI audio file %@", (int)errorCode, url);
    AudioServicesPlaySystemSound(PlaySoundID);
    AudioServicesPlayAlertSound(PlaySoundID);
    
//    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    if (errorCode) {
        DDLogError(@"1.ERROR %d: Failed to initialize UI audio file %@  :  %d", (int)errorCode,fileURL,(unsigned int)PlaySoundID);
    }
}
-(void)notiBillTable:(NSDictionary *)data{
    [LoadMenuAll retrieveData:@"" success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *dataListTables = [NSArray arrayWithArray:dataTables];
            NSDictionary *filteredArray = (NSDictionary *)[NSDictionary searchDictionary:data[@"t_id"]
                                                                                andvalue:@"(id = %@)"
                                                                                 andData:dataListTables];
            [LoadMenuAll loadDataOpenTable:filteredArray[@"id"] success:^(NSDictionary *dictionary) {
                UINavigationController *navcon = (UINavigationController *)self.window.rootViewController;
                [SelectBill selectBill:navcon andData:dictionary andDataTable:filteredArray fromRole:nil];
            }];
        });
    }];
}
- (void)dealloc {
    popoverAlertController = nil;
}
-(NSString *)getStatus:(int)num{
    switch (num) {
              // ไม่ต้อง
        case 1: // เปิดโต๊ะ (ไม่ส่ง)
        case 2: // ปิดโต๊ะ cashier เช็คบิล => staff อัพเดท table status
        case 3: // ย้ายโต๊ะ (ไม่ส่ง)
             return @"โต๊ะ";
            // kitchen
        case 10: // staff สั่งอาหาร => kitchen เพิ่มหรืออัพเดทรายการ order
        case 11: // staff ยกเลิกรายการอาหาร => kitchen ลบรายการนั้นออก
            return @"kitchen";
            // staff
        case 20: // admin อนุมัติโปรโมชั่น => staff แสดง popup notification และแสดงปุ่ม promotion นั้น
            return @"staff";
        case 12: // kitchen เปลี่ยนสถานะรายการอาหาร เช่น เสร็จแล้ว ยกเลิก => staff ไม่ต้องทำอะไรถ้าเป็นการยกเลิก staff แสดง popup notification และอัพเดทรายการอาหารนั้น
            return @"reloadDataOrder";
        case 13: // kitchen ยกเลิกรายการอาหาร => staff แสดง popup notification และอัพเดทรายการอาหารนั้น
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            if([[prefs objectForKey:@"role"] isEqualToString:@"staff"]){
                return @"staff";
            }
            else{
                return @"kitchen";
            }
        }
        case 60:  // kitchen block menu => staff อัพเดทรายการเมนู
        {
            [self updateMenu:nil];
            return @"";
        }
             // admin
        case 21: // staff   ร้องขออนุมัติโปรโมชัน => admin แสดง notification หรือ อัพเทด list pending
        case 31: // checker ร้องขออนุมัติใบสั่งสินค้า => admin แสดง notification หรือ อัพเทด list pending
        case 40: // cashier ส่งยอด => admin แสดง notification หรือ อัพเทด list pending
        case 41: // cashier ร้องขอการอนุมัติส่วนลดเพิ่มเติม => admin แสดง notification หรือ อัพเทด list pending
        case 50: // checker ส่งรายงาน stock => admin แสดง notification หรือ อัพเทด list pending
            return @"admin";
           // checker
        case 30: // admin อนุบัติใบสั่งสินค้า => checker แสดง notification หรืออัพเทด list invoice
            return @"checker";
        case 42: // admin อนุบัติใบสั่งสินค้า => checker แสดง notification หรืออัพเทด list invoice
            return @"cashier";
        default:
            return @"";
    }
}
//- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
//{
//    UIApplicationState state = [application applicationState];
//    if (state == UIApplicationStateActive) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"มีการอัพเดทข้อมูลเมนู"
//                                                        message:@""
//                                                       delegate:self cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//        [self updateMenu:nil];
//    }
//    
//    // Set icon badge number to zero
//    application.applicationIconBadgeNumber = 0;
//}

-(void)updateMenu:(id)sender{
    [LoadMenuAll checkDataMenu:@"0"];
}
-(void)switchTolanguage:(NSBundle *)bnd{
    @try {
        
//        
//        NSBundle *bnd = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:lang ofType:@"lproj"]];
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:bnd];
        UIViewController *initViewController = [storyBoard instantiateInitialViewController];
        [self.window makeKeyAndVisible];
        [self.window setRootViewController:initViewController];
    }
    @catch (NSException *exception) {
        DDLogError(@" error switchTolanguage");
    }
}

@end
