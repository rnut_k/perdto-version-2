//
//  OrderForOutcheckerController.h
//  Wunjun Resturant
//
//  Created by Nites on 4/26/2559 BE.
//  Copyright © 2559 AgeNt. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "Member.h"

@interface OrderForOutcheckerController : UIViewController <UITableViewDelegate, UITableViewDataSource>



@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonItem;
@property (weak, nonatomic) IBOutlet UINavigationItem *barNavigationItem;
@property (weak, nonatomic) IBOutlet UINavigationBar *barNavigationBar;
@property (nonatomic,strong) Member *member;
@property (nonatomic,strong) NSDictionary *data;
@property (nonatomic,strong) NSArray *kitchens;
@property (nonatomic,strong) NSString *outchecker_id;
@property (weak, nonatomic) IBOutlet UIScrollView *rootView;
@property (nonatomic,strong ) IBOutlet UITableView *tableList;

- (IBAction)clickBarButtonItem:(id)sender;
- (void)updateTable:(NSDictionary*)data;
@end