//
//  AppDelegate.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"
#import "MenuDB.h"
#import "RoleViewController.h"
#import "InitViewController.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) BOOL shouldRotate;
@property (nonatomic, retain) id mainViewController;

+ (void)connectWebSocketActiveAppNoti;
- (IBAction)btnClosePop:(id)sender ;

-(void)switchTolanguage:(NSBundle *)bnd;

@end

