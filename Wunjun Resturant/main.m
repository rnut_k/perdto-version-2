//
//  main.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        @try {
            
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
            
        } @catch (NSException *e) {
            DDLogError(@"CRASH: %@", e);
            DDLogError(@"Stack Trace: %@", [e callStackSymbols]);
        }
    }
}
