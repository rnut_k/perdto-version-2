//
//  ButtonMenuView.h
// Resturant
//
//  Created by AgeNt on 2/16/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPager.h"

@interface ButtonMenuView : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutButtonFood;
@property (strong, nonatomic) id tableSingleBillViewController;
@property (nonatomic) BOOL notClike;


@property (weak, nonatomic) IBOutlet UIButton *labelMakeOrder;
@property (weak, nonatomic) IBOutlet UIButton *labelOrderDrink;
@property (weak, nonatomic) IBOutlet UIButton *labelPromotion;


- (IBAction)btnFood:(id)sender;
- (IBAction)btnBeverage:(id)sender ;
- (IBAction)btnDirnk:(id)sender ;
- (IBAction)btnPromotiom:(id)sender ;
@end
