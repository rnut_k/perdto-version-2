//
//  PopupView.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "PopupView.h"
#import "GlobalBill.h"
#import "check.h"

@interface PopupView(){    
    PJRSignatureView *signatureView;
    NSMutableArray *indexKey;
}

@end
@implementation PopupView
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    if (self.tableSingleBillViewController == nil)
        indexKey = [[NSMutableArray alloc]initWithArray:[self.drinkGrirlsReportViewController.drinkMenu allKeys]];
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    
}
- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle{
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    NSAttributedString *titlePop  = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"รายการ", nil) andColor:[UIColor whiteColor]];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = titlePop;
//    titleLabel.textColor = [UIColor whiteColor];
    
    NSAttributedString *buttonTitle  = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ปิด", nil) andColor:[UIColor whiteColor]];
    NSMutableArray *arr = [NSMutableArray arrayWithObject:titleLabel];
    
    if (self.drinkGrirlsReportViewController != nil) {
        
        NSAttributedString *lineOne      = [self createNSAttributedString:paragraphStyle and:kDrinkOne andColor:[UIColor whiteColor]];
        NSAttributedString *lineTwo      = [self createNSAttributedString:paragraphStyle and:kDrinkTwo andColor:[UIColor whiteColor]];
        NSAttributedString *buttonThree  = [self createNSAttributedString:paragraphStyle and:kDrinkThree andColor:[UIColor whiteColor]];
        
        CNPPopupButton *buttonItem          = [self createButton:buttonTitle];
        CNPPopupButton *buttonAddTable      = [self createButton:lineOne];
        CNPPopupButton *buttonMoveTable     = [self createButton:lineTwo];
        CNPPopupButton *buttonSeparateBill  = [self createButton:buttonThree];
        
        if ([self.status isEqual:@"0"]) {
            NSAttributedString *title = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ไม่ว่าง", nil) andColor:[UIColor ht_leadColor]];
            UILabel *titleLabel = [self setLabel:title];
            buttonItem.layer.cornerRadius = 10;
            buttonItem.backgroundColor = [UIColor ht_pinkRoseColor];
            self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel,buttonItem]];
            self.popupController.theme = [CNPPopupTheme defaultTheme];
        }
        else{
           
            [arr addObjectsFromArray:@[buttonAddTable,buttonMoveTable,buttonSeparateBill,buttonItem]];
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme = [CNPPopupTheme defaultTheme];
            self.popupController.theme.backgroundColor = [UIColor clearColor];
        }
        self.popupController.delegate = self.drinkGrirlsReportViewController;
    }
    else if ([[self.tableDataBillViewController.selfSuper tableSuperClass] cashTableStatus] != nil){
        
        NSArray *listButton;
        if ([self.status intValue] != 1) {
            listButton = [self cretaButtonPopup:@[AMLocalizedString(@"แก้ไขจำนวน", nil),AMLocalizedString(@"คืนของ(ลบรายการ)", nil),AMLocalizedString(@"ไม่", nil)] andPara:paragraphStyle];
        }
        else {
            listButton = [self cretaButtonPopup:@[AMLocalizedString(@"ทำเสร็จ", nil),AMLocalizedString(@"คืนของ(ลบรายการ)", nil),AMLocalizedString(@"ไม่", nil)] andPara:paragraphStyle];
        }
        [arr addObjectsFromArray:listButton];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = [[self.tableDataBillViewController.selfSuper tableSuperClass] cashTableStatus];
    }
    else if (self.foodItemsOrderViewController != nil){
        NSAttributedString *title2   = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ต้องการที่จะลบ", nil) andColor:[UIColor ht_leadColor]];
        titleLabel = [self setLabel:title2];
        NSString *titleDe = self.foodItemsOrderViewController.deleteTag[@"title"];
        NSAttributedString *lineTwoTitle = [[NSAttributedString alloc] initWithString:titleDe
                                                                           attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor ht_leadColor], NSParagraphStyleAttributeName : paragraphStyle}];
        UILabel  * lineLable = [self setLabel:lineTwoTitle];
        NSMutableArray *arr = [NSMutableArray arrayWithObjects:titleLabel,lineLable,nil];
        [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"ไม่", nil),AMLocalizedString(@"ใช่", nil)]
                                                andPara:paragraphStyle]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.delegate = self.foodItemsOrderViewController;
    }
    else if (self.tableAllZoneCollectionView != nil){
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        if (self.tableAllZoneCollectionView.numMode == 0) {
            [arr addObjectsFromArray:[self cretaButtonPopup:@[kTableOpen,kTableOpenSum,AMLocalizedString(@"ปิด", nil)]
                                                    andPara:paragraphStyle]];
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme.backgroundColor = [UIColor clearColor];
        }
        else if (self.tanPopup == 44 ) {
            arr = [NSMutableArray array];
            NSAttributedString *title1 = [self createNSAttributedString:paragraphStyle
                                                                    and:AMLocalizedString(@"แสดง QR Code", nil)
                                                               andColor:[UIColor ht_leadColor]];
            UILabel *lineLabel = [self setLabel:title1];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:self.imagePop];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [imageView setNeedsLayout];
            
            [arr addObject:lineLabel];
            [arr addObject:imageView];
            [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"ปิด", nil)]
                                                    andPara:paragraphStyle]];
            
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        }
        else{
            [arr addObjectsFromArray:[self cretaButtonPopup:@[kTableMove,kTablesPlitBill,AMLocalizedString(@"แสดง QR Code", nil),AMLocalizedString(@"ปิด", nil)]
                                                    andPara:paragraphStyle]];
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme.backgroundColor = [UIColor clearColor];
           
        }


        self.popupController.delegate = self.tableAllZoneCollectionView;
    }
    else if (self.tableDataBillViewController != nil && self.tanPopup == 11) {
        NSAttributedString *lineTwo      = [self createNSAttributedString:paragraphStyle and:kTitleSignature andColor:[UIColor ht_leadColor]];
        
        CNPPopupButton *buttonItem          = [self createButton:buttonTitle];
        CNPPopupButton *buttonMoveTable     = [self createButton:lineTwo];
        [arr addObjectsFromArray:@[buttonMoveTable,buttonItem]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.tableDataBillViewController;
    }
    else if (self.tableReportDrinkGirls != nil) {
        CNPPopupButton *button1;
        CNPPopupButton *button2;
        if (self.tanPopup == 11) {
            NSAttributedString *title1 = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"แสดงลายเซ็นต์",nil) andColor:[UIColor whiteColor]];
            button1  = [self createButton:title1];
        }
        else{
            NSAttributedString *titlestart = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"แสดงลายเซ็นต์ start",nil) andColor:[UIColor whiteColor]];
            NSAttributedString *titlestop = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"แสดงลายเซ็นต์ stop",nil) andColor:[UIColor whiteColor]];
            button1  = [self createButton:titlestart];
            button2  = [self createButton:titlestop];
        }
        NSAttributedString *titleCen = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ไม่",nil) andColor:[UIColor whiteColor]];
        CNPPopupButton *button3  = [self createButton:titleCen];
        [arr addObjectsFromArray:(self.tanPopup != 11)?@[button1,button2,button3]:@[button1,button3]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.tableReportDrinkGirls;
    }
    else if (self.tableSingleBillViewController != nil) {
//        if (self.tanPopup == 33 ) {
//            arr = [NSMutableArray array];
//            NSAttributedString *title1 = [self createNSAttributedString:paragraphStyle
//                                                                    and:AMLocalizedString(@"รายการ", nil)
//                                                               andColor:[UIColor ht_leadColor]];
//            UILabel *lineLabel = [self setLabel:title1];
//            [arr addObject:lineLabel];
//            [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"เช็คบิล", nil),
//                                                              AMLocalizedString(@"แสดง QR Code", nil)]
//                                                    andPara:paragraphStyle]];
//            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
//            self.popupController.theme.backgroundColor = [UIColor clearColor];
//        }
         if (self.tanPopup == 44 ) {
            arr = [NSMutableArray array];
            NSAttributedString *title1 = [self createNSAttributedString:paragraphStyle
                                                                    and:AMLocalizedString(@"แสดง QR Code", nil)
                                                               andColor:[UIColor ht_leadColor]];
            UILabel *lineLabel = [self setLabel:title1];
            UIImageView *imageView = [[UIImageView alloc] initWithImage:self.imagePop];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            [imageView setNeedsLayout];
            
            [arr addObject:lineLabel];
            [arr addObject:imageView];
            [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"ปิด", nil)]
                                                    andPara:paragraphStyle]];
           
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme = [CNPPopupTheme defaultTheme];
        }
        else if ([Check checkNull:self.status]) {
            NSArray *arrBut = @[AMLocalizedString(@"คูปอง",nil),
                                AMLocalizedString(@"ขอส่วนลดเพิ่ม",nil),
                                AMLocalizedString(@"แก้ไขค่าบริการ",nil),
                                AMLocalizedString(@"พิมพ์บิลเก็บเงิน",nil),
                                AMLocalizedString(@"พิมพ์บิลย่อยและชำระเงิน",nil),
                                AMLocalizedString(@"ไม่",nil)];
            [arr addObjectsFromArray:[self cretaButtonPopup:arrBut andPara:paragraphStyle]];
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme.backgroundColor = [UIColor clearColor];
        }
        else{
            arr = [NSMutableArray array];
            NSAttributedString *title1      = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"แจ้งเตือน!", nil) andColor:[UIColor ht_leadColor]];
            NSAttributedString *lineTwoTitle = [self createNSAttributedString:paragraphStyle and:self.titlePop andColor:[UIColor ht_leadColor]];
            UILabel *lineLabel = [self setLabel:title1];
            UILabel *lineLabel1 = [self setLabel:lineTwoTitle];
            [arr addObject:lineLabel];
            [arr addObject:lineLabel1];
            [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"ใช่", nil),
                                                              AMLocalizedString(@"ไม่", nil)]
                                                    andPara:paragraphStyle]];
            
            self.popupController = [[CNPPopupController alloc] initWithContents:arr];
            self.popupController.theme = [CNPPopupTheme defaultTheme];
        }
        self.popupController.delegate = self.tableSingleBillViewController;
    }
    else if (self.ordersListViewController != nil) {
        [arr addObjectsFromArray:[self cretaButtonPopup:@[AMLocalizedString(@"ไม่เช็ค",nil),
                                                          AMLocalizedString(@"เช็คเมนูซ้ำ",nil),
                                                          AMLocalizedString(@"เช็คโต๊ะซ้ำ",nil)]
                                                andPara:paragraphStyle]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.ordersListViewController;
    }
    else if (self.printerCreateController != nil) {
        NSArray * arrBut;
        NSAttributedString *lineTwoTitle;
        
        if (self.tanPopup == 1) {
            arrBut = @[@"58",@"80"];
            lineTwoTitle = [self createNSAttributedString:paragraphStyle
                                                      and:AMLocalizedString(@"เลือกขนาดกระดาษ",nil)
                                                 andColor:[UIColor whiteColor]];
        }
        else if (self.tanPopup == 2){
            arrBut = @[@"EPSON",@"Other"];
            lineTwoTitle = [self createNSAttributedString:paragraphStyle
                                                      and:AMLocalizedString(@"เลือกยี่ห้อเครื่องปริ้นเตอร์",nil)
                                                 andColor:[UIColor whiteColor]];
        }

        UILabel *lineLabel = [self setLabel:lineTwoTitle];
        [arr addObject:lineLabel];
        [arr addObjectsFromArray:[self cretaButtonPopup:arrBut andPara:paragraphStyle]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.printerCreateController;
    }
    else if (self.printerSetingViewController != nil) {
        NSArray * arrBut = @[@"Test Printer",@"Delete Printer"];
        if (self.printerSetingViewController.isLanguage) {
            arrBut = @[@"TH",@"EN"];
        }
        
        [arr addObjectsFromArray:[self cretaButtonPopup:arrBut andPara:paragraphStyle]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.printerSetingViewController;
    }
    else if (self.tablesViewController != nil) {
        NSArray * arrBut = [NSArray arrayWithArray:self.tablesViewController.dataListItemOrderPop];
        [arr addObjectsFromArray:[self cretaButtonPopup:arrBut andPara:paragraphStyle]];
         self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.tablesViewController;
    }
    else if (self.cashSpecifyTheAmount != nil) {
        NSArray * arrBut = @[@"Cash",@"Credit"];
        [arr addObjectsFromArray:[self cretaButtonPopup:arrBut andPara:paragraphStyle]];
        self.popupController = [[CNPPopupController alloc] initWithContents:arr];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.backgroundColor = [UIColor clearColor];
        self.popupController.delegate = self.cashSpecifyTheAmount;
    }
    
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromBottom;
    [self.popupController presentPopupControllerAnimated:YES];
}
-(NSArray *)cretaButtonPopup:(NSArray *)listTitle andPara:(NSMutableParagraphStyle *)paragraphStyle {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSString *tite in listTitle) {
        NSAttributedString *titleCen = [self createNSAttributedString:paragraphStyle and:tite andColor:[UIColor whiteColor]];
        CNPPopupButton *button  = [self createButton:titleCen];
        [arr addObject:button];
    }
    return [NSArray arrayWithArray:[arr copy]];
}
-(CNPPopupButton *)createButton:(NSAttributedString *)title{    
    UIColor *col =  [UIColor ht_silverColor];
    if([title isEqual:AMLocalizedString(@"ใช่", nil)]){
        col = [UIColor colorWithRed:147.0/255.0 green:198.0/255.0 blue:43.0/255.0 alpha:1.0];
    }
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0,0,250,40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:[title string] forState:UIControlStateNormal];
    button.backgroundColor = col;
    button.titleLabel.textColor = [UIColor whiteColor];
    [button.layer setCornerRadius:10];
    
    button.selectionHandler = ^(CNPPopupButton *button){
        [self popupController:self.popupController didDismissWithButtonTitle:button.titleLabel.text];
    };
    return button;
}
-(NSAttributedString *)createNSAttributedString:(NSMutableParagraphStyle *)paragraphStyle  and:(NSString *) title andColor:(UIColor *)color{
    
    UIColor *col = color;
    NSAttributedString *buttonTitle = [[NSAttributedString alloc] initWithString:title
                                                                      attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:18],
                                                                                   NSForegroundColorAttributeName : color, 
                                                                                   NSParagraphStyleAttributeName : paragraphStyle}];
    return buttonTitle;
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    
    if (self.drinkGrirlsReportViewController != nil) {
        [self.drinkGrirlsReportViewController setTypeDr:title];
        if (![title  isEqual: @"ปิด"] &&  title != nil) {
            [self createPopoverDrinkMenu:self.drinkGrirlsReportViewController];
        }
    }
    else if (self.foodItemsOrderViewController != nil) {
        [self.foodItemsOrderViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.tableAllZoneCollectionView != nil){
         [self.tableAllZoneCollectionView popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.tableDataBillViewController != nil){
        [self.tableDataBillViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.tableReportDrinkGirls != nil){
        [self.tableReportDrinkGirls popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.tableSingleBillViewController != nil){
        [self.tableSingleBillViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.ordersListViewController != nil) {
        [self.ordersListViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.printerCreateController != nil) {
        [self.printerCreateController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.printerSetingViewController != nil) {
        [self.printerSetingViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.tablesViewController != nil) {
         [self.tablesViewController popupController:controller didDismissWithButtonTitle:title];
    }
    else if (self.cashSpecifyTheAmount != nil) {
        [self.cashSpecifyTheAmount popupController:controller didDismissWithButtonTitle:title];
    }
    [self.popupController dismissPopupControllerAnimated:YES];
}
-(void) createPopoverSing:(UIViewController *)view{
//    
    UIViewController* contentViewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"SignatureImage"];
    contentViewController.view.layer.masksToBounds = YES;
    
    contentViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    
    self.wyPopoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    self.wyPopoverController.delegate = self;
    [self.wyPopoverController presentPopoverFromRect:CGRectZero
                                       inView:view.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    
    for (UIView *_vi in  [contentViewController.view subviews]) {
        if ([_vi isMemberOfClass:[PJRSignatureView class]]) {
            CGSize size = _vi.bounds.size;
            signatureView = [[PJRSignatureView alloc] initWithFrame:CGRectMake(0,0,
                                                                               size.width-20,size.height)];
            [signatureView setBackgroundColor:[UIColor clearColor]];
            [_vi addSubview:signatureView];
        }
        else if ([_vi isKindOfClass:[UIButton class]]){
            UIButton *button  =(UIButton *)_vi;
            if (button.tag == 11) {
                [(UIButton *)_vi addTarget:self
                                    action:@selector(getImageBtnPressed:)
                          forControlEvents:UIControlEventTouchUpInside];
            }
            else{
                [(UIButton *)_vi addTarget:self
                                    action:@selector(clearImageBtnPressed:)
                          forControlEvents:UIControlEventTouchUpInside];
            }
        }
    }
}
-(void) createPopoverDrinkMenu:(UIViewController *)view{
    @try {
        UITableViewController* contentViewController = [[UITableViewController alloc]init];
        contentViewController.view.layer.masksToBounds = YES;
        contentViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
        self.wyPopoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
        self.wyPopoverController.delegate = self;
        [self.wyPopoverController presentPopoverFromRect:CGRectZero
                                                  inView:view.view
                                permittedArrowDirections:WYPopoverArrowDirectionNone
                                                animated:NO];
        [contentViewController.tableView setDelegate:self];
        [contentViewController.tableView setDataSource:self];
        [contentViewController.tableView reloadData];
    }
    @catch (NSException *exception) {
        DDLogError(@"error createPopoverDrinkMenu :%@",exception);
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.tableSingleBillViewController != nil) 
        return AMLocalizedString(@"รายการแยกบิล", nil) ;
    else
        return AMLocalizedString(@" รายการ เครื่องดื่ม ", nil);
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    @try {
        if (self.tableSingleBillViewController != nil){
            if ([self.tableSingleBillViewController isPrintSubBill]) {
                 return [self.tableSingleBillViewController.listTitleSubBill count];
            }
            return [self.tableSingleBillViewController.splitDataList count];
        }
        else
            return [self.drinkGrirlsReportViewController.drinkMenu count];
    }
    @catch (NSException *exception) {
        DDLogError(@" error %@",exception);
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        NSString *cellIden = @"cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIden];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIden];
        }
        
        if (self.tableSingleBillViewController != nil) {
             NSString *key = @"";
            if ([self.tableSingleBillViewController isPrintSubBill]) {
                NSMutableArray *arrList = [self.tableSingleBillViewController.listTitleSubBill mutableCopy];
                int num = [arrList[indexPath.row][@"subbill"] intValue];
                key = [NSString stringWithFormat:@"\t %@ %d",AMLocalizedString(@"บิลย่อยที่", nil),num+1];
            }
            else{
                 key = [NSString stringWithFormat:@"\t%@  %d",AMLocalizedString(@"บิลย่อยที่", nil),(int)indexPath.row+1];
            }
            cell.textLabel.text = key;
        }
        else{
            NSString *key = [NSString stringWithFormat:@"%@",indexKey[indexPath.row]];
            NSDictionary *data = self.drinkGrirlsReportViewController.drinkMenu[key];
            
            cell.textLabel.text = data[@"name"];
            cell.detailTextLabel.text = data[@"price"];
        }
        
        return  cell;
    }
    @catch (NSException *exception) {
        DDLogError(@" error %@",exception);
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.tableSingleBillViewController != nil) {
        if (self.cashServiceViewController != nil) {
            NSDictionary *billID = self.tableSingleBillViewController.splitDataList[indexPath.row];
            [self.cashServiceViewController selectSubbill:billID];
        }
        else{
            if ([self.tableSingleBillViewController isPrintSubBill]) {
                [self.tableSingleBillViewController printSubBillView:(int)indexPath.row];
            }
            else{
                
                NSString *billID = self.tableSingleBillViewController.splitDataList[indexPath.row];
                GlobalBill *globalBill = [GlobalBill sharedInstance];
                globalBill.indexSplitBill = billID;
                NSString *paht = [NSString stringWithFormat:@"%@",self.tableSingleBillViewController.detailTable[@"id"]];
                [self.tableSingleBillViewController setIndexSplitBill:billID];
                [TableSingleBillViewController loadDataOpenTableSing:paht];
            }
        }
        [self btnClosePop:self.wyPopoverController];
    }
    else{
        [self btnClosePop:self.wyPopoverController];
        NSString *key = [NSString stringWithFormat:@"%@",indexKey[indexPath.row]];
        NSDictionary *data = self.drinkGrirlsReportViewController.drinkMenu[key];
        self.drinkGrirlsReportViewController.typeDrinkMenu = data[@"id"];
        [self createPopoverSing:self.drinkGrirlsReportViewController];
    }
}
- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
    
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)aPopoverController
{
    return YES;
}
-(void)btnClosePop:(WYPopoverController *)sender{
    [sender dismissPopoverAnimated:YES];
}
- (IBAction)clearImageBtnPressed:(id)sender
{
    [signatureView clearSignature];
    [self.wyPopoverController dismissPopoverAnimated:YES];
}
- (IBAction)getImageBtnPressed:(id)sender
{
    UIImage *image = [signatureView getSignatureImage];
    if (self.drinkGrirlsReportViewController != nil) {
        [self.drinkGrirlsReportViewController getImageBtnPressed:image];
    }
    else if (self.tableDataBillViewController != nil) {
        [self.tableDataBillViewController getImageBtnPressed:image];
    }
    else if (self.detailOrderPopViewController != nil) {
        [self.detailOrderPopViewController getImageBtnPressed:image];
    }
    [self.wyPopoverController dismissPopoverAnimated:YES];
  
}
-(UILabel *) setLabel:(NSAttributedString *)title{
    UILabel *label = [[UILabel alloc] init];
    label.numberOfLines = 0;
    label.attributedText = title;
//    label.textColor = [UIColor whiteColor];
    return label;
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
