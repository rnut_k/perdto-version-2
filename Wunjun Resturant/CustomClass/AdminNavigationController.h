//
//  AdminNavigationController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminNavigationController : UINavigationController

@property (nonatomic,strong) NSString *active;

@end
