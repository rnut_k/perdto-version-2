//
//  CustomNavigationController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableDataBillViewController.h"

@interface CustomNavigationController : UINavigationController

@property (nonatomic ,strong)  NSDictionary *dataAllZone;
@property (nonatomic ,strong)  id dataSingleBillViewController;

@property (nonatomic ,strong) NSDictionary *detailTable;
@property (nonatomic ,strong) NSDictionary *dataListSingBill;
@property (nonatomic,strong)  NSString *billTitle;
@property (nonatomic,strong)  NSString *countAmount;
@property (nonatomic,strong)  NSString *drickAmount;
@property (nonatomic,strong)  NSString *foodAmount;
//@property (strong,nonatomic) NSMutableDictionary *sectionBill;
@property (nonatomic,strong) TableDataBillViewController  *tableDataBillView;

@property (nonatomic) BOOL blIsStockReport;
@property (nonatomic) BOOL blIsStockManege;

@property (nonatomic) BOOL notBegin;

@property (nonatomic) BOOL blNewInvoice; // สร้างใบ invoice

@property (nonatomic,strong) id selfClass;

@property (nonatomic,strong) NSString *role;
@property (nonatomic,strong) NSDictionary *viewInfo;
@property (nonatomic,strong) NSDictionary *dataIncome;

//stock submit view
@property (nonatomic,strong) NSDictionary *dataStockSubmit;

-(void) setColorBar;
@end
