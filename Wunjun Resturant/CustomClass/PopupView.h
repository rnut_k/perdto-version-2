//
//  PopupView.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNPPopupController.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "PJRSignatureView.h"
#import "LoadProcess.h"
#import "Constants.h"
#import "WYPopoverController.h"

#import "TableReportDrinkGirlsTableViewController.h"
#import "DrinkGrirlsReportViewController.h"
#import "FoodItemsOrderViewController.h"
#import "TableDataBillViewController.h"
#import "TableAllZoneCollectionView.h"
#import "DetailOrderPopViewController.h"
#import "TableSingleBillViewController.h"
#import "TablePendingViewController.h"
#import "OrdersListViewController.h"
#import "PrinterCreateController.h"
#import "PrinterSetingViewController.h"
#import "TablesViewController.h"
#import "CashSpecifyTheAmount.h"
#import "CashServiceViewController.h"

#import "CashTableStatus.h"

@interface PopupView : UITableViewController<CNPPopupControllerDelegate,WYPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) CNPPopupController  *popupController;
@property (nonatomic, strong) NSString            *status;
@property (nonatomic, strong) WYPopoverController *wyPopoverController;
@property (nonatomic        ) NSInteger           tanPopup;
@property (nonatomic        ) NSString*           titlePop;
@property (nonatomic, strong) UIImage            *imagePop;

@property (nonatomic) BOOL blDrinkGirls;

@property (nonatomic, strong) TableReportDrinkGirlsTableViewController *tableReportDrinkGirls;
@property (nonatomic, strong) DrinkGrirlsReportViewController          *drinkGrirlsReportViewController;
@property (nonatomic, strong) FoodItemsOrderViewController             *foodItemsOrderViewController;
@property (nonatomic, strong) TableDataBillViewController              *tableDataBillViewController;
@property (nonatomic ,strong) TableAllZoneCollectionView               *tableAllZoneCollectionView;
@property (nonatomic ,strong) DetailOrderPopViewController             *detailOrderPopViewController;
@property (nonatomic ,strong) TableSingleBillViewController            *tableSingleBillViewController;
@property (nonatomic ,strong) OrdersListViewController                 *ordersListViewController;
@property (nonatomic ,strong) PrinterCreateController                  *printerCreateController;
@property (nonatomic ,strong) PrinterSetingViewController              *printerSetingViewController;
@property (nonatomic ,strong) TablesViewController                     *tablesViewController;
@property (nonatomic ,strong) CashSpecifyTheAmount                     *cashSpecifyTheAmount;
@property (nonatomic ,strong) CashServiceViewController                *cashServiceViewController;

-(void) showPopupWithStyle:(CNPPopupStyle)popupStyle;
-(void) btnClosePop:(WYPopoverController *)sender;
-(void) createPopoverSing:(UIViewController *)view;
-(void) createPopoverDrinkMenu:(UIViewController *)view;
@end
