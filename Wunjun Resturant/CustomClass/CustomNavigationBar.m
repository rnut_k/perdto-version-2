//
//  CustomNavigationBar.m
//  NavigationBarChangingImage
//
//  Created by Luka Gabric on 26.11.2011..
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//


#import "CustomNavigationBar.h"


@interface CustomNavigationBar ()


@property (nonatomic, retain) UIImage *bgImg;


@end


@implementation CustomNavigationBar


@synthesize bgImg = _bgImg;


- (void)drawRect:( CGRect )rect
{
	if(_bgImg) 
        [_bgImg drawInRect:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    else 
        [super drawRect:rect];
}


- (void)setBgImage:(UIImage *)img
{
    self.bgImg = img;
    if ([self respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        [self setBackgroundImage:self.bgImg forBarMetrics:UIBarMetricsDefault];
    }
    else
    {
        [self setNeedsDisplay];
    }
}


@end