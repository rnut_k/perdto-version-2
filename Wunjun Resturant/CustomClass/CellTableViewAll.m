//
//  CellTableViewAll.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CellTableViewAll.h"
#import "Time.h"
#import "TableDataBillViewController.h"

@interface CellTableViewAll(){
    NSDictionary *dataDict;
}

@end
@implementation CellTableViewAll


//- (IBAction)btnAdjustAmount:(UIStepper *)sender {
//    UIStepper *stepper = (UIStepper *)sender;
//    NSString *adjust = [NSString stringWithFormat:@"%1.0f",stepper.value];
//    NSIndexPath *indexPath = [self getIndexPath:sender];
//    dataDict = @{@"index":indexPath,
//                 @"adjust":adjust};
//    [self postNotificationName:@"cleckAdjustAmount"];
//}
-(void)awakeFromNib{
    [super awakeFromNib];
    [self setUpLabel];
}
-(void)setUpLabel{
    _labelReport.text = AMLocalizedString(@"report", nil);
    _labelCheckStock.text = AMLocalizedString(@"checkstock", nil);
    _labelTotalBill.text = AMLocalizedString(@"total", nil);
    _mNameOrder.text = AMLocalizedString(@"total_amount_buy", nil);
    
    _labelBillTotal.text = AMLocalizedString(@"bill_total", nil);
    _labelTotal.text = AMLocalizedString(@"total", nil);
    _labelDiscountPromotion.text = AMLocalizedString(@"discount_promotion", nil);
    
    _labelProductName.text = AMLocalizedString(@"product_name", nil);
    _labelAmountInStock.text = AMLocalizedString(@"amount_in_stock", nil);
    _labelBuy.text = AMLocalizedString(@"buy", nil);
    _labelAdjust.text = AMLocalizedString(@"adjust", nil);
    
    
    
    
}
- (IBAction)clickOn:(id)sender {
     NSLog(@"clickOn");
    int num = [self.mLabel.text intValue];
    self.mLabel.text = [@(++num) stringValue];
}

- (IBAction)clickDown:(id)sender {
    NSLog(@"clickDown");
    int num = [self.mLabel.text intValue];
    num = (num != 0)?--num:0;
    self.mLabel.text = [@(num) stringValue];
    
}
- (IBAction)cilckDelete:(id)sender {
    NSLog(@"cilckDelete");
    NSIndexPath *indexPath = [self getIndexPath:sender];
    dataDict = [NSDictionary dictionaryWithObject:indexPath  forKey:@"index"];
    [self postNotificationName:@"cleckDeleteTable"];
}

- (IBAction)btnStop:(id)sender {
    NSLog(@"btnStop");
    UIButton *btn = (UIButton*)sender;
    if (btn.tag != 99) {
        NSArray *viewList = [[sender superview] subviews];
        for (UIView *vi in viewList) {
            if (vi.tag == 1) {
                UILabel *la = (UILabel *)vi;
                [la setText:[Time getTime]];
                [la setTextColor:[UIColor redColor]];
            }
        }
        
        NSIndexPath *indexPath = [self getIndexPath:sender];
        dataDict = [NSDictionary dictionaryWithObject:indexPath  forKey:@"index"];
        [self postNotificationName:@"checkStopDrink"];
    }
}
- (IBAction)btnStopOrder:(id)sender {
    NSLog(@"btnStopOrder");
    UIButton *btn = (UIButton*)sender;
    dataDict = [NSDictionary dictionaryWithObject:btn  forKey:@"index"];
    [self postNotificationName:@"checkbtnClose"];
}
- (IBAction)btnOK:(id)sender {
    NSLog(@"btnOK");
    NSIndexPath *indexPath = [self getIndexPath:sender];
    dataDict = [NSDictionary dictionaryWithObject:indexPath forKey:@"section"];
    [self postNotificationName:@"checkbtnOK"];
}
- (IBAction)btnEditOrder:(id)sender {
    NSLog(@"btnEditOrder");
    UIButton *btn = (UIButton*)sender;
    UITableViewCell *buttonCell = [self getUICell:btn];
    dataDict = [NSDictionary dictionaryWithObject:buttonCell forKey:@"section"];
    [self postNotificationName:@"checkbtnEditOrder"];
}
- (IBAction)btnClose:(id)sender {
    NSLog(@"btnClose");
    NSIndexPath *indexPath = [self getIndexPath:sender];
    dataDict = [NSDictionary dictionaryWithObject:indexPath forKey:@"section"];
    [self postNotificationName:@"checkbtnClose"];
}
- (IBAction)btnCancelOrder:(id)sender {
    NSLog(@"btnCancelOrder");
    UIButton *btn = (UIButton*)sender;
    dataDict = [NSDictionary dictionaryWithObject:btn  forKey:@"index"];
    [self postNotificationName:@"checkCancelOrder"];
}
-(void)postNotificationName:(NSString *)name{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:name
                          object:self
                        userInfo:dataDict];
}
- (NSIndexPath *)getIndexPath:(id)sender {
    UITableView *superTable = (UITableView *)self.superview.superview;
    int i = 0;
    UITableViewCell *buttonCell = (UITableViewCell*)[sender superview];
    NSIndexPath *indexPath;
    while (i < 10) {
        indexPath = [superTable indexPathForCell:buttonCell];
        if (indexPath == nil) {
            buttonCell = (UITableViewCell*)[buttonCell superview];
            i++;
        }
        else{
            break;
        }
    }
    return indexPath;
}
- (UITableViewCell *)getUICell:(id)sender {
    int i = 0;
    UITableViewCell *buttonCell = (UITableViewCell*)[sender superview];
    while (i < 10) {
        if ([buttonCell isKindOfClass:[CellTableViewAll class]]) {
            return buttonCell;
        }
        buttonCell = (UITableViewCell*)[buttonCell superview];
    }
    return buttonCell;
}
@end
