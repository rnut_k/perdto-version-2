//
//  AdminNavigationController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AdminNavigationController.h"

@interface AdminNavigationController ()

@end

@implementation AdminNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     [self.navigationBar setBarStyle:UIBarStyleBlack];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
