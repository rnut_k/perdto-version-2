//
//  CustomNavigationController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CustomNavigationController.h"
#import "TableBillMenuViewController.h"

@interface NavigationController : UINavigationController <UINavigationControllerDelegate>
@end
@implementation CustomNavigationController

- (id) initWithRootViewController:(UIViewController *) rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.delegate = self;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationBar setBarStyle:UIBarStyleBlack];


    
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    [self addImageOnTopOfTheNavigationBar];
}
-(void)addImageOnTopOfTheNavigationBar {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top_drink"]];
    CGSize targetHeight = self.navigationBar.frame.size;
    [imageView setFrame:CGRectMake(0,-22,targetHeight.width,targetHeight.height +22)];
    [self.navigationController.navigationBar setBackgroundImage:imageView.image forBarMetrics:UIBarMetricsDefault];
}
-(void) setColorBar{
    UIColor *barColour = [UIColor colorWithRed:0.0588f green:0.0627f blue:0.2f alpha:1.0f];
    UIView *colourView = [[UIView alloc] initWithFrame:CGRectMake(0.f, -20.f, 320.f, 64.f)];
    colourView.opaque = NO;
    colourView.alpha = .7f;
    colourView.backgroundColor = barColour;
    self.navigationBar.barTintColor = barColour;
    [self.navigationBar.layer insertSublayer:colourView.layer atIndex:1];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
}
@end
