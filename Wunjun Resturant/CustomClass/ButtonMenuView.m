//
//  ButtonMenuView.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/16/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "ButtonMenuView.h"
#import "FoodItemsOrderViewController.h"
#import "TablePromotiomViewController.h"
#import "TableSingleBillViewController.h"
#import "TableSubBillViewController.h"
#import "TableBeverageListViewController.h"
#import "CustomNavigationController.h"
#import "DrinkGrirlsReportViewController.h"
#import "LocalizationSystem.h"

@implementation ButtonMenuView

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setUpStaticLabel];
}
-(void)setUpStaticLabel{
    [_labelMakeOrder setTitle:AMLocalizedString(@"make_order", nil) forState:UIControlStateNormal];
    [_labelMakeOrder setTitle:AMLocalizedString(@"make_order", nil) forState:UIControlStateHighlighted];
    
    
    [_labelOrderDrink setTitle:AMLocalizedString(@"order_drink", nil) forState:UIControlStateNormal];
    [_labelOrderDrink setTitle:AMLocalizedString(@"order_drink", nil) forState:UIControlStateHighlighted];
    
    [_labelPromotion setTitle:AMLocalizedString(@"promotion", nil) forState:UIControlStateNormal];
    [_labelPromotion setTitle:AMLocalizedString(@"promotion", nil) forState:UIControlStateHighlighted];
    
    
    
    
}
- (IBAction)btnFood:(id)sender {
    if (!self.notClike) {

        FoodItemsOrderViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"FoodItemsOrderView"];
        if (self.tableSingleBillViewController != nil) {
            [_viewController setTableSingleBillViewController:self.tableSingleBillViewController];
        }
        else{
            [_viewController setTableSubBillViewController:self.tableSingleBillViewController];
        }
    
        [[self navigationController] pushViewController:_viewController animated:YES];
    }
}
- (IBAction)btnBeverage:(id)sender {
    if (!self.notClike) {

        TableBeverageListViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableBeverageListView"];
        [[self navigationController] pushViewController:_viewController animated:NO];
    }

}
- (IBAction)btnDirnk:(id)sender {
    if (!self.notClike) {

        if ([self.tableSingleBillViewController isKindOfClass:[TableSingleBillViewController class]]) {
            DrinkGrirlsReportViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"DrinkGrirlsReport"]; // edit
            [[self navigationController] pushViewController:_viewController animated:NO];
            if ([self.tableSingleBillViewController isKindOfClass:[TableSingleBillViewController class]]) {
                [_viewController setTableSingleBillViewControll:self.tableSingleBillViewController];
            }
            else{
                [_viewController setTableSubBillViewController:self.tableSingleBillViewController];
            }
        }
        else {
            DrinkGrirlsReportViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"DrinkGrirlsReport"]; // edit
//            [_viewController setSelfClass:self.tableSingleBillViewController];
            [[self navigationController] pushViewController:_viewController animated:NO];
            if ([self.tableSingleBillViewController isKindOfClass:[TableSingleBillViewController class]]) {
                [_viewController setTableSingleBillViewControll:self.tableSingleBillViewController];
            }
            else{
                [_viewController setTableSubBillViewController:self.tableSingleBillViewController];
            }
        }
    }

}
- (IBAction)btnPromotiom:(id)sender {
    if (!self.notClike){

    
        THSegmentedPager *pager;
        pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
        NSMutableArray *pages = [NSMutableArray new];
        
        for(int i = 0;i < 2;i++){
            TablePromotiomViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TablePromotiomView"];
            if (self.tableSingleBillViewController != nil) {
                [_viewController setTableSingleBillViewControll:self.tableSingleBillViewController];
            }
            else{
                [_viewController setTableSubBillViewController:self.tableSingleBillViewController];
            }
            
            if(i == 0)
            {
                [_viewController loadDataPromotion];
                [_viewController.mTableProductPromotion setTag:111];
                [_viewController setTitle:AMLocalizedString(@"โปรโมชั่นสินค้า", nil)];
            }
            else
            {
                [_viewController loadDataPromotionRequirePromtion];
                [_viewController.mTableProductPromotion setTag:222];
                [_viewController setTitle:AMLocalizedString(@"ขออนุมัติโปรโมชั่น", nil)];
            }
            [pages addObject:_viewController];
        }
        
        [pager setPages:pages];
        
        pager.view.frame = self.view.bounds;
        
        [self addChildViewController:pager];
        [pager didMoveToParentViewController:self];
        [pager setTitle:AMLocalizedString(@"โปรโมชั่น", nil)];
        
        [[self navigationController] pushViewController:pager animated:NO];
    }
}
@end
