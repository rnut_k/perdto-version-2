//
//  CustomNavigationBar.h
//  NavigationBarChangingImage
//
//  Created by Luka Gabric on 26.11.2011..
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//


@interface CustomNavigationBar : UINavigationBar


- (void)setBgImage:(UIImage *)img;


@end