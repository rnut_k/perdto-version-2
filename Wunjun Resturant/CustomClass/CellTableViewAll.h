//
//  CellTableViewAll.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellTableViewAll : UITableViewCell<UITextViewDelegate>

@property (nonatomic ,strong) IBOutlet UIView *mView;
@property (nonatomic ,strong) IBOutlet UITextView *mTextView;
@property (strong, nonatomic) IBOutlet UILabel *mCountOrder;
@property (strong, nonatomic) IBOutlet UILabel *mLabel;
@property (strong, nonatomic) IBOutlet UILabel *mCount;
@property (weak, nonatomic) IBOutlet UILabel *mTotalFood;
@property (weak, nonatomic) IBOutlet UILabel *mDiscountFood;
@property (weak, nonatomic) IBOutlet UILabel *mTotalAllFood;
@property (weak, nonatomic) IBOutlet UILabel *mTimeDrink;
@property (weak, nonatomic) IBOutlet UIImageView *mImageBackgroud;
@property (weak, nonatomic) IBOutlet UITextField *mCountEdit;
@property (weak, nonatomic) IBOutlet UITextField *mPasswordEdit;
@property (weak, nonatomic) IBOutlet UIButton *mBtnEdit;
@property (weak, nonatomic) IBOutlet UIImageView *mUporDown;
@property (weak, nonatomic) IBOutlet UILabel *mDifference;
@property (weak, nonatomic) IBOutlet UILabel *mOption;
@property (weak, nonatomic) IBOutlet UILabel *mTypeOrderBuy;
@property (weak, nonatomic) IBOutlet UILabel *mNameOrder;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (strong, nonatomic) IBOutlet UILabel *mLabelBy;
@property (strong, nonatomic) IBOutlet UILabel *mLabelRoleBy;
@property (weak, nonatomic) IBOutlet UIImageView *mImageBy;

@property (weak, nonatomic) IBOutlet UILabel *mTextVat;
@property (weak, nonatomic) IBOutlet UILabel *mNumVat;
@property (weak, nonatomic) IBOutlet UILabel *mCountBill;
@property (weak, nonatomic) IBOutlet UILabel *mNameTable;

// service
@property (weak, nonatomic) IBOutlet UILabel *mServiceTotal;
@property (weak, nonatomic) IBOutlet UILabel *mServicePer;

@property (weak, nonatomic) IBOutlet UILabel *mDate;
@property (weak, nonatomic) IBOutlet UILabel *mDaily;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutWidthImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutHightPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelCheckStock;


//static label
@property (weak, nonatomic) IBOutlet UILabel *labelReport;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalBill;
//static label -> bill

@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UILabel *labelDiscountPromotion;
@property (weak, nonatomic) IBOutlet UILabel *labelBillTotal;


//static label -> crate order
@property (weak, nonatomic) IBOutlet UILabel *labelProductName;
@property (weak, nonatomic) IBOutlet UILabel *labelAmountInStock;
@property (weak, nonatomic) IBOutlet UILabel *labelBuy;
@property (weak, nonatomic) IBOutlet UILabel *labelAdjust;
//- (IBAction)btnAdjustAmount:(UIStepper *)sender;
- (IBAction)clickOn:(id)sender;
- (IBAction)clickDown:(id)sender;
- (IBAction)cilckDelete:(id)sender;
- (IBAction)btnStop:(id)sender;
- (IBAction)btnCancelOrder:(id)sender ;
- (IBAction)btnOK:(id)sender;
- (IBAction)btnClose:(id)sender;
- (IBAction)btnStopOrder:(id)sender;
- (IBAction)btnEditOrder:(id)sender ;
@end
