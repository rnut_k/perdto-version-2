//
//  OrderListKitchenViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "OrderListKitchenViewController.h"
#import "PrintQuenceTableViewController.h"

#import "DeviceData.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "OrdersKitchen.h"
#import "UIColor+HTColor.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "OrdersKitchenTableViewCell.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "NSString+GetString.h"
#import "Check.h"
#import "FoodOptionSum.h"

@interface OrderListKitchenViewController (){
    LoadProcess *loadProgress;
    NSMutableArray *rowData;
    Member *member;
    MenuDB *menuDB;
    NSDictionary *menuData;
    NSDictionary *tableData;
    NSDictionary *dStatusData;
    NSDictionary *dStatusAction;
    NSString *maxPrevOrderId;
    UITableView *tableList;
    OrdersKitchen *oKit;
    
    //column width
    float tableH;
    float cImgW;
    float cIdW;
    float cAcW;
    float avaiW;
    float cNameW;
    float cOptionW;
    float cMentW;
    float cPriceW;
    float cTabW;
    float cNW;
    
    
    NSDictionary *dataPrintTemp;
    __block NSMutableArray *listMenu;
    
    int typePrintPop;
    int idxPop;
    NSString *deliverPop;
    
    
    BOOL blOnePrint;
    
}
@property (nonatomic, strong) UILongPressGestureRecognizer *lpgr;
@end

@implementation OrderListKitchenViewController
@synthesize tableList,selectKitchen;
- (void)viewDidLoad {
    [super viewDidLoad];
    loadProgress = [LoadProcess sharedInstance];
    member = [Member getInstance];
    [member loadData];
    
    menuData = [[NSDictionary alloc]init];
    tableData = [[NSDictionary alloc]init];
    dStatusData = @{
                    @"1":@{
                           @"title":AMLocalizedString(@"รอการทำ", nil),
                           @"action":AMLocalizedString(@"หยุดทำ", nil),
                           @"color":[UIColor ht_sunflowerColor]
                            },
                    @"2":@{
                            @"title":AMLocalizedString(@"กำลังทำ", nil),
                            @"action":AMLocalizedString(@"เริ่มทำ",nil),
                            @"color":[UIColor ht_blueJeansDarkColor]
                            },
                    @"3":@{
                            @"title":AMLocalizedString(@"รอนำเซิร์ฟ", nil),
                            @"action":AMLocalizedString(@"เริ่มทำ", nil),//@"action":@"ทำเสร็จแล้ว",
                            @"color":[UIColor ht_sunflowerColor]
                            },
                    @"4":@{
                            @"title":AMLocalizedString(@"ถูกยกเลิก", nil),
                            @"action":AMLocalizedString(@"ยกเลิกรายการนี้", nil),
                            @"color":[UIColor grayColor]
                            },
                    @"5":@{
                            @"title":AMLocalizedString(@"นำเสิร์ฟแล้ว", nil),
                            @"action":AMLocalizedString(@"นำเสิร์ฟ", nil),
                            @"color":[UIColor ht_emeraldColor]
                            },
                    };
    
    dStatusAction = @{
                      @"1":@[@"2",@"3",@"5",@"4"],
                      @"2":@[@"3",@"4"],
                      @"3":@[@"5"]
                      };
//    dStatusAction = @{
//                      @"1":@[@"1",@"2",@"3",@"5",@"4"],
//                      @"2":@[@"1",@"2",@"3",@"4"],
//                      @"3":@[@"5"]
//                      };
    
#pragma mark create tab
    tableH = 80;
    cImgW = tableH;
    
    oKit = [OrdersKitchen getInstance];
    
    NSLog(self.kitchenId);
    MenuDB *menuDB = [MenuDB getInstance];
    [menuDB loadData:@"menu_0"];
    NSDictionary *data = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    
    for(id key in [data objectForKey:@"kitchen"]){
        
        if([self.kitchenId isEqualToString:data[@"kitchen"][key][@"id"] ]){
            selectKitchen = data[@"kitchen"][key];
        }
    }
    
    tableList = [[UITableView alloc] init];
    [self.view addSubview:tableList];
    tableList.delegate = self;
    tableList.dataSource = self;
    [self fixFrameTable];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
                addObserver:self selector:@selector(orientationChanged:)
                name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [tableList addSubview:refreshControl];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    rowData = [[NSMutableArray alloc]init];
    NSMutableArray *unSortRowData = [[NSMutableArray alloc]init];
    menuData = [oKit.menu mutableCopy];
    tableData = oKit.tables;
    for(NSString *k in oKit.fetchOrders[self.dStatus]){
        
        if([oKit.fetchOrders[self.dStatus][k][@"deliver_status"] isEqualToString:@"2"])
            continue;
        [unSortRowData addObject:oKit.fetchOrders[self.dStatus][k]];
    }

    if ([self.dStatus isEqualToString:@"1"]) {
        rowData = [Time sortDateAscending:unSortRowData];
    }
    else {
        BOOL reverseSort = NO;
        rowData = [[NSMutableArray alloc]initWithArray:[unSortRowData sortedArrayUsingFunction:deliverSort
                                                                                       context:&reverseSort]];
    }
    [self fixFrameTable];
    
    [self setCounter:[rowData count]];
}
-(void)updateMenu{
    
}
- (NSString *)viewControllerTitle{
    return _viewTitle;
}

- (void) orientationChanged:(NSNotification *)note{
    UIDevice * device = [UIDevice currentDevice];
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            NSLog(@"u size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"d size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"l size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"r size: %f", self.view.frame.size.width);
            break;
            
        default:
            break;
    };
    
    [self fixFrameTable];
}

- (void) fixFrameTable
{
    avaiW = self.view.frame.size.width - cImgW;
    cIdW = avaiW/8;
    cNameW = (avaiW/8) + cIdW + cImgW;
    
    cOptionW = avaiW/8;
    cMentW = avaiW/8;
    cPriceW = avaiW/8;
    cTabW = avaiW/8;
    cAcW = avaiW/8;
    cNW = avaiW/8;
    
    
//    [tableList setFrame:CGRectMake(tableList.frame.origin.x,tableList.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height - tableList.frame.origin.y)];
    [self loadViewTableList];
    [tableList reloadData];
}
- (void)loadViewTableList {
    [tableList setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    // Width constraint, half of parent view width
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableList
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0
                                                           constant:0]];
    
    // Height constraint, half of parent view height
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableList
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeHeight
                                                         multiplier:1.0
                                                           constant:0]];
    
    // Center horizontally
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableList
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                           constant:0.0]];
    
    // Center vertically
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:tableList
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1.0
                                                           constant:0.0]];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 40;
    
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    float headH = 40;
    UIView *headTable = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, headH)];
    headTable.backgroundColor = [UIColor colorWithRed:23/255.0f green:20/255.0f blue:66/255.0f alpha:1.0f];
    headTable.layer.borderWidth = 0;
    
    float nextX = 0;
    UIView *column;
    UIView *rLine;
//    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cImgW, headH)];
//    rLine = [[UIView alloc]initWithFrame:CGRectMake(cImgW - 1, 5, 1, headH - 10)];
//    rLine.backgroundColor = [UIColor ht_cloudsColor];
//    [column addSubview:rLine];
//    [headTable addSubview:column];
//    nextX += cImgW;
//    
//    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cIdW, headH)];
//    rLine = [[UIView alloc]initWithFrame:CGRectMake(cIdW - 1, 5, 1, headH - 10)];
//    rLine.backgroundColor = [UIColor ht_cloudsColor];
//    [column addSubview:rLine];
//    [headTable addSubview:column];
//    UILabel *idLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cIdW - 4, headH - 4)];
//    idLbl.lineBreakMode = NSLineBreakByWordWrapping;
//    idLbl.numberOfLines = 0;
//    idLbl.text = @"no";
//    idLbl.font = [UIFont systemFontOfSize:14];
//    idLbl.textAlignment = NSTextAlignmentCenter;
//    idLbl.textColor = [UIColor ht_cloudsColor];
//    [column addSubview:idLbl];
//    nextX += cIdW*2;
    
//    cNameW += cImgW+cIdW;
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0,cNameW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cNameW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cNameW - 4, headH - 4)];
    nameLbl.lineBreakMode = NSLineBreakByWordWrapping;
    nameLbl.numberOfLines = 0;
    nameLbl.text = AMLocalizedString(@"รายการอาหาร", nil);
    nameLbl.font = [UIFont systemFontOfSize:14];
    nameLbl.textAlignment = NSTextAlignmentCenter;
    nameLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:nameLbl];
    nextX += cNameW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cOptionW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cOptionW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *optionLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cOptionW - 4, headH - 4)];
    optionLbl.lineBreakMode = NSLineBreakByWordWrapping;
    optionLbl.numberOfLines = 0;
    optionLbl.text = AMLocalizedString(@"เพิ่มเติม", nil);
    optionLbl.font = [UIFont systemFontOfSize:14];
    optionLbl.textAlignment = NSTextAlignmentCenter;
    optionLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:optionLbl];
    nextX += cOptionW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cMentW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cMentW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *mentLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cMentW - 4, headH - 4)];
    mentLbl.lineBreakMode = NSLineBreakByWordWrapping;
    mentLbl.numberOfLines = 0;
    mentLbl.text = AMLocalizedString(@"คอมเมนต์", nil);
    mentLbl.font = [UIFont systemFontOfSize:14];
    mentLbl.textAlignment = NSTextAlignmentCenter;
    mentLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:mentLbl];
    nextX += cMentW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cPriceW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cPriceW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *priceLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cPriceW - 4, headH - 4)];
    priceLbl.lineBreakMode = NSLineBreakByWordWrapping;
    priceLbl.numberOfLines = 0;
    priceLbl.text = AMLocalizedString(@"ราคา", nil);
    priceLbl.font = [UIFont systemFontOfSize:14];
    priceLbl.textAlignment = NSTextAlignmentCenter;
    priceLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:priceLbl];
    nextX += cPriceW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cTabW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cTabW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *tabLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cTabW - 4, headH - 4)];
    tabLbl.lineBreakMode = NSLineBreakByWordWrapping;
    tabLbl.numberOfLines = 0;
    tabLbl.text = AMLocalizedString(@"โต๊ะ", nil);
    tabLbl.font = [UIFont systemFontOfSize:14];
    tabLbl.textAlignment = NSTextAlignmentCenter;
    tabLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:tabLbl];
    nextX += cTabW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cAcW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cAcW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *actLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cAcW - 4, headH - 4)];
    actLbl.lineBreakMode = NSLineBreakByWordWrapping;
    actLbl.numberOfLines = 0;
    actLbl.text = AMLocalizedString(@"สถานะ", nil);
    actLbl.font = [UIFont systemFontOfSize:14];
    actLbl.textAlignment = NSTextAlignmentCenter;
    actLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:actLbl];
    nextX += cAcW;
    
    column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cNW, headH)];
    rLine = [[UIView alloc]initWithFrame:CGRectMake(cNW - 1, 5, 1, headH - 10)];
    rLine.backgroundColor = [UIColor ht_cloudsColor];
    [column addSubview:rLine];
    [headTable addSubview:column];
    UILabel *nLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cNW - 4, headH - 4)];
    nLbl.lineBreakMode = NSLineBreakByWordWrapping;
    nLbl.numberOfLines = 0;
    nLbl.text = AMLocalizedString(@"จำนวน", nil);
    nLbl.font = [UIFont systemFontOfSize:14];
    nLbl.textAlignment = NSTextAlignmentCenter;
    nLbl.textColor = [UIColor ht_cloudsColor];
    [column addSubview:nLbl];
    nextX += cNW;
    
    return headTable;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableH;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [rowData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        }
        NSDictionary *menu = menuData[[NSString stringWithFormat:@"id_%@", rowData[indexPath.row][@"menu_id"]]];
        
        UIView *tableContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0 ,tableView.frame.size.width, 40)];
        tableContent.tag = 1;
        tableContent.backgroundColor = [UIColor clearColor];
        
        float nextX = 0;
        UIView *column;
        UIView *rLine;
        
//        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cImgW, tableH)];
//        rLine = [[UIView alloc]initWithFrame:CGRectMake(cImgW - 1, 5, 1, tableH - 10)];
//        rLine.backgroundColor = [UIColor ht_cloudsColor];
//        [column addSubview:rLine];
//        [tableContent addSubview:column];
//        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(2, 2, cImgW - 4, tableH - 4)];
//        NSString *def_img = ([menu[@"type"]intValue] == 1)?@"food_default":@"drink_default";
//        NSDictionary *picture = [NSDictionary dictionaryWithString:menu[@"image_url"]];
//        [img setImageWithURL:[NSURL URLWithString:(picture[@"th_url"]!=nil)?picture[@"th_url"]:@""] placeholderImage:[UIImage imageNamed:def_img]];
//        [column addSubview:img];
//        nextX += cImgW;
//        
//        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cIdW, tableH)];
//        rLine = [[UIView alloc]initWithFrame:CGRectMake(cIdW - 1, 5, 1, tableH - 10)];
//        rLine.backgroundColor = [UIColor ht_cloudsColor];
//        [column addSubview:rLine];
//        [tableContent addSubview:column];
//        UILabel *idLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cIdW - 4, tableH - 4)];
//        idLbl.font = [UIFont systemFontOfSize:14];
//        idLbl.lineBreakMode = NSLineBreakByWordWrapping;
//        idLbl.numberOfLines = 0;
//        idLbl.textAlignment = NSTextAlignmentCenter;
//        int newOderId = 0;
//        if(rowData[indexPath.row][@"order_id"] == nil || [rowData[indexPath.row][@"order_id"] isKindOfClass:[NSNull class]]){
//            newOderId = [rowData[indexPath.row][@"id"]intValue] - [maxPrevOrderId intValue];
//        }else{
//            newOderId = [rowData[indexPath.row][@"order_id"]intValue] - [maxPrevOrderId intValue];
//        }
//        idLbl.text = [NSString stringWithFormat:@"%d", newOderId];
//        [column addSubview:idLbl];
//        nextX += cIdW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cNameW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cNameW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(4, 2, cNameW - 8, tableH - 4)];
        nameLbl.font = [UIFont systemFontOfSize:18];
        nameLbl.lineBreakMode = NSLineBreakByWordWrapping;
        nameLbl.numberOfLines = 0;
        nameLbl.text = menu[@"name"];
        [column addSubview:nameLbl];
        nextX += cNameW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cOptionW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cOptionW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *optionLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cOptionW - 4, tableH - 4)];
        optionLbl.font = [UIFont systemFontOfSize:14];
        optionLbl.lineBreakMode = NSLineBreakByWordWrapping;
        optionLbl.numberOfLines = 0;
        NSString *option = @"";
        if (![Check checkNull:rowData[indexPath.row][@"food_option"]]) {
            option = [FoodOptionSum getFoodOption:rowData[indexPath.row][@"food_option"]];
        }

        optionLbl.text = option;
        [column addSubview:optionLbl];
        nextX += cOptionW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cMentW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cMentW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *mentLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cMentW - 4, tableH - 4)];
        mentLbl.font = [UIFont systemFontOfSize:14];
        mentLbl.lineBreakMode = NSLineBreakByWordWrapping;
        mentLbl.numberOfLines = 0;
        NSMutableString *fComment = (![rowData[indexPath.row][@"food_comment"] isKindOfClass:[NSNull class]])?rowData[indexPath.row][@"food_comment"]:@"";
        id tableIDData = rowData[indexPath.row][@"table_id"];
        NSString *table_id;
        if ([tableIDData isKindOfClass:[NSString class]]) {
            table_id = [NSString checkNull:tableIDData];
        }
        else{
            table_id = [NSString checkNull:[tableIDData firstObject]];
        }
        
        if ([table_id isEqualToString:@"0"]) {
            fComment = [NSMutableString stringWithString:@" กลับบ้าน"];
        }
        mentLbl.text = fComment;
        [column addSubview:mentLbl];
        nextX += cMentW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cPriceW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cPriceW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *priceLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cPriceW - 4, tableH - 4)];
        priceLbl.font = [UIFont systemFontOfSize:14];
        priceLbl.lineBreakMode = NSLineBreakByWordWrapping;
        priceLbl.numberOfLines = 0;
        priceLbl.textAlignment = NSTextAlignmentCenter;
        priceLbl.text = (![rowData[indexPath.row][@"price"] isKindOfClass:[NSNull class]])?rowData[indexPath.row][@"price"]:@"";
        [column addSubview:priceLbl];
        nextX += cPriceW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cTabW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cTabW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *tabLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cTabW - 4, tableH - 4)];
        tabLbl.font = [UIFont systemFontOfSize:14];
        tabLbl.lineBreakMode = NSLineBreakByWordWrapping;
        tabLbl.numberOfLines = 0;
        tabLbl.textAlignment = NSTextAlignmentCenter;
        
        
        if ([table_id isEqualToString:@"0"]) {
            NSString *bill_id = [NSString checkNull:rowData[indexPath.row][@"bill_id"]];
             tabLbl.text = [self getTitleTakehome:bill_id];
        }
        else{
            tabLbl.text = [NSString stringWithFormat:@"%@", tableData[[NSString stringWithFormat:@"id_%@",table_id]][@"label"]];
        }

        [column addSubview:tabLbl];
        nextX += cTabW;
        
        int deliver = [rowData[indexPath.row][@"deliver_status"] intValue];
        NSDictionary *dataStatus = dStatusData[[@(deliver) stringValue]];
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cAcW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cAcW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *actLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cAcW - 4, tableH - 4)];
        actLbl.font = [UIFont systemFontOfSize:14];
        actLbl.lineBreakMode = NSLineBreakByWordWrapping;
        actLbl.numberOfLines = 0;
        actLbl.textAlignment = NSTextAlignmentCenter;
        actLbl.text = dataStatus[@"title"];
        actLbl.textColor = (UIColor*)dataStatus[@"color"];
        [column addSubview:actLbl];
        nextX += cAcW;
        
        column = [[UIView alloc]initWithFrame:CGRectMake(nextX, 0, cNW, tableH)];
        rLine = [[UIView alloc]initWithFrame:CGRectMake(cNW - 1, 5, 1, tableH - 10)];
        rLine.backgroundColor = [UIColor ht_cloudsColor];
        [column addSubview:rLine];
        [tableContent addSubview:column];
        UILabel *nLbl = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, cNW - 4, tableH - 4)];
        nLbl.lineBreakMode = NSLineBreakByWordWrapping;
        nLbl.numberOfLines = 0;
        nLbl.text = rowData[indexPath.row][@"n"];
        nLbl.textAlignment = NSTextAlignmentCenter;
        [column addSubview:nLbl];
        nextX += cNW;
        
        [[cell viewWithTag:1] removeFromSuperview];
        
        [cell addSubview:tableContent];
        
        if (![self.dStatus isEqualToString:@"4"] && deliver != 5) {
            self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                      action:@selector(btnTagSelectorMenu:)];
            self.lpgr.minimumPressDuration = 1.0f;
            self.lpgr.allowableMovement = 100.0f;
            [cell addGestureRecognizer:self.lpgr];
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"cell error exception %@ at row %ld",exception, (long)indexPath.row);
        DDLogError(@"row_data : %@", rowData[indexPath.row]);
        DDLogError(@"menu_data : %@", menuData[[NSString stringWithFormat:@"id_%@", rowData[indexPath.row][@"menu_id"]]]);
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@" index %ld",(long)[indexPath  row]);
    if (!self.blClikeChecker) {
        typePrintPop = 1;
        idxPop = (int)indexPath.row;
        deliverPop = @"";
        __block OrderListKitchenViewController *selfBlock = self;
        
        NSMutableDictionary *dataRow = [NSMutableDictionary dictionaryMutableWithObject:rowData[idxPop]];
        dataPrintTemp = [NSDictionary dictionaryWithObject:dataRow];
        listMenu = [[selfBlock sortDataOrder:(int)indexPath.row] mutableCopy];
        
      
        
        if ([dataPrintTemp[@"deliver_status"]intValue] != 5) {
            if ([listMenu count] != 1) {
                [self alerViewDataPrint:listMenu];
            }
            else{
                [self actionPrint:NO];
            }
        }
    }
}
-(void) setPrintQuenceTable:(int)typePrint andIndexData:(int)idx{
    [loadProgress.loadView showWithStatus];
    listMenu = [NSMutableArray array];
    if (typePrint == 3) {
         int count = (int)[NSString checkNullConvertFloat:self.nPrint];
        if (count > [rowData count])
            count = (int)[rowData count];
        
        for (NSInteger i = 0; i < count; ++i)
            [listMenu addObject:rowData[i]];
    }
    else if (typePrint == 2){
        listMenu = [[self sortCategoryMenuTable:rowData] mutableCopy];
    }
 
    typePrintPop = typePrint;
    idxPop = idx;
    deliverPop = @"";
    
    [self actionPrint:YES];
    
//    if ([listMenu count] != 1) {
//        [self alerViewDataPrint:listMenu];
//    }
//    else{
//        [self actionPrint:NO];
//    }
}
-(void) actionPrint:(BOOL)bl{
    [loadProgress.loadView showWithStatus];
    
    blOnePrint = bl;
    __block NSMutableArray *listMenuTemp = [NSMutableArray arrayWithArray:listMenu];
    if (!bl) {
        listMenuTemp = [NSMutableArray arrayWithObject:[listMenu firstObject]];
        if (typePrintPop == 0) {
            typePrintPop = 1;
        }
        
    }
    __block OrderListKitchenViewController *selfBlock = self;
    [self setPrintQuenceTable:typePrintPop andIndexData:idxPop deliver:deliverPop success:^(BOOL completion) {
        [selfBlock forSentdata:listMenuTemp];
    }];
}
- (void) forSentdata:(NSMutableArray *)listMenu1{
    __block OrderListKitchenViewController *selfBlock = self;
    for ( NSDictionary *dataRow in listMenu1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [selfBlock loadDataPrintSent:dataRow[@"order_id"]
                                 tableId:dataRow[@"table_id"]
                                 deliver:deliverPop];
        });
    }
}
-(NSMutableArray *) sortCategoryMenuTable:(NSMutableArray *)data{
    NSMutableArray *tempArr = [NSMutableArray array];
    NSMutableArray *listData = [NSMutableArray arrayWithArray:data];
    
    while ([listData count] > 0) {
        NSDictionary *dix = [listData firstObject];
        NSString *key = dix[@"table_id"];
        NSString * value =  @"table_id = %@";
        NSMutableArray *dataOrder  = (NSMutableArray *)[NSDictionary searchDictionaryALL:key  andvalue:value andData:data];
        [tempArr addObjectsFromArray:dataOrder];
        
        [listData removeObjectsInArray:dataOrder];
    }
    return [tempArr mutableCopy];
}

-(void) setPrintQuenceTable:(int)typePrint andIndexData:(int)idx  deliver:(NSString *)deliver success:(void (^)(BOOL completion)) block{
    PrintQuenceTableViewController *printController = [[PrintQuenceTableViewController alloc] init];
    
    if (typePrint == 1) {  // กดปริ้น ที่ละใบ
        NSMutableDictionary *dataRow = [NSMutableDictionary dictionaryMutableWithObject:rowData[idx]];
        if (![deliver isEqualToString:@""]) {
             [dataRow setObject:deliver forKey:@"deliver_status"];
        }
        dataPrintTemp = [NSDictionary dictionaryWithObject:dataRow];
        [printController setDataPrint:dataRow];
    }
    [printController setOnePrint:blOnePrint];
    [printController setTypePrint:[@(typePrint) stringValue]];
    [printController setMenuData:menuData];
    [printController setRowData:rowData];
    [printController setTableData:tableData];
    [printController setSortRepeat:self.sortRepeat];
    [printController setMaxPrevBillId:self.maxPrevBillId];
    [printController setTypePrintCut:self.typePrintCut];
    [printController setNPrint:self.nPrint];
    [printController initNetworkCommunication:^(bool completion) {
        if (completion) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [printController createViewPrint];
                dispatch_async(dispatch_get_main_queue(), ^{
                    block(YES);
                });
            });
        }
        else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 999 ) {
        if(buttonIndex == 0){
            [self actionPrint:NO];
        }
        else{
            [self actionPrint:YES];
        }
    }
    else{
        if(buttonIndex == 0){
            //close ...
        }else{
            NSString *deliver = @"4";
            if (buttonIndex == 1) {
                deliver = @"5";
            }
            
            typePrintPop = 1;
            idxPop       = (int)alertView.tag;
            deliverPop   = deliver;
            [self actionPrint:NO];
        }
    }
}
-(void)loadDataPrintSent:(NSString *)orderId  tableId:(NSString *)tableID deliver:(NSString *)deliver {
    if([selectKitchen[@"outchecker_id"] isEqualToString:@"0"]){
        
        if([deliver isEqualToString:@"6"])
            deliver = @"4";
    }
    else{ // User outchecker
        if([deliver isEqualToString:@"6"])
            deliver = @"4";
        else
            deliver = @"2";
    }
    NSDictionary *parameters = @{
                                 @"code":member.code,
                                 @"order_id":orderId,
                                 @"deliver_status":deliver,
                                 @"table_id":tableID,
                                 @"outchecker_id": selectKitchen[@"outchecker_id"]
                                 };
    NSLog(@"%@",parameters);
    NSString *url = [NSString stringWithFormat:SET_DELIVER_STATUS,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue]){
            [self refresh:nil];
             NSLog(@"11 %@",parameters);
        }
        [LoadProcess dismissLoad];
    }];
}
-(NSMutableArray *)sortDataOrder:(int)idx {
    
    NSString *key ;
    NSString *value ;
    if ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คเมนูซ้ำ", nil)]) {
        key = dataPrintTemp[@"menu_id"];
        value =  @"menu_id = %@";
    }
    else if ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คโต๊ะซ้ำ", nil)]) {
        key = dataPrintTemp[@"table_id"];
        value =  @"table_id = %@";
    }
    NSMutableArray *dataOrder = [NSMutableArray array];
    if (![Check checkNull:key]) {
        dataOrder  = (NSMutableArray *)[NSDictionary searchDictionaryALL:key  andvalue:value andData:rowData];
    }
    else{
        [dataOrder addObject:[NSMutableDictionary dictionaryMutableWithObject:rowData[idx]]];
    }
    
    return dataOrder;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) setCounter:(unsigned long) counter{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationUpdateTabKitchen"
                                                        object:self
                                                      userInfo:nil];
}

#pragma mark sorting array
NSInteger deliverSort(NSDictionary *order1, NSDictionary *order2, void *reverse)
{
    NSString *deliverDatetime1 = order1[@"deliver_datetime"];
    NSString *deliverDatetime2 = order2[@"deliver_datetime"];
    
    NSComparisonResult comparison = [deliverDatetime1 localizedCaseInsensitiveCompare:deliverDatetime2];
    if (comparison == NSOrderedDescending) {
        
        deliverDatetime1 = order1[@"deliver_datetime"];
        deliverDatetime2 = order2[@"deliver_datetime"];
        comparison = [deliverDatetime1 localizedCaseInsensitiveCompare:deliverDatetime2];
    }
    
    if (*(BOOL *)reverse == YES) {
        return 0 - comparison;
    }
    return comparison;
}

#pragma mark update from socket
- (void)updateTable:(id)sender{
    NSDictionary *data = (NSDictionary*)[sender userInfo];
    int action = [data[@"data"][@"data"][@"a"] intValue];
    
    
    OrdersKitchen *oKit_ = [OrdersKitchen getInstance];
    
    if(action == 10)
    { //insert new
        NSDictionary *orders = data[@"data"][@"data"][@"d"][@"orders"];
        
        for (NSString *orderId in orders) {
            NSDictionary *specialMenu = data[@"data"][@"data"][@"d"][@"special_menu"];
            NSDictionary *newOrder = orders[orderId];
            
            if(menuData[[NSString stringWithFormat:@"id_%@",newOrder[@"menu_id"]]] == nil)
            {
                if(![specialMenu isKindOfClass:[NSNull class]] && specialMenu != nil){
                    NSString *speKey = [NSString stringWithFormat:@"id_%@",newOrder[@"menu_id"]];
                    oKit_.orders = [oKit_.orders mutableCopy];
                    [oKit_.orders setValue:specialMenu[speKey] forKey:speKey];
                    [menuData setValue:specialMenu[speKey] forKey:speKey];
                }else{
                    NSLog(@"empty menu do something ..");
                }
            }
            
            [oKit_.orders setValue:newOrder forKey:[NSString stringWithFormat:@"id_%@",orderId]];
        }
    }
    else if(action == 11)
    { //remove from list
        NSDictionary *orderData = data[@"data"][@"data"][@"d"][@"order_data"];
        NSString *orderId = orderData[@"id"];
        
        oKit_.orders = [oKit_.orders mutableCopy];
        [oKit_.orders setValue:nil forKey:[NSString stringWithFormat:@"id_%@",orderId]];
    }
    else
    {
        NSDictionary *orderData = data[@"data"][@"data"][@"d"][@"order_data"];
        NSString *orderId = orderData[@"id"];
        
        NSDictionary *curOrder = [oKit_.orders[[NSString stringWithFormat:@"id_%@",orderId]]mutableCopy];
        [curOrder setValue:[NSString stringWithFormat:@"%@", orderData[@"deliver_status"]] forKey:@"deliver_status"];
        oKit_.orders = [oKit_.orders mutableCopy];
        [oKit_.orders setValue:curOrder forKey:[NSString stringWithFormat:@"id_%@",orderId]];
    }
    [oKit_ sortOrdersData];
    
    rowData = [[NSMutableArray alloc]init];
    NSMutableArray *unSortRowData = [[NSMutableArray alloc]init];
    
    for(NSString *k in oKit.fetchOrders[self.dStatus]){
        if([oKit.fetchOrders[self.dStatus][k][@"deliver_status"] isEqualToString:@"2"])
            continue;
        
        [unSortRowData addObject:oKit.fetchOrders[self.dStatus][k]];
    }
    
    if ([self.dStatus isEqualToString:@"1"]) {
        rowData = [Time sortDateAscending:unSortRowData];
    }
    else {
        BOOL reverseSort = NO;
        rowData = [[NSMutableArray alloc]initWithArray:[unSortRowData sortedArrayUsingFunction:deliverSort
                                                                                       context:&reverseSort]];
    }

    [tableList reloadData];
    
    [self setCounter:[rowData count]];
}

- (void) refresh:(UIRefreshControl *)sender{
    NSDictionary *parameters = @{
                                 @"code":member.code,
                                 @"kitchen_id":self.kitchenId,
                                 @"deliver_status":@"0"
                                 };
    NSString *url = [NSString stringWithFormat:GET_ORDERS_KITCHEN,member.nre];
    [loadProgress.loadView showWithStatus];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue])
        {
            NSDictionary *data = json[@"data"];
//            oKit.maxPrevOrderId = data[@"max_prev_order_id"];
            oKit.orders = [data[@"orders"] mutableCopy];

            
            menuDB = [MenuDB getInstance];
            [menuDB loadData:@"menu_0"];
            NSDictionary *database = [NSDictionary dictionaryWithString:menuDB.valueMenu];
            NSDictionary *menu = [database[@"menu"] mutableCopy];
            
            for(NSString *key in data[@"menu"]){
                
                
                if(menu[key] == nil)
                    [menu setValue:data[@"menu"][key] forKey:key];
            }
            oKit.menu = menu;
            
            [oKit sortOrdersData];
            
            DeviceData *deviceData = [DeviceData getInstance];
            if([deviceData.tables[@"tables"]count] > 0){
                NSDictionary *tableDict = [[[NSDictionary alloc]init] mutableCopy];
                for(NSDictionary *_tableData in deviceData.tables[@"tables"]){
                    [tableDict setValue:_tableData forKey:[NSString stringWithFormat:@"id_%@",_tableData[@"id"]]];
                }
                oKit.tables = tableDict;
            }
            oKit.zones = deviceData.tables[@"zones"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationUpdateTabKitchen"
                                                                object:self
                                                              userInfo:nil];
            rowData = [[NSMutableArray alloc]init];
            for(NSString *k in oKit.fetchOrders[self.dStatus]){
                if([oKit.fetchOrders[self.dStatus][k][@"deliver_status"] isEqualToString:@"2"])
                    continue;
                [rowData addObject:oKit.fetchOrders[self.dStatus][k]];
            }
            [tableList reloadData];
            [LoadProcess dismissLoad];
        }
    }];
    
    [sender endRefreshing];
}

- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)gestureRecognizer{
    @try {
        if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
            CGPoint p = [gestureRecognizer locationInView:self.tableList];
            NSIndexPath *indexPath = [self.tableList indexPathForRowAtPoint:p];
            
            NSLog(@" index %ld",(long)[indexPath  row]);
            if (!self.blClikeChecker) {
                [self.tableList deselectRowAtIndexPath:indexPath animated:NO];
                NSString *deliver_status = [NSString checkNull:rowData[indexPath.row][@"deliver_status"]];
                if (![self.dStatus isEqualToString:@"3"]) {
                    if (![deliver_status isEqualToString:@"2"]) {
                        deliver_status = @"2";
                    }
                }
                listMenu = [NSMutableArray arrayWithObject:rowData[indexPath.row]];
                if([dStatusAction[deliver_status] count] > 0){
                    UIAlertView *alertViewOption = [[UIAlertView alloc]
                                                    initWithTitle:@"จัดการ"
                                                    message:@""
                                                    delegate:self
                                                    cancelButtonTitle:@"ปิด"
                                                    otherButtonTitles:nil
                                                    ];
                    
                    for(int i = 0;i < [dStatusAction[deliver_status] count];i++){
                        [alertViewOption addButtonWithTitle:dStatusData[[NSString stringWithFormat:@"%@", dStatusAction[deliver_status][i]]][@"action"]];
                    }
                    UILabel *orderId = [[UILabel alloc]init];
                    orderId.tag = 101;
                    orderId.text = rowData[indexPath.row][@"order_id"];
                    [alertViewOption addSubview:orderId];
                    
                    UILabel *tableId = [[UILabel alloc]init];
                    tableId.tag = 102;
                    tableId.text = rowData[indexPath.row][@"table_id"];
                    [alertViewOption addSubview:tableId];
                    
                    UILabel *deliverStatus = [[UILabel alloc]init];
                    deliverStatus.tag = 103;
                    deliverStatus.text = deliver_status;
                    [alertViewOption addSubview:deliverStatus];
                    
                    [alertViewOption setTag:(int)indexPath.row];
                    [alertViewOption show];
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@" error btnDetailMenuPop : %@",exception);
    }
}
-(void) alerViewDataPrint:(NSMutableArray *)dataOrder{

    NSString *title   = [NSString stringWithFormat:@"%@ %d %@",AMLocalizedString(@"มีเมนู เหมือนกัน", nil),(int)[dataOrder count],AMLocalizedString(@"รายการ คุณต้องการปริ้นพร้อมกันใช่หรือไม่", nil)];
    
    UIAlertView *alertViewOption = [[UIAlertView alloc]
                                    initWithTitle:AMLocalizedString(@"จัดการ", nil)
                                    message:title
                                    delegate:self
                                    cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                    otherButtonTitles:AMLocalizedString(@"ใช่", nil),nil
                                    ];
    
    [alertViewOption setTag:999];
    [alertViewOption show];
}

-(NSString *)getTitleTakehome:(NSString *)billId{
    
    int q = [billId intValue];
    q -= [self.maxPrevBillId intValue];
    
    return [NSString stringWithFormat:@"Q%@",[@(q) stringValue]];
}
@end
