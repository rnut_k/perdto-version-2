//
//  MamaMainViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "KVNViewController.h"
#import "LoadMenuAll.h"
#import "Time.h"
#import "DrinkGirlLogViewController.h"

@interface MamaMainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *reportTable;

@end
