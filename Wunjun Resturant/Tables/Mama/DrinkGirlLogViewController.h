//
//  DrinkGirlLogViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Time.h"
#import "UIImageView+AFNetworking.h"
#import "NSDictionary+DictionaryWithString.h"
#import "MenuDB.h"
#import "LocalizationSystem.h"

@interface DrinkGirlLogViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *GirlImg;
@property (weak, nonatomic) IBOutlet UILabel *GirlName;
@property (weak, nonatomic) IBOutlet UITableView *GirlTableLog;
@property (nonatomic,strong) NSString *role;
@property (nonatomic,strong) NSDictionary *inputData;
@end
