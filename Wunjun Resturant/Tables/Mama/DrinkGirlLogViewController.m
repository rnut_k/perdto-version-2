//
//  DrinkGirlLogViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "DrinkGirlLogViewController.h"
#import "NSString+GetString.h"
#import "NSDictionary+DictionaryWithString.h"

@interface DrinkGirlLogViewController ()
{
    NSMutableArray *rowData;
    NSMutableArray *orderData;
    NSDictionary *girlData;
    MenuDB *menuDB;
    NSDictionary *menuData;
    NSDictionary *tableUsage;
}
@end

@implementation DrinkGirlLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.automaticallyAdjustsScrollViewInsets = YES;
    NSString *imgJson = @"";
    if(self.inputData[@"orders"] != nil){
        rowData = [[NSMutableArray alloc]init];
        for(NSDictionary*data in self.inputData[@"orders"]){
            imgJson = [NSString checkNull:data[@"image_url"]];
            [rowData addObject:data];
        }
    }
    if(self.inputData[@"girl"] != nil){
        girlData = [NSDictionary dictionaryWithObject:self.inputData[@"girl"]];
    }
    if(self.inputData[@"tableUsage"] != nil){
        tableUsage = [NSDictionary dictionaryWithObject:self.inputData[@"tableUsage"]];
    }
    
    self.GirlTableLog.delegate = self;
    self.GirlTableLog.dataSource = self;
    [self.GirlTableLog reloadData];
    
    NSDictionary *imgObj = [NSDictionary dictionaryWithString:imgJson];
    [self.GirlImg setImageWithURL:[NSURL URLWithString:[NSString checkNull:imgObj[@"th_url"]]]
                 placeholderImage:[UIImage imageNamed:@"ic_mama"]];
    [self.GirlName setText:[NSString stringWithFormat:@"%@ %@", girlData[@"name"], girlData[@"lastname"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60;
    
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DrinkLogCell"];
    UIView *head = cell.contentView;
    head.backgroundColor = [UIColor blackColor];
    
    UIView *col1 = [head viewWithTag:101];
    col1.backgroundColor = [UIColor blackColor];
    UILabel *lbl1 = (UILabel*)[col1 viewWithTag:1011];
    lbl1.text = AMLocalizedString(@"โต๊ะ", nil);
    lbl1.textColor = [UIColor whiteColor];
    
    UIView *col2 = [head viewWithTag:102];
    col2.backgroundColor = [UIColor blackColor];
    UILabel *lbl2 = (UILabel*)[col2 viewWithTag:1021];
    lbl2.text = AMLocalizedString(@"เริ่ม", nil);
    lbl2.textColor = [UIColor whiteColor];
    
    UIView *col3 = [head viewWithTag:103];
    col3.backgroundColor = [UIColor blackColor];
    UILabel *lbl3 = (UILabel*)[col3 viewWithTag:1031];
    lbl3.text = AMLocalizedString(@"หยุด", nil);
    lbl3.textColor = [UIColor whiteColor];
    
    UIView *col4 = [head viewWithTag:104];
    col4.backgroundColor = [UIColor blackColor];
    UILabel *lbl4 = (UILabel*)[col4 viewWithTag:1041];
    lbl4.text = AMLocalizedString(@"จำนวนดริ๊งค์", nil);
    lbl4.textColor = [UIColor whiteColor];
    lbl4.textAlignment = NSTextAlignmentRight;
    
    return head;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [rowData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DrinkLogCell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"DrinkLogCell"];
    }
    UIView *cellContent = cell.contentView;
    
    NSDictionary *orders = rowData[indexPath.row];
    
    NSDictionary *table = tableUsage[[NSString stringWithFormat:@"usage_id_%@", orders[@"table_usage_id"]]];
    UIView *col1 = [cellContent viewWithTag:101];
    UILabel *lbl1 = (UILabel*)[col1 viewWithTag:1011];
    lbl1.text = (table != nil)?table[@"table_label"]:@"-";
    
    NSString *start_datetime = ([orders[@"start_drink_time"] isEqualToString:@"0000-00-00 00:00:00"])?[Time getTHTimeFromDate:orders[@"created_datetime"]]:[Time getTHTimeFromDate:orders[@"start_drink_time"]];
    UIView *col2 = [cellContent viewWithTag:102];
    UILabel *lbl2 = (UILabel*)[col2 viewWithTag:1021];
    lbl2.text = start_datetime;
    
    NSString *end_datetime = ([orders[@"stop_drink_time"] isEqualToString:@"0000-00-00 00:00:00"])?[Time getTHTimeFromDate:orders[@"created_datetime"]]:[Time getTHTimeFromDate:orders[@"stop_drink_time"]];
    UIView *col3 = [cellContent viewWithTag:103];
    UILabel *lbl3 = (UILabel*)[col3 viewWithTag:1031];
    lbl3.text = end_datetime;
    
    UIView *col4 = [cellContent viewWithTag:104];
    UILabel *lbl4 = (UILabel*)[col4 viewWithTag:1041];
    lbl4.text = orders[@"n"];
    lbl4.textAlignment = NSTextAlignmentRight;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
