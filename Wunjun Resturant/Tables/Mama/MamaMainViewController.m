//
//  MamaMainViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 5/29/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "MamaMainViewController.h"
#import "Check.h"

@interface MamaMainViewController ()
{
    Member *member;
    NSMutableArray *rowData;
    NSDictionary *girlData;
    NSDictionary *orderData;
    NSDictionary *tableUsage;
    KVNViewController *loadProgress;
}
@end

@implementation MamaMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigation];
    // Do any additional setup after loading the view.
    
//    adv = (AdminNavigationController *)self.parentViewController;
    self.automaticallyAdjustsScrollViewInsets = YES;
    [self setTitle:AMLocalizedString(@"รายงานพนักงานดริ๊งค์", nil)];
    self.reportTable.delegate = self;
    self.reportTable.dataSource = self;
    
    [self getReport];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.reportTable addSubview:refreshControl];
}
-(void)initNavigation{
    UIImage *image;
    CGSize size = self.navigationController.navigationBar.frame.size;
    if ([Check checkDevice]) { // iphione
        image = [UIImage imageNamed:@"top_drink@2x"];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
            image = [UIImage imageNamed:@"top_drink@3x"];
        }
    }
    else{ // ipan
        image = [UIImage imageNamed:@"top_drink@3x"];
    }
    image = [self imageWithImage:image scaledToSize:CGSizeMake(size.width,size.height+22)];
    [self.navigationController.navigationBar setBackgroundImage:image
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void) getReport{
    rowData = [[NSMutableArray alloc]init];
    girlData = [[[NSDictionary alloc]init] mutableCopy];
    orderData = [[[NSDictionary alloc]init] mutableCopy];
    member = [Member getInstance];
    
    NSDictionary *parameters = @{
                                 @"code":member.code
                                 };
    NSString *url = [NSString stringWithFormat:GET_MAMA_REPORT,member.nre];
    loadProgress  = [[KVNViewController alloc] init];
    [loadProgress showWithStatus];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if ([json[@"status"] boolValue]) {
//            NSLog(@"%@", json[@"data"]);
            if(json[@"data"][@"orders"] != nil){
                for(NSString*girl_key in json[@"data"][@"orders"]){
                    NSMutableArray *orders = [[NSMutableArray alloc]init];
                    for(NSDictionary*data in json[@"data"][@"orders"][girl_key]){
                        [orders addObject:data];
                    }
                    [orderData setValue:orders forKey:girl_key];
                }
                tableUsage = json[@"data"][@"table_usage"];
            }
            if(json[@"data"][@"girl"] != nil){
                for(NSDictionary*data in json[@"data"][@"girl"]){
                    NSString *girl_key = [NSString stringWithFormat:@"girl_id_%@", data[@"id"]];
                    [girlData setValue:data forKey:girl_key];
                    [rowData addObject:girl_key];
                }
            }
            [self.reportTable reloadData];
        }
        [KVNProgress dismiss];
    }];
}

- (void) refresh:(UIRefreshControl *)sender{
    [UIView animateWithDuration:0 animations:^{
            [self getReport];
    } completion:^(BOOL finished) {
        [sender endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 60;
    
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MamaCellHead"];
    UIView *head = cell.contentView;
    head.backgroundColor = [UIColor blackColor];
    
    UIView *col1 = [head viewWithTag:101];
    col1.backgroundColor = [UIColor blackColor];
    UILabel *lbl1 = (UILabel*)[col1 viewWithTag:1011];
    lbl1.text = AMLocalizedString(@"ชื่อเด็กดริ๊งค์", nil);
    lbl1.textColor = [UIColor whiteColor];
    
    UIView *col2 = [head viewWithTag:102];
    col2.backgroundColor = [UIColor blackColor];
    UILabel *lbl2 = (UILabel*)[col2 viewWithTag:1021];
    lbl2.text = AMLocalizedString(@"จำนวนดริ๊งค์", nil);
    lbl2.textAlignment = NSTextAlignmentRight;
    lbl2.textColor = [UIColor whiteColor];
    
    UIView *col3 = [head viewWithTag:103];
    col3.backgroundColor = [UIColor blackColor];
    UILabel *lbl3 = (UILabel*)[col3 viewWithTag:1031];
    lbl3.text = AMLocalizedString(@"ดริ๊งค์ล่าสุด", nil);
    lbl3.textAlignment = NSTextAlignmentCenter;
    lbl3.textColor = [UIColor whiteColor];
    
    UIView *col4 = [head viewWithTag:104];
    col4.backgroundColor = [UIColor blackColor];
    UILabel *lbl4 = (UILabel*)[col4 viewWithTag:1041];
    lbl4.text = AMLocalizedString(@"สถานะเด็กดริ๊งค์", nil);
    lbl4.textAlignment = NSTextAlignmentCenter;
    lbl4.textColor = [UIColor whiteColor];
    
    return head;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [rowData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MamaCellHead"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MamaCellHead"];
    }
    UIView *cellContent = cell.contentView;
    
    if ([rowData count] == 0)  return cell;
    NSString *girlKey = rowData[indexPath.row];
    NSDictionary *girlInfo = girlData[girlKey];
    
    NSString *girlNickName = girlInfo[@"nick_name"];
    UIView *col1 = [cellContent viewWithTag:101];
    UILabel *lbl1 = (UILabel*)[col1 viewWithTag:1011];
    lbl1.text = girlNickName;
    
    NSString *shotN = [NSString stringWithFormat:@"%lu", (unsigned long)[orderData[girlKey] count]];
    UIView *col2 = [cellContent viewWithTag:102];
    UILabel *lbl2 = (UILabel*)[col2 viewWithTag:1021];
    lbl2.text = shotN;
    lbl2.textAlignment = NSTextAlignmentRight;
    
    NSString *lastShot = ([orderData[girlKey] count] > 0)?[Time getTHTimeFromDate:orderData[girlKey][[orderData[girlKey] count] - 1][@"created_datetime"]]:@" - ";
    UIView *col3 = [cellContent viewWithTag:103];
    UILabel *lbl3 = (UILabel*)[col3 viewWithTag:1031];
    lbl3.text = lastShot;
    lbl3.textAlignment = NSTextAlignmentCenter;
    
    NSString *girlStatus = girlInfo[@"status"];
    UIView *col4 = [cellContent viewWithTag:104];
    UILabel *lbl4 = (UILabel*)[col4 viewWithTag:1041];
    [lbl4 removeFromSuperview];
    UIImageView *img1 = (UIImageView*)[col4 viewWithTag:1042];
    img1.image = [UIImage imageNamed:([girlStatus integerValue] == 0)?@"ic_drink_off":@"ic_drink_on"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *girlKey = rowData[indexPath.row];
    if([orderData[girlKey] count] == 0){
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}


#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender // ยกเลิก prepareForSegue
{
    if([identifier isEqualToString:@"DrinkgirlLog"]){
        NSIndexPath *indexPath = [self.reportTable indexPathForSelectedRow];
        NSString *girlKey = rowData[indexPath.row];
        if([orderData[girlKey] count] == 0){
            return NO;
        }else{
            return YES;
        }
    }else{
        return YES;
    }
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"DrinkgirlLog"]){
        DrinkGirlLogViewController *obj = [segue destinationViewController];
        NSIndexPath *indexPath = [self.reportTable indexPathForSelectedRow];
        NSString *girlKey = rowData[indexPath.row];
        NSDictionary *inputData = @{
                                    @"girl":girlData[girlKey],
                                    @"orders":orderData[girlKey],
                                    @"tableUsage":tableUsage
                                    };
        [obj setInputData:inputData];
    }
    else if ([[segue identifier] isEqualToString:@"logout"]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}
#pragma mark - UIInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self initNavigation];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
