//
//  TableSubBillViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableSubBillViewController.h"
#import "GlobalBill.h"
#import "SearchPromotion.h"
#import "ButtonMenuView.h"
#import "Member.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "NSDictionary+DictionaryWithString.h"
#import "TablePromotiomViewController.h"
#import "Check.h"
#import "LoadProcess.h"

@interface TableSubBillViewController(){
    GlobalBill *globalBill;
    
    bool usb;
    bool isa;
    
    LoadProcess *loadProgress;
}

@end

static NSMutableDictionary *dataSubBill;
static NSString *totalBill;
static TableSubBillViewController *blockSelf;

@implementation TableSubBillViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    blockSelf = self;
    loadProgress = [LoadProcess sharedInstance];
    
    NSString *title;
    if (self.isCashierTake || self.isStaffTake) {
        title = self.titleTable;
    }
    else{
        title = [NSString stringWithFormat:@"%@ %@ ",AMLocalizedString(@"โต๊ะที่", nil),self.detailTable[@"label"]];
    }
    self.navigationItem.title = [NSString stringWithFormat:@" %@ : บิลย่อยที่ %@ ",title,self.numSubBill];
    
    globalBill = [GlobalBill sharedInstance];
    usb = [globalBill.billResturant[@"use_sub_bill"] boolValue];
    isa = [globalBill.billResturant[@"is_sign_all"] boolValue];
    
    [self setButtonMenuView];
    [self loadTableBillData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [loadProgress.loadView showWaitProgress];
    [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
}
- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - loadData OpenTable
+(void)loadDataOpenTableSubBill:(NSString *)idTable{    
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{@"code":member.code};
    NSString *url;
    if (blockSelf.isStaffTake) {
        NSString *getBill = [NSString stringWithFormat:GET_BILL,member.nre,@"0"];
        url = [NSString stringWithFormat:@"%@/%@",getBill,blockSelf.detailTable[@"bill_id"]];
    }
    else{
       url = [NSString stringWithFormat:GET_BILL,member.nre,idTable];
    }

    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        @try {
            NSDictionary *_data = json[@"data"];
            totalBill =  [NSString stringWithFormat:@"%@",_data[@"total"]];
            dataSubBill = [NSMutableDictionary dictionaryMutableWithObject:_data[@"subbill"]];
            NSMutableDictionary *_dataPromotion = [NSMutableDictionary dictionaryMutableWithObject:_data[@"promotion"]];  // is_promotion = 0 ไม่มีโปร,1 มีโปร
            NSMutableArray *_dataOrders    = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"orders"]]];
            NSMutableArray *_pendingOrders = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"pending_orders"]]];
            [blockSelf setTableBill:_dataOrders and:_pendingOrders andPro:_dataPromotion andIDPool:_data[@"promotion_pool_id"] ];
        }
        @catch (NSException *exception) {
            DDLogError(@"Caught exception loadDataOpenTableSubBill : %@", exception);
        }
    }];
}
-(void) setTableBill:(NSMutableArray *)_dataOrders and:(NSMutableArray *)_pendingPrders andPro:(NSMutableDictionary *)datapro andIDPool:(NSString *)idPool{
    NSMutableArray *dataSection = [NSMutableArray array];
    NSArray *addFoodBillPENDING;
    NSArray *promotionPool;
    NSDictionary *promotionInfo;
    NSMutableArray *promotionBill = [NSMutableArray array];
    NSMutableDictionary *promotionApproval = [NSMutableDictionary dictionary];
    [self.tableDataBillView.sectionBill removeAllObjects];
    
    if ([datapro[@"is_promotion"] boolValue]) {
        NSArray *promotionFloder  = [datapro[@"promotion_folder"] allValues];
        promotionInfo = datapro[@"promotion_info"];
        promotionPool = datapro[@"promotion_pool"];
        promotionApproval = datapro[@"promotion_approval"];
        
        [SearchPromotion setStaticValel:1 and:2];
        NSMutableArray *promotionList = (NSMutableArray *)[SearchPromotion getPromotionPoolAttachBill:promotionPool
                                                                                                  and:(NSMutableArray *)promotionFloder
                                                                                                  and:promotionInfo and:promotionApproval];
        [SearchPromotion setStaticValel:promotionInfo];        
        addFoodBillPENDING  = [SearchPromotion addBillData:_pendingPrders  andPop:NO];  // 9999
        if ([promotionList count] != 0 && ([_pendingPrders count] != 0)) {
            if ([addFoodBillPENDING count] != 0) {
                for (NSDictionary *dic in promotionList) {
                    NSString *key = dic[@"id"];
                    NSString *value = @"promotion_id = %@";
                    NSArray *data = [NSDictionary searchDictionary:key andvalue:value andData:[addFoodBillPENDING valueForKey:@"dataorder"]];
                    if (data == nil) {
                        [promotionBill addObject:dic];
                    }
                }
            }
        }
        else if ( ([_pendingPrders count] == 0)){
            [promotionBill addObjectsFromArray:promotionList];
        }
    }
    NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:promotionBill forKey:@"PRO"];  // ไม่มีโปร  promotionBill = 0
    [dataSection addObject:data];

    if ([_pendingPrders count] != 0) {
        if ([addFoodBillPENDING count] != 0) {
            NSMutableDictionary *addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:@{@"data":addFoodBillPENDING,
                                                                                                   @"commit":@"0"}];
            [self.tableDataBillView.sectionBill setObject:addFoodBillList forKey:@"1"];
            NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:addFoodBillList forKey:@"PENDING"];
            [dataSection addObject:data];
        }
    }
    if ([self.tableDataBillView.sectionBill count] != 0) {
        NSLog(@"self.tableDataBillView.sectionSubBill------------ yes ");
    }
    else
    NSLog(@"self.tableDataBillView.sectionSubBill------------ no ");
    
    [self.tableDataBillView setSelfSuper:self];
    [self.tableDataBillView setTableID:self.detailTable[@"id"]];
    [self.tableDataBillView setSectionDataIndex:dataSection];
    [self.tableDataBillView setPromotionPoolID:idPool];
    [self.tableDataBillView setDataPromotion:datapro];
    [self.tableDataBillView setUseSubBill:usb];
    [self.tableDataBillView setIsSignAll:isa];
    [self.tableDataBillView setIsStaffTake:self.isStaffTake];
    [self.tableDataBillView setIsCashierTake:self.isCashierTake];
    [self.tableDataBillView.mTableViewBill reloadData];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}
-(void) loadTableBillData{
//    
    self.tableDataBillView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableDataBill"];
    [self.tableDataBillView setDetailTable:self.detailTable];
    [self.tableDataBillView setTableSingleBillViewController:self];
    
    self.tableDataBillView.view.frame = self.mContainerTableBill.bounds;
    [self addChildViewController:self.tableDataBillView];
    [self.mContainerTableBill addSubview:self.tableDataBillView.view];
    [self.tableDataBillView didMoveToParentViewController:self];
}
-(void) setButtonMenuView{
    Member *member = [Member getInstance];
    [member loadData];
    
    NSDictionary *dicres = globalBill.billResturant;
    bool bl = [dicres[@"has_drink"] boolValue];
    NSString *nameButton;
    if (!bl || [member.girlID isEqual:@"0"]) {nameButton = @"notButtonDrink";}
    else{ nameButton = @"allButton";}
    
//    
    ButtonMenuView  *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:nameButton];
    [pager setTableSingleBillViewController:self];
    
    pager.view.frame = self.mContainerMenu.bounds;
    [self addChildViewController:pager];
    [self.mContainerMenu addSubview:pager.view];
    [pager didMoveToParentViewController:self];
}

- (IBAction)btnOK:(id)sender {
    [self.tableDataBillView btnOK:sender];
}
-(void)pushTableZone:(id)sender{
//    
    UIViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableZone"];
    [self presentViewController:viewController_   animated:YES completion:nil];
}
- (IBAction)btnBackHome:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - create table container
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"prepareForSegue :%@",segue.identifier);
    if ([segue.identifier isEqualToString:@"requirePromtion"]) {
        TablePromotiomViewController *_viewController = segue.destinationViewController;
        [_viewController setTableSingleBillViewControll:self.tableDataBillView.tableSingleBillViewController];
        [_viewController loadDataPromotionRequirePromtion];
    }
}
@end
