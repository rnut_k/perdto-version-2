//
//  TableMoveViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableMoveViewController.h"
#import "TableBillMenuViewController.h"
#import "TableAllZoneCollectionView.h"
#import "TableSingleBillViewController.h"
#import "TableSubBillViewController.h"

#import "Member.h"
#import "LoadMenuAll.h"
#import "GlobalBill.h"
#import "Check.h"
#import "SelectBill.h"
#import "LoadProcess.h"

@interface TableMoveViewController ()<CNPPopupControllerDelegate>{
     LoadProcess *loadProgress;
}
@property (nonatomic, strong) CNPPopupController *popupController;

@end
@implementation TableMoveViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpUI];
    self.indexMoveTo = [NSDictionary dictionary];
    
    [self setTitle:kTableMove];
    self.mNumMove.text = self.indexMove[@"label"];
    loadProgress = [LoadProcess sharedInstance];
    
    self.otpTableTxt.placeholder = self.indexMoveTo[@"id"];
    self.otpValTxt.placeholder = [self randomStringWithLength:4];
    
}
-(void)setUpUI{
    _labelMoveTable.text = AMLocalizedString(@"move_to_table", nil);
    _buttonConfirm.title = AMLocalizedString(@"confirm", nil);
}
-(void)loadDataOpenTable{
    [loadProgress.loadView showWithStatus];
    Member *member = [Member getInstance];
    [member loadData];
    GlobalBill *_globalBill = [GlobalBill sharedInstance];
    NSArray *arr = [LoadMenuAll requirebillID:self.indexMove[@"id"]];
    
    NSString *otpTable = ([self.otpTableTxt.text isEqualToString:@""])?self.otpTableTxt.placeholder:self.otpTableTxt.text;
    NSString *otpVal = ([self.otpValTxt.text isEqualToString:@""])?self.otpValTxt.placeholder:self.otpValTxt.text;
    NSString *otp = [NSString stringWithFormat:@"%@%@", otpTable, otpVal];
    NSDictionary *parameters = @{@"code":member.code,
                                 @"action":@"move",
                                 @"bill_id":arr[1],
                                 @"from":self.indexMove[@"id"],
                                 @"to":self.indexMoveTo[@"id"],
                                 @"otp":otp
                                 };
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        bool bl = [dictionary[@"status"] boolValue];
        if (bl) {
//            
            if ([_globalBill.billType  isEqual: kSingleBill]) {
                TableSingleBillViewController *tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSingleBill"];
                [tableSingleBill setTitleTable:[NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ", nil),self.indexMoveTo[@"label"]]];
                [LoadMenuAll loadDataOpenTable:self.indexMoveTo[@"id"] success:^(NSDictionary *dictionary) {
                     [tableSingleBill setDataListSingBill:dictionary];
                }];
                [tableSingleBill setNumberTable:self.indexMoveTo[@"label"]];
                [tableSingleBill setDetailTable:self.indexMoveTo];
                [tableSingleBill setCheckOpenTable:YES];
                [[self navigationController] pushViewController:tableSingleBill animated:NO];
            }
            else{
                TableSingleBillViewController *tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSubBill"];
                [tableSingleBill setTitleTable:[NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ", nil),self.indexMoveTo[@"label"]]];
                [LoadMenuAll loadDataOpenTable:self.indexMoveTo[@"id"] success:^(NSDictionary *dictionary) {
                    [tableSingleBill setDataListSingBill:dictionary];
                }];
                [tableSingleBill setNumberTable:self.indexMoveTo[@"label"]];
                [tableSingleBill setDetailTable:self.indexMoveTo];
                [tableSingleBill setCheckOpenTable:YES];
                [[self navigationController] pushViewController:tableSingleBill animated:NO];
            }
            [LoadProcess dismissLoad];
        }
        else
            [loadProgress.loadView showError];
    }];
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
//    Member  *member = [Member getInstance];
//    [member loadData];
//    UIViewController *controller = segue.destinationViewController;
//    UIStoryboard *storyboard = self.storyboard;
//    THSegmentedPager *pager = [storyboard instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
//    NSMutableArray *pages = [NSMutableArray new];
    
//    NSDictionary *tableIndexData = [[[NSDictionary alloc]init] mutableCopy];
//    [LoadMenuAll retrieveData:@"0" success:^(NSArray *dataTables){
//        dispatch_async(dispatch_get_main_queue(), ^{
//            for(NSDictionary *data in dataTables){
//                NSString *key = [NSString stringWithFormat:@"zone_id_%@", data[@"zone_id"]];
//                NSMutableArray *tableListInZone = (tableIndexData[key] != nil)?(NSMutableArray*)tableIndexData[key]:[[NSMutableArray alloc]init];
//                [tableListInZone addObject:data];
//                [tableIndexData setValue:tableListInZone forKey:key];
//            }
//            
//            for (NSDictionary *dic in self.dataListZoneAll) {
//                
//                NSString *key = [NSString stringWithFormat:@"zone_id_%@", dic[@"id"]];
//                TableAllZoneCollectionView *_tableAllZoneCollectionView = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableAllZone"];
//                [pages addObject:_tableAllZoneCollectionView];
//                [_tableAllZoneCollectionView setViewTitle:dic[@"name"]];
//                [_tableAllZoneCollectionView setDataListZone:dic];
//                [_tableAllZoneCollectionView setNumTable:[Check checkDevice]?4:8];
//                [_tableAllZoneCollectionView setTableMoveView:self];
//                [_tableAllZoneCollectionView setDataListTables:tableIndexData[key]];
//
//                [_tableAllZoneCollectionView.collectionView reloadData];
//                
//            }
//            
//            [pager setPages:pages];
//            [controller addChildViewController:pager];
//            [controller.view addSubview:pager.view];
//            [pager didMoveToParentViewController:controller];
//        });
//    }];
    
    Member  *member = [Member getInstance];
    [member loadData];
    UIViewController *controller = segue.destinationViewController;
//    
    THSegmentedPager *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    NSMutableArray *pages = [NSMutableArray new];
    for (NSDictionary *dic in self.dataListZoneAll) {
        TableAllZoneCollectionView  *_tableAllZone = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableAllZone"];
        [pages addObject:_tableAllZone];
        [_tableAllZone setViewTitle:dic[@"name"]];
        [_tableAllZone setDataListZone:dic];
        [_tableAllZone setNumTable:[Check checkDevice]?3:6];
        [_tableAllZone setTableMoveView:self];
        
        [LoadMenuAll retrieveData:dic[@"id"] success:^(NSArray *dataTables){
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableAllZone setDataListTables:dataTables];
                [_tableAllZone.collectionView reloadData];
            });
        }];
    }
    [pager setPages:pages];
    [controller addChildViewController:pager];
    [controller.view addSubview:pager.view];
    [pager didMoveToParentViewController:controller];
    
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)btnBackZone:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnConfirm:(id)sender {
    if (![self.mNumMoveTo.text isEqual:@""]) {
        [self loadDataOpenTable];
    }
}
-(NSString *) randomStringWithLength: (int) len {
    //    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex:arc4random_uniform((int32_t)[letters length])]];
    }
    
    return randomString;
}
@end
