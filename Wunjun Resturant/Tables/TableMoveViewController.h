//
//  TableMoveViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"

#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"

@interface TableMoveViewController :UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIImageView *mImageTableMove;
@property (weak, nonatomic) IBOutlet UIImageView *mImageTableMoveTo;
@property (weak, nonatomic) IBOutlet UILabel *mNumMove;
@property (weak, nonatomic) IBOutlet UILabel *mNumMoveTo;
@property (weak, nonatomic) IBOutlet UITextField *otpTableTxt;
@property (weak, nonatomic) IBOutlet UITextField *otpValTxt;

@property (nonatomic,strong)  NSMutableArray *dataListZoneAll;
@property (nonatomic,strong)  NSArray *dataListTablesAll;
@property (nonatomic,strong)  NSDictionary *indexMove;
@property (nonatomic,strong)  NSDictionary *indexMoveTo;
@property (nonatomic,strong)  NSDictionary *dataTable;

//static label
@property (weak, nonatomic) IBOutlet UILabel *labelMoveTable;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonConfirm;

//============
- (IBAction)btnBackZone:(id)sender;
- (IBAction)btnConfirm:(id)sender;
@end
