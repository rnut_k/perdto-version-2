//
//  TablesViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "CNPPopupController.h"
#import "LocalizationSystem.h"

@interface TablesViewController : UIViewController<CNPPopupControllerDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic ,strong)  NSMutableArray *dataListItemOrder;
@property (nonatomic ,strong)  NSMutableArray *dataListItemOrderPop;

-(IBAction)barButtonItemAction:(id)sender;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
@end
