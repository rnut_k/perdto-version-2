//
//  TableSumViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableSumViewController.h"
#import "TableZoneCheckViewController.h"
#import "TableAllZoneCollectionView.h"
#import "Time.h"
#import "Check.h"

@implementation TableSumViewController{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mTimeTable setText:[Time getTime]];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(mTagKeybroad:)];
    [tapGesture setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapGesture];
    
    [self setTitle:kTableSum];
}

- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    UIViewController *controller = segue.destinationViewController;
//    
    THSegmentedPager *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    NSMutableArray *pages = [NSMutableArray new];
    for (int i = 1; i < 5; i++) {
        
        TableAllZoneCollectionView  *_tableAllZone = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableAllZone"];
        [_tableAllZone setViewTitle:[NSString stringWithFormat:@"Zone %d",i]];
        [_tableAllZone setNumTable:[Check checkDevice]?3:6];
        [pages addObject:_tableAllZone];
    }
    [pager setPages:pages];
    [controller addChildViewController:pager];
    [controller.view addSubview:pager.view];
    [pager didMoveToParentViewController:controller];
}
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)mTagKeybroad:(id)sender {
    [_mCountMen resignFirstResponder];
    [_mCountWomen resignFirstResponder];
}

- (IBAction)btnTableOpen:(id)sender {
    
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
