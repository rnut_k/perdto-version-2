//
//  TableSingleBillViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableSingleBillViewController.h"
#import "TablePromotiomViewController.h"
#import "TableSubBillViewController.h"
#import "CellTableViewAll.h"
#import "CashTableStatus.h"
#import "CashCheckBillViewController.h"
#import "CustomNavigationController.h"
#import "TableOpenViewController.h"
#import "AlertViewNotiController.h"
#import "CashRequestDiscountsViewController.h"
#import "CashSpecifyTheAmount.h"
#import "PrintReceiptViewController.h"

#import "AppDelegate.h"
#import "CNPPopupController.h"
#import "Constants.h"
#import "Time.h"
#import "ButtonMenuView.h"
#import "MenuDB.h"
#import "NSDictionary+DictionaryWithString.h"
#import "NSString+GetString.h"
#import "Member.h"
#import "LoadMenuAll.h"
#import "SearchPromotion.h"
#import "PopupView.h"
#import "GlobalBill.h"
#import "Check.h"
#import "LoadProcess.h"


@interface TableSingleBillViewController()<CNPPopupControllerDelegate,WYPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate>{
    int _price;
    BOOL tmpPrice;
    PopupView *popupView;
    GlobalBill *globalBill;
    UIButton *titleLabelButton;
    WYPopoverController *popoverController;
    
    NSString *numSubBill;
    UITableView *tableSreachPop;
    
    bool blu;

    NSDictionary *tempSectionData;
    CustomNavigationController *vc;
    
    float foodTotal;
    float drickTotal;
    
    CGRect tableDataBillSize;
}
@property (nonatomic,strong) CNPPopupController            *popupController;
@property (nonatomic,weak  ) ButtonMenuView                *buttonMenuView;
@property (nonatomic,copy  ) TableSingleBillViewController *blockSelf;

@end
static NSDictionary *dataSubbill  = nil;
static TableSingleBillViewController *blockSelf = nil;
static LoadProcess *loadProgress = nil;

static NSString *idPassivePro                     = @"";
static NSDictionary *dataSeclectPassivePro        = nil;
static NSMutableDictionary *billSeclectPassivePro ;
static BOOL blSeclectPassivePro                   = NO;


@implementation TableSingleBillViewController

- (id)init
{
    if (!self) {
        self = [[TableSingleBillViewController alloc] init];
    }
    return self;
}
+ (void)initialize {
    if (self == [TableSingleBillViewController class]) {
        idPassivePro = @"";
        dataSeclectPassivePro = nil;
        blSeclectPassivePro = NO;
        if ([Check checkNull:billSeclectPassivePro]) {
            billSeclectPassivePro = [NSMutableDictionary dictionary];
        }
    }
}
+ (void)resetBillSeclectPassivePro:(NSString *)idBill{
    [billSeclectPassivePro removeObjectForKey:idBill];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [TableSingleBillViewController initialize];
    loadProgress = [LoadProcess sharedInstance];
    numSubBill = @"1";
    blockSelf = self;
    vc = (CustomNavigationController *)self.parentViewController;
    
    if(![self.role isEqualToString:@"admin"]){
        if (vc.dataSingleBillViewController != nil) {
            [self setDataALL];
        }
    }
    
    globalBill = [GlobalBill sharedInstance];
    blu = [globalBill.billResturant[@"use_sub_bill"] boolValue];
    
    if([self.role isEqualToString:@"admin"]){
        [self.leftBarBtn setImage:[UIImage imageNamed:@"ic_back"]];
        [self.rightBarBtn setImage:nil];
        [self.rightBarBtn setEnabled:NO];
        self.mContainerMenu.backgroundColor = [UIColor whiteColor];
        
        [self.checkBillBtn setEnabled:NO];
        [self.checkBillBtn setHidden:YES];
    }
    else{
        [self setButtonMenuView];
    }
    [self loadTableBillData];
    [self createButtonTitleView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self setLoadDataTable];
  
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
}
-(void) setLoadDataTable{
    if (![self.tableSuperClass isKindOfClass:[TableOpenViewController class]] &&
        ![self.tableSuperClass isKindOfClass:[CustomNavigationController class]] &&
        ![self.role isEqualToString:@"admin"]) {
        if ([self isCashier] || self.isCashTableStatus || self.isCashierTake) {
            NSString *idBill = self.detailTable[@"id"];
            if (!self.isPopupCashier && !self.isStaffTake) {
                self.sizeButtonCheckBill.constant = 30;
            }
            if (self.isPopupCashier) {
                if (self.detailTable[@"bill_id"] != nil) {
                    idBill = self.detailTable[@"bill_id"];
                }
                [TableSingleBillViewController loadDataBillDetailBy:idBill];
            }
            else{
                if (self.isStaffTake) {
                    idBill = self.dataListSingBill[@"no"];
                }
                
                NSDictionary *data = billSeclectPassivePro[idBill];
                if (![Check checkNull:data]) {
                    idPassivePro = data[@"id"];
                    blSeclectPassivePro = YES;
                    [self.mPassivePro setTitle:data[@"name"] forState:UIControlStateNormal];
                }
                [TableSingleBillViewController loadDataOpenTableCashier:idBill];
               self.navigationItem.rightBarButtonItem = nil;
            }
            
        }
        else if (self.isDeleteBill){
            NSString *idBill = self.detailTable[@"object_id"];
            [self loadDataBillDelete:idBill];
        }
        else{
            NSString *idBill = self.detailTable[@"id"];
            [TableSingleBillViewController loadDataOpenTableSing:idBill];
            [self createButtonBarRight];
        }
    }
    else{
        if (self.isCashTableStatus) { // หน้า view tab table admin
            NSString *idBill = self.detailTable[@"id"];
            [TableSingleBillViewController loadDataOpenTableCashier:idBill];
            [self.sizePassivePromotion setConstant:0.0];
            [self.mPassivePro setTitle:@"" forState:UIControlStateNormal];
        }
        else if (!self.isPopup) {
            NSString *idBill = self.detailTable[@"id"];
            [TableSingleBillViewController loadDataOpenTableSing:idBill];
            [self createButtonBarRight];
        }
      
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}

-(void)setDataALL{
    self.detailTable      = [vc.dataSingleBillViewController detailTable];
    self.titleTable       = [vc.dataSingleBillViewController titleTable];
    self.dataListSingBill = [vc.dataSingleBillViewController dataListSingBill];
    self.numberTable      = [vc.dataSingleBillViewController numberTable];
    self.checkOpenTable   = [vc.dataSingleBillViewController checkOpenTable];
}

#pragma mark - loadData OpenTable
+(void)loadDataOpenTableSing:(NSString *)idTable{
    
    if (![Check checkNull:blockSelf.indexSplitBill]) {
        idTable = [idTable stringByAppendingFormat:@"/%@",blockSelf.indexSplitBill];
        [blockSelf setTitleLabelButton:blockSelf.indexSplitBill];
    }
    
    [loadProgress.loadView showWithStatus];
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{@"code":member.code};
    
    NSString *url;
    if (blockSelf.isStaffTake || blockSelf.isCashierTake) {
        NSString *getBill = [NSString stringWithFormat:GET_BILL,member.nre,@"0"];
        url = [NSString stringWithFormat:@"%@/%@",getBill,blockSelf.detailTable[@"bill_id"]];
    }
    else{
        url = [NSString stringWithFormat:GET_BILL,member.nre,idTable];
    }

    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        @try {
            bool bl = [json[@"status"] boolValue];
            if (bl) {
                NSDictionary *_data                 = [NSDictionary dictionaryWithObject:json[@"data"]];
                dataSubbill                         = [NSDictionary dictionaryWithObject: _data[@"subbill"]];
                blockSelf.dataListSingBill          = _data;
                blockSelf.splitDataList             = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"bill_split_list"]]];
                NSMutableDictionary *_dataPromotion = [NSMutableDictionary dictionaryMutableWithObject:_data[@"promotion"]]; // is_promotion = 0 ไม่มีโปร,1 มีโปร
                NSMutableArray *_dataOrders         = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"orders"]]];
                NSMutableArray *_tmpPendingOrders   = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"pending_orders"]]];
                NSMutableArray *_pendingOrders      = [[NSMutableArray alloc]init];
                for(NSDictionary *pOrders in _tmpPendingOrders)
                {
                    [_pendingOrders addObject:pOrders];
                    
                    NSString *relateKey = [NSString stringWithFormat:@"relate_order_id_%@", pOrders[@"id"]];
                    if ([_data[@"pending_drink_log"] count] != 0) {
                        NSMutableArray *relateList = _data[@"pending_drink_log"][relateKey];
                        if(relateList != nil){
                            for(NSDictionary *rOrders in relateList){
                                [_pendingOrders addObject:rOrders];
                            }
                        }
                    }
                }
                
                NSMutableDictionary *dataPromotion1  = [NSMutableDictionary dictionaryMutableWithObject:_data[@"promotion"]];
                [SearchPromotion setStaticValel:[NSDictionary dictionaryWithObject:dataPromotion1[@"promotion_info"]]];
                [SearchPromotion setSpecialtFood:[Check checkObjectType:_data[@"menu"]]];
                [blockSelf setTableBill:_dataOrders and:_pendingOrders andPro:_dataPromotion andIDPool:_data[@"promotion_pool_id"]];
            }
            else{
                DDLogError(@"error loadDataOpenTableSing");
                [LoadProcess dismissLoad];
            }
        }
        @catch (NSException *exception) {
            DDLogError(@"Caught exception loadDataOpenTableSing : %@", exception);
        }
    }];
}
-(void)createButtonBarRight{
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"ic_check_bill"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnRequireCheckBill:) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, 0,40,40)];
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [self.navigationItem setRightBarButtonItem:rightBarButtonItem];
}

+(void)loadDataOpenTableCashier:(NSString *)idTable {
    
    [loadProgress.loadView showWithStatus];
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{@"code":member.code};
    
    NSString *url;
    if(blockSelf.isCashierTake){
        NSString *getBill = [NSString stringWithFormat:GET_BILL_CASHIER,member.nre,@"0"];
        url = [NSString stringWithFormat:@"%@/%@",getBill,blockSelf.detailTable[@"bill_id"]];
    }
    else{
        url = [NSString stringWithFormat:GET_BILL_CASHIER,member.nre,idTable];
    }
    if (blSeclectPassivePro) {
        NSError *error = nil;
        NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[idPassivePro]
                                                                                            options:NSJSONWritingPrettyPrinted
                                                                                              error:&error]
                                                   encoding:NSUTF8StringEncoding];
        NSMutableDictionary *data = [NSMutableDictionary dictionaryMutableWithObject:parameters];
        [data setObject:cartJSON forKey:@"passive_pro_id"];
        parameters = [data mutableCopy];
    }

    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        bool bl = [dictionary[@"status"] boolValue];
        if (bl) {
            NSDictionary *_data        = [NSDictionary dictionaryWithObject:dictionary[@"data"]];
            dataSubbill                = [NSDictionary dictionaryWithObject:_data[@"subbill_info"]];
            blockSelf.dataListSingBill = _data;

//            NSString *tot              = [NSString stringWithFormat:@"bill_id_%@",blockSelf.dataListSingBill[@"bill_list"][0]];
            blockSelf.splitDataList    = [NSMutableArray arrayWithArray:[Check checkObjectType:[_data[@"bill_info"] allValues]]];// 1
//            NSMutableDictionary *dataOrdersTemp1 = [NSMutableDictionary dictionaryMutableWithObject:_data[@"orders_info"]];     // 1
//            NSMutableArray *_dataOrders          = [NSMutableArray array];
//
//            if ([dataOrdersTemp1 count] != 0 && !blockSelf.isCashier) {
//                _dataOrders = [NSMutableArray arrayWithArray:dataOrdersTemp1[tot]];
//            }
            
            NSMutableDictionary *dataPromotion1  = [NSMutableDictionary dictionaryMutableWithObject:_data[@"promotion"]];
            [SearchPromotion setStaticValel:[NSDictionary dictionaryWithObject:dataPromotion1[@"promotion_info"]]];
            [SearchPromotion setSpecialtFood:[Check checkObjectType:_data[@"menu"]]];
            [blockSelf setTableBill:_data and:nil andPro:dataPromotion1 andIDPool:@""];
        }
        else{
            DDLogError(@"error loadDataOpenTableCashier");
            [LoadProcess dismissLoad];
        }
    }];
}
+(void)loadDataBillDetailBy:(NSString *)idTable{
    [loadProgress.loadView showWithStatus];
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{
                                 @"code":member.code,
                                 @"type":@"1",
                                 @"bill_id":idTable,
                                 @"table_check":@"1"
                                 };
    NSString *url = [NSString stringWithFormat:GET_BILL_DETAIL,member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        @try {
            bool bl = [json[@"status"] boolValue];
            if (bl) {
                NSDictionary *_data = [NSDictionary dictionaryWithObject:json[@"data"]];
                dataSubbill = [NSDictionary dictionaryWithObject: _data[@"subbill"]];
                blockSelf.dataListSingBill = _data;
                blockSelf.splitDataList = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"bill_split_list"]]];
                NSMutableDictionary *_dataPromotion = [NSMutableDictionary dictionaryMutableWithObject:_data[@"promotion"]];   // is_promotion = 0 ไม่มีโปร,1 มีโปร
                
                NSMutableArray *_tmpPendingOrders = [NSMutableArray arrayWithArray:[Check checkObjectType:_data[@"pending_orders"]]];
                NSMutableArray *_pendingOrders = [[NSMutableArray alloc]init];
                for(NSDictionary *pOrders in _tmpPendingOrders)
                {
                    [_pendingOrders addObject:pOrders];
                    
                    NSString *relateKey = [NSString stringWithFormat:@"relate_order_id_%@", pOrders[@"id"]];
                    if ([_data[@"pending_drink_log"] count] != 0) {
                        NSMutableArray *relateList = _data[@"pending_drink_log"][relateKey];
                        if(relateList != nil){
                            for(NSDictionary *rOrders in relateList){
                                [_pendingOrders addObject:rOrders];
                            }
                        }
                    }
                }
           
                [SearchPromotion setIdBillOrder:idTable];
                [SearchPromotion setStaticValel:[NSDictionary dictionaryWithObject:_dataPromotion]];
                [SearchPromotion setSpecialtFood:[Check checkObjectType:_data[@"menu"]]];
                [blockSelf setTableBill:_data and:_pendingOrders andPro:_dataPromotion andIDPool:_data[@"promotion_pool_id"] ];
            }
            else{
                DDLogError(@"error loadDataOpenTableSing");
                [LoadProcess dismissLoad];
            }
        }
        @catch (NSException *exception) {
            DDLogError(@"Caught exception loadDataBillDetailBy : %@", exception);
        }
    }];
}
- (void) loadDataBillDelete:(NSString *)idTable{
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{@"code":member.code,
                                 @"is_deleted":@"1",
                                 @"type":@"1",
                                 @"bill_id":idTable
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_LIST_BILLDETAIL,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSDictionary *data = [NSDictionary dictionaryWithObject:json];
        dataSubbill = [NSDictionary dictionaryWithObject:data[@"subbill"]];
        [blockSelf setDataListSingBill:data];;

//        NSDictionary *orders = [NSDictionary dictionaryWithObject:data[@"orders"]];
//        NSMutableArray *dataOrders = [NSMutableArray arrayWithArray:[[orders allValues] firstObject]];
        NSDictionary *tables = [NSDictionary dictionaryWithObject:data[@"tables"]];
        NSDictionary *usage = [NSDictionary dictionaryWithObject:[data[@"usage"] firstObject]];
        NSDictionary *dataTable = tables[[NSString stringWithFormat:@"id_%@",usage[@"table_id"]]];
        NSString *title =  [NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ", nil),dataTable[@"label"]];
        [titleLabelButton setTitle:title forState:UIControlStateNormal];
        
        id billList = [Check checkObjectType:data[@"bill"]];
        id dataBill = [billList firstObject];
        [blockSelf setSplitDataList:dataBill];
        NSMutableDictionary *dataPromotion  = [NSMutableDictionary dictionaryMutableWithObject:data[@"promotion"]];        
        [SearchPromotion setIdBillOrder:idTable];
        [SearchPromotion setStaticValel:[NSDictionary dictionaryWithObject:dataPromotion]];
        [SearchPromotion setSpecialtFood:[Check checkObjectType:data[@"menu"]]];
        
        [blockSelf setTableBill:data and:nil andPro:dataPromotion andIDPool:@""];
    }];
}
-(void) setTableBill:(id)_dataOrders and:(NSMutableArray *)_pendingPrders andPro:(NSMutableDictionary *)datapro andIDPool:(NSString *)idPool{
    
    NSMutableArray *dataSection = [NSMutableArray array];
    NSArray *addFoodBillPENDING;
    NSArray *promotionPool;
    NSDictionary *promotionInfo;
    NSMutableArray *promotionBill = [NSMutableArray array];
    NSDictionary *promotionApproval = [NSDictionary dictionary];
    [self.tableDataBillView.sectionBill removeAllObjects];
    
    @try {
        addFoodBillPENDING  = [SearchPromotion addBillData:_pendingPrders andPop:NO];   // 9999
        if ([datapro[@"is_promotion"] boolValue] && !blu && ![self.role isEqualToString:@"admin"]) {
            NSDictionary *promotionFloder  = [NSDictionary dictionaryWithObject:datapro[@"promotion_folder"]];
            NSArray *promotionFloder1  = [promotionFloder allValues];
            promotionInfo = [NSDictionary dictionaryWithObject:datapro[@"promotion_info"]];
            promotionPool = [NSArray arrayWithArray:datapro[@"promotion_pool"]];
            promotionApproval =  [NSDictionary dictionaryWithObject:datapro[@"promotion_approval"]];
            
            [SearchPromotion setStaticValel:promotionInfo];
            [SearchPromotion setStaticValel:1 and:2];
            NSMutableArray *promotionList = (NSMutableArray *)[SearchPromotion getPromotionPoolAttachBill:promotionPool
                                                                                                      and:(NSMutableArray *)promotionFloder1
                                                                                                      and:promotionInfo and:promotionApproval];
            if (![self isCashier] && !self.isCashTableStatus) {
                if ([promotionList count] != 0 && ([_pendingPrders count] != 0)) {
                    if ([addFoodBillPENDING count] != 0) {
                        for (NSDictionary *dic in promotionList) {
                            NSString *key = dic[@"id"];
                            NSString *value = @"promotion_id = %@";
                            NSArray *data = [NSDictionary searchDictionary:key andvalue:value andData:[addFoodBillPENDING valueForKey:@"dataorder"]];
                            if (data == nil) {
                                [promotionBill addObject:dic];
                            }
                        }
                    }
                }
                else if ( ([_pendingPrders count] == 0)){
                    [promotionBill addObjectsFromArray:promotionList];
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"Caught exception setTableBill : %@", exception);
    }
    
    NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:promotionBill forKey:@"PRO"];   // ไม่มีโปร  promotionBill = 0
    [dataSection addObject:data];
    
    if ([_dataOrders count] != 0) {   // get buffer add bill
        NSMutableArray *addFoodBill  = [SearchPromotion addBillData:_dataOrders andPop:self.isPopupCashier];
         if ([addFoodBill count] != 0) {
             if (![self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
                 if (![self isCashier]) {
                     if (blu) {
                         addFoodBill = [self sortDataOrder:addFoodBill];
                     }
                 }
             }
             else{
                 if (blu) {
                     addFoodBill = [self sortDataOrder:addFoodBill];
                 }
             }
             
             if (self.isCashTableStatus) {
                 foodTotal = 0;
                 drickTotal = 0;
                 
                 if ([_dataOrders isKindOfClass:[NSArray class]]) {
                     for (NSDictionary *dic  in _dataOrders) {
                         NSString *type = [NSString checkNull:dic[@"menu_type"]];
                         float total = [NSString checkNullConvertFloat:dic[@"total"]];
                         if([type isEqualToString:@"1"]){
                             foodTotal += total;
                         }
                         else if ([type isEqualToString:@"2"]){
                             drickTotal += total;
                         }
                     }
                 }
                 else{
                     NSDictionary *subTotalFood  = [NSDictionary dictionaryWithObject:_dataOrders[@"sub_total_food"]];
                     NSDictionary *subTotalDrink = [NSDictionary dictionaryWithObject:_dataOrders[@"sub_total_drink"]];
                     
                     foodTotal = [[NSString checkNull:subTotalFood[@"total"]] floatValue];
                     drickTotal = [[NSString checkNull:subTotalDrink[@"total"]] floatValue];
                 }
               
             }
             self.listTitleSubBill = [NSMutableArray array];
             [self.listTitleSubBill addObjectsFromArray:addFoodBill];
             NSMutableDictionary *addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:@{@"data":addFoodBill,
                                                                                                    @"commit":@"1"}];
             [self.tableDataBillView.sectionBill setObject:addFoodBillList forKey:@"0"];
             NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:addFoodBillList forKey:@"ORDER"];
             [dataSection addObject:data];
         }
    }
     if (![self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
          if ([_pendingPrders count] != 0 && !blu && ![self isCashier]) {
             if ([addFoodBillPENDING count] != 0) {
                 NSMutableDictionary *addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:@{@"data":addFoodBillPENDING,
                                                                                                        @"commit":@"0"}];
                 [self.tableDataBillView.sectionBill setObject:addFoodBillList forKey:@"1"];
                 NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:addFoodBillList forKey:@"PENDING"];
                 [dataSection addObject:data];
             }
         }
     }
     else{
         if ([_pendingPrders count] != 0 && !blu) {
             if ([addFoodBillPENDING count] != 0) {
                 NSMutableDictionary *addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:@{@"data":addFoodBillPENDING,
                                                                                                        @"commit":@"0"}];
                 [self.tableDataBillView.sectionBill setObject:addFoodBillList forKey:@"1"];
                 NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:addFoodBillList forKey:@"PENDING"];
                 [dataSection addObject:data];
             }
         }
     }
   
    if ([self.tableDataBillView.sectionBill count] != 0) {
        NSLog(@"self.tableDataBillView.sectionBill------------ yes ");
    }
    else
        NSLog(@"self.tableDataBillView.sectionBill------------ no ");
    
    NSMutableArray *passivePromotion = [NSMutableArray arrayWithArray:self.dataListSingBill[@"passive_promotion"]];
    if ([passivePromotion count] > 0) {
        [self setIsPassivePromotion:YES];
    }
    else{
        [self.sizePassivePromotion setConstant:0.0];
        [self.mPassivePro setTitle:@"" forState:UIControlStateNormal];
    }
    [passivePromotion insertObject:@{@"id":@"-333",
                                     @"name":AMLocalizedString(@"ไม่มีเงื่อนไข", nil)}
                           atIndex:0];
    [self setPassivePromotion:passivePromotion];
    
    
    [self.tableDataBillView setIsPopupCashier:self.isPopupCashier];
    [self.tableDataBillView setIsCashier:(self.isCashTableStatus)?YES:NO];
    [self.tableDataBillView setSelfSuper:self];
    [self.tableDataBillView setDataSubBill:dataSubbill];
    [self.tableDataBillView setTableID:self.detailTable[@"id"]];
    [self.tableDataBillView setSectionDataIndex:dataSection];
    [self.tableDataBillView setPromotionPoolID:idPool];
    [self.tableDataBillView setDataPromotion:datapro];
    [self.tableDataBillView setUseSubBill:blu];
    [self.tableDataBillView setIsStaffTake:self.isStaffTake];
    [self.tableDataBillView setIsCashierTake:self.isCashierTake];
    
    [self.tableDataBillView.mTableViewBill reloadData];
    [LoadProcess dismissLoad];
}
- (IBAction)btnBackHome:(id)sender {
    if ([self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
        
        AppDelegate *app = [[UIApplication sharedApplication] delegate];
        [app.window makeKeyAndVisible];
        app.window.rootViewController = [app.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"TableZone"];
    }
    else{
        if (self.checkOpenTable && !self.isCashierTake && !self.isStaffTake)
            [self.navigationController popToRootViewControllerAnimated:YES];
        else
            [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)btnGetPro:(id)sender{
    UIViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ขอโปร"];
    [[self navigationController] pushViewController:_viewController animated:NO];
}
- (IBAction)btnRequireCheckBill:(id)sender{
    [self alertShowCasiher];
//    popupView = [[PopupView alloc] init];
//    [popupView setTitlePop:AMLocalizedString(@"รายการ", nil)];
//    [popupView setTableSingleBillViewController:self];
//    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
- (IBAction)btnCheckBill:(id)sender{
    
    if (![self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
        if ([self isCashier] || [self isCashTableStatus] || self.isCashierTake) {
            
//            CashSpecifyTheAmount *cashSpecifyTheAmount = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashSpecifyTheAmount"];
//            [cashSpecifyTheAmount setViewTitle:AMLocalizedString(@"เงินสด", nil)];
//            [cashSpecifyTheAmount setCountAmount:[NSString stringWithFormat:@"%@",self.dataListSingBill[@"total"][@"total"]]];
//            [cashSpecifyTheAmount setBillTitle:blockSelf.titleTable];
//            [cashSpecifyTheAmount setSelfClass:blockSelf];
//            [cashSpecifyTheAmount setTableDataBillView:blockSelf.tableDataBillView];
//            [cashSpecifyTheAmount setIsCredit:blSeclectPassivePro];
//            
//            [self.navigationController  pushViewController:cashSpecifyTheAmount  animated:YES];
            
            if ([self checkOrderNot]) {
                [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาตรวจสอบรายการอาหาร", nil)];
            }
            else{
                CashSpecifyTheAmount *cashSpecifyTheAmount = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashSpecifyTheAmount"];
                [cashSpecifyTheAmount setViewTitle:AMLocalizedString(@"เงินสด", nil)];
                [cashSpecifyTheAmount setCountAmount:[NSString stringWithFormat:@"%@",self.dataListSingBill[@"total"][@"total"]]];
                [cashSpecifyTheAmount setBillTitle:blockSelf.titleTable];
                [cashSpecifyTheAmount setSelfClass:blockSelf];
                [cashSpecifyTheAmount setTableDataBillView:blockSelf.tableDataBillView];
                [cashSpecifyTheAmount setIsCredit:blSeclectPassivePro];
                
                [self.navigationController  pushViewController:cashSpecifyTheAmount  animated:YES];
            }
        }
    }
}
- (IBAction)btnPasssivePromotion:(id)sender{
    UIButton *btn = (UIButton *)sender;
    [btn setTag:333];
    [self createPopoverController:btn];
}
-(BOOL)checkOrderNot{
    NSArray *dataAddFoodBill = self.tableDataBillView.sectionBill[@"0"][@"data"];
   
    for (NSDictionary *_dic in dataAddFoodBill) {
        int num = [_dic[@"dataorder"][@"deliver_status"]  intValue];
        if (num == 1) {
            int typePro = [_dic[@"dataorder"][@"type"] intValue];
            if (typePro == 1) {
                return YES;
            }      
        }
    }
    return NO;
}
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan){
        popupView = [[PopupView alloc] init];
        [popupView setTanPopup:999];
        [popupView setTableSingleBillViewController:self];
        [popupView showPopupWithStyle:CNPPopupStyleCentered];
    }
}

#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
    if ([title isEqual:AMLocalizedString(@"ขอส่วนลดเพิ่ม", nil)]) {

        CustomNavigationController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"requestDiscounts"];
        [_viewController setDataListSingBill:self.detailTable];
        [_viewController setFoodAmount:[@(foodTotal) stringValue]];
        [_viewController setDrickAmount:[@(drickTotal) stringValue]];
        [_viewController setCountAmount:[NSString stringWithFormat:@"%@",self.dataListSingBill[@"total"][@"total"]]];
        [self  presentViewController:_viewController animated:YES completion:nil];
    }
    else if ([title isEqualToString:AMLocalizedString(@"พิมพ์บิลเก็บเงิน", nil)]){

        PrintReceiptViewController *print = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"PrintReceiptViewController1"];
        [print setTitle:self.title];
        [print setTableDataBillView:self.tableDataBillView];
        [print setTableSingleBillView:self];
        [print setIsPrintAllSubBill:YES];
        [print setIsPrintSubBill:NO];
        
        [self printCheckBillView:YES];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self  presentViewController:print animated:YES completion:nil];
            });
        });
    }
    else if ([title isEqualToString:AMLocalizedString(@"พิมพ์บิลย่อยและชำระเงิน", nil)]){
        NSMutableArray *arrList = [NSMutableArray arrayWithArray:self.listTitleSubBill];
        self.listTitleSubBill = [NSMutableArray array];
        self.isPrintSubBill = YES;
        for (NSDictionary *dic in arrList) {
            if (dic[@"subbill"] != nil) {
                [self.listTitleSubBill addObject:dic];
            }
        }
        [self didTapTitleView:self];
    }
    else if ([title isEqualToString:AMLocalizedString(@"ใช่", nil)]){
        [self alertCashier];
    }
    else if ([title isEqualToString:AMLocalizedString(@"คูปอง", nil)]){

        CustomNavigationController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:kidViewCoupon];
        [_viewController setDataListSingBill:self.detailTable];
        [self  presentViewController:_viewController animated:YES completion:nil];
    }
    else if ([title isEqualToString:AMLocalizedString(@"แก้ไขค่าบริการ", nil)]){

        CustomNavigationController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashServiceView"];
        [_viewController setDataListSingBill:self.dataListSingBill];
        [_viewController setDetailTable:self.detailTable];
        [_viewController  setDataSingleBillViewController:self];
        
        [self  presentViewController:_viewController animated:YES completion:nil];
    }
    else if ([title isEqualToString:AMLocalizedString(@"เช็คบิล", nil)]){
        [self alertShowCasiher];
    }
    else if ([title isEqualToString:AMLocalizedString(@"แสดง QR Code", nil)]){
        [self alertShowQRCode];
    }
}
- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}
-(void) alertCashier{
    [loadProgress.loadView showWithStatus];
    Member *member = [Member getInstance];
    [member loadData];
    
    NSDictionary *parameters = @{@"code":member.code,
                                 @"bill_id":self.dataListSingBill[@"bill_id"],
                                 @"table_id":self.dataListSingBill[@"table_id"],
                                 @"usage_id":self.dataListSingBill[@"usage_id"]};
    
    NSString *url = [NSString stringWithFormat:GET_ALERT_CASHIER,member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        BOOL bl = [dictionary[@"status"] boolValue];
        if (bl) {
            [loadProgress.loadView showWithMessage:AMLocalizedString(@"ส่งคำขอเช็คบิลสำเร็จ", nil)
                                           success:^(BOOL isComplete) {
                if ([Check checkVersioniOS8]) {
                    [self performSelector:@selector(dismissSheet) withObject:nil afterDelay:0];
                }
                else{
                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                }
            }];
        }
        else{
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"ส่งคำขอเช็คบิลไปแล้ว", nil)];
        }
    }];
}
-(void)dismissSheet
{
    [vc.navigationController popToRootViewControllerAnimated:YES];
}
-(void) printCheckBillView:(BOOL)blLoop{
    [blockSelf.tableDataBillView setDataImagePrint:[NSMutableDictionary dictionary]];
    [blockSelf.tableDataBillView setIsAdddataPrint:YES];
    [blockSelf.tableDataBillView setIsPrintAllSubBill:YES];
    [blockSelf.tableDataBillView setIsPrintSubBill:NO];
    tableDataBillSize = self.tableDataBillView.mTableViewBill.frame;
    [self setHeightTable];
    
    int sectionTable = (int)[self.tableDataBillView.sectionDataIndex count];
    for (int i = 1; i < sectionTable+1; i++) {
        NSIndexSet * sections = [NSIndexSet indexSetWithIndex:i];
        [blockSelf.tableDataBillView.mTableViewBill reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    }   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            [blockSelf.tableDataBillView.mTableViewBill setFrame:tableDataBillSize];
        });
    });
}
-(void) printSubBillView:(int)num{
    [loadProgress.loadView showWithStatus];
    PrintReceiptViewController *print = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"PrintReceiptViewController1"];
    [print setTitle:self.title];
    [print setTableDataBillView:self.tableDataBillView];
    [print setTableSingleBillView:self];
    [print setIsPrintSubBill:YES];
    [print setIsPrintAllSubBill:NO];
    [print setDataSubbill:dataSubbill];
    [print setIndexSubBill:[@(num+1) stringValue]];
    
    [blockSelf.tableDataBillView setDataImagePrint:[NSMutableDictionary dictionary]];
    [blockSelf.tableDataBillView setIsAdddataPrint:YES];
    [blockSelf.tableDataBillView setIsPrintSubBill:YES];
    [blockSelf.tableDataBillView setIsPrintAllSubBill:NO];
    [blockSelf.tableDataBillView setIndexSubBill:[@(num+1) stringValue]];
    tableDataBillSize = self.tableDataBillView.mTableViewBill.frame;
    [self setHeightTable];
    [blockSelf.tableDataBillView.mTableViewBill reloadData];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self  presentViewController:print animated:YES completion:nil];
            [blockSelf.tableDataBillView.mTableViewBill setFrame:tableDataBillSize];
        });
    });
}
-(void)setHeightTable{
    int heightmTableInvoice = 300;
    heightmTableInvoice += self.tableDataBillView.mTableViewBill.contentSize.height + self.tableDataBillView.mTableViewBill.frame.origin.y;
    CGRect frame = CGRectMake(0, 0,self.tableDataBillView.mTableViewBill.frame.size.width,heightmTableInvoice);
    [blockSelf.tableDataBillView.mTableViewBill setFrame:frame];
}

-(void) loadTableBillData{

    self.tableDataBillView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableDataBill"];
    
    [self.tableDataBillView setDetailTable:self.detailTable];
    [self.tableDataBillView setTableSingleBillViewController:self];
    
    if([self.role isEqualToString:@"admin"]){
        [self.tableDataBillView setRole:@"admin"];
    }
    
    self.tableDataBillView.view.frame = self.mContainerTableBill.bounds;
    [self addChildViewController:self.tableDataBillView];
    [self.mContainerTableBill addSubview:self.tableDataBillView.view];
    [self.tableDataBillView didMoveToParentViewController:self];
}
-(void) setButtonMenuView{
    Member *member = [Member getInstance];
    [member loadData];
    
    NSDictionary *dicres = globalBill.billResturant;
    bool bl = [dicres[@"has_drink"] boolValue];
    
    NSString *nameButton ;
    if (!bl || [member.girlID isEqual:@"0"] || self.isStaffTake) nameButton = @"notButtonDrink";
    else nameButton = @"allButton";
    
    ButtonMenuView  *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:nameButton];
    if (![self.tableSuperClass isKindOfClass:[TableOpenViewController class]] && ![self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
          if ([self isCashTableStatus]) {
           [pager setNotClike:YES];
        }
    }
    
    [pager setTableSingleBillViewController:self];
    pager.view.frame = self.mContainerMenu.bounds;
    [self addChildViewController:pager];
    [self.mContainerMenu addSubview:pager.view];
    [pager didMoveToParentViewController:self];
}

-(void) createButtonTitleView{
    NSString *bill_id = self.dataListSingBill[@"bill_id"];
    titleLabelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    titleLabelButton.frame = CGRectMake(0, 0,70, 44);
    titleLabelButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [titleLabelButton addTarget:self action:@selector(didTapTitleView:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.titleView = titleLabelButton;
    
    [self setTitleLabelButton:bill_id];
    
}
-(void)setTitleLabelButton:(NSString *)bill_id{

    NSArray *bill_split_list = self.dataListSingBill[@"bill_split_list"];
    NSInteger i = 0;
    
    if ([Check checkNull:bill_id]) {
        i = [[bill_split_list lastObject] integerValue];
    }
    else{
        i = [bill_split_list indexOfObject:bill_id];
    }
    
    NSString *title_bill;
    if (![Check checkNull:bill_split_list] && !self.isCashTableStatus && !self.isStaffTake && !self.isCashierTake  && [bill_split_list count] >= 2) {
        
        title_bill = [NSString stringWithFormat:@"%@ %@ %ld",self.titleTable,AMLocalizedString(@"บิลหลักที่", nil),(long)i+1];
    }
    else
        title_bill = self.titleTable;
    
    [titleLabelButton setTitle:title_bill forState:UIControlStateNormal];
}
- (IBAction)didTapTitleView:(id) sender
{
    if (!self.isCashTableStatus || self.isPrintSubBill) {
        NSLog(@"Title tap");
        popupView = [[PopupView alloc] init];
        [popupView setTableSingleBillViewController:self];
        [popupView createPopoverDrinkMenu:self];
    }
}
-(NSMutableArray *) sortDataOrder:(NSMutableArray *)data1{
    [data1 sortUsingComparator:
     ^(id obj1, id obj2)
     {
         NSInteger value1 = [[obj1 objectForKey: @"dataorder"][@"subbill_id"] intValue];
         NSInteger value2 = [[obj2 objectForKey: @"dataorder"][@"subbill_id"] intValue];
         if (value1 > value2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         if (value1 < value2){
             return (NSComparisonResult)NSOrderedAscending;
         }
         return (NSComparisonResult)NSOrderedSame;
     }];
    
    int count = 0;
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:[data1 copy]];
    NSMutableArray *addData = [[NSMutableArray alloc] init];
    NSMutableArray *listd = [NSMutableArray arrayWithArray:[dataSubbill allValues]];
    NSDictionary *dataSubbill_1 = (NSDictionary *)[Time sortDateDescriptors:listd];
    
    for (NSDictionary *index in dataSubbill_1) {
        @try {
            NSString *keyBill = index[@"id"];
            NSString *value = @"subbill_id = %@";
            NSArray *data = [NSDictionary searchDictionaryALL:keyBill andvalue:value andData:[arr valueForKey:@"dataorder"]];
            if ([data count] != 0) {
                [addData insertObject:@{@"subbill":[NSNumber numberWithInt:count],
                                        @"index":keyBill}
                              atIndex:[addData count]];
                count ++;
                for (NSDictionary *dic in arr) {
                    NSString *value1 = [NSString checkNull:dic[@"dataorder"][@"subbill_id"]];
                    if ([keyBill isEqual:value1]) {
                        [addData addObject:dic];
                    }
                }
                if (self.isCashTableStatus) { //  isCashTableStatus
                    NSMutableDictionary *dic = [NSMutableDictionary dictionaryMutableWithObject:index];
                    [dic setObject:@"TotalSubbill" forKey:@"index"];
                    [addData addObject:dic];
                }
            }
        }
        @catch (NSException *exception) {
            DDLogError(@"error exception sortDataOrder : %@",exception);
        }
    }
    
    numSubBill =  [@(count+1) stringValue];
//    NSString *keyBill = @"*999";
//    NSString *value = @"index = %@";
//    NSArray *data = [NSDictionary searchDictionaryALL:keyBill andvalue:value andData:arr];
//    if ([data count] != 0) {
//           [addData addObject:data[0]];
//    }
 
    
    NSString *keyBill = @"totalBill";
    NSString *value = @"index = %@";
    NSArray *data = [NSDictionary searchDictionaryALL:keyBill andvalue:value andData:arr];
    if ([data count] != 0) {
          [addData addObject:data[0]];
    }
  
//    for (NSDictionary *string in addData) {
//        NSLog(@"%@", string[@"dataorder"][@"subbill_id"]);
//    }
    return addData;
}

#pragma mark - create popover Controller

- (void)createPopoverController:(id)sender {
    UIButton *btn = (UIButton *)sender;
    UITableViewController *viewController = [[UITableViewController alloc] init];
    viewController.title = AMLocalizedString(@"รายการ", nil);
    tableSreachPop =  viewController.tableView;
    tableSreachPop.delegate = self;
    tableSreachPop.dataSource = self;
    
    CGSize size;
    if (btn.tag == 333) {
         size = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-50);
        [tableSreachPop setTag:btn.tag];
    }
    else{
        if (![Check  checkDevice]) {
            size = CGSizeMake(320,320);
        }
        else{
            size = CGSizeMake(self.view.frame.size.width-50,320);
        }
    }
    
    viewController.preferredContentSize = CGSizeMake(size.width,size.height);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    
    if (btn.tag == 333) {
        [popoverController presentPopoverFromRect:btn.bounds
                                                 inView:btn
                               permittedArrowDirections:WYPopoverArrowDirectionAny
                                               animated:YES
                                                options:WYPopoverAnimationOptionFadeWithScale];
    }
    else{
        [popoverController presentPopoverFromBarButtonItem:sender
                                  permittedArrowDirections:WYPopoverArrowDirectionAny
                                                  animated:NO];
    }
}
#pragma mark - delete table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 333) {
        return [self.passivePromotion count];
    }
    return  4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenfine = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenfine];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenfine];
    }
    if (tableView.tag == 333) {
        NSInteger row = indexPath.row;
        NSDictionary *data = self.passivePromotion[row];
        [cell.textLabel setText:data[@"name"]];
    }
    else{
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = AMLocalizedString(@"แสดงทั้งหมด", nil);
                break;
            case 1:
                cell.textLabel.text = AMLocalizedString(@"แสดงอาหาร", nil);
                break;
            case 2:
                cell.textLabel.text = AMLocalizedString(@"แสดงเครื่องดื่ม", nil);
                break;
            case 3:
                cell.textLabel.text = AMLocalizedString(@"แสดงดริ้ง", nil);
                break;
            default:
                break;
        }
    }
    cell.textLabel.textColor = [UIColor ht_leadDarkColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger tag = tableView.tag;
    if (tag == 333) {
        NSLog(@" passsive pro");
        dataSeclectPassivePro = [NSDictionary dictionaryWithObject:self.passivePromotion[indexPath.row]];
        [self.mPassivePro setTitle:dataSeclectPassivePro[@"name"] forState:UIControlStateNormal];
       
        if (![dataSeclectPassivePro[@"id"] isEqualToString:@"-333"]) {
            idPassivePro = dataSeclectPassivePro[@"id"];
              blSeclectPassivePro = YES;
        }
        else{
            idPassivePro = @"0";
              blSeclectPassivePro = NO;
        }
        NSString *idBill = self.detailTable[@"id"];
        [TableSingleBillViewController loadDataOpenTableCashier:idBill];
        
        [billSeclectPassivePro setValue:dataSeclectPassivePro forKey:idBill];
        [popoverController dismissPopoverAnimated:YES];
    }
    else{
        switch (indexPath.row) {
            case 0:{
                [self sortMenuOrder:@"0"];
            }
                break;
            case 1:{ // food
                [self sortMenuOrder:@"1"];
            }
                break;
            case 2:{ // drink
                [self sortMenuOrder:@"2"];
            }
                break;
            case 3:{ // drinkgirls
                [self sortMenuOrder:@"3"];
            };
                break;
            default:
                break;
        }
    }
}
-(void) sortMenuOrder:(NSString *)numSort{
    if (tempSectionData == nil) {
        tempSectionData = [NSDictionary dictionaryWithObject:self.tableDataBillView.sectionBill[@"0"]];
    }
    NSMutableDictionary *addFoodBillList;
    if ([tempSectionData count] != 0 && ![numSort isEqual:@"0"]) {
        NSMutableArray *addFoodBill = [NSMutableArray array];
        NSMutableDictionary *addFoodBillList_1 = tempSectionData[@"data"];
        for (NSDictionary *dic in addFoodBillList_1) {
            if ([dic[@"dataorder"][@"menu_type"] isEqual:numSort] || dic[@"subbill"] != nil) {
                [addFoodBill addObject:dic];
            }
        }
        
        addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:@{@"data":addFoodBill,
                                                                                               @"commit":@"1"}];
        [self.tableDataBillView.sectionBill setObject:addFoodBillList forKey:@"0"];
        
    }
    else if ([numSort isEqual:@"0"]){
        [self.tableDataBillView.sectionBill setObject:tempSectionData forKey:@"0"];
        addFoodBillList = [NSMutableDictionary dictionaryMutableWithObject:tempSectionData];
    }
    int i = 0;
    for (NSDictionary *dic in self.tableDataBillView.sectionDataIndex) {
        if (dic[@"ORDER"] != nil) {
            NSLog(@"order");
            NSMutableDictionary *data  = [NSMutableDictionary dictionaryWithObject:addFoodBillList forKey:@"ORDER"];
            [self.tableDataBillView.sectionDataIndex replaceObjectAtIndex:i withObject:data];
        }
        i++;
    }
    
    if ([self.tableDataBillView.sectionBill count] != 0) {
        NSLog(@"self.tableDataBillView.sectionBill------------ yes ");
    }
    else
        NSLog(@"self.tableDataBillView.sectionBill------------ no ");

    [self.tableDataBillView.mTableViewBill reloadData];
    [popoverController dismissPopoverAnimated:YES];
}
#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender // ยกเลิก prepareForSegue
{
    if (![self.tableSuperClass isKindOfClass:[CustomNavigationController class]]) {
        if([identifier isEqualToString:@"tableSingleBillSub"] && [self isCashier]) {
            [self createPopoverController:sender];
            return NO;
        }
    }    
    return YES;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    NSLog(@"prepareForSegue :%@",segue.identifier);
    
    if ([segue.identifier isEqualToString:@"cpdc_check_embed"]) {

        UIViewController *controller = segue.destinationViewController;
        ButtonMenuView *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ButtonMenuView"];
        UIViewController  *_tableAllZone = [pager.storyboard instantiateViewControllerWithIdentifier:@"allButton"];
        [pager.view addSubview:_tableAllZone.view];
        
        pager.view.bounds = controller.view.bounds;
        [controller addChildViewController:pager];
        [controller.view addSubview:pager.view];
        [pager didMoveToParentViewController:controller];
    }
    else if ([segue.identifier isEqualToString:@"requirePromtion"] && !self.isCashier) {
        TablePromotiomViewController *_viewController = segue.destinationViewController;
        [_viewController setTableSingleBillViewControll:self.tableDataBillView.tableSingleBillViewController];
        [_viewController loadDataPromotionRequirePromtion];
    }
    else if ([segue.identifier isEqualToString:@"tableSingleBillSub"]) {
     TableSubBillViewController *controller = segue.destinationViewController;
     [controller setDetailTable:self.detailTable];
     [controller setTitleTable:self.titleTable];
     [controller setNumSubBill:numSubBill];
     [controller setIsStaffTake:self.isStaffTake];
     [controller setIsCashierTake:self.isCashierTake];
    }
}
-(void) alertShowQRCode{
    Member *member = [Member getInstance];
    [member loadData];
    
    NSString *tables = [NSString checkNull:self.detailTable[@"id"]];
    NSDictionary *usage = [NSDictionary dictionaryWithObject:self.dataListSingBill[@"usage"]];
    NSDictionary *dataTable = usage[[NSString stringWithFormat:@"table_id_%@",tables]];
    NSString *url = [NSString stringWithFormat:GET_QRCODE,member.nre];
    url = [url stringByAppendingString: dataTable[@"otp"]];
    popupView = [[PopupView alloc] init];
    [popupView setTanPopup:44];
    [popupView setTitlePop:AMLocalizedString(@"แสดง QR Code", nil)];
    [popupView setTableSingleBillViewController:self];
    
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.clipsToBounds = YES;
    imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [imgView setImageWithURLRequest :imageRequest placeholderImage:nil success:^(NSURLRequest *request,NSHTTPURLResponse *response,UIImage *image){
        [imgView setImage:image];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [imgView setNeedsLayout];
        [popupView setImagePop:imgView.image];
        [popupView showPopupWithStyle:CNPPopupStyleCentered];
    }failure:^(NSURLRequest *request,NSHTTPURLResponse *response, NSError *error){
        DDLogError(@"error alertShowQRCode :%@",error);
    }];
}
-(void) alertShowCasiher{
    NSString *title = [NSString stringWithFormat:AMLocalizedString(@"คุณต้องการแจ้ง แคชเชียร์ เช็คบิลโต๊ะ %@ ใช่หรือไม่", nil),self.detailTable[@"label"]];
    popupView = [[PopupView alloc] init];
    [popupView setStatus:@"1"];
    [popupView setTanPopup:999];
    [popupView setTitlePop:title];
    [popupView setTableSingleBillViewController:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
    NSLog(@"replt");
}
@end
