//
//  TableDrinkGirlsViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "KRLCollectionViewGridLayout.h"
#import "TableCollectionViewCell.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "LoadProcess.h"

@interface TableDrinkGirlsViewController : UIViewController<THSegmentedPageViewControllerDelegate,UISearchBarDelegate,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchField;
@property (weak, nonatomic) IBOutlet UICollectionView *mCollectionItem;

@property (nonatomic,strong) NSString *orderID;
@property (nonatomic,strong) NSMutableDictionary *drinkMenu;
@property (nonatomic,strong) NSString *typeDr;
@property (nonatomic,strong) NSString *typeDrinkMenu;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnBackHome:(id)sender;
- (void)btnReply:(UIImage *)singImage;
- (IBAction)getImageBtnPressed:(UIImage *)sender;
@end