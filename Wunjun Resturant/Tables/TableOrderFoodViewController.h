//
//  TableOrderFoodViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "CellTableViewAll.h"
#import "CNPPopupController.h"
#import "Constants.h"
#import "LoadProcess.h"
#import "FoodItemsOrderViewController.h"
#import "LocalizationSystem.h"

@interface TableOrderFoodViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView *mTableOrgerFood;
@property (weak, nonatomic)  IBOutlet UILabel *mTitleListFood;
@property (weak, nonatomic)  IBOutlet UIImageView *mImageListFood;

@property (nonatomic,weak)   NSDictionary *dataMenuList;
@property (nonatomic,strong) NSString *strTitleFood;
@property (nonatomic,strong) UIImage *imgTitleFood;
@property (nonatomic,strong) NSString *priceFood;
@property (nonatomic,strong) FoodItemsOrderViewController *FoodItemsOrderView;

- (IBAction)tagKeybroad:(id)sender;
- (IBAction)buttonTap:(id)sender;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnReply:(id)sender;
- (IBAction)btnBackHome:(id)sender;
@end
