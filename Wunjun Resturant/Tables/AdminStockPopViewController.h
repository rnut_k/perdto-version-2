//
//  AdminStockPopViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 4/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdminStockPopViewController : UIViewController

@property (nonatomic,strong) NSString *systemAmount;
@property (nonatomic,strong) NSString *amount;
@property (nonatomic,strong) NSString *comment;
@property (weak, nonatomic) IBOutlet UILabel *systemAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UITextView *commentTxt;

//label

@property(nonatomic,weak) IBOutlet UILabel *labelAmount;
@property(nonatomic,weak) IBOutlet UILabel *labelRealAmount;
@end
