//
//  AdminStockPopViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 4/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AdminStockPopViewController.h"

@interface AdminStockPopViewController ()

@end

@implementation AdminStockPopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpLabel];
    // Do any additional setup after loading the view.
    
    self.systemAmountLbl.text = self.systemAmount;
    self.amountLbl.text = self.amount;
    self.commentTxt.text = self.comment;
}

- (void)setUpLabel {
    _labelAmount.text = AMLocalizedString(@"amount", nil);
    _labelRealAmount.text = AMLocalizedString(@"real_amount", nil);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
