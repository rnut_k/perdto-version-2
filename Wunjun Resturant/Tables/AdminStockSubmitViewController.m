//
//  AdminStockSubmitViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 4/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AdminStockSubmitViewController.h"

@interface AdminStockSubmitViewController ()
{
    CustomNavigationController *vc;
    NSMutableArray *rowData;
    NSDictionary *dailyStock;
    NSDictionary *menuData;
    NSDictionary *errorData;
    
    WYPopoverController *popoverController;
    AdminStockPopViewController *adminStockPopUp;
}
@end

@implementation AdminStockSubmitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    rowData = [[NSMutableArray alloc]init];
    
    vc = (CustomNavigationController *)self.parentViewController;
    self.reportTitle.text = vc.title;
    self.title = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"รายงานสต็อก", nil),vc.title];
    dailyStock = vc.dataStockSubmit[@"daily_stock"];
    menuData   = vc.dataStockSubmit[@"menu"];
    errorData  = vc.dataStockSubmit[@"error_stock"];
    
    for(NSDictionary *row in vc.dataStockSubmit[@"daily_stock"]){
        [rowData addObject:row];
    }
    [self.reportTable reloadData];
//    [self setUpLabel];
}
-(void)setUpLabel{
    _labelReport.text = AMLocalizedString(@"report", nil);
    _labelCheckStock.text = AMLocalizedString(@"checkstock", nil);
}
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [rowData count] + 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        NSString *cellIdentifier = @"CellITemInvoice";
        
        if (indexPath.row == 0) {
            CellTableViewAll *cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:@"CellTilteStockManage"];
            return cell;
        }
        
        CellTableViewAll *cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *info = rowData[indexPath.row - 1];
        if([info[@"checkin_menu_id"] isKindOfClass:[NSNull class]])
        {
            cell.mLabel.text = AMLocalizedString(@"ไม่มีข้อมูล", nil);
            cell.mCount.text = AMLocalizedString(@"0 หน่วย", nil);
            cell.mTotalFood.text = @"0";
        }
        else
        {
            NSString *keyId = [NSString stringWithFormat:@"id_%@", info[@"checkin_menu_id"]];
            cell.mLabel.text = menuData[keyId][@"name"];
            int stockDiff = [info[@"amount"] intValue] - [info[@"system_amount"] intValue];
            cell.mCount.text = [NSString stringWithFormat:@"%d %@",stockDiff, menuData[keyId][@"unit_label"]];
            cell.mTotalFood.text = [NSString stringWithFormat:@"%@ %@",info[@"amount"], menuData[keyId][@"unit_label"]];
            cell.mTotalFood.textColor = [UIColor ht_aquaColor];
            
            if(stockDiff < 0){
                cell.mUporDown.image = [UIImage imageNamed:@"select_down"];
            }else if(stockDiff > 0){
                cell.mUporDown.image = [UIImage imageNamed:@"select_up"];
            }
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"error CellITemInvoice : %@",exception);
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if(indexPath.row == 0){
        return;
    }
    NSDictionary *info = rowData[indexPath.row - 1];
    if([info[@"checkin_menu_id"] isKindOfClass:[NSNull class]])
        return;
    NSString *keyId = [NSString stringWithFormat:@"id_%@", info[@"checkin_menu_id"]];
    NSString *keyMenuId = [NSString stringWithFormat:@"menu_id_%@", info[@"checkin_menu_id"]];
//    
    adminStockPopUp = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"AdminStockPopView"];
    [adminStockPopUp setTitle:menuData[keyId][@"name"]];
    [adminStockPopUp setAmount:info[@"amount"]];
    [adminStockPopUp setSystemAmount:info[@"system_amount"]];
    
    if ([errorData count] != 0) {
        NSString *descript = ([errorData[keyMenuId][@"description"] isKindOfClass:[NSNull class]])?@"":errorData[keyMenuId][@"description"];
        [adminStockPopUp setComment:descript];
    }
    
    float popS = [Check checkDevice]?self.view.frame.size.width-50:300;
    adminStockPopUp.preferredContentSize = CGSizeMake(popS,popS);
    adminStockPopUp.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:adminStockPopUp];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)dismissModal:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
