//
//  TableZoneCheckViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableZoneCheckViewController.h"
#import "LoadProcess.h"
#import "Check.h"

@interface TableZoneCheckViewController()<CNPPopupControllerDelegate>{
    LoadProcess *loadProgress;
    
     UIRefreshControl *refreshControl;
}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation TableZoneCheckViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.collectionView.collectionViewLayout;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.layout.numberOfItemsPerLine = [[actionSheet buttonTitleAtIndex:buttonIndex] integerValue];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.layout.numberOfItemsPerLine = self.numTable;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
    
    loadProgress  = [LoadProcess sharedInstance];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)updateTitleLabels {
    
}
- (void)refershControlAction
{
    NSLog(@"Reload grid");
    
    [LoadMenuAll retrieveData:self.dataDetailZone[@"id"] success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setDataListTables:dataTables];
            [self.collectionView reloadData];
            [refreshControl endRefreshing];
        });
    }];
    
    
}
- (NSString *)viewControllerTitle
{
    return _viewTitle ? _viewTitle : self.title;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataListTables count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
    if (dic != nil) {
        cell.userNameLabel.text = [NSString stringWithFormat:@"%@",dic[@"label"]];
        NSDictionary *dataTable = self.tableOpenSumView.dataTable;
        BOOL bl  = ([dataTable[@"id"] isEqual:dic[@"id"]] && ([dataTable[@"label"] isEqual:dic[@"label"]])) ? YES:NO;
        if ([dic[@"status"] intValue]!= 1) {     // สถานะโต็ะ
            cell.userNameLabel.textColor = [UIColor whiteColor];
            cell.mImageTable.image = [UIImage imageNamed:@"board_off"];
            cell.tag = 1;
        }
        else{
            cell.userNameLabel.textColor = [UIColor ht_leadColor];
            cell.mImageTable.image = [UIImage imageNamed:@"board_on"];
            if (bl) {
                cell.mImageCheck.image = [UIImage imageNamed:@"check"];
                 cell.tag = 1;
            }
            else
                cell.tag = 0;
        }
    } 
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    TableCollectionViewCell *_cell = (TableCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
    UIImage *_img =  _cell.mImageCheck.image;
    
    if (_cell.tag != 1) {
        if (_img != nil) {
            _cell.mImageCheck.image = nil;
            [self.tableOpenSumView.addOpenTable removeObjectForKey:[@(indexPath.row) stringValue]];
        }
        else{
            _cell.mImageCheck.image = [UIImage imageNamed:@"check"];
            [self.tableOpenSumView.addOpenTable setObject:dic[@"id"] forKey:[@(indexPath.row) stringValue]];
        }
    }
    else{
         [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"โต๊ะไม่ว่าง", nil)];
    }
    
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    UIDeviceOrientation ori = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(ori)) {
        self.layout.numberOfItemsPerLine = [Check checkDevice]?6:8;
    }
    else{
        self.layout.numberOfItemsPerLine = [Check checkDevice]?3:6;
    }
}
@end
