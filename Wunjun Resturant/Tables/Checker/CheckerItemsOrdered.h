//
//  CheckerItemsOrdered.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "ECSlidingViewController.h"
#import "SWTableViewCell.h"


@interface CheckerItemsOrdered : UITableViewController<ECSlidingViewControllerDelegate,SWTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mtableListItenOrder;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
//@property (nonatomic ,strong) SWTableViewCellDelegate *delegate;

- (IBAction)menuButtonTapped:(id)sender;
@end
