//
//  CheckerInvoiceOrderController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CheckerInvoiceOrderController.h"
#import "CheckerpPopUpAddGoods.h"
#import "CustomNavigationController.h"
#import "FilterInvioceViewController.h"
#import "CellCheckAmountTableViewCell.h"

#import "MEDynamicTransition.h"
#import "METransitions.h"
#import "UIViewController+ECSlidingViewController.h"
#import "WYPopoverController.h"
#import "CellTableViewAll.h"
#import "GlobalBill.h"
#import "Member.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "Check.h"
#import "UIColor+HTColor.h"
#import "NSString+GetString.h"
#import "InvoiceDB.h"
#import "LoadProcess.h"
#import "KVNViewController.h"
#import <sys/sysctl.h>

#define kPopUpAddGoods             (@"popUpAddGoods")
//#define kPopUpEditInvoice          (@"popUpEditInvoice")
#define kPopUpStoreCheck           (@"popUpStoreCheck")
//#define kPopUpCheckinInfo          (@"popUpCheckinInfo")
#define kPopUpPendingInvoiceAdmin  (@"popUpPendingInvoiceAdmin") // ครั้งที่ 2
#define kPopUpApprovalInvoiceAdmin (@"popUpApprovalInvoiceAdmin") // ครั้งที่ 1


#define kCellStockReport (@"CellStockReport")
#define kCellITemInvoice (@"CellITemInvoice")
#define kCellTotalInvoice (@"cellTotalInvoice")

#define kCellTitleCreateInvioce (@"cellTitleCreateInvioce")
#define kCellDetailInvioce      (@"cellDetailInvioce")
#define kCellTitleCellAmount    (@"cellTitleCellAmount")
#define kCellTotalAmount        (@"cellTotalAmount")
#define kCellDetailAmount       (@"cellDetailAmount")
#define kCellTitleAppriove      (@"cellTitleAppriove")
#define kCellDetailAppriove     (@"cellDetailAppriove")

@interface CheckerInvoiceOrderController()<WYPopoverControllerDelegate>{
    WYPopoverController *popoverController;
    CheckerpPopUpAddGoods *checkerpPopUpAddGoods;
    CustomNavigationController *vc;
    LoadProcess *loadProcess;
    
    NSMutableArray *dataManageStockTemp;
    NSMutableArray *dataManageStock;
    NSMutableArray *dataListItemOrder;
    NSDictionary   *dataInvoice;
    NSMutableArray *dataCheckIn;
    
    UIButton *checkboxButton;
    NSMutableArray *dataAddInvoice;
    NSMutableDictionary *dataEditInvoice;
    
    int indexManage;
    NSIndexPath *indexPathManage;
    NSMutableDictionary *dataEditManageStock;
    
    BOOL isStockReport; //store this value for easier checking role admin
    BOOL isCheckInvoiceDB;
    
    NSMutableArray *updateData;
    NSMutableArray *insertData;
    
    BOOL numEdit;
    
    NSArray *filterData;
    
    int trueAmountTotal;
    int estimatedAmountTotal;
    
    NSString *nVendor;
    
    int heightmTableInvoice;
    
    __block NSMutableDictionary *dataVender;
    
    // search
    BOOL isSearching;
    NSMutableArray *filteredDataManageStock;
    
    
    
    
}
@property (nonatomic ,strong) InvoiceDB *invoiceDB;
@property (nonatomic, strong) Member *member;
@property (nonatomic, strong) METransitions *transitions;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;




@end

@implementation CheckerInvoiceOrderController

#pragma mark init value
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

-(InvoiceDB *)invoiceDB{
    if (!_invoiceDB) {
        _invoiceDB = [InvoiceDB getInstance];
        if (self.blNewInvoice) {
            [_invoiceDB loadData:@"NewInvoice"];
        }
        else if (vc.blIsStockManege){
            [_invoiceDB loadData:@"IsStockManege"];
        }
        else{
            [_invoiceDB loadData:self.dataEditItem[@"id"]];
        }
    }
    return _invoiceDB;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpLabel];
    loadProcess = [LoadProcess sharedInstance];
    [loadProcess.loadView showWithStatus];
    
    dataAddInvoice      = [NSMutableArray array];
    dataListItemOrder   = [NSMutableArray array];
    dataEditManageStock = [NSMutableDictionary dictionary];
    dataEditInvoice     = [NSMutableDictionary dictionary];     // เก็บข้อมูลที่แก้ไข
    filteredDataManageStock = [[NSMutableArray alloc] init];    // ค้นหาสินค้า
    
    DeviceData *deviceData = [DeviceData getInstance];
    filterData = [NSArray arrayWithArray:deviceData.vendor];    // เก็บข้อมูล ตัวเลือก ในการสรา้งบิล
    
    vc = (CustomNavigationController *)self.parentViewController;
    if ([vc blIsStockReport] || [vc blIsStockManege] || self.adminStockReport)
    {
        NSString *date_ = [Time covnetTimeDMY];
        [self.mTime setText:[NSString stringWithFormat:@"%@ %@ ",AMLocalizedString(@"วันที่", nil),date_]];
        if ([vc blIsStockReport] || self.adminStockReport) {
            isStockReport = YES;
            [self setTitle:AMLocalizedString(@"รายงานสต็อก", nil)];
        }
        else if ([vc blIsStockManege]){
            [self setTitle:[NSString stringWithString:AMLocalizedString(@"ตรวจเช็คสต็อก", nil)]];
        }
        if (!self.adminStockReport) {
            [self loadDataManageStock:@"get"];
        }

    }
    if (self.blNewInvoice) {
        NSString *textTitle = [NSString  stringWithFormat:@"%@ %@ ",AMLocalizedString(@"ใบสั่งซื้อ", nil),[Time covnetTimeDMY1:[Time getTimeALL]]];
        [self setTitle:textTitle];
        nVendor = AMLocalizedString(@"คู่ค้าทั้งหมด", nil);
        [self loadDataManageStock:@"get"];
        [self initNavigationItem];
    }
    else if (self.blEditNotApproved){
        [self initNavigationBack];
        [self setTitle:AMLocalizedString(@"รายงานสินค้าที่ต้องซื้อ", nil)];
        NSString *date_ = [Time covnetTimeDMY1:self.dataEditItem[@"invoice_date"]];
        [self.mTime setText:[NSString stringWithFormat:@"%@ %@ ",AMLocalizedString(@"วันที่", nil),date_]];
        [self loadDataManageStock:@"get"];
        [self.buttomBtn setTitle:AMLocalizedString(@"ยืนยันการแก้ไข", nil) forState:UIControlStateNormal];
    }
    else if (self.blEditApproved){
        [self initNavigationBack];
        NSString *date_ = [Time covnetTimeDMY1:self.dataEditItem[@"invoice_date"]];
        [self.mTime setText:[NSString stringWithFormat:@"%@ %@ ",AMLocalizedString(@"วันที่", nil),date_]];
        if ([self.role isEqualToString:@"admin"]) {
            [self.mNameVendor setText:self.nameVendor];
        }
        [self setTitle:AMLocalizedString(@"รายงานสินค้าที่ต้องซื้อ", nil)];
    }
    
    if([self.role isEqualToString:@"admin"]){   //fix add item to approve btn
        [self.mNameVendor setText:self.nameVendor];
         NSString *date_ = [Time covnetTimeDMY];
        [self.mTime setText:date_];
        [self initNavigationBack];
        [self.buttomBtn setTag:5];
        [self loadDataManageStock:@"get"];
    
        self.navigationItem.rightBarButtonItem.title = AMLocalizedString(@"แก้ไข", nil);
        int numStatus = [self.dataEditItem[@"status"] intValue];
        
        if(numStatus == 1)
        {
            NSString *date_ = [Time covnetTimeDMY1:self.dataEditItem[@"invoice_date"]];
            [self setTitle:[NSString stringWithFormat:@"%@ %@ ",AMLocalizedString(@"ใบสั่งซื้อ", nil),date_]];
            [self.mTime setText:AMLocalizedString(@"รายงานสินค้าที่ต้องซื้อ", nil)];
            
            [self.buttomBtn setTitle:AMLocalizedString(@"อนุมัติการสั่งซื้อ", nil) forState:UIControlStateNormal];
            [self.buttomBtn setImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
            [self.buttomBtn setBackgroundColor:[UIColor ht_cloudsColor]];
            [self.buttomBtn setTitleColor:[UIColor ht_grassColor] forState:UIControlStateNormal];
            [self.buttomBtn setTitleColor:[UIColor ht_grassDarkColor] forState:UIControlStateHighlighted];
        }
        else if(numStatus == 2)
        {
            self.navigationItem.rightBarButtonItem = nil;
            [self.buttomBtn setTitle:AMLocalizedString(@"กำลังดำเนินการ", nil) forState:UIControlStateNormal];
            [self.buttomBtn setBackgroundColor:[UIColor grayColor]];
            [self.buttomBtn setEnabled:NO];
            [self.buttomBtn setImage:nil forState:UIControlStateNormal];
        }
        else if(numStatus == 3)
        {
            [self.buttomBtn setTitle:AMLocalizedString(@"อนุมัติ", nil) forState:UIControlStateNormal];
            [self.buttomBtn setImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
            [self.buttomBtn setBackgroundColor:[UIColor ht_cloudsColor]];
            [self.buttomBtn setTitleColor:[UIColor ht_grassColor] forState:UIControlStateNormal];
            [self.buttomBtn setTitleColor:[UIColor ht_grassDarkColor] forState:UIControlStateHighlighted];
        }
        else if(numStatus == 4)
        {
            self.layoutHightButtonAdd.constant = 0.0f;
            self.navigationItem.rightBarButtonItem = nil;
            [self.buttomBtn setTitle:AMLocalizedString(@"เรียบร้อยทุกขั้นตอน", nil) forState:UIControlStateNormal];
            [self.buttomBtn setEnabled:NO];
            [self.buttomBtn setImage:nil forState:UIControlStateNormal];
            [self.buttomBtn setBackgroundColor:[UIColor ht_grassDarkColor]];
        }
    }
    [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
     setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],
                             NSForegroundColorAttributeName, nil]     forState:UIControlStateNormal];
}
-(void)setUpLabel{
    _labelItemList.text = AMLocalizedString(@"item_list", nil);
    _labelAmount.text = AMLocalizedString(@"amount", nil);
    
    
    
    
    if(self.blNewInvoice){
        [_buttomBtn setTitle:AMLocalizedString(@"create_invoice", nil) forState:UIControlStateNormal];
        [_buttomBtn setTitle:AMLocalizedString(@"create_invoice", nil) forState:UIControlStateNormal];
    }else{
        [_buttomBtn setTitle:AMLocalizedString(@"commit_buying", nil) forState:UIControlStateNormal];
        [_buttomBtn setTitle:AMLocalizedString(@"commit_buying", nil) forState:UIControlStateNormal];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self initEcsliding];
    if (self.blEditApproved && ![self.role isEqualToString:@"admin"]) {
//        self.layoutHightButtonAdd.constant = 0.0f;
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self setHeightTable];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [self initInvoiceDB];
    [LoadProcess dismissLoad];
}

- (NSString *)platformRawString {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}
-(void)setHeightTable{
//    NSString *platform = [self platformRawString];
    
    heightmTableInvoice = 60;
//    if ([platform isEqualToString:@"iPhone3,3"] || [platform isEqualToString:@"iPhone3,1"]){
//        heightmTableInvoice = 100;
//    }
    heightmTableInvoice += self.mTableInvoice.contentSize.height + self.mTableInvoice.frame.origin.y;
    
}
//- (void)updateViewConstraints {
//    if (self.view.superview != nil && [[self.view.superview constraints] count] == 0) {
//        NSDictionary* views = @{@"mTableInvoice" : self.view};
//        
//        [self.view.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[mTableInvoice]|" options:0 metrics:0 views:views]];
//        [self.view.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[mTableInvoice]|" options:0 metrics:0 views:views]];
//    }
//    [super updateViewConstraints];
//}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    if (self.blNewInvoice) {
          [self.mTableInvoice reloadSections:[NSIndexSet indexSetWithIndex:0]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}
-(void)initNavigationItem{
    UIImage *image = [[UIImage imageNamed:@"ic_update"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 40, 40)];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                           style:UIBarButtonItemStyleDone
                                                          target:self
                                                          action:@selector(selectFilter:)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}
-(void)initNavigationBack{
    UIImage *image = [[UIImage imageNamed:@"ic_back"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 40, 40)];
    UIBarButtonItem  *leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(menuButtonTapped:)];
    self.navigationItem.leftBarButtonItem = leftBarButtonItem;
}
-(void)initEcsliding{
    if(self.slidingViewController != nil)
    {
        self.transitions.dynamicTransition.slidingViewController = self.slidingViewController;
        self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping| ECSlidingViewControllerAnchoredGesturePanning;
        self.slidingViewController.customAnchoredGestures = @[];
        [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}
-(void)initInvoiceDB{
    if ((self.blEditApproved || self.blNewInvoice || vc.blIsStockManege) && !checkerpPopUpAddGoods.isApprovalInvoiceAdmin) {
        NSString *idOrder;
        NSString *cartJSON;
        
        if (self.blEditApproved) {
            idOrder = self.dataEditItem[@"id"];
            NSError *error = nil;
            cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dataEditInvoice
                                                                                                options:NSJSONWritingPrettyPrinted
                                                                                                  error:&error]
                                                       encoding:NSUTF8StringEncoding];
        }
        else if (vc.blIsStockManege){
            idOrder  = @"IsStockManege";
            NSError *error = nil;
            cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dataEditManageStock
                                                                                      options:NSJSONWritingPrettyPrinted
                                                                                        error:&error]
                                             encoding:NSUTF8StringEncoding];
        }
        else{   // self.blNewInvoice
            dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
            
            idOrder  = @"NewInvoice";
            NSMutableArray *listStock = [NSMutableArray array];
            for (NSDictionary *dic in dataManageStockTemp) {
                if (dic[@"amount"] != nil && ![dic[@"amount"] isEqualToString:@"0"]) {
                    [listStock addObject:dic];
                }
            }
            if ([listStock count] != 0) {
                NSError *error = nil;
                cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:listStock
                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                            error:&error]
                                                 encoding:NSUTF8StringEncoding];
            }
        }
        
        if ((([dataEditInvoice count] != 0 || self.blNewInvoice || vc.blIsStockManege)&& !isCheckInvoiceDB ) && cartJSON != nil) {
            if (![self.invoiceDB isKeyDB:idOrder]){
                [self.invoiceDB insertLoginData:cartJSON and:idOrder];
            }
            else{
                [self.invoiceDB updateDb:cartJSON andId:idOrder];
            }
        }
        else if (isCheckInvoiceDB){
            [self.invoiceDB updateDb:@"nil" andId:idOrder];
        }
    }
}
#pragma mark - Properties
- (METransitions *)transitions {
    if (_transitions) return _transitions;
    
    _transitions = [[METransitions alloc] init];
    
    return _transitions;
}
- (UIPanGestureRecognizer *)dynamicTransitionPanGesture {
    if (_dynamicTransitionPanGesture) return _dynamicTransitionPanGesture;
    
    _dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.transitions.dynamicTransition
                                                                           action:@selector(handlePanGesture:)];
    
    return _dynamicTransitionPanGesture;
}
-(void)loadDataReportSeles{
    @try {
        NSDictionary *parameters = [NSDictionary dictionary];
        parameters = @{@"code":self.member .code,
                       @"action":@"get",
                       @"invoice_id":self.dataEditItem[@"id"],
                       @"status":self.dataEditItem[@"status"],
                       @"last_id":@""};
        
        NSString *url = [NSString stringWithFormat:GET_MANAGE_INVOICE,_member.nre];
//        NSLog(@"%@ \n %@",parameters,url);
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            bool bl = [json[@"status"] boolValue];
            if (bl) {
                NSDictionary *data = [NSDictionary dictionaryWithObject:json[@"data"]];
                dataInvoice        = [NSDictionary dictionaryWithObject:[Check checkObjectType:data[@"invoice"]]];
                self.dataGoodsMenu = [NSDictionary dictionaryWithObject:[Check checkObjectType:data[@"goods_menu"]]];
                dataCheckIn        = [NSMutableArray arrayWithArray:[Check checkObjectType:data[@"checkin"]]];
                dataListItemOrder  = [NSMutableArray arrayWithArray:[self.dataGoodsMenu allValues]];
                
                if (dataCheckIn != nil && self.blEditNotApproved) {
                    [self checkDataInvoice:dataCheckIn];
                    filteredDataManageStock = [NSMutableArray arrayWithArray:dataManageStock];
                }
                else if (self.blEditApproved){
                     [self checkDataNewInvoice];
                }
                [self.mTableInvoice reloadData];
                [LoadProcess dismissLoad];
            }
        }];
    }
    @catch (NSException *exception) {
        DDLogError(@"error loadDataReportSeles : %@",exception);
    }
}

-(void)checkDataInvoice:(NSMutableArray *)dicList{
    @try {
        NSMutableArray *temp = [NSMutableArray arrayWithArray:[dataManageStock copy]];
        if ([dicList count] != 0) {
            for (int i = (int)[dicList count] -1; i > -1 ; i--) {
                NSDictionary *data = dicList[i];
                NSString *keyInfo = data[@"checkin_menu_id"];
                NSDictionary *dataOrder = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                               andvalue:@"(id = %@)"
                                                                                andData:temp];
                if ([dataOrder count] != 0) {
                    int index = (int)[dataManageStock indexOfObject:dataOrder];
                    [self moveObjectAtIndex:index toIndex:0];
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error checkDataInvoice : %@",exception);
    }
}
- (void)moveObjectAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    if (fromIndex < toIndex) {
        toIndex--; // Optional
    }
    
    id object = [dataManageStock objectAtIndex:fromIndex];
    [dataManageStock removeObjectAtIndex:fromIndex];
    [dataManageStock insertObject:object atIndex:toIndex];
}
-(void)loadDataManageStock:(NSString *)action{
    NSDictionary *parameters;
    
    if ([action isEqual:@"get"]) {
        parameters = @{@"code":self.member.code,
                       @"action":@"get"};
    }
    else{
        NSMutableArray *checkinList = [NSMutableArray array];
        for(NSDictionary *dataItem in dataManageStock){
            NSDictionary *data = [[[NSDictionary alloc]init]mutableCopy];
            NSString *amount   = dataItem[@"stock"];
            NSString *key      = [NSString stringWithFormat:@"id_%@", dataItem[@"id"]];
            if(dataEditManageStock[key] != nil)
            {
                NSDictionary *editData = dataEditManageStock[key];
                amount = editData[@"count"];
                
                if(![editData[@"comment"] isEqual:@"รายละเอียด"] && ![editData[@"comment"] isEqual:@""] && ![editData[@"comment"] isKindOfClass:[NSNull class]])
                {
                    NSDictionary *errorList = @{
                                                @"name":@"คลาดเคลื่อน",
                                                @"description":([editData[@"comment"] isEqual:@"รายละเอียด"])?@"":editData[@"comment"]
                                                };
                    [data setValue:errorList forKey:@"error_list"];
                }
            }
            NSDictionary *checkinMenu = @{
                                          @"id":dataItem[@"id"],
                                          @"stock":dataItem[@"stock"],
                                          @"amount":amount,
                                          @"type":dataItem[@"type"]
                                        };
            [data setValue:checkinMenu forKey:@"checkin_menu"];
            
            [checkinList addObject:data];
        }
       
        NSError *error = nil;
        NSString *checkinMenuDataJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:checkinList
                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                            error:&error]
                                                                          encoding:NSUTF8StringEncoding];
        parameters = @{@"code":self.member.code,
                       @"action":@"submit",
                       @"checkin_menu_data":checkinMenuDataJson};
    }

    NSString *url = [NSString stringWithFormat:GET_MANAGE_STOCK,_member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) { 
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            dataManageStock = [NSMutableArray arrayWithArray:json[@"data"]];
            filteredDataManageStock = [NSMutableArray arrayWithArray:dataManageStock];
            
            [self checkDataNewInvoice];
            dataManageStockTemp = [dataManageStock mutableCopy];
            
            if ((self.blEditNotApproved || self.role) && !self.adminStockReport) {
                [self loadDataReportSeles];
            }
            else if (vc.blIsStockManege && ![action isEqual:@"submit"]){
                [self checkDataManageStock];
                [self.mTableInvoice reloadData];
              
                [LoadProcess dismissLoad];
            }
            else{
                if ([action isEqual:@"submit"]) {
                    isCheckInvoiceDB = YES;
//                    
                    CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerMain"];
                    self.slidingViewController.topViewController = cashSalesReportView;
                }
                else
                     [self.mTableInvoice reloadData];
                
                [LoadProcess dismissLoad];
            }
        }
        else{
            [loadProcess.loadView showError];
        }
    }];
}
-(void)checkDataNewInvoice{
    @try {
        NSString *valueInvoice = [self.invoiceDB valueInvoice];
        if (![valueInvoice isEqualToString:@"nil"] && valueInvoice != nil) {
            NSMutableArray *temp = [NSMutableArray arrayWithArray:[dataManageStock copy]];
            if (self.blEditApproved) {
                temp = [NSMutableArray arrayWithArray:[dataCheckIn copy]];
            }
            
            NSArray *dicList;
            id object = [NSDictionary dictionaryWithString:valueInvoice];
            if (![object isKindOfClass:[NSDictionary class]]) {
                dicList = [NSArray arrayWithArray:(NSArray *)object];
            }
            else{
                dicList = [object allValues];
            }
            
            if ([dicList count] != 0) {
                for (NSDictionary *data in dicList) {
                    NSString *keyInfo = data[@"id"];
                    NSDictionary *dataOrder = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                                   andvalue:@"(id = %@)"
                                                                                    andData:temp];
                    if ([dataOrder count] != 0) {
                        int index = (int)[temp indexOfObject:dataOrder];
                        [temp replaceObjectAtIndex:index withObject:data];
                        [dataEditInvoice setObject:data forKey:[@(index) stringValue]];
                    }
                }
                if (self.blEditApproved) {
                    dataCheckIn = [NSMutableArray arrayWithArray:[temp copy]];
                }
                else{
                    dataManageStock = [NSMutableArray arrayWithArray:[temp copy]];
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error checkDataInvoice : %@",exception);
    }
}
-(void)checkDataManageStock{
    @try {
        NSString *valueInvoice = [self.invoiceDB valueInvoice];
        if (![valueInvoice isEqualToString:@"nil"] && valueInvoice != nil) {
            NSDictionary *dic = [NSDictionary dictionaryWithString:valueInvoice];
            if ([dic count] != 0) {
                dataEditManageStock = [NSMutableDictionary dictionaryWithDictionary:dic];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error checkDataInvoice : %@",exception);
    }
}

#pragma mark UITableViewDataSource
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
  if (self.blNewInvoice) {
      UIView *headerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.bounds.size.width,30)];
      [headerView setBackgroundColor:[UIColor colorWithRed:0.059 green:0.063 blue:0.200 alpha:1.000]];
      UILabel *nameTitle = [self createLabel:nVendor andSize:CGRectMake(0,4,tableView.bounds.size.width,24)];
      [headerView addSubview:nameTitle];
      
      return headerView;
  }
//  else if ([self.role isEqualToString:@"admin"] && [self.dataEditItem[@"status"] intValue] == 1){
//      static NSString *HeaderCellIdentifier = @"cellTitleAppriove";
//      
//      CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:HeaderCellIdentifier];
//      if (cell == nil) {
//          cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HeaderCellIdentifier];
//      }
//      return cell;
//  }
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.blNewInvoice) {
        return 30;
    }

    return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.blEditApproved && !self.blViewStatusInvoice){
        @try {
            estimatedAmountTotal = 0;
            trueAmountTotal = 0;
            for (NSDictionary *data in dataCheckIn) {
                float estimatedTotalCost   = [NSString checkNullConvertFloat:data[@"estimated_total_cost"]];
                estimatedAmountTotal += estimatedTotalCost;
                
//                if ([data[@"tag"] isEqualToString:@"1"]) {    // กดแล้ว
                    float trueCost = [NSString checkNullConvertFloat:data[@"true_cost"]];
                    trueAmountTotal += trueCost;
//                }
            }
            int num = (int)[dataCheckIn count]+1;
            if (self.role || self.blEditApproved) {
                num +=1;
            }
            return num;
        }
        @catch (NSException *exception) {
            DDLogError(@" error numberOfRowsInSection %@",exception);
        }
    }
    else if (self.blEditNotApproved){
        return [dataManageStock count] + 1;
    }
    else if ([dataManageStock count] != 0 && !self.blViewStatusInvoice) {
        int num = (int)[dataManageStock count];
        if (self.blNewInvoice) {
            num +=1;
        }
        return num;
    }
    else if (self.blViewStatusInvoice){
        return [dataCheckIn count]+2;
    }
    else if (isSearching) {
        return [filteredDataManageStock count];
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    @try {
        NSString *cellIdentifier;
        CellTableViewAll *cell;
        
        if ((self.role && !self.blViewStatusInvoice) && !self.adminStockReport){
            cellIdentifier = [self getcellIdentifierAppriove:indexPath];
        }
        else if (self.blViewStatusInvoice || self.blEditApproved){
            cellIdentifier = [self getcellIdentifier:indexPath];
        }
        else{
            if (isStockReport) {
                cellIdentifier = kCellStockReport;
            }
            else if (self.blNewInvoice || self.blEditNotApproved){
                if (indexPath.row == 0) {
                    cellIdentifier = kCellTitleCreateInvioce;
                }
                else{
                    cellIdentifier = kCellDetailInvioce;
                }
            }
            else{
                cellIdentifier = kCellITemInvoice;
            }
            
            cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                if (isStockReport) {
                    cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
                }
                else{
                    cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
                }
            }
            if (![vc blIsStockManege])
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        NSString *tmpCost;
        NSString *tmpAmount;
        NSString *tmpName;
        
        if (self.blNewInvoice) { // checker สร้างใบสั่งซื้อใหม่
            if (indexPath.row != 0) {
                @try {
                    NSDictionary *data = dataManageStock[indexPath.row-1];
                    tmpAmount = [NSString stringWithFormat:@"  %@ ",data[@"stock"]];
                    tmpName   = [NSString stringWithFormat:@"  %@ ",data[@"name"]];
                    
                    if (data[@"amount"] != nil) {
                        tmpCost = data[@"amount"];
                        [cell.mCountEdit setText:tmpCost];
                        cell.stepper.value = [tmpCost intValue];
                    }
                    else{
                        [cell.mCountEdit setText:@""];
                        cell.mCountEdit.placeholder = @"0";
                        cell.stepper.value = 0;
                    }
                    
                    [cell.mCountEdit  setTag:indexPath.row];
                    [cell.stepper     setTag:indexPath.row];
                    [cell.mNameOrder  setText:tmpName];
                    [cell.mCountOrder setText:tmpAmount];
                    [cell.mTypeOrderBuy setText:data[@"unit_label"]];
                    [cell.stepper addTarget:self action:@selector(stepperTapped:)
                           forControlEvents:UIControlEventValueChanged];
                }
                @catch (NSException *exception) {
                    DDLogError(@" error blNewInvoice %@",exception);
                }
            }
            return  cell;
        }
        else if ((self.role && [self.dataEditItem[@"status"] intValue] != 4) && !self.adminStockReport){
            CellCheckAmountTableViewCell *cellCheck = [self setTableCellViewStatusInvoice:cellIdentifier andUItable:tableView andIndex:indexPath];
            cellCheck.selectionStyle = UITableViewCellSelectionStyleNone;
            return cellCheck;
        }
        else if (self.blViewStatusInvoice){
            CellCheckAmountTableViewCell *cellCheck = [self setCellEditApproved:cellIdentifier andUItable:tableView andIndex:indexPath];
            cellCheck.selectionStyle = UITableViewCellSelectionStyleNone;
            return cellCheck;
        }
        else if (self.blEditNotApproved && !self.role){      // checker รายการสินค้ายังไม่อนุมัติ
            CellTableViewAll *cellCheck = [self setNotApproved:cellIdentifier andUItable:tableView andIndex:indexPath];
            cellCheck.selectionStyle = UITableViewCellSelectionStyleNone;
            return cellCheck;
        }
        else if (isStockReport || [vc blIsStockManege]) {
            if (isStockReport) {
                NSDictionary *data = dataManageStock[indexPath.row];
                [cell.textLabel setText:[NSString checkNull:data[@"name"]]];
                [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@",data[@"stock"]]];
                [self setContentView:cell andType:data[@"unit_label"]];
                return  cell;
            }
            else if([vc blIsStockManege]){
                NSString *count    = @"";
                NSDictionary *data = dataManageStock[indexPath.row];
                if ([dataEditManageStock count] != 0) {
                    NSString *key      = [NSString stringWithFormat:@"id_%@", data[@"id"]];
                    count              = (dataEditManageStock[key] == nil)?@"":dataEditManageStock[key][@"count"];
                }
                tmpCost            = [NSString stringWithFormat:@" %@ ",[count isEqual:@""]?@"--":count];
                tmpAmount          = [NSString stringWithFormat:@"%@  %@ ",AMLocalizedString(@"จำนวน", nil),data[@"stock"]];
                tmpName            = data[@"name"];
            }
        }
        
        if (self.blEditApproved && !self.role) {      // checker อนุมัติใบสั่งซื้อ กำลังดำเนินการ
            CellCheckAmountTableViewCell *cellCheck = [self setCellEditApproved:cellIdentifier andUItable:tableView andIndex:indexPath];
            cellCheck.selectionStyle = UITableViewCellSelectionStyleNone;
            return cellCheck;
        }
        else if(self.blEditNotApproved || self.blNewInvoice){
            [cell.mBtnEdit setImage:[UIImage imageNamed:@"a_img_invoice"] forState:UIControlStateNormal];
            [cell.mBtnEdit addTarget:self action:@selector(btnCilkeCell:) forControlEvents:UIControlEventTouchUpInside];
            [cell.mBtnEdit setTag:2];    // รออนุมัติ
        }
        else if(self.blViewStatusInvoice){
            if([self.dataEditItem[@"status"] intValue] == 4){
                [cell.mBtnEdit setImage:[UIImage imageNamed:@"send"] forState:UIControlStateNormal];
                NSDictionary *data  = dataCheckIn[indexPath.row];
                NSString *cost1     = [NSString stringWithFormat:@"%@ ",[Check checkNull:data[@"estimated_total_cost"]]?@"0":data[@"estimated_total_cost"]];
//                float floatValue    = [cost1 floatValue];
                tmpCost             = [Check checkNull:cost1]?@"0.0":cost1;
                NSString *true_cost = ([data[@"true_cost"] isKindOfClass:[NSNull class]])?@"":data[@"true_cost"];
                NSString *cost      = [NSString conventCurrency:[@([true_cost floatValue] - [cost1 floatValue]) stringValue]];
                if ([cost intValue] > 0) {
                    [cell.mUporDown setImage:[UIImage imageNamed:@"c_ic_up"]];
                    [cell.mDifference setText:cost];
                    [cell.mDifference setTextColor:[UIColor ht_carrotColor]];
                }
                else if ([cost intValue] < 0){
                    [cell.mUporDown setImage:[UIImage imageNamed:@"c_ic_down"]];
                    [cell.mDifference setText:cost];
                    [cell.mDifference setTextColor:[UIColor ht_emeraldColor]];
                }
            }
            else{
                [cell.mBtnEdit setImage:[UIImage imageNamed:@"a_img_invoice"] forState:UIControlStateNormal];
            }
            [cell.mBtnEdit setEnabled:NO];
        }
        [cell.mLabel setText:tmpName];
        [cell.mTotalFood setText:tmpAmount];
        [cell.mCount setText:tmpCost];
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"error CellITemInvoice : %@",exception);
    }
}
-(void) setContentView:(UITableViewCell *)cell andType:(NSString *)type{
    
    for (UIView *obj in cell.contentView.subviews) {
        if ([obj isKindOfClass:[UILabel class]] && obj.tag == 111) {
            [obj removeFromSuperview];
        }
    }
    
    UILabel *typeOrder = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 15)];
    typeOrder.translatesAutoresizingMaskIntoConstraints = NO;
    [typeOrder setText:type];
    [typeOrder setTag:111];
    [typeOrder setFont:[UIFont systemFontOfSize:11.0]];
    [typeOrder setTextColor:[UIColor ht_silverColor]];
    [typeOrder setTextAlignment:NSTextAlignmentRight];
    [cell.contentView addSubview:typeOrder];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(typeOrder);
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeOrder(150)]"
                                                                             options:0 metrics:nil views:views]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[typeOrder(15)]"
                                                                             options:0 metrics:nil views:views]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[typeOrder]-3-|"
                                                                             options:0 metrics:nil views:views]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[typeOrder]-0-|"
                                                                             options:0 metrics:nil views:views]];
}
-(NSString *)getcellIdentifier:(NSIndexPath *)indexPath{
    NSString *cellIdentifier;
    int num = 0;
    if (self.blEditApproved || self.blViewStatusInvoice) {
        num = (int)[dataCheckIn count]+1;
    }
    else{
         num = (int)[dataManageStock count]+1;
    }
    
    if ([indexPath row] == num) {
        cellIdentifier = kCellTotalAmount;
    }
    else if (indexPath.row == 0){
        cellIdentifier = kCellTitleCellAmount;
    }
    else{
        cellIdentifier = kCellDetailAmount;
    }
    return cellIdentifier;
}
-(NSString *)getcellIdentifierAppriove:(NSIndexPath *)indexPath{
    NSString *cellIdentifier;
    int num = (int)[dataCheckIn count]+1;
    
    if ([indexPath row] == num) {
        cellIdentifier = kCellTotalAmount;
    }
    else if (indexPath.row == 0){
        cellIdentifier = kCellTitleAppriove;
    }
    else{
        cellIdentifier = kCellDetailAppriove;
    }
    return cellIdentifier;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
-(CellCheckAmountTableViewCell *) setCellEditApproved:(NSString *)cellIdentifier
                                           andUItable:(UITableView *)tableView andIndex:(NSIndexPath *)indexPath{    // blEditNotApprove
    
    @try {
        CellCheckAmountTableViewCell *cellCheck = (CellCheckAmountTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cellCheck) {
            cellCheck = [[CellCheckAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                            reuseIdentifier:cellIdentifier];
        }
        if ([indexPath row] == [dataCheckIn count]+1) {
            float numT = 0;
            float numC = 0;
            for (NSDictionary *dic in dataCheckIn) {
                float  estimatedTotal = [NSString checkNullConvertFloat:dic[@"amount"]] * [NSString checkNullConvertFloat:dic[@"cost_per_unit"]];
                numT = numT + estimatedTotal;
                float true_cost = [NSString checkNullConvertFloat:dic[@"true_cost"]];
                numC = numC + true_cost;
            }
            NSString *costEstimate = [NSString stringWithFormat:@"%@ %@ บาท",AMLocalizedString(@"ราคาประเมิน",nil),[NSString conventCurrency:[@(numT)  stringValue]]];
            NSString *actualPrices = [NSString stringWithFormat:@"%@ %@ บาท",AMLocalizedString(@"จ่ายจริง", nil),[NSString conventCurrency:[@(numC)  stringValue]]];
            
            [cellCheck.mCostEstimate setText:costEstimate];
            [cellCheck.mActualPrices setText:actualPrices];
            return  cellCheck;
        }
        else if (indexPath.row == 0){
            return cellCheck;
        }
        
        NSDictionary *data = dataCheckIn[indexPath.row-1];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        if(dataMenu != nil)
        {
            NSString *true_amount;
            if ([data[@"tag"] isEqualToString:@"1"] || [self.role isEqualToString:@"admin"]) {    // กดแล้ว
                NSString *true_cost = [NSString checkNull:data[@"true_cost"]];
                true_amount         = [NSString checkNull:data[@"true_amount"]];
                true_cost           = [NSString conventCurrency:true_cost];
                
                if ([true_cost intValue] >= 0) {
                    [cellCheck.mBuyNumber    setText:true_amount];
                    [cellCheck.mBuyPrice     setText:true_cost];
                }
            }
            else{
                [cellCheck.mBuyNumber  setText:@""];
                [cellCheck.mBuyPrice   setText:@""];
            }
            NSString *nameGoods         = [NSString stringWithFormat:@" %@ ",dataMenu[@"name"]];
            float  estimatedTotal = [NSString checkNullConvertFloat:data[@"amount"]] * [NSString checkNullConvertFloat:data[@"cost_per_unit"]];
            NSString *estimated_total_cost   = [NSString conventCurrency:[@(estimatedTotal) stringValue]];
            NSString *unitLabel         = dataMenu[@"unit_label"];
            NSString *amount1           = data[@"amount"];
            
            [cellCheck.mNameGoods       setText:nameGoods];
            [cellCheck.mOrderNumber     setText:amount1];
            [cellCheck.mOrderNumberType setText:unitLabel];
            [cellCheck.mPriceOrder      setText:estimated_total_cost];
            [cellCheck.mBuyNumberType   setText:unitLabel];
            
            int numStatus = [self.dataEditItem[@"status"] intValue];
            if (self.role && (numStatus == 3)){
                if (![cellCheck.mBuyNumber.text isEqualToString:@""] || [true_amount intValue] != [amount1 intValue]) {
                    if([true_amount intValue] != [estimated_total_cost intValue]){
                        [cellCheck.mBuyPrice setTextColor:[UIColor redColor]];
                        [cellCheck.mBuyNumber setTextColor:[UIColor redColor]];
                    }
                }
            }
        }
        return cellCheck;
    }
    @catch (NSException *exception) {
        DDLogError(@" error setCellEditApproved %@",exception);
    }

}
-(CellTableViewAll *) setNotApproved:(NSString *)cellIdentifier  andUItable:(UITableView *)tableView andIndex:(NSIndexPath *)indexPath{
    
    @try {
        CellTableViewAll *cellCheck = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cellCheck) {
            cellCheck = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:cellIdentifier];
        }
        
        if (indexPath.row == 0){
            return cellCheck;
        }

        NSDictionary *data = dataManageStock[indexPath.row-1];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"id"]]];
        if(dataMenu != nil)
        {
            BOOL bl = NO;
            NSString *keyInfo = data[@"id"];
            NSDictionary *dataOrder = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                           andvalue:@"(checkin_menu_id = %@)"
                                                                            andData:dataCheckIn];
            NSString *true_amount = [NSString checkNull:dataOrder[@"amount"]];
            true_amount = (data[@"amount"] != nil) ? data[@"amount"]:true_amount;
                
            if ([dataOrder count] != 0 || [true_amount floatValue] >= 0) {      // กดแล้ว
                NSString *true_cost   = [NSString checkNull:dataOrder[@"estimated_total_cost"]];
                true_cost             = [NSString conventCurrency:true_cost];
                
                if ([true_amount floatValue] >= 0) {
                    [cellCheck.mCountEdit setText:true_amount];
                    cellCheck.stepper.value = [true_amount intValue];
                    bl = YES;
                }
            }
            if (!bl){
                [cellCheck.mCountEdit setText:@""];
                cellCheck.mCountEdit.placeholder = @"0";
                cellCheck.stepper.value = 0;
            }
        }
        
        NSString *tmpAmount = [NSString checkNull:data[@"stock"]];
        NSString *tmpName   = [NSString checkNull:data[@"name"]];
        
        [cellCheck.mCountEdit  setTag:indexPath.row];
        [cellCheck.stepper     setTag:indexPath.row];
        [cellCheck.mNameOrder  setText:tmpName];
        [cellCheck.mCountOrder setText:tmpAmount];
        [cellCheck.mTypeOrderBuy setText:data[@"unit_label"]];
        [cellCheck.stepper addTarget:self action:@selector(stepperTapped:)
               forControlEvents:UIControlEventValueChanged];
        
        return cellCheck;
    }
    @catch (NSException *exception) {
        DDLogError(@" error blNewInvoice %@",exception);
    }
}
-(CellCheckAmountTableViewCell *)setTableCellViewStatusInvoice:(NSString *)cellIdentifier  andUItable:(UITableView *)tableView andIndex:(NSIndexPath *)indexPath{
    
    NSLog(@" %ld : %lu ",(long)indexPath.row,(unsigned long)[dataCheckIn count]);
    CellCheckAmountTableViewCell *cellCheck = (CellCheckAmountTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cellCheck) {
        cellCheck = [[CellCheckAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                        reuseIdentifier:cellIdentifier];
    }
    
    if (indexPath.row == 0){
        return cellCheck;
    }
    
    if ([indexPath row] == [dataCheckIn count]+1) {
        float numT = 0;
        for (NSDictionary *dic in dataCheckIn) {
            numT = numT + ([NSString checkNullConvertFloat:dic[@"cost_per_unit"]] * [NSString checkNullConvertFloat:dic[@"amount"]]);
        }
        float numC = 0;
        for (NSDictionary *dic in dataCheckIn) {
            NSString *true_cost = ([dic[@"true_cost"] isKindOfClass:[NSNull class]])?@"0":dic[@"true_cost"];
            numC = numC + [true_cost floatValue];
        }
        NSString *costEstimate;
        NSString *actualPrices;
        int numStatus =  [self.dataEditItem[@"status"] intValue];
        if (self.role && (numStatus == 4 || numStatus == 1)){ // 1,3
            costEstimate = [NSString stringWithString:AMLocalizedString(@"ราคา", nil)];
            actualPrices = [NSString stringWithFormat:@"%@ %@",[NSString checkNullCurrency:[@(numT)  stringValue]],AMLocalizedString(@"บาท", nil)];
        }
        else{
            costEstimate = [NSString stringWithFormat:@"%@ %@ บาท",AMLocalizedString(@"ราคาประเมิน", nil),[NSString checkNullCurrency:[@(numT)  stringValue]]];
            actualPrices = [NSString stringWithFormat:@"%@ %@ บาท",AMLocalizedString(@"จ่ายจริง", nil),[NSString checkNullCurrency:[@(numC)  stringValue]]];
        }

        
        [cellCheck.mCostEstimate setText:costEstimate];
        [cellCheck.mActualPrices setText:actualPrices];
        
        return  cellCheck;
    }
    
    NSDictionary *data = dataCheckIn[indexPath.row-1];
    NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
    
    if(dataMenu != nil)
    {
        NSString *amount1  = data[@"amount"];
        float  estimatedTotal          = [NSString checkNullConvertFloat:data[@"amount"]] * [NSString checkNullConvertFloat:data[@"cost_per_unit"]];
        NSString *estimated_total_cost = [NSString checkNullCurrency:[@(estimatedTotal) stringValue]];
        
        NSString *true_cost = @"";
        if ([data[@"tag"] isEqualToString:@"1"] || self.blViewStatusInvoice) {    // กดแล้ว
            true_cost   = [NSString checkNull:data[@"true_cost"]];
            NSString *true_amount = [NSString checkNull:data[@"true_amount"]];
            if ([true_cost isEqualToString:@""]) {
                true_cost = [NSString checkNull:data[@"cost_per_unit"]];
                true_cost  = [@([true_cost intValue] * [true_amount intValue]) stringValue];
            }
            if (!self.blViewStatusInvoice && !self.role) {
                true_cost  = [@([true_cost intValue] * [true_amount intValue]) stringValue];
            }
            
            true_cost = [NSString checkNullCurrency:true_cost];
            
            if ([true_cost intValue] >= 0) {
                [cellCheck.mBuyNumber    setText:true_amount];
                [cellCheck.mBuyPrice     setText:true_cost];
            }
        }
        else{
            [cellCheck.mBuyNumber  setText:amount1];
            [cellCheck.mBuyPrice   setText:estimated_total_cost];
        }
        
        NSString *stock   = [NSString checkNull:dataMenu[@"stock"]];
        if (self.blViewStatusInvoice) {
            stock = data[@"amount"];
        }
        NSString *nameGoods            = [NSString stringWithFormat:@" %@ ",dataMenu[@"name"]];
        NSString *unitLabel            = dataMenu[@"unit_label"];
        
        [cellCheck.mNameGoods       setText:nameGoods];
        [cellCheck.mOrderNumber     setText:stock];
        [cellCheck.mOrderNumberType setText:unitLabel];
        [cellCheck.mPriceOrder      setText:estimated_total_cost];
        [cellCheck.mBuyNumberType   setText:unitLabel];
        
        int numStatus = [self.dataEditItem[@"status"] intValue];
        if (self.role && (numStatus == 3)){
            if (![cellCheck.mBuyNumber.text isEqualToString:@""] || [stock intValue] != [amount1 intValue]) {
                if([true_cost intValue] != [estimated_total_cost intValue]){
                    [cellCheck.mBuyPrice setTextColor:[UIColor redColor]];
                    [cellCheck.mBuyNumber setTextColor:[UIColor redColor]];
                }
            }
        }
    }
    return cellCheck;
}
- (IBAction)btnCilkeCell:(id)sender {
//    UITableViewCell *buttonCell1 = (UITableViewCell*)[btn superview];
//    UITableViewCell *buttonCell2 = (UITableViewCell*)[[btn superview] superview];
//    UITableViewCell *buttonCell3 = (UITableViewCell*)[[[btn superview] superview] superview];
//    indexPath = [self.mTableInvoice indexPathForCell:buttonCell1];
//    if (indexPath == nil) {
//        indexPath = [self.mTableInvoice indexPathForCell:buttonCell2];
//        if (indexPath == nil) {
//            indexPath = [self.mTableInvoice indexPathForCell:buttonCell3];
//        }
//    } 
    
    UITableViewCell *buttonCell1 = (UITableViewCell*)[sender superview];
    NSIndexPath *indexPath;
    int i = 0;
    while (i < 10) {
        indexPath = [self.mTableInvoice indexPathForCell:buttonCell1];
        if (indexPath == nil) {
            buttonCell1 = (UITableViewCell*)[buttonCell1 superview];
            i++;
        }
        else{
            break;
        }
    }
    
    if(indexPath != nil)
        indexManage = (int)indexPath.row;
    else
        indexPath = indexPathManage;
    
    if((int)indexManage < 0)
    {
        NSLog(@"ERROR:INDEX_EDIT_ROW");
        return;
    }
    
    if (self.blEditNotApproved) {    // || self.blEditApproved
        NSDictionary *data = dataCheckIn[indexManage];
        if (data[@"id"] != nil) {
            NSMutableDictionary *dataChange = [NSMutableDictionary dictionaryMutableWithObject:data];
            [dataChange setValue:@"delete" forKey:@"type"];
            [dataEditInvoice setObject:dataChange forKey:[@(indexManage)stringValue]];
        }
        else{
            [dataEditInvoice removeObjectForKey:[@(indexManage) stringValue]];
        }
        [dataCheckIn removeObjectAtIndex:indexPath.row];
    }
    else if (self.blNewInvoice){
        [dataAddInvoice removeObjectAtIndex:indexPath.row];
    }
    else{
        [dataListItemOrder removeObjectAtIndex:indexPath.row];
    }
    [self.mTableInvoice beginUpdates];
    [self.mTableInvoice deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.mTableInvoice endUpdates];
}
-(void)loadClikeDelete{
    @try {
        NSDictionary *parameters = [NSDictionary dictionary];
        parameters = @{@"code":self.member .code,
                       @"action":@"get",
                       @"invoice_id":self.dataEditItem[@"id"],
                       @"status":self.dataEditItem[@"status"],
                       @"last_id":@""};
        
        NSString *url = [NSString stringWithFormat:GET_MANAGE_INVOICE,_member.nre];
        NSLog(@"%@ \n %@",parameters,url);
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            bool bl = [json[@"status"] boolValue];
            if (bl) {
                NSDictionary *data = json[@"data"];
                dataInvoice   = [NSDictionary dictionaryWithObject:[Check checkObjectType:data[@"invoice"]]];
                self.dataGoodsMenu = [NSDictionary dictionaryWithObject:[Check checkObjectType:data[@"goods_menu"]]];
                dataCheckIn   = [NSMutableArray arrayWithArray:[Check checkObjectType:data[@"checkin"]]];
               
                dataListItemOrder = [NSMutableArray arrayWithArray:[self.dataGoodsMenu allValues]];
                
                [self.mTableInvoice reloadData];
            }
        }];
    }
    @catch (NSException *exception) {
        DDLogError(@"error loadDataReportSeles : %@",exception);
    }
}

-(void)loadClikeEditInvoice:(NSString *)typeEdit{
    @try {
        [popoverController dismissPopoverAnimated:YES];
        NSMutableArray *checkin_data = [NSMutableArray array];
        NSMutableArray *insert_checkin_data = [NSMutableArray array];
        if (self.blEditNotApproved || self.blEditApproved || self.blNewInvoice || checkerpPopUpAddGoods) {
            NSArray *dataList = (self.blNewInvoice || checkerpPopUpAddGoods.isPendingInvoiceAdmin)?dataAddInvoice:[dataEditInvoice allValues];
            if (([dataList count] != 0 && ([dataCheckIn count] == [dataList count] || self.blEditNotApproved)) || checkerpPopUpAddGoods.isApprovalInvoiceAdmin || checkerpPopUpAddGoods.isPendingInvoiceAdmin) {
                for (NSDictionary *checkin in dataList) {
                    NSString *type = checkin[@"type"];
                    if ([type isEqual:@"update"]) {
                        NSDictionary *parUpdate;
                        if ((self.blEditNotApproved || self.role) && !checkerpPopUpAddGoods.isPendingInvoiceAdmin) {
                            parUpdate = @{@"checkin_menu_id":checkin[@"checkin_menu_id"],
                                          @"n":(self.role)?checkin[@"true_amount"]:checkin[@"amount"],
                                          @"is_enabled":@"1",
                                          @"id":checkin[@"id"]
                                          };
                        }
                        else{
                            parUpdate = @{@"checkin_menu_id":checkin[@"checkin_menu_id"],
                                          @"true_cost":checkin[@"true_cost"],
                                          @"is_enabled":@"1",
                                          @"id":checkin[@"id"],
                                          @"true_amount":checkin[@"true_amount"]
                                          };
                        }
                        
                        [checkin_data addObject:parUpdate];
                    }
                    else if ([type isEqual:@"add"]){
                        NSDictionary *parsAdd = @{@"id":checkin[@"checkin_menu_id"],
                                                  @"amount":checkin[@"amount"],
                                                  @"cost_per_unit":checkin[@"cost_per_unit"],
                                                  @"comment":checkin[@"comment"]
                                                  };
                        [insert_checkin_data addObject:parsAdd];
                    }
                    else if ([type isEqual:@"delete"]){
                        NSDictionary  *parsDele = @{@"checkin_menu_id":checkin[@"checkin_menu_id"],
                                                    @"true_cost":@"0",
                                                    @"is_enabled":@"0",
                                                    @"id":checkin[@"id"]
                                                    };
                        [checkin_data addObject:parsDele];
                    }
                }
                NSError *error = nil;
                NSString *cartID = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:checkin_data
                                                                                                  options:NSJSONWritingPrettyPrinted
                                                                                                    error:&error]
                                                         encoding:NSUTF8StringEncoding];
                
                NSString *cartIDInsert = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:insert_checkin_data
                                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                                          error:&error]
                                                               encoding:NSUTF8StringEncoding];
                NSLog(@"self.dataEditItem %@",self.dataEditItem);
                NSDictionary *parameters = @{@"code":self.member.code,
                                             @"invoice_id":[Check checkObjectType:self.dataEditItem] == nil?@"":self.dataEditItem[@"id"],
                                             @"invoice_comment":@"",
                                             @"checkin_data":cartID,              // ลบ หรือ แก้ไข
                                             @"insert_checkin_data":cartIDInsert , // เพิ่ม
                                             @"status_invoice":([self.dataEditItem[@"status"] integerValue] == 2)?@"3":@""
                                             };
                
                NSString *url = [NSString stringWithFormat:GET_UPDATE_CHECKIN_GOODS,_member.nre];
                DDLogInfo(@"%@ \n %@",parameters,url);
                [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
                    bool bl = [json[@"status"] boolValue];
                    if (bl) {
                        isCheckInvoiceDB = YES;
                         [self loadDataReportSeles];
                        if (self.blEditNotApproved || self.blEditApproved) {
                            [self.navigationController popViewControllerAnimated:YES];
                        }
//                        else{
//                            [self loadDataReportSeles];
//                        }
                    }
                }];
            }
            else{
                [loadProcess.loadView showWithStatusAddError:@"กรุณาตรวจสอบข้อมูล"];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error loadDataReportSeles : %@",exception);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSLog(@" %ld ",(long)indexPath.row);
    indexPathManage = indexPath;
    indexManage = (int)indexPath.row;

    if (0 == indexManage && ![vc blIsStockManege] ) return;
    if (([dataCheckIn count]+1 == indexManage) && ![vc blIsStockManege]) return;
    
    if (self.blEditApproved  || [vc blIsStockManege] || self.blViewStatusInvoice) {
        NSLog(@"index : %d",indexManage);
        if ([vc blIsStockManege]) {
            UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTag:4];
            [self createPopoverController:btn];
        }
        else if (!self.isViewStatusInvoiceNo && (self.blEditApproved || self.blViewStatusInvoice)) {
            int numStatus = [self.dataEditItem[@"status"] intValue];
            if (self.blEditApproved || (self.role && numStatus == 4) || self.blViewStatusInvoice) {
                indexManage --;
            }
            UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
            [self  createPopoverController:btn];
        }
    }
}
#pragma mark alertview click
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1){
        if(buttonIndex == 1)
        {
            UIAlertView *alertViewOption = [[UIAlertView alloc]
                                            initWithTitle:AMLocalizedString(@"ยืนยันการลบ", nil)
                                            message:@""
                                            delegate:self
                                            cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                            otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil
                                            ];
            alertViewOption.tag = 2;
            
            [alertViewOption show];
        }
    }
    else{
        if(buttonIndex == 1)
        {
            UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];
            [self btnCilkeCell:[btn viewWithTag:2]];
        }
    }
    
}

#pragma mark - create popover Controller
- (IBAction)createPopoverController:(id)sender {
    UIButton *btn = (UIButton *)sender;
//    
    
    NSString *idStory;
    NSString *title;
    if (btn.tag == 1) {
        idStory = kPopUpPendingInvoiceAdmin;
        title = AMLocalizedString(@"เพิ่มสินค้าลงในใบสั่งของ", nil);
    }
    else if (btn.tag == 5) {
        if([self.role isEqualToString:@"admin"]){ //approve
            [loadProcess.loadView showProgress];
            
            int status = 0;
            if([self.pendingData[@"status"] intValue] == 0)
                status = 1;
            else if([self.pendingData[@"status"] intValue] == 1)
                status = 2;
            else if([self.pendingData[@"status"] intValue] == 2)
                status = 3;
            else if([self.pendingData[@"status"] intValue] == 3)
                status = 3;
            
//            NSLog(@"%@", self.dataEditItem);
//            NSLog(@"%@", self.pendingData);
            NSDictionary *parameters = @{
                                         @"code":self.member.code,
                                         @"action":@"approve",
                                         @"pending_id":self.pendingData[@"id"],
                                         @"status":[NSString stringWithFormat:@"%d", status]
                                         };
            NSString *url = [NSString stringWithFormat:GET_MANAGE_INVOICE,_member.nre];
            [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
                if([json[@"status"] boolValue])
                {
                    if ([self.role isEqualToString:@"admin"]) {
                        dataVender = [NSMutableDictionary dictionary];
                        dataVender  = [self checkVender];
                        __block int numKey = 0;
                        [self blockVender:numKey];
                    }
                    else{
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                }
                [LoadProcess dismissLoad];
            }];
            return;
        }
    }
    else if(!self.blViewStatusInvoice && self.blEditApproved && !self.role){ // checker
        idStory                = kPopUpPendingInvoiceAdmin; // admin = รอตรวจรายการสินค้าจากใบสั่งซื้อ
        NSDictionary *data     = dataCheckIn[indexManage];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        title                  = dataMenu[@"name"];
    }
    else if(self.blViewStatusInvoice && !self.blEditApproved && self.role) {  // admin
        idStory                = kPopUpPendingInvoiceAdmin; // admin = รอตรวจรายการสินค้าจากใบสั่งซื้อ
        NSDictionary *data     = dataCheckIn[indexManage];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        title                  = dataMenu[@"name"];
    }
    else if(!self.blViewStatusInvoice && self.blEditApproved && self.role) {  // admin
        idStory  = kPopUpApprovalInvoiceAdmin; // admin = รอการอนุมัติใบสั่งซื้อ
        NSDictionary *data = dataManageStock[indexManage];
        title              = data[@"name"];
    }
    else if (btn.tag == 3 || [self.dataEditItem[@"status"] intValue] == 3) {
        idStory  = kPopUpApprovalInvoiceAdmin; // admin = รอการอนุมัติใบสั่งซื้อ
        NSDictionary *data = dataManageStock[indexManage];
        title              = data[@"name"];
    }
    else if (btn.tag == 4) {
        idStory            = kPopUpStoreCheck;
        NSDictionary *data = dataManageStock[indexManage];
        title              = data[@"name"];
    }
    else{
        idStory = kPopUpAddGoods;
        title   = AMLocalizedString(@"สินค้า", nil);
    }
    
    checkerpPopUpAddGoods = [UIStoryboardMain instantiateViewControllerWithIdentifier:idStory];
    [checkerpPopUpAddGoods setTitle:title];
    [checkerpPopUpAddGoods setSelfSuper:self];
   
    if ((self.blEditNotApproved && btn.tag != 99) || self.blEditApproved) {
        NSDictionary *data = dataCheckIn[indexManage];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        if(dataMenu == nil){
            return;
        }
        if (data[@"id"] == nil) {
            DDLogInfo(@"%@",dataEditInvoice);
            DDLogInfo(@"index %d",indexManage);
            data = dataEditInvoice[[@(indexManage+1) stringValue]];
        }
        NSString *nameGoods  = [NSString stringWithFormat:@" %@ ",dataMenu[@"name"]];
        [checkerpPopUpAddGoods setTitle:nameGoods];
        [checkerpPopUpAddGoods setDataCheckIn:data];
        
        if(self.blViewStatusInvoice && !self.blEditApproved && self.role) {
           [checkerpPopUpAddGoods setIsPendingInvoiceAdmin:YES];        // admin = รอตรวจรายการสินค้าจากใบสั่งซื้อ
        }
        else if(!self.blViewStatusInvoice && self.blEditApproved && self.role)
            [checkerpPopUpAddGoods setIsApprovalInvoiceAdmin:YES];      // admin = รอการอนุมัติใบสั่งซื้อ
    }
    else if (btn.tag == 4){
         NSDictionary *data = dataManageStock[indexManage];
        [checkerpPopUpAddGoods setDataManageStock:data];
    }
    else if (btn.tag == 1){
        NSDictionary *data = dataManageStock[indexManage-1];
        [checkerpPopUpAddGoods setDataEditInvoice:data];
    }
    else if(self.blViewStatusInvoice){
        NSDictionary *data = dataCheckIn[indexManage];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        [checkerpPopUpAddGoods setDataCheckIn:data];
        [checkerpPopUpAddGoods setDataCheckInGoodMenu:dataMenu];
        [checkerpPopUpAddGoods setIsPendingInvoiceAdmin:YES];
    }
    
    CGSize size;
    CGFloat sizeHieght = 320;
    
    if(!self.blViewStatusInvoice && self.blEditApproved && !self.role) sizeHieght = 400;
    if(self.blViewStatusInvoice && !self.blEditApproved && self.role)  sizeHieght = 400;
        
   
    if (![Check  checkDevice]) {
        size = CGSizeMake(320,sizeHieght);
    }
    else{
        size = CGSizeMake(self.view.frame.size.width-50,sizeHieght);
    }
    
    checkerpPopUpAddGoods.preferredContentSize = CGSizeMake(size.width,size.height);
    checkerpPopUpAddGoods.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:checkerpPopUpAddGoods];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}

-(void)loadDataaddCheckinGood{
    NSMutableArray *listarr = [NSMutableArray array];
    if (self.blNewInvoice) {
        dataManageStock = [[self sumDataManageStock] mutableCopy];
        for (NSDictionary *data in dataManageStock) {
            if (data[@"amount"] != nil) {
                NSDictionary *pars = @{@"id":data[@"id"],
                                       @"amount":data[@"amount"],
                                       @"cost_per_unit":data[@"cost_per_unit"],
                                       @"comment":data[@"comment"]
                                       };
                [listarr addObject:pars];
            }
        }
    }
    else{
        for (NSDictionary *data in dataCheckIn) {
            NSDictionary *pars = @{@"id":data[@"id"],
                                   @"amount":data[@"amount"],
                                   @"cost_per_unit":data[@"cost_per_unit"],
                                   @"comment":data[@"comment"]
                                   };
            [listarr addObject:pars];
        }
    }
    if ([listarr count] != 0) {
        NSError *error = nil;
        NSString *cartID = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:listarr
                                                                                          options:NSJSONWritingPrettyPrinted
                                                                                            error:&error]
                                                 encoding:NSUTF8StringEncoding];
        NSDictionary *parameters = @{@"code":self.member.code,
                                     @"invoice_id":@"",        // สร้างครั้งแรก ไม่มี invoice id
                                     @"invoice_comment":@"",
                                     @"checkin_data":cartID
                                     };
        
        NSString *url = [NSString stringWithFormat:GET_ADD_CHECKIN_GOODS,self.member.nre];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            bool bl = [json[@"status"] boolValue];
            if(bl){
                isCheckInvoiceDB = YES;
//                [loadProcess.loadView showProgress];
                [self.navigationController popViewControllerAnimated:YES];
            }else{
                [loadProcess.loadView showError];
            }
        }];
    }
    else{
        [loadProcess.loadView showWithStatusAddError:@"ไม่มีรายการ"];
    }
}

- (void) captureScreenBlockSync:(NSString *)key andVevnder:(NSMutableDictionary *)dataVenderTemp success:(void (^)(BOOL completion)) block
{
    [UIView animateWithDuration:0 animations:^{
        NSDictionary *dVendor = (NSDictionary *)[NSDictionary searchDictionary:key
                                                                      andvalue:@"(id = %@)"
                                                                       andData:filterData];
        [self.mTitleOrder setText:self.title];
        [self.layoutHightTitleOrder setConstant:50.0];
        [self.mTime setText:dVendor[@"name"]];
        [self.mNameVendor setText:[NSString stringWithFormat:@"%@: %@ :%@",AMLocalizedString(@"ผู้สั่ง", nil),self.member.titleRes,self.mNameVendor.text]];
        NSMutableArray *arr = [dataVenderTemp[key] mutableCopy];
        dataCheckIn = [arr mutableCopy];
        [self.mTableInvoice reloadData];
    } completion:^(BOOL finished) {
        [self setHeightTable];
         self.navigationItem.leftBarButtonItem = nil;
        block(YES);
    }];
}
-(void)captureScreen:(void (^)(BOOL completion)) block{

    [self.buttomBtn setHidden:YES];
    [self.layoutHightButtonAdd setConstant:0.0];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect frame = CGRectMake(0, 0,screenRect.size.width,heightmTableInvoice);
    CGRect frameWindow = self.view.bounds;
    self.view.bounds = frame;
    
    UIGraphicsBeginImageContextWithOptions(frame.size,NO,[UIScreen mainScreen].scale);
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil,nil, nil);
    self.view.bounds = frameWindow;
    block(YES);
}

//-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
//{
//    NSString *message;
//    NSString *title;
//    if (!error) {
//        title = AMLocalizedString(@"บันทึกรูปใบสั่งซื้อ", @"");
//        message = AMLocalizedString(@"เสร็จเรียบร้อย", @"");
//    } else {
//        title = AMLocalizedString(@"บันทึกรูปใบสั่งซื้อ", @"");
//        message = [error description];
//    }
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:AMLocalizedString(@"ButtonOK", @"")
//                                          otherButtonTitles:nil];
//    [alert show];
//}

//-(void)captureScreen:(void (^)(BOOL completion)) block{
//
//    [self.buttomBtn setHidden:YES];
//    [self.layoutHightButtonAdd setConstant:0.0];
//    
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGRect frame = CGRectMake(0, 0,screenRect.size.width,heightmTableInvoice);
//    CGRect frameWindow = self.view.window.bounds;
//    self.view.window.bounds = frame;
//    
//    UIGraphicsBeginImageContextWithOptions(frame.size,NO,[UIScreen mainScreen].scale);
//    [self.view.window drawViewHierarchyInRect:self.view.window.bounds afterScreenUpdates:YES];
//    [self.view.window.layer renderInContext:UIGraphicsGetCurrentContext()];
//    
//    [UIView animateWithDuration:0 animations:^{
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
//    } completion:^(BOOL finished) {
//        self.view.window.bounds = frameWindow;
//        block(YES);
//    }];
//   
//
//}
-(void)blockVender:(int)numKey{
    NSString *key = [dataVender allKeys][numKey];
    [self captureScreenBlockSync:key andVevnder:dataVender success:^(BOOL completion) {
        DDLogInfo(@"compert finished %@",[dataVender allKeys][numKey]);
        [self captureScreen:^(BOOL completion) {
            __block int num = numKey;
            num += 1;
            if (num < [[dataVender allKeys] count]) {
                [self blockVender:num];
            }
            else{
                [UIView animateWithDuration:0.5 animations:^{
                     [self dismissViewControllerAnimated:YES completion:nil];
                } completion:^(BOOL finished) {
                     [LoadProcess dismissLoad];
                }];
            }
        }];
    }];
}

-(NSMutableDictionary *)checkVender{
    
    NSMutableDictionary *dataVenderTemp = [NSMutableDictionary dictionary];
    int i = 0;
    while (i != [dataCheckIn count]) {
        NSDictionary *data = dataCheckIn[i];
        NSDictionary *dataMenu = self.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
        NSString *venderID = dataMenu[@"vendor_id"];
        BOOL bl = (dataVenderTemp[venderID] != nil)?YES:NO;
        
        if (bl) {
            NSMutableArray *arr = [dataVenderTemp[venderID] mutableCopy];
            [arr addObject:data];
            [dataVenderTemp setValue:arr forKey:venderID];
        }
        else{
            NSMutableArray *arr = [NSMutableArray arrayWithObject:data];
            [dataVenderTemp setValue:arr forKey:venderID];
        }
        i++;
    }
    return dataVenderTemp;
}
-(void)btnAddGoods:(id )sender and:(NSDictionary *)data andTpye:(NSString *)type{
    if (![checkerpPopUpAddGoods.mCountGoods.text isEqual:@""] && ![checkerpPopUpAddGoods.mPrice.text isEqual:@""]) {
        if (self.blEditNotApproved || self.blEditApproved) {    // แก้ไขในส่วน คลิกเพื่อแก้ไข และ ไม่ใช่กดปุ่มเพื่อเพิ่ม  *** รายการยังไม่อนุมัติ
            if ([type isEqualToString:@"add"]) {
                [dataCheckIn addObject:data];
                [dataEditInvoice setObject:data forKey:[@([dataCheckIn count]) stringValue]];
            }
            else{
                [dataCheckIn replaceObjectAtIndex:indexManage withObject:data];
                [dataEditInvoice setObject:data forKey:[@(indexManage) stringValue]];
            }
            if (self.blEditApproved) {
                [self.mTableInvoice reloadData];
            }
        }
        else if ([type isEqual:@"manage"]){
            NSString *key = [NSString stringWithFormat:@"id_%@", dataManageStock[indexManage][@"id"]];
            [dataEditManageStock setObject:data forKey:key];
            [self.mTableInvoice reloadData];
        }
        else{
            [dataAddInvoice addObject:data];
            [self.mTableInvoice reloadData];
        }
        if (!self.blEditNotApproved) {
            [popoverController dismissPopoverAnimated:YES];
            [loadProcess.loadView showSuccess];
            CGSize size = self.mTableInvoice.contentSize;
            if (size.height < heightmTableInvoice) {
                size.height = heightmTableInvoice;
                [self.mTableInvoice setContentSize:size];
            }
        }
    }
    else{
        [loadProcess.loadView showWithStatusAddError:AMLocalizedString(@"ป้อนข้อมูลไม่ครบ", nil)];
    }
}
-(void)addDataNotApproved:(NSDictionary *)data and:(NSString *)amount{
    if (self.blEditNotApproved){      // แก้ไขในส่วน คลิกเพื่อแก้ไข และ ไม่ใช่กดปุ่มเพื่อเพิ่ม   *** รายการยังไม่อนุมัติ
        NSString *type;
        NSString *checkin_menu_id;
        NSMutableDictionary *dataChangeNot;
        NSString *keyInfo = data[@"id"];
        NSDictionary *dataOrder = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                       andvalue:@"(menu_id = %@)"
                                                                        andData:dataCheckIn];
        if ([dataOrder  count] == 0) {
            dataOrder = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                             andvalue:@"(checkin_menu_id = %@)"
                                                              andData:dataCheckIn];
        }
        
        type = dataOrder[@"type"];
        if ([dataOrder count] != 0 && (type == nil || [type isEqualToString:@"update"])) {
            type = @"update";
            dataChangeNot = [NSMutableDictionary dictionaryMutableWithObject:dataOrder];
            checkin_menu_id = dataChangeNot[@"checkin_menu_id"];
        }
        else{
            type = @"add";
            dataChangeNot = [NSMutableDictionary dictionaryMutableWithObject:data];
            checkin_menu_id = dataChangeNot[@"id"];
        }

        float price = [NSString checkNullConvertFloat:amount];
        NSString *cost  = [@([dataChangeNot[@"cost_per_unit"] intValue] * price) stringValue];
      
        [dataChangeNot setValue:amount forKey:@"amount"];
        [dataChangeNot setValue:@""   forKey:@"comment"];
        [dataChangeNot setValue:checkin_menu_id  forKey:@"checkin_menu_id"];
        [dataChangeNot setValue:cost  forKey:@"estimated_total_cost"];
        [dataChangeNot setValue:type  forKey:@"type"];
        
        if ([type isEqualToString:@"add"] && [dataOrder count] == 0) {
            [dataCheckIn addObject:dataChangeNot];
             float index = [dataCheckIn indexOfObject:dataChangeNot];
            [dataEditInvoice setObject:dataChangeNot forKey:[@(index) stringValue]];
        }
        else{
            float index = [dataCheckIn indexOfObject:dataOrder];
            [dataCheckIn replaceObjectAtIndex:index withObject:dataChangeNot];
            [dataEditInvoice setObject:dataChangeNot forKey:[@(index) stringValue]];
        }
    }
}
-(void)btnCancelGoods:(id )sender{
//    if (self.blEditApproved) { // || self.blEditApproved
//        [self checkBox:checkboxButton];
//    }
    [popoverController dismissPopoverAnimated:YES];
    
}
-(IBAction)btnEditManageStock:(id)sender{
    
    if (self.blEditApproved) {
        [self loadClikeEditInvoice:@"editInvoice"];
    }
    else
        [self loadDataManageStock:@"submit"];
}
-(IBAction)btnAddInvioce:(id )sender{
    
    if([self.role isEqualToString:@"admin"]){
        [self createPopoverController:sender];
    }
    else if (self.blEditNotApproved || self.blEditApproved){
        [self loadClikeEditInvoice:@"editInvoice"];
    }
    else if (vc.blIsStockManege){
         [self loadDataManageStock:@"submit"];
    }
    else{
        [self loadDataaddCheckinGood];
    }
}


- (IBAction)leftBtnAction:(id)sender{
    if ([self.role isEqualToString:@"admin"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [[self navigationController] popViewControllerAnimated:YES];
    }
}
#pragma mark - delegate UITextField

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing %ld",(long)textField.tag);
    
    NSIndexPath *indexPath = [self getIndexPatn:textField];
    int index = (int)textField.tag-1;
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:dataManageStock[index]];
    if ([data count] != 0 && ![textField.text isEqualToString:@""]) {
        if (self.blEditNotApproved) {
            indexManage = index;
            [self addDataNotApproved:data and:textField.text];
        }
        if ([textField.text isEqualToString:@"0"]) {
            [data removeObjectForKey:@"amount"];
        }
        else{
            [data setObject:textField.text  forKey:@"amount"];
        }
        
        [data setObject:textField.text forKey:@"amount"];
        [dataManageStock replaceObjectAtIndex:index withObject:data];

        [self.mTableInvoice reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

- (NSIndexPath *)getIndexPatn:(id)sender {
    UITableView *superTable = (UITableView *)self.mTableInvoice;
    int i = 0;
    UITableViewCell *buttonCell = (UITableViewCell*)[sender superview];
    
    NSIndexPath *indexPath;
    while (i < 10) {
        indexPath = [superTable indexPathForCell:buttonCell];
        if (indexPath == nil) {
            buttonCell = (UITableViewCell*)[buttonCell superview];
            i++;
        }
        else{
            break;
        }
    }
    return indexPath;
}

- (CellTableViewAll *)getIndexCell:(UIView *)sender {
    UIView *parentCell = sender.superview;
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    return (CellTableViewAll *)parentCell;
}

-(void)stepperTapped:(UIStepper*)stepper{
    @try {
        NSString *stepperValue = [NSString stringWithFormat:@"%1.0f",stepper.value];
        if (![stepperValue isEqual:nil]) {
            int index = (int)stepper.tag-1;
            NSMutableDictionary *data = [NSMutableDictionary dictionaryWithDictionary:dataManageStock[index]];
            if (self.blEditNotApproved) {
                indexManage = index;
                [self addDataNotApproved:data and:stepperValue];
            }
            if ([stepperValue isEqualToString:@"0"]) {
                [data removeObjectForKey:@"amount"];
            }
            else{
                [data setObject:stepperValue  forKey:@"amount"];
            }
            [dataManageStock replaceObjectAtIndex:index withObject:data];
            dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
            
            [self.mTableInvoice reloadData];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error excepton %@",exception);
    }
   
}
#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender   // ยกเลิก prepareForSegue
{
    if (self.blNewInvoice) {
        if ([dataAddInvoice count] == 0) {
            return NO;
        }
    }
    if (self.blEditNotApproved) {
        if ([dataEditInvoice count] == 0) {
            [loadProcess.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาเช็คสินค้าให้ครบทุกรายการ", nil)];
            return NO;
        }
    }
    if (self.blEditApproved) {
        if ([dataEditInvoice count] != [dataCheckIn count]) {
            [loadProcess.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาเช็คสินค้าให้ครบทุกรายการ", nil)];
            return NO;
        }
    }
    if(![identifier isEqualToString:@"addGoods"])  {
        return NO;
    }
    return YES;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"addGoods"]) {  //    logout
        isCheckInvoiceDB = YES;
        [self loadClikeEditInvoice:@"editInvoice"];
    }
}
-(void) setLineCell:(UITableViewCell *)cell{
    for (UIView *vi in cell.contentView.subviews) {
        if ([vi isMemberOfClass:[UILabel class]] || [vi isMemberOfClass:[UIView class]]) {
            vi.layer.borderWidth = 0.30f;
            vi.layer.borderColor = [UIColor ht_silverColor].CGColor;
            vi.clipsToBounds = NO;
            vi.layer.masksToBounds = YES;
        }
    }
}
- (IBAction)menuButtonTapped:(id)sender {
    if (((self.blEditNotApproved || self.blEditApproved) && [self.role isEqualToString:@"admin"]) || (self.blViewStatusInvoice && !self.isViewStatusInvoiceNo)){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else  if (self.blEditApproved || self.blEditNotApproved || self.isViewStatusInvoiceNo) {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    else
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

#pragma mark - delegate UITextField

-(void)selectFilter:(id)sender{
    
    CGSize size;
    if ([Check  checkDevice]) {
        size = CGSizeMake(200,250);
    }
    else{
        size = CGSizeMake(250,250);
    }
    
    FilterInvioceViewController *tableFilter = [[FilterInvioceViewController alloc] init];
    [tableFilter setFilterData:filterData];
    [tableFilter setCheckerInvoiceOrder:self];
    [tableFilter.tableView reloadData];
    
    tableFilter.preferredContentSize = size;
    tableFilter.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:tableFilter];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromBarButtonItem:sender
                              permittedArrowDirections:WYPopoverArrowDirectionAny
                                              animated:NO];
}
-(void) sortVendor:(NSInteger )idVendor{
    dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
    
    if (idVendor == 0) {
        dataManageStock = [NSMutableArray arrayWithArray:[dataManageStockTemp mutableCopy]];
        nVendor = AMLocalizedString(@"คู่ค้าทั้งหมด", nil);
    }
    else{
        NSDictionary *dVendor = filterData[--idVendor];
        nVendor = dVendor[@"name"];
        NSString *keyInfo = dVendor[@"id"];
        dataManageStock = [NSMutableArray arrayWithArray:(NSMutableArray *)[NSDictionary searchDictionaryALL:keyInfo
                                                                     andvalue:@"(vendor_id = %@)"
                                                                      andData:dataManageStockTemp]];
    }
   
    [self.mTableInvoice reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.mTableInvoice reloadData];
}
-(NSMutableArray *)sumDataManageStock{
    NSMutableArray *listStock = [NSMutableArray arrayWithArray:[dataManageStockTemp mutableCopy]];
    for (NSDictionary *dic in dataManageStock) {
        if (dic[@"amount"] != nil) {
            NSString *keyInfo = dic[@"id"];
            NSMutableDictionary *dicTemp = (NSMutableDictionary *)[NSDictionary searchDictionary:keyInfo
                                                                                        andvalue:@"(id = %@)"
                                                                                         andData:dataManageStockTemp];
            NSUInteger num = [dataManageStockTemp indexOfObject:dicTemp];
            [listStock replaceObjectAtIndex:num withObject:dic];
        }
    }
    return listStock;
}


-(UILabel *)createLabel:(NSString *)title andSize:(CGRect)size{
    UILabel *label = [[UILabel alloc] initWithFrame:size];
    [label setText:title];
    [label setTextColor:[UIColor whiteColor]];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont boldSystemFontOfSize:14];
    return label;
}

#pragma mark UISearchBar
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//    dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",isSearching);
    
    //Remove all objects first.
    [dataManageStock removeAllObjects];
    
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
//        dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
        dataManageStock = [dataManageStockTemp mutableCopy];
        isSearching = NO;
    }
    [self.mTableInvoice reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Cancel clicked");
//    dataManageStockTemp = [[self sumDataManageStock] mutableCopy];
    dataManageStock = [dataManageStockTemp mutableCopy];
    [searchBar resignFirstResponder];
    [self.mTableInvoice reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search Clicked");
    [self searchTableList];
}
- (void)searchTableList {
    NSString *searchString = self.mSearchOrder.text;
    
    for (NSDictionary *dataOrder in dataManageStockTemp) {
        NSString *tempStr = dataOrder[@"name"];
        NSComparisonResult result = [tempStr compare:searchString
                                             options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
                                               range:NSMakeRange(0, [searchString length])];
        if (result == NSOrderedSame) {
            [dataManageStock addObject:dataOrder];
        }
    }
    
    if ([dataManageStock count] == 0) {
        dataManageStock = [dataManageStockTemp mutableCopy];
    }
}
@end
