//
//  CheckerMenuViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckerMenuViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mtableMenuChecker;



@property(weak,nonatomic)IBOutlet UILabel *labelHome;
@property(weak,nonatomic)IBOutlet UILabel *labelInvoiceList;
@property(weak,nonatomic)IBOutlet UILabel *labelInventoryReport;
@property(weak,nonatomic)IBOutlet UILabel *labelUpdateInventory;
@property(weak,nonatomic)IBOutlet UILabel *labelSignOut;
@end
