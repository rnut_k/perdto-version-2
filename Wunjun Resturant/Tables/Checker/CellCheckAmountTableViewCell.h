//
//  CellCheckAmountTableViewCell.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/3/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellCheckAmountTableViewCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *mNameGoods;
@property (weak, nonatomic) IBOutlet UILabel *mOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *mOrderNumberType;
@property (weak, nonatomic) IBOutlet UILabel *mPriceOrder;
@property (weak, nonatomic) IBOutlet UILabel *mBuyNumber;
@property (weak, nonatomic) IBOutlet UILabel *mBuyNumberType;
@property (weak, nonatomic) IBOutlet UILabel *mBuyPrice;

@property (weak, nonatomic) IBOutlet UILabel *mCostEstimate;
@property (weak, nonatomic) IBOutlet UILabel *mActualPrices;

//label
@property (weak, nonatomic) IBOutlet UILabel *labelProduct;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;
@property (weak, nonatomic) IBOutlet UILabel *labelAmountBuy;
@property (weak, nonatomic) IBOutlet UILabel *labelRealPaid;

@end
