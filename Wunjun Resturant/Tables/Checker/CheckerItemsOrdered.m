//
//  CheckerItemsOrdered.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CheckerItemsOrdered.h"
#import "CheckerInvoiceOrderController.h"
#import "TableAllZoneCollectionView.h"
#import "GlobalBill.h"
#import "MenuDB.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Check.h"
#import "WYPopoverController.h"
#import "CustomNavigationController.h"

#import "UIViewController+ECSlidingViewController.h"
#import "MEDynamicTransition.h"
#import "METransitions.h"
#import "SWTableViewCell.h"
#import "LoadProcess.h"
#import "Time.h"

@interface CheckerItemsOrdered ()<WYPopoverControllerDelegate>{
    WYPopoverController *popoverController;
    UITextField *textFieldNum;
    
    NSMutableArray *dataListItemOrder;
    
    NSDictionary *dataRequireInvoice;
    
    NSMutableArray *pendingData;
}
@property (nonatomic, strong) Member *member;
@property (nonatomic, strong) METransitions *transitions;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation CheckerItemsOrdered{
    GlobalBill *_globalBill;
    MenuDB *_menuDB;
    
    LoadProcess *loadProcess;
}

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initEcsliding];
    loadProcess = [LoadProcess sharedInstance];
    
    _globalBill = [GlobalBill sharedInstance];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(toggleCells:) forControlEvents:UIControlEventValueChanged];
    refreshControl.tintColor = [UIColor blueColor];
    self.refreshControl = refreshControl;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [loadProcess.loadView showWithStatus];
    [self loadViewItem];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setInsertUpdataInvoice:)
                                                 name:@"updateInvoiceList" object:nil];

    self.title = AMLocalizedString(@"invoice_list", nil);

}
-(void)loadViewItem{
    NSDictionary *dic = @{@"id":@"",
                          @"status":@"",
                          @"last_id":@""};
    [self loadDataReportSeles:dic andAction:@"get"];
    [self getPendingDataOrder];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
  
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateInvoiceList" object:nil];
}
-(void)initEcsliding{
    self.transitions.dynamicTransition.slidingViewController = self.slidingViewController;
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures = @[];
    [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}
- (void)doCalculationsAndUpdateUIs {
    [LoadMenuAll checkDataMenu:@"0"];
}
#pragma mark - Properties

- (METransitions *)transitions {
    if (_transitions) return _transitions;
    
    _transitions = [[METransitions alloc] init];
    
    return _transitions;
}
- (UIPanGestureRecognizer *)dynamicTransitionPanGesture {
    if (_dynamicTransitionPanGesture) return _dynamicTransitionPanGesture;
    
    _dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.transitions.dynamicTransition
                                                                           action:@selector(handlePanGesture:)];
    
    return _dynamicTransitionPanGesture;
}
- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

-(void)loadDataReportSeles:(NSDictionary *)data andAction:(NSString *)action{
    
    NSDictionary *parameters = [NSDictionary dictionary];
    
    if ([action isEqual:@"get"]) {
        parameters = @{@"code":self.member.code,
                       @"action":@"get",
                       @"invoice_id":data[@"id"],
                       @"status":data[@"status"],
                       @"last_id":data[@"last_id"]};
    }
    else{
        parameters = @{@"code":self.member.code,
                       @"action":@"approve",
                       @"object_id":@"",
                       @"pending_id":@"",
                       @"status":@""};
    }
    
    NSString *url = [NSString stringWithFormat:GET_MANAGE_INVOICE,_member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            dataListItemOrder = [NSMutableArray arrayWithArray:json[@"data"]];
            [self.mtableListItenOrder reloadData];
            [KVNProgress dismiss];
        }
    }];
}
-(void) getPendingDataOrder{
    NSDictionary *parameters = @{
                                 @"code":self.member.code,
                                 };
    NSString *url = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:GET_PENDING_INVOICE,self.member.nre]];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue]){
            pendingData = [NSMutableArray array];
            NSArray *pending = json[@"data"][@"pending"];
            if([pending count] > 0){
                [pendingData addObjectsFromArray:pending];
                
//                for (NSDictionary *row in pending) {
//                    [pendingData addObject:row];
//                }
            }
            NSLog(@"%@",pendingData);
        }
    }];
}


#pragma mark UITableViewDataSource
-(void) setInsertUpdataInvoice:(NSNotification *)data{
    @try {
        NSDictionary *dataInvoice = [[data userInfo] copy];
        if ([dataInvoice count] != 0) { // socket
            for (NSDictionary *dic in dataListItemOrder) {
                if ([dic[@"id"] isEqual:dataInvoice[@"invoice"][@"id"]]) {
                    int index = (int)[dataListItemOrder indexOfObject:dic];
                    [dataListItemOrder replaceObjectAtIndex:index withObject:dataInvoice[@"invoice"]];
                    break;
                }
            }
            dataListItemOrder = (NSMutableArray *)[self sortDataOrder:dataListItemOrder];
            dataListItemOrder = [NSMutableArray arrayWithArray:dataListItemOrder];
            [self.mtableListItenOrder reloadData];
        }
        else{  // noti
            [self loadViewItem];
        }
        
    }
    @catch (NSException *exception) {
        DDLogError(@"error excepton %@",exception);
    }
}

-(NSArray *) sortDataOrder:(NSMutableArray *)data1{
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy HH:mm:ss a"];
    
    NSArray *sortedTimes = [data1 sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2)
    {
        NSDate *date1 = [dateFormatter dateFromString:obj1[@"created_datetime"]];
        NSDate *date2 = [dateFormatter dateFromString:obj2[@"created_datetime"]];
        return [date1 compare:date2];
    }];
    return  sortedTimes;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [dataListItemOrder count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"UMCell";
    SWTableViewCell *cell = (SWTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dic = dataListItemOrder[indexPath.row];
    NSString *myString = dic[@"invoice_date"];
    NSArray *myComponents = [myString componentsSeparatedByString:@"-"];
    if ([dic[@"status"] intValue] == 0) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.rightUtilityButtons = [self rightButtons];
        cell.delegate = self;
    }
    else{
        cell.rightUtilityButtons = nil;
        if (([dic[@"status"] intValue] != 2) && ([dic[@"status"] intValue] != 4)) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        else if ([dic[@"status"] intValue] == 2 || [dic[@"status"] intValue] == 4){
           cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    
    [cell.detailOrder setText:[self getStatusInvoice:[dic[@"status"] intValue]]];
    cell.bgDate.backgroundColor = [self setColorInvoice:[dic[@"status"] intValue]];
    cell.mountYear.text = [NSString stringWithFormat:@"%@ | %@",[self getMonth:[myComponents[1] intValue]],myComponents[0]];
    cell.day.text = [NSString stringWithFormat:@"%@",myComponents[2]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
     NSDictionary *dic = dataListItemOrder[indexPath.row];
     if ([dic[@"status"] intValue] == 2) {  //  Check: ไปซื้อสินค้าตามใบสั่งที่สร้าง => ใบสั่งสินค้า status 2
         CheckerInvoiceOrderController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerInvoiceStockCheck"];
         [viewController_  setDataEditItem:dic];
         [viewController_  setBlEditApproved:YES];
         [viewController_  loadDataReportSeles];
         
         [[self navigationController] pushViewController:viewController_ animated:NO];
     }
     else if ([dic[@"status"] intValue] == 4){
         DeviceData *deviceData = [DeviceData getInstance];
         NSDictionary *listStaff = [NSDictionary dictionaryWithDictionary:deviceData.staffList];

         NSString *idStaff = [NSString stringWithFormat:@"id_%@",dic[@"creator"]];
         NSString *idCreate = [NSString stringWithFormat:@"ID:%@",dic[@"id"]];
         NSString *nameCreate = [NSString stringWithFormat:@"โดย %@",listStaff[idStaff][@"name"]];
         
         CheckerInvoiceOrderController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerInvoiceStockCheck"];
         NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:dic[@"created_datetime"]]];
         NSString *titleText = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ใบสั่งสินค้า", nil),date_];
         [viewController_ setTitle:titleText];
         [viewController_ setNameVendor:[NSString stringWithFormat:@"%@ %@",idCreate,nameCreate]];
         [viewController_ setRole:@"admin"];
         [viewController_ setDataEditItem:dic];
         [viewController_ setBlEditNotApproved:NO];
         [viewController_ setBlEditApproved:NO];
         [viewController_ setBlViewStatusInvoice:YES];
         [viewController_ setIsViewStatusInvoiceNo:YES];
         [[self navigationController] pushViewController:viewController_ animated:NO];
     }
     else if ([dic[@"status"] intValue] == 0){
//         [[[SWTableViewCell alloc]init]didTransitionToState:2];
         
//         if ([self.delegate respondsToSelector:@selector(swipeableTableViewCell:scrollingToState:)])
//         {
//             [self.delegate swipeableTableViewCell:self scrollingToState:kCellStateCenter];
//         }];
//         
//         SWTableViewCell *cell = (SWTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];;
//         [tableView swipeableTableViewCell:cell didTriggerRightUtilityButtonWithIndex:indexPath.row];
//         DeviceData *deviceData = [DeviceData getInstance];
//         NSDictionary *listStaff = [NSDictionary dictionaryWithDictionary:deviceData.staffList];
//         
//         NSString *idStaff = [NSString stringWithFormat:@"id_%@",dic[@"creator"]];
//         NSString *idCreate = [NSString stringWithFormat:@"ID:%@",dic[@"id"]];
//         NSString *nameCreate = [NSString stringWithFormat:@"โดย %@",listStaff[idStaff][@"name"]];
//         
//         CheckerInvoiceOrderController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerInvoiceStockCheck"];
//         NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:dic[@"created_datetime"]]];
//         NSString *titleText = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ใบสั่งสินค้า", nil),date_];
//         [viewController_ setTitle:titleText];
//         [viewController_ setNameVendor:[NSString stringWithFormat:@"%@ %@",idCreate,nameCreate]];
//         [viewController_ setRole:@"admin"];
//         [viewController_ setDataEditItem:dic];
//         [viewController_ setBlEditNotApproved:YES];
//         [viewController_ setBlEditApproved:NO];
//         [viewController_ setBlViewStatusInvoice:NO];
//         [viewController_ setIsViewStatusInvoiceNo:NO];
//         [[self navigationController] pushViewController:viewController_ animated:NO];
         
//         SWTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//         [cell showRightUtilityButtonsAnimated:YES];  
     }
     if (!tableView.isEditing) {
         [tableView deselectRowAtIndexPath:indexPath animated:NO];
     }
}

#pragma mark - UIRefreshControl Selector

- (void)toggleCells:(UIRefreshControl*)refreshControl
{
    [refreshControl beginRefreshing];
    self.refreshControl.tintColor = [UIColor blueColor];
    [self loadViewItem];
//    [self.mtableListItenOrder reloadData];
    [refreshControl endRefreshing];
}

#pragma mark - delegate UIScrollViewDelegate

-(NSString *)getStatusInvoice:(int)num{
    switch (num) {
        case 0:
            return AMLocalizedString(@"รายการสินค้ายังไม่ขออนุมัติ", nil);
        break;
        case 1:
            return AMLocalizedString(@"รอการอนุมัติใบสั่งซื้อ", nil);
        break;
        case 2:
            return AMLocalizedString(@"อนุมัติใบสั่งซื้อ กำลังดำเนินการ", nil);
        break;
        case 3:
            return AMLocalizedString(@"รอตรวจรายการสินค้าจากใบสั่งซื้อ", nil);
        break;
        case 4:
            return AMLocalizedString(@"เสร็จเรียบร้อย", nil);
        break;
        case 5:
            return AMLocalizedString(@"ยกเลิกใบสั่งซื้อ", nil);
        break;
        default:
            return nil;
        break;
    }
}
-(NSString *)getMonth:(int)num{

    switch (num) {
        case 1: return AMLocalizedString(@"ม.ค.", nil);
            break;
        case 2: return AMLocalizedString(@"ก.พ.", nil);
            break;
        case 3: return AMLocalizedString(@"มี.ค.", nil);
            break;
        case 4: return AMLocalizedString(@"เม.ย.", nil);
            break;
        case 5: return AMLocalizedString(@"พ.ค.", nil);
            break;
        case 6: return AMLocalizedString(@"มิ.ย.", nil);
            break;
        case 7: return AMLocalizedString(@"ก.ค.", nil);
            break;
        case 8: return AMLocalizedString(@"ส.ค.", nil);
            break;
        case 9:
            return AMLocalizedString(@"ก.ย.", nil);
            break;
        case 10:
            return AMLocalizedString(@"ต.ค.", nil);
            break;
        case 11:
            return AMLocalizedString(@"พ.ย.", nil);
            break;
        case 12:
            return AMLocalizedString(@"ธ.ค.", nil);
            break;
        default:
            return nil;
            break;
    }
}
-(UIColor *)setColorInvoice:(int)num{
    switch (num) {
        case 0:
           return  [UIColor colorWithRed:(156/255.0) green:(156/255.0) blue:(156/255.0) alpha:1.000];
        break;
        case 1:
            return [UIColor colorWithRed:0.386 green:0.364 blue:0.704 alpha:1.000];
        break;
        case 2:
            return [UIColor colorWithRed:0.936 green:0.390 blue:0.070 alpha:1.000];
        break;
        case 3:
            return [UIColor colorWithRed:0.386 green:0.364 blue:0.704 alpha:1.000];
        break;
        case 4:
            return [UIColor colorWithRed:0.445 green:0.628 blue:0.248 alpha:1.000];
        break;
        case 5:
            return [UIColor colorWithRed:0.986 green:0.000 blue:0.027 alpha:1.000];
        break;
        default:
            return nil;
        break;
    }
    
}

//#pragma mark - convert color
//- (UIColor *)colorWithHexString:(NSString *)str {
//    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
//    long x = strtol(cStr+1, NULL, 16);
//    return [self colorWithHex:(UInt32)x];
//}
//- (UIColor *)colorWithHex:(UInt32)col {
//    unsigned char r, g, b;
//    b = col & 0xFF;
//    g = (col >> 8) & 0xFF;
//    r = (col >> 16) & 0xFF;
//    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
//}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.445 green:0.628 blue:0.248 alpha:1.000]
                                                title:AMLocalizedString(@"แก้ไข", nil)];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.000 green:0.000 blue:1.000 alpha:0.840]
                                                title:AMLocalizedString(@"ขออนุมัติ", nil)];
    
    return rightUtilityButtons;
}

#pragma mark - SWTableViewDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    switch (state) {
        case 0:
            NSLog(@"utility buttons closed");
        break;
        case 1:
            NSLog(@"left utility buttons open");
        break;
        case 2:
            NSLog(@"right utility buttons open");
        break;
        default:
        break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    switch (index) {
        case 0:
            NSLog(@"left button 0 was pressed");
        break;
        case 1:
            NSLog(@"left button 1 was pressed");
        break;
        case 2:
            NSLog(@"left button 2 was pressed");
        break;
        case 3:
            NSLog(@"left btton 3 was pressed");
        default:
        break;
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *cellIndexPath = [self.mtableListItenOrder indexPathForCell:cell];
    dataRequireInvoice = dataListItemOrder[cellIndexPath.row];
    
    switch (index) {  // เลือกปุ่มทางขวา
        case 0:
        {
//            
            CheckerInvoiceOrderController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerInvoiceOrderController"];
            [viewController_ setDataEditItem:dataRequireInvoice];
            [viewController_ setBlEditNotApproved:YES];
//            [viewController_ loadDataReportSeles];
          
            [[self navigationController] pushViewController:viewController_ animated:NO];
            
            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:AMLocalizedString(@"คุณต้องการสั่งใบสั่งชื่อสินค้า", nil)
                                                             message:AMLocalizedString(@"เพื่อร้องขอการอนุมัติใช่หรือไม่", nil)
                                                            delegate:self
                                                   cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                                   otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
//            [alert addButtonWithTitle:AMLocalizedString(@"ใช่", nil)];
            [alert show];
            
            [cell hideUtilityButtonsAnimated:YES];
            
            break;
        }
        default:
        break;
    }
}
-(void)loadDataRequireInvoice{
    [loadProcess.loadView showProgress];
    NSDictionary *parameters = @{@"code":self.member.code,
                                @"invoice_id":dataRequireInvoice[@"id"]};
    
    NSString *url = [NSString stringWithFormat:GET_REQUIRE_INVOICE,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSDictionary *dic = @{@"id":@"",
                                  @"status":@"",
                                  @"last_id":@""};
            [self loadDataReportSeles:dic andAction:@"get"];
            [LoadProcess dismissLoad];
        }
    }];
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
        // set to NO to disable all left utility buttons appearing
            return YES;
        break;
        case 2:
        // set to NO to disable all right utility buttons appearing
            return YES;
        break;
        default:
        break;
    }
    
    return YES;
}

#pragma mark - delegate UIAlertView
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:AMLocalizedString(@"ใช่", nil)])
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:AMLocalizedString(@"ส่งใบขออนุมัติสั่งซื้อสินค้าแล้ว", nil)
                                                         message:AMLocalizedString(@"...กำลังรอการอนุมัติ", nil)
                                                        delegate:self
                                               cancelButtonTitle:AMLocalizedString(@"ยืนยัน", nil)
                                               otherButtonTitles: nil];
        [alert show];
    }
    else if([title isEqualToString:AMLocalizedString(@"ไม่", nil)])
    {
        NSLog(@"Button 2 was selected.");
    }
    else if([title isEqualToString:AMLocalizedString(@"ยืนยัน", nil)])
    {
        [self loadDataRequireInvoice];
        NSLog(@"Button 3 was selected.");
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
     if ([[segue identifier] isEqualToString:@"CreateInvoice"]){
        CheckerInvoiceOrderController *viewController_ = (CheckerInvoiceOrderController *)segue.destinationViewController;
        [viewController_  setBlNewInvoice:YES];
         
    }
}
@end
