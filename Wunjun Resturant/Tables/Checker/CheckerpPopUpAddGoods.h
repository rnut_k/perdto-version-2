//
//  CheckerpPopUpAddGoods.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"

@interface CheckerpPopUpAddGoods : UIViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UISearchBar *mSearchGoods;
@property (weak, nonatomic) IBOutlet UITextField *mCountGoods;
@property (weak, nonatomic) IBOutlet UILabel *mPrice;
@property (weak, nonatomic) IBOutlet UILabel *mUnitLabel;
@property (weak, nonatomic) IBOutlet UITextField *mRealPrice;
@property (weak, nonatomic) IBOutlet UITextView *mComment;
@property (weak, nonatomic) IBOutlet UILabel *mText;
@property (weak, nonatomic) IBOutlet UILabel *mType;
@property (weak, nonatomic) IBOutlet UITextField *mNameGoods;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutHightButton;

@property (strong, nonatomic) NSDictionary *dataCheckIn;
@property (strong, nonatomic) NSDictionary *dataCheckInGoodMenu;
@property (strong, nonatomic) NSDictionary *dataManageStock;
@property (strong, nonatomic) NSDictionary *dataEditInvoice;

@property (nonatomic) BOOL isApprovalInvoiceAdmin;
@property (nonatomic) BOOL isPendingInvoiceAdmin;

@property (nonatomic ,strong) id selfSuper;
- (IBAction)btnAddGoods:(id)sender;
- (IBAction)btnCancelGoods:(id)sender;

//@property (nonatomic) BOOL popUpCheckinInfo; //for popup checin data information

@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UILabel *trueAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *costLbl;
@property (weak, nonatomic) IBOutlet UILabel *trueCostLbl;
@property (weak, nonatomic) IBOutlet UITextView *commentTxt;


@end
