//
//  CellCheckAmountTableViewCell.m
//  Wunjun Resturant
//
//  Created by AgeNt on 6/3/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CellCheckAmountTableViewCell.h"

@implementation CellCheckAmountTableViewCell

- (void)awakeFromNib {
    [self setUplabel];
}
-(void)setUplabel{
    _labelProduct.text = AMLocalizedString(@"product", nil);
    _labelAmount.text = AMLocalizedString(@"orderamount", nil);
    _labelPrice.text = AMLocalizedString(@"price", nil);
    _labelAmountBuy.text = AMLocalizedString(@"amountbuy", nil);
    _labelRealPaid.text = AMLocalizedString(@"realpaid", nil);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
