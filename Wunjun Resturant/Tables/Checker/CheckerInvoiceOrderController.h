//
//  CheckerInvoiceOrderController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TablePendingViewController.h"

@interface CheckerInvoiceOrderController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic  ) IBOutlet UISearchBar        *mSearchOrder;
@property (weak, nonatomic  ) IBOutlet UILabel            *mNameVendor;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *layoutHightButtonAdd;
@property (weak, nonatomic  ) IBOutlet UILabel            *mTime;
@property (weak, nonatomic  ) IBOutlet UITableView        *mTableInvoice;

@property (strong, nonatomic) NSString           *nameVendor;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *mLayoutTopTable;
@property (weak, nonatomic  ) IBOutlet UIBarButtonItem    *rightInvoiceBarItem;
@property (weak, nonatomic  ) IBOutlet UILabel            *mTitleOrder;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *layoutHightTitleOrder;


@property (nonatomic ,strong) NSDictionary *dataEditItem;
@property (nonatomic ,strong) NSDictionary *dataGoodsMenu;
@property (nonatomic ,strong) TablePendingViewController *tablePending1;

@property (nonatomic) BOOL blEditApproved;      // *** อนุมัติใบสั่งซื้อ
@property (nonatomic) BOOL blEditNotApproved;   // *** รายการยังไม่อนุมัติ
@property (nonatomic) BOOL blNewInvoice;        // *** สร้างรายการใหม่


//========== ADMIN ==========
@property (nonatomic,strong) NSString *role;
@property (nonatomic,strong) NSDictionary *pendingData; // for approve invoice
@property (nonatomic) BOOL blViewStatusInvoice;         // view invoice item only without any editting
@property (nonatomic) BOOL adminStockReport;
@property (nonatomic) BOOL isViewStatusInvoiceNo;    //แสดงวิวแต่ กดดูไม่ได้


@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarBtn;
@property (weak, nonatomic) IBOutlet UIButton *buttomBtn;

//static label
@property (weak, nonatomic) IBOutlet UILabel *labelItemList;
@property (weak, nonatomic) IBOutlet UILabel *labelAmount;


//===========================

-(void)loadDataReportSeles;
-(void)btnAddGoods:(id )sender and:(NSDictionary *)data andTpye:(NSString *)type;
-(void)btnCancelGoods:(id )sender;
-(void)loadClikeEditInvoice:(NSString *)typeEdit;

-(IBAction)createPopoverController:(id)sender;
-(IBAction)menuButtonTapped:(id)sender ;
-(IBAction)btnEditManageStock:(id )sender;
-(IBAction)leftBtnAction:(id)sender ;
-(void)sortVendor:(NSInteger)idVendor;

@end
