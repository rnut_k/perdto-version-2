//
//  CheckerpPopUpAddGoods.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CheckerpPopUpAddGoods.h"
#import "CheckerInvoiceOrderController.h"
#import "WYPopoverController.h"
#import "CustomNavigationController.h"



#import "Member.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "Check.h"
#import "NSString+GetString.h"

@interface CheckerpPopUpAddGoods()<WYPopoverControllerDelegate,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextFieldDelegate,UITextViewDelegate>{
    WYPopoverController *popoverController;
    Member *_member;
    CheckerInvoiceOrderController *checkerInvoiceOrder;
    
    NSDictionary *dataSelect;
    NSMutableArray *dataStock;
    NSMutableArray *dataStockTemp;
    UITableView *tableViewStock;
    
    NSString *mCountGoodsTemp;
    BOOL beginSrarch;
    
    float sizeHight;
}

@end
@implementation CheckerpPopUpAddGoods

-(void)viewDidLoad{
    [super viewDidLoad];
    sizeHight = 0.0;
    beginSrarch = YES;
    checkerInvoiceOrder = (CheckerInvoiceOrderController *)self.selfSuper;
    _member = [Member getInstance];
    [_member loadData];
    
    [self loadDatamanageStock];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    if ([checkerInvoiceOrder dataCheckboxButton] != nil) {
//        NSDictionary *data          = [checkerInvoiceOrder dataCheckboxButton];
//        dataSelect                  = [[NSDictionary alloc] initWithDictionary:data];
//        dataSelect                  = data;
//        NSDictionary *dataGoodsMenu = checkerInvoiceOrder.dataGoodsMenu;
//        NSDictionary *dataMenu      = dataGoodsMenu[[NSString stringWithFormat:@"id_%@",data[@"checkin_menu_id"]]];
//        NSString *tmpCost           = [NSString checkNullCurrency:data[@"estimated_total_cost"]];
//        
//        NSString *tmpAmount         = [NSString stringWithFormat:@"%@ : %@ ",data[@"amount"],dataMenu[@"unit_label"]];
//        NSString *tmpName           = dataMenu[@"name"];
//        
//        [self setTitle:tmpName];
//        [self.mPrice setText:tmpCost];
//        [self.mUnitLabel setText:tmpAmount];
//    }
    if (checkerInvoiceOrder.blEditNotApproved && [self.dataCheckIn count] != 0) {
         dataSelect = self.dataCheckIn;
         NSDictionary *dataMenu;
         NSString *tmpCost;
         NSString *amount;
        if (self.dataCheckIn[@"data"] != nil) {
            dataSelect = self.dataCheckIn[@"data"][@"item"];
            dataMenu = checkerInvoiceOrder.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"data"][@"item"][@"id"]]];
            NSString  *num = [@([self.dataCheckIn[@"data"][@"item"][@"cost_per_unit"] intValue] * [self.dataCheckIn[@"data"][@"count"] intValue]) stringValue];
            tmpCost = [NSString checkNullCurrency:num];
            amount = self.dataCheckIn[@"data"][@"count"];
        }
        else{
            NSDictionary *dataGoodsMenu = checkerInvoiceOrder.dataGoodsMenu;
            dataMenu = dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"checkin_menu_id"]]];
            tmpCost = [NSString checkNullCurrency:self.dataCheckIn[@"estimated_total_cost"]];
            amount = self.dataCheckIn[@"amount"];
        }
        NSString *tmpAmount = dataMenu[@"unit_label"];
        NSString *tmpName  = dataMenu[@"name"];
        
        [self.mSearchGoods setText:tmpName];
        [self.mPrice setText:tmpCost];
        [self.mUnitLabel setText:tmpAmount];
        [self.mCountGoods setText:amount];
    }
    else  if ([self.dataManageStock count] != 0) {
        dataSelect = [NSDictionary dictionaryWithObject:self.dataManageStock];
        NSString *tmpCost = [NSString stringWithFormat:@" %@",dataSelect[@"stock"]];
        
        [self.mPrice setText:tmpCost];
    }
    else if (self.isApprovalInvoiceAdmin){
        dataSelect = self.dataCheckIn;
        NSDictionary *dataMenu;
        NSString *tmpCost;
        NSString *amount;
        NSString *true_amount;
        NSString *true_cost;
        
        if (self.dataCheckIn[@"type"] != nil) {
            dataSelect = self.dataCheckIn;
            true_amount = [NSString checkNull:self.dataCheckIn[@"true_amount"]];
            true_cost  = [NSString checkNullComparetT:@"0" andF:self.dataCheckIn[@"true_cost"]];
        }
        else{
            NSDictionary *dataGoodsMenu = checkerInvoiceOrder.dataGoodsMenu;
            dataMenu                    = dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"checkin_menu_id"]]];
            amount                      = self.dataCheckIn[@"amount"];
            NSString *num = [@([self.dataCheckIn[@"cost_per_unit"] intValue] * [self.dataCheckIn[@"amount"] intValue])stringValue];
//            NSString *est               = self.dataCheckIn[@"estimated_total_cost"];
            tmpCost                     = [NSString stringWithFormat:@" %@ %@",[NSString conventCurrency:num],AMLocalizedString(@"บาท", nil)];
            
            if ([true_amount intValue] > 0) {
                amount = true_amount;
            }
            if ([true_cost intValue] <= 0) {
                true_cost = num;
            }
        }
        
        NSString *tmpName   = dataMenu[@"name"];
        NSString *tmpAmount = dataMenu[@"unit_label"];
        [self.mPrice setText:tmpCost];
        [self.mUnitLabel setText:tmpAmount];
        [self.mCountGoods setText:amount];
        [self.mRealPrice setText:true_cost];
        [self.mNameGoods setText:tmpName];
        mCountGoodsTemp = self.mCountGoods.text;
    }
//    else  if (checkerInvoiceOrder.blEditApproved) {
//        dataSelect = self.dataCheckIn;
//        NSDictionary *dataMenu;
//        NSString *tmpCost;
//        NSString *amount;
//        NSString *true_amount;
//        NSString *true_cost;
//        if (self.dataCheckIn[@"data"] != nil) {
//            dataSelect   = self.dataCheckIn[@"data"][@"item"];
//            dataMenu     = checkerInvoiceOrder.dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"data"][@"item"][@"id"]]];
//            NSString *num = [@([self.dataCheckIn[@"data"][@"item"][@"cost_per_unit"] intValue] * [self.dataCheckIn[@"data"][@"count"] intValue])stringValue ];
//            tmpCost      = [NSString stringWithFormat:@"%@ บาท",[NSString conventCurrency:num]];
//            amount       = self.dataCheckIn[@"data"][@"count"];
//        }
//        else{
//            NSDictionary *dataGoodsMenu = checkerInvoiceOrder.dataGoodsMenu;
//            dataMenu                    = dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"checkin_menu_id"]]];
//            amount                      = self.dataCheckIn[@"amount"];
//            NSString *est               = self.dataCheckIn[@"estimated_total_cost"];
//            NSString *numCost           = [@([est intValue] * [amount intValue]) stringValue];
//            tmpCost                     = [NSString stringWithFormat:@" %@ บาท",[NSString conventCurrency:numCost]];
//            true_cost                   = [NSString checkNull:self.dataCheckIn[@"true_cost"]];
//            true_amount                 = [NSString checkNull:self.dataCheckIn[@"true_amount"]];
//            
//            
//            if ([true_amount intValue] > 0) {
//                amount = true_amount;
//            }
//            if ([true_cost intValue] <= 0) {
//                true_cost = est;
//            }
//        }
//        
//        NSString *tmpAmount = dataMenu[@"unit_label"];
//        [self.mPrice setText:tmpCost];
//        [self.mUnitLabel setText:tmpAmount];
//        [self.mCountGoods setText:amount];
//        [self.mRealPrice setText:true_cost];
//        if (checkerInvoiceOrder.role) {
//            [self.mText setHidden:YES];
//            [self.mType setHidden:YES];
//            [self.mRealPrice setHidden:YES];
//            [self.mRealPrice setUserInteractionEnabled:NO];
//        }
//        
//        mCountGoodsTemp = self.mCountGoods.text;
//    }
    else if(self.isPendingInvoiceAdmin || checkerInvoiceOrder.blEditApproved){
        dataSelect = self.dataCheckIn;
        NSString *num = [@([self.dataCheckIn[@"cost_per_unit"] intValue] * [self.dataCheckIn[@"amount"] intValue])stringValue ];
        NSString *tmpCost1 = [NSString conventCurrency:num];
        NSString *amount     = [NSString checkNull:self.dataCheckIn[@"amount"]];
        NSString *unit_label = [NSString checkNullComparetT:@"หน่วย" andF:self.dataCheckInGoodMenu[@"unit_label"]];
        NSString *trueAmount = [NSString checkNull:self.dataCheckIn[@"true_amount"]];
        NSString *trueCost;
        NSString *comment    = [NSString checkNull:self.dataCheckIn[@"comment"]];
        
        if (self.isPendingInvoiceAdmin) {
            trueCost  = [NSString checkNullComparetT:@"0" andF:self.dataCheckIn[@"true_cost"]];
        }
        else{
            
            NSDictionary *dataGoodsMenu = checkerInvoiceOrder.dataGoodsMenu;
            NSDictionary *dataMenu    = dataGoodsMenu[[NSString stringWithFormat:@"id_%@",self.dataCheckIn[@"checkin_menu_id"]]];
            unit_label = dataMenu[@"unit_label"];
            
            if (self.dataCheckIn[@"type"] != nil) {
                trueAmount = [NSString checkNull:self.dataCheckIn[@"true_amount"]];
                trueCost  = [NSString checkNullComparetT:@"0" andF:self.dataCheckIn[@"true_cost"]];
            }
            else{
                trueAmount = amount;
                trueCost = num;
            }
        }

        self.trueAmountLbl.text = [NSString stringWithFormat:@"%@ %@", amount,unit_label];
        self.trueCostLbl.text   = [NSString stringWithFormat:@"%@ %@",tmpCost1,AMLocalizedString(@"บาท", nil)];
        
        [self.commentTxt setDelegate:self];
        self.commentTxt.text    = comment;
        
        [self.mCountGoods setText:trueAmount];
        [self.mRealPrice setText:trueCost];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:YES];

}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (sizeHight == 0) {
       sizeHight = self.mScrollView.contentSize.height - 200;
    }
    [self.mScrollView setContentSize:CGSizeMake(self.mScrollView.contentSize.width,sizeHight)];
}

-(void)loadDatamanageStock{
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"action":@"get"
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_MANAGE_STOCK,_member.nre];

    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if(bl){
            dataStock = [NSMutableArray arrayWithArray:[Check checkObjectType:json[@"data"]]];
            dataStockTemp = [NSMutableArray arrayWithArray:dataStock];
        }
    }];
}

#pragma mark - popoverController
- (IBAction)createPopoverController:(id)sender {
    UIButton *btn = (UIButton *)sender;
    UITableViewController  *viewController = [[UITableViewController alloc] init];
    tableViewStock = viewController.tableView;
    [tableViewStock setDataSource:self];
    [tableViewStock setDelegate:self];
    
    CGSize size;
    if (![Check  checkDevice]) {
        size = CGSizeMake(320,320);
    }
    else{
        size = CGSizeMake(self.view.frame.size.width-50,320);
    }
    
    viewController.preferredContentSize = CGSizeMake(size.width,size.height);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:btn.bounds
                                       inView:btn
                     permittedArrowDirections:WYPopoverArrowDirectionAny
                                     animated:YES
                                      options:WYPopoverAnimationOptionFadeWithScale];
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController1{
    popoverController = nil;
}

#pragma mark - delete table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [dataStock count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenfine = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenfine];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenfine];
    }
    [cell.textLabel setText:dataStock[indexPath.row][@"name"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"%ld",indexPath.row);
    
    dataSelect = [NSDictionary dictionaryWithObject:dataStock[indexPath.row]];
    [self.mSearchGoods setText:dataSelect[@"name"]];
    float total = [dataSelect[@"cost_per_unit"] floatValue] * (([self.mCountGoods.text integerValue] <=0)?1.0:[self.mCountGoods.text integerValue]);
    [self.mPrice setText:[NSString stringWithFormat:@"%.02f %@",total,AMLocalizedString(@"บาท", nil)]];
    [self.mUnitLabel setText:dataSelect[@"unit_label"]];
    [self.mSearchGoods resignFirstResponder];
    [popoverController dismissPopoverAnimated:YES];
}

#pragma mark - delete UITextField
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    beginSrarch = NO;
    textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    beginSrarch = YES;
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    NSLog(@"%@  : %@",string,textField.text);
    if ([self.dataManageStock count] != 0) {
        return YES;
    }
    if (checkerInvoiceOrder.blEditApproved && !self.isApprovalInvoiceAdmin) {
        return YES;
    }
    if (checkerInvoiceOrder.blEditNotApproved || checkerInvoiceOrder.blEditApproved) {
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterNoStyle];
        NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        NSNumber * number = [nf numberFromString:newString];
        
        if ([dataSelect count] != 0) {
            NSString *total = [@([dataSelect[@"cost_per_unit"] floatValue] * [number integerValue]) stringValue];
            [self.mPrice setText:[NSString stringWithFormat:@"%@ บาท",[NSString conventCurrency:total]]];
        }
        else{
            [self.mPrice setText:[NSString stringWithFormat:@"0 %@",AMLocalizedString(@"บาท", nil)]];
            [self.mUnitLabel setText:@""];
        }
        
        const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
        int isBackSpace = strcmp(_char,"\b"); // เช็กช่องว่าง
        if (isBackSpace == -8) {
            // is backspace
//            NSLog(@"%d",isBackSpace);
            NSString *numdele = [newString substringToIndex:[newString length]-1];
            NSNumber * number = [nf numberFromString:numdele];
            NSInteger total = [dataSelect[@"cost_per_unit"] integerValue] * [number integerValue];
            [self.mPrice setText:[NSString conventCurrency:[@(total) stringValue]]];
        }
    }
    return YES;
}

#pragma mark - delete UITextView
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    textView.textColor = [UIColor blackColor];
    textView.text = @"";
    /*YOUR CODE HERE*/
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    /*YOUR CODE HERE*/
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
//        NSLog(@"Return pressed, do whatever you like here");
        return NO; // or true, whetever you's like
    }
    
    return YES;
}

#pragma mark - delete UISearchBar
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (searchBar.text.length > 0) {
        searchBar.text = nil;
        [searchBar resignFirstResponder];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSMutableArray *dataMenuListTmp = [NSMutableArray arrayWithArray:dataStockTemp];
    NSMutableArray *dataTmp = [NSMutableArray array];
    [dataStock removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF contains[cd] %@",
                                    searchText];
    
    NSArray *searchResults = [[dataMenuListTmp valueForKeyPath:@"name"] filteredArrayUsingPredicate:resultPredicate];
    
    for (NSString *name in searchResults) {
        for (NSDictionary *dic in dataMenuListTmp) {
            if ([name isEqual:dic[@"name"]]) {
                [dataTmp addObject:dic];
                break;
            }
        }
    }
    if ([dataTmp count] != 0){
        dataStock = [NSMutableArray arrayWithArray:dataTmp];
    }
    else{
        dataStock = [NSMutableArray arrayWithArray:dataTmp];
    }
    [tableViewStock reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (checkerInvoiceOrder.blEditNotApproved && (self.dataCheckIn[@"data"] != nil)) {
        return NO;
    }
    if (!beginSrarch) {
        return NO;
    }
    return YES;
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    if ([searchBar isEqual:self.mSearchGoods] && popoverController == nil) {
        [self createPopoverController:searchBar];
        searchBar.showsScopeBar = YES;
        [searchBar sizeToFit];
    }
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}
- (IBAction)btnAddGoods:(id)sender {
    NSDictionary *data;
    NSString *type = @"NO";
//    NSLog(@"blEditNotApproved   %d",checkerInvoiceOrder.blEditNotApproved);
//    NSLog(@"blNewInvoice        %d",checkerInvoiceOrder.blNewInvoice);
//    NSLog(@"mRealPrice          %@",self.mRealPrice.text);
//    NSLog(@"mComment            %@",self.mComment.text);
//    NSLog(@"mCountGoods         %@",self.mCountGoods.text);
//    NSLog(@"mSearchGoods        %@",self.mSearchGoods.text);
//    NSLog(@"mPrice              %@",self.mPrice.text);
//    NSLog(@"mUnitLabel          %@",self.mUnitLabel.text);
//    NSLog(@"dataSelect          %@",dataSelect);
    
    if (checkerInvoiceOrder.blEditNotApproved){   // แก้ไขในส่วน คลิกเพื่อแก้ไข และ ไม่ใช่กดปุ่มเพื่อเพิ่ม    *** รายการยังไม่อนุมัติ
        if ([self.dataCheckIn count] != 0) {
            type = @"update";
        }
        else{
            type = @"add";
        }
        NSString *comm = (self.mComment.text == nil || [self.mComment.text isEqualToString:@"รายละเอียด"])?@"":self.mComment.text;
        NSString *price = self.mCountGoods.text;
        NSMutableDictionary *dataChange = [NSMutableDictionary dictionaryMutableWithObject:dataSelect];
        NSString *cost = [@([dataChange[@"cost_per_unit"] intValue] * [price floatValue]) stringValue];
        [dataChange setValue:price forKey:@"amount"];
        [dataChange setValue:comm  forKey:@"comment"];
        [dataChange setValue:dataChange[@"checkin_menu_id"]  forKey:@"checkin_menu_id"];
        [dataChange setValue:cost  forKey:@"estimated_total_cost"];
        [dataChange setValue:type  forKey:@"type"];
        [checkerInvoiceOrder btnAddGoods:self and:dataChange andTpye:type];
    }
//    else if ([checkerInvoiceOrder dataCheckboxButton] != nil){   // แก้ไขในส่วน ติ้กเพื่อแก้ไข และ ไม่ใช่กดปุ่มเพื่อเพิ่ม  *** อนุมัติใบสั่งซื้อ
//        data = @{@"item":dataSelect,
//                 @"count":self.mRealPrice.text
//                 };
//        [checkerInvoiceOrder btnAddGoods:self and:@{@"data":data,@"type":@"update"} andTpye:type];
//    }
    else if (checkerInvoiceOrder.blEditApproved || self.isPendingInvoiceAdmin) {
        NSString *comm = (self.mComment.text == nil || [self.mComment.text isEqualToString:@"รายละเอียด"])?@"":self.mComment.text;
        NSString *price = self.mRealPrice.text;
        NSString *true_amount = self.mCountGoods.text;
        NSMutableDictionary *dataChange = [NSMutableDictionary dictionaryMutableWithObject:dataSelect];
        [dataChange setValue:price forKey:@"true_cost"];
        [dataChange setValue:comm  forKey:@"comment"];
        [dataChange setValue:@"update"  forKey:@"type"];
        [dataChange setValue:@"1"  forKey:@"tag"];
        [dataChange setValue:true_amount  forKey:@"true_amount"];
        [checkerInvoiceOrder btnAddGoods:self and:dataChange andTpye:@"update"];
        if (checkerInvoiceOrder.role) {
            [checkerInvoiceOrder loadClikeEditInvoice:@"editInvoice"];
        }
    }
    else if (checkerInvoiceOrder.blNewInvoice){    //  กดปุ่มสร้างรายการ บิล หน้าเพิ่ม invoice ใหม่ และ หน้าแก้ไข 11
        NSString *comm = (self.mComment.text == nil)?@"":self.mComment.text;
        NSString *price = self.mCountGoods.text;
        NSMutableDictionary *dataChange = [NSMutableDictionary dictionaryMutableWithObject:dataSelect];
        NSString *cost = [@([dataChange[@"cost_per_unit"] intValue] * [price intValue]) stringValue];
        [dataChange setValue:price forKey:@"amount"];
        [dataChange setValue:comm  forKey:@"comment"];
        [dataChange setValue:dataChange[@"id"]  forKey:@"checkin_menu_id"];
        [dataChange setValue:cost  forKey:@"estimated_total_cost"];
        [dataChange setValue:type  forKey:@"type"];
        [checkerInvoiceOrder btnAddGoods:self and:dataChange andTpye:type];
    }
    else{
        if (self.dataManageStock != nil) {
            data = @{@"item":self.dataManageStock,
                     @"count":self.mRealPrice.text,
                     @"comment":self.mComment.text
                     };
            type = @"manage";
        }
        else if (checkerInvoiceOrder.blEditNotApproved){
            if (checkerInvoiceOrder.blEditNotApproved && [self.dataCheckIn count] != 0){
                type = @"update";
            }
            else{
                type = @"add";
            }
        }
        [checkerInvoiceOrder btnAddGoods:self and:data andTpye:type];
    }
    
    [self.mSearchGoods resignFirstResponder];
    [self.mCountGoods resignFirstResponder];
}

- (IBAction)btnCancelGoods:(id)sender {
    [checkerInvoiceOrder btnCancelGoods:self];
}
@end
