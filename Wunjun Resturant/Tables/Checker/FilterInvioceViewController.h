//
//  FilterInvioceViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/3/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckerInvoiceOrderController.h"

@interface FilterInvioceViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) CheckerInvoiceOrderController *checkerInvoiceOrder;
@property (nonatomic,strong) NSArray *filterData;

@end
