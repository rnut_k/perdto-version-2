//
//  FilterInvioceViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 6/3/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "FilterInvioceViewController.h"

@interface FilterInvioceViewController (){
    NSMutableArray *dataVendor;
}

@end

@implementation FilterInvioceViewController

-(instancetype)init{
    if (self) {
        self.filterData = [NSMutableArray array];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataVendor = [NSMutableArray arrayWithArray:self.filterData];
    [dataVendor insertObject:@{@"name":AMLocalizedString(@"คู่ค้าทั้งหมด", nil),
                               @"id":@"0"} atIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [dataVendor count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idCell = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idCell];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idCell];
    }
    
    NSInteger row = indexPath.row;
    NSDictionary *dVendor = dataVendor[row];
    [cell.textLabel setText:dVendor[@"name"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
//    NSLog(@"%ld",(long)row);
    [self.checkerInvoiceOrder sortVendor:row];
    [self.checkerInvoiceOrder btnCancelGoods:nil];
}
@end
