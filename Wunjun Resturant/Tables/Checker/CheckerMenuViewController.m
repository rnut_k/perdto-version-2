//
//  CheckerMenuViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CheckerMenuViewController.h"
#import "CustomNavigationController.h"

#import "UIViewController+ECSlidingViewController.h"
#import "DeviceData.h"
#import "Member.h"
#import "MenuDB.h"
#import "Constants.h"
#import "LoadMenuAll.h"
#import "Member.h"

@interface CheckerMenuViewController(){
    Member *_member;
    MenuDB *_menuDB;
}
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;
@end

@implementation CheckerMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    _member = [Member getInstance];
    [_member loadData];
    [self setUpLabel];

}
-(void)setUpLabel{
//    @property(weak,nonatomic)IBOutlet UILabel *labelHome;
//    @property(weak,nonatomic)IBOutlet UILabel *labelInvoiceList;
//    @property(weak,nonatomic)IBOutlet UILabel *labelInventoryReport;
//    @property(weak,nonatomic)IBOutlet UILabel *labelUpdateInventory;
//    @property(weak,nonatomic)IBOutlet UILabel *labelSignOut;
    _labelHome.text = AMLocalizedString(@"main_menu", nil);
    _labelInvoiceList.text = AMLocalizedString(@"invoice_list", nil);
    _labelInventoryReport.text = AMLocalizedString(@"inventory_report", nil);
    _labelUpdateInventory.text = AMLocalizedString(@"update_inventory", nil);
    _labelSignOut.text = AMLocalizedString(@"sign_out", nil);
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel * sectionHeader = [[UILabel alloc] initWithFrame:CGRectZero];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    sectionHeader.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.text = AMLocalizedString(@"เมนู", nil);
    return sectionHeader;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *menuItem = [@(indexPath.row) stringValue];
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
//    
    if ([menuItem isEqualToString:@"0"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
//        self.slidingViewController.topViewController = self.transitionsNavigationController;
    }
    else if ([menuItem isEqualToString:@"1"]) {
        self.slidingViewController.topViewController = self.transitionsNavigationController;
    }
    else if ([menuItem isEqualToString:@"2"]) {
        CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerStockReport"];
        [cashSalesReportView setBlIsStockReport:YES];
        self.slidingViewController.topViewController = cashSalesReportView;
    }
    else if ([menuItem isEqualToString:@"3"]) {
        CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerStockCheck"];
        [cashSalesReportView setBlIsStockManege:YES];
        self.slidingViewController.topViewController = cashSalesReportView;
    }
    else if ([menuItem isEqualToString:@"4"]) {
        [LoadMenuAll logoutView:@{@"error":@"101"}];
    }
    [self.slidingViewController resetTopViewAnimated:YES];
}


#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender   // ยกเลิก prepareForSegue
{
//    if(![identifier isEqualToString:@"logout"])  {
//        return NO;
//    }
    return YES;
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
