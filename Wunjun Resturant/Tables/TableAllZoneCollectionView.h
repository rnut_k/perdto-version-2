//
//  TableAllZoneCollectionView.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "KRLCollectionViewGridLayout.h"
#import "TableCollectionViewCell.h"
#import "CNPPopupController.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"

#import "TableSumViewController.h"
#import "TableMoveViewController.h"
#import "CashTableStatus.h"

@interface TableAllZoneCollectionView : UICollectionViewController<THSegmentedPageViewControllerDelegate,CNPPopupControllerDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *mAllZoneCV;
@property (nonatomic,strong) NSString *viewTitle;
@property (nonatomic) int numTable;
@property (nonatomic,strong) NSMutableArray *dataListZoneAll;
@property (nonatomic,strong) NSDictionary *dataListZone;
@property (nonatomic,strong) NSArray *dataListTables;
@property (nonatomic)        NSInteger numMode;

@property (nonatomic,strong) TableSumViewController *tableSumView;
@property (nonatomic,strong) TableMoveViewController *tableMoveView;
@property (nonatomic,strong) CashTableStatus *cashTableStatus;

@property (nonatomic,strong) NSString *role;

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
-(void)loadDataOpenTable:(id)idTable andType:(NSString *)type;
@end
