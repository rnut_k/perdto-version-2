//
//  KitchenViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/17/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "KitchenViewController.h"
#import "FoodSpecialtyViewController.h"
#import "CustomNavigationController.h"
#import "Check.h"
#import "OrderListKitchenViewController.h"

@interface KitchenViewController (){
    LoadProcess *loadProgress;
    Member *member;
    MenuDB *menuDB;
    NSMutableArray *rowData;
    FoodSpecialtyViewController *foodSpecialty;
}

@end

@implementation KitchenViewController
 
- (void)viewDidLoad {
    [super viewDidLoad];
    
    CustomNavigationController *vc = (CustomNavigationController *)self.parentViewController;
    if ([vc.selfClass isMemberOfClass:[FoodSpecialtyViewController class]]) {
        foodSpecialty = vc.selfClass;
    }
    
    loadProgress = [LoadProcess sharedInstance];
    member = [Member getInstance];
    [member loadData];
    [self setTitle:AMLocalizedString(@"ครัว", nil)];
    
    self.tableKitchenList.delegate = self;
    self.tableKitchenList.dataSource = self;
    self.automaticallyAdjustsScrollViewInsets = YES;
    [self getKitchenList];
}

- (void) getKitchenList
{
    menuDB = [MenuDB getInstance];
    [menuDB loadData:@"menu_0"];
    NSDictionary *data = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    rowData = [[NSMutableArray alloc]init];
    for(id key in [data objectForKey:@"kitchen"]){
        [rowData addObject:data[@"kitchen"][key]];
    }
    [self.tableKitchenList reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [rowData count];
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }

    cell.layer.borderWidth = 1.0f;
    cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    cell.clipsToBounds = NO;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.masksToBounds = YES;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    cell.textLabel.text = rowData[indexPath.row][@"name"];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@" index %ld",(long)[indexPath  row]);
    if (foodSpecialty) {
        [foodSpecialty setDataKitchen:rowData[indexPath.row]];
        [foodSpecialty btnClosePop:nil];
    }
}

#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender // ยกเลิก prepareForSegue
{
    if(foodSpecialty)  {
        return NO;
    }
    return YES;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"selectKitchen"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [prefs setValue:@"kitchen" forKey:@"role"];
        [prefs setValue:rowData[indexPath.row][@"id"] forKey:@"kitchen_id"];
        
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center postNotificationName:@"UIApplicationConnectSocket"
                              object:self
                            userInfo:nil];
        
        
        OrdersListViewController *obj = [segue destinationViewController];
        [obj setRole:@"kitchen"];
        [obj setKitchenId:rowData[indexPath.row][@"id"]];
        [obj setKitchenName:rowData[indexPath.row][@"name"]];
        [obj setTypePrintCut:rowData[indexPath.row][@"type"]];
    }
    else if([[segue identifier] isEqualToString:@"logoutKitchen"]){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
