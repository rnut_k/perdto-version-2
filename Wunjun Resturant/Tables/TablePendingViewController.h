//
//  TablePendingViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "OrdersKitchen.h"
#import "UIColor+HTColor.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "Time.h"
#import "Text.h"
#import "NSDictionary+DictionaryWithString.h"

#import "CustomNavigationController.h"
#import "CNPPopupController.h"

@interface TablePendingViewController : UITableViewController<THSegmentedPageViewControllerDelegate>
@property (nonatomic,strong) NSString *viewTitle;
@property (nonatomic,assign) int type; 
@property (nonatomic,strong) NSDictionary *counterData;
//@property (nonatomic) BOOL showShare;
@property (nonatomic,strong) UIImage *imageScreenInvioce;

-(void)updataPending:(NSNotification *)notification;
//- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
@end
