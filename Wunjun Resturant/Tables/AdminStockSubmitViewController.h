//
//  AdminStockSubmitViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 4/10/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomNavigationController.h"
#import "WYPopoverController.h"
#import "AdminStockPopViewController.h"
#import "CellTableViewAll.h"
#import "UIColor+HTColor.h"
#import "Check.h"
#import "LocalizationSystem.h"

@interface AdminStockSubmitViewController : UIViewController<WYPopoverControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *reportTable;
@property (weak, nonatomic) IBOutlet UILabel *reportTitle;


//label
@property (nonatomic,weak)IBOutlet UILabel *labelReport;
@property (nonatomic,weak)IBOutlet UILabel *labelCheckStock;



- (IBAction)dismissModal:(id)sender;

@end
