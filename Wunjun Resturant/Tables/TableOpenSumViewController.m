//
//  TableOpenSumViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableOpenSumViewController.h"
#import "TableBillMenuViewController.h"
#import "TableAllZoneCollectionView.h"
#import "TableZoneCheckViewController.h"

#import "Time.h"
#import "Member.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "TableSubBillViewController.h"
#import "TableSingleBillViewController.h"
#import "GlobalBill.h"
#import "LoadMenuAll.h"
#import "SelectBill.h"
#import "Check.h"
#import "NSString+GetString.h"

@interface TableOpenSumViewController ()<CNPPopupControllerDelegate>{
    LoadProcess *loadProcess;
}
@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, strong) GlobalBill *globalBill;
@property (nonatomic, strong) Member *member;
@end
@implementation TableOpenSumViewController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

-(GlobalBill *)globalBill{
    if (!_globalBill) {
        _globalBill = [GlobalBill sharedInstance];
    }
    return _globalBill;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    loadProcess = [LoadProcess sharedInstance];
    
    [self.mTimeTable setText:[Time getTime]];
    [self.mCountMen setDelegate:self];
    [self.mCountWomen setDelegate:self];
    [self setTitle:kTableSum];
    
    self.addOpenTable = [NSMutableDictionary dictionary];
    
    self.otpTableTxt.placeholder = self.dataTable[@"id"];
    self.otpValTxt.placeholder = [self randomStringWithLength:4];
}
-(void)loadDataOpenTable{
    [loadProcess.loadView showWithStatus];
    NSMutableArray *listarr = [NSMutableArray arrayWithObject:self.dataTable[@"id"]];
    [listarr addObjectsFromArray:[self.addOpenTable allValues]];
    
    NSError *error = nil;
    NSString *cartID = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:listarr
                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                          error:&error]
                                               encoding:NSUTF8StringEncoding];
    
    NSString *otpTable = ([self.otpTableTxt.text isEqualToString:@""])?self.otpTableTxt.placeholder:self.otpTableTxt.text;
    NSString *otpVal = ([self.otpValTxt.text isEqualToString:@""])?self.otpValTxt.placeholder:self.otpValTxt.text;
    NSString *otp = [NSString stringWithFormat:@"%@%@", otpTable,otpVal];
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"in",
                                 @"table_data":@{
                                         @"id":cartID,
                                         @"n_male":self.mCountMen.text,
                                         @"n_female":self.mCountWomen.text
                                         },
                                 @"otp":[otp uppercaseString]
                                 };
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,self.member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        NSDictionary *_data = dictionary[@"data"];
        if ([dictionary[@"status"] boolValue]) {
            [SelectBill setCheckOpenTable:NO];   // no = เปิดโต็ะ
            if (self.cashTableStatus != nil){
                [SelectBill setIscashTableStatus:YES];
            }
            else{
                [SelectBill setIscashTableStatus:NO];
            }
            [SelectBill setIsCashier:NO];
            [SelectBill setIsTake:3];
            [SelectBill selectBill:self andData:_data andDataTable:self.dataTable fromRole:nil];
        }
        else{
             [loadProcess.loadView showError];
        }
    }];
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    UIViewController *controller = segue.destinationViewController;
    THSegmentedPager *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    NSMutableArray *pages = [NSMutableArray new];
    for (NSDictionary *dic in self.dataListZoneAll) {
        TableZoneCheckViewController  *_tableAllZone = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableZoneCheck"];
        [pages addObject:_tableAllZone];
        [_tableAllZone setViewTitle:dic[@"name"]];
        [_tableAllZone setDataListZone:self.dataListZoneAll];
        [_tableAllZone setNumTable:[Check checkDevice]?3:6];
        [_tableAllZone setTableOpenSumView:self];
        [_tableAllZone setDataDetailZone:dic];
        
        [LoadMenuAll retrieveData:[dic objectForKey:@"id"] success:^(NSArray *dataTables){
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableAllZone setDataListTables:dataTables];
                [_tableAllZone.collectionView reloadData];
            });
        }];
    }
    [pager setPages:pages];
    [controller addChildViewController:pager];
    [controller.view addSubview:pager.view];
    [pager didMoveToParentViewController:controller];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing  %@  ",textField.text);
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@" textFieldDidEndEditing %@  ",textField.text);
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    if (self.mCountMen != textField) {
        self.mCountTotal.text = [@([number integerValue]) stringValue];
    }
    else{
        self.mCountTotal.text = [@([self.mCountWomen.text  integerValue] + [number integerValue]) stringValue];
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char,"\b"); // เช็กช่องว่าง
    if (isBackSpace == -8) {
        // is backspace
        if (self.mCountWomen == textField){
            self.mCountWomen.text = [self.mCountWomen.text substringToIndex:[textField.text length]-1];
            self.mCountTotal.text = self.mCountMen.text;
        }
        else{
            self.mCountMen.text = [self.mCountMen.text substringToIndex:[textField.text length] -1];
            self.mCountTotal.text = self.mCountWomen.text;
        }
    }
    if (number)
        return YES;
    else
        return NO;
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)mTagKeybroad:(id)sender {
    [_mCountMen resignFirstResponder];
    [_mCountWomen resignFirstResponder];
}

- (IBAction)btClickOpenTable:(id)sender {
    if (![self.mCountWomen.text isEqual:@""] && ![self.mCountMen.text isEqual:@""] && !([self.mCountWomen.text isEqual:@"0"] && [self.mCountMen.text isEqual:@"0"])) {
        [self loadDataOpenTable];
    }
    else{
        [loadProcess.loadView showWithStatusTitle];
    }
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int32_t)[letters length])]];
    }
    
    return randomString;
}
@end
