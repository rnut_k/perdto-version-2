//
//  TableComboSetViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 10/15/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "TableComboSetViewController.h"
#import "CustomNavigationController.h"

#import "Member.h"
#import "LoadMenuAll.h"
#import "Member.h"
#import "Constants.h"
#import "SearchPromotion.h"
#import "NSDictionary+DictionaryWithString.h"
#import "GlobalBill.h"
#import "Check.h"
#import "NSString+GetString.h"
#import "WYPopoverController.h"
#import "UIColor+HTColor.h"
#import "UIImageView+AFNetworking.h"
#import "CellTableViewAll.h"

@interface TableComboSetViewController ()<WYPopoverControllerDelegate>{
    GlobalBill *globalBill;
    LoadProcess *loadProgress;
    WYPopoverController *popoverController;
    
    NSArray *giftValueList;
    NSMutableArray *dataComboSet;
    
}

@end

@implementation TableComboSetViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    globalBill   = [GlobalBill sharedInstance];
    loadProgress = [LoadProcess sharedInstance];
    if (!self.isPopUP) {
        [self loadDataListItemPromotiom];
    }
    else{
        [self.mSizeBtnDone setConstant:0.0];
        [self.mDone setHidden:YES];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadDataListItemPromotiom{

    NSString *giftValue = [NSString checkNull:self.dataPromotionInfo[@"gift_value"]];
    NSData *data = [giftValue dataUsingEncoding:NSUTF8StringEncoding];
    id dict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    giftValueList  = [NSArray arrayWithArray:dict];
    
    self.indexSeclect = 0;
    self.dataComboSetSeclect = [NSMutableArray array];
    for (int i = 0; i < [giftValueList count]; i++)
         [self.dataComboSetSeclect addObject:@""];
    
    MenuDB *menuDB = [MenuDB getInstance];
    NSString *typeDB = @"menu_0";
    [menuDB loadData:typeDB];
    NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    NSMutableArray *menuList = [NSMutableArray arrayWithArray:(NSMutableArray *)[json[@"menu"] allValues]];
    
    dataComboSet = [NSMutableArray array];
    for (NSArray *listData in giftValueList) {
        NSMutableArray *arr = [NSMutableArray array];
        if ([listData count] == 0) continue;
        for (NSString *key in listData) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id = %@)",[NSString checkNull:key]];
            id dic = [menuList filteredArrayUsingPredicate:predicate];
            if (![Check checkNull:dic]) {
                [arr addObject:[dic firstObject]];
            }
        }
        
        if ([arr count] > 0) {
            [dataComboSet addObject:arr];
        }
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.isPopUP) {
        return [self.dataComboSetList count];
    }
    else{
        return [giftValueList count];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        NSString *CellIdentifier;
        if (self.isPopUP) {
            CellIdentifier = @"cellComboSetPopup";
        }
        else{
            CellIdentifier = @"cellComboSet";
        }
        CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        NSInteger row = [indexPath row];
        if (self.isPopUP) {
            cell.mNameOrder.textColor = [UIColor blackColor];
            NSDictionary *dic = [NSDictionary dictionaryWithObject:self.dataComboSetList[row]];
            NSString *_singImage = dic[@"image_url"];
            NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];

            [cell.mNameOrder setText:dic[@"name"]];
            [cell.mImageBackgroud setImageWithURL:[NSURL URLWithString:dataSing[@"th_url"]]];
        }
        else{
            NSDictionary *data = self.dataComboSetSeclect[row];
            if (![Check checkNull:data]) {
                cell.mNameOrder.textColor = [UIColor blackColor];
                NSString *_singImage = data[@"image_url"];
                NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];
                
                [cell.mNameOrder setText:data[@"name"]];
                [cell.mImageBackgroud setImageWithURL:[NSURL URLWithString:dataSing[@"th_url"]]];
            }
            else{
                cell.mNameOrder.textColor = [UIColor grayColor];
                [cell.mNameOrder setText:AMLocalizedString(@"รายการ", nil)];
            }
        }
        [self setLine:cell.mView];
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"exception cellForRowAtIndexPath : %@ ",exception);
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSInteger row = [indexPath row];
        if (self.isPopUP) {
            NSDictionary *data = self.dataComboSetList[row];
            [self.tableComboSet.dataComboSetSeclect replaceObjectAtIndex:self.indexSeclect withObject:data];
            [self.tableComboSet btnClosePop:nil];
            [self.tableComboSet.mTableSeclectComboset reloadData];
            
            NSInteger index = [self.tableComboSet.dataComboSetSeclect indexOfObject:@""];
            if (index == NSNotFound) {
                [self.tableComboSet.mDone setBackgroundColor:[UIColor colorWithRed:0.0 green:0.502 blue:1.0 alpha:1.0]];
            }
        }
        else{
            NSArray *listData = dataComboSet[row];
            self.indexSeclect = row;
            [self createPopoverController:listData];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception cellForRowAtIndexPath : %@ ",exception);
    }
}

#pragma mark - create popover Controller

- (void)createPopoverController:(NSArray *)data {

    TableComboSetViewController *viewPop = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableComboSetView"];
    viewPop.title = AMLocalizedString(@"รายการ", nil);
    viewPop.mTableSeclectComboset.delegate = self;
    viewPop.mTableSeclectComboset.dataSource = self;
    
    CGSize size = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    viewPop.preferredContentSize = CGSizeMake(size.width,size.height);
    viewPop.modalInPopover = NO;
    
    CustomNavigationController* contentViewController = [[CustomNavigationController alloc] initWithRootViewController:viewPop];
    [contentViewController setColorBar];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    
    UIImage *image = [[UIImage imageNamed:@"close"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(btnClosePop:)];
    [viewPop.navigationItem setRightBarButtonItem:rightBarButtonItem];
 
    [viewPop setDataComboSetList:[NSMutableArray arrayWithArray:data]];
    [viewPop setIsPopUP:YES];
    [viewPop setTableComboSet:self];
    [viewPop setIndexSeclect:self.indexSeclect];
}
-(void)setLine:(UIView *)obj{
    obj.layer.borderWidth = 1.0f;
    obj.layer.borderColor = [UIColor ht_cloudsColor].CGColor;
    obj.clipsToBounds = NO;
}
- (IBAction)btnDone:(id)sender{
    NSInteger index = [self.dataComboSetSeclect indexOfObject:@""];
    if (index == NSNotFound) {
        NSString *key = @"[";
        for (NSDictionary *dic in self.dataComboSetSeclect) {
            key = [[key stringByAppendingString:dic[@"id"]] stringByAppendingString:@","];
        }
        key = [[key substringToIndex:[key length]-1] stringByAppendingString:@"]"];
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
        tempDict = [self.dataPromotionInfo mutableCopy];
        [tempDict setValue:key forKey:@"gift_value"];
        
        if (self.tableSingleBillViewController != nil) {
            NSArray *arrList = [NSArray arrayWithObjects:self.tableSingleBillViewController.tableDataBillView.promotionPoolID,tempDict,nil];
            [self.tableSingleBillViewController.tableDataBillView addPromotion:arrList andDataOrder:nil];
            [self.navigationController popToViewController:self.tableSingleBillViewController animated:NO];
        }
        else{
            NSArray *arrList = [NSArray arrayWithObjects:self.tableSubBillViewController.tableDataBillView.promotionPoolID,tempDict,nil,nil];
            [self.tableSubBillViewController.tableDataBillView addPromotion:arrList andDataOrder:nil];
            [self.navigationController popToViewController:self.tableSubBillViewController animated:NO];
        }
    }
    else{
        [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาเลือกเมนูให้ครบตามจำนวน", nil)];
    }
}
-(IBAction)btnClosePop:(id)sender   {
    [popoverController dismissPopoverAnimated:YES];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
