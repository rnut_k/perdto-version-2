//
//  TableDrinkGirlsViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableDrinkGirlsViewController.h"
#import "TableBillMenuViewController.h"
#import "TableSingleBillViewController.h"

#import "Check.h"
#import "GlobalBill.h"
#import "UIColor+HTColor.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Member.h"
#import "UIImageView+AFNetworking.h"
#import "PopupView.h"
#import "MenuDB.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"

@interface TableDrinkGirlsViewController (){
    LoadProcess *loadProgress;
    Member *_member;
    MenuDB *_menuDB;
    PopupView *popupView;
    
    GlobalBill *globalBill;
    
    NSIndexPath *tmpIndexPath;
    NSMutableDictionary *drinkGirlList;
    NSMutableArray *menuList;
}

@end

@implementation TableDrinkGirlsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.mCollectionItem.collectionViewLayout;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    loadProgress  = [LoadProcess sharedInstance];
    globalBill = [GlobalBill sharedInstance];
    
    self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
//    [LoadMenuAll checkDataMenu:@"3"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_food"]
                                                  forBarMetrics:UIBarMetricsDefault];
    
    [self loadDataDrink];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)loadDataDrink{
    _menuDB = [MenuDB getInstance];
    [_menuDB loadData:@"menu_3"];
    
    NSDictionary *json = [NSDictionary dictionaryWithString:_menuDB.valueMenu];
    drinkGirlList = [NSMutableDictionary dictionaryMutableWithObject:json[@"drinkgirl"]];
    menuList = [NSMutableArray arrayWithArray:(NSMutableArray *)[Check checkObjectType:[json[@"menu"] allValues]]];
    self.drinkMenu = [NSMutableDictionary dictionaryMutableWithObject:json[@"drink_menu"]];
    [self.mCollectionItem reloadData];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [menuList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dataMenuList = menuList[indexPath.row];
    NSString *girlID  =[NSString stringWithFormat:@"id_%@",dataMenuList[@"girl_id"]];
    NSDictionary *dataDrinkGirlList = drinkGirlList[girlID];
    NSString *jsonString = dataMenuList[@"image_url"];
    if (jsonString != nil) {
        NSData* data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [cell.mImageTable setImageWithURL:[NSURL URLWithString:jsonObject[@"th_url"]]];
    }
    cell.userNameLabel.text = dataDrinkGirlList[@"nick_name"];
    cell.userNameLabel.alpha = 0.8;
    
    if ([dataDrinkGirlList[@"status"] isEqual:@"0"]) {
        [cell.mStatusDrink setImage:[UIImage imageNamed:@"ic_drink_off"]];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    tmpIndexPath  = indexPath;
    NSString *girlID =[NSString stringWithFormat:@"id_%@",menuList[tmpIndexPath.row][@"girl_id"]];
    NSString *status = drinkGirlList[girlID][@"status"];
    
    popupView = [[PopupView alloc] init];
    [popupView setStatus:status];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
- (void)btnReply:(UIImage *)singImage{
    if ([globalBill.billType  isEqual: kSingleBill]) {
        TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
            myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        }
        
        NSMutableDictionary *dataMenuList = menuList[tmpIndexPath.row];
        NSString *girlID  =[NSString stringWithFormat:@"id_%@",dataMenuList[@"girl_id"]];
        NSMutableDictionary *dataDrinkGirlList = drinkGirlList[girlID];
        
        NSMutableDictionary *_data = [NSMutableDictionary dictionary];
        [_data setObject:@""      forKey:@"Option"];
        [_data setObject:@""      forKey:@"Comment"];
        [_data setObject:@"1"     forKey:@"Count"];
        if ([kDrinkTwo  isEqual:self.typeDr])
            [_data setObject:@"0"   forKey:@"Price"];
        else
            [_data setObject:dataMenuList[@"price"] forKey:@"Price"];
        
        [_data setObject:self.typeDr     forKey:@"TypeDrink"];
        
        [_data setObject:singImage  forKey:@"StarSing"];
        [_data setObject:@""        forKey:@"StopSing"];
        [_data setObject:dataMenuList  forKey:@"data"];
        [_data setObject:dataDrinkGirlList  forKey:@"drinkGirl"];
        [_data setObject:self.typeDrinkMenu forKey:@"shot_type"];
        
        [myController.tableDataBillView addOrderDrink:_data];
        [self.navigationController popToViewController:myController animated:NO];
    }
    else if ([globalBill.billType  isEqual: kSubBill]) {
        TableSubBillViewController *myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        if (![myController isKindOfClass:[TableSubBillViewController class]]) {
            myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        }
        
        NSMutableDictionary *dataMenuList = menuList[tmpIndexPath.row];
        NSString *girlID  =[NSString stringWithFormat:@"id_%@",dataMenuList[@"girl_id"]];
        NSMutableDictionary *dataDrinkGirlList = drinkGirlList[girlID];
        
        NSMutableDictionary *_data = [NSMutableDictionary dictionary];
        [_data setObject:@""      forKey:@"Option"];
        [_data setObject:@""      forKey:@"Comment"];
        [_data setObject:@"1"     forKey:@"Count"];
        if ([kDrinkTwo  isEqual:self.typeDr])
        [_data setObject:@"0"   forKey:@"Price"];
        else
        [_data setObject:dataMenuList[@"price"] forKey:@"Price"];
        
        [_data setObject:self.typeDr     forKey:@"TypeDrink"];
        
        [_data setObject:singImage  forKey:@"StarSing"];
        [_data setObject:@""        forKey:@"StopSing"];
        [_data setObject:dataMenuList  forKey:@"data"];
        [_data setObject:dataDrinkGirlList  forKey:@"drinkGirl"];
        
        [myController.tableDataBillView addOrderDrink:_data];
        [self.navigationController popToViewController:myController animated:NO];
    }
    else{
        TableBillMenuViewController *myController = (TableBillMenuViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        [self.navigationController popToViewController:myController animated:NO];
    }
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (searchBar.text.length > 0) {
        searchBar.text = nil;
        [searchBar resignFirstResponder];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSMutableArray *dataSearch = [NSMutableArray array];
    NSMutableArray *menuListTmp = [NSMutableArray arrayWithArray:menuList];
    
    if (![searchText  isEqual: @""]) {
        [menuList removeAllObjects];
        NSArray *keyList = [drinkGirlList allKeys];
        NSMutableArray *listData = [NSMutableArray array];
        for (NSString *str in keyList) {
            [listData addObject:[drinkGirlList valueForKey:str]];
        }
        
        for (NSDictionary *data in listData) {
            searchText  = [searchText lowercaseString];
            NSString *menu = data[@"name" ];
            NSLog(@"click cell at %@  :%@",searchText,menu);
            if ([menu containsString:searchText]) {
                NSString *key =    data[@"id"];
                NSString *value =  @"girl_id = %@";
                NSArray *arr = [NSDictionary searchDictionary:key andvalue:value  andData:menuListTmp];
                if (arr != nil) {
                    [dataSearch addObject:arr];
                }               
            }
            else {
                NSLog(@"string does not contain bla");
            }
        }
        if ([dataSearch count] != 0)
        menuList = dataSearch;
        else
        menuList = menuListTmp;
    }
    else{
        NSDictionary *json = [NSDictionary dictionaryWithString:_menuDB.valueMenu];
        menuList = [NSMutableArray arrayWithArray:(NSMutableArray *)[json[@"menu"] allValues]];
    }
    [self.mCollectionItem reloadData];
    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:NO];
    return YES;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:NO];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}
- (IBAction)getImageBtnPressed:(UIImage *)sender
{
    UIImage *image =  sender; //[signatureView getSignatureImage];
    if (image != nil) {
        [popupView btnClosePop:popupView.wyPopoverController];
        [self btnReply:image];
    }
    else{
        [loadProgress.loadView showWithStatusSignature];
    }
}
- (IBAction)btnBack:(id)sender {
//    [KVNProgress showWithStatus:@"Loading..."];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBackHome:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
@end
