//
//  TableZoneCheckViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "KRLCollectionViewGridLayout.h"
#import "TableCollectionViewCell.h"
#import "CNPPopupController.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "TableOpenSumViewController.h"
#import "LocalizationSystem.h"

@interface TableZoneCheckViewController : UICollectionViewController<THSegmentedPageViewControllerDelegate,CNPPopupControllerDelegate>

@property (nonatomic,strong) NSDictionary *dataDetailZone;
@property (nonatomic,strong) NSMutableArray *dataListZone;
@property (nonatomic,strong) NSArray *dataListTables;
@property (nonatomic,strong) NSString *viewTitle;
@property (nonatomic) int numTable;

@property(nonatomic,strong) TableOpenSumViewController *tableOpenSumView;
@end
