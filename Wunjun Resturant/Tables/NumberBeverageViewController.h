//
//  NumberBeverageViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadProcess.h"

@interface NumberBeverageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *mTitleItem;
@property (weak, nonatomic) IBOutlet UILabel *mImageTitle;
@property (weak, nonatomic) IBOutlet UILabel *mCountNumber;

@property (nonatomic,strong) NSString *strTitleBeverage;
@property (nonatomic,strong) UIImage  *imgTitleBeverage;
@property (nonatomic,strong) NSString *priceBeverage;
@property (nonatomic,weak)   NSDictionary *dataMenuList;


- (IBAction)btnBack:(id)sender;
- (IBAction)btnCilckAdd:(id)sender;
- (IBAction)btnCilckSub:(id)sender;

- (IBAction)btnReply:(id)sender;
- (IBAction)btnBackHome:(id)sender ;
@end
