//
//  OrdersListViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "OrdersKitchen.h"
#import "OrderListKitchenViewController.h"
#import "WYPopoverController.h"
#import "NSDictionary+DictionaryWithString.h"
#import "DeviceData.h"
#import "CNPPopupController.h"

@interface OrdersListViewController : UIViewController<WYPopoverControllerDelegate, UITableViewDataSource, UITableViewDelegate,CNPPopupControllerDelegate>

@property (nonatomic) BOOL blClikeChecker;
@property (nonatomic,strong) NSString *kitchenId;
@property (nonatomic,strong) NSString *kitchenName;
@property (nonatomic,strong) NSString *typePrintCut;
@property (weak, nonatomic) IBOutlet UIView *tabContainer;

- (IBAction)actionBlockBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *selectModePrint;
@property (weak, nonatomic) IBOutlet UITextField *nPrint;
@property (weak, nonatomic) IBOutlet UIButton *printOne;
@property (weak, nonatomic) IBOutlet UIButton *printAll;

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;

@property (nonatomic,strong) NSString *role;
@end
