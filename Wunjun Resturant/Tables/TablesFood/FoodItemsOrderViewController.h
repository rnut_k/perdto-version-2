//
//  FoodItemsOrderViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellFoodItemView.h"
#import "OTPageScrollView.h"
#import "OTPageView.h"
#import "CNPPopupController.h"
#import "LoadProcess.h"
#import "TableSingleBillViewController.h"
#import "TableSubBillViewController.h"

@interface FoodItemsOrderViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *mCollectionMenu;

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollTagCategory;
@property (weak, nonatomic) IBOutlet UITableView *mTableTagFood;
//@property (weak, nonatomic) IBOutlet UITableView *mTableMenuFood;
@property (weak, nonatomic) IBOutlet UITableView *mTableCategoryFood;
@property (weak, nonatomic) IBOutlet UISearchBar *searchField;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutTopTableTagFood;

@property (nonatomic ,strong)  NSMutableArray *dataMenu;       // ข้อมูลทั้งหมด
@property (nonatomic ,strong)  NSMutableArray *dataMenuList;   // เก็บข้อมูลเมนูโชว์
@property (nonatomic,strong)   NSMutableArray *dataTagAdd;

@property (nonatomic,strong)   TableSingleBillViewController *tableSingleBillViewController;
@property (nonatomic,strong)   TableSubBillViewController *tableSubBillViewController;

@property (nonatomic,strong)  NSMutableArray *addFoodBill;
@property (nonatomic,strong)  NSMutableDictionary *addFoodBillList;
@property (nonatomic,strong)  NSMutableDictionary *deleteTag;


- (IBAction)btnTagKeybroad:(UITapGestureRecognizer *)gestureRecognizer;
- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)gestureRecognizer;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnPopMenu:(id)sender;
- (IBAction)btnBackHome:(id)sender;

-(void)loadViewFood:(NSString *)key;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
-(void) addOrderAppvoceoption:(NSDictionary *)menuData option:(id)option  comment:(NSString *)comment count:(int)count;
@end
