//
//  FoodOptionSum.m
//  Wunjun Resturant
//
//  Created by AgeNt on 8/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "FoodOptionSum.h"
#import "NSDictionary+DictionaryWithString.h"
#import "Check.h"

@implementation FoodOptionSum

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

+(NSString *) getFoodOption:(id)data{
    
    NSString *option = @"";
    NSArray *optionList = [NSArray arrayWithObject:data];
    if ([optionList count] != 0) {
        id dataOptionFoodList = [NSDictionary dictionaryWithString:[optionList firstObject]];
        if ([Check checkObjectType:dataOptionFoodList]) {
            if ([dataOptionFoodList isKindOfClass:[NSArray class]]) {
                NSMutableString *tempStr = [NSMutableString string];
                for (NSDictionary *dicOption in dataOptionFoodList) {
                    id nameList = dicOption[@"name"];
                    [tempStr appendString:[FoodOptionSum typeDicTostring:nameList]];
                    [tempStr appendString:@" , "];
                }
                if ([tempStr length] > 0) {
                    option = [tempStr substringToIndex:[tempStr length] - 2];
                }
                else
                    option = [NSString stringWithString:tempStr];
            }
            else{
                if ([dataOptionFoodList isEqualToString:@"[]"]) {
                    dataOptionFoodList = @"";
                }
                option = [NSString stringWithString:dataOptionFoodList];
            }
        }
    }
    return option;
}

+(NSString *) typeDicTostring:(id)nameList{
    Member *_member = [Member getInstance];
    [_member loadData];
    
    if ([nameList isKindOfClass:[NSDictionary class]]) {
        id str =  [nameList valueForKey:_member.lg];
        if (![Check checkNull:str]) {
            return str;
        }
        else{
            id str =  [nameList valueForKey:@"df"];
            if (![Check checkNull:str]) {
                return str;
            }
            NSString *title = [nameList valueForKey:[[nameList allKeys] firstObject]];
            return  title;
        }
    }
    return nameList;
}
@end