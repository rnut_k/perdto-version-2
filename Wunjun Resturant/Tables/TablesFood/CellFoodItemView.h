//
//  CellFoodItemView.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellFoodItemView : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *mBtnTttleTag;
@property (weak, nonatomic) IBOutlet UIButton *mBtnDelete;
@property (weak, nonatomic) IBOutlet UILabel *mCountOrder;


@end
