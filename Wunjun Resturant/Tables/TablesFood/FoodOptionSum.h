//
//  FoodOptionSum.h
//  Wunjun Resturant
//
//  Created by AgeNt on 8/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Member.h"

@interface FoodOptionSum : NSObject

@property (nonatomic,strong) Member *member;

+ (NSString *) getFoodOption:(id)data;
+ (NSString *) typeDicTostring:(id)nameList;
@end
