//
//  TableDataBillViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNPPopupController.h"

@interface TableDataBillViewController : UITableViewController<CNPPopupControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *mTableViewBill;

@property (nonatomic,strong) id selfSuper;
@property (nonatomic,strong) NSMutableArray *sectionDataIndex;
@property (nonatomic,strong) NSString  *promotionPoolID;
@property (nonatomic,strong) NSDictionary  *dataPromotion;
@property (nonatomic,strong) NSDictionary  *detailTable;
@property (strong,nonatomic) NSMutableDictionary *sectionBill;
@property (nonatomic,strong) id tableSingleBillViewController;
@property (nonatomic,strong) NSString *tableID;
@property (nonatomic,strong) NSString *indexSubBill;
@property (nonatomic,strong) NSDictionary *dataSubBill;
@property (nonatomic,strong) NSMutableDictionary *dataImagePrint;
@property (nonatomic,strong) NSMutableArray *dataImagePrintList;

@property (nonatomic) bool isAdddataPrint; // จัดเก็บข้อมูลปริ้นหรือไม่

@property (nonatomic) bool useSubBill;
@property (nonatomic) bool isSignAll;
@property (nonatomic) bool isCashier;
@property (nonatomic) BOOL isCashierTake;
@property (nonatomic) BOOL isStaffTake;
@property (nonatomic) bool isPopupCashier;
@property (nonatomic) bool isPrintSubBill;
@property (nonatomic) bool isPrintAllSubBill;


@property (nonatomic,strong) NSString *role;
- (void)reloadData;
- (IBAction)btnClosePop:(id)sender;
- (IBAction)btnOK:(NSNotification *)note;
- (IBAction)btnClose:(NSNotification *)note;
- (IBAction)getImageBtnPressed:(UIImage *)sender;
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender;
- (IBAction)checkStopDrink:(NSNotification *)_note ;
- (IBAction)imageSingSubBill:(id)sender;

-(void)addOrder:(NSMutableArray *)dataAddFoodBill andPro:(NSArray *)proList;
-(void)addOrderDrink:(NSMutableDictionary *)dataOrder;
-(void)addPromotion:(NSArray *)dataPro andDataOrder:(NSDictionary *)dataOrder;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
-(void)cencelOrderApprove:(NSDictionary *)data andUser:(NSDictionary *)user;
@end
