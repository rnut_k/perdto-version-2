//
//  FoodItemsOrderViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "FoodItemsOrderViewController.h"
#import "WYPopoverController.h"
#import "CellTableViewAll.h"
#import "TableOrderFoodViewController.h"
#import "Constants.h"
#import "MenuDB.h"
#import "AFNetworking.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "WYStoryboardPopoverSegue.h"
#import "PopupView.h"
#import "LoadMenuAll.h"
#import "Check.h"
#import "OrderDB.h"
#import "KRLCollectionViewGridLayout.h"
#import "CellCheckAmountTableViewCell.h"

#import "FoodOptionSum.h"
#import "NSString+GetString.h"
#import "UIColor+Conventcolor.h"
#import "FMDBDataAccess.h"

#define kSpecialtyFood (@"specialtyFood")

@interface FoodItemsOrderViewController()<OTPageScrollViewDataSource,OTPageScrollViewDelegate,WYPopoverControllerDelegate,UIAlertViewDelegate>{
    OTPageView *pscrollView;
    PopupView *popupView;
    WYPopoverController *popoverController;
    LoadProcess *loadProgress;
    
    NSMutableArray *dataCategory;
    NSMutableArray *dataCategoryList;
    NSMutableDictionary *dataTagFoodList;
    
    UITableViewController *viewControllerShowMenu;
    UITableView *tableListShowMenu;
    
    NSDictionary *_dataPageScrollView;
    UITableView *tableSreachPop;
    NSMutableDictionary *dataSearchTag;
    NSInteger srocllIndex;
    NSString *titlePop;
    
    int tagScroll;
    
//    OrderDB *orderDB;
    BOOL isCheckOrderDB;
    BOOL isCheckSubmit;
    
    
    UIButton *btnAlertView;
    
    UIImageView *imageback;
    UIImageView *imagenext;
    BOOL searchBarBegin;
    
    
    NSDictionary *dataMenuDic;
    NSDictionary *menuDataSeclect;
    NSDictionary *menuUpsale;
    NSDictionary *menuUpPromotion;
    NSDictionary *cheerOption;
}

@property (nonatomic ,strong) PrinterDB *printerDB;
@property (nonatomic ,strong) OrderDB *orderDB;
@end

@implementation FoodItemsOrderViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(PrinterDB *)printerDB{
    if (!_printerDB) {
        FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
        _printerDB = [db getCustomers]; 
    }
    return _printerDB;
}
-(OrderDB *)orderDB{
    if (!_orderDB) {
        _orderDB = [OrderDB getInstance];
    }
    return _orderDB;
}
- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.mCollectionMenu.collectionViewLayout;
}
- (void)viewDidLoad
{

    _searchField.placeholder = AMLocalizedString(@"search_menu_list", nil);
    [super viewDidLoad];
    [self initNavigation];
    
    self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
    
    self.mTableTagFood.backgroundColor = [UIColor clearColor];
    
    srocllIndex = 0;
    // Bind event to cell here
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(btnTagSelectorMenu:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.mCollectionMenu addGestureRecognizer:lpgr];
    
    // NSLog(@"Bind here, view title ++++ %@",self.title);
    
    dataSearchTag    = [NSMutableDictionary dictionary];
    self.addFoodBill = [NSMutableArray array];
    self.dataTagAdd  = [NSMutableArray array];
    popupView = [[PopupView alloc] init];
    loadProgress   = [LoadProcess sharedInstance];
     
    titlePop = @"tag";
    _dataPageScrollView  = [NSMutableDictionary dictionary];
    _dataPageScrollView = @{@"tag":@[AMLocalizedString(@"ผัก", nil),AMLocalizedString(@"เนื้อ", nil),AMLocalizedString(@"กรรมวิธี", nil),AMLocalizedString(@"อื่น ๆ", nil)],
                            @"category":@[AMLocalizedString(@"หมวดหมู่", nil)]};
    
    CGFloat sizeWidht = self.mScrollTagCategory.frame.size.width;
    pscrollView = [[OTPageView alloc] initWithFrame:CGRectMake(0,0,sizeWidht,self.mScrollTagCategory.frame.size.height)];
    pscrollView.pageScrollView.dataSource = self;
    pscrollView.pageScrollView.delegate = self;
    pscrollView.pageScrollView.padding = 10;
    pscrollView.pageScrollView.leftRightOffset = 0;
    pscrollView.pageScrollView.frame = CGRectMake(30,0,sizeWidht-60,self.mScrollTagCategory.frame.size.height);
    [pscrollView.pageScrollView reloadData];
    
    imageback =[[UIImageView alloc] initWithFrame:CGRectMake(0,3, 24 ,24)];
    imageback.image = [UIImage imageNamed:@"back"];
    
    imagenext =[[UIImageView alloc] initWithFrame:CGRectMake(sizeWidht+30,3, 24 ,24)];
    imagenext.image = [UIImage imageNamed:@"next"];
    
    [self.mScrollTagCategory addSubview:imageback];
    [self.mScrollTagCategory addSubview:imagenext];
    [self.mScrollTagCategory addSubview:pscrollView];
    
    self.dataMenuList = [NSMutableArray array];
    
    float left = 40.0;
    float right = self.view.frame.size.width-172.0;
    UIView *navTitle1 = [[UIView alloc] initWithFrame:CGRectMake(left,0,right,44)];
    UILabel *topTitle =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,navTitle1.bounds.size.width, 44)];
    topTitle.text = AMLocalizedString(@"สั่งอาหาร", nil);
    [topTitle setTextAlignment:NSTextAlignmentCenter];
    topTitle.font = [UIFont systemFontOfSize:18];
    [topTitle setTextColor:[UIColor whiteColor]];
    [navTitle1 addSubview:topTitle];
    self.navigationItem.titleView = navTitle1;

    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                               target:nil action:nil];
    fixedItem.width = 20.0f;  // or whatever you want
    
    UIImage *image = [UIImage imageNamed:@"ic_add_order"];
    CGRect frame = CGRectMake(0, 0, image.size.width+10, image.size.height+10);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(barButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    NSArray *buttons = @[fixedItem,barButtonItem,fixedItem];  // สั่งอาหาร
    self.navigationItem.rightBarButtonItems = [self.navigationItem.rightBarButtonItems arrayByAddingObjectsFromArray:buttons];
    [self loadViewFood:@"menu_0"];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    for (UIView *subview in self.searchField.subviews)
    {
        for (UIView *subSubview in subview.subviews)
        {
            if ([subSubview conformsToProtocol:@protocol(UITextInputTraits)])
            {
                UITextField *textField = (UITextField *)subSubview;
                [textField setKeyboardAppearance: UIKeyboardTypeNumberPad];
                textField.returnKeyType = UIReturnKeyDone;
                break;
            }
        }
    }
    isCheckOrderDB = YES;
    [self.mTableTagFood reloadData];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    if (isCheckOrderDB || isCheckSubmit) {
        [self initOrderDB];
    }
}
-(void)viewDidLayoutSubviews{
    CGFloat sizeWidht = self.mScrollTagCategory.frame.size.width;
    pscrollView.frame = CGRectMake(0,0,sizeWidht,self.mScrollTagCategory.frame.size.height);
    pscrollView.pageScrollView.frame = CGRectMake(30,0,sizeWidht-60,self.mScrollTagCategory.frame.size.height);
    imageback.frame = CGRectMake(0,3, 24 ,24);
    imagenext.frame = CGRectMake(sizeWidht-30,3, 24 ,24);
    [pscrollView.pageScrollView reloadData];
}

-(void)initNavigation{
    UIImage *image;
    CGSize size = self.navigationController.navigationBar.frame.size;
//    if ([Check checkDevice]) { // iphione
//        image = [UIImage imageNamed:@"top_food"];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
//            image = [UIImage imageNamed:@"top_food@3x"];
//        }
//    }
//    else{ // ipan
//        image = [UIImage imageNamed:@"top_food@3x"];
//    }
    image = [UIImage imageNamed:@"top_food"];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(size.width,size.height+22)];
    [self.navigationController.navigationBar setBackgroundImage:image
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)initOrderDB{
    NSString *idOrder = @"";
    if (self.tableSingleBillViewController.tableDataBillView) {
        idOrder = [self.tableSingleBillViewController.tableDataBillView detailTable][@"id"];
        idOrder = [NSString stringWithFormat:@"table_id_%@",idOrder];
    }
    
    if (isCheckSubmit){
        [self.orderDB updateDb:@"nil" andId:idOrder andTypeFood:YES];
    }
    else if ( isCheckOrderDB) {
        NSError *error = nil;
        NSDictionary *dic = @{@"tag":self.dataTagAdd,
                              @"bill":self.addFoodBill};
        NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic
                                                                                            options:NSJSONWritingPrettyPrinted
                                                                                              error:&error]
                                                   encoding:NSUTF8StringEncoding];
        if (![self.orderDB isKeyDB:idOrder]){
            [self.orderDB insertLoginData:cartJSON andDrink:@"orderDrink" and:idOrder];
        }
        else{
            [self.orderDB updateDb:cartJSON andId:idOrder andTypeFood:YES];
        }
    }
}
-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    [pscrollView setFrame:CGRectMake(0,0,self.mScrollTagCategory.frame.size.width,self.mScrollTagCategory.frame.size.height)];
}
    
-(void)loadViewFood:(NSString *)key{
    @try {
        self.dataMenuList = [NSMutableArray array];
        MenuDB *menuDB = [MenuDB getInstance];
        [menuDB loadData:key];
        
        NSString *idOrder;
        if (self.tableSingleBillViewController.tableDataBillView) {
            idOrder = [self.tableSingleBillViewController.tableDataBillView detailTable][@"id"];
            idOrder = [NSString stringWithFormat:@"table_id_%@",idOrder];
        }
        [self.orderDB loadData:idOrder];
 
        if (self.orderDB.orderFood) {
            NSDictionary *jsonOrder = [NSDictionary dictionaryWithString:self.orderDB.orderFood];
            if (jsonOrder) {
                self.addFoodBill = [jsonOrder[@"bill"] mutableCopy];
                self.dataTagAdd = [jsonOrder[@"tag"] mutableCopy];
            }
        }
      
        
        NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
        NSDictionary *data = [NSDictionary dictionaryWithObject:json[@"menu"]];
        NSMutableArray *keyList = [NSMutableArray array];
        [keyList addObjectsFromArray:[json[@"menu"] allKeys]];
        
        self.dataMenu = [NSMutableArray array];
        for (NSString *str in keyList) {
            NSDictionary *dic = data[str];
            if ([dic[@"type"] isEqualToString:@"1"]) {
                [self.dataMenu addObject:dic];
            }
            
        }

        if ([self.dataMenu count] != 0) {
            self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenu];
        }
        
        NSDictionary *listUsaageTemp = [NSDictionary dictionaryWithDictionary:[Check checkObjectType:json[@"category"]]];
        NSString *keyBill = @"1"; //@"food";
        NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(category_type = %@)" andData:[listUsaageTemp allValues]];
        dataCategoryList = [NSMutableArray arrayWithArray:listUsaage];
        
        dataTagFoodList =  json[@"tag"][@"food"];
        
        if ([dataTagFoodList count] != 0) {
            [self getDataCategory:1];
        }
        if ([dataCategoryList count] != 0) {
            titlePop = @"category";
            [self showDataCategoryList];
            [pscrollView.pageScrollView reloadData];
            imageback.image = nil;
            imagenext.image = nil;
        }
        if ([self.dataMenu count] != 0){
            self.dataMenuList = [self sortDataOrder:self.dataMenuList andKey:@"order"];
            [self.mCollectionMenu reloadData];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error exception loadViewFood : %@ ",exception);
    }
  
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.mTableCategoryFood){
        return 80;
    }
    return tableView.rowHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.mTableCategoryFood)
       return [dataCategory count];
     else if (tableView == tableListShowMenu)
       return [self.dataMenuList count];
    else if (tableView == self.mTableTagFood){
        self.mTableTagFood.backgroundColor = ([self.dataTagAdd count] == 0)?[UIColor clearColor]:[UIColor whiteColor];
        return [self.dataTagAdd count];
    }
    else if (tableView == tableSreachPop)
        return 5;

    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"CellFoodItem";
    
    if (tableView == self.mTableTagFood) {
        CellFoodItemView *_cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (_cell == nil) {
            _cell = [[CellFoodItemView alloc]initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:identifier];
        }
        NSDictionary *dic = [self.dataTagAdd objectAtIndex:indexPath.row];
        
        [_cell.mBtnTttleTag setTitle:dic[@"data"][@"name"] forState:UIControlStateNormal];
        [_cell.mBtnTttleTag addTarget:self action:@selector(showClickDeleteTag:)
                     forControlEvents:UIControlEventTouchUpInside];
        _cell.mBtnTttleTag.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [_cell.mCountOrder setText:[NSString stringWithFormat:@"(%@)",dic[@"count"]]];
        [self setLineView:_cell];
         return _cell;
    }
    else if(tableView == self.mTableCategoryFood){
        CellCheckAmountTableViewCell *cell = (CellCheckAmountTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellCategory"];
        if (cell == nil) {
            cell = [[CellCheckAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                       reuseIdentifier:@"cellCategory"];
        }
        NSDictionary *dic = [dataCategory objectAtIndex:indexPath.row];
        int num = [dic[@"id"] intValue] * 100;
        [cell.mNameGoods setText:[dic objectForKey:@"name"]];
        [cell.mOrderNumber setText:[@(num) stringValue]];
        

        
        if (dic[@"color"] != nil) {
            cell.backgroundColor = [UIColor colorWithHexString:dic[@"color"]];
        }
        else
            cell.backgroundColor = [UIColor clearColor];
        [self setLineView:cell];
        return cell;
    }
    else if (tableView == tableSreachPop){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        switch (indexPath.row) {
            case 0:{
                cell.textLabel.text = AMLocalizedString(@"ค้นหาแบบแท็ก", nil);
                cell.imageView.image = [UIImage imageNamed:@"search"];
            }break;
            case 1: {
                cell.textLabel.text = AMLocalizedString(@"ค้นหาแบบหมวดหมู่", nil);
                cell.imageView.image = [UIImage imageNamed:@"search"];
            } break;
            case 2: cell.textLabel.text = AMLocalizedString(@"รีเซ็ตการค้นหา", nil);  break;
            case 3: cell.textLabel.text = AMLocalizedString(@"อัพเดตเมนูอาหาร",nil);  break;
            default: cell.textLabel.text = AMLocalizedString(@"สั่งอาหารนอกเมนู", nil);  break;
        }
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        return cell;
    }
    if (tableListShowMenu == tableView) {       // popup
        CellTableViewAll *_cell = (CellTableViewAll*)[tableView dequeueReusableCellWithIdentifier:kCellOptionFood];
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        _cell.mLabel.text = [dic objectForKey:@"name"];
        return _cell;
    }
    else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellListItem"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellListItem"];
        }
        
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        BOOL blBlock = [dic[@"is_block"] boolValue];
        if (blBlock) {
            cell.textLabel.text = [dic objectForKey:@"name"];
            cell.textLabel.textColor = [UIColor ht_silverColor];
            [cell.imageView setImage:[UIImage imageNamed:@"btn_delete"]];
            [cell.imageView setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            cell.textLabel.text = [dic objectForKey:@"name"];
            cell.textLabel.textColor = [UIColor blackColor];
            [cell.imageView setImage:nil];
        }
        
        cell.textLabel.font = [UIFont systemFontOfSize:14.0];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tableSreachPop) {
        [popupView btnClosePop:popoverController];
        switch (indexPath.row) {
            case 0:{
                 titlePop = @"tag";
                [self getDataCategory:1];
                [pscrollView.pageScrollView reloadData];
                imageback.image = [UIImage imageNamed:@"back"];
                imagenext.image = [UIImage imageNamed:@"next"];
            }
            break;
            case 1:{
                titlePop = @"category";
                [self showDataCategoryList];
                [pscrollView.pageScrollView reloadData];
                imageback.image = nil;
                imagenext.image = nil;
            }
            break;
            case 2:{
                [self.dataMenuList removeAllObjects];
                self.dataMenuList = [self.dataMenu mutableCopy];
                [self.mCollectionMenu reloadData];
            };
            break;
            case 3:{
                [LoadMenuAll checkDataMenu:@"1"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [LoadProcess dismissLoad];
                });
            }
                break;
            default:{
                [self createViewSpecialtyFood];
            }
            break;
        }
    }
    
    if (tableView == self.mTableCategoryFood) {
        [dataSearchTag setObject:dataCategory[indexPath.row] forKey:[@(srocllIndex) stringValue]];
        [self insertDataMenuCategory:(int)tableView.tag];
        if (tableView.tag == 111) { // tag
            [pscrollView.pageScrollView setIndexPage:(int)srocllIndex+1];
        }
        [self.mCollectionMenu reloadData];
    }
//    else if (tableView == self.mTableMenuFood || tableView == tableListShowMenu) {
    else if ( tableView == tableListShowMenu) {
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        BOOL blBlock = [dic[@"is_block"] boolValue];
        if (blBlock) {
            [self showMessage:nil];
        }
        else{
            self.mTableTagFood.backgroundColor = [UIColor whiteColor];
            [popupView btnClosePop:popoverController];
            NSDictionary *menuData = [self.dataMenuList[indexPath.row] mutableCopy];
            [self addOrderAppvoceoption:menuData option:@"" comment:@"" count:1];
            [self.mTableTagFood reloadData];
        }
    }
}

-(void) addOrderAppvoceoption:(NSDictionary *)menuData option:(id)option  comment:(NSString *)comment count:(int)count{
    @try {
        NSDictionary *dataTemp = [self sortDataOrderAdd:menuData option:option comment:comment];
        if ([dataTemp count] != 0) {
            NSNumber *indexP = [NSNumber numberWithInt:[dataTemp[@"index"] intValue]];
            if (indexP != NULL) {
                NSMutableDictionary *tagD = [self.addFoodBill[indexP.intValue] mutableCopy];
                NSString *countD = [@([tagD[@"Count"] intValue] + count) stringValue];
                [tagD setObject:countD forKey:@"Count"];
                
                [self.addFoodBill replaceObjectAtIndex:indexP.intValue withObject:tagD];
                
                NSMutableDictionary *tagT = [self.dataTagAdd[indexP.intValue] mutableCopy];
                NSString *countT = [@([tagT[@"count"] intValue] +count) stringValue];
                [tagT setObject:countT forKey:@"count"];
                
                [self.dataTagAdd replaceObjectAtIndex:indexP.intValue withObject:tagT];
            }
        }
        else{
            NSMutableDictionary *_dataT = [NSMutableDictionary dictionary];
            [_dataT setObject:menuData    forKey:@"data"];
            [_dataT setObject:[@(count) stringValue]  forKey:@"count"];
            [self.dataTagAdd addObject:_dataT];
            
            NSString *price            = menuData[@"price"];
            NSMutableDictionary *_data = [NSMutableDictionary dictionary];
            [_data setObject:option    forKey:@"Option"];
            [_data setObject:[@(count) stringValue]   forKey:@"Count"];
            [_data setObject:price  forKey:@"Price"];
            [_data setObject:comment    forKey:@"Comment"];
            [_data setObject:[NSNumber numberWithInteger:[self.addFoodBill count]] forKey:@"index"];
            [_data setObject:menuData  forKey:@"data"];
            
            [self.addFoodBill addObject:_data];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception addOrderAppvoceoption %@",exception);
    }

//    [self.mTableTagFood reloadData];
}
-(NSDictionary *)sortDataOrderAdd:(NSDictionary *)data  option:(id )option  comment:(NSString *)comment{
    @try {
        NSArray *listAddFB = [self.addFoodBill mutableCopy];
        NSString *keyBill = data[@"price"];
        NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(Price = %@)" andData:listAddFB];
        if ([listUsaage count] != 0) {
//            NSString *name = [[FoodOptionSum class] typeDicTostring:[listUsaage firstObject]];
            listUsaage = [NSDictionary searchDictionaryALL:option andvalue:@"(Option = %@)" andData:listUsaage];
            if ([listUsaage count] != 0) {
                listUsaage = [NSDictionary searchDictionaryALL:comment andvalue:@"(Comment = %@)" andData:listUsaage];
                if ([listUsaage count] != 0) {
                    NSDictionary *dataFB = [NSDictionary dictionaryWithObject:listUsaage[0]];
                    keyBill = data[@"id"];
                    listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(id = %@)" andData:[listUsaage valueForKey:@"data"]];
                    if ([listUsaage count] != 0) {
                        return dataFB;
                    }
                }
            }
        }
        return nil;
    }
    @catch (NSException *exception) {
        DDLogError(@"exception sortDataOrderAdd %@",exception);
    }
}
- (IBAction)showMessage:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                      message:AMLocalizedString(@"เมนูนี้ถูกยกเลิก !", nil)
                                                     delegate:nil
                                            cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                            otherButtonTitles:nil];
    
    [message show];
}
- (void) insertDataMenuCategory:(int)tag {
    NSMutableArray *listTemp_ = [NSMutableArray arrayWithArray:self.dataMenu];
//    for (NSString *str in self.dataMenu) {
//        [listTemp_ addObject:[self.dataMenu valueForKey:str]];
//    }

    NSArray *keyList = [dataSearchTag allKeys];
    for (NSString *key in keyList) {
        NSDictionary *dic = dataSearchTag[key];
        [self.dataMenuList removeAllObjects];
        NSString *type = (tag == 111)?[@"tag" stringByAppendingString:dic[@"type"]]:@"category_id";
        // [@"tag" stringByAppendingString:dic[@"type"]] ค้นหาแบบ tag
        //  @"category_id"; // ค้นหาแบบ category
        
        for (NSDictionary *dicMenu in listTemp_) {
            if ([dicMenu[type] isEqual:dic[@"id"]]) {
                [self.dataMenuList addObject:dicMenu];
            }
        }
    
        NSArray *dataMenuList2 = [self sortDataOrder:self.dataMenuList andKey:@"order"];
        
        listTemp_ = [dataMenuList2 copy];
    }
}
- (void)clickTitleTag:(UIButton *)sender
{
    CellFoodItemView *cell = (CellFoodItemView *)[[sender superview]superview];
    NSIndexPath *indexPath = [self.mTableTagFood indexPathForCell:cell];
    NSLog(@"clickTitleTag   : %d",(int)indexPath.row);
}
- (IBAction)showClickDeleteTag:(id)sender {
    btnAlertView = (UIButton *)sender;
    [btnAlertView setTag:88];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                      message:AMLocalizedString(@"ต้องการลบรายการนี้", nil)
                                                     delegate:self
                                            cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                            otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
    
    [message show];
}

-(void)clickDeleteTag:(UIButton *)sender{
    
    CellFoodItemView *buttonCell = (CellFoodItemView*)[sender superview];
    NSIndexPath *indexPath = [self.mTableTagFood indexPathForCell:buttonCell];
    if (indexPath == nil) {
        buttonCell = (CellFoodItemView*)[[sender superview] superview];
        indexPath = [self.mTableTagFood indexPathForCell:buttonCell];
        if (indexPath == nil) {
            buttonCell = (CellFoodItemView*)[[[sender superview] superview]superview];
            indexPath = [self.mTableTagFood indexPathForCell:buttonCell];
        }
    }
    
    [self.addFoodBill removeObjectAtIndex:indexPath.row];
    self.deleteTag = [NSMutableDictionary dictionary];
    [self.deleteTag setObject:indexPath forKey:@"index"];
    [self.deleteTag setObject:buttonCell.mBtnTttleTag.titleLabel.text forKey:@"title"];
    [self.dataTagAdd removeObjectAtIndex:indexPath.row];
    [self.mTableTagFood reloadData];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
- (NSInteger)numberOfPageInPageScrollView:(OTPageScrollView*)pageScrollView{
    return [_dataPageScrollView[titlePop] count];
}

- (UIView*)pageScrollView:(OTPageScrollView*)pageScrollView viewForRowAtIndex:(int)index{
    UIView *cell = [[UIView alloc] initWithFrame:CGRectMake(0,0,pageScrollView.frame.size.width-10,pageScrollView.frame.size.height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,cell.frame.size.width, cell.frame.size.height)];
   
    label.text = _dataPageScrollView[titlePop][index];
     label.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:label];
    return cell;
}

- (CGSize)sizeCellForPageScrollView:(OTPageScrollView*)pageScrollView
{
    return CGSizeMake(pageScrollView.frame.size.width-10, pageScrollView.frame.size.height);
}

- (void)pageScrollView:(OTPageScrollView *)pageScrollView didTapPageAtIndex:(NSInteger)index{
    [self resetDataInsert:index];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (![scrollView isEqual:self.mTableCategoryFood] && ![scrollView isEqual:self.mCollectionMenu] && ![scrollView isEqual:self.mTableTagFood]) {
//    if (![scrollView isEqual:self.mTableCategoryFood] && ![scrollView isEqual:self.mTableTagFood]) {
        NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
        [self resetDataInsert:index];
    }
}
-(void) resetDataInsert:(NSInteger)index{
    srocllIndex = index;
    if (index != 0) {
        if ([dataSearchTag count] != 0 && [dataSearchTag count] > index) {
            [dataSearchTag removeObjectForKey:[@(index) stringValue]];
            srocllIndex = index;
            [self insertDataMenuCategory:111];
//            [self.mTableMenuFood reloadData];
            [self.mCollectionMenu reloadData];
        }
        [self getDataCategory:index+1];
    }
    else{
        [dataSearchTag removeAllObjects];
        [self.dataMenuList removeAllObjects];
        srocllIndex = index;
//        for (NSString *str in self.dataMenu) {
//            [self.dataMenuList addObject:[self.dataMenu valueForKey:str]];
//        }
        self.dataMenuList = [self.dataMenu mutableCopy];
//        [self.mTableMenuFood reloadData];
        [self.mCollectionMenu reloadData];
        [self getDataCategory:index+1];
    }
}
-(void) getDataCategory:(NSInteger)integer{
    dataCategory  = [NSMutableArray array];
    //NSMutableArray *dataCategory2  = [NSMutableArray array];
    NSArray *dataMenuListTemp = [NSArray arrayWithArray:self.dataMenuList];
    for (NSString *key in dataTagFoodList) {
        NSString *num = (NSString *)dataTagFoodList[key][@"type"];
        NSString *keyBill = dataTagFoodList[key][@"id"];
    
        // ค้นหา tag
        NSArray *blCheckTag = [NSDictionary searchDictionaryALL:keyBill
                                                       andvalue:[self tagValue:[num intValue]]
                                                        andData:dataMenuListTemp];
        if ([num isEqual:[@(integer) stringValue]] && [blCheckTag count] != 0) {
            [dataCategory addObject:dataTagFoodList[key]];
        }
    }
    [self.mTableCategoryFood setTag:111];
    [self.mTableCategoryFood reloadData];
}
-(NSString *)tagValue:(int)num{
    switch (num) {
        case 1:
            return @"(tag1 = %@)";
            break;
        case 2:
            return @"(tag2 = %@)";
            break;
        case 3:
            return @"(tag3 = %@)";
            break;
        case 4:
            return @"(tag4 = %@)";
            break;
            
        default:
            return @"";
            break;
    }
}
-(void) showDataCategoryList{
    [dataCategory removeAllObjects];

    dataCategory = [NSMutableArray arrayWithArray:dataCategoryList];
    dataCategory = [self sortDataOrder:dataCategory andKey:@"order"];
    [self.mTableCategoryFood setTag:222];
    [self.mTableCategoryFood reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (searchBar.text.length > 0) {
        self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenuList];
        searchBar.text = nil;
        [searchBar resignFirstResponder];
    }       
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSMutableArray *dataMenuListTmp = [NSMutableArray arrayWithArray:self.dataMenuList];
    [self.dataMenuList removeAllObjects];
    NSMutableArray *listData = [NSMutableArray arrayWithArray:self.dataMenu];

    for (NSDictionary *data in listData) {
        searchText  = [searchText lowercaseString];
        NSString *menu = data[@"index_id"];
        
        NSRange aRange = [menu rangeOfString:searchText];
        if (aRange.location == NSNotFound) {
            NSLog(@"string not found");
        } else {
            NSLog(@"string was at index %lu ",(unsigned long)aRange.location);
            [self.dataMenuList addObject:data];
        }
    }
    if ([self.dataMenuList count] != 0)
      [self.mCollectionMenu reloadData];
    else
        self.dataMenuList = dataMenuListTmp;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBarBegin = NO;
    [self.mLayoutTopTableTagFood setConstant:-10];
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:NO];

    [self showDataCategoryList];
    [self.mCollectionMenu reloadData];
    
    return YES;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    for (UIView *subview in self.searchField.subviews){
        for (UIView *subSubview in subview.subviews){
            if ([subSubview conformsToProtocol:@protocol(UITextInputTraits)]){
                UITextField *textField = (UITextField *)subSubview;
                [textField setKeyboardAppearance: UIKeyboardAppearanceAlert];
                textField.returnKeyType = UIReturnKeyDone;
                textField.keyboardType = UIKeyboardTypeNumberPad;
                break;
            }
        }
    }
    searchBarBegin = YES;
    CGFloat sizeTot = 120;
    [self.mLayoutTopTableTagFood setConstant:-sizeTot];
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:NO];
    
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenu];
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}
- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope {
    searchBar.keyboardType = UIKeyboardTypeNumberPad;
    [searchBar resignFirstResponder];
    [searchBar becomeFirstResponder];
    
}
- (IBAction)btnPopMenu:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
    viewControllerShowMenu = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ListOptionFood"];
    viewControllerShowMenu.title = AMLocalizedString(@"ตัวเลือก", nil);
    tableListShowMenu =  viewControllerShowMenu.tableView;
    tableListShowMenu.delegate = self;
    tableListShowMenu.dataSource = self;
    
    viewControllerShowMenu.preferredContentSize = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-50);
    viewControllerShowMenu.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewControllerShowMenu];
    
    UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(viewControllerShowMenu.view.frame.size.width - 50,3,30, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [contentViewController.view addSubview:btnOpiton];

    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}
-(void)createViewSpecialtyFood{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
    UIViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:kSpecialtyFood];
    [[self navigationController] pushViewController:viewController_ animated:NO];
}
- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        isCheckOrderDB = NO;
        CGPoint p = [gestureRecognizer locationInView:self.mCollectionMenu];
        NSIndexPath *indexPath = [self.mCollectionMenu indexPathForItemAtPoint:p];
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        BOOL blBlock = [dic[@"is_block"] boolValue];
        if (blBlock) {
            return;
        }

        TableOrderFoodViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableOrderFood"];
        
        viewController_.dataMenuList = dic;
        viewController_.strTitleFood = dic[@"name"];
        viewController_.priceFood    = dic[@"price"];
        [viewController_ setFoodItemsOrderView:self];
        [[self navigationController] pushViewController:viewController_ animated:NO];
    }
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBackHome:(id)sender {
    UITableViewController *viewController = [[UITableViewController alloc] init];
    viewController.title = AMLocalizedString(@"รายการ", nil);
    tableSreachPop =  viewController.tableView;
    tableSreachPop.delegate = self;
    tableSreachPop.dataSource = self;
    
    viewController.preferredContentSize = CGSizeMake(200,300);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromBarButtonItem:sender
                              permittedArrowDirections:WYPopoverArrowDirectionAny animated:NO];
}
-(IBAction)btnClosePop:(id)sender   {
    [self.searchField resignFirstResponder];
    [popupView btnClosePop:popoverController];
}
- (IBAction)barButtonItemAction:(id)sender {
    if ([self.addFoodBill count] != 0) {
        isCheckSubmit = YES;
        if (self.tableSingleBillViewController != nil) {
            [self.tableSingleBillViewController.tableDataBillView addOrder:self.addFoodBill andPro:nil];
            [self.navigationController popToViewController:self.tableSingleBillViewController animated:NO];
        }
        else{
            [self.tableSubBillViewController.tableDataBillView addOrder:self.addFoodBill andPro:nil];
            [self.navigationController popToViewController:self.tableSubBillViewController animated:NO];
        }
        if ([self.tableSubBillViewController isKindOfClass:[TableSubBillViewController class]]) {
            [TableSubBillViewController loadDataOpenTableSubBill:self.tableSubBillViewController.detailTable[@"id"]];
        }
        else{
            [TableSingleBillViewController loadDataOpenTableSing:self.tableSingleBillViewController.detailTable[@"id"]];
        }
    }
    else{
        [loadProgress.loadView showWithStatusTitle];
    }
}

#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {

    if ([title  isEqual:AMLocalizedString(@"ใช่", nil)]) {
        NSIndexPath *indexPath = self.deleteTag[@"index"];
        [self.mTableTagFood beginUpdates];
        
        if ([self.dataTagAdd count] == 0) {
            self.dataMenuList  = [self.dataMenu mutableCopy];
        }
        else{
            [self.dataTagAdd removeObjectAtIndex:indexPath.row];
        }
        [self insertDataMenuCategory:111];
        [self.mTableTagFood deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
        [self.mTableTagFood endUpdates];
        [self.mTableTagFood reloadData];
        [self.mCollectionMenu reloadData];
    }
}
-(void)setDataMenu{
//    NSMutableArray *keyList = [NSMutableArray array];
//    [keyList addObjectsFromArray:self.dataMenu];
//    for (NSString *str in self.dataMenu) {
//        [self.dataMenuList addObject:[self.dataMenu valueForKey:str]];
//    }
    self.dataMenuList =[self.dataMenu mutableCopy];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqual:@"orderFood"]) {
        TableOrderFoodViewController *viewController_ = (TableOrderFoodViewController *)[segue destinationViewController];
        CGPoint p = [sender locationInView:self.mCollectionMenu];
        NSIndexPath *indexPath = [self.mCollectionMenu indexPathForItemAtPoint:p];
        
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        viewController_.strTitleFood = [dic objectForKey:@"name"];
        viewController_.priceFood = [dic objectForKey:@"price"];
        [viewController_ setFoodItemsOrderView:self];
    }
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataMenuList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellDelete" forIndexPath:indexPath];
    NSDictionary *dic = self.dataMenuList[indexPath.row];
    BOOL blBlock = [dic[@"is_block"] boolValue];
    [cell.mCountBill setText:[NSString checkNull:dic[@"index_id"]]];
    [cell.mNameTable setText:[NSString checkNull:dic[@"name"]]];
   
    if (blBlock) {
        cell.mCountBill.textColor = [UIColor ht_silverColor];
        cell.mNameTable.textColor = [UIColor ht_silverColor];
        [cell.mImageCheck setImage:[UIImage imageNamed:@"btn_delete"]];
        [cell.mImageCheck setContentMode:UIViewContentModeScaleAspectFit];
    }
    else{
        cell.mCountBill.textColor = [UIColor colorWithWhite:0.200 alpha:1.000];
        cell.mNameTable.textColor = [UIColor colorWithWhite:0.200 alpha:1.000];
        [cell.mImageCheck setImage:nil];
    }

    [self setLineView:cell];
    [cell.mViewSub setBackgroundColor:[UIColor colorWithWhite:0.902 alpha:1.000]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.dataMenuList[indexPath.row];
    BOOL blBlock = [dic[@"is_block"] boolValue];
    if (blBlock) {
        [self showMessage:nil];
    }
    else{
        self.mTableTagFood.backgroundColor = [UIColor whiteColor];
        [popupView btnClosePop:popoverController];
//        NSDictionary *menuData = [self.dataMenuList[indexPath.row] mutableCopy];
//        [self addOrderAppvoceoption:menuData option:@"" comment:@"" count:1];
       
        
        menuDataSeclect = [NSDictionary dictionaryWithObject:self.dataMenuList[indexPath.row]];
        NSString *textOption = [NSString checkNull:menuDataSeclect[@"cheer_option"]];
        cheerOption  = [NSDictionary dictionaryWithString:textOption];
        BOOL status = false;
        if ([cheerOption count] != 0 && [self.printerDB.isMenuSug isEqualToString:@"1"]) {
            status = [cheerOption[@"is_enabled"] boolValue];
            if (status) {
                [self creteAlerViewMenuOption:cheerOption];
            }
        }
        
        if (!status){
            [self addDataOrderAlerView:menuDataSeclect option:@""];
        } 
    }
}

//#pragma mark - convert color
//- (UIColor *)colorWithHexString:(NSString *)str {
//    const char *cStr = [str cStringUsingEncoding:NSASCIIStringEncoding];
//    long x = strtol(cStr+1, NULL, 16);
//    return [self colorWithHex:(UInt32)x];
//}
//- (UIColor *)colorWithHex:(UInt32)col {
//    unsigned char r, g, b;
//    b = col & 0xFF;
//    g = (col >> 8) & 0xFF;
//    r = (col >> 16) & 0xFF;
//    return [UIColor colorWithRed:(float)r/255.0f green:(float)g/255.0f blue:(float)b/255.0f alpha:1];
//}

#pragma mark - UIInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    UIInterfaceOrientation ori = [[UIApplication sharedApplication] statusBarOrientation];
    if (ori == UIInterfaceOrientationPortrait) {
        self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    }
    else{
        self.layout.numberOfItemsPerLine = [Check checkDevice]?3:7;
    }
    [self initNavigation];
}

- (IBAction)btnTagKeybroad:(UITapGestureRecognizer *)gestureRecognizer{
    NSLog(@"55555");
    [self.searchField resignFirstResponder];
}
-(void)setLineView:(id)sender{
    if ([sender isMemberOfClass:[TableCollectionViewCell class]]) {
        TableCollectionViewCell *cell = (TableCollectionViewCell *)sender;
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.clipsToBounds = NO;
        cell.layer.masksToBounds = YES;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    }
    else{
        UITableViewCell *cell = (UITableViewCell *)sender;
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.clipsToBounds = NO;
        cell.layer.masksToBounds = YES;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    }
}
-(NSMutableArray *) sortDataOrder:(NSMutableArray *)data1 andKey:(NSString *)key{
   [data1 sortUsingComparator:
     ^(id obj1, id obj2)
     {
         NSInteger value1 = [[obj1 objectForKey:key] intValue];
         NSInteger value2 = [[obj2 objectForKey:key]intValue];
         if (value1 > value2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         if (value1 < value2){
             return (NSComparisonResult)NSOrderedAscending;
         }
         return (NSComparisonResult)NSOrderedSame;
     }];
    
    return  [NSMutableArray arrayWithArray:[data1 mutableCopy]];
}

-(void) creteAlerViewMenuOption:(NSDictionary *)data{
    
    //    1   upsize
    //    2   upsale  --> id ของ menu
    //    3   promotion
    
    int type = [[NSString checkNull:data[@"type"]] intValue];
    NSDictionary *dataDetail  = [NSDictionary dictionaryWithObject:data[@"data"]];
    
//    NSString *tile = AMLocalizedString(@"คุณต้องการเพิ่ม", nil);
    NSString *tile = [[FoodOptionSum class] typeDicTostring:data[@"title"]];
    NSString *alertMessage ;
    switch (type) {
        case 1:{
            NSDictionary *nameD =  [NSDictionary dictionaryWithObject:dataDetail[@"name"]]; 
            NSString *price =  [NSString checkNull:dataDetail[@"price"]]; 
            NSString *name = [[FoodOptionSum class] typeDicTostring:nameD];
            alertMessage = [NSString stringWithFormat:@" %@ %@ %@ %@ %@",tile,name,AMLocalizedString(@"ราคา", nil),price,AMLocalizedString(@"ใช่หรือไม่", nil)];
        }            
            break;
        case 2:{
            NSString *idMenu =  [NSString checkNull:dataDetail[@"id"]]; 
            menuUpsale = (NSDictionary*)[NSDictionary searchDictionary:idMenu andvalue:@"(id == %@)" andData:self.dataMenu];
            NSString *name =  [NSString checkNull:menuUpsale[@"name"]]; 
            alertMessage = [NSString stringWithFormat:@" %@ %@ %@",tile,name,AMLocalizedString(@"ใช่หรือไม่", nil)];
        }
            break;
        case 3:{            
            NSDictionary *promotion = [NSDictionary dictionaryWithObject:self.tableSingleBillViewController.dataListSingBill[@"promotion"][@"promotion_info"]];
            NSString *idMenu =  [NSString checkNull:dataDetail[@"id"]]; 
            NSString *keyBill = [NSString stringWithFormat:@"id_%@",idMenu];
            menuUpPromotion = [NSDictionary dictionaryWithObject:promotion[keyBill]];
            
            if ([menuUpPromotion count] != 0) {
                NSString *name =  [NSString checkNull:menuUpPromotion[@"name"]]; 
                alertMessage = [NSString stringWithFormat:@" %@ %@ %@",tile,name,AMLocalizedString(@"ใช่หรือไม่", nil)];
            } 
            
        }
            break;
            
        default:
            break;
    }
    
    NSString *alertTitle = [NSString stringWithFormat:@" %@ ",AMLocalizedString(@"แนะนำ", nil)];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertTitle
                                                      message:alertMessage
                                                     delegate:self
                                            cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                            otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
    [message setTag:999];    
    [message show];
}
-(void) actionAlerViewMenu{
    
    //    1   upsize
    //    2   upsale  --> id ของ menu
    //    3   promotion
    
    int type = [[NSString checkNull:cheerOption[@"type"]] intValue];    
    switch (type) {
        case 1:{
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:menuDataSeclect];
            NSDictionary *dataDetail = [NSDictionary dictionaryWithObject:cheerOption[@"data"]];
            int price1 = [[NSString checkNull:dataDetail[@"price"]] intValue];
            int price2 = [[NSString checkNull:dic[@"price"]] intValue];            
            price2 += price1;
            
            [dic setObject:[@(price2) stringValue] forKey:@"price"];
        
            [self addDataOrderAlerView:dic option:@[dataDetail]];
        }            
            break;
        case 2:{
            [self addDataOrderAlerView:menuDataSeclect option:@""];
            [self addDataOrderAlerView:menuUpsale option:@""];
        }
            break;
        case 3:{            
            [self sendPromotion];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ((buttonIndex == 1 && btnAlertView.tag == 88)) {
        [self clickDeleteTag:btnAlertView];
    }
    else if (buttonIndex == 1 && alertView.tag == 999 ){
        [self actionAlerViewMenu];
    }
    else if (buttonIndex == 0 && alertView.tag == 999 ){
        [self addDataOrderAlerView:menuDataSeclect option:@""];
    }
    
}
-(void) addDataOrderAlerView:(NSDictionary *)data option:(id)list{
    [self addOrderAppvoceoption:data option:list comment:@"" count:1];
    [self.mTableTagFood reloadData];
    if (searchBarBegin) {
        [self showDataCategoryList];
        [self.searchField resignFirstResponder];
    }
}

-(void)sendPromotion{ 
    [self initOrderDB];
    if (self.tableSingleBillViewController != nil) {
        NSArray *arrList = [NSArray arrayWithObjects:self.tableSingleBillViewController.tableDataBillView.promotionPoolID,menuUpPromotion,@"",nil];
        [self.tableSingleBillViewController.tableDataBillView addPromotion:arrList andDataOrder:nil];
        [self.navigationController popToViewController:self.tableSingleBillViewController animated:NO];
    }
    else{
        NSArray *arrList = [NSArray arrayWithObjects:self.tableSubBillViewController.tableDataBillView.promotionPoolID,menuUpPromotion,@"",nil];
        [self.tableSubBillViewController.tableDataBillView addPromotion:arrList andDataOrder:nil];
        [self.navigationController popToViewController:self.tableSubBillViewController animated:NO];
        
    }
}
@end
