//
//  FoodSpecialtyViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/6/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIStoryboard+Main.h"

@interface FoodSpecialtyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mViewBackground;
@property (weak, nonatomic) IBOutlet UITextField *mNameFood;
@property (weak, nonatomic) IBOutlet UITextView *mCommentFood;
@property (weak, nonatomic) IBOutlet UITextField *mPriceFood;
@property (weak, nonatomic) IBOutlet UITextField *mCountFood;
@property (weak, nonatomic) IBOutlet UITextField *mKitchen;

@property (nonatomic,strong) NSDictionary *dataKitchen;

- (IBAction)btnReply:(id)sender;
- (IBAction)btnClosePop:(id)sender;
@end
