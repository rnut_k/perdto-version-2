//
//  TableDataBillViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableDataBillViewController.h"
#import "CellTableViewAll.h"
#import "TablePromotiomViewController.h"
#import "ListItemPromotiomViewController.h"
#import "DetailOrderPopViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

#import "Text.h"
#import "Constants.h"
#import "Time.h"
#import "WYPopoverController.h"
#import "PopupView.h"
#import "AFNetworking.h"
#import "Member.h"
#import "GlobalBill.h"
#import "NSString+GetString.h"
#import "NSDictionary+DictionaryWithString.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "LoadMenuAll.h"
#import "Check.h"
#import "LoadProcess.h"
#import "FoodOptionSum.h"


static const NSArray *listCellTable;
@interface TableDataBillViewController()<WYPopoverControllerDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UIAlertViewDelegate>{
    WYPopoverController *popoverController;
    LoadProcess *loadProgress;
    GlobalBill *globalBill;
    NSInteger sectionTable;
    NSMutableArray *addFoodBill;   //  เก็บข้อมูลในส่วนที่ยัง ไม่ commit แล้ว
    NSNotification *note;

// Promotion
    NSMutableArray *promotionList;
    
//  imageSingSubBill
    NSIndexPath *indexPathimageSingSubBill;
    NSIndexPath *indexPathPop;
    
    BOOL scrollToLast;
    int indexSectionAler;
    
    NSString *typeEditCashier;
    
    BOOL addOrderNotApp;
    BOOL blAddPrit ;
}

@property (nonatomic,strong) Member *member;
@property (nonatomic, strong) NSString* fileName;
@end

static   NSString *relateOrderID;
static NSString *pahtTh_url;
static PopupView *popupView;
static TableDataBillViewController *detailOrderSelf;

@implementation TableDataBillViewController
+ (void)initialize
{
    // do not run for derived classes
    if (self != [TableDataBillViewController class])
        return;
    
    listCellTable = @[kCellBillTop,
                        kCellBill,
                        kCellBillBotton,
                        kCellDrink,
                        kCellDrinkSubmit,
                        kCellTotal,
                        kCellBillCommit,
                        kCellDrinkBill,
                        kCellPromotion,
                        kCellSignature,
                        kCellTileSubBill,
                        kCellTotalALL,
                        kCellTotalVat,
                        kCellTotalSubbill,
                        kCellOptionFood,
                        kCellSumTotalBill,
                        kCellProTotalBill];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    loadProgress = [LoadProcess sharedInstance];
    
    detailOrderSelf = self;
    pahtTh_url = @"";
    self.sectionBill = [NSMutableDictionary dictionary];
    self.dataImagePrint = [NSMutableDictionary dictionary];
    self.dataImagePrintList = [NSMutableArray array];
    
    self.member = [Member getInstance];
    addFoodBill = [NSMutableArray array];
    promotionList = [NSMutableArray array];
    self.sectionDataIndex  = [NSMutableArray array];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self scrollViewRefreshControl];
    [self setNSNotificationCenter];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSIndexPath *scrollIndexPath = [NSIndexPath indexPathForRow:0 inSection:sectionTable];
    [[self mTableViewBill] scrollToRowAtIndexPath:scrollIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}
-(void)setNSNotificationCenter{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnOK:)
                                                 name:@"checkbtnOK"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnClose:)
                                                 name:@"checkbtnClose"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(btnTagSignature:)
                                                 name:@"checkbtnTagSignature"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkStopDrink:)
                                                 name:@"checkStopDrink"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkbtnEditOrder:)
                                                 name:@"checkbtnEditOrder"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notiReloadData:)
                                                 name:@"reloadDataOrder"
                                               object:nil];
}
-(void)scrollViewRefreshControl{
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithWhite:0.800 alpha:1.000];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(reloadData)
                  forControlEvents:UIControlEventValueChanged];

}
- (void)reloadData
{
    // End the refreshing
    if (self.refreshControl) {
        [self.tableSingleBillViewController setLoadDataTable];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"อัพเดตรายการ ", nil),[formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
}
-(void)notiReloadData:(id)sender{
    NSDictionary *dataInvoice = [[sender userInfo] copy];
    if ([dataInvoice count] != 0) {
        NSString *idTable = [dataInvoice[@"table_id"] copy];
        if ([self.tableID isEqualToString:idTable]) {
            [self reloadData];
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    sectionTable = [self.sectionDataIndex count];
    return [self.sectionDataIndex count]+1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numberOfRows = 0;
    if ([self.sectionDataIndex count] == 0 || [self.sectionDataIndex count] == section) {
        numberOfRows = 1;
        return numberOfRows;
    }
    NSMutableDictionary *dataSectionTmp = self.sectionDataIndex[section];
    if ([dataSectionTmp count] != 0 ) {
        NSMutableArray *data = dataSectionTmp[@"PRO"];
        if ([data count] != 0) {
            promotionList = [data copy];
            return [data count];
        }
        NSMutableDictionary *dataAddFoodBill = dataSectionTmp[@"ORDER"];
        if ([dataAddFoodBill count] != 0) {
            data         = dataAddFoodBill[@"data"];
            numberOfRows = [data count];
            
            if (self.isPopupCashier || (self.isCashier && self.useSubBill)) {
                numberOfRows -=1 ;
            }
            return  numberOfRows;
        }
        NSMutableDictionary *pendingPrders = dataSectionTmp[@"PENDING"];
        if ([pendingPrders count] != 0) {
            numberOfRows = ([pendingPrders[@"commit"] isEqual:@"0"] || self.isSignAll)?1:0;
            NSMutableArray *pendingData = [[NSMutableArray alloc]init];
            for(NSDictionary *d in pendingPrders[@"data"]){
                if([d[@"dataorder"][@"menu_type"]intValue] != 9){
                    [pendingData addObject:d];
                }
            }
            data  = pendingPrders[@"data"];     //pendingData;
            numberOfRows +=  [data  count];
            return  numberOfRows;
        }
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        
        NSString *cellIden;
        if (sectionTable == indexPath.section){  // type section total - 3
            if (self.isSignAll) {
                cellIden = kCellSignature;
            }
            else if([self.selfSuper isKindOfClass:[TableSingleBillViewController class]]){
                if (self.isCashier || self.isPopupCashier) {
                    cellIden = kCellTotalALL;
                }
                else{
                    cellIden = kCellTotal;
                }
            }
        }
        else if (self.isCashier || self.isPopupCashier){
            NSInteger section = indexPath.section;
            NSMutableDictionary *dataSectionTmp = self.sectionDataIndex[section];
            NSArray *dataAddFoodBill = [dataSectionTmp[@"ORDER"][@"data"] copy];
            NSString *idIndex = [NSString checkNull:dataAddFoodBill[indexPath.row][@"index"]];
            
            if ([idIndex isEqualToString:@"totalBill"]) {
                cellIden = kCellTotalVat;
            }
            else if ([idIndex isEqualToString:@"TotalSubbill"]) {
                cellIden = kCellTotalSubbill;
            }
            else if ([idIndex isEqualToString:@"dataSubBill"]) {
                cellIden = kCellSumTotalBill;
            }
            else if ([idIndex isEqualToString:@"dataProPassiveBill"] ||
                     [idIndex isEqualToString:@"serviceBill"] ||
                     [idIndex isEqualToString:@"couponBill"]) {
                cellIden = kCellProTotalBill;
            }
        }
        
        CGFloat height      = [self getSizeHightTable:cellIden];
        tableView.rowHeight = height;
        return height;
    }
    @catch (NSException *exception) {
        DDLogError(@"Caught exception heightForRowAtIndexPath : %@", exception);
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
// type section submit      - 1
// type section add order   - 2
// type section total       - 3
    
    NSString *cellIden  =  @"Cell";
    CellTableViewAll *cell =  [tableView dequeueReusableCellWithIdentifier:cellIden];
    if (cell == nil) {
        cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIden];
    }
    @try {
        if (sectionTable == indexPath.section){   // type section total     - 3
            if (self.isSignAll) {
                cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:kCellSignature];
                
                [cell.mImageBackgroud setUserInteractionEnabled:YES];
                cell.layer.cornerRadius = 10.0f;
                cell.layer.borderWidth = 1.0f;
                cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
                cell.clipsToBounds = NO;
                cell.backgroundColor = [UIColor whiteColor];
                cell.layer.masksToBounds = YES;
                
                UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageSingSubBill:)];
                [singleTap setNumberOfTapsRequired:1];
                [cell.mImageBackgroud addGestureRecognizer:singleTap];
                cellIden = kCellSignature;
                indexPathimageSingSubBill = indexPath;
            }
            else if([self.selfSuper isKindOfClass:[TableSingleBillViewController class]]){
                NSDictionary *cellList = nil;
                if (self.isPopupCashier || self.useSubBill) {
                    cellIden = kCellTotalVat;
                    NSMutableDictionary *dataSectionTmp = nil;
                    if ([self.sectionDataIndex count] != 0) {
                        dataSectionTmp = self.sectionDataIndex[1];
                    }
                    else{
                        dataSectionTmp = [self.sectionDataIndex  firstObject];
                    }
                    NSArray *dataAddFoodBill = [dataSectionTmp[@"ORDER"][@"data"] copy];
                    cellList = [dataAddFoodBill lastObject];
                }
                else{
                    if (self.isCashier || self.isPopupCashier) {
                        cellIden = kCellTotalALL;
                    }
                    else{
                        cellIden = kCellTotal;
                    }
                    
                }
                
                cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                cellIden = [self cellTotal:cell and:tableView andDataTotal:cellList];
            }
        }
        else {
            NSString *typeDrink;
            NSDictionary *cellList;
            NSString *commit = @"99";
            if ([promotionList count] != 0 && indexPath.section == 0) {
                cellIden = kCellPromotion;
            }
            else{
                @try{
                    NSLog(@" indexPath %ld",(long)indexPath.row);
                    
                    NSArray *arrKey = [self.sectionBill allKeys];
                    NSMutableDictionary *dataAddFoodBill = self.sectionBill[arrKey[indexPath.section-1]];
                    addFoodBill = dataAddFoodBill[@"data"];
                    commit = dataAddFoodBill[@"commit"];
                    
                    if ([commit isEqual:@"1"]) {
                        cellIden = kCellBillCommit;
                        cellList = addFoodBill[indexPath.row];
                        
                        NSNumber *blnum     = cellList[@"subbill"];
                        NSNumber *blnumbill = cellList[@"bill"];
                        NSNumber *blindex   = cellList[@"bill_id"];
                    
                        NSString *menuID   = cellList[@"dataorder"][@"menu_id"];
                        NSString *giftType = cellList[@"data"][@"gift_type"];
                        NSString *type     = [NSString checkNull:cellList[@"dataorder"][@"type"]];
                        NSString *protypeLv1 = [NSString checkNull:cellList[@"data"][@"protype_lv1"]];
                        
                        NSLog(@" type %@",type);
                        
                        if ([menuID isEqual:@"0"] && [giftType isEqual:@"3"] ) {    // โปรโมชั่น ที่ยังไม่ได้กดปุ่ม
                            cellIden = kCellPromotion;
                        }
                        else if (blnum || blnumbill || blindex) {
                            cellIden = kCellTileSubBill;
                        }
                        else if (([type intValue] == 2) && ([protypeLv1 isEqualToString:@"2"] && [self checkGiftType:giftType])) {
                              cellIden = kCellTileSubBill;
                        }
                        else{
                            typeDrink = ([cellList count] == 4 )?cellList[@"TypeDrink"]:
                            [self getTypeDrink:cellList[@"dataorder"][@"drinkgirl_type"]];
                        }
                    }
                    else{
                        if([addFoodBill count] == indexPath.row){
                            cellIden = kCellBillBotton;
                        }
                        else{
                            NSString *menuID = addFoodBill[indexPath.row][@"dataorder"][@"menu_id"];
                            NSString *giftType = addFoodBill[indexPath.row][@"data"][@"gift_type"];
                            if ([menuID isEqual:@"0"] && [giftType isEqual:@"3"]) {         // โปรโมชั่น ที่ยังไม่ได้กดปุ่ม
                                cellIden = kCellPromotion;
                            }
                            else if (addFoodBill.firstObject == addFoodBill[indexPath.row]) {
                                cellList = addFoodBill[indexPath.row];
                                typeDrink = ([cellList count] == 4 )?cellList[@"TypeDrink"]:
                                [self getTypeDrink:cellList[@"dataorder"][@"drinkgirl_type"]];
                                if (typeDrink == nil) {
                                    cellIden = kCellBillTop;
                                }
                            }
                            else{
                                cellIden = kCellBill;
                                cellList = addFoodBill[indexPath.row];
                                typeDrink = ([cellList count] == 4 )?cellList[@"TypeDrink"]:
                                [self getTypeDrink:cellList[@"dataorder"][@"drinkgirl_type"]];
                            }
                        }
                    }
                }
                @catch (NSException *exception) {
                    DDLogError(@"3 .Caught exception cellForRowAtIndexPath : %@", exception);
                }
            }
            if ([cellIden isEqual:kCellTileSubBill]) {
                
                NSString *indexBill = [NSString checkNull:cellList[@"index"]];
                if ([indexBill isEqual:@"titleSubBill"]) {
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    NSNumber *blnum = [NSNumber numberWithInt:[cellList[@"bill"] intValue]+1];
                    
                    cell.mLabel.text = [NSString stringWithFormat:@" %@: %@ ",AMLocalizedString(@"บิลหลักที่ ", nil),[blnum stringValue]];
                    
                }
                else if ([indexBill isEqual:@"totalBill"]) {
                    if (self.isCashier) {
                        cellIden = kCellTotalVat;
                    }
                    else{
                        cellIden = kCellTotal;
                    }
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    cellIden = [self cellTotal:cell and:tableView andDataTotal:cellList];
                }
                else if ([indexBill isEqual:@"TotalSubbill"]) {
                    if (self.isCashier) {
                        cellIden = kCellTotalSubbill;
                    }
                    else{
                        cellIden = kCellTotal;
                    }
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    cellIden = [self cellTotal:cell and:tableView andDataTotal:cellList];
                }
                else if ([indexBill isEqualToString:@"dataSubBill"]) {
                    NSString *total = [NSString checkNull:cellList[@"total"]];
                    cellIden = kCellSumTotalBill;
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    cell.mTotalFood.text = [NSString conventCurrency:total];
                }
                else if ([indexBill isEqualToString:@"dataProPassiveBill"] ||   // service and coupon bill total
                         [indexBill isEqualToString:@"serviceBill"] ||
                         [indexBill isEqualToString:@"couponBill"]) {
                    cellIden = kCellProTotalBill;
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    
                    NSString *nameFood;
                    NSString *total;
                    if ([indexBill isEqualToString:@"serviceBill"]) {
                        float serPer   = [cellList[@"per"] floatValue];
                        float totalSer = [cellList[@"total"] floatValue];
                        nameFood = [NSString stringWithFormat:@"%@ %@ %@",AMLocalizedString(@"ค่าบริการ", nil),[@(serPer) stringValue],@"%"];
                        total = [NSString conventCurrency:[@(totalSer) stringValue]];
                    }
                    else if ([indexBill isEqualToString:@"couponBill"]) {
                        float totalSer = [cellList[@"total"] floatValue];
                        nameFood = [NSString stringWithFormat:@"%@",AMLocalizedString(@"รวมส่วนลดคูปอง", nil)];
                        total = [NSString conventCurrency:[@(totalSer) stringValue]];
                    }
                    else{
                        nameFood = [NSString checkNull:cellList[@"data"][@"name"]];
                        nameFood = [nameFood stringByAppendingFormat:@" ( %@ )",[NSString checkNullCurrency:cellList[@"subTotal"]]];
                        total = [NSString checkNullCurrency:cellList[@"dataorder"][@"total"]];
                    }
                    
                    [cell.mNameOrder setText:nameFood];
                    [cell.mTotalFood setText:total];
                }
                else{
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    NSNumber *blnum = [NSNumber numberWithInt:[cellList[@"subbill"] intValue]+1];
                    NSString *stringTitle = [NSString stringWithFormat:@" %@ : %@ ",AMLocalizedString(@"บิลย่อยที่", nil),[blnum stringValue]];
                    cell.mLabel.text = stringTitle;
                }
                cell.tag = 44;
            }
            else if ([cellIden  isEqual: kCellPromotion]) {
                cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                NSDictionary *dataProCell;
                if ([commit isEqual:@"99"]) {
                    dataProCell = promotionList[indexPath.row];
                }
                else{
                    dataProCell = addFoodBill[indexPath.row][@"data"];
                }
             
                if ([self checkDeliverStatus:cellList[@"dataorder"][@"deliver_status"]]) {
                    [cell.mLabel setAttributedText:[NSString checkDeliverStatus:dataProCell[@"name"]]];
                    [cell.mLabel setTag:-789];
                }
                else{
                    [cell.mLabel setText:dataProCell[@"name"]];
                }
                bool bl = [dataProCell[@"protype_lv1"] boolValue];
                if (bl) {
                    NSString *gift_type = dataProCell[@"gift_type"];
                    NSString *protype_lv2 = dataProCell[@"protype_lv2"];
                    if ([gift_type isEqual:@"3"] || [protype_lv2 isEqual:@"4"]) cell.tag = 33;
                    else  cell.tag =  11;
                    
                    cell.mImageBackgroud.image = nil;
                    if(![commit isEqual:@"99"]){    // กรณี โปรโมชั่นที่ยังไม่ได้กดใช้
                        cell.tag = 99;
                    }
                }
                else{
                    cell.tag = 22;
                }
            }
            else if ([cellIden isEqual:kCellBillBotton]) {
                cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
            }
            else if (![typeDrink isEqual:kDrinkTwo]) {
                float cu = ![Check checkNull:cellList[@"dataorder"][@"total"]]?[cellList[@"dataorder"][@"total"] floatValue]:[cellList[@"dataorder"][@"price"] floatValue] * [cellList[@"Count"] intValue];
                if (typeDrink != nil) {
                    cellIden = kCellDrinkBill;
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    if (indexPath.row == 0 && [commit isEqual:@"0"]) {
                        [cell.mImageBackgroud setImage:[UIImage imageNamed:@"paper_bill_top"]];
                    }
                    else if ([commit isEqual:@"1"]){
                        [cell.mImageBackgroud setImage:nil];
                    }
                }
                else{
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                }
                @try {
                    NSString *nameFood       = (typeDrink != nil) ?cellList[@"drinkGirl"][@"name"]:cellList[@"data"][@"name"];
                    NSString *count          = cellList[@"Count"];
                    NSString *bl             = cellList[@"dataorder"][@"promotion_id"];
                    NSString *menu_id        = cellList[@"dataorder"][@"menu_id"];
                    NSDictionary *dataM      = cellList[@"data"];
                    NSDictionary *dataProApp = cellList[@"dataPro"];
                    
                    if (![dataProApp isEqual:@""]) {
                        NSString *protype_lv1 = dataProApp[@"protype_lv1"];
                        NSString *protype_lv2 = dataProApp[@"protype_lv2"];
                        NSString *giftType = dataProApp[@"gift_type"];                        
                        float priceTotal = [cellList[@"dataorder"][@"price"] floatValue] * [cellList[@"Count"] intValue];
                        
                        if ([protype_lv1 isEqualToString:@"1"] && [protype_lv2 isEqualToString:@"4"]) {     // บุฟเฟอร์
                            nameFood = [NSString stringWithFormat:@"%@",nameFood];
                        }
                        else if ([protype_lv1 isEqualToString:@"1"] && [giftType isEqualToString:@"2"]) {   // GIFT as discount
                            nameFood = [NSString stringWithString:AMLocalizedString(@"ส่วนลด", nil)];
                        }
                        else if ([protype_lv1 isEqualToString:@"1"] && [giftType isEqualToString:@"1"]) {   // GIFT as product
                             nameFood = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ฟรี", nil),nameFood];
                        }
                        else if (priceTotal == 0){
                            nameFood = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ฟรี", nil),nameFood];
                        }
                    }
                    else if (![bl isEqual:@"0"] || [Check checkNull:nameFood]) {
                        float total      = [cellList[@"dataorder"][@"total"] floatValue];
                        float priceTotal = [cellList[@"dataorder"][@"price"] floatValue] * [cellList[@"Count"] intValue];
                        float price      = [cellList[@"dataorder"][@"price"] floatValue];
                        cu = ![Check checkNull:cellList[@"dataorder"][@"total"]]?total:priceTotal;
                        int typePro = [cellList[@"dataorder"][@"type"] intValue];
                        if ([dataM count] == 0) {
                            if (typePro == 2 || typePro == 3){
                                nameFood = [NSString stringWithString:AMLocalizedString(@"ส่วนลด", nil)];
                            }
                            else if (typePro == 4){
                                nameFood = [NSString stringWithFormat:@"%@ %.02f",AMLocalizedString(@"คูปอง", nil),fabsf(price)];
                            }
                            else if (cu == 0 && [menu_id isEqual:@"0"]){
                                nameFood = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ฟรี", nil),nameFood];
                            }
                        }
                    }
                    
                    NSString *countOrder = [NSString stringWithFormat:@"(%@)",(typeDrink  == nil)?count:typeDrink];
                    if (typeDrink != nil) {
                        nameFood = [NSString stringWithFormat:@"%@  (%@)",nameFood,(typeDrink  == nil)?count:typeDrink];
                        countOrder = @"(1)";
                    }
                    
                    NSString *option = [FoodOptionSum getFoodOption:cellList[@"dataorder"][@"food_option"]];
                    
                    if ([self checkDeliverStatus:cellList[@"dataorder"][@"deliver_status"]]) {
                        [cell.mLabel setAttributedText:[NSString checkDeliverStatus:nameFood]];
                        [cell.mCount setAttributedText:[NSString checkDeliverStatus:[NSString conventCurrency:[@(cu) stringValue]]]];
                        [cell.mOption setAttributedText:[NSString checkDeliverStatus:option]];
                        [cell.mCountOrder setAttributedText:[NSString checkDeliverStatus:countOrder]];
                         [cell.mLabel setTag:-789];
                    }
                    else{
                        [cell.mLabel setText:nameFood];
                        cell.mCount.text = [NSString conventCurrency:[@(cu) stringValue]];
                        [cell.mOption setText:option];
                        [cell.mCountOrder setText:countOrder];
                    }
                }
                @catch (NSException *exception) {
                    DDLogError(@"1 .Caught exception cellForRowAtIndexPath : %@", exception);
                }
            }
            else if ([typeDrink isEqual:kDrinkTwo]){
                @try {
                NSString *dateStrat = [Time conventTime:cellList[@"dataorder"][@"start_drink_time"]];
                NSString *dateStop  = [Time conventTime:cellList[@"dataorder"][@"stop_drink_time"]];
                
                if ([commit isEqual:@"1"] && (![dateStop isEqual:@"00:00"])) {
                    cellIden = kCellDrinkSubmit;
                    cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                    NSString *name       = cellList[@"drinkGirl"][@"name"];
                    NSString *countOrder = [NSString stringWithFormat:@"(%@)",cellList[@"TypeDrink"]];
                    NSString *total      = [NSString stringWithFormat:@"%@",cellList[@"dataorder"][@"total"]];
                    NSString *countDrink = [NSString stringWithFormat:@"(%@)",cellList[@"dataorder"][@"n"]];
                   
                    [cell.mTimeDrink setTextAlignment:NSTextAlignmentRight];
                    [cell.mCountOrder setText:countOrder];
                    if ([self checkDeliverStatus:cellList[@"dataorder"][@"deliver_status"]]) {
                        [cell.mLabel setAttributedText:[Text setColor:name
                                                                  and:[NSString stringWithFormat:@"(%@)",dateStrat]
                                                             andColor:[UIColor redColor]]];
                        
                        [cell.mLabel setAttributedText:[NSString checkDeliverStatus:cell.mLabel.text]];
                        [cell.mTimeDrink setAttributedText:[NSString checkDeliverStatus:countDrink]];
                        [cell.mCount setAttributedText:[NSString checkDeliverStatus:[NSString conventCurrency:total]]];
                         [cell.mLabel setTag:-789];
                    }
                    else{
                        [cell.mLabel setAttributedText:[Text setColor:name
                                                                  and:[NSString stringWithFormat:@"(%@)",dateStrat]
                                                             andColor:[UIColor redColor]]];
                        [cell.mTimeDrink setText:countDrink];
                        [cell.mCount setText:[NSString conventCurrency:total]];
                    }
                }
                else{
                        cellIden = kCellDrink;
                        cell = (CellTableViewAll *)[tableView dequeueReusableCellWithIdentifier:cellIden];
                        
                        NSString *name  = cellList[@"drinkGirl"][@"name"];
                        NSString *countOrder = [NSString stringWithFormat:@"(%@)",cellList[@"TypeDrink"]];
                        NSString *n2 = [NSString stringWithFormat:@"%@",name];
                        NSString *timeD = ([dateStop isEqual:@"00:00"])?(([dateStrat isEqual:@"00:00"])?dateStrat:[Time getTime]):dateStop;
                        
                        
                        [cell.mLabel setAttributedText:[Text setColor:n2
                                                                  and:dateStrat
                                                             andColor:[UIColor redColor]]];
                        
                        [cell.mTimeDrink setText:timeD];
                        [cell.mCountOrder setText:countOrder];
                        
                        if ([self checkDeliverStatus:cellList[@"dataorder"][@"deliver_status"]]) {
                            [cell.mLabel setAttributedText:[NSString checkDeliverStatus:cell.mLabel.text]];
                            [cell.mTimeDrink setAttributedText:[NSString checkDeliverStatus:timeD]];
                            [cell.mCountOrder setText:countOrder];
                             [cell.mLabel setTag:-789];
                        }
                        
                        if (indexPath.row == 0 && [commit isEqual:@"0"]) {
                            [cell.mImageBackgroud setImage:[UIImage imageNamed:@"paper_bill_top"]];
                        }
                        else if ([commit isEqual:@"1"]) {
                            [cell.mImageBackgroud setImage:nil];
                        }
                    }
                }
                @catch (NSException *exception) {
                     DDLogError(@"2 .Caught exception cellForRowAtIndexPath : %@", exception);
                }
            }
            if (cell.tag != 44) {
                NSString *statusEmblem  = @"●";
                int num = 5;
                int typePro = [cellList[@"dataorder"][@"type"] intValue];
                if (typePro == 1) {
                    num = [cellList[@"dataorder"][@"deliver_status"]  intValue];
                }
                
                UIColor *color = [self getColorEmblem:num];
                [cell.mLabel setAttributedText:[Text setColor:statusEmblem and:cell.mLabel.text andColor:color]];
                if ([self checkDeliverStatus:cellList[@"dataorder"][@"deliver_status"]]) {
                     [cell.mLabel setAttributedText:[NSString checkDeliverStatus:cell.mLabel.text]];
                     [cell.mLabel setTag:-789];
                }
            }
        }
        CGFloat height      = [self getSizeHightTable:cellIden];
        tableView.rowHeight = height;
        
        if (self.isAdddataPrint) {
            NSString *str = [NSString stringWithFormat:@" %@ : %@ ",AMLocalizedString(@"บิลย่อยที่", nil),self.indexSubBill];
            NSString *secIdx = [@(indexPath.section) stringValue];
            if (self.dataImagePrint[secIdx] == nil) {
                self.dataImagePrintList = [NSMutableArray array];
                [self.dataImagePrint setObject:self.dataImagePrintList forKey:secIdx];
                if ([cell.mLabel.text  isEqual:str] && self.isPrintSubBill) {
                    blAddPrit = YES;
                    [self.dataImagePrintList addObject:cell];
                    NSLog(@"add 1");
                }
                else if (!self.isPrintSubBill || [secIdx isEqualToString:@"2"]){
                    [self.dataImagePrintList addObject:cell];
                    NSLog(@"add 2");
                }
            }
            else{
                if (self.isPrintSubBill) {
                    if ([cell.mLabel.text rangeOfString:AMLocalizedString(@"บิลย่อยที่", nil)].location != NSNotFound && (cell.mLabel != nil)){
                        if ([cell.mLabel.text  isEqual:str] && self.isPrintSubBill) {
                            if (blAddPrit) {
                                blAddPrit = NO;
                            }
                            else{
                                [self.dataImagePrintList addObject:cell];
                                blAddPrit = YES;
                                NSLog(@"add 3");
                            }
                        }
                        else if (blAddPrit){
                            blAddPrit = NO;
                        }
                    }
                    else if (blAddPrit){
                       [self.dataImagePrintList addObject:cell];
                        NSLog(@"add 4");
                    }
                }
                else{
                    if (self.isPrintAllSubBill && cell.mDifference == nil) {
                        [self.dataImagePrintList addObject:cell];
                        NSLog(@"add 5");
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"Caught exception cellForRowAtIndexPath : %@", exception);
    }
    return cell;
}
-(UIColor *)getColorEmblem:(int)num{
    switch (num) {
        case 1:
        case 6:
            return  [UIColor colorWithRed:0.918 green:0.751 blue:0.171 alpha:1.000];
            break;
        case 2:
            return [UIColor colorWithRed:0.253 green:0.174 blue:0.920 alpha:1.000];
            break;
        case 3:
            return [UIColor colorWithRed:0.918 green:0.753 blue:0.332 alpha:1.000];
            break;
        case 4:
            return [UIColor colorWithRed:0.467 green:0.465 blue:0.399 alpha:1.000];
            break;
        case 0:
        case 5:
            return [UIColor colorWithRed:0.150 green:0.759 blue:0.068 alpha:1.000];
            break;
        default:
            return [UIColor whiteColor];
            break;
    }
}
-(BOOL)checkDeliverStatus:(NSString *)status{
    if ([status isEqual:@"4"]) {
        return YES;
    }
    return NO;
}
-(NSString *)cellTotal:(CellTableViewAll *)cell and:(UITableView *)tableView andDataTotal:(NSDictionary *)_total{
    
    NSString *cellIden;
    BOOL blTotalSubBill = NO;
    if ([_total[@"index"] isEqual:@"TotalSubbill"]) {
        cellIden = kCellTotalSubbill;
        blTotalSubBill = YES;
    }
    else if (self.isCashier) {
        cellIden = kCellTotalVat;
    }
    else{
        cellIden = kCellTotal;
    }

    NSInteger price    = 0;
    NSInteger discount = 0;
    NSInteger total    = 0;
//    NSInteger serPer   = 0;
//    NSInteger totalSer = 0;
    
    float vat = 0;
    if (_total != nil && !blTotalSubBill) {
        globalBill            = [GlobalBill sharedInstance];
//        NSDictionary *dicres  = [NSDictionary dictionaryWithObject:globalBill.billResturant];
//        NSDictionary *vat1    = [NSDictionary dictionaryWithString:dicres[@"config"]];
//        NSDictionary *vat2    = [NSDictionary dictionaryWithObject:vat1[@"vat"]];
        NSString *percent     = [NSString checkNull:_total[@"percent_vat"]];
//        NSDictionary *service = [NSDictionary dictionaryWithObject:_total[@"service"]];
        
//        serPer   = [service[@"per"] floatValue];
//        totalSer = [service[@"total"] floatValue];
        price    = [_total[@"total"] floatValue];
        discount = [_total[@"discount"] floatValue];
        total    = [_total[@"totalAll"] floatValue];
        vat      = [_total[@"vat"] floatValue];
        
        [cell.mTextVat setText:[NSString stringWithFormat:@"%@ %@ %@",AMLocalizedString(@"ภาษี", nil),percent,@"%"]];
        
//        NSString *serPerText = [NSString stringWithFormat:@"%@ %@ %@",AMLocalizedString(@"ค่าบริการ", nil),[@(serPer) stringValue],@"%"];
//        cell.mServicePer.text   = serPerText;
//        cell.mServiceTotal.text = [NSString conventCurrency:[@(totalSer) stringValue]];
    }
    else if (blTotalSubBill){
        NSDictionary *dicres1 = [NSDictionary dictionaryWithObject:_total];
        NSString *paid        = [NSString checkNull:dicres1[@"paid"]];
        NSString *change      = [NSString checkNull:dicres1[@"change"]];
        NSString *total       = [NSString checkNull:dicres1[@"total"]];
        
        [cell.mTotalFood setText:[NSString conventCurrency:total]];
        [cell.mTotalFood setTag:[dicres1[@"id"] intValue]];
        [cell.mDifference setText:[NSString stringWithFormat:@"%@ / %@",[NSString conventCurrency:paid],[NSString conventCurrency:change]]];
        return cellIden;
    }
    else{
        if (self.isCashier) {
            if ([self.sectionBill count] != 0) {
                NSArray *data = self.sectionBill[@"0"][@"data"];
                if ([self.sectionBill[@"0"][@"commit"] isEqual:@"1"]) {
                    for (NSDictionary *dic in data) {
                        if ([dic[@"index"] isEqual:@"totalBill"]) {
                            price    += [dic[@"total"] floatValue];
                            discount += [dic[@"discount"] floatValue];
                            total    += [dic[@"totalAll"] floatValue];
                        }
                    }
                }
            }
        }
        else{
            if ([self.sectionBill count] != 0) {
                NSArray *data = self.sectionBill[@"0"][@"data"];
                if ([self.sectionBill[@"0"][@"commit"] isEqual:@"1"]) {
                    for (NSDictionary *dic in data) {
                         if (![self checkDeliverStatus:dic[@"dataorder"][@"deliver_status"]]) {
                             NSInteger p2 =  [dic[@"dataorder"][@"total"] floatValue];
                             if (p2 < 0) {
                                 discount += p2;
                             }
                             else{
                                 price += p2;
                             }
                             total += p2;
                        }
                    }
                }
            }
        }
    }

    cell.mTotalFood.text    = [NSString conventCurrency:[@(price) stringValue]];
    cell.mDiscountFood.text = [NSString conventCurrency:[@(discount) stringValue]];
    [cell.mTotalAllFood setText:[NSString conventCurrency:[@(total) stringValue]]];
    [cell.mNumVat setText:[NSString conventCurrency:[@(vat) stringValue]]];
    return cellIden;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if([self.role isEqualToString:@"admin"]){
        return;
    }
    CellTableViewAll *cell =  (CellTableViewAll *)[tableView cellForRowAtIndexPath:indexPath];
    if (self.isCashier) {
        NSLog(@"yes");
        NSDictionary *dataOrder = addFoodBill[indexPath.row][@"dataorder"];
        if ([dataOrder count] == 0)  return;
//        if ([dataOrder[@"type"] intValue] == 2)  return;
        indexPathPop = indexPath;
        popupView = [[PopupView alloc] init];
        [popupView setTableDataBillViewController:self];
        [popupView setStatus:dataOrder[@"deliver_status"]];
        [popupView showPopupWithStyle:CNPPopupStyleCentered];
    }
    else{
        if (cell.tag == 22) {
            if ([self.dataPromotion[@"is_promotion"] boolValue]) {
                NSDictionary *dataProCell;
                if (cell.tag == 33) {
                    dataProCell = promotionList[indexPath.row];
                }
                else{
                    dataProCell = addFoodBill[indexPath.row][@"dataPro"];
                }
                NSString *idGiftType = [NSString stringWithFormat:@"id_%@",dataProCell[@"gift_type"]];
                
                TablePromotiomViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TablePromotiomView"];
                [_viewController setIdGiftType:idGiftType];
                [_viewController setSelecCell:dataProCell];
                [_viewController setTableSingleBillViewControll:(TableSingleBillViewController *)self.tableSingleBillViewController];
                [_viewController setTableSubBillViewController:(TableSubBillViewController *)self.tableSingleBillViewController];
                [_viewController loadDataPromotion];
                [[self navigationController] pushViewController:_viewController animated:NO];
            }
        }
        else if (cell.tag == 33 || cell.tag == 99) {
            if ([self.dataPromotion[@"is_promotion"] boolValue]) {
                ListItemPromotiomViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ListItemPromotiomView"];
                [_viewController setTableSingleBillViewController:(TableSingleBillViewController *)self.tableSingleBillViewController];
                [_viewController setTableSubBillViewController:(TableSubBillViewController *)self.tableSingleBillViewController];
                
            
                NSDictionary *dataProCell;
                if (cell.tag == 33) {
                    dataProCell = promotionList[indexPath.row];
                }
                else{
                    int indexSection = (indexPath.section == 0)?0:(int)indexPath.section-1;
                    NSArray *arrKey = [self.sectionBill allKeys];
                    NSMutableDictionary *dataAddFoodBill = self.sectionBill[arrKey[indexSection]];
                    NSMutableArray *dataBill = dataAddFoodBill[@"data"];
                    NSDictionary *dataPro ;
                    if ([dataBill count] != 0) {
                        dataPro = [NSDictionary dictionaryWithObject:dataBill[indexPath.row]];
                    }
                    dataProCell = dataPro[@"data"];
                    [_viewController setDataOrder:dataPro[@"dataorder"]];
                }

                NSArray *promotionPool  = [NSArray arrayWithArray:self.dataPromotion[@"promotion_pool"]];
                NSString *key =    dataProCell[@"id"];
                NSString *value =  @"promotion_id = %@";
                NSDictionary *dataPool = (NSDictionary *)[NSDictionary searchDictionary:key  andvalue:value andData:promotionPool];                
                NSString *keyInfo = [NSString stringWithFormat:@"id_%@",dataProCell[@"id"]];
                
                //if is buffet
                if([dataProCell[@"protype_lv2"] integerValue] == 4){
                    NSString *relateId = @"";
                    for(NSDictionary *orders in self.sectionBill[@"0"][@"data"]){
                        NSDictionary *dataOrder = [NSDictionary dictionaryWithObject:orders[@"dataorder"]];
                        float menuType          = [NSString checkNullConvertFloat:dataOrder[@"menu_type"]];
                        NSString *promotionID   = [NSString checkNull:dataOrder[@"promotion_id"]];
                        float delivery_status   = [NSString checkNullConvertFloat:dataOrder[@"delivery_status"]];
                        float is_enabled        = [NSString checkNullConvertFloat:dataOrder[@"is_enabled"]];
                        
                        if(menuType == 4 && [promotionID isEqualToString:key] && delivery_status != 4 && is_enabled != 0){
                            relateId = dataOrder[@"id"];
                            [_viewController setPromotionRelateId:relateId];
                            break;
                        }
                    }
                }
                
                [_viewController setLocTitle:dataProCell[@"name"]];
                [_viewController setDataPromotion:(NSMutableDictionary *)self.dataPromotion];
                [_viewController setDataPromotionPool:dataPool];
                [_viewController setDataPromotionInfo:self.dataPromotion[@"promotion_info"][keyInfo]];
                
                [[self navigationController] pushViewController:_viewController animated:NO];
            }
        }
        else if (cell.tag == 11) {
            NSDictionary *dataProCell = promotionList[indexPath.row];
            NSArray *arrList = [NSArray arrayWithObjects:self.promotionPoolID,dataProCell,nil];
            [self addPromotion:arrList andDataOrder:nil];
        }
        else{
            indexSectionAler = (indexPath.section == 0)?0:(int)indexPath.section-1;
            if (indexSectionAler != 0) {
                NSArray *arrKey = [self.sectionBill allKeys];
                NSMutableDictionary *dataAddFoodBill = self.sectionBill[arrKey[indexSectionAler]];
                NSMutableArray *dataBill = dataAddFoodBill[@"data"];
                NSDictionary *dataPro ;
                if ([dataBill count] != 0) {
                    dataPro = [NSDictionary dictionaryWithObject:dataBill[indexPath.row]];
                }
                
                NSString *titleAler = [NSString stringWithFormat:@"%@  %@ %@",AMLocalizedString(@"คุณต้องการยกเลิก", nil),dataPro[@"data"][@"name"],AMLocalizedString(@"ใช่หรือไม่", nil)];
                UIAlertView *alerMenu = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                                   message:titleAler
                                                                  delegate:self
                                                         cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                                         otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
                [alerMenu setTag:indexPath.row];
                [alerMenu show];
            }
        }
    }
}
-(void) cheakSubBillData:(NSMutableArray *)data{
    
}
-(NSString *)getTypeDrink:(NSString *)ind{
    if ([ind isEqual:@"1"]) {
        return kDrinkOne;
    }
    else if ([ind isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else if ([ind isEqual:@"3"]) {
        return kDrinkThree;
    }
    else{
        return nil;
    }
}
-(CGFloat )getSizeHightTable:(NSString *)ident{
    
    NSInteger index = [listCellTable indexOfObject:ident];
    switch (index) {
        case 0:
        case 11:
            return 50.0;
        case 5:
            return 103.0;
        case 9:
            return 140.0;
        case 12:
            return 60.0;
        case 13:
            return 70.0;
        case 15:
        case 16:
            return 30.0;
        default:
//        case 1:
//        case 2:
//        case 3:
//        case 4:
//        case 6:
//        case 7:
//        case 8:
//        case 10:
//        case 14:
            return 40.0;
            break;
    }
}
//#define kCellBillTop      (@"cellBillTop")            50
//#define kCellBill         (@"cellBill")               40
//#define kCellBillBotton   (@"cellBillBotton")         40
//#define kCellDrink        (@"cellDrink")              40
//#define kCellDrinkSubmit  (@"cellDrinkSubmit")        40
//#define kCellTotal        (@"cellTotal")              103
//#define kCellBillCommit   (@"cellBillCommit")         40
//#define kCellDrinkBill    (@"cellDrinkBill")          40
//#define kCellPromotion    (@"cellPromotion")          40
//#define kCellSignature    (@"cellSignature")          140
//#define kCellTileSubBill  (@"cellTileSubBill")        40
//#define kCellTotalALL     (@"cellTotalALL")           50
//#define kCellTotalVat     (@"cellTotalVat")           60
//#define kCellTotalSubbill (@"cellTotalSubbill")       70
//#define kCellOptionFood   (@"CellListOptionFood")
//#define kCellSumTotalBill (@"cellSumTotalBill")       30
//#define kCellProTotalBill (@"cellProTotalBill")       30

- (IBAction)btnOK:(NSNotification *)sender {
    NSLog(@"btnOK");
    addOrderNotApp = YES;
    if (self.useSubBill) {
        if (![pahtTh_url isEqual:@""] || !self.isSignAll) {
            [self loadDatasubmitOrders:0];
            [[self navigationController] popViewControllerAnimated:YES];
        }
        else{
            DDLogError(@"error btnOK");
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาป้อนลายเซ็นต์", nil)];
        }
    }
    else{
        NSMutableArray *orderData = (NSMutableArray *)[self.sectionBill[@"1"][@"data"] allObjects];
        if ([self checkPromotionMixer:orderData]) {
            DDLogError(@"error btnOK");
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาเลือกเมนูให้ครบตามจำนวน", nil)];
        }
        else{
            NSIndexPath *index  = [sender userInfo][@"section"];
            [self loadDatasubmitOrders:index.row];
        }
    }
}

#pragma mark - addOrder
-(void)addPromotion:(NSArray *)dataPro andDataOrder:(NSDictionary *)dataOrder{
    NSArray *arrID;
    if (self.isStaffTake) {
        arrID = @[self.detailTable[@"id"],self.detailTable[@"bill_id"]];
    }
    else{
        arrID = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
    }
    
    NSError *error = nil;
    NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dataPro[1]
                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                          error:&error]
                                               encoding:NSUTF8StringEncoding];
    NSString *image;
    if ([dataPro count] == 3) {
        image = [NSString checkNull:dataPro[2]];
    }
    else{
        image = @"";
    }
    
    NSString *billID = [NSString checkNull:arrID[1]];
    
    NSDictionary *parameter =@{@"code":self.member.code,
                               @"promotion_id":dataPro[1][@"id"],
                               @"promotion_pool_num":dataPro[0],
                               @"promotion_data":cartJSON,
                               @"usage_id":arrID[0],
                               @"bill_id":billID,
                               @"table_id":self.detailTable[@"id"],
                               @"picture":image};
    
    if (dataPro != nil) {
        NSString *protype = dataPro[1][@"protype_lv2"];
        if ([protype isEqualToString:@"4"]) {
            NSMutableDictionary *dicP = [NSMutableDictionary dictionaryMutableWithObject:parameter];
            NSDictionary *usage = [self.tableSingleBillViewController dataListSingBill][@"usage"];
            NSString *idTab = [NSString stringWithFormat:@"table_id_%@",self.detailTable[@"id"]];
            NSDictionary *dataUsage = usage[idTab];
            if ([dataUsage count] != 0) {
                NSString *count = [NSString checkNull:[@([dataUsage[@"n_female"] intValue] + [dataUsage[@"n_male"] intValue]) stringValue]];
                [dicP setObject:count forKey:@"n"];
                parameter = [dicP copy];
            }
        }
    }    
    
    if ([dataOrder count] != 0) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryMutableWithObject:parameter];
        [dic setObject:dataOrder[@"id"] forKey:@"order_id"];
        parameter = [dic copy];
    }
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryMutableWithObject:parameter];
    [dic setObject:self.member.UDID forKey:@"device_id"];
    [dic setObject:self.member.lg forKey:@"lang"];
    parameter = [dic copy];
    
    NSString *url = [NSString stringWithFormat:GET_REQUIRE_PROMOTIOM,self.member.nre];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer =[AFHTTPResponseSerializer serializer];
    [manager POST:url  parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if(responseObject != nil)
        {
            NSError *error;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&error];
            NSString *errorCode = json[@"error"];
            if([errorCode intValue] == 305 || [json[@"status"] boolValue]){
                
                if([errorCode intValue] == 305){
                    [loadProgress.loadView showWithMessage:AMLocalizedString(@"ส่งคำขอเรียบร้อย", nil) success:^(BOOL isComplete){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if ([self.tableSingleBillViewController isMemberOfClass:[TableSingleBillViewController class]]) {
                                [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                            }
                            else if ([self.tableSingleBillViewController isMemberOfClass:[TableSubBillViewController class]]){
                                [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                            }
                        });
                    }];
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if ([self.tableSingleBillViewController isMemberOfClass:[TableSingleBillViewController class]]) {
                            [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                        }
                        else if ([self.tableSingleBillViewController isMemberOfClass:[TableSubBillViewController class]]){
                            [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                        }
                    });
                }
            }
            else{
                DDLogError(@"error loadData  addPromotion : %@",json);
                [loadProgress.loadView showError];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSString* newStr = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
        DDLogError(@"ERROR loadData  %@",newStr);
    }];
}
-(void)addOrder:(NSMutableArray *)dataAddFoodBill andPro:(NSArray *)proList{
    addOrderNotApp = YES;
    NSLog(@"addOrder star");
    NSMutableArray  *listAddorder = [NSMutableArray array];
    NSArray *arrID;
    if (self.isStaffTake) {
        arrID = @[self.detailTable[@"id"],self.detailTable[@"bill_id"]];
    }
    else{
       arrID = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
    }
    NSString *billID = [NSString checkNull:arrID[1]];

    
    for (NSDictionary *dic in dataAddFoodBill) {
        NSString *comment = dic[@"Comment"];
        if (!comment) {
            comment = @"";
        }
        else if (![comment isEqual:AMLocalizedString(@"ข้อความ", nil)]){
            comment = dic[@"Comment"];
        }

        NSDictionary *data  = dic[@"data"];
        NSDictionary *para;
        NSError *error = nil;
        NSString *strOption = @"";
        if (![Check checkNull:dic[@"Option"]]) {
            strOption = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic[@"Option"]
                                                                                                 options:NSJSONWritingPrettyPrinted
                                                                                                   error:&error]
                                                        encoding:NSUTF8StringEncoding];
        }
        relateOrderID = ([Check checkNull:dic[@"Relate_order_id"]])?@"":dic[@"Relate_order_id"];
        
        if (![data isKindOfClass:[NSString class]]) {
            para =@{@"menu_type":data[@"type"], // 8
                  @"menu_id":data[@"id"],       // 1
                  @"type":@"1",
                  @"usage_id":arrID[0],
                  @"bill_id":billID,
                  @"price":dic[@"Price"],
                  @"n":dic[@"Count"],
                  @"food_option":strOption,
                  @"food_comment":comment,
                  @"kitchen_id":data[@"kitchen_id"],
                  @"relate_order_id":relateOrderID};
        }
        else{
            para =@{@"menu_name":dic[@"name"],
                    @"menu_type":@"8",
                    @"menu_id":@"1",
                    @"type":@"1",
                    @"usage_id":arrID[0],
                    @"bill_id":billID,
                    @"price":dic[@"Price"],
                    @"n":dic[@"Count"],
                    @"food_option":strOption,
                    @"food_comment":comment,
                    @"kitchen_id":dic[@"kitchen"][@"id"],
                    @"relate_order_id":relateOrderID
                    };
        }
        [listAddorder addObject:para];
    }
    [self loadDataAddOrder:listAddorder andImage:@"" andPro:proList];
    NSLog(@"addOrder end");
}
-(void)addOrderDrink:(NSMutableDictionary *)dataOrder{
    [loadProgress.loadView showProgress];
    NSArray *arrID ;
    addOrderNotApp = YES;
    
    if (self.isStaffTake) {
        arrID = @[self.detailTable[@"id"],self.detailTable[@"bill_id"]];
    }
    else{
        arrID = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
    }
    
    NSString *billID = [NSString checkNull:arrID[1]];
  
    UIImage *image = (UIImage *)dataOrder[@"StarSing"];
    NSString *typeDrink = dataOrder[@"TypeDrink"];

    
    NSMutableArray  *listAddorder = [NSMutableArray array];
    NSDictionary *data  = dataOrder[@"data"];
    NSString *kitchen_id = [Check checkNull:data[@"kitchen_id"]]?@"0":data[@"kitchen_id"];
    
    NSDictionary *para =@{@"start_drink_time":@"",
                          @"stop_drink_time":@"",
                          @"drinkgirl_type":[NSString getDrinkgirlTypeID:typeDrink],
                          @"shot_type":dataOrder[@"shot_type"],
                          @"menu_type":data[@"type"],
                          @"menu_id":data[@"id"],
                          @"type":@"1",
                          @"usage_id":arrID[0],
                          @"bill_id":billID,
                          @"price":data[@"price"],
                          @"n":dataOrder[@"Count"],
                          @"food_option":@"",
                          @"food_comment":@"",
                          @"kitchen_id":kitchen_id};
    [listAddorder addObject:para];
    [self uploadImageSign:image  andDic:listAddorder andType:@"start"];
}

#pragma mark - uploadImageSign
-(void)uploadImageSign:(UIImage *)image andDic:(NSMutableArray *)listAddorder andType:(NSString *)type{
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSDictionary *parameters = @{@"code": self.member.code,
                                 @"files":@"file"};
    NSString *url = [NSString stringWithFormat:UPLOAD_IMAGE,self.member.nre];
    [LoadMenuAll loadDataImage:imageData andParameter:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSString *paht = json[@"data"][@"th_url"];
            if (self.useSubBill || [type isEqual:@"blSub"]) {
                if (![image isEqual:nil]) {
                    CellTableViewAll *cell =  (CellTableViewAll *)[self.mTableViewBill cellForRowAtIndexPath:indexPathimageSingSubBill];
                    [cell.mImageBackgroud setImageWithURL:[NSURL URLWithString:paht]];
                    pahtTh_url = paht;
                }
                
                if ([type isEqual:@"stop"]) {
                    [self loadDataManageDrink:(NSMutableDictionary *)listAddorder and:paht];
                }
                else{
                    [self loadDataAddOrder:listAddorder andImage:paht andPro:nil];
                }
            }
            else if ([type isEqual:@"stop"]) {
                [self loadDataManageDrink:(NSMutableDictionary *)listAddorder and:paht];
            }
            else{
                [self loadDataAddOrder:listAddorder andImage:paht andPro:nil];
            }
        }
        else{
            [self uploadImageSign:image andDic:listAddorder andType:type];
        }
    }];
}

#pragma mark - loadData AddOrder
-(void)loadDataAddOrder:(NSMutableArray *)listAddorder  andImage:(NSString *)image andPro:(NSArray *)listPro{
    NSArray *arrID ;
    
    if (self.isStaffTake) {
        arrID = @[self.detailTable[@"id"],self.detailTable[@"bill_id"]];
    }
    else{
        arrID = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
    }
    
    NSError *error = nil;
    NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:listAddorder
                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                          error:&error]
                                               encoding:NSUTF8StringEncoding];
    
    NSDictionary *parameter = [NSDictionary dictionary];
    BOOL isProNull = false;
    if (listPro == nil) {
        listPro = [NSArray arrayWithObjects:@"",@"",nil];
         isProNull = true;
    }

    NSString *billID = [NSString checkNull:arrID[1]];
    
    parameter = @{@"code":self.member.code,
                @"usage_id":arrID[0],
                @"bill_id":billID,
                @"promotion_id":listPro[0],
                @"promotion_pool_num":listPro[1],
                @"menu_list":cartJSON,
                @"sign_pic_url":image};
    
   
    if (!isProNull) {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:parameter];
        [dic setObject:relateOrderID forKey:@"relate_order_id"];
        [dic setValue:@"1" forKey:@"is_buffet"];
        
        parameter = [dic mutableCopy];
        
    }
    
    NSString *url = [NSString stringWithFormat:GET_ADD_ORDER,self.member.nre];    
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        dispatch_async(dispatch_get_main_queue(), ^{ // 2
            BOOL bl = [json[@"status"] boolValue];
            if (bl) {
                if (addOrderNotApp) {
                    if ([self.tableSingleBillViewController isKindOfClass:[TableSubBillViewController class]]) {
                        [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                    }
                    else{
                        [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                    }
                    addOrderNotApp = NO;
                }
                else
                    [LoadProcess dismissLoad];
            }
            else{
                NSString *error = [NSString checkNull:json[@"error"]];
                if ([error isEqualToString:@"307"]) {
                    [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"โปรโมชั่นนี้หมดเวลา", nil)];
                }
                [LoadProcess dismissLoad];
            }
        });
    }];
}
-(void)loadDatasubmitOrders:(NSInteger)index{
    @try {
        NSArray *arrID;// = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
        
        NSString *billID;
        NSString *table_id;
        
        if (self.isStaffTake) {
            arrID = @[self.detailTable[@"table_id"],self.detailTable[@"bill_id"]];
            billID = [NSString checkNull:self.detailTable[@"bill_id"]];
            table_id = [NSString checkNull:self.detailTable[@"table_id"]];
        }
        else{
            arrID = [LoadMenuAll requirebillID:self.detailTable[@"id"]];
            billID = [NSString checkNull:arrID[1]];
            table_id = [NSString checkNull:self.detailTable[@"id"]];
        }
        
        NSMutableArray *orderData = (NSMutableArray *)[[self.sectionBill[@"1"][@"data"] valueForKey:@"dataorder"] allObjects];
        if (orderData != nil) {
            NSError *error = nil;
            NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:orderData
                                                                                               options:NSJSONWritingPrettyPrinted
                                                                                                 error:&error]
                                                      encoding:NSUTF8StringEncoding];
            
            NSDictionary *parameter = @{@"code":self.member.code,
                                        @"order_data":cartJSON,
                                        @"bill_id":billID,
                                        @"table_id":table_id,
                                        @"sign_pic_url":pahtTh_url};
            
            [loadProgress.loadView showWithStatus];
            NSString *url = [NSString stringWithFormat:GET_SUBMIT_ORDER,self.member.nre];
            [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
                bool status = [[json valueForKey:@"status"] boolValue];
                if (status){
                    [self.sectionDataIndex removeLastObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [loadProgress.loadView showProgress];
                        if ([self.tableSingleBillViewController isMemberOfClass:[TableSingleBillViewController class]]) {
                            [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                        }
                        else if ([self.tableSingleBillViewController isMemberOfClass:[TableSubBillViewController class]]){
                            [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                            [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                        }
//                        [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                         [LoadProcess dismissLoad];
                    });
                }
                else{
                    [loadProgress.loadView showError];
                }
            }];
        }
        else
            NSLog(@" error loadDatasubmitOrders");
    }
    @catch (NSException *exception) {
        DDLogError(@" error exception loadDatasubmitOrders : %@",exception);
    }
}
- (IBAction)btnClose:(NSNotification *)sender {
    NSLog(@"btnClose");
     @try {
         id orderData = [self.sectionBill[@"1"][@"data"] valueForKey:@"dataorder"];
         if (orderData != nil) {
             [self cancelOrder:orderData];
         }
         else
             NSLog(@" error btnClose");
     }
    @catch (NSException *exception) {
        DDLogError(@" error exception  btnClose : %@",exception);
    }
}
-(void)cancelOrder:(id )orderData{
    NSMutableArray *arrList = [NSMutableArray array];
    if ([orderData isKindOfClass:[NSDictionary class]]) {
        [arrList addObject:orderData];
    }
    else{
        for (NSDictionary *dic in orderData) {
            [arrList addObject:dic];
        }
    }
    NSError *error = nil;
    NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:arrList
                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                          error:&error]
                                               encoding:NSUTF8StringEncoding];
    NSDictionary *parameter = @{@"code":self.member.code,
                                @"order_data":cartJSON,
                                @"table_id":self.detailTable[@"id"]};
  
    NSString *url = [NSString stringWithFormat:SET_CANCEL_ORDER,self.member.nre];
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        bool status = [[json valueForKey:@"status"] boolValue];
        if (status){
            if ([self isCashier]) {
                [TableSingleBillViewController loadDataOpenTableCashier:self.detailTable[@"id"]];
            }
            else{
                if ([self.tableSingleBillViewController isMemberOfClass:[TableSingleBillViewController class]]) {
                    [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                }
                else if ([self.tableSingleBillViewController isMemberOfClass:[TableSubBillViewController class]]){
                    [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                }
            }
        }
        else{
            [loadProgress.loadView showError];
        }
    }];
}
- (IBAction)btnTagSignature:(NSNotification *)sender {
    NSIndexPath *index  = [sender userInfo][@"section"];
    NSDictionary *dataAddFoodBill = self.sectionBill[@"0"][@"data"][index.row];
    UIViewController  *viewController = [[UIViewController alloc] init];
    viewController.title = AMLocalizedString(@"ลายเซ็นต์", nil);
    
    viewController.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(contentViewController.view.frame.size.width - 70,3,30, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [contentViewController.view addSubview:btnOpiton];
    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    UIView *aView = [Check checkVersioniOS8]?nil:self.view;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:aView
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    CGSize size = popoverController.popoverContentSize;
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView setFrame:CGRectMake(0,0,size.width,size.height)];
    NSString *_singImage = dataAddFoodBill[@"dataorder"][@"sign"];
    NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];
    NSString *pahtstart = dataSing[@"start"];
    NSString *pahtstop = dataSing[@"stop"];
    if (pahtstop == nil) {
        [imageView setImageWithURL:[NSURL URLWithString:pahtstart]];
    }
    else{
        UIImageView *imageStart = [[UIImageView alloc] init];
        [imageStart setFrame:CGRectMake(0,0,size.width,size.height/2)];
        [imageStart setImageWithURL:[NSURL URLWithString:pahtstart]];
        
        UIImageView *imageStop = [[UIImageView alloc] init];
        [imageStop setFrame:CGRectMake(0,imageStart.frame.size.height,size.width,size.height/2)];
        [imageStop setImageWithURL:[NSURL URLWithString:pahtstart]];
        
        [imageView addSubview:imageStart];
        [imageView addSubview:imageStop];
    }
    [viewController.view addSubview:imageView];
}
- (IBAction)checkStopDrink:(NSNotification *)_note {
    if (note == nil) {
        note = [_note copy];
        popupView = [[PopupView alloc] init];
        [popupView setTableDataBillViewController:self];
        [popupView createPopoverSing:self];
    }
}
- (IBAction)imageSingSubBill:(id)sender {
    popupView = [[PopupView alloc] init];
    [popupView setTableDataBillViewController:self];
    [popupView createPopoverSing:self];
}
- (IBAction)getImageBtnPressed:(UIImage *)sender
{
    UIImage *image =  sender;
    if (image != nil) {
        [popupView btnClosePop:popupView.wyPopoverController];
        NSIndexPath *indexPath  = [note userInfo][@"index"];
        NSString *indexNum = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
        NSMutableDictionary *dataAddFoodBill;
        NSMutableDictionary *addFoodBillList;
        if ([indexNum isEqualToString:@"999"]) {
            addFoodBillList = [NSMutableDictionary dictionary];
            [addFoodBillList setObject:[note userInfo][@"drinkGirl"] forKey:@"drinkGirl"];
            [addFoodBillList setObject:[note userInfo][@"dataorder"] forKey:@"dataorder"];
        }
        else{
            if (sectionTable == 2) {
                int n = [indexNum intValue] -1;
                indexNum = [@(n) stringValue] ;
            }
            dataAddFoodBill  = self.sectionBill[indexNum];
            addFoodBillList  = dataAddFoodBill[@"data"][indexPath.row];
            if (self.useSubBill && ![[note name] isEqual:@"checkStopDrink"]) {
                dataAddFoodBill  = self.sectionBill[@"1"];
                addFoodBillList  = dataAddFoodBill[@"data"];
            }
            else if (self.useSubBill){
                dataAddFoodBill  = self.sectionBill[@"0"];
                addFoodBillList  = dataAddFoodBill[@"data"][indexPath.row];
                image = nil;
            }
        }
        
        [self uploadImageSign:image andDic:(NSMutableArray *)addFoodBillList andType:([[note name] isEqual:@"checkStopDrink"])?@"stop":@"blSub"];
    }
}
-(void)loadDataOrder:(NSString *)orderID success:(void (^)(NSDictionary * json))completion{
    if([self.role isEqualToString:@"admin"]){
        return;
    }
    NSDictionary *parameter = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_ORDER_DETAIL,self.member.nre,orderID];
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        bool status = [[json valueForKey:@"status"] boolValue];
        if (status){
            dispatch_async(dispatch_get_main_queue(), ^{ // 2
                if(completion) {
                    NSDictionary *data = json[@"data"];
                    completion(data);
                }
            });
        }
        else{
            [loadProgress.loadView showError];
        }
    }];
        
}
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender {
    @try {
        if (sender.state == UIGestureRecognizerStateBegan && !self.isCashier){// ([[self.selfSuper tableSuperClass] cashTableStatus] == nil)) {
            CGPoint p = [sender locationInView:self.mTableViewBill];
            indexPathPop = [self.mTableViewBill indexPathForRowAtPoint:p];
            UITableViewCell *buttonCell = [self.mTableViewBill cellForRowAtIndexPath:indexPathPop];
            
            if (buttonCell.tag == 44) {
                popupView = [[PopupView alloc] init];
                [popupView setTanPopup:11];
                [popupView setTableDataBillViewController:self];
                [popupView showPopupWithStyle:CNPPopupStyleCentered];
            }
            else{
                [self createDetailOrderPop];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@" error btnDetailMenuPop : %@",exception);
    }
}
-(void)createDetailOrderPop{
    NSMutableDictionary *dataAddFoodBill = self.sectionBill[@"0"];
    addFoodBill = dataAddFoodBill[@"data"];
    NSDictionary *dataOrder = addFoodBill[indexPathPop.row][@"dataorder"];
    NSDictionary *dataMenu = addFoodBill[indexPathPop.row];
    NSString *bill_id = dataOrder[@"id"];
    if (![bill_id isEqual:nil]) {
        [self loadDataOrder:bill_id success:^(NSDictionary *data){
            if([data[@"orders"] count] > 0){
                
                DetailOrderPopViewController *detailOrderPopView = (DetailOrderPopViewController *)[UIStoryboardMain instantiateViewControllerWithIdentifier:@"DetailOrderPopView"];
                [detailOrderPopView setDataOrder:data];
                [detailOrderPopView setDataMenu:dataMenu];
                [detailOrderPopView setTableDataBillView:self];
                [detailOrderPopView setTypeEditCashier:typeEditCashier];
                
                if ([self.tableSingleBillViewController isMemberOfClass:[TableSingleBillViewController class]]) {
                    [self.tableSingleBillViewController setIsPopup:YES];
                    
                }
                detailOrderPopView.preferredContentSize          = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height+80);
                detailOrderPopView.modalInPopover                = NO;
                
                popoverController                                = [[WYPopoverController alloc] initWithContentViewController:detailOrderPopView];
                popoverController.delegate                       = self;
                [popoverController presentPopoverFromRect:CGRectZero
                                                   inView:self.view
                                 permittedArrowDirections:WYPopoverArrowDirectionNone
                                                 animated:NO];
            }
            
        }];
    }
}
#pragma mark - upLoadImageStopDrink
-(void)upLoadImageStopDrink:(NSDictionary *)theData{
    UIButton *btn = [theData objectForKey:@"index"];
    [btn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn setTitle:@"..." forState:UIControlStateNormal];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    btn.tag = 99;
}
-(void)loadDataManageDrink:(NSMutableDictionary *)dataAddFoodBill and:(NSString *)image{
    NSString *orderID = dataAddFoodBill[@"dataorder"][@"id"];
    NSString *girlID = dataAddFoodBill[@"drinkGirl"][@"id"];
    
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"stop",
                                 @"order_id":orderID,
                                 @"girl_id":girlID,
                                 @"sign_pic_url":image};
    
    NSString *url = [NSString stringWithFormat:GET_MANAGE_DRINK,self.member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl){
            if ([self.tableSingleBillViewController isKindOfClass:[TableSubBillViewController class]]) {
                [TableSubBillViewController loadDataOpenTableSubBill:self.detailTable[@"id"]];
                note = nil;
            }
            else{
                [TableSingleBillViewController loadDataOpenTableSing:self.detailTable[@"id"]];
                NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
                [center postNotificationName:@"DrinkGirlsloadData"
                                      object:self
                                    userInfo:nil];
            }
            [LoadProcess dismissLoad];
        }
        else{
            [loadProgress.loadView showError];
        }
    }];
}
-(IBAction)btnClosePop:(id)sender   {
    [popoverController dismissPopoverAnimated:YES];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);

    if ([title isEqual:kTitleSignature]) {
        UIViewController  *viewController = [[UIViewController alloc] init];
        viewController.title = AMLocalizedString(@"ลายเซ็นต์", nil);
        
        viewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-100);
        viewController.modalInPopover = NO;
        UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnOpiton setFrame:CGRectMake(contentViewController.view.frame.size.width - 70,3,30, 30)];
        [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
        btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        [contentViewController.view addSubview:btnOpiton];
        
        popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
        popoverController.delegate = self;
        [popoverController presentPopoverFromRect:CGRectZero
                                           inView:self.view
                         permittedArrowDirections:WYPopoverArrowDirectionNone
                                         animated:NO];
        
        NSMutableDictionary *dataAddFoodBill = self.sectionBill[@"0"][@"data"][indexPathPop.row];
        NSString *index_ = dataAddFoodBill[@"index"];
        
        NSString *path = self.dataSubBill[[NSString stringWithFormat:@"id_%@",index_]][@"sign_pic_url"];
        CGSize size = popoverController.popoverContentSize;
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setFrame:CGRectMake(0,0,size.width,size.height)];
        [imageView setImageWithURL:[NSURL URLWithString:path] placeholderImage:nil];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [viewController.view addSubview:imageView];
    }
    else if ([title isEqual:AMLocalizedString(@"แก้ไขจำนวน", nil)]){
        [self createDetailOrderPop];
        typeEditCashier = @"edit";
    }
    else if ([title isEqual:AMLocalizedString(@"ทำเสร็จ", nil)]){
        [self createDetailOrderPop];
        typeEditCashier = @"complete";
    }
    else if ([title isEqual:AMLocalizedString(@"คืนของ(ลบรายการ)", nil)]){
        NSMutableDictionary *dataAddFoodBill = self.sectionBill[@"0"];
        addFoodBill = dataAddFoodBill[@"data"];
        NSDictionary *dataOrder = addFoodBill[indexPathPop.row][@"dataorder"];
        
        if(self.isCashier){
            [self createDetailOrderPop];
            typeEditCashier = @"delete";
        }
        else{
            [self cancelOrder:dataOrder];
        }
    }
}
- (IBAction)checkbtnEditOrder:(NSNotification *)_note {
    NSLog(@"checkbtnEditOrder");
    [loadProgress.loadView showSuccess];
    
    NSMutableDictionary *dataAddFoodBill = self.sectionBill[@"0"];
    addFoodBill = dataAddFoodBill[@"data"];
    NSDictionary *dataOrder = addFoodBill[indexPathPop.row][@"dataorder"];
    
    CellTableViewAll *buttonCell = [_note userInfo][@"section"];
    [buttonCell.mCountEdit resignFirstResponder];
    [buttonCell.mPasswordEdit resignFirstResponder];
    
    NSDictionary *arruser = @{@"username":self.member.un,@"password":buttonCell.mPasswordEdit.text};
    if (self.isCashier && [typeEditCashier isEqualToString:@"complete"]) {
        [self cencelOrderApprove:dataOrder andUser:arruser];
    }
    else{
        NSDictionary *parameters = @{@"code":self.member.code,
                                     @"user":arruser,
                                     @"action":typeEditCashier,
                                     @"order_id":dataOrder[@"id"]};
        
        if ([typeEditCashier isEqualToString:@"edit"]) {
            NSDictionary *arrupdate = @{@"n":buttonCell.mCountEdit.text};
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:parameters];
            [dic setObject:arrupdate forKey:@"update"];
            
            parameters = [dic mutableCopy];
        }
        
        NSString *url = [NSString stringWithFormat:GET_MANAGE_ORDER,self.member.nre];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            bool bl = [json[@"status"] boolValue];
            if (bl){
                [popoverController dismissPopoverAnimated:YES];
                [LoadProcess dismissLoad];
            }
            else{
                [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"รหัสผ่านไม่ถูกต้อง", nil)];
            }
        } error:^(NSDictionary *json) {
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"รหัสผ่านไม่ถูกต้อง", nil)];
        }];
    }
}

// delegate UIAlerView
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    int indexPath = (int)alertView.tag;
    if (buttonIndex == 1) {
        NSArray *arrKey = [self.sectionBill allKeys];
        NSMutableDictionary *dataAddFoodBill = self.sectionBill[arrKey[indexSectionAler]];
        NSMutableArray *dataBill = dataAddFoodBill[@"data"];
        NSDictionary *dataPro ;
        if ([dataBill count] != 0) {
            dataPro = [NSDictionary dictionaryWithObject:dataBill[indexPath]];
        }
        
        if (indexSectionAler == 0) {
             [self cencelOrderApprove:dataPro[@"dataorder"] andUser:nil];
        }
        else{
             [self cancelOrder:dataPro[@"dataorder"]];
        }
    }
}
-(void)cencelOrderApprove:(NSDictionary *)data andUser:(NSDictionary *)user{
    NSString *orderId = data[@"id"];
    
    NSDictionary *parameters;
    if (user != nil) {
        parameters = @{
                     @"code":self.member.code,
                     @"order_id":orderId,
                     @"deliver_status":@"5",
                     @"table_id":self.detailTable[@"id"],
                     @"user":user
                     };
    }
    else{
        NSString *deliver_status = @"4";
        if ([data[@"deliver_status"] isEqualToString:@"5"]) {
            deliver_status = @"6";
        }
        parameters = @{
                     @"code":self.member.code,
                     @"order_id":orderId,
                     @"deliver_status":deliver_status,
                     @"table_id":self.detailTable[@"id"]
                     };
    }
   
    NSString *url = [NSString stringWithFormat:SET_DELIVER_STATUS,self.member.nre];
    [loadProgress.loadView showWithStatus];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl){
            if (user != nil) {
                [popoverController dismissPopoverAnimated:YES];
            }
            else{
                [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"รหัสผ่านไม่ถูกต้อง", nil)];
            }
        [LoadProcess dismissLoad];
        }
    }];
}
-(BOOL)checkGiftType:(NSString *)num{
    switch ([num intValue]) {
        case 4:
        case 5:
        case 6:
        case 7:
            return true;
            break;
        default:
            return false;
            break;
    }
}
-(BOOL)checkPromotionMixer:(NSArray *)data{
    
    for (NSDictionary *dic in data) {
        NSString *menuID   = dic[@"dataorder"][@"menu_id"];
        NSString *giftType = dic[@"data"][@"gift_type"];
        if ([menuID isEqual:@"0"] && [giftType isEqual:@"3"] ) {    // โปรโมชั่น ที่ยังไม่ได้กดปุ่ม
            return true;
        }
    }
    return false;

}
@end
