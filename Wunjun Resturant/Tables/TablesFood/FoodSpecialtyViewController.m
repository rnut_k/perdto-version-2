//
//  FoodSpecialtyViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/6/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "FoodSpecialtyViewController.h"
#import "KitchenViewController.h"
#import "CustomNavigationController.h"
#import "FoodItemsOrderViewController.h"

#import "UIColor+HTColor.h"
#import "WYPopoverController.h"

@interface FoodSpecialtyViewController ()<UITextFieldDelegate,UITextViewDelegate,WYPopoverControllerDelegate>{
    WYPopoverController *popoverController;
}

@end

@implementation FoodSpecialtyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:AMLocalizedString(@"สั่งอาหารนอกเมนู", nil)];
    [self setTextView];
}
-(void) setTextView{
    self.mCommentFood.layer.cornerRadius = 10.0f;
    self.mCommentFood.layer.borderWidth = 1.0f;
    self.mCommentFood.clipsToBounds = NO;
    self.mCommentFood.backgroundColor = [UIColor whiteColor];
    self.mCommentFood.layer.masksToBounds = YES;
    self.mCommentFood.layer.borderColor = [UIColor whiteColor].CGColor;
    
    NSArray *allView = [self.mViewBackground subviews];
    for (UIView *vi in allView) {
        if ([vi isMemberOfClass:[UIView class]]) {
            vi.layer.cornerRadius = 10.0f;
            vi.layer.borderWidth = 1.0f;
            vi.clipsToBounds = NO;
            vi.layer.masksToBounds = YES;
            vi.layer.borderColor = [UIColor ht_silverColor].CGColor;
        }
    }
    
    [self.mCommentFood setTextColor:[UIColor ht_silverColor]];
    [self.mCommentFood setText:AMLocalizedString(@"อธิบาย", nil)];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    [self.mCommentFood setTextColor:[UIColor blackColor]];
    [self.mCommentFood setText:@""];
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"text begin");
    if ([self.mKitchen isEqual:textField]) {
        [self createViewPopupSelecKitchen];
        [textField resignFirstResponder];
    }
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"text end");
}

-(void)createViewPopupSelecKitchen{
//    
    CustomNavigationController *navCon = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"Kitchen"];
    [navCon setSelfClass:self];
    
    navCon.preferredContentSize = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-50);
    navCon.modalInPopover = NO;
    
    UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(navCon.view.frame.size.width - 50,3,30, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [navCon.view addSubview:btnOpiton];
    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:navCon];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}
- (IBAction)btnReply:(id)sender {
    
    if ([self.dataKitchen count]!= 0) {
        FoodItemsOrderViewController  *myController = (FoodItemsOrderViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        if (![myController isKindOfClass:[FoodItemsOrderViewController class]]) {
            myController = (FoodItemsOrderViewController *)[self.navigationController.viewControllers objectAtIndex:3];
        }
        
        NSString *price = self.mPriceFood.text;
        NSString *comment = self.mCommentFood.text;
        if ([comment isEqualToString:AMLocalizedString(@"อธิบาย", nil)]) {
            comment = @"";
        }
        
        NSString *count = self.mCountFood.text;
        NSString *option = @"";
        NSString *name = self.mNameFood.text;
        
        NSMutableDictionary *_data = [NSMutableDictionary dictionary];
        [_data setObject:comment  forKey:@"Comment"];
        [_data setObject:option   forKey:@"Option"];
        [_data setObject:count    forKey:@"Count"];
        [_data setObject:price    forKey:@"Price"];
        [_data setObject:name     forKey:@"name"];
        [_data setObject:self.dataKitchen forKey:@"kitchen"];
        [_data setObject:@""     forKey:@"data"];
        [myController.addFoodBill addObject:_data];
        
        NSMutableDictionary *_dataTag = [NSMutableDictionary dictionary];
        [_dataTag setObject:name  forKey:@"name"];
        [_dataTag setObject:count    forKey:@"count"];
        [_dataTag setObject:_data forKey:@"data"];
        [myController.dataTagAdd addObject:_dataTag];
        
        [self.navigationController popToViewController:myController animated:NO];
    }
    else{
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                          message:AMLocalizedString(@"กรุณาเลือกรายการครัว", nil)
                                                         delegate:self
                                                cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                                otherButtonTitles:AMLocalizedString(@"ไม่", nil), nil];
        
        [message show];
    }
    
}
- (IBAction)btnClosePop:(id)sender {
    [popoverController dismissPopoverAnimated:YES];
    if ([self.dataKitchen count] != 0) {
        [self.mKitchen setText:self.dataKitchen[@"name"]];
    }
    [self.mKitchen resignFirstResponder];
}
-(BOOL)shouldAutorotate
{
    return NO;
}

@end
