//
//  AdminTabBarViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 9/22/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "AdminTabBarViewController.h"
#import "Check.h"
#import "UIStoryboard+Main.h"

@interface AdminTabBarViewController (){
    
    LoadProcess *loadProcess;
    Member *member;
    
}
@end

@implementation AdminTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

    [self initSetViewTab:self.index];
//    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
//    {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.automaticallyAdjustsScrollViewInsets = YES;
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) initSetViewTab:(NSInteger)index{
    
    switch (index) {
        case 0:{
            AdminPendingViewController *pending   = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"AdminPending"];
            pending.view.frame = self.mContainer.bounds;
            [self.mContainer addSubview:pending.view];
            [self addChildViewController:pending];
        }
            break;
        case 1:{
            CashTableStatus *navCon  = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashMainView"];
            [navCon setRole:@"admin"];
            
            navCon.view.frame = self.mContainer.bounds;
            [self.mContainer addSubview:navCon.view];
            [self addChildViewController:navCon];
            
        }
            break;
        case 2:{
            CashSalesReportViewController *cashSales = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashSalesReportView"];
             [cashSales setDataAllZone:self.dataAllZone];
            
            cashSales.view.frame = self.mContainer.bounds;
            [self.mContainer addSubview:cashSales.view];
            [self addChildViewController:cashSales];
            
        }
            break;
        case 3:{
            CheckerInvoiceOrderController *stockView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerStockReportView"];
            [stockView setRole:@"admin"];
            [stockView setAdminStockReport:YES];
            
            stockView.view.frame = self.mContainer.bounds;
            [self.mContainer addSubview:stockView.view];
            [self addChildViewController:stockView];
        }
        default:
            break;
    }
    self.isFirst = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
