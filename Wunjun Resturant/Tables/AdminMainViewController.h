//
//  AdminMainViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/24/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdminNavigationController.h"
#import "AdminPendingViewController.h"
#import "CashSalesReportViewController.h"
#import "CashTableStatus.h"
#import "CheckerInvoiceOrderController.h"
#import "Member.h"
#import "AppDelegate.h"
#import "LoadMenuAll.h"
#import "LocalizationSystem.h"
#import "AdminTabBarViewController.h"

@interface AdminMainViewController : UITabBarController

@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UINavigationItem *titleName;

//@property (weak, nonatomic) IBOutlet UIView *ButtonCon;
//
//@property (weak, nonatomic) IBOutlet UIButton *homeBtn;
//@property (weak, nonatomic) IBOutlet UIButton *billBtn;
//@property (weak, nonatomic) IBOutlet UIButton *saleBtn;
//@property (weak, nonatomic) IBOutlet UIButton *reportBtn;
//@property (weak, nonatomic) IBOutlet UIButton *logOutBtn;
//
//@property (weak, nonatomic) IBOutlet UIImageView *homeImg;
//@property (weak, nonatomic) IBOutlet UIImageView *billImg;
//@property (weak, nonatomic) IBOutlet UIImageView *saleImg;
//@property (weak, nonatomic) IBOutlet UIImageView *reportImg;
//@property (weak, nonatomic) IBOutlet UIImageView *logOutImg;


//- (IBAction) clickHome:(id)sender;
//- (IBAction) clickBill:(id)sender;
//- (IBAction) clickSale:(id)sender;
//- (IBAction) clickReport:(id)sender;
//- (IBAction) clickLogOut:(id)sender;


@end
