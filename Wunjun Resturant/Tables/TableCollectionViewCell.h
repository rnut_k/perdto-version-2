//
//  TableCollectionViewCell.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *mImageTable;
@property (weak, nonatomic) IBOutlet UIImageView *mImageCheck;
@property (weak, nonatomic) IBOutlet UIImageView *mStatusDrink;

@property (weak, nonatomic) IBOutlet UILabel *mCountBill;
@property (weak, nonatomic) IBOutlet UILabel *mNameTable;
@property (weak, nonatomic) IBOutlet UILabel *mTimeOpen;
@property (weak, nonatomic) IBOutlet UILabel *mTimeClose;
@property (weak, nonatomic) IBOutlet UILabel *mSumTotal;
@property (weak, nonatomic) IBOutlet UIView *mViewCollection;
@property (weak, nonatomic) IBOutlet UIView *mViewSub;

@end
