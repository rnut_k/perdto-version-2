//
//  TableCollectionView.m
//  Wunjun Pub & Resturant
//
//  Created by AgeNt on 1/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableCollectionView.h"


@interface TableCollectionView () <UIActionSheetDelegate>

@end

@implementation TableCollectionView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.collectionViewLayout.numberOfItemsPerLine = 3;
    self.collectionView.collectionViewLayout.aspectRatio = 1;
    self.collectionView.collectionViewLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.collectionView.collectionViewLayout.interitemSpacing = 10;
    self.collectionView.collectionViewLayout.lineSpacing = 10;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 25;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (indexPath.section % 2 == 1) {
        cell.contentView.backgroundColor = [UIColor blueColor];
    } else {
        cell.contentView.backgroundColor = [UIColor redColor];
    }
    
    return cell;
}
@end
