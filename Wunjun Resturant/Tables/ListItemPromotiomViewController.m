//
//  ListItemPromotiomViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "ListItemPromotiomViewController.h"
#import "TableBillMenuViewController.h"
#import "MenuDB.h"
#import "NSDictionary+DictionaryWithString.h"
#import "GlobalBill.h"
#import "Constants.h"
#import "LocalizationSystem.h"

@interface ListItemPromotiomViewController(){
    UILabel *viewForHeaderCount;
    
    NSMutableArray *dataSelectProduct;
    NSMutableArray *dataSelectAddMixer;
    
    int maxCount;
    NSMutableDictionary *countMixer;
    NSString *protype_lv2;
    
    GlobalBill *globalBill;
    LoadProcess *loadProgress;
}
@end

@implementation ListItemPromotiomViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = YES;
    globalBill                      = [GlobalBill sharedInstance];
    loadProgress                    = [LoadProcess sharedInstance];


    self.mTitlePro.text             = self.locTitle;
    self.navigationController.title = self.locTitle;
    dataSelectProduct               = [NSMutableArray array];
    dataSelectAddMixer              = [NSMutableArray array];
    
    countMixer = [NSMutableDictionary dictionary];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadDataListItemPromotiom];
}
- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewDidDisappear:animated];
}

-(void)loadDataListItemPromotiom{
//    maxCount = [self.dataPromotionPool[@"n_limit"] intValue];
    maxCount = [self.dataOrder[@"n"] intValue];
    if (maxCount == 0) {
        maxCount = [self.dataPromotionPool[@"n_limit"] intValue];
    }
    
    MenuDB *menuDB = [MenuDB getInstance];
    NSString *typeDB = @"menu_0";
    [menuDB loadData:typeDB];
    NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    NSMutableArray *menuList = [NSMutableArray arrayWithArray:(NSMutableArray *)[json[@"menu"] allValues]];
    
    NSString *buffetMenuID = self.dataPromotionInfo[@"buffet_menu_id"];
    protype_lv2 = self.dataPromotionInfo[@"protype_lv2"];
    NSMutableArray *filteredArray = [NSMutableArray array];
    if ([protype_lv2 isEqual:@"4"]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id = %@)",buffetMenuID];
        NSDictionary *dic = [menuList filteredArrayUsingPredicate:predicate][0];
        NSString *str = dic[@"buffet_item_list"];
        
        NSError *jsonError;
        NSData *objectData = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        
        for (NSString *id_ in json) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id = %@)",id_];
            [filteredArray addObject:[menuList filteredArrayUsingPredicate:predicate][0]];
        }
        maxCount = 99;
    }
    else{
        NSString *type = @"2";
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(subtype == %@) AND (type == %@)",type,type];
        filteredArray = (NSMutableArray *)[menuList filteredArrayUsingPredicate:predicate];
    }
    if ([filteredArray count] != 0) {
        [dataSelectProduct addObjectsFromArray:filteredArray];
        [self.mTableSelectProduct reloadData];
    }
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.bounds.size.width,
                                                                  tableView.bounds.size.height)];
    if (tableView == self.mTableSelectProduct) {
        [headerView setBackgroundColor:[UIColor colorWithRed:32/255.0f green:33/255.0f blue:92/255.0f alpha:1.0f]];
    }
    else{
       [headerView setBackgroundColor:[UIColor ht_jayColor]];
       
        viewForHeaderCount = [[UILabel alloc] initWithFrame:CGRectMake(tableView.bounds.size.width-30,9,30,30)];
        viewForHeaderCount.text = [NSString stringWithFormat:@"%d/%d",(int)[dataSelectAddMixer count],maxCount];
        [viewForHeaderCount setTextColor:[UIColor ht_carrotColor]];
        viewForHeaderCount.font = [UIFont boldSystemFontOfSize:14];
        viewForHeaderCount.textAlignment = NSTextAlignmentCenter;
        [headerView addSubview:viewForHeaderCount];
    }

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,10,tableView.bounds.size.width-20,24)];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    [label setTextColor:[UIColor whiteColor]];
    label.font = [UIFont boldSystemFontOfSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    [headerView addSubview:label];
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.mTableSelectProduct) {
        return [NSString  stringWithString:AMLocalizedString(@"เลือกรายการ", nil)];
    }
    else{
        return [NSString  stringWithString:AMLocalizedString(@"รายการถูกเลือก", nil)];
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.mTableSelectProduct)
            return [dataSelectProduct count];
    else
        return [dataSelectAddMixer count];

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    cell.layer.cornerRadius = 10.0f;
    cell.layer.borderWidth = 1.0f;
    cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    cell.clipsToBounds = NO;
    cell.backgroundColor = [UIColor whiteColor];
    cell.layer.masksToBounds = YES;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    if (tableView == self.mTableSelectProduct) {
        NSDictionary *data = dataSelectProduct[indexPath.row];
        cell.textLabel.text = data[@"name"];
    }
    else{
        NSDictionary *data = dataSelectAddMixer[indexPath.row];
        cell.textLabel.text = data[@"name"];
        
        if ([protype_lv2 isEqual:@"4"]) {
            NSString *num1 = countMixer[data[@"id"]];
            cell.detailTextLabel.text = num1;
            cell.detailTextLabel.font = [UIFont systemFontOfSize:10];
        }
        [self setContentView:cell];
    }
    return cell;
}
-(void) setContentView:(UITableViewCell *)cell{
    UIImageView *customImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_delete"]];
    customImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [cell.contentView addSubview:customImageView];
    
    NSDictionary *views = NSDictionaryOfVariableBindings(customImageView);
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[customImageView(25)]"
                                                                             options:0 metrics:nil views:views]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[customImageView(25)]"
                                                                             options:0 metrics:nil views:views]];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[customImageView]-3-|"
                                                                             options:0 metrics:nil views:views]];
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[customImageView]-5-|"
                                                                             options:0 metrics:nil views:views]];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@" index %ld",(long)[indexPath  row]);
    if (tableView == self.mTableSelectProduct) {
        if ([dataSelectAddMixer count] < maxCount){
            
            if ([protype_lv2 isEqual:@"4"]) {
                NSString *idProCount = dataSelectProduct[indexPath.row][@"id"];
                NSString *num1 = countMixer[idProCount];
                if (num1  == nil) {
                    [dataSelectAddMixer  addObject:dataSelectProduct[indexPath.row]];
                }
                NSInteger numTmp = [num1 integerValue] + 1;
                NSString *num =[@(numTmp) stringValue];
                [countMixer setObject:num forKey:idProCount];
            }
            else{
                [dataSelectAddMixer  addObject:dataSelectProduct[indexPath.row]];
            }
        }
        else{
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"จำนวนเกินที่กำหนด", nil)];
        }
    }
    else{
        NSString *idProCount = dataSelectAddMixer[indexPath.row][@"id"];
        [countMixer removeObjectForKey:idProCount];
        [dataSelectAddMixer removeObjectAtIndex:indexPath.row];
    }
    
    if ([dataSelectAddMixer count] == maxCount || (maxCount == 99 && [dataSelectAddMixer count] > 0)) {
        [self createReplyAddMixer:1];
    }
    else{
        [self createReplyAddMixer:0];
    }
    
    [self.mTableSelectedItems reloadData];
    [self.mTableSelectProduct reloadData];
}
-(void)createReplyAddMixer:(int)num{
    if (num == 1) {
        UIImage *image = [[UIImage imageNamed:@"send"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
        self.mReplyAddMixer = [[UIBarButtonItem alloc] initWithImage:image
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(barButtonCustomPressed:)];
        self.navigationItem.rightBarButtonItem = self.mReplyAddMixer;
    }
    else{
        self.mReplyAddMixer = nil;
        self.navigationItem.rightBarButtonItem = nil;
    }
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)barButtonCustomPressed:(UIBarButtonItem*)btn
{
    @try {
        if ([protype_lv2 isEqual:@"4"]) {
            NSMutableArray *addFoodBill = [NSMutableArray array];
            for (NSDictionary *dic in dataSelectAddMixer) {
                NSString *num1 = countMixer[dic[@"id"]];
                NSMutableDictionary *_data = [NSMutableDictionary dictionary];
                [_data setObject:@""        forKey:@"Option"];
                [_data setObject:num1       forKey:@"Count"];
                [_data setObject:@"0"       forKey:@"Price"];
                [_data setObject:self.promotionRelateId forKey:@"Relate_order_id"];
                [_data setObject:dic  forKey:@"data"];
                [addFoodBill addObject:_data];
            }
            
            if ([addFoodBill count] != 0) {
                NSString *promotion_id = self.dataPromotionPool[@"promotion_id"];
                NSString *promotion_pool_num = self.dataPromotionPool[@"pool_num"];
                NSArray *listPro = [NSArray arrayWithObjects:promotion_id,promotion_pool_num, nil];
                
                if ([globalBill.billType  isEqual: kSingleBill]) {
                    [self.tableSingleBillViewController.tableDataBillView addOrder:addFoodBill andPro:listPro];
                    [self.navigationController popToViewController:self.tableSingleBillViewController animated:YES];
                }
                else{
                    [self.tableSubBillViewController.tableDataBillView addOrder:addFoodBill andPro:listPro];
                    [self.navigationController popToViewController:self.tableSubBillViewController animated:YES];
                }
            }
        }
        else{
            NSString *key = @"[";
            for (NSDictionary *dic in dataSelectAddMixer) {
                key = [[key stringByAppendingString:dic[@"id"]] stringByAppendingString:@","];
            }
            key = [[key substringToIndex:[key length]-1] stringByAppendingString:@"]"];
            NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
            tempDict = [self.dataPromotionInfo mutableCopy];
            [tempDict setValue:key forKey:@"gift_value"];
            
            if (self.tableSingleBillViewController != nil) {
                NSArray *arrList = [NSArray arrayWithObjects:self.tableSingleBillViewController.tableDataBillView.promotionPoolID,tempDict,nil];
                [self.tableSingleBillViewController.tableDataBillView addPromotion:arrList andDataOrder:self.dataOrder];
                [self.navigationController popToViewController:self.tableSingleBillViewController animated:YES];
            }
            else{
                NSArray *arrList = [NSArray arrayWithObjects:self.tableSubBillViewController.tableDataBillView.promotionPoolID,tempDict,self.dataOrder,nil];
                [self.tableSubBillViewController.tableDataBillView addPromotion:arrList andDataOrder:self.dataOrder];
                [self.navigationController popToViewController:self.tableSubBillViewController animated:YES];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception barButtonCustomPressed : %@",exception);
    }
}
@end
