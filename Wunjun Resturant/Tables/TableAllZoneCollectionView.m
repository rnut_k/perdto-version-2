//
//  TableAllZoneCollectionView.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableAllZoneCollectionView.h"
#import "TableOpenViewController.h"
#import "TableSubBillViewController.h"
#import "TableSingleBillViewController.h"
#import "TableOpenSumViewController.h"
#import "Member.h"
#import "GlobalBill.h"
#import "PopupView.h"
#import "LoadMenuAll.h"
#import "LoadProcess.h"
#import "SelectBill.h"
#import "Check.h"
#import "NSString+GetString.h"

@interface TableAllZoneCollectionView()<CNPPopupControllerDelegate>{
    NSDictionary *_dataTablesTemp;
    
    GlobalBill *_globalbill;
    PopupView *popupView;
    
    LoadProcess *loadProgress;
    
    UIRefreshControl *refreshControl;
}
@property (nonatomic, strong) Member *member;
@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, strong) UILongPressGestureRecognizer *lpgr;

@end

@implementation TableAllZoneCollectionView

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
-(int)numTable{
    UIDeviceOrientation ori = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(ori)) {
        return  [Check checkDevice]?6:8;
    }
    else{
        return  [Check checkDevice]?3:6;
    }
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (KRLCollectionViewGridLayout *)layout{
    return (id)self.mAllZoneCV.collectionViewLayout;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    self.layout.numberOfItemsPerLine = [[actionSheet buttonTitleAtIndex:buttonIndex] integerValue];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    popupView = [[PopupView alloc] init];
    self.layout.numberOfItemsPerLine = self.numTable;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    [self.mAllZoneCV addSubview:refreshControl];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [LoadProcess dismissLoad];
}
- (void)refershControlAction
{
    NSLog(@"Reload grid");
    
    [LoadMenuAll retrieveData:self.dataListZone[@"id"] success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setDataListTables:dataTables];
            [self.collectionView reloadData];
            [refreshControl endRefreshing];
        });
    }];
    
  
}
- (void)updateTitleLabels {
    
}
- (NSString *)viewControllerTitle{
    return _viewTitle ? _viewTitle : self.title;
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataListTables count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
    cell.userNameLabel.text = [NSString stringWithFormat:@"%@",dic[@"label"]];
    if ([dic[@"status"] intValue]!= 1) {      // สถานะโต็ะ
        cell.userNameLabel.textColor = [UIColor whiteColor];
        cell.mImageTable.image = [UIImage imageNamed:@"board_off"];
        cell.tag = 1;
        
        if (self.cashTableStatus == nil) {
            self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                      action:@selector(handleLongPressGestures:)];
            self.lpgr.minimumPressDuration = 1.0f;
            self.lpgr.allowableMovement = 100.0f;
            [cell addGestureRecognizer:self.lpgr];
        }
    }
    else{
        cell.userNameLabel.textColor = [UIColor ht_leadColor];
        cell.mImageTable.image = [UIImage imageNamed:@"board_on"];
        cell.tag = 0;
    }
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    TableCollectionViewCell *_cell = (TableCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];

    if (self.tableMoveView != nil) {
        if (_cell.tag != 1) {
            
            NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
            _dataTablesTemp = [NSMutableDictionary dictionaryWithObject:dic];
            self.tableMoveView.mNumMoveTo.text = _dataTablesTemp[@"label"];
            self.tableMoveView.otpTableTxt.placeholder = _dataTablesTemp[@"id"];
            [self.tableMoveView setIndexMoveTo:_dataTablesTemp];
        }
        else{
            [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"โต๊ะไม่ว่าง", nil)];
        }
    }
    else{
        TableCollectionViewCell *_cell = (TableCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        if (_cell.tag == 1){
            id num = [NSNumber numberWithInteger:indexPath.row];
            [self loadDataOpenTable:num andType:@"in"];
        }
        else{
            if (self.cashTableStatus == nil){
                if(![self.role isEqualToString:@"admin"]){
                    self.numMode = 0;
                    NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
                    _dataTablesTemp = [NSMutableDictionary dictionaryWithObject:dic];
                    popupView.tanPopup = 0;
                    [popupView setTableAllZoneCollectionView:self];
                    [popupView showPopupWithStyle:CNPPopupStyleCentered];
                }
            }                
        }
        // 0 เปิดโต็ะ ครั้งแรก
        // 1 เปิดโต็ะแล้ว
    }
}

// Change Background of UICollectionView Cell on Tap
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cellCV = [collectionView cellForItemAtIndexPath:indexPath];
    cellCV.contentView.backgroundColor = [UIColor colorWithRed:0.075 green:0.098 blue:0.290 alpha:1.000];
    [UIView animateWithDuration:0.5 animations:^{
        cellCV.contentView.backgroundColor = [UIColor clearColor];
    }];
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
    for(UICollectionViewCell *cell in self.mAllZoneCV.visibleCells){
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
    }
    
//    
    if ([title isEqual:kTableOpen]) {
        TableOpenViewController *viewController_ = (TableOpenViewController *)[UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableOpen"];
        [viewController_ setDataTable:_dataTablesTemp];
        [viewController_ setCashTableStatus:self.cashTableStatus];
        [[self navigationController] pushViewController:viewController_ animated:NO];
    }
    else if ([title isEqual:AMLocalizedString(@"เปิดโต๊ะแบบรวม", nil)]) {
        TableOpenSumViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableOpenSum"];
        [viewController_ setDataTable:_dataTablesTemp];
        [viewController_ setDataListTablesAll:self.dataListTables];
        [viewController_ setDataListZoneAll:self.dataListZoneAll];
        [viewController_ setCashTableStatus:self.cashTableStatus];
        [[self navigationController] pushViewController:viewController_ animated:NO];
    }
    else if ([title isEqual:kTableMove]) {
        [LoadMenuAll loadDataOpenTable:_dataTablesTemp[@"id"] success:^(NSDictionary *dictionary) {
            //    โหลดเพื่อ เพิ่มข้อมูลใน  globalbill billList
        }];
        TableMoveViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableMove"];
        [viewController_ setIndexMove:_dataTablesTemp];
        [viewController_ setDataListTablesAll:self.dataListTables];
        [viewController_ setDataListZoneAll:self.dataListZoneAll];
        [[self navigationController] pushViewController:viewController_ animated:NO];
    }
    else if ([title isEqual:kTablesPlitBill]) {
        [self loadDataSplitBill:_dataTablesTemp[@"id"] ];
    }
    else if([title isEqual:kTableOTP]){
        
        NSDictionary *parameters = @{@"code":self.member.code};
        NSString *url = [NSString stringWithFormat:GET_BILL,self.member.nre,_dataTablesTemp[@"id"]];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if([json[@"status"] boolValue])
            {
                NSDictionary *data = json[@"data"];
                NSString *table_key = [NSString stringWithFormat:@"table_id_%@",data[@"table_id"]];
                NSString *otp = data[@"usage"][table_key][@"otp"];
                otp = ([otp isEqualToString:@""])?@"-":otp;
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OTP"
                                                                message:otp
                                                               delegate:nil
                                                      cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
    else if ([title isEqualToString:AMLocalizedString(@"แสดง QR Code", nil)]){
        [self alertShowQRCode];
    }
}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan){
        TableCollectionViewCell *_cell = (TableCollectionViewCell *)[sender view];
        NSIndexPath *indexPath = [self.mAllZoneCV indexPathForCell:_cell];
        int _num = (int)_cell.tag;
        
        NSLog(@"handleLongPressGestures :%ld",(long)indexPath);
        
        NSDictionary *dic = [self.dataListTables objectAtIndex:[indexPath row]];
        _dataTablesTemp = [NSMutableDictionary dictionaryWithObject:dic];
        self.numMode = _num;
        [popupView setTableAllZoneCollectionView:self];
        [popupView showPopupWithStyle:CNPPopupStyleCentered];
    }
}
-(void)loadDataOpenTable:(id)idTable andType:(NSString *)type{
    [loadProgress.loadView showWithStatus];
    
    NSDictionary *dic;
    if ([type isEqual:@"out"])
        dic = [NSDictionary dictionaryWithObject:idTable];
    else if ([type isEqual:@"split"]){
        NSArray *data = [NSDictionary searchDictionary:[idTable stringValue]
                                             andvalue:@"(id = %@)"
                                              andData:self.dataListTables];
        dic = (NSDictionary *)[data copy];
    }
    else{
        dic = self.dataListTables[[idTable intValue]];
    }
    

   
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"in",
                                 @"table_id":dic[@"id"]};
    NSString *url = [NSString stringWithFormat:GET_BILL,self.member.nre,dic[@"id"]];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSDictionary *_data = json[@"data"];
        if ([_data count] != 0) {
            NSLog(@"\n loadDataOpenTable");
            [SelectBill setCheckOpenTable:YES];
            [SelectBill setIsTake:3];
            if (self.cashTableStatus != nil){
                [SelectBill setIscashTableStatus:YES];
            }
            else{
                [SelectBill setIscashTableStatus:NO];
            }
            [SelectBill setIsCashier:NO];
            [SelectBill selectBill:self andData:_data andDataTable:dic fromRole:self.role];
        }
    }];
}
-(void)loadDataSplitBill:(NSString *)idTable{
    [loadProgress.loadView showWithStatus];
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"split",
                                 @"table_id":idTable
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        bool bl = [dictionary[@"status"] boolValue];
        if (bl) {
            id num = [NSNumber numberWithInteger:[idTable intValue]];
            [self loadDataOpenTable:num andType:@"split"];
        }
    }];
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    UIDeviceOrientation ori = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(ori)) {
        self.layout.numberOfItemsPerLine = [Check checkDevice]?6:8;
    }
    else{
        self.layout.numberOfItemsPerLine = [Check checkDevice]?3:6;
    }
}
-(void) alertShowQRCode{
    Member *member = [Member getInstance];
    [member loadData];
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_BILL,self.member.nre,_dataTablesTemp[@"id"]];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"] boolValue])
        {
            NSDictionary *data = json[@"data"];
            NSString *table_key = [NSString stringWithFormat:@"table_id_%@",data[@"table_id"]];
            NSString *otp = data[@"usage"][table_key][@"otp"];

            NSString *url = [NSString stringWithFormat:GET_QRCODE,member.nre,otp];
            popupView = [[PopupView alloc] init];
            [popupView setTanPopup:44];
            [popupView setTitlePop:AMLocalizedString(@"แสดง QR Code", nil)];
            [popupView setTableAllZoneCollectionView:self];
            
            UIImageView *imgView = [[UIImageView alloc] init];
            imgView.contentMode = UIViewContentModeScaleAspectFit;
            imgView.clipsToBounds = YES;
            imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            
            NSURLRequest *imageRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
            [imgView setImageWithURLRequest :imageRequest placeholderImage:nil success:^(NSURLRequest *request,NSHTTPURLResponse *response,UIImage *image){
                [imgView setImage:image];
                imgView.contentMode = UIViewContentModeScaleAspectFit;
                imgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
                [imgView setNeedsLayout];
                [popupView setImagePop:imgView.image];
                [popupView showPopupWithStyle:CNPPopupStyleCentered];
            }failure:^(NSURLRequest *request,NSHTTPURLResponse *response, NSError *error){
                DDLogError(@"error alertShowQRCode :%@",error);
            }];
        }
    }];
 
}
@end
