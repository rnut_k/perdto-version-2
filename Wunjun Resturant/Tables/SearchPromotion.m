//
//  SearchPromotion.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/5/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "SearchPromotion.h"
#import "MenuDB.h"
#import "NSDictionary+DictionaryWithString.h"
#import "NSString+GetString.h"
#import "GlobalBill.h"
#import "Check.h"

@implementation SearchPromotion

static NSString  *idBillOrder = @"0";
static NSInteger idprotype_lv2 = 0;
static NSInteger idprotype_lv1 = 0;
static NSDictionary *promotion_info;
static NSDictionary *menuSpecialt;

+(void)setIdBillOrder:(NSString *)idBill{
    idBillOrder = idBill;
}
+(void)setStaticValel:(NSDictionary *)_promotion_info{
    promotion_info = _promotion_info;
}
+(void)setSpecialtFood:(NSDictionary *)menu{
    menuSpecialt = [NSDictionary dictionaryWithObject:menu];
}
+(void)setStaticValel:(NSInteger)lv1 and:(NSInteger)lv2{
    idprotype_lv1 = lv1;
    idprotype_lv2 = lv2;
}
+(NSArray *) getPromotionPool:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo{
    NSMutableArray *promotionPool = [NSMutableArray array];
    @try {
        for (NSDictionary *dic in dataPool) {
            NSInteger n_limit = [dic[@"n_limit"]  integerValue];
            NSInteger n_current = [dic[@"n_current"]  integerValue];
            NSString *idPool = [NSString stringWithFormat:@"id_%@",dic[@"promotion_id"]];
            NSDictionary *dataInfoList = dataInfo[idPool];
            NSInteger protype_lv2 = [dataInfoList[@"protype_lv2"] integerValue];
            if (n_current < n_limit || protype_lv2 == 4) {
                NSInteger protype_lv1 = [dataInfoList[@"protype_lv1"] integerValue];
                NSString *folder_id = dataInfoList[@"folder_id"];
                if ((protype_lv1 == 1) && (protype_lv2 == 1) && [folder_id isEqual:@"0"]) { // ไม่มี floder_id
                    [promotionPool addObject:dataInfoList];
                }
                else if(![folder_id isEqual:@"0"]) {   //  เช็กว่ามีอยู่ใน floder_id
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id = %@)",folder_id];
                    NSArray *filteredArray = [dataFloder filteredArrayUsingPredicate:predicate];
                    if ([filteredArray count] != 0) {
                        NSMutableArray *menuListTmp = [NSMutableArray array];
                        for (id arr in promotionPool) {   // ตรวจสอบโปรโมชั่นที่มีอยู่แล้ว
                            if ([arr isKindOfClass:[NSArray class]])
                                [menuListTmp addObject:arr[0]];
                            else
                                [menuListTmp addObject:arr];
                        }
                        NSArray *filteredArray1 = [NSDictionary searchDictionary:folder_id andvalue:@"(id = %@)" andData:menuListTmp];
                        if ([filteredArray1 count] == 0) {
                            [promotionPool addObject:filteredArray];
                        }
                    }
                }
                else if((protype_lv1 == 1) && (protype_lv2 == 4)){  // buffet promotion
                    if (n_limit == 0) {
                            NSMutableArray *menuListTmp = [NSMutableArray array];
                            for (id arr in promotionPool) {   // ตรวจสอบโปรโมชั่นที่มีอยู่แล้ว
                                if ([arr isKindOfClass:[NSArray class]])
                                    [menuListTmp addObject:arr[0]];
                                else
                                    [menuListTmp addObject:arr];
                            }
                            NSArray *filteredArray1 = [NSDictionary searchDictionary:folder_id andvalue:@"(id = %@)" andData:menuListTmp];
                            if ([filteredArray1 count] == 0) {
                                [promotionPool addObject:dataInfoList];
                            }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"1. Caught exception getPromotionPool : %@", exception);
    }
    return promotionPool;
}
+(NSArray *) getPromotionPoolAttachBill:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo and:(NSDictionary *)promotionApproval{
    NSMutableArray *promotionPool = [NSMutableArray array];
    @try {
        for (NSDictionary *dic in dataPool) {
            NSInteger n_limit = [dic[@"n_limit"]  integerValue];
            NSInteger n_current = [dic[@"n_current"]  integerValue];
            NSString *idPool = [NSString stringWithFormat:@"id_%@",dic[@"promotion_id"]];
            NSDictionary *dataInfoList = dataInfo[idPool];
            NSInteger protype_lv2 = [dataInfoList[@"protype_lv2"] integerValue];
            if (n_current < n_limit || protype_lv2 == 4) {
                NSInteger protype_lv1 = [dataInfoList[@"protype_lv1"] integerValue];
                NSString *folder_id = dataInfoList[@"folder_id"];
                
                NSString *idPoolApp = [NSString stringWithFormat:@"promotion_id_%@",dic[@"promotion_id"]]; // เช็กโปร id promotion_approval
                NSDictionary *pmtApproval;
                if ([promotionApproval count] != 0) {
                    pmtApproval = promotionApproval[idPoolApp];
                }
                
                if ((protype_lv1 == 1) && (protype_lv2 == 2) && [folder_id isEqual:@"0"]) {   // ไม่มี floder_id
                    [promotionPool addObject:dataInfoList];
                }
                else if((protype_lv1 == 1) && (protype_lv2 == 2)){   // มี floder_id
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(id = %@)",folder_id];
                    NSArray *filteredArray = [dataFloder filteredArrayUsingPredicate:predicate];
                    if ([filteredArray count] != 0) {
                        NSMutableArray *menuListTmp = [NSMutableArray array];
                        for (NSArray *arr in promotionPool) {       // ตรวจสอบโปรโมชั่นที่มีอยู่แล้ว
                            [menuListTmp addObject:arr[0]];
                        }
                        NSArray *filteredArray1 = [NSDictionary searchDictionary:folder_id andvalue:@"(id = %@)" andData:menuListTmp];
                        if ([filteredArray1 count] == 0) {
                            [promotionPool addObject:filteredArray];
                        }
                    }
                }
                else if([pmtApproval count] != 0){     // เช็กโปร promotion_approval
                    NSString *status = pmtApproval[@"status"];
                    if ([status isEqual:@"1"]) {
                        NSString *object_id = pmtApproval[@"object_id"];
                        NSString *idPool = [NSString stringWithFormat:@"id_%@",object_id];
                        NSDictionary *dataInfoList = dataInfo[idPool];
                        [promotionPool addObject:dataInfoList];
                    }
                    
                }
                else if((protype_lv1 == 1) && (protype_lv2 == 4)){   // มี floder_id
                    if (n_limit == 9999) {
                        [promotionPool addObject:dataInfoList];
                    }
                }
            }
         
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"2. Caught exception getPromotionPool : %@", exception);
    }
    return promotionPool;
}
+(NSArray *) getPromotionRequire:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo{
    NSMutableArray *promotionPool = [NSMutableArray array];
    @try {
        for (NSDictionary *dic in dataPool) {
            NSInteger n_limit = [dic[@"n_limit"]  integerValue];
            NSInteger n_current = [dic[@"n_current"]  integerValue];
            NSString *idPool = [NSString stringWithFormat:@"id_%@",dic[@"promotion_id"]];
            NSDictionary *dataInfoList = dataInfo[idPool];
            NSInteger protype_lv2 = [dataInfoList[@"protype_lv2"] integerValue];
            if (n_current < n_limit) {
                NSInteger protype_lv1 = [dataInfoList[@"protype_lv1"] integerValue];
                NSString *folder_id = dataInfoList[@"folder_id"];
                
                if ((protype_lv1 == 1) && (protype_lv2 == 3) && [folder_id isEqual:@"0"]) {   // ไม่มี floder_id
                    [promotionPool addObject:dataInfoList];
                }
                else if((protype_lv1 == 1) && (protype_lv2 == 3)){     // มี floder_id
                    NSArray *filteredArray = [NSDictionary searchDictionary:folder_id
                                                                    andvalue:@"(id = %@)"
                                                                     andData:dataFloder];
                    if ([filteredArray count] != 0) {
                        NSMutableArray *menuListTmp = [NSMutableArray array];
                        for (NSArray *arr in promotionPool) {        // ตรวจสอบโปรโมชั่นที่มีอยู่แล้ว
                            [menuListTmp addObject:arr[0]];
                        }
                        NSArray *filteredArray1 = [NSDictionary searchDictionary:folder_id
                                                                        andvalue:@"(id = %@)"
                                                                         andData:menuListTmp];
                        if ([filteredArray1 count] == 0) {
                            [promotionPool addObject:filteredArray];
                        }
                    }
                }
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"3. Caught exception getPromotionPool : %@", exception);
    }
    return promotionPool;
}
+(NSMutableArray *)addBillData:(NSMutableArray *)data  andPop:(BOOL)isPopupCashier{
    NSMutableArray *addFoodBill = [NSMutableArray array];
    MenuDB *menuDB = [MenuDB getInstance];
    
    NSString *typeDB;
    NSDictionary *drinkGirlData;
    NSDictionary *dataMenu ;
    
    
    typeDB = @"menu_0";
    [menuDB loadData:typeDB];
    if ([Check checkNull:menuDB.valueMenu]) {
        menuDB.valueMenu = nil;
    }
    NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    NSMutableDictionary *drinkMenu = [NSMutableDictionary dictionaryMutableWithObject:json[@"drink_menu"]];
    
    dataMenu  = json[@"menu"];
    drinkGirlData = json[@"drinkgirl"];
    NSString *keyInfoTmp;
    NSString *keyInfo;
    
    if ([data isKindOfClass:[NSDictionary class]]) {
         @try {
             
             if (false) {
                 addFoodBill = [SearchPromotion createBillPayment:data];
             }
             else{
                 NSDictionary *dic_1 = (NSDictionary *)data;
                 int count = 0;
                 NSInteger index = 0;
                 
                 NSInteger totalPassive = 0;
                 
                 NSDictionary *serviceInfo    = [NSDictionary dictionary];
                 NSDictionary *servicePer     = [NSDictionary dictionary];
                 NSDictionary *orders_info    = [NSDictionary dictionary];
                 NSArray *billList            = [NSArray array];
                 NSDictionary *totalList      = [NSDictionary dictionary];
                 NSDictionary *discountCoupon = [NSDictionary dictionary];
                 NSDictionary *totalVat       = [NSDictionary dictionary];
                 NSDictionary *vatPer         = [NSDictionary dictionary];
                 NSDictionary *discount       = [NSDictionary dictionary];
                 if (isPopupCashier) {
                     orders_info    = [NSDictionary dictionaryWithObject:dic_1[@"orders"]];
                     billList       = [NSArray arrayWithArray:dic_1[@"bill"]];
                     totalList      = [NSDictionary dictionaryWithObject:dic_1[@"total"]];
                     
                     NSString *value = @"id = %@";
                     id dataOrderBill = [NSDictionary searchDictionary:idBillOrder
                                                         andvalue:value
                                                            andData:billList];
                     if (![Check checkNull:dataOrderBill]) {
                         billList = [NSArray arrayWithObject:dataOrderBill];
                     }
                     else
                         billList = [NSArray array];
            
                 }
                 else{
                     serviceInfo    = [NSDictionary dictionaryWithObject:dic_1[@"service"]];
                     servicePer     = [NSDictionary dictionaryWithObject:dic_1[@"service_per"]];
                     orders_info    = [NSDictionary dictionaryWithObject:dic_1[@"orders_info"]];
                     billList       = [NSArray arrayWithArray:dic_1[@"bill_list"]];
                     totalList      = [NSDictionary dictionaryWithObject:dic_1[@"total"]];
                     discountCoupon = [NSDictionary dictionaryWithObject:dic_1[@"discount_coupon"]];
                     totalVat       = [NSDictionary dictionaryWithObject:dic_1[@"vat"]];
                     vatPer         = [NSDictionary dictionaryWithObject:dic_1[@"vat_per"]];
                     discount       = [NSDictionary dictionaryWithObject:dic_1[@"discount"]];
                 }
                 
                NSString *billVat = @"";
                 
                 for (id dataBill  in billList) {
                     NSMutableArray *dataProPassiveBill = [NSMutableArray array];
//                     NSMutableArray *addDataCoupon = [NSMutableArray array];
                     NSArray *orderInfo = [NSArray array];
                     NSString *ind;
                     NSString *key;
                     float totalCoupon  = 0.0; // ราคา coupon ในแต่บิล
                     @try {
                         if (isPopupCashier) {
                             key = [NSString checkNull:dataBill[@"id"]];
                         }
                         else{
                             key = [NSString checkNull:dataBill];
                         }
                         ind = [NSString stringWithFormat:@"bill_id_%@",key];
                         if (![Check checkNull:orders_info]){
                             orderInfo = [NSArray arrayWithArray:orders_info[ind]];
                             billVat   = [NSString checkNull:totalVat[ind]];
                         }
                     }
                     @catch (NSException *exception) {
                         DDLogError(@" exception orderInfo : %@",exception);
                     }
                     
                     index = [addFoodBill count];
                     [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count],   // จำนวน ราคาของบิลนั้น
                                                 @"index":@"titleSubBill",
                                                 @"vat":billVat}
                                       atIndex:index];
                     count ++;
                     keyInfoTmp = @"";
                     for (NSDictionary *dic in orderInfo) {
                         float type = [NSString checkNullConvertFloat:dic[@"type"]];
                         keyInfo = dic[@"promotion_id"];
                         NSDictionary *dataMenu_1;
                         NSDictionary *dataMenu_Pro;
                         
                         NSString *key = [NSString stringWithFormat:@"id_%@",dic[@"menu_id"]];
                         dataMenu_1 = dataMenu[key];
                         @try {
                             if (dataMenu_1 == nil) {
                                 if ([keyInfo intValue]!= 0 ){  //&& ![keyInfo isEqual:keyInfoTmp]) {   // เช็กโปรโมชั่น
                                     keyInfoTmp = keyInfo;
                                     NSString *value = @"id = %@";
                                     dataMenu_Pro = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                                         andvalue:value
                                                                                          andData:[promotion_info allValues]];
                                     dataMenu_1 = [dataMenu_Pro copy];
                                 }
                                 else if(type == 3 ) {
                                     dataMenu_1 = [NSDictionary dictionary];
                                 }
                                 else if(type == 4 ) {
                                     dataMenu_1 = [NSDictionary dictionary];
                                     NSString *total   = [NSString checkNull:dic[@"total"]];
                                     totalCoupon += [total floatValue];
                                 }
                                 else if([dic[@"menu_type"] intValue] == 8) {
                                     dataMenu_1 = [NSDictionary dictionaryWithObject:menuSpecialt[key]];
                                 }
                                 else
                                     continue;
                             }
                         }
                         @catch (NSException *exception) {
                             DDLogError(@" exception dataMenu_1 : %@",exception);
                         }
                         
                         NSString *count = [@([dic[@"n"] intValue] * [dic[@"price"] intValue]) stringValue];
                         NSMutableDictionary *_data = [NSMutableDictionary dictionary];
                         [_data setObject:dic[@"food_option"] forKey:@"Option"];
                         [_data setObject:dic[@"n"]  forKey:@"Count"];
                         [_data setObject:count      forKey:@"Price"];
                         [_data setObject:dataMenu_1 forKey:@"data"];
                         [_data setObject:dic        forKey:@"dataorder"];
                         
                         if ([dataMenu_Pro count] != 0) {
                             [_data setObject:dataMenu_Pro  forKey:@"dataPro"];
                         }
                         else{
                             [_data setObject:@""  forKey:@"dataPro"];
                         }
                         
                         if ([drinkGirlData count] != 0 && [SearchPromotion checkNull:[dataMenu_1 objectForKey:@"girl_id"]] && dataMenu_Pro == nil) {
                             NSString *idDrink = [NSString stringWithFormat:@"id_%@",dataMenu_1[@"girl_id"]];
                             if (drinkGirlData[idDrink] != nil) {
                                 [_data setObject:drinkGirlData[idDrink]  forKey:@"drinkGirl"];
                                 NSString *drinkID = [NSString checkNull:dic[@"shot_type"]];
                                 if (![drinkID isEqual:@"0"]) {
                                     NSDictionary *dataDrinkGirlList = drinkMenu[[NSString stringWithFormat:@"id_%@",drinkID]];
                                     [_data setObject:dataDrinkGirlList[@"name"]  forKey:@"TypeDrink"];
                                 }
                             }
                         }
                         
                         NSString *protypeLv1 = [NSString checkNull:dataMenu_1[@"protype_lv1"]];
                         NSString *giftType   = [NSString checkNull:dataMenu_1[@"gift_type"]];
                         
                         if ((type == 2) && ([protypeLv1 isEqualToString:@"2"] && [self checkGiftTypeDiscount:giftType])) {
                             
                             NSString *total   = [NSString checkNull:dic[@"total"]];
                             NSString *totalSub = @"0";
                             totalSub = [self getTotalGiftType:giftType and:dic_1];
                             totalPassive += [total integerValue];
                             [_data setObject:totalSub     forKey:@"subTotal"];
                             [_data setObject:@"dataProPassiveBill"  forKey:@"index"];
                             [dataProPassiveBill addObject:_data];
                         }
                         else
                             [addFoodBill addObject:_data];
                     }
                     index++;
                     
                     float totalService   ; // ราคา service ในแต่บิล
                     float totalPropassive; // ราคา ทั้งหมด ในแต่บิล
                     NSString  *serPer   ;
                     NSString  *discountText ;  // ราคารวม discount ในแต่บิล
                     NSString  *totalAllText ;
                     float totalSubBill = 0.0;
                     NSString  *percentVat;
                     
                     if (isPopupCashier) {
                         totalService    = [dataBill[@"service"] floatValue];// ราคา service ในแต่บิล
                         totalPropassive = [dataBill[@"total"] floatValue];// ราคา ทั้งหมด ในแต่บิล
                         serPer          = [NSString checkNull:dataBill[@"service_per"]];
                         billVat         = [NSString checkNull:dataBill[@"vat"]];
                         discountText    = @"0";
                         totalAllText    = [NSString checkNull:dataBill[@"total"]];
                         totalSubBill    = [totalAllText floatValue];
                         percentVat      = [NSString checkNull:dataBill[@"vat_per"]]; // percent Vat ในแต่บิล
                     }
                     else{
                         totalService    = [serviceInfo[ind] floatValue];// ราคา service ในแต่บิล
                         totalCoupon     = [discountCoupon[ind] floatValue];// ราคา coupon ในแต่บิล
                         totalPropassive = [totalList[ind] floatValue];// ราคา ทั้งหมด ในแต่บิล
                         serPer          = [NSString checkNull:servicePer[ind]];
                         discountText    = [NSString checkNull:discount[ind]];
                         totalAllText    = [NSString checkNull:totalList[ind]];
                         totalSubBill    = [totalList[ind] floatValue];
                         percentVat      = [NSString checkNull:vatPer[ind]];
                     }
                     float totalDiscount  =  totalCoupon + fabsf(totalService)  + [billVat floatValue] + totalPassive;
                     totalSubBill = totalSubBill - totalDiscount;
                     
                     if (index < [addFoodBill count]) {
                         
                         index = [addFoodBill count];
                         [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count], // หัวข้อบิล ที่..
                                                     @"index":@"dataSubBill",
                                                     @"total":[@(totalSubBill) stringValue]
                                                     }
                                           atIndex:index++];
                         if ([dataProPassiveBill count] > 0) {
                             for (NSDictionary *data in dataProPassiveBill) {
                                 [addFoodBill insertObject:data atIndex:index++];
                             }
                         }
                         
                         [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count], // ราคา service ในแต่บิล
                                                  @"index":@"serviceBill",
                                                  @"total":[@(totalService) stringValue],
                                                  @"per":serPer
                                                  }
                                           atIndex:index++];
                         
                         if (totalCoupon != 0) {  // ราคา coupon ในแต่บิล
                             [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count], // หัวข้อบิล ที่..
                                                      @"index":@"couponBill",
                                                      @"total":[@(totalCoupon) stringValue]
                                                      }
                                               atIndex:index++];
                         }
                         [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count],
                                                     @"index":@"totalBill",
                                                     @"total":[@(totalPropassive) stringValue],
                                                     @"discount":discountText,
                                                     @"totalAll":totalAllText,
                                                     @"vat":billVat,
                                                     @"coupon":[@(totalCoupon) stringValue],
                                                     @"percent_vat":percentVat
                                                     }
                                           atIndex:index++];

                     }
                     else{
                         [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count],
                                                  @"index":@"dataSubBill",
                                                  @"total":[@(totalSubBill) stringValue]
                                                  }];
                         if ([dataProPassiveBill count] > 0) {
                             [addFoodBill addObjectsFromArray:dataProPassiveBill];
                         }
                         
                         [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count], // ราคา service ในแต่บิล
                                                  @"index":@"serviceBill",
                                                  @"total":[@(totalService) stringValue],
                                                  @"per":serPer
                                                  }
                          ];
                         if (totalCoupon != 0) { // ราคา coupon ในแต่บิล
                             [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count], // หัวข้อบิล ที่..
                                                      @"index":@"couponBill",
                                                      @"total":[@(totalCoupon) stringValue]
                                                      }];
                         }
                         [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count],
                                                  @"index":@"totalBill",
                                                  @"total":[@(totalPropassive) stringValue],
                                                  @"discount":discountText,
                                                  @"totalAll":totalAllText,
                                                  @"vat":billVat,
                                                  @"coupon":[@(totalCoupon) stringValue],
                                                  @"percent_vat":percentVat
                                                  }
                          ];
                        index += 2;
                     }
                   
                 }
             }
        }
        @catch (NSException *exception) {
            DDLogError(@" 1. exception addBillData : %@",exception);
        }
    }
    else{
        @try {
            for (NSDictionary *dic in data) {
                keyInfo = dic[@"promotion_id"];
                NSDictionary *dataMenu_1;
                NSDictionary *dataMenu_Pro;
                
                NSString *key = [NSString stringWithFormat:@"id_%@",dic[@"menu_id"]];
                dataMenu_1 = dataMenu[key];
                
                if (dataMenu_1 == nil) {
                    if (![self checkNull:keyInfo]|| ![self checkNull:dic[@"menu_id"]]) {
                        continue;
                    }
//                    if ([keyInfo intValue]!= 0 && ![keyInfo isEqual:keyInfoTmp] && promotion_info) { // เช็กโปรโมชั่น
                    if ([keyInfo intValue] != 0 && promotion_info) { // เช็กโปรโมชั่น
                        keyInfoTmp = keyInfo;
                        NSString *value = @"id = %@";
                        dataMenu_Pro = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                            andvalue:value
                                                                             andData:[promotion_info allValues]];
                        dataMenu_1 = [dataMenu_Pro copy];
                        dataMenu_Pro = nil;
                        if (dataMenu_1 == nil) {
                            continue;
                        }
                    }
                    else if([dic[@"type"] intValue] == 3 || [dic[@"type"] intValue] == 4) {
                        dataMenu_1 = [NSDictionary dictionary];
                    }
                    else if([dic[@"menu_type"] intValue] == 8) {
                        dataMenu_1 = [NSDictionary dictionaryWithObject:menuSpecialt[key]];
                    }
                    else
                        continue;
                }
                else if(![keyInfo isEqualToString:@"0"]){
                    keyInfoTmp = keyInfo;
                    NSString *value = @"id = %@";
                    NSArray *info = [promotion_info allValues];
                    dataMenu_Pro = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                        andvalue:value
                                                                         andData:info];
                }
                
                NSString *count = [@([dic[@"n"] intValue] * [dic[@"price"] intValue]) stringValue];
                NSMutableDictionary *_data = [NSMutableDictionary dictionary];
                [_data setObject:dic[@"food_option"] forKey:@"Option"];
                [_data setObject:dic[@"n"]  forKey:@"Count"];
                [_data setObject:count      forKey:@"Price"];
                [_data setObject:dataMenu_1 forKey:@"data"];
                [_data setObject:dic        forKey:@"dataorder"];
                
                if ([dataMenu_Pro count] != 0) {
                     [_data setObject:dataMenu_Pro  forKey:@"dataPro"];
                }
                else{
                    [_data setObject:@""  forKey:@"dataPro"];
                }
                
                if ([drinkGirlData count]!= 0 && [SearchPromotion checkNull:[dataMenu_1 objectForKey:@"girl_id"]] && dataMenu_Pro == nil) {
                    NSString *idDrink = [NSString stringWithFormat:@"id_%@",dataMenu_1[@"girl_id"]];
                    if (drinkGirlData[idDrink] != nil) {
                        [_data setObject:drinkGirlData[idDrink]  forKey:@"drinkGirl"];
                        NSString *drinkID = [NSString checkNull:dic[@"shot_type"]];
                        if (![drinkID isEqual:@"0"]) {
                            NSDictionary *dataDrinkGirlList = drinkMenu[[NSString stringWithFormat:@"id_%@",drinkID]];
                            [_data setObject:dataDrinkGirlList[@"name"]  forKey:@"TypeDrink"];
                        }
                    }
                }
                [addFoodBill addObject:_data];
                
            }
        }
        @catch (NSException *exception) {
            DDLogError(@" 2. exception addBillData : %@",exception);
        }
    }

    return addFoodBill;
}
+ (NSMutableArray *)createBillPayment:(NSMutableArray *)data {
    
    NSMutableArray *addFoodBill = [NSMutableArray array];
    MenuDB *menuDB = [MenuDB getInstance];
    
    NSString *typeDB;
    NSDictionary *drinkGirlData;
    NSDictionary *dataMenu ;
    
    typeDB = @"menu_0";
    [menuDB loadData:typeDB];
    NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    NSMutableDictionary *drinkMenu = [NSMutableDictionary dictionaryMutableWithObject:json[@"drink_menu"]];
    
    dataMenu  = json[@"menu"];
    drinkGirlData = json[@"drinkgirl"];
    NSString *keyInfoTmp;
    NSString *keyInfo;
    
    @try {
        NSDictionary *dic_1 = (NSDictionary *)data;
        int count = 0;
        int i = 0;
//        NSInteger price = 0;
//        NSInteger discount = 0;
////        NSInteger totalBill = 0;
//        NSInteger servicePer = 0;
//        NSInteger totalService = 0;
        
//        NSDictionary *orders_info = dic_1[@"orders"];
//        NSArray *bill_list        = dic_1[@"bill"];
//        NSDictionary *totalList   = dic_1[@"total"];
        
        NSDictionary *serviceInfo    = [NSDictionary dictionaryWithObject:dic_1[@"service"]];
        NSDictionary *servicePer     = [NSDictionary dictionaryWithObject:dic_1[@"service_per"]];
        NSDictionary *orders_info    = [NSDictionary dictionaryWithObject:dic_1[@"orders"]];
        NSArray *billList            = [NSArray arrayWithArray:dic_1[@"bill"]];
        NSDictionary *totalList      = [NSDictionary dictionaryWithObject:dic_1[@"total"]];
        NSDictionary *discountCoupon = [NSDictionary dictionaryWithObject:dic_1[@"discount_coupon"]];
        NSDictionary *discount       = [NSDictionary dictionaryWithObject:dic_1[@"discount"]];
        NSString *billVat = @"";
        
        for (NSDictionary *dataBill  in billList) {
            
            //NSLog(@"bill_list = %@",dataBill);
//            price = 0;
//            discount = 0;
////            totalBill = 0;
//            servicePer = 0;
//            totalService = 0;
            
//            NSDictionary *dataBillInfo;
            NSArray *orderInfo;
            NSString *ind;
            @try {
                NSString *key = dataBill[@"id"];
                ind = [NSString stringWithFormat:@"bill_id_%@",key];
                if ([orders_info  count] == 0) continue;
                orderInfo = orders_info[ind];
//                billVat = [NSString checkNull:totalVat[ind]];
//                NSString *idBill = [@"id_" stringByAppendingString:key];
//                dataBillInfo = [NSDictionary dictionaryWithObject:bill_info[idBill]];
//                if (![Check checkNull:dataBillInfo]) { // เปอร์เช็น setvice
//                    servicePer   = [dataBillInfo[@"service_per"] integerValue];
//                }
//                totalService = [serviceInfo[ind] integerValue]; // ราคา service ในแต่บิล
            }
            @catch (NSException *exception) {
                DDLogError(@" exception orderInfo : %@",exception);
            }
            
            if ([billList count] >= 2 ) {   // titleSubBill
                [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count],
                                            @"index":@"titleSubBill",
                                            @"vat":billVat}
                                  atIndex:i];
            }

            count ++;
            //                //NSLog(@"count order %lu :",[orderInfo count]);
            keyInfoTmp = @"";
            for (NSDictionary *dic in orderInfo) {
                //NSLog(@"dataorder %d  : %@ ",i, dic);
                
                i++;
                keyInfo = dic[@"promotion_id"];
                NSDictionary *dataMenu_1;
                NSDictionary *dataMenu_Pro;
                
                NSString *key = [NSString stringWithFormat:@"id_%@",dic[@"menu_id"]];
                dataMenu_1 = dataMenu[key];
                @try {
                    if (dataMenu_1 == nil) {
                        if ([keyInfo intValue]!= 0 ){   // เช็กโปรโมชั่น
                            keyInfoTmp = keyInfo;
                            NSString *value = @"id = %@";
                            dataMenu_Pro = (NSDictionary*)[NSDictionary searchDictionary:keyInfo
                                                                                andvalue:value
                                                                                 andData:[promotion_info allValues]];
                            dataMenu_1 = [dataMenu_Pro copy];
                        }
                        else if([dic[@"type"] intValue] == 3 || [dic[@"type"] intValue] == 4) {
                            dataMenu_1 = [NSDictionary dictionary];
                        }
                        else if([dic[@"menu_type"] intValue] == 8) {
                            dataMenu_1 = [NSDictionary dictionaryWithObject:menuSpecialt[key]];
                        }
                        else
                            continue;
                    }
                }
                @catch (NSException *exception) {
                    DDLogError(@" exception dataMenu_1 : %@",exception);
                }
                
                NSString *count = [@([dic[@"n"] intValue] * [dic[@"price"] intValue]) stringValue];
                NSMutableDictionary *_data = [NSMutableDictionary dictionary];
                [_data setObject:dic[@"food_option"] forKey:@"Option"];
                [_data setObject:dic[@"n"]  forKey:@"Count"];
                [_data setObject:count      forKey:@"Price"];
                [_data setObject:dataMenu_1 forKey:@"data"];
                [_data setObject:dic        forKey:@"dataorder"];
                
                if ([dataMenu_Pro count] != 0) {
                    [_data setObject:dataMenu_Pro  forKey:@"dataPro"];
                }
                else{
                    [_data setObject:@""  forKey:@"dataPro"];
                }
                
                if ([drinkGirlData count] != 0 && [SearchPromotion checkNull:[dataMenu_1 objectForKey:@"girl_id"]] && dataMenu_Pro == nil) {
                    NSString *idDrink = [NSString stringWithFormat:@"id_%@",dataMenu_1[@"girl_id"]];
                    if (drinkGirlData[idDrink] != nil) {
                        [_data setObject:drinkGirlData[idDrink]  forKey:@"drinkGirl"];
                        NSString *drinkID = [NSString checkNull:dic[@"shot_type"]];
                        if (![drinkID isEqual:@"0"]) {
                            NSDictionary *dataDrinkGirlList = drinkMenu[[NSString stringWithFormat:@"id_%@",drinkID]];
                            [_data setObject:dataDrinkGirlList[@"name"]  forKey:@"TypeDrink"];
                        }
                    }
                    
                }
                [addFoodBill addObject:_data];
                
//                NSInteger p2 =  [dic[@"total"] integerValue];
//                if (p2 < 0) {
//                    discount = p2 + discount;
//                }
//                else{
//                    price = p2 + price;
////                    total += p2;
//                }
            }
            
            i++;
//            if (i < [addFoodBill count]) {
//                [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count],
//                                            @"index":@"*888",
//                                            @"total":[totalList[ind] stringValue],
//                                            @"discount":[@(discount) stringValue],
//                                            @"totalAll":dataBill[@"total"],
//                                            @"vat":dataBill[@"vat"],
//                                            @"service":@{@"total":[NSNumber numberWithInteger:totalService],
//                                                         @"per":[NSNumber numberWithInteger:servicePer]
//                                                         }
//                                            }
//                                  atIndex:i];
//            }
//            else{
//                [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count],
//                                         @"index":@"*888",
//                                         @"total":[totalList[ind] stringValue],
//                                         @"discount":[@(discount) stringValue],
//                                         @"totalAll":dataBill[@"total"],
//                                         @"vat":dataBill[@"vat"],
//                                         @"service":@{@"total":[NSNumber numberWithInteger:totalService],
//                                                      @"per":[NSNumber numberWithInteger:servicePer]
//                                                      }
//                                         }];
//            }
            i++;
            if (i < [addFoodBill count]) {
                [addFoodBill insertObject:@{@"bill":[NSNumber numberWithInt:count],
                                            @"index":@"totalBill",
                                            @"total":[totalList[ind] stringValue],
                                            @"discount":[discount[ind] stringValue],
                                            @"totalAll":[totalList[ind] stringValue],
                                            @"vat":billVat,
                                            @"service":@{@"total":[serviceInfo[ind] stringValue],
                                                         @"per":[servicePer[ind] stringValue]
                                                         },
                                            @"coupon":[discountCoupon[ind] stringValue]
                                            }
                                  atIndex:i];
            }
            else{
                [addFoodBill addObject:@{@"bill":[NSNumber numberWithInt:count],
                                         @"index":@"totalBill",
                                         @"total":[totalList[ind] stringValue],
                                         @"discount":[discount[ind] stringValue],
                                         @"totalAll":[totalList[ind] stringValue],
                                         @"vat":billVat,
                                         @"service":@{@"total":[serviceInfo[ind] stringValue],
                                                      @"per":[servicePer[ind] stringValue]
                                                      },
                                         @"coupon":[discountCoupon[ind] stringValue]
                                         }];
            }
            i++;
        }
        return addFoodBill;
    }
    @catch (NSException *exception) {
        DDLogError(@" 1. exception createBillPayment : %@",exception);
    }
}
+(BOOL)checkNull:(NSString *)object{
    if([object isKindOfClass:[NSNull class]])
        return NO;
    else
        return YES;
}
+(BOOL)checkGiftTypeDiscount:(NSString *)num{ // menu_type
    switch ([num intValue]) {
        case 4: // buffet
        case 5: // discount for all orders in bill
        case 6: // discount for drink only in bill
        case 7: // discount for mixer only in bill
            return true;
            break;
        default:
            return false;
            break;
    }
}
+(BOOL)checkGiftType:(NSString *)num{ // menu_type
    switch ([num intValue]) {
        case 1: // food
        case 2: // drink
        case 3: // drinkgirl
            return true;
            break;
        default:
            return false;
            break;
    }
}
+(NSString *)getTotalGiftType:(NSString *)num and:(NSDictionary *)data{
    NSString *key = @"";
    switch ([num intValue]) {
        case 4: key = @"sub_total";
        case 5: key = @"sub_total_drink";
        case 6: key = @"sub_total_mixer";
        case 7: key = @"sub_total_food";
            NSDictionary *totalData = [NSDictionary dictionaryWithObject:data[key]];
            NSString *total = [NSString checkNull:totalData[@"total"]];
            if ([Check checkNull:total]) {
                total = @"0";
            }
            return total;
        break;
    }
    return @"0";
}
@end
