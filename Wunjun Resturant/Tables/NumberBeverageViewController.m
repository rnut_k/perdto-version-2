//
//  NumberBeverageViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "NumberBeverageViewController.h"
#import "GlobalBill.h"
#import "Constants.h"
#import "CNPPopupController.h"
#import "TableBeverageListViewController.h"
#import "TableSubBillViewController.h"

@interface NumberBeverageViewController()<CNPPopupControllerDelegate>{
    GlobalBill *globalBill;
    LoadProcess *loadProgress;
}
@property (nonatomic, strong) CNPPopupController *popupController;
@end
@implementation NumberBeverageViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    loadProgress = [LoadProcess sharedInstance];
    
    globalBill = [GlobalBill sharedInstance];
    bool blu = [globalBill.billResturant[@"use_sub_bill"] boolValue];
    
    if (!blu) {
        globalBill.billType = kSingleBill;
    }
    self.mTitleItem.text = self.strTitleBeverage;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCilckAdd:(id)sender {
    NSLog(@"clickOn");
    int num = [self.mCountNumber.text intValue];
    self.mCountNumber.text = [@(++num) stringValue];
}

- (IBAction)btnCilckSub:(id)sender {
    
    NSLog(@"clickDown");
    int num = [self.mCountNumber.text intValue];
    num = (num != 0)?--num:0;
    self.mCountNumber.text = [@(num) stringValue];
}

- (IBAction)btnReply:(id)sender {
    if (![self.mCountNumber.text isEqual:@"0"]) {
        [loadProgress.loadView showWaitProgress];
        
        TableBeverageListViewController *myController = (TableBeverageListViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        if (![myController isKindOfClass:[TableBeverageListViewController class]]) {
            myController = (TableBeverageListViewController *)[self.navigationController.viewControllers objectAtIndex:3];
        }
        
        NSString *count = self.mCountNumber.text;
        [myController addOrderAppvoceoption:self.dataMenuList option:@"" comment:@"" count:[count intValue]];
        [myController.mTableTagDrink reloadData];
        [self.navigationController popToViewController:myController animated:NO];
    }
    else{
       [loadProgress.loadView showWithStatusTitle];
    }
}
- (IBAction)btnBackHome:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
@end
