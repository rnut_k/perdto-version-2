//
//  TableSingleBillViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "TableDataBillViewController.h"
#import "CNPPopupController.h"

@interface TableSingleBillViewController : UIViewController<CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *mContainerTableBill;
@property (weak, nonatomic) IBOutlet UIView *mContainerMenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeButtonCheckBill;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizePassivePromotion;
@property (weak, nonatomic) IBOutlet UIButton *mPassivePro;

@property (nonatomic) BOOL checkOpenTable;
@property (nonatomic,strong) NSDictionary   *detailTable;
@property (nonatomic,strong) NSDictionary   *dataListSingBill;
@property (nonatomic,strong) NSString       *titleTable;
@property (nonatomic,strong) NSString       *numberTable;
@property (nonatomic,strong) NSMutableArray *splitDataList;
@property (nonatomic,strong) NSString       *indexSplitBill;
@property (nonatomic,strong) NSMutableArray *listTitleSubBill;
@property (nonatomic,strong) NSMutableArray *passivePromotion;

@property (nonatomic,strong) TableDataBillViewController  *tableDataBillView;
@property (nonatomic,strong) id  tableSuperClass;
@property (nonatomic) bool isCashier;
@property (nonatomic) bool isCashTableStatus;
@property (nonatomic) bool isPopup;
@property (nonatomic) bool isPopupCashier;
@property (nonatomic) BOOL isCashierTake;
@property (nonatomic) BOOL isStaffTake;
@property (nonatomic) BOOL isPrintSubBill;
@property (nonatomic) bool isPassivePromotion;
@property (nonatomic) bool isDeleteBill;

@property (nonatomic,strong) NSString *role;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarBtn;
@property (weak, nonatomic) IBOutlet UIButton *checkBillBtn;

- (IBAction)btnRequireCheckBill:(id)sender;
- (IBAction)btnCheckBill:(id)sender;
- (IBAction)btnBackHome:(id)sender;
- (IBAction)btnGetPro:(id)sender;
- (IBAction)btnPasssivePromotion:(id)sender;

-(void) setLoadDataTable;
+(void)loadDataOpenTableSing:(NSString *)idTable;
+(void)loadDataOpenTableCashier:(NSString *)idTable ;
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender;
-(void) printSubBillView:(int)num;
-(void) setTableBill:(id )_dataOrders and:(NSMutableArray *)_pendingPrders andPro:(NSMutableDictionary *)datapro andIDPool:(NSString *)idPool;

+ (void)resetBillSeclectPassivePro:(NSString *)idBill;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title ;

-(void) printCheckBillView:(BOOL)blLoop;

@end
