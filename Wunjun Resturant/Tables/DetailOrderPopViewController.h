//
//  DetailOrderPopViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableDataBillViewController.h"

@interface DetailOrderPopViewController : UIViewController<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *mContainerType;
@property (weak, nonatomic) IBOutlet UILabel *mTitlePro;
@property (weak, nonatomic) IBOutlet UIImageView *mImageTitle;
@property (weak, nonatomic) IBOutlet UILabel *mIdOrder;
@property (weak, nonatomic) IBOutlet UILabel *mNameOrder;
@property (weak, nonatomic) IBOutlet UILabel *mCountOrder;
@property (weak, nonatomic) IBOutlet UILabel *mSaleOrder;
@property (weak, nonatomic) IBOutlet UILabel *mStatusOrder;
@property (weak, nonatomic) IBOutlet UILabel *mListPromotion;
@property (strong, nonatomic)IBOutlet UIScrollView *mScrollView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mContanierView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutContainerType;

- (IBAction)btnClosePop:(id)sender;

@property (nonatomic ,strong) TableDataBillViewController* tableDataBillView;
@property (nonatomic ,strong) NSDictionary *dataMenu;
@property (nonatomic ,strong) NSDictionary *dataOrder;
@property (nonatomic ,strong) UITableView  *tableContainer;
@property (nonatomic ,strong) NSString     *typeEditCashier;

- (IBAction)getImageBtnPressed:(UIImage *)sender;
- (IBAction)cancelOrder:(id)sender;
@end
