//
//  CellCollectionTableView.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CellCollectionTableView.h"

@implementation CellCollectionTableView


- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.collectionViewLayout;
}
- (void)viewDidLoad
{
    self.layout.numberOfItemsPerLine = 6;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.layout.interitemSpacing = 10;
    self.layout.lineSpacing = 10;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 25;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.userNameLabel.text = [@(indexPath.row) stringValue];
    cell.contentView.backgroundColor = [UIColor blueColor];
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor yellowColor];
}
@end
