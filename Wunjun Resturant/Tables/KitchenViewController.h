//
//  KitchenViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/17/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "OrdersListViewController.h"
#import "OrdersKitchen.h"

@interface KitchenViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableKitchenList;

@end
