//
//  AdminPendingViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AdminPendingViewController.h"
#import "Check.h"

@interface AdminPendingViewController ()
{
    THSegmentedPager *pager;
    Member *member;
}
@end

@implementation AdminPendingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self setIndexPage:[pager.pageControl selectedSegmentIndex]];
    
//    
    pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    NSMutableArray *pages = [NSMutableArray new];
    
    member = [Member getInstance];
    
    LoadProcess *loadProgress  = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
    NSDictionary *parameters = @{
                                 @"code":member.code
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_COUNTER_PENDING,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSMutableArray *counters = [[NSMutableArray alloc]init];
            
            [counters addObject:json[@"data"][@"promotion"][@"count"]];
            [counters addObject:json[@"data"][@"invoice"][@"count"]];
            [counters addObject:json[@"data"][@"income"][@"count"]];
            
            NSArray *pendingList = @[
                                     @{
                                         @"title":@"Staff",
                                         @"type":@"1",
                                         @"count":@"promotion"
                                         },
                                     @{
                                         @"title":@"Checker",
                                         @"type":@"2",
                                         @"count":@"invoice"
                                         },
                                     @{
                                         @"title":@"Cashier",
                                         @"type":@"3",
                                         @"count":@"income"
                                         }
                                     ];
            
            for (int i = 0; i < [pendingList count]; i++) {
                TablePendingViewController *tableView = [pager.storyboard instantiateViewControllerWithIdentifier:@"TablePending"];
                [pages addObject:tableView];
                [tableView setViewTitle:pendingList[i][@"title"]];
                [tableView setType:i+1];
                [tableView setCounterData:json[@"data"][pendingList[i][@"count"]]];
            }
            [pager setPages:pages];
            [pager setCounters:counters];
            
            pager.view.frame = self.view.bounds;
            [self.view addSubview:pager.view];
            [self addChildViewController:pager];
            [pager didMoveToParentViewController:self];
            
            [LoadProcess dismissLoad];
        }
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getViewPending:)
                                                 name:@"NotiupdataPending" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCounter:)
                                                 name:@"UIApplicationSetCounter" object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"UIApplicationSetCounter" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void) setCounter:(id) sender
{
    member = [Member getInstance];
    NSDictionary *parameters = @{
                                 @"code":member.code
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_COUNTER_PENDING,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSMutableArray *counters = [[NSMutableArray alloc]init];
            
            [counters addObject:json[@"data"][@"promotion"][@"count"]];
            [counters addObject:json[@"data"][@"invoice"][@"count"]];
            [counters addObject:json[@"data"][@"income"][@"count"]];
            NSLog(@"%@", pager.counters);
            [pager setCounters:counters];
            [pager updateTitleLabels];
        }
        [LoadProcess dismissLoad];
        
    }];
    
}
-(void) getViewPending:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    if (![Check checkNull:userInfo]) {
        NSInteger num = [userInfo[@"a"] integerValue];
        int type = [self setTypePending:num];
        int indexPage = type - 1;
        
        NSMutableArray *counters = [pager counters];
        NSNumber *numPager = [NSNumber numberWithInt:[counters[indexPage] intValue]+1];
        [counters replaceObjectAtIndex:indexPage withObject:numPager];
        [pager updateTitleLabels];
        
        [pager setSelectedPageIndex:indexPage animated:YES];
        TablePendingViewController *tableView = (TablePendingViewController *)pager.selectedController;
        [tableView setType:type];
        [tableView updataPending:notification];
        
        [self setIndexPage:indexPage];
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(int ) setTypePending:(NSInteger)num{
    switch (num) {
        case 21: // staff   ร้องขออนุมัติโปรโมชัน => admin แสดง notification หรือ อัพเทด list pending
        {
         
            return 1;
        }
            break;
        case 31: // checker ร้องขออนุมัติใบสั่งสินค้า => admin แสดง notification หรือ อัพเทด list pending
        case 50: // checker ส่งรายงาน stock => admin แสดง notification หรือ อัพเทด list pending
        {
            return 2;
        }
            break;
        case 40: // cashier ส่งยอด => admin แสดง notification หรือ อัพเทด list pending
        case 41: // cashier ร้องขอการอนุมัติส่วนลดเพิ่มเติม => admin แสดง notification หรือ อัพเทด list pending
        {
            return 3;
        }
            break;
        default:
            return 0;
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
