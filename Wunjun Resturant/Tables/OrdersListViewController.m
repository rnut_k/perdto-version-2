//
//  OrdersListViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "OrdersListViewController.h"
#import "Check.h"
#import "PopupView.h"

@interface OrdersListViewController ()

@end

@implementation OrdersListViewController{
    LoadProcess *loadProgress;
    Member *member;
    MenuDB *menuDB;
    NSDictionary *menuData;
    NSString *deliverStatus;
    WYPopoverController *popoverController;
    NSMutableArray *rowData;
    NSMutableArray *fullRowData;
    THSegmentedPager *pager;
    NSArray *dataTab;
    NSDictionary *tagData;
    UITableView *tableList;
    
    NSDictionary *editBlockData;
    UIButton *tagBtn;
    
    PopupView  *popupView;
    
    OrderListKitchenViewController *OrderListKitchenView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadProgress   = [LoadProcess sharedInstance];
    member = [Member getInstance];
    
    [self setTitle:self.kitchenName];
    
    editBlockData = [[[NSDictionary alloc]init] mutableCopy];
    if([self.role isEqualToString:@"kitchen"]){
        [self popUpStock];
    }
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    dataTab = @[
                                @{
                                    @"deliver_status":@"0",
                                    @"title":AMLocalizedString(@"รอการทำ", nil)
                                    },
                                @{
                                    @"deliver_status":@"1",
                                    @"title":AMLocalizedString(@"ทั้งหมด", nil)
                                    }
                                ];
//    dataTab = @[
//                @{
//                    @"deliver_status":@"0",
//                    @"title":@"ทั้งหมด"
//                    },
//                @{
//                    @"deliver_status":@"1",
//                    @"title":@"รอการทำ"
//                    },
//                @{
//                    @"deliver_status":@"3",
//                    @"title":@"นำเสิร์ฟ"
//                    },
//                @{
//                    @"deliver_status":@"4",
//                    @"title":@"ไม่"
//                    }
//                ];
    [self createViewTab:dataTab];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self setLineView:self.selectModePrint];
    [self.selectModePrint addTarget:self action:@selector(selectModePrint:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setLineView:self.printAll];
    [self.printAll addTarget:self action:@selector(printAll:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setLineView:self.printOne];
    [self.printOne addTarget:self action:@selector(printOne:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateTabKitchen:)
                                                 name:@"UIApplicationUpdateTabKitchen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateKitchenTable:)
                                                 name:@"NotisetOrdersKitchen" object:nil];
}
-(void)updateKitchenTable:(id)sender{
    OrderListKitchenViewController *obj = (OrderListKitchenViewController *)pager.selectedController;
    [obj updateTable:sender];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UIApplicationUpdateTabKitchen" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotisetOrdersKitchen" object:nil];
}
-(void) createViewTab:(NSArray *)_dataTab {
    
    OrdersKitchen *oKit = [OrdersKitchen getInstance];
    NSDictionary *parameters = @{
                                 @"code":member.code,
                                 @"kitchen_id":self.kitchenId,
                                 @"deliver_status":@"0"
                                 };
    
    NSString *url = [NSString stringWithFormat:GET_ORDERS_KITCHEN,member.nre];
    [loadProgress.loadView showWithStatus];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue])
        {
            NSDictionary *data = json[@"data"];
            oKit.maxPrevOrderId = data[@"max_prev_order_id"];
            oKit.orders = [data[@"orders"] mutableCopy];
            
            menuDB = [MenuDB getInstance];
            [menuDB loadData:@"menu_0"];
            NSDictionary *database = [NSDictionary dictionaryWithString:menuDB.valueMenu];
            NSDictionary *menu = [database[@"menu"] mutableCopy];
            tagData = [[NSDictionary alloc] initWithDictionary:database[@"tag"]];
            
            for(NSString *key in data[@"menu"]){
                if(menu[key] == nil)
                    [menu setValue:data[@"menu"][key] forKey:key];
            }
            oKit.menu = menu;
            
            [oKit sortOrdersData];
            
            DeviceData *deviceData = [DeviceData getInstance];
            if([deviceData.tables[@"tables"]count] > 0){
                NSDictionary *tableDict = [[[NSDictionary alloc]init] mutableCopy];
                for(NSDictionary *tableData in deviceData.tables[@"tables"]){
                    [tableDict setValue:tableData forKey:[NSString stringWithFormat:@"id_%@",tableData[@"id"]]];
                }
                oKit.tables = tableDict;
            }
            oKit.zones = deviceData.tables[@"zones"];
            
//            
            pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
            NSMutableArray *pages = [NSMutableArray new];
            NSMutableArray *counters = [[NSMutableArray alloc]init];
            for (int i = 0; i < [_dataTab count]; i++) {
                OrderListKitchenViewController *tableView = [pager.storyboard instantiateViewControllerWithIdentifier:@"OrderListKitchen"];
                [pages addObject:tableView];
                [tableView setBlClikeChecker:self.blClikeChecker];
                [tableView setKitchenId:self.kitchenId];
                [tableView setTitle:_dataTab[i][@"title"]];
                [tableView setViewTitle:_dataTab[i][@"title"]];
                [tableView setDStatus:_dataTab[i][@"deliver_status"]];
                [tableView setTypePrintCut:self.typePrintCut];
                [tableView setMaxPrevBillId:data[@"max_prev_bill_id"]];
                
                NSDictionary *forders = oKit.fetchOrders[_dataTab[i][@"deliver_status"]];
                [counters addObject:[NSString stringWithFormat:@"%lu", (unsigned long)[forders count]]];
            }            
            [pager setPages:pages];
            [pager setCounters:counters];
            
            CGRect rect = self.view.bounds;
            rect.size.height = self.tabContainer.frame.size.height;
            
            pager.view.frame = rect;
            pager.view.bounds = rect;
            [self.tabContainer addSubview:pager.view];
            
            [self addChildViewController:pager];
            [pager didMoveToParentViewController:self];
            
            [self setOrderListKitchenView:@"ไม่เช็ค"];
        }
         [LoadProcess  dismissLoad];
    }];
}

- (void) updateTabKitchen:(id)sender{
    OrdersKitchen *oKit = [OrdersKitchen getInstance];
    NSMutableArray *counters = [[NSMutableArray alloc]init];
    for (int i = 0; i < [dataTab count]; i++) {
        NSDictionary *forders = oKit.fetchOrders[dataTab[i][@"deliver_status"]];
        [counters addObject:[NSString stringWithFormat:@"%lu", (unsigned long)[forders count]]];
    }
    [pager setCounters:counters];
    [pager updateTitleLabels];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark popup
- (IBAction)actionBlockBtn:(id)sender{
    [self popUpStock];
}
- (void)popUpStock{
    editBlockData = [[[NSDictionary alloc]init] mutableCopy];
    
    menuDB = [MenuDB getInstance];
    [menuDB loadData:@"menu_0"];
    
    menuData = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    menuData = menuData[@"menu"];
    
    UIViewController  *viewController = [[UIViewController alloc] init];
    viewController.title = AMLocalizedString(@"Block เมนู", nil);
    
    CGSize size;
    if (![Check  checkDevice]) {
        size = CGSizeMake(320,320);
    }
    else{
        size = CGSizeMake(self.view.frame.size.width-50,320);
    }
    
    viewController.preferredContentSize = CGSizeMake(size.width,size.height);
    viewController.modalInPopover = NO;
    
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    size = popoverController.popoverContentSize;
    
    tagBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 2.5, size.width/2, 35)];
    [tagBtn setTitle:AMLocalizedString(@"ทั้งหมด", nil) forState:UIControlStateNormal];
//    [tagBtn setBackgroundColor:[UIColor ht_carrotColor]];
    [tagBtn addTarget:self action:@selector(alertTag) forControlEvents:UIControlEventTouchUpInside];
    tagBtn.layer.cornerRadius = 4;
    [tagBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [tagBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    [tagBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    
    UIButton *blockAllBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 40, 100, 40)];
    [blockAllBtn setTitle:AMLocalizedString(@"ทั้งหมด", nil) forState:UIControlStateNormal];
    [blockAllBtn addTarget:self action:@selector(checkItAll:) forControlEvents:UIControlEventTouchUpInside];
    blockAllBtn.tag = 0;
    [blockAllBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [blockAllBtn setBackgroundColor:[UIColor whiteColor]];
    [blockAllBtn setImage:[UIImage imageNamed:@"c_ic_check"] forState:UIControlStateNormal];
    [blockAllBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    UIButton *saveblockBtn = [[UIButton alloc]initWithFrame:CGRectMake(size.width/2, 2.5, size.width/2 - 5, 35)];
    [saveblockBtn setTitle:AMLocalizedString(@"บันทึก", nil) forState:UIControlStateNormal];
    [saveblockBtn addTarget:self action:@selector(saveItAll:) forControlEvents:UIControlEventTouchUpInside];
    [saveblockBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveblockBtn setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [saveblockBtn setBackgroundColor:[UIColor ht_grassColor]];
    saveblockBtn.layer.cornerRadius = 4;
    [saveblockBtn.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [saveblockBtn setImage:[UIImage imageNamed:@"ic_sign_done"] forState:UIControlStateNormal];
    
    [viewController.view addSubview:tagBtn];
    [viewController.view addSubview:blockAllBtn];
    [viewController.view addSubview:saveblockBtn];
    viewController.view.backgroundColor = [UIColor whiteColor];
    tableList = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, size.width, size.height-45-40-40)];
    tableList.delegate = self;
    tableList.dataSource = self;
    [viewController.view addSubview:tableList];
    NSMutableArray *menuByKId = [[NSMutableArray alloc]init];
    for(NSString *k in menuData){
        if(![menuData[k][@"kitchen_id"] isKindOfClass:[NSNull class]]){
            if([menuData[k][@"kitchen_id"] isEqualToString:self.kitchenId]){
                [menuByKId addObject:menuData[k]];
            }
        }
    }
    BOOL reverseSort = YES;
    rowData = [[NSMutableArray alloc]initWithArray:[menuByKId sortedArrayUsingFunction:isBlockSort
                                                                               context:&reverseSort]];
    fullRowData = [rowData copy];
    
    [tableList reloadData];
}

#pragma mark alert view tag
- (void) alertTag
{
    
    UIAlertView *tagAlert = [[UIAlertView alloc]
                             initWithTitle:AMLocalizedString(@"ค้นหาจากแท๊ก", nil)
                             message:@""
                             delegate:self
                             cancelButtonTitle:AMLocalizedString(@"ปิด", nil)
                             otherButtonTitles:AMLocalizedString(@"ทั้งหมด", nil), nil
                             ];
    
    NSString *type;
    NSMutableArray *tagIndex = [[NSMutableArray alloc]init];
    NSDictionary *tagWithKey = [[[NSDictionary alloc]init] mutableCopy];
    NSString *idKey;
    for (NSDictionary *menu in fullRowData) {
        if([menu[@"type"] intValue] == 1){
            type = @"food";
        }
        else if([menu[@"type"] intValue] == 2){
            type = @"drink";
        }
        else
        {
            continue;
        }
        
        if(menu[@"tag1"] != nil && ![menu[@"tag1"] isKindOfClass:[NSNull class]] && ![menu[@"tag1"] isEqualToString:@""])
        {
            idKey = [NSString stringWithFormat:@"id_%@", menu[@"tag1"]];
            [tagWithKey setValue:tagData[type][idKey][@"name"] forKey:[NSString stringWithFormat:@"%@", menu[@"tag1"]]];
        }
        if(menu[@"tag2"] != nil && ![menu[@"tag2"] isKindOfClass:[NSNull class]] && ![menu[@"tag2"] isEqualToString:@""])
        {
            idKey = [NSString stringWithFormat:@"id_%@", menu[@"tag2"]];
            [tagWithKey setValue:tagData[type][idKey][@"name"] forKey:[NSString stringWithFormat:@"%@", menu[@"tag2"]]];
        }
        if(menu[@"tag3"] != nil && ![menu[@"tag3"] isKindOfClass:[NSNull class]] && ![menu[@"tag3"] isEqualToString:@""])
        {
            idKey = [NSString stringWithFormat:@"id_%@", menu[@"tag3"]];
            [tagWithKey setValue:tagData[type][idKey][@"name"] forKey:[NSString stringWithFormat:@"%@", menu[@"tag3"]]];
        }
        if(menu[@"tag4"] != nil && ![menu[@"tag4"] isKindOfClass:[NSNull class]] && ![menu[@"tag4"] isEqualToString:@""])
        {
            idKey = [NSString stringWithFormat:@"id_%@", menu[@"tag4"]];
            [tagWithKey setValue:tagData[type][idKey][@"name"] forKey:[NSString stringWithFormat:@"%@", menu[@"tag4"]]];
        }
        
    }
    
    for (NSString *index in tagWithKey) {
        [tagIndex addObject:index];
        [tagAlert addButtonWithTitle:tagWithKey[index]];
    }
    
    NSString *indexJson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:tagIndex options:NSJSONWritingPrettyPrinted error:nil]  encoding:NSUTF8StringEncoding];
    
    UILabel *hidden = [[UILabel alloc]init];
    hidden.tag = 101;
    hidden.text = indexJson;
    
    [tagAlert addSubview:hidden];
    
    [tagAlert show];
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        rowData = [fullRowData copy];
        [tagBtn setTitle:[alertView buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        [tableList reloadData];
    }
    else if(buttonIndex > 1){
        UILabel *hidden = (UILabel*)[alertView viewWithTag:101];
        NSMutableArray *tagIndex = [[NSMutableArray alloc]init];
        NSDictionary *tagDict = [NSDictionary dictionaryWithString:hidden.text];
        for(NSString *tag in tagDict){
            [tagIndex addObject:tag];
        }
        
        [tagBtn setTitle:[alertView buttonTitleAtIndex:buttonIndex] forState:UIControlStateNormal];
        
        NSString *searchId = [tagIndex objectAtIndex:buttonIndex-2];
        NSMutableArray *searchMenu = [[NSMutableArray alloc]init];
        for (NSDictionary *menu in fullRowData) {
            if([menu[@"tag1"] isEqualToString:searchId] || [menu[@"tag2"] isEqualToString:searchId] || [menu[@"tag3"] isEqualToString:searchId] || [menu[@"tag4"] isEqualToString:searchId])
            {
                [searchMenu addObject:menu];
            }
        }
        BOOL reverseSort = YES;
        rowData = [[NSMutableArray alloc]initWithArray:[searchMenu sortedArrayUsingFunction:isBlockSort
                                                                                   context:&reverseSort]];
        [tableList reloadData];
    }
}

- (void) checkItAll:(UIButton*)sender
{
    int isBlock = (int)sender.tag;
    NSMutableArray *menuIdArr = [[NSMutableArray alloc]init];
    if(isBlock == 1){
        isBlock = 0;
        [sender setImage:[UIImage imageNamed:@"c_ic_check"] forState:UIControlStateNormal];
    }else{
        isBlock = 1;
        [sender setImage:[UIImage imageNamed:@"c_ic_check_in"] forState:UIControlStateNormal];
    }
    
    sender.tag = isBlock;
    
    for(NSDictionary *menu in rowData){
        [menuIdArr addObject:menu[@"id"]];
        NSString *key = [NSString stringWithFormat:@"id_%@", menu[@"id"]];
        [editBlockData setValue:@{
                                  @"id":menu[@"id"],
                                  @"is_block":[NSString stringWithFormat:@"%d", isBlock]
                                  } forKey:key];
    }
    
    [tableList reloadData];
    
}
- (void) saveItAll:(UIButton*)sender
{
    if([editBlockData count] > 0){
        NSMutableArray *data = [[NSMutableArray alloc]init];
        for(NSString *key in editBlockData){
            [data addObject:editBlockData[key]];
        }
        NSDictionary *parameters = @{
                                     @"code":member.code,
                                     @"data":[[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:nil]  encoding:NSUTF8StringEncoding]
                                     };
        NSString *url = [NSString stringWithFormat:SET_BLOCK_MENU,member.nre];
        [loadProgress.loadView showWithStatus];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if([json[@"status"]boolValue])
            {
                [menuDB loadData:@"menu_0"];
                NSDictionary *dbData = [[NSDictionary dictionaryWithString:menuDB.valueMenu] mutableCopy];
                menuData = [dbData[@"menu"] mutableCopy];
                for(NSString *key in editBlockData){
                    NSDictionary * menu = [menuData[key] mutableCopy];
                    [menu setValue:[NSString stringWithFormat:@"%@",editBlockData[key][@"is_block"]] forKey:@"is_block"];
                    [menuData setValue:menu forKey:key];
                }
                
                NSMutableArray *menuByKId = [[NSMutableArray alloc]init];
                for(NSString *k in menuData){
                    if(![menuData[k][@"kitchen_id"] isKindOfClass:[NSNull class]]){
                        if([menuData[k][@"kitchen_id"] isEqualToString:self.kitchenId]){
                            [menuByKId addObject:menuData[k]];
                        }
                    }
                }
                BOOL reverseSort = YES;
                rowData = [[NSMutableArray alloc]initWithArray:[menuByKId sortedArrayUsingFunction:isBlockSort
                                                                                           context:&reverseSort]];
                fullRowData = [rowData copy];
                
                //update datebase
                [dbData setValue:menuData forKey:@"menu"];
                [LoadMenuAll sortIndexMenu:dbData withKey:@"0"];
            }
             [LoadProcess dismissLoad];
//            [menuDB loadData:@"menu_0"];
//            NSDictionary *dbData = [[NSDictionary dictionaryWithString:menuDB.valueMenu] mutableCopy];
            [tableList reloadData];
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [rowData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    
    NSDictionary *menu = rowData[indexPath.row];
    cell.textLabel.text = menu[@"name"];
    NSLog(@"name %@",menu[@"name"]);
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    NSString *key = [NSString stringWithFormat:@"id_%@", menu[@"id"]];
    BOOL isBlock = (editBlockData[key] != nil)?[editBlockData[key][@"is_block"] boolValue]:[menu[@"is_block"]boolValue];
    if(!isBlock)
    {
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.contentView.alpha = 1;
        cell.imageView.image = [UIImage imageNamed:@"c_ic_check"];
    }else{
        cell.contentView.backgroundColor = [UIColor ht_cloudsColor];
        cell.contentView.alpha = 0.4;
        cell.imageView.image = [UIImage imageNamed:@"c_ic_check_in"];
    }
    
//    NSString *def_img = ([menu[@"type"]intValue] == 1)?@"food_default":@"drink_default";
//    NSDictionary *picture = [NSDictionary dictionaryWithString:menu[@"image_url"]];
//    [cell.imageView setImageWithURL:[NSURL URLWithString:(picture[@"th_url"]!=nil)?picture[@"th_url"]:@""] placeholderImage:[UIImage imageNamed:def_img]];
//    cell.imageView setI
//
//    UIView *tableContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0 ,tableView.frame.size.width, 40)];
//    tableContent.tag = 1;
//    tableContent.backgroundColor = [UIColor clearColor];
//    
//    
//    [[cell viewWithTag:1] removeFromSuperview];
//    
//    [cell addSubview:tableContent];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *menu = [rowData[indexPath.row] mutableCopy];
    NSString *key = [NSString stringWithFormat:@"id_%@", menu[@"id"]];
    NSString *isBlock = (editBlockData[key] != nil)?editBlockData[key][@"is_block"]:menu[@"is_block"];
    [editBlockData setValue:@{
                              @"id":menu[@"id"],
                              @"is_block":[NSString stringWithFormat:@"%d", ![isBlock boolValue]]
                              } forKey:key];
    [tableList reloadData];
    
//    NSLog(@" index %ld",[indexPath  row]);
//     [loadProgress.loadView showWithStatus];
////    [tableView deselectRowAtIndexPath:indexPath animated:NO];
////    
//      NSDictionary *menu = [rowData[indexPath.row] mutableCopy];
////    return;
//    NSError *error = nil;
//    NSString *block = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:@[menu[@"id"]]
//                                                                                    options:NSJSONWritingPrettyPrinted
//                                                                                      error:&error]
//                                                                   encoding:NSUTF8StringEncoding];
//    
//    NSDictionary *parameters = @{
//                                 @"code":member.code,
//                                 @"menu_id":block,
//                                 @"is_block":[NSString stringWithFormat:@"%d", ![menu[@"is_block"] boolValue]]
//                                 };
//    NSString *url = [NSString stringWithFormat:SET_BLOCK_MENU,member.nre];
//   
//    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
//        if([json[@"status"]boolValue])
//        {
//            
//            NSString *key = [NSString stringWithFormat:@"id_%@", menu[@"id"]];
//            NSString *isBlock = (editBlockData[key] != nil)?editBlockData[key][@"is_block"]:menu[@"is_block"];
//            [editBlockData setValue:@{
//                                      @"id":menu[@"id"],
//                                      @"is_block":[NSString stringWithFormat:@"%d", ![isBlock boolValue]]
//                                      } forKey:key];
//            [tableView reloadData];
//
//            
//            menuData = [menuData mutableCopy];
//            [menu setValue:[NSString stringWithFormat:@"%d", ![menu[@"is_block"] boolValue]] forKey:@"is_block"];
//            [menuData setValue:menu forKey:[NSString stringWithFormat:@"id_%@", menu[@"id"]]];
//            [rowData setObject:menu atIndexedSubscript:indexPath.row];
//            [tableView reloadData];
//            
//            //update datebase
//            NSError *error = nil;
//            [menuDB loadData:@"menu_0"];
//            NSDictionary *dbData = [[NSDictionary dictionaryWithString:menuDB.valueMenu] mutableCopy];
//            [dbData setValue:menuData forKey:@"menu"];
//            NSString *strUpdate = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dbData
//                                                                                                 options:NSJSONWritingPrettyPrinted
//                                                                                                   error:&error]
//                                                        encoding:NSUTF8StringEncoding];
//            [menuDB updateDb:@{
//                               @"valueMenu":strUpdate,
//                               @"date":@""
//                               } andId:@"menu_0"];
//            
//            [menuDB loadData:[NSString stringWithFormat:@"menu_%@",menu[@"type"]]];
//            NSDictionary *typeDbData = [[NSDictionary dictionaryWithString:menuDB.valueMenu] mutableCopy];
//            [typeDbData setValue:menu forKey:[NSString stringWithFormat:@"id_%@",menu[@"id"]]];
//            NSString *typeStrUpdate = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:typeDbData
//                                                                                                 options:NSJSONWritingPrettyPrinted
//                                                                                                   error:&error]
//                                                        encoding:NSUTF8StringEncodin g];
//            [menuDB updateDb:@{
//                               @"valueMenu":typeStrUpdate,
//                               @"date":@""
//                               } andId:[NSString stringWithFormat:@"menu_%@",menu[@"type"]]];
//        }
//         [LoadProcess dismissLoad];
//    }];
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    if ([Check checkNull:title]) {
        title = AMLocalizedString(@"ไม่เช็ค", nil);
    }
    [self setOrderListKitchenView:title];
    [self.selectModePrint setTitle:title forState:UIControlStateNormal];
}
-(void)selectModePrint:(id)sender{
    popupView = [[PopupView alloc] init];
    [popupView setOrdersListViewController:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
-(void)printAll:(id)sender{
    NSLog(@"printAll");
    [self setOrderListKitchenView:@"printAll"];
    [OrderListKitchenView setPrintQuenceTable:2 andIndexData:0];
    
}
-(void)printOne:(id)sender{
    NSLog(@"printCount");
   [self setOrderListKitchenView:@"printCount"];
   [OrderListKitchenView setPrintQuenceTable:3 andIndexData:0];
   [self.nPrint resignFirstResponder];
}
-(void) setOrderListKitchenView:(NSString *)type{
    NSMutableArray *counters = [pager pages];
    if (!OrderListKitchenView) {
        OrderListKitchenView = (OrderListKitchenViewController *)counters[0];
    }
    [OrderListKitchenView setSortRepeat:type];
    NSString  *num = [self.nPrint.text isEqualToString:@""]?@"0":self.nPrint.text;
    [OrderListKitchenView setNPrint:num];
}

#pragma mark sorting array
NSInteger isBlockSort(NSDictionary *menu1, NSDictionary *menu2, void *reverse)
{
    NSString *isBlock1 = menu1[@"is_block"];
    NSString *isBlock2 = menu2[@"is_block"];
    
    NSComparisonResult comparison = [isBlock1 localizedCaseInsensitiveCompare:isBlock2];
    if (comparison == NSOrderedSame) {
        
        isBlock1 = menu1[@"is_block"];
        isBlock2 = menu2[@"is_block"];
        comparison = [isBlock1 localizedCaseInsensitiveCompare:isBlock2];
    }
    
    if (*(BOOL *)reverse == YES) {
        return 0 - comparison;
    }
    return comparison;
}
-(void)setLineView:(UIButton *)sender{
    sender.layer.cornerRadius = 10.0f;
    sender.layer.borderWidth = 1.0f;
    sender.clipsToBounds = NO;
    sender.layer.masksToBounds = YES;
    sender.layer.borderColor = [UIColor ht_silverColor].CGColor;
    
}
@end
