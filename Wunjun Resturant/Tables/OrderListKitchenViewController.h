//
//  OrderListKitchenViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPageViewControllerDelegate.h"


@interface OrderListKitchenViewController : UIViewController<THSegmentedPageViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic        ) BOOL        blClikeChecker;
@property (nonatomic,strong ) NSString    *dStatus;
@property (nonatomic,strong ) NSString    *viewTitle;
@property (nonatomic,strong ) NSString    *kitchenId;
@property (nonatomic,strong ) NSString    *sortRepeat;
@property (nonatomic,strong ) NSString    *typePrintCut;
@property (strong, nonatomic) NSString    *nPrint;
@property (strong, nonatomic) NSString    *maxPrevBillId;
@property (nonatomic,strong) NSDictionary *selectKitchen;

@property (nonatomic,strong ) IBOutlet UITableView *tableList;

- (void)updateTable:(NSDictionary*)data;
- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)gestureRecognizer;
-(void) setPrintQuenceTable:(int)typePrint andIndexData:(int)idx  deliver:(NSString *)deliver success:(void (^)(BOOL completion)) block;
-(void) setPrintQuenceTable:(int)typePrint andIndexData:(int)idx;
@end
