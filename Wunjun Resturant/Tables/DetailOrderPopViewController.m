//
//  DetailOrderPopViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "DetailOrderPopViewController.h"
#import "UIImageView+AFNetworking.h"
#import "CashTakeawayTableViewController.h"

#import "CellTableViewAll.h"
#import "Constants.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "PopupView.h"
#import "Member.h"
#import "LoadMenuAll.h"
#import "TableSingleBillViewController.h"
#import "GlobalBill.h"
#import "NSString+GetString.h"
#import "Check.h"

#define kCellEditOrder            (@"CellEditOrder")
#define kCellContainerTime        (@"CellContainerTime")
#define kCellContainerButtonStop  (@"CellContainerButtonStop")
#define kCellContainerSingStart   (@"CellContainerSingStart")
#define kCellContainerSingStop    (@"CellContainerSingStop")
#define kCellContainerPro         (@"CellContainerPro")
#define kCellContainerBy        (@"CellContainerBy")
#define kCellContainerStatus      (@"CellContainerStatus")
#define kCellContainerButtonCancel  (@"CellContainerButtonCancel")


@interface DetailOrderPopViewController(){
    LoadProcess *loadProcess;
    PopupView *popupView;
    CGFloat sizemContanierView;
    NSMutableArray *dataTableOrder;
    NSMutableDictionary *sizeHeight;
    
    NSString *dataStop;
    NSString *timeStop;
    
    NSDictionary *staff_info;
    NSDictionary *member_info;
    NSString *member_id;
    NSString *staff_id;
}

@end


@implementation DetailOrderPopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    dataTableOrder = [NSMutableArray array];
    sizeHeight = [NSMutableDictionary dictionary];
    loadProcess  = [LoadProcess sharedInstance];
    
    NSString *nameOrder  = self.dataMenu[@"data"][@"name"];
    NSString *countOrder = self.dataMenu[@"Count"];
    NSString *saleOrder  = self.dataMenu[@"Price"];
    NSString *idOrder    = self.dataOrder[@"orders"][@"id"];
    NSString *drinkgirl_type    = self.dataOrder[@"orders"][@"drinkgirl_type"];
    NSDictionary *promotion  = self.dataOrder[@"promotion"];
    NSString *status     = self.dataOrder[@"orders"][@"deliver_status"];
    staff_info = self.dataOrder[@"staff_info"];
    member_info = self.dataOrder[@"member_info"];
    member_id =self.dataOrder[@"orders"][@"member_id"];
    staff_id =self.dataOrder[@"orders"][@"staff_id"];
    
    if(![staff_id isEqualToString:@"0"] || ![member_id isEqualToString:@"0"])
        [dataTableOrder  addObject:kCellContainerBy];
    
    if (![drinkgirl_type isEqual:@"0"]) {
        NSString *str = [self getTypeDrink:drinkgirl_type];
        if ([kDrinkTwo isEqual:str]) {
            NSString *dateStop  = [Time conventTime: self.dataOrder[@"orders"][@"stop_drink_time"]];
            if (dateStop  == nil) {
                [dataTableOrder  addObject:kCellContainerTime];
                if ([[self.tableDataBillView.selfSuper tableSuperClass] isKindOfClass:[CashTakeawayTableViewController class]]) {
                    [dataTableOrder  addObject:kCellContainerButtonStop];
                    [dataTableOrder  addObject:kCellContainerSingStart];
                }
                else{
                    if ([[self.tableDataBillView.selfSuper tableSuperClass] cashTableStatus] != nil) {
                        [dataTableOrder  addObject:kCellEditOrder];
                    }
                    else{
                        [dataTableOrder  addObject:kCellContainerButtonStop];
                        [dataTableOrder  addObject:kCellContainerSingStart];
                    }
                }
            }
            else{
                [dataTableOrder  addObject:kCellContainerTime];
                if ([[self.tableDataBillView.selfSuper tableSuperClass] isKindOfClass:[CashTakeawayTableViewController class]]) {
                    [dataTableOrder  addObject:kCellContainerSingStart];
                    [dataTableOrder  addObject:kCellContainerSingStop];
                }
                else{
                    if ([[self.tableDataBillView.selfSuper tableSuperClass] cashTableStatus] != nil) {
                        [dataTableOrder  addObject:kCellEditOrder];
                    }
                    else{
                        [dataTableOrder  addObject:kCellContainerSingStart];
                        [dataTableOrder  addObject:kCellContainerSingStop];
                    }
                }
            }
        }
        else{
            [dataTableOrder  addObject:kCellContainerTime];
            [dataTableOrder  addObject:kCellContainerSingStart];
            
        }
        nameOrder = [NSString stringWithFormat:@" %@ (%@)",nameOrder,str];
    }
    else  if ([promotion count] != 0) {
        if ([[self.tableDataBillView.selfSuper tableSuperClass] isKindOfClass:[CashTakeawayTableViewController class]]) {
             [dataTableOrder  addObject:kCellContainerPro];
        }
        else{
            if ([[self.tableDataBillView.selfSuper tableSuperClass] cashTableStatus] != nil) {
                [dataTableOrder  addObject:kCellEditOrder];
            }
            else{
                [dataTableOrder  addObject:kCellContainerPro];
                NSString *menuType  = self.dataMenu[@"dataorder"][@"menu_type"];
                if ([menuType isEqual:@"4"]) {
                    [dataTableOrder  addObject:kCellContainerButtonCancel];
                }
            }
        }
    }
    else{
        
        if ([[self.tableDataBillView.selfSuper tableSuperClass] isKindOfClass:[CashTakeawayTableViewController class]]) {
            [dataTableOrder  addObject:kCellContainerStatus];
            if (![status isEqual:@"4"]) {
                [dataTableOrder  addObject:kCellContainerButtonCancel];
            }
        }
        else{
            if ([[self.tableDataBillView.selfSuper tableSuperClass] cashTableStatus] != nil) {
                [dataTableOrder  addObject:kCellEditOrder];
            }
            else{
                [dataTableOrder  addObject:kCellContainerStatus];
                if (![status isEqual:@"4"]) {
                    [dataTableOrder  addObject:kCellContainerButtonCancel];
                }
            }
        }
    }

    NSString *imagePaht = self.dataMenu[@"data"][@"image_url"];
    [self setImage:imagePaht];
    
    self.mNameOrder.text = nameOrder;
    self.mCountOrder.text = [NSString stringWithFormat:self.mCountOrder.text,countOrder];
    self.mSaleOrder.text = [NSString conventCurrency:saleOrder];
    self.mIdOrder.text = [NSString stringWithFormat:self.mIdOrder.text,idOrder];
  
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.tableContainer setScrollEnabled:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkStopDrink:)
                                                 name:@"checkStopDrinkOrder"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelOrder:)
                                                 name:@"checkCancelOrder"
                                               object:nil];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (self.tableDataBillView.isCashier) {
        [TableSingleBillViewController loadDataOpenTableCashier:self.tableDataBillView.tableID];
    }
    else{
        [TableSingleBillViewController loadDataOpenTableSing:self.tableDataBillView.tableID];
    }

}

- (void)viewDidLayoutSubviews {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        [self.view layoutIfNeeded];
        [self.mScrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
    }
    else{
        [self.view setNeedsLayout];
    }
    
    sizemContanierView = 0;
    for (NSString *ind in [sizeHeight allValues]) {
        sizemContanierView += [ind intValue];
    }
    [self.mScrollView setContentSize:CGSizeMake(self.mScrollView.frame.size.width,self.mScrollView.frame.size.height+sizemContanierView-157)];
    [self.mContanierView setConstant:self.mScrollView.contentSize.height];
    [self.mLayoutContainerType setConstant:sizemContanierView];
}
-(void) setImage:(NSString *)paht{
    if (![Check checkNull:paht]) {
        NSData* data = [paht dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self.mImageTitle setImageWithURL:[NSURL URLWithString:jsonObject[@"th_url"]]];
    }
}
-(NSString *)checkStatus:(NSString *)status{
//    if ([status isEqual:@"1"]) {
//        return @"รอทำ";
//    }
//    else  if ([status isEqual:@"2"]) {
//        return  @"กำลังทำ";
//    }
//    else  if ([status isEqual:@"3"]) {
//        return  @"นำเสริฟแล้ว";
//    }
//    else{
//        return  @"ไม่มีสถานะ";
//    }
    
    NSInteger num = [status integerValue];
    switch (num) {
        case 1:
        case 6:
            return AMLocalizedString(@"รอทำ", nil);
            break;
        case 2:
             return  AMLocalizedString(@"กำลังทำ", nil);
            break;
        case 3:
              return  AMLocalizedString(@"นำเสริฟแล้ว", nil);
            break;
        case 4:
            return  AMLocalizedString(@"ยกเลิกรายการอาหาร", nil);
            break;
        case 0:
        case 5:
            return  AMLocalizedString(@"นำเสริฟแล้ว", nil);
            break;
        default:
            return  AMLocalizedString(@"ไม่มีสถานะ", nil);
            break;
    }
    
}
-(NSString *)getTypeDrink:(NSString *)ind{
    if ([ind isEqual:@"1"]) {
        return kDrinkOne;
    }
    else if ([ind isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else if ([ind isEqual:@"3"]) {
        return kDrinkThree;
    }
    else{
        return nil;
    }
}
- (IBAction)cancelOrder:(id)sender{
    Member *member = [Member getInstance];
    [member loadData];
    
    GlobalBill *globalBill = [GlobalBill sharedInstance];
    NSMutableDictionary *dataListSingBill = globalBill.billList;
    NSString *table_id  = dataListSingBill[@"table_id"];
    
    NSString *orderId = self.dataOrder[@"orders"][@"id"];
    
    NSString *deliver_status = @"4";
    if ([self.dataOrder[@"orders"][@"deliver_status"] isEqualToString:@"5"]) {
        deliver_status = @"6";
    }
    NSDictionary *parameter = @{@"code":member.code,
                                @"order_id":orderId,
                                @"deliver_status":deliver_status,
                                @"table_id":table_id};
    
    NSString *url = [NSString stringWithFormat:SET_DELIVER_STATUS,member.nre];
    
    [LoadMenuAll loadData:parameter andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl){
            [self.tableDataBillView btnClosePop:sender];
            [loadProcess.loadView showProgress];
        }
        else{
            [loadProcess.loadView showError];
        }
    }];
}

- (IBAction)checkStopDrink:(NSNotification *)_note {
    popupView = [[PopupView alloc] init];
    [popupView setDetailOrderPopViewController:self];
    [popupView createPopoverSing:self];
}
- (IBAction)getImageBtnPressed:(UIImage *)sender
{
    UIImage *image =  sender;
    if (image != nil) {
        [self uploadImageSign:image];
    }
}
-(void)uploadImageSign:(UIImage *)image {
    Member *member = [Member getInstance];
    [member loadData];
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    NSDictionary *parameters = @{@"code": member.code,
                                 @"files":@"file"};
    NSString *url = [NSString stringWithFormat:UPLOAD_IMAGE,member.nre];
    [LoadMenuAll loadDataImage:imageData andParameter:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
             NSString *th_url = json[@"data"][@"th_url"];
            [self loadDataManageDrink:th_url];
        }
    }];
}
-(void)loadDataManageDrink:(NSString *)image{
    
    Member *member = [Member getInstance];
    [member loadData];
    
    NSString *orderID = self.dataMenu[@"dataorder"][@"id"];
    NSString *girlID = self.dataMenu[@"drinkGirl"][@"id"];
    
    NSDictionary *parameters = @{@"code":member.code,
                                 @"action":@"stop",
                                 @"order_id":orderID,
                                 @"girl_id":girlID,
                                 @"sign_pic_url":image};
    NSString *url = [NSString stringWithFormat:GET_MANAGE_DRINK,member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl){
            [sizeHeight setObject:@"180" forKey:[@([dataTableOrder count]) stringValue]];
            timeStop = [Time getTime];
            dataStop = image;
            [dataTableOrder removeObjectAtIndex:1];
            [dataTableOrder  addObject:kCellContainerSingStop];
            [self viewDidLayoutSubviews];
            [self.tableContainer setScrollEnabled:YES];
            [self.tableContainer reloadData];
            
            [LoadProcess dismissLoad];
        }
    }];
}
#pragma mark - create table container
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"prepareForSegue :%@",segue.identifier);
    if ([segue.identifier isEqualToString:@"containerView"]){

        UIViewController *controller = segue.destinationViewController;
        [controller.view setBackgroundColor:[UIColor redColor]];
        UIViewController *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"DetailOrderContainer"];
        for (UIView *view in [pager.view subviews]) {
            if ([view isKindOfClass:[UITableView class]]) {
                self.tableContainer = (UITableView *)view;
                [self.tableContainer setDataSource:self];
                [self.tableContainer setDelegate:self];
            }
        }
        sizemContanierView = pager.view.frame.size.height;
           
        [controller addChildViewController:pager];
        [controller.view addSubview:pager.view];
        [pager didMoveToParentViewController:controller];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"numberOfRowsInSection %ld",(long)[dataTableOrder count]);
    return [dataTableOrder count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"height %ld",(long)indexPath.row);
    NSString *cellIden  =  (NSString *)dataTableOrder[indexPath.row];
    if ([cellIden isEqual:kCellContainerSingStart] || [cellIden isEqual:kCellContainerSingStop]) {
        [sizeHeight setObject:@"180" forKey:[@(indexPath.row) stringValue]];
        return 180;
    }
    else if ([cellIden isEqual:kCellEditOrder]) {
        [sizeHeight setObject:@"125" forKey:[@(indexPath.row) stringValue]];
        return 125;
    }else if ([kCellContainerBy isEqual:cellIden]){
         return 125;
    }else{
        [sizeHeight setObject:@"60" forKey:[@(indexPath.row) stringValue]];
        return 60;
    }
  
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     NSString *cellIden  =  (NSString *)dataTableOrder[indexPath.row];
    
    CellTableViewAll *cell =  [tableView dequeueReusableCellWithIdentifier:cellIden];
    if (cell == nil) {
        cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIden];
    }
    else if ([kCellEditOrder isEqual:cellIden]){
        if ([self.typeEditCashier isEqualToString:@"delete"]) {
            [cell.mLabel setHidden:YES];
            cell.mCountEdit.text = AMLocalizedString(@"ยกเลิกรายการ", nil);
            [cell.mCountEdit setFont:[UIFont systemFontOfSize:18]];
            cell.mCountEdit.textColor = [UIColor redColor];
            [cell.mCountEdit setUserInteractionEnabled:NO];
        }
        else  if ([self.typeEditCashier isEqualToString:@"complete"]) {
            [cell.mLabel setHidden:YES];
            cell.mCountEdit.text = AMLocalizedString(@"ทำเสร็จ", nil);
            [cell.mCountEdit setFont:[UIFont systemFontOfSize:18]];
            cell.mCountEdit.textColor = [UIColor redColor];
            [cell.mCountEdit setUserInteractionEnabled:NO];
        }
    }
    if ([cellIden isEqual:kCellContainerPro]) {
        NSDictionary *promotion  = self.dataOrder[@"promotion"];
        cell.mCount.text = promotion[@"name"];
    }
    else if ([kCellContainerStatus isEqual:cellIden]){
        NSString *status     = self.dataOrder[@"orders"][@"deliver_status"];
        if ([status isEqualToString:@"4"]) {
            [cell.mBtnEdit setHidden:YES];
        }
        cell.mTimeDrink.text = [self checkStatus:status];
    }
    else if ([kCellContainerTime isEqual:cellIden]){
        NSString *dateStrat = [Time conventTime:self.dataOrder[@"orders"][@"start_drink_time"]];
        
        cell.mTimeDrink.text = dateStrat;
    }
    else if ([kDrinkTwo isEqual:cellIden]){
        NSDictionary *promotion  = self.dataOrder[@"promotion"];
        cell.mCount.text = promotion[@"name"];
    }
    else if ([kCellContainerSingStart isEqual:cellIden]){
        NSString *dateStrat = [Time conventTime:self.dataOrder[@"orders"][@"start_drink_time"]];
        cell.mTimeDrink.text = [NSString stringWithFormat:cell.mTimeDrink.text,dateStrat];
        
        NSString *_singImage = self.dataMenu[@"dataorder"][@"sign"];
        NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];
        NSString *pahtstart = dataSing[@"start"];
        [cell.mImageBackgroud setImageWithURL:[NSURL URLWithString:pahtstart]];
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
        cell.clipsToBounds = NO;
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.masksToBounds = YES;
    }
    else if ([kCellContainerSingStop isEqual:cellIden]){
        NSString *dateStrat = [Time conventTime:self.dataOrder[@"orders"][@"stop_drink_time"]];
        
        NSString *_singImage = self.dataMenu[@"dataorder"][@"sign"];
        
        NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];
        NSString *pahtstop = dataSing[@"stop"];
        if (pahtstop == nil) {
            pahtstop = dataStop;
        }
        if (dateStrat == nil) {
            dateStrat = timeStop;
        }
        
        cell.mTimeDrink.text = [NSString stringWithFormat:cell.mTimeDrink.text,dateStrat];
        [cell.mImageBackgroud  setImageWithURL:[NSURL URLWithString:pahtstop]];

        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
        cell.clipsToBounds = NO;
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.masksToBounds = YES;
    }else if ([kCellContainerBy isEqual:cellIden]){
        @try{
        NSLog(@"staff_id:");
            if(![staff_id isEqualToString:@"0"]){
            cell.mLabelBy.text = staff_info[@"name"];
            cell.mLabelRoleBy.text = @"Staff";
            }else if(![member_id isEqualToString:@"0"]){
            
            cell.mLabelBy.text = member_info[@"name"];
            cell.mLabelRoleBy.text = @"Member";
                
            
            [cell.mImageBy  setImageWithURL:[NSURL URLWithString:member_info[@"avatar"][@"th_url"]]];
        }
        }@catch(NSException *exception){
        }
    }
    return cell;
}

- (IBAction)btnClosePop:(id)sender {
    [self.tableDataBillView btnClosePop:sender];
}

@end
