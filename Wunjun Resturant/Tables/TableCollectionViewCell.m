//
//  TableCollectionViewCell.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableCollectionViewCell.h"
@implementation TableCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}
- (void)setSelected:(BOOL)selected
{

}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    //Add any drawing code you need here. For example, I placed a method here that loads the font of a label in the cell
}

@end
