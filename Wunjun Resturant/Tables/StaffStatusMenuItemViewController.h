//
//  StaffStatusMenuItemViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/12/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StaffStatusMenuItemViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

-(void)loadDataMenu:(NSString *)kitchenID;

@property (nonatomic ,strong) NSDictionary *dataKitchen;
@end
