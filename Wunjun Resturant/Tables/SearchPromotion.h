//
//  SearchPromotion.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/5/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchPromotion : NSObject

+(void)setIdBillOrder:(NSString *)idBill;
+(BOOL)checkNull:(NSString *)object;
+(void)setStaticValel:(NSDictionary *)_promotion_info;
+(void)setStaticValel:(NSInteger)lv1 and:(NSInteger)lv2;
+(NSArray *) getPromotionPool:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo;
+(NSArray *) getPromotionPoolAttachBill:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo and:(NSDictionary *)promotionApproval;
+(NSMutableArray *)addBillData:(NSMutableArray *)data  andPop:(BOOL)isPopupCashier;
+(NSArray *) getPromotionRequire:(NSArray *)dataPool  and:(NSMutableArray *)dataFloder and:(NSDictionary *)dataInfo;
+(void)setSpecialtFood:(NSDictionary *)menu;
@end
