//
//  TablePromotiomViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/1/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "TableSingleBillViewController.h"
#import "TableDataBillViewController.h"
#import "TableSubBillViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface TablePromotiomViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mTableProductPromotion;
@property (nonatomic ,strong) TableSingleBillViewController *tableSingleBillViewControll;
@property (nonatomic ,strong) TableSubBillViewController *tableSubBillViewController;
@property (nonatomic ,strong) NSDictionary *selecCell;
@property (nonatomic ,strong) NSDictionary *detailTable;
@property (nonatomic ,strong) NSString *idGiftType;

@property (nonatomic,strong) UITableView *tableTemp;

-(void)loadDataPromotionFloder:(NSDictionary *)promotionFloder;
- (IBAction)btnBack:(id)sender;
-(void)loadDataPromotion;
-(void)loadDataPromotionRequirePromtion;
@end
