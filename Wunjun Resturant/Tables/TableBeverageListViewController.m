//
//  TableBeverageListViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableBeverageListViewController.h"
#import "NumberBeverageViewController.h"
#import "WYPopoverController.h"
#import "CellTableViewAll.h"
#import "Constants.h"
#import "NumberBeverageViewController.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "NSDictionary+DictionaryWithString.h"
#import "NSString+GetString.h"
#import "PopupView.h"
#import "TableSingleBillViewController.h"
#import "GlobalBill.h"
#import "Check.h"
#import "OrderDB.h"
#import "CellCheckAmountTableViewCell.h"
#import "UIColor+Conventcolor.h"

#import "FoodOptionSum.h"
#import "FMDBDataAccess.h"

@interface TableBeverageListViewController()<OTPageScrollViewDataSource,OTPageScrollViewDelegate,WYPopoverControllerDelegate,UIAlertViewDelegate>{
    OTPageView *pscrollView;
    PopupView *popupView;
    GlobalBill *globalBill;
    LoadProcess *loadProgress;
    
    NSDictionary *_dataPageScrollView;
    WYPopoverController *popoverController;
    UITableViewController *viewControllerShowMenu;
    UITableView *tableListShowMenu;
    
    NSMutableArray *dataCategoryList;
    NSMutableDictionary *dataSearchTag;
    UITableView *tableSreachPop;
    NSInteger srocllIndex;
    NSString *titlePop;
    
    BOOL isCheckOrderDB;
    BOOL isCheckSubmit;
    
    NSString *idOrder;
    
    BOOL searchBarBegin;
    UIImageView *imageback;
    UIImageView *imagenext;
    UIButton *btnAlertView;
    
    
    NSDictionary *dataMenuDic;
    NSDictionary *menuDataSeclect;
    NSDictionary *menuUpsale;
    NSDictionary *menuUpPromotion;
    NSDictionary *cheerOption;
}
@property (nonatomic ,strong) PrinterDB *printerDB;
@property (nonatomic ,strong) MenuDB *menuDB;
@property (nonatomic ,strong) OrderDB *orderDB;

@end
@implementation TableBeverageListViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(PrinterDB *)printerDB{
    if (!_printerDB) {
        FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
        _printerDB = [db getCustomers]; 
    }
    return _printerDB;
}
-(OrderDB *)orderDB{
    if (!_orderDB) {
        _orderDB = [OrderDB getInstance];
    }
    return _orderDB;
}
-(MenuDB *)menuDB{
    if (!_menuDB) {
        _menuDB = [MenuDB getInstance];
    }
    return _menuDB;
}
- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.mCollectionView.collectionViewLayout;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    _mSearcField.placeholder = AMLocalizedString(@"search_menu_list", nil);
    [self initNavigation];
    
    self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
    
    [self.mSearcField setDelegate:self];
    popupView = [[PopupView alloc] init];
    globalBill  = [GlobalBill sharedInstance];
    
    self.dataMenu = [NSMutableArray array];
    self.dataCategory = [NSMutableArray array];
    self.dataTagDrinkList = [NSMutableDictionary dictionary];
    self.dataMenuList  = [NSMutableArray array];
    self.dataTagAdd  = [NSMutableArray array];
    self.addFoodBill = [NSMutableArray array];
    loadProgress  = [LoadProcess sharedInstance];
    
    dataSearchTag  = [NSMutableDictionary dictionary];
    srocllIndex = 0;
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(btnTagSelectorMenu:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [self.mCollectionView addGestureRecognizer:lpgr];

    titlePop = @"tag";
    _dataPageScrollView = [NSMutableDictionary dictionary];
    _dataPageScrollView = @{@"tag":@[AMLocalizedString(@"ประเภท", nil),AMLocalizedString(@"ยี่ห้อ", nil),AMLocalizedString(@"ขนาด", nil)],
                            @"category":@[AMLocalizedString(@"หมวดหมู่", nil)]};
    
    CGFloat sizeWidht = self.mScrollTagCategory.frame.size.width;
    pscrollView = [[OTPageView alloc] initWithFrame:CGRectMake(0,0,sizeWidht,self.mScrollTagCategory.frame.size.height)];
    pscrollView.pageScrollView.dataSource = self;
    pscrollView.pageScrollView.delegate = self;
    pscrollView.pageScrollView.padding = 10;
    pscrollView.pageScrollView.leftRightOffset = 0;
    pscrollView.pageScrollView.frame = CGRectMake(30,0,sizeWidht,self.mScrollTagCategory.frame.size.height);
    [pscrollView.pageScrollView reloadData];
    
    imageback =[[UIImageView alloc] initWithFrame:CGRectMake(0,3, 24 ,24)];
    imageback.image = [UIImage imageNamed:@"back"];
    
    imagenext =[[UIImageView alloc] initWithFrame:CGRectMake(sizeWidht-30,3, 24 ,24)];
    imagenext.image = [UIImage imageNamed:@"next"];
    
    
    [self.mScrollTagCategory addSubview:imageback];
    [self.mScrollTagCategory addSubview:imagenext];
    [self.mScrollTagCategory addSubview:pscrollView];
    [self setNavBarLogo];
    [self loadViewFood:@"menu_2"];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
     isCheckOrderDB = YES;
    if ([globalBill.billType  isEqual: kSingleBill]) {
        TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
            myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        }
        idOrder = [myController.tableDataBillView detailTable][@"id"];
        idOrder = [NSString stringWithFormat:@"table_id_%@",idOrder];;
    }
    else{
        TableSubBillViewController *myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        if (![myController isKindOfClass:[TableSubBillViewController class]]) {
            myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        }
        idOrder = [myController.tableDataBillView detailTable][@"id"];
        idOrder = [NSString stringWithFormat:@"table_id_%@",idOrder];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    if (isCheckOrderDB || isCheckSubmit) {
        [self initOrderDB];
    }
    
}
-(void)viewDidLayoutSubviews{
    CGFloat sizeWidht = self.mScrollTagCategory.frame.size.width;
    pscrollView.frame = CGRectMake(0,0,sizeWidht,self.mScrollTagCategory.frame.size.height);
    pscrollView.pageScrollView.frame = CGRectMake(30,0,sizeWidht-60,self.mScrollTagCategory.frame.size.height);
    imageback.frame = CGRectMake(0,3, 24 ,24);
    imagenext.frame = CGRectMake(sizeWidht-30,3, 24 ,24);
    [pscrollView.pageScrollView reloadData];
}
//-(void)initNavigation{
//    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) {
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_drink@3x.png"]
//                                                      forBarMetrics:UIBarMetricsDefault];
//    }
//    else
//    {
//        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"top_drink@2x.png"]
//                                                      forBarMetrics:UIBarMetricsDefault];
//    }
//}
-(void)initNavigation{
    UIImage *image;
    CGSize size = self.navigationController.navigationBar.frame.size;
//    if ([Check checkDevice]) { // iphione
//        image = [UIImage imageNamed:@"top_water@2x"];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
//            image = [UIImage imageNamed:@"top_water@3x"];
//        }
//    }
//    else{ // ipan
//        image = [UIImage imageNamed:@"top_water@3x"];
//    }
    image = [UIImage imageNamed:@"top_water"];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(size.width,size.height+22)];
    [self.navigationController.navigationBar setBackgroundImage:image
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)initOrderDB{
    if (isCheckSubmit){
        [self.orderDB updateDb:@"nil" andId:idOrder andTypeFood:YES];
    }
    else if (isCheckOrderDB){
        NSError *error = nil;
        NSDictionary *dic = @{@"tag":self.dataTagAdd,
                              @"bill":self.addFoodBill};
        NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic
                                                                                            options:NSJSONWritingPrettyPrinted
                                                                                              error:&error]
                                                   encoding:NSUTF8StringEncoding];
        if (![self.orderDB isKeyDB:idOrder]){
            [self.orderDB insertLoginData:cartJSON andDrink:@"orderDrink" and:idOrder];
        }
        else{
            [self.orderDB updateDb:cartJSON andId:idOrder andTypeFood:YES];
        }
    }
//    else if (isCheckOrderDB) {
//        NSError *error = nil;
//        NSDictionary *dic = @{@"tag":self.dataTagAdd,
//                              @"bill":self.addFoodBill};
//        NSString *cartJSON = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic
//                                                                                            options:NSJSONWritingPrettyPrinted
//                                                                                              error:&error]
//                                                   encoding:NSUTF8StringEncoding];
//        if (![self.orderDB isKeyDB:idOrder]){
////            [self.orderDB insertLoginData:@"orderFood" andDrink:cartJSON and:idOrder];
//            [self.orderDB insertLoginData:cartJSON andDrink:@"orderDrink" and:idOrder];
//        }
//        else{
//            [self.orderDB updateDb:cartJSON andId:idOrder andTypeFood:YES];
//        }
//    }
}
- (void)setNavBarLogo {

    float left = 40.0;
    float right = self.view.frame.size.width-172.0;
    UIView *navTitle1 = [[UIView alloc] initWithFrame:CGRectMake(left,0,right,44)];
    UILabel *topTitle =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,navTitle1.bounds.size.width, 44)];
    topTitle.text = AMLocalizedString(@"สั่งเครื่องดื่ม", nil);
    [topTitle setTextAlignment:NSTextAlignmentCenter];
    topTitle.font = [UIFont systemFontOfSize:18];
    [topTitle setTextColor:[UIColor whiteColor]];
    [navTitle1 addSubview:topTitle];
    self.navigationItem.titleView = navTitle1;
    
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                                                               target:nil action:nil];
    fixedItem.width = 20.0f;    // or whatever you want
    
    UIImage *image = [UIImage imageNamed:@"ic_add_order"];
    CGRect frame = CGRectMake(0, 0, image.size.width+10, image.size.height+10);
    UIButton* button = [[UIButton alloc] initWithFrame:frame];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setShowsTouchWhenHighlighted:YES];
    [button addTarget:self action:@selector(barButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    NSArray *buttons = @[fixedItem,barButtonItem,fixedItem];
    self.navigationItem.rightBarButtonItems = [self.navigationItem.rightBarButtonItems arrayByAddingObjectsFromArray:buttons];
}
-(void)loadViewFood:(NSString *)key{
    [self.menuDB loadData:key];
    
    NSDictionary *json = [NSDictionary dictionaryWithObject:[NSDictionary dictionaryWithString:self.menuDB.valueMenu]];
    self.dataCategory = [dataCategoryList mutableCopy];
    [self.orderDB loadData:idOrder];
    if (self.orderDB.orderDrink) {
//        NSDictionary *jsonOrder = [NSDictionary dictionaryWithString:self.orderDB.orderDrink];
        NSDictionary *jsonOrder = [NSDictionary dictionaryWithString:self.orderDB.orderFood];
        if (jsonOrder) {
            self.addFoodBill = [jsonOrder[@"bill"] mutableCopy];
            self.dataTagAdd = [jsonOrder[@"tag"] mutableCopy];
        }
    }
    
    NSDictionary *data = [NSDictionary dictionaryWithObject:json[@"menu"]];
    NSMutableArray *keyList = [NSMutableArray array];
    [keyList addObjectsFromArray:[json[@"menu"] allKeys]];
    
    self.dataMenu = [NSMutableArray array];
    for (NSString *str in keyList) {
        NSDictionary *dic = data[str];
        if ([dic[@"type"] isEqualToString:@"2"]) {
             [self.dataMenu addObject:dic];
        }
        
    }
    if ([self.dataMenu count] != 0) {
        self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenu];
    }
    
    self.dataTagDrinkList =  json[@"tag"][@"drink"];
    if ([self.dataTagDrinkList  count] != 0) {
        [self getDataCategory:1];
    }
    
    NSDictionary *listUsaageTemp = [NSDictionary dictionaryWithDictionary:[Check checkObjectType:json[@"category"]]];
//    NSString *keyBill = @"drink";
    NSString *keyBill = @"2";//@"food";
    NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(category_type = %@)" andData:[listUsaageTemp allValues]];
    dataCategoryList = [NSMutableArray arrayWithArray:listUsaage];
    if ([dataCategoryList count] != 0) {
        titlePop = @"category";
        [self showDataCategoryList];
        [pscrollView.pageScrollView reloadData];
        imageback.image = nil;
        imagenext.image = nil;
    }
    if ([self.dataMenu count] != 0){
        self.dataMenuList = [self sortDataOrder:self.dataMenuList andKey:@"order"];
        [self.mCollectionView reloadData];
    }
}
-(void) getDataCategory:(NSInteger)integer{
    self.dataCategory  = [NSMutableArray array];
    NSArray *dataMenuListTemp = [NSArray arrayWithArray:self.dataMenuList];
    for (NSString *key in self.dataTagDrinkList) {
        NSString *num = (NSString *)self.dataTagDrinkList[key][@"type"];
        NSString *keyBill = self.dataTagDrinkList[key][@"id"];
        // ค้นหา tag
        NSArray *blCheckTag = [NSDictionary searchDictionaryALL:keyBill
                                                       andvalue:[self tagValue:[num intValue]]
                                                        andData:dataMenuListTemp];
        if ([num isEqual:[@(integer) stringValue]] && [blCheckTag count] != 0) {
            [self.dataCategory addObject:self.dataTagDrinkList[key]];
        }
    }
    [self.mTableCategory setTag:111];
    [self.mTableCategory reloadData];
}
-(NSString *)tagValue:(int)num{
    switch (num) {
        case 1:
            return @"(tag1 = %@)";
            break;
        case 2:
            return @"(tag2 = %@)";
            break;
        case 3:
            return @"(tag3 = %@)";
            break;
        case 4:
            return @"(tag4 = %@)";
            break;
            
        default:
            return @"";
            break;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.mTableCategory){
        return 80;
    }
    return tableView.rowHeight;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.mTableCategory)
        return [self.dataCategory count];
    else if (tableView == tableListShowMenu)
        return [self.dataMenuList count];
    else if (tableView == self.mTableTagDrink){
        self.mTableTagDrink.backgroundColor = ([self.dataTagAdd count] == 0)?[UIColor clearColor]:[UIColor whiteColor];
        return [self.dataTagAdd count];
    }
    else if (tableView == tableSreachPop)
        return 4;
    
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CellDrinkItem";
    @try {
        NSInteger row = [indexPath row];
        if (tableView == self.mTableTagDrink) {
            NSLog(@" drink : %ld",(long)indexPath.row);
            CellFoodItemView *_cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (_cell == nil) {
                _cell = [[CellFoodItemView alloc]initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:identifier];
            }
//            _cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tx_tag.png"]];
            
            NSDictionary *dic = self.dataTagAdd[row];
            [_cell.mBtnTttleTag setTitle:dic[@"data"][@"name"] forState:UIControlStateNormal];
            [_cell.mBtnTttleTag addTarget:self action:@selector(showClickDeleteTag:)
                         forControlEvents:UIControlEventTouchUpInside];
            _cell.mBtnTttleTag.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [_cell.mCountOrder setText:[NSString stringWithFormat:@"(%@)",dic[@"count"]]];
//            [_cell.mBtnDelete addTarget:self action:@selector(clickDeleteTag:)
//                       forControlEvents:UIControlEventTouchUpInside];
            [self setLineView:_cell];
            return _cell;
        }
        else if(tableView == self.mTableCategory){
            CellCheckAmountTableViewCell *cell = (CellCheckAmountTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellCategory"];
            if (cell == nil) {
                cell = [[CellCheckAmountTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                           reuseIdentifier:@"cellCategory"];
            }
             NSDictionary *dic = self.dataCategory[row];
            int num = [dic[@"id"] intValue] * 100;
            [cell.mNameGoods setText:[dic objectForKey:@"name"]];
            [cell.mOrderNumber setText:[@(num) stringValue]];
            
            if (dic[@"color"] != nil) {
                cell.backgroundColor = [UIColor colorWithHexString:dic[@"color"]];
            }
            else
                cell.backgroundColor = [UIColor clearColor];
            [self setLineView:cell];
            return cell;
        }
        else if (tableListShowMenu == tableView) {      // popup
            CellTableViewAll *_cell = (CellTableViewAll*)[tableView dequeueReusableCellWithIdentifier:kCellOptionFood];
            NSDictionary *dic = self.dataMenuList[row];
            _cell.mLabel.text = [dic objectForKey:@"name"];
            return _cell;
        }
        else if (tableView == tableSreachPop){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
            }
            
            switch (indexPath.row) {
                case 0:{
                    cell.textLabel.text = AMLocalizedString(@"ค้นหาแบบแท็ก", nil);
                    cell.imageView.image = [UIImage imageNamed:@"search"];
                }break;
                case 1: {
                    cell.textLabel.text = AMLocalizedString(@"ค้นหาแบบหมวดหมู่", nil);
                    cell.imageView.image = [UIImage imageNamed:@"search"];
                } break;
                case 2: cell.textLabel.text = AMLocalizedString(@"รีเซ็ตการค้นหา", nil);  break;
                default: cell.textLabel.text = AMLocalizedString(@"อัพเดตเมนูอาหาร", nil);  break;
            }
            cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont systemFontOfSize:14.0];
            return cell;
        }
        else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellListItem"];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellListItem"];
            }
            
            NSDictionary *dic = self.dataMenuList[row];
            BOOL blBlock = [dic[@"is_block"] boolValue];
            if (blBlock) {
                cell.textLabel.text = [dic objectForKey:@"name"];
                cell.textLabel.textColor = [UIColor ht_silverColor];
                [cell.imageView setImage:[UIImage imageNamed:@"btn_delete"]];
                [cell.imageView setContentMode:UIViewContentModeScaleAspectFit];
            }
            else{
                cell.textLabel.text = [dic objectForKey:@"name"];
                cell.textLabel.textColor = [UIColor blackColor];
                [cell.imageView setImage:nil];
            }
            cell.textLabel.font = [UIFont systemFontOfSize:14.0];
            
            return cell;
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error exception :%@",exception);
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"didSelectRowAtIndexPath   : %d",(int)indexPath.row);
    if (tableView == tableSreachPop) {
        [popupView btnClosePop:popoverController];
        switch (indexPath.row) {
            case 0:{
                titlePop = @"tag";
                [self getDataCategory:1];
                [pscrollView.pageScrollView reloadData];
                imageback.image = [UIImage imageNamed:@"back"];
                imagenext.image = [UIImage imageNamed:@"next"];
                
            }break;
            case 1:{
                
                titlePop = @"category";
                [self showDataCategoryList];
                [pscrollView.pageScrollView reloadData];
                imageback.image = nil;
                imagenext.image = nil;
            }break;
            case 2:{
                [self.dataMenuList removeAllObjects];
                self.dataMenuList = [self.dataMenu mutableCopy];
                [self.mCollectionView reloadData];
            }break;
            default:{
                [LoadMenuAll checkDataMenu:@"2"];
                [LoadProcess dismissLoad];
            }break;
        }
    }
    if (tableView == self.mTableCategory) {
        [dataSearchTag setObject:self.dataCategory[indexPath.row] forKey:[@(srocllIndex) stringValue]];
        [self insertDataMenuCategory:(int)tableView.tag];
        if (tableView.tag == 111) {
            [pscrollView.pageScrollView setIndexPage:(int)srocllIndex+1];
        }
        [self.mCollectionView reloadData];
    }
    else if (tableView == tableListShowMenu) {
        NSDictionary *dic = self.dataMenuList[indexPath.row];
        BOOL blBlock = [dic[@"is_block"] boolValue];
        if (blBlock) {
            [self showMessage:nil];
        }
        else{
             self.mTableTagDrink.backgroundColor = [UIColor whiteColor];
            [popupView btnClosePop:popoverController];
            NSDictionary *menuData = [self.dataMenuList[indexPath.row] mutableCopy];
            [self addOrderAppvoceoption:menuData option:@"" comment:@"" count:1];            
            [self.mTableTagDrink reloadData];
        }
    }
}
-(void) addOrderAppvoceoption:(NSDictionary *)menuData option:(NSString *)option  comment:(NSString *)comment count:(int)count{
    NSDictionary *dataTemp = [self sortDataOrderAdd:menuData option:option comment:comment];
    if ([dataTemp count] != 0) {
        NSNumber *indexP = [NSNumber numberWithInt:[dataTemp[@"index"] intValue]];
        if (indexP != nil) {
            NSMutableDictionary *tagD = [self.addFoodBill[indexP.intValue] mutableCopy];
            NSString *countD = [@([tagD[@"Count"] intValue] + count) stringValue];
            [tagD setObject:countD forKey:@"Count"];
            
            [self.addFoodBill replaceObjectAtIndex:indexP.intValue withObject:tagD];
            
            NSMutableDictionary *tagT = [self.dataTagAdd[indexP.intValue] mutableCopy];
            NSString *countT = [@([tagT[@"count"] intValue] +count) stringValue];
            [tagT setObject:countT forKey:@"count"];
            
            [self.dataTagAdd replaceObjectAtIndex:indexP.intValue withObject:tagT];
        }
    }
    else{
        NSMutableDictionary *_dataT = [NSMutableDictionary dictionary];
        [_dataT setObject:menuData    forKey:@"data"];
        [_dataT setObject:[@(count) stringValue]  forKey:@"count"];
        [self.dataTagAdd addObject:_dataT];
        
        NSString *price = menuData[@"price"];
        NSMutableDictionary *_data = [NSMutableDictionary dictionary];
        [_data setObject:@""    forKey:@"Option"];
        [_data setObject:[@(count) stringValue]   forKey:@"Count"];
        [_data setObject:price  forKey:@"Price"];
        [_data setObject:@""    forKey:@"Comment"];
        [_data setObject:[NSNumber numberWithInt:(int)[self.addFoodBill count]] forKey:@"index"];
        [_data setObject:menuData  forKey:@"data"];
        
        [self.addFoodBill addObject:_data];
    }
}
-(NSDictionary *)sortDataOrderAdd:(NSDictionary *)data  option:(NSString *)option  comment:(NSString *)comment{
    NSArray *listAddFB = [self.addFoodBill mutableCopy];
    NSString *keyBill = data[@"price"];
    NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(Price = %@)" andData:listAddFB];
    if ([listUsaage count] != 0) {
        listUsaage = [NSDictionary searchDictionaryALL:option andvalue:@"(Option = %@)" andData:listUsaage];
        if ([listUsaage count] != 0) {
            listUsaage = [NSDictionary searchDictionaryALL:comment andvalue:@"(Comment = %@)" andData:listUsaage];
            if ([listUsaage count] != 0) {
                NSDictionary *dataFB = [NSDictionary dictionaryWithObject:listUsaage[0]];
                keyBill = data[@"id"];
                listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(id = %@)" andData:[listUsaage valueForKey:@"data"]];
                if ([listUsaage count] != 0) {
                    return dataFB;
                }
            }
        }
    }
    return nil;
}
- (IBAction)showMessage:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                   message:AMLocalizedString(@"เมนูนี้ถูกยกเลิก !", nil)
                                                  delegate:sender
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];

    [message show];
}
- (void)clickTitleTag:(UIButton *)sender{
    
    CellFoodItemView *cell = (CellFoodItemView *)[[sender superview]superview];
    NSIndexPath *indexPath = [self.mTableTagDrink indexPathForCell:cell];
    NSLog(@"clickTitleTag   : %d",(int)indexPath.row);
}
-(void)clickDeleteTag:(UIButton *)sender{
    [sender setTag:888];
//    [self showMessage:sender];
    
    CellFoodItemView *buttonCell = (CellFoodItemView*)[sender superview];
    NSIndexPath *indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
    if (indexPath == nil) {
        buttonCell = (CellFoodItemView*)[[sender superview] superview];
        indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
        if (indexPath == nil) {
            buttonCell = (CellFoodItemView*)[[[sender superview] superview]superview];
            indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
        }
    }
    
    [self.addFoodBill removeObjectAtIndex:indexPath.row];
    self.deleteTag = [NSMutableDictionary dictionary];
    [self.deleteTag setObject:indexPath forKey:@"index"];
    [self.deleteTag setObject:buttonCell.mBtnTttleTag.titleLabel.text forKey:@"title"];
    [self.dataTagAdd removeObjectAtIndex:indexPath.row];
    [self.mTableTagDrink reloadData];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
- (IBAction)showClickDeleteTag:(id)sender {
    btnAlertView = (UIButton *)sender;
    [btnAlertView setTag:88];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:nil
                                                      message:AMLocalizedString(@"ต้องการลบรายการนี้", nil)
                                                     delegate:self
                                            cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                            otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
    
    [message show];
}

-(void)popupClickDeleteTag:(UIButton *)sender{
    CellFoodItemView *buttonCell = (CellFoodItemView*)[sender superview];
    NSIndexPath *indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
    if (indexPath == nil) {
        buttonCell = (CellFoodItemView*)[[sender superview] superview];
        indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
        if (indexPath == nil) {
            buttonCell = (CellFoodItemView*)[[[sender superview] superview]superview];
            indexPath = [self.mTableTagDrink indexPathForCell:buttonCell];
        }
    }
    
    NSLog(@"clickDeleteTag    : %d",(int)indexPath.row);
    [self.addFoodBill removeObjectAtIndex:indexPath.row];
    self.deleteTag = [NSMutableDictionary dictionary];
    [self.deleteTag setObject:indexPath forKey:@"index"];
    [self.deleteTag setObject:buttonCell.mBtnTttleTag.titleLabel.text forKey:@"title"];
    [self.dataTagAdd removeObjectAtIndex:indexPath.row];
    [self.mTableTagDrink reloadData];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
- (NSInteger)numberOfPageInPageScrollView:(OTPageScrollView*)pageScrollView{
    return [_dataPageScrollView[titlePop] count];
}

- (UIView*)pageScrollView:(OTPageScrollView*)pageScrollView viewForRowAtIndex:(int)index{
    
    UIView *cell = [[UIView alloc] initWithFrame:CGRectMake(0,0,pageScrollView.frame.size.width-10,pageScrollView.frame.size.height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,0,cell.frame.size.width, cell.frame.size.height)];
    label.text = _dataPageScrollView[titlePop][index];
    label.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:label];
    return cell;
}

- (CGSize)sizeCellForPageScrollView:(OTPageScrollView*)pageScrollView{
    
    return CGSizeMake(pageScrollView.frame.size.width-10, pageScrollView.frame.size.height);
}

- (void)pageScrollView:(OTPageScrollView *)pageScrollView didTapPageAtIndex:(NSInteger)index{
    
    NSLog(@"click cell at %d",(int)index);
    [self resetDataInsert:index];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (![scrollView isEqual:self.mTableCategory] && ![scrollView isEqual:self.mCollectionView] && ![scrollView isEqual:self.mTableTagDrink]) {
        NSInteger index = scrollView.contentOffset.x / scrollView.frame.size.width;
        NSLog(@"scrollViewDidEndDecelerating cell at %d",(int)index);
        [self resetDataInsert:index];
    }
}
-(void) resetDataInsert:(NSInteger)index{
    srocllIndex = index;
    if (index != 0) {
        if ([dataSearchTag count] != 0 && [dataSearchTag count] > index) {
            [dataSearchTag removeObjectForKey:[@(index) stringValue]];
            srocllIndex = index;
            [self insertDataMenuCategory:111];
            [self.mCollectionView reloadData];
        }
        [self getDataCategory:index+1];
    }
    else{
        [dataSearchTag removeAllObjects];
        [self.dataMenuList removeAllObjects];
        srocllIndex = index;
//        NSArray *keyList = [self.dataMenu allKeys];
//        for (NSString *str in keyList) {
//            [self.dataMenuList addObject:[self.dataMenu valueForKey:str]];
//        }
        self.dataMenuList = [self.dataMenu mutableCopy];
        [self.mCollectionView reloadData];
        [self getDataCategory:index+1];
    }
    
}
- (void) insertDataMenuCategory:(int)tag {
    NSMutableArray *listTemp_ = [NSMutableArray arrayWithArray:self.dataMenu];
    NSArray *keyList = [dataSearchTag allKeys];
    for (NSString *key in keyList) {
        NSDictionary *dic = [dataSearchTag objectForKey:key];
        [self.dataMenuList removeAllObjects];
        NSString *type = (tag == 111)?[@"tag" stringByAppendingString:dic[@"type"]]:@"category_id";
        // [@"tag" stringByAppendingString:dic[@"type"]] ค้นหาแบบ tag
        //  @"category_id"; // ค้นหาแบบ category
        
        for (NSDictionary *dicMenu in listTemp_) {
            if ([dicMenu[type] isEqual:dic[@"id"]]) {
                [self.dataMenuList addObject:dicMenu];
            }
        }
        self.dataMenuList = [self sortDataOrder:self.dataMenuList andKey:@"order"];
        
        listTemp_ = [self.dataMenuList copy];
    }
}
-(void) showDataCategoryList{
    [self.dataCategory removeAllObjects];
//    NSArray *ketList = [self.dataTagDrinkList allKeys];
//    for (NSString *key in ketList) {
//        [self.dataCategory addObject:self.dataTagDrinkList[key]];
//    }
//    [self.mTableCategory reloadData];
    
    
//    for (NSString *key in dataCategoryList) {
//        [self.dataCategory  addObject:[dataCategoryList valueForKey:key]];
//    }
    
    self.dataCategory = [self sortDataOrder:dataCategoryList andKey:@"order"];
    //self.dataCategory = [NSMutableArray arrayWithArray:dataCategoryList];
    [self.mTableCategory setTag:222];
    [self.mTableCategory reloadData];
    
}
- (IBAction)btnBack:(id)sender {
//    [KVNProgress showWithStatus:@"Loading..."];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnEnlarge:(id)sender {
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
    viewControllerShowMenu = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ListOptionFood"];
    viewControllerShowMenu.title = @"Option";
    tableListShowMenu =  viewControllerShowMenu.tableView;
    tableListShowMenu.delegate = self;
    tableListShowMenu.dataSource = self;
    
    viewControllerShowMenu.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    viewControllerShowMenu.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewControllerShowMenu];
    
    UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(viewControllerShowMenu.view.frame.size.width - 50,3,30, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [contentViewController.view addSubview:btnOpiton];
    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}
-(IBAction)btnClosePop:(id)sender{
    [self.mScrollTagCategory resignFirstResponder];
    [popoverController dismissPopoverAnimated:YES];
}
- (IBAction)barButtonItemAction:(id)sender {
    if ([self.addFoodBill count] != 0) {
         isCheckSubmit = YES;
         if ([globalBill.billType  isEqual: kSingleBill]) {
             TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
             if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
                 myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
             }
             [myController.tableDataBillView addOrder:self.addFoodBill andPro:nil];
             [self.navigationController popToViewController:myController animated:NO];
             
             [TableSingleBillViewController loadDataOpenTableSing:myController.detailTable[@"id"]];
         }
        else{
            TableSubBillViewController *myController;
            int  i = 1 ;
            do {
                myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:i];
                i++;
            } while (![myController isKindOfClass:[TableSubBillViewController class]] && i < 10);

            
            [myController.tableDataBillView addOrder:self.addFoodBill andPro:nil];
            [self.navigationController popToViewController:myController animated:NO];
            [TableSubBillViewController loadDataOpenTableSubBill:myController.detailTable[@"id"]];
        }
        
    }
    else{
        [loadProgress.loadView showWithStatusTitle];
    }
 
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    if (searchBar.text.length > 0) {
        searchBar.text = nil;
        [searchBar resignFirstResponder];
        self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenuList];
    }
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSMutableArray *dataMenuListTmp = [NSMutableArray arrayWithArray:self.dataMenuList];
    [self.dataMenuList removeAllObjects];
    NSMutableArray *listData = [NSMutableArray arrayWithArray:self.dataMenu];
//    NSMutableArray *listData = [NSMutableArray array];
    
//    for (NSString *str in keyList) {
//        [listData addObject:[self.dataMenu valueForKey:str]];
//    }
//    listData = [[self.dataMenu allValues] mutableCopy];
    for (NSDictionary *data in listData) {
//        searchText  = [searchText lowercaseString];
        NSString *menu = data[@"index_id" ];
//        NSString *menu = data[@"index_id"];
        
        menu  = [menu lowercaseString];
        NSLog(@"click cell at %@:%@",searchText,menu);
        NSRange aRange = [menu rangeOfString:searchText];
        if (aRange.location == NSNotFound) {
            NSLog(@"string not found");
        } else {
            NSLog(@"string was at index %lu ",(unsigned long)aRange.location);
            [self.dataMenuList addObject:data];
        }
        
//        if ([menu containsString:searchText]) {
//            NSLog(@"string contains bla!");
//            [self.dataMenuList addObject:data];
//        } else {
//            NSLog(@"string does not contain bla");
//        }
    }
    if ([self.dataMenuList count] != 0)
        [self.mCollectionView reloadData];
    else
        self.dataMenuList = dataMenuListTmp;
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBarBegin = NO;
    [self.mLayoutTopTableTagDrink setConstant:-10];
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:NO];
    [self showDataCategoryList];
    [self.mCollectionView reloadData];
    return YES;
}
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    for (UIView *subview in self.mSearcField.subviews){
        for (UIView *subSubview in subview.subviews){
            if ([subSubview conformsToProtocol:@protocol(UITextInputTraits)]){
                UITextField *textField = (UITextField *)subSubview;
                [textField setKeyboardAppearance: UIKeyboardAppearanceAlert];
                textField.returnKeyType = UIReturnKeyDone;
                textField.keyboardType = UIKeyboardTypeNumberPad;
                break;
            }
        }
    }
    searchBarBegin = YES;
    CGFloat sizeTot = 120;
    [self.mLayoutTopTableTagDrink setConstant:-sizeTot];
    searchBar.showsScopeBar = YES;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:YES animated:NO];
    return YES;
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.dataMenuList = [NSMutableArray arrayWithArray:self.dataMenu];
    searchBar.text = nil;
    [searchBar resignFirstResponder];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//    NumberBeverageViewController *_numberBeverage = (NumberBeverageViewController *)[segue destinationViewController];
//    NSIndexPath *ip = [self.mTableMenu indexPathForSelectedRow];
//    _numberBeverage.strTitleBeverage = _dataArray[ip.row];
//    _numberBeverage.priceBeverage = listAddData[ip.row];
}
- (IBAction)btnBackHome:(id)sender {
    UITableViewController *viewController = [[UITableViewController alloc] init];
    viewController.title = AMLocalizedString(@"รายการ", nil);
    tableSreachPop =  viewController.tableView;
    tableSreachPop.delegate = self;
    tableSreachPop.dataSource = self;

    viewController.preferredContentSize = CGSizeMake(200,300);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:WYPopoverArrowDirectionAny animated:NO];
    
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    
    if ([title  isEqual: @"ใช่"]) {
        NSIndexPath *indexPath = self.deleteTag[@"index"];
        [self.mTableTagDrink beginUpdates];
        
        if ([self.dataTagAdd count] == 0) {
            self.dataMenuList  = [self.dataMenu mutableCopy];
        }
        else{
            [self.dataTagAdd removeObjectAtIndex:indexPath.row];
        }
        [self insertDataMenuCategory:111];
        [self.mTableTagDrink deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                  withRowAnimation:UITableViewRowAnimationFade];
        [self.mTableTagDrink endUpdates];
        [self.mTableTagDrink reloadData];
        [self.mCollectionView reloadData];
    }
}
- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateBegan){
        isCheckOrderDB = NO;
        CGPoint p = [sender locationInView:self.mCollectionView];
        NSIndexPath *indexPath = [self.mCollectionView indexPathForItemAtPoint:p];
        NSDictionary *dic =  self.dataMenuList[indexPath.row];
        BOOL blBlock = [dic[@"is_block"] boolValue];
        if (blBlock) {
            return;
        }

        NumberBeverageViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"NumberBeverage"];
        
        viewController_.dataMenuList = dic;
        viewController_.strTitleBeverage = dic[@"name"];
        viewController_.priceBeverage    = dic[@"price"];
        [[self navigationController] pushViewController:viewController_ animated:NO];
    }
}

#pragma mark - UICollectionView Delegate
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dataMenuList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellDelete" forIndexPath:indexPath];
    NSDictionary *dic = self.dataMenuList[indexPath.row];
    BOOL blBlock = [dic[@"is_block"] boolValue];
    [cell.mCountBill setText:[NSString checkNull:dic[@"index_id"]]];
    [cell.mNameTable setText:[NSString checkNull:dic[@"name"]]];
    
    if (blBlock) {
        cell.mCountBill.textColor = [UIColor ht_silverColor];
        cell.mNameTable.textColor = [UIColor ht_silverColor];
        [cell.mImageCheck setImage:[UIImage imageNamed:@"btn_delete"]];
        [cell.mImageCheck setContentMode:UIViewContentModeScaleAspectFit];
    }
    else{
        cell.mCountBill.textColor = [UIColor colorWithWhite:0.200 alpha:1.000];
        cell.mNameTable.textColor = [UIColor colorWithWhite:0.200 alpha:1.000];
        [cell.mImageCheck setImage:nil];
    }
    
    [self setLineView:cell];
    [cell.mViewSub setBackgroundColor:[UIColor colorWithWhite:0.902 alpha:1.000]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = self.dataMenuList[indexPath.row];
    BOOL blBlock = [dic[@"is_block"] boolValue];
    if (blBlock) {
        [self showMessage:nil];
    }
    else{
        self.mTableTagDrink.backgroundColor = [UIColor whiteColor];
        [popupView btnClosePop:popoverController];
//        NSDictionary *menuData = [self.dataMenuList[indexPath.row] mutableCopy];
//        [self addOrderAppvoceoption:menuData option:@"" comment:@"" count:1];
//        [self.mTableTagDrink reloadData];
//        if (searchBarBegin) {
//            [self showDataCategoryList];
//            [self.mSearcField resignFirstResponder];
//        }
        
        
        menuDataSeclect = [NSDictionary dictionaryWithObject:self.dataMenuList[indexPath.row]];
        NSString *textOption = [NSString checkNull:menuDataSeclect[@"cheer_option"]];
        cheerOption  = [NSDictionary dictionaryWithString:textOption];
        BOOL status = false;
        if ([cheerOption count] != 0 && [self.printerDB.isMenuSug isEqualToString:@"1"])  {
            status = [cheerOption[@"is_enabled"] boolValue];
            if (status) {
                [self creteAlerViewMenuOption:cheerOption];
            }
        }
        
        if (!status){
            [self addDataOrderAlerView:menuDataSeclect option:@""];
        } 
    }
}

-(void)setLineView:(id)sender{
    if ([sender isMemberOfClass:[TableCollectionViewCell class]]) {
        TableCollectionViewCell *cell = (TableCollectionViewCell *)sender;
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.clipsToBounds = NO;
        cell.layer.masksToBounds = YES;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    }
    else{
        UITableViewCell *cell = (UITableViewCell *)sender;
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.clipsToBounds = NO;
        cell.layer.masksToBounds = YES;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
    }
    
    
}
-(NSMutableArray *) sortDataOrder:(NSMutableArray *)data1 andKey:(NSString *)key{
    [data1 sortUsingComparator:
     ^(id obj1, id obj2)
     {
         NSInteger value1 = [[obj1 objectForKey:key] intValue];
         NSInteger value2 = [[obj2 objectForKey:key]intValue];
         if (value1 > value2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         if (value1 < value2){
             return (NSComparisonResult)NSOrderedAscending;
         }
         return (NSComparisonResult)NSOrderedSame;
     }];
    
    return  [NSMutableArray arrayWithArray:[data1 mutableCopy]];
}
#pragma mark - UIInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    UIInterfaceOrientation ori = [[UIApplication sharedApplication] statusBarOrientation];
    if (ori == UIInterfaceOrientationPortrait) {
        self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    }
    else{
        self.layout.numberOfItemsPerLine = [Check checkDevice]?3:7;
    }
    [self initNavigation];
}
-(void) creteAlerViewMenuOption:(NSDictionary *)data{
    
    //    1   upsize
    //    2   upsale  --> id ของ menu
    //    3   promotion
    
    int type = [[NSString checkNull:data[@"type"]] intValue];
    NSDictionary *dataDetail  = [NSDictionary dictionaryWithObject:data[@"data"]];
    
    //    NSString *tile = AMLocalizedString(@"คุณต้องการเพิ่ม", nil);
    //    NSString *alertMessage = [[FoodOptionSum class] typeDicTostring:data[@"title"]];
    NSString *alertMessage ;
    switch (type) {
        case 1:{
            NSDictionary *nameD =  [NSDictionary dictionaryWithObject:data[@"title"]];
            alertMessage = [[FoodOptionSum class] typeDicTostring:nameD];
            //            alertMessage = [NSString stringWithFormat:@" %@ %@ %@ %@ %@",tile,name,AMLocalizedString(@"ราคา", nil),price,AMLocalizedString(@"ใช่หรือไม่", nil)];
        }
            break;
        case 2:{
            NSDictionary *nameD =  [NSDictionary dictionaryWithObject:data[@"title"]];
            alertMessage = [[FoodOptionSum class] typeDicTostring:nameD];
            
            NSString *idMenu =  [NSString checkNull:dataDetail[@"id"]];
            menuUpsale = (NSDictionary*)[NSDictionary searchDictionary:idMenu andvalue:@"(id == %@)" andData:self.dataMenu];
            //            alertMessage =  [NSString checkNull:menuUpsale[@"name"]];
            //            alertMessage = [NSString stringWithFormat:@" %@ %@ %@",tile,name,AMLocalizedString(@"ใช่หรือไม่", nil)];
        }
            break;
        case 3:{
            NSDictionary *promotion = [NSDictionary dictionary];
            TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
            if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
                myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
            }
            promotion = [NSDictionary dictionaryWithObject:myController.dataListSingBill[@"promotion"][@"promotion_info"]];
            
            NSString *idMenu =  [NSString checkNull:dataDetail[@"id"]];
            NSString *keyBill = [NSString stringWithFormat:@"id_%@",idMenu];
            menuUpPromotion = [NSDictionary dictionaryWithObject:promotion[keyBill]];
            
            if ([menuUpPromotion count] != 0) {
                alertMessage =  [NSString checkNull:menuUpPromotion[@"name"]];
                //                alertMessage = [NSString stringWithFormat:@" %@ %@ %@",tile,name,AMLocalizedString(@"ใช่หรือไม่", nil)];
            }
            
        }
            break;
            
        default:
            break;
    }
    
    NSString *alertTitle = [NSString stringWithFormat:@" %@ ",AMLocalizedString(@"แนะนำ", nil)];
    
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertTitle
                                                      message:alertMessage
                                                     delegate:self
                                            cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                            otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
    [message setTag:999];    
    [message show];
}
-(void) actionAlerViewMenu{
    
    //    1   upsize
    //    2   upsale  --> id ของ menu
    //    3   promotion
    
    int type = [[NSString checkNull:cheerOption[@"type"]] intValue];    
    switch (type) {
        case 1:{
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:menuDataSeclect];
            NSDictionary *dataDetail = [NSDictionary dictionaryWithObject:cheerOption[@"data"]];
            int price1 = [[NSString checkNull:dataDetail[@"price"]] intValue];
            int price2 = [[NSString checkNull:dic[@"price"]] intValue];            
            price2 += price1;
            
            [dic setObject:[@(price2) stringValue] forKey:@"price"];
            
            [self addDataOrderAlerView:dic option:@[dataDetail]];
        }            
            break;
        case 2:{
            [self addDataOrderAlerView:menuDataSeclect option:@""];
            [self addDataOrderAlerView:menuUpsale option:@""];
        }
            break;
        case 3:{            
            [self sendPromotion];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UIAlertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if ((buttonIndex == 1 && btnAlertView.tag == 88)) {
        [self clickDeleteTag:btnAlertView];
    }
    else if (buttonIndex == 1 && alertView.tag == 999 ){
        [self actionAlerViewMenu];
    }
    else if (buttonIndex == 0 && alertView.tag == 999 ){
        [self addDataOrderAlerView:menuDataSeclect option:@""];
    }
    
}
-(void) addDataOrderAlerView:(NSDictionary *)data option:(id)list{
    [self addOrderAppvoceoption:data option:list comment:@"" count:1];
    [self.mTableTagDrink reloadData];
    if (searchBarBegin) {
        [self showDataCategoryList];
        [self.mSearcField resignFirstResponder];
    }
}

-(void)sendPromotion{ 
    [self initOrderDB];
    if ([globalBill.billType  isEqual: kSingleBill]) {
        TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
        if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
            myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
        }
        NSArray *arrList = [NSArray arrayWithObjects:myController.tableDataBillView.promotionPoolID,menuUpPromotion,@"",nil];
        [myController.tableDataBillView addPromotion:arrList andDataOrder:nil];
        [self.navigationController popToViewController:myController animated:NO];
    }
    else{
        TableSubBillViewController *myController;
        int  i = 1 ;
        do {
            myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:i];
            i++;
        } while (![myController isKindOfClass:[TableSubBillViewController class]] && i < 10);
        
        NSArray *arrList = [NSArray arrayWithObjects:myController.tableDataBillView.promotionPoolID,menuUpPromotion,@"",nil];
        [myController.tableDataBillView addPromotion:arrList andDataOrder:nil];
        [self.navigationController popToViewController:myController animated:NO];
    }
}

@end
