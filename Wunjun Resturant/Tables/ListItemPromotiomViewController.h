//
//  ListItemPromotiomViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSingleBillViewController.h"
#import "TableSubBillViewController.h"

#import "UIColor+HTColor.h"
#import "LoadProcess.h"

@interface ListItemPromotiomViewController : UIViewController

@property (weak, nonatomic  ) IBOutlet UITableView     *mTableSelectProduct;
@property (weak, nonatomic  ) IBOutlet UITableView     *mTableSelectedItems;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *mReplyAddMixer;
@property (weak, nonatomic  ) IBOutlet UILabel         *mTitlePro;

@property (nonatomic,strong)  TableSingleBillViewController *tableSingleBillViewController;
@property (nonatomic,strong)  TableSubBillViewController *tableSubBillViewController;
 
@property (strong, nonatomic) NSString            *locTitle;
@property (strong, nonatomic) NSString            *promotionRelateId;
@property (strong, nonatomic) NSMutableDictionary *dataPromotion;
@property (strong, nonatomic) NSDictionary        *dataPromotionPool;
@property (strong, nonatomic) NSDictionary        *dataPromotionInfo;
@property (strong, nonatomic) NSDictionary        *dataOrder;

- (IBAction)btnBack:(id)sender;

@end
