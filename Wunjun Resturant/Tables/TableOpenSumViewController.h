//
//  TableOpenSumViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/4/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "CashTableStatus.h"

#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "LoadProcess.h"

@interface TableOpenSumViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UITextField *mCountWomen;
@property (weak, nonatomic) IBOutlet UITextField *mCountMen;
@property (weak, nonatomic) IBOutlet UILabel *mTimeTable;
@property (weak, nonatomic) IBOutlet UILabel *mCountTotal;
@property (weak, nonatomic) IBOutlet UITextField *otpTableTxt;
@property (weak, nonatomic) IBOutlet UITextField *otpValTxt;

@property (nonatomic,strong)  NSMutableArray *dataListZoneAll;
@property (nonatomic,strong)  NSArray *dataListTablesAll;
@property (nonatomic,strong)  NSDictionary *dataTable;
@property (nonatomic,strong)  NSString *indexOpen;
 
@property (nonatomic ,strong) NSMutableDictionary *addOpenTable;
@property (nonatomic,strong) CashTableStatus *cashTableStatus;

- (IBAction)btnBack:(id)sender;
- (IBAction)mTagKeybroad:(id)sender;
- (IBAction)btClickOpenTable:(id)sender;
@end
