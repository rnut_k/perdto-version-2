//
//  TableBeverageListViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellFoodItemView.h"
#import "OTPageScrollView.h"
#import "OTPageView.h"
#import "LoadProcess.h"

@interface TableBeverageListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollTagCategory;
@property (weak, nonatomic) IBOutlet UITableView *mTableTagDrink;
//@property (weak, nonatomic) IBOutlet UITableView *mTableMenu;
@property (weak, nonatomic) IBOutlet UITableView *mTableCategory;
//@property (weak, nonatomic) IBOutlet UISearchBar *searchField;
@property (weak, nonatomic) IBOutlet UICollectionView *mCollectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *mSearcField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutTopTableTagDrink;

@property (nonatomic,strong) NSMutableArray *dataMenu;
@property (nonatomic,strong) NSMutableArray *dataCategory;
@property (nonatomic,strong) NSMutableDictionary *dataTagDrinkList;
@property (nonatomic,strong) NSMutableArray *dataMenuList;   // เก็บข้อมูลเมนูโชว์
@property (nonatomic,strong) NSMutableArray *dataTagAdd;
@property (nonatomic,strong) NSMutableArray *addFoodBill;


@property (nonatomic,strong)  NSMutableDictionary *deleteTag;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnEnlarge:(id)sender;
- (IBAction)btnBackHome:(id)sender;

- (IBAction)btnTagSelectorMenu:(UILongPressGestureRecognizer *)sender;

-(void) addOrderAppvoceoption:(NSDictionary *)menuData option:(NSString *)option  comment:(NSString *)comment count:(int)count;
@end
