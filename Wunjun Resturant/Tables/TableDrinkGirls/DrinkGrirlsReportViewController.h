//
//  DrinkGrirlsReportViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSubBillViewController.h"
#import "TableSingleBillViewController.h"

@interface DrinkGrirlsReportViewController : UIViewController<CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *mNameDrink;
@property (weak, nonatomic) IBOutlet UIView *mContainerView;
@property (weak, nonatomic) IBOutlet UILabel *mPriceDrink;
@property (weak, nonatomic) IBOutlet UIButton *mBtnAddDrink;

@property (nonatomic,strong) NSMutableDictionary *drinkMenu;
@property (nonatomic,strong) NSString *typeDr;
@property (nonatomic,strong) NSString *typeDrinkMenu;

@property (nonatomic ,strong) TableSingleBillViewController *tableSingleBillViewControll;
@property (nonatomic ,strong) TableSubBillViewController *tableSubBillViewController;

- (IBAction)btnAddDrinkGrils:(id)sender;

- (IBAction)getImageBtnPressed:(UIImage *)sender;
@end
