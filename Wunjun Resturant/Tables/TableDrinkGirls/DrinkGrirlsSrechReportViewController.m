//
//  DrinkGrirlsSrechReportViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "DrinkGrirlsSrechReportViewController.h"
#import "TableReportDrinkGirlsTableViewController.h"
#import "TableBillMenuViewController.h"
#import "TableSingleBillViewController.h"

#import "GlobalBill.h"
#import "MenuDB.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Member.h"
#import "Constants.h"
#import "LoadProcess.h"
#import "NSString+GetString.h"
#import "Check.h"

@interface DrinkGrirlsSrechReportViewController (){
    WYPopoverController *popoverController;
    LoadProcess *loadProgress;
    UILabel *toolTipLabel;
    UIView *bgView;
    
    UITextField *tempDate;
    
    UIButton *btnOpiton;
    UIButton *btnOpiton1;
    
    NSString *startTime;
    NSString *stopTime;
}
@property (nonatomic,strong) GlobalBill *globalBill;
@property (nonatomic,strong) Member *member;
@end

@implementation DrinkGrirlsSrechReportViewController
#pragma mark View Lifecycle

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

-(GlobalBill *)globalBill{
    if (!_globalBill) {
        _globalBill = [GlobalBill sharedInstance];
    }
    return _globalBill;
}
- (void)loadView {
    [super loadView];
    loadProgress = [LoadProcess sharedInstance];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:AMLocalizedString(@"สั่งดริ้งค์", nil)];
    
    UIImage *image = [UIImage imageNamed:@"ic_calendar"];
    
    self.mStartTime.leftViewMode = UITextFieldViewModeAlways;
    self.mStartTime.leftView = [[UIImageView alloc] initWithImage:image];
    
    self.mStopTime.leftViewMode = UITextFieldViewModeAlways;
    self.mStopTime.leftView = [[UIImageView alloc] initWithImage:image];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.mNameDrink setText:self.nameDrick];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadData{
    
    if (![Check checkNull:stopTime]) {
        if ([Check checkNull:startTime]) {
            startTime = [Time getTimeandDate];
        }        
        NSDictionary *parameters = @{@"code":self.member.code,
                                     @"girl_id":self.member.girlID,
                                     @"datetime":@{
                                             @"start_datetime":startTime,
                                             @"end_datetime":stopTime
                                             }};
        
        NSString *url = [NSString stringWithFormat:GET_REPORT_DREINK_GIRL,self.member.nre];
        
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if ([json[@"status"] boolValue]) {
                NSDictionary *dataAllZone = json[@"data"];
                [self loadDataTable:dataAllZone];
            }
        }];
    }
    else
        [loadProgress.loadView showWithStatusAddError:@"กรุณาระบุวันที่"];
}
-(NSMutableArray *)sortTimedate:(NSDictionary *)data{
    NSString *timeTemp = @"";
    NSMutableArray *arrList = [NSMutableArray array];
    NSMutableArray *arrListsub = [NSMutableArray array];
    NSDictionary *orders = [NSDictionary dictionaryWithObject:data[@"order_data"]];
    NSArray *orderList = [NSArray arrayWithArray:[Check checkObjectType:orders[@"orders"]]];
    for (NSDictionary *dic in orderList) {
        NSString *time = [Time getDateFromDatetime:dic[@"created_datetime"]];
        if ([time isEqual:timeTemp] || [timeTemp isEqual:@""]) {
            [arrListsub addObject:dic];
            timeTemp = time;
        }
        else{
            [arrList addObject:[arrListsub copy]];
            [arrListsub removeAllObjects];
            [arrListsub addObject:dic];
            timeTemp = time;
        }
    }
    if ([arrListsub count] != 0) {
        [arrList addObject:[arrListsub copy]];
    }
    return arrList;
}
-(void)loadDataTable:(NSDictionary *)data{
    [self.mContainerView setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
    TableReportDrinkGirlsTableViewController *tableReport = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableReportDrinkGirls"];
    [tableReport setBlClassSreach:YES];
    
    NSDictionary *orders = [NSDictionary dictionaryWithObject:data[@"order_data"]];
    NSDictionary *orderList1 = [NSDictionary dictionaryWithObject:data[@"table_data"]];
    NSString *total = [[NSNumber numberWithLong:[orders[@"total"] doubleValue]] stringValue];
    [self.mPriceDrink setText:[NSString conventCurrency:total]];
    
    [tableReport setDataOrderDrink:[self sortTimedate:data]];
    [tableReport setDataTableDrink:orderList1];
    
    tableReport.view.frame = self.mContainerView.bounds;
    [self.mContainerView addSubview:tableReport.view];
    [self addChildViewController:tableReport];
    [tableReport didMoveToParentViewController:self];
}
- (IBAction)createPopover:(id)sender {
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.view.frame.size.width,300)];
    bgView.backgroundColor = [UIColor colorWithWhite:0.888 alpha:0.970];
    [self.view addSubview:bgView];

    CGSize size = bgView.frame.size;
    calVC = [[OCCalendarViewController alloc] initAtPoint:CGPointMake((size.width/2)+7,40)
                                                   inView:bgView
                                            arrowPosition:OCArrowPositionNone];
    calVC.delegate = self;
    calVC.selectionMode = OCSelectionSingleDate;
    [self.view addSubview:calVC.view];
    
    btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton setFrame:CGRectMake(10,10,(size.width/2)-10, 30)];
    [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton setBackgroundImage:[UIImage imageNamed:@"btn_green"] forState:UIControlStateNormal];
    [btnOpiton setTitle:AMLocalizedString(@"ยันยืน", nil) forState:UIControlStateNormal];
    [btnOpiton addTarget:self action:@selector(handleSingleTapGesture:) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [self.view addSubview:btnOpiton];
    
    btnOpiton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnOpiton1 setTag:11];
    [btnOpiton1 setFrame:CGRectMake(btnOpiton.frame.size.width + 10,10,(size.width/2)-10,30)];
    [[btnOpiton1 imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [btnOpiton1 setBackgroundImage:[UIImage imageNamed:@"btn_red"] forState:UIControlStateNormal];
    [btnOpiton1 setTitle:AMLocalizedString(@"ไม่", nil) forState:UIControlStateNormal];
    [btnOpiton1 addTarget:self action:@selector(completedWithNoSelection) forControlEvents:UIControlEventTouchUpInside];
    btnOpiton1.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    [self.view addSubview:btnOpiton1];
}
-(void)btnClosePop:(WYPopoverController *)sender{

}
#pragma  – mark Textfield Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    NSLog(@"textFieldDidBeginEditing");
    tempDate = textField;
    [self createPopover:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
//    NSLog(@"textFieldShouldEndEditing");
    textField.backgroundColor = [UIColor whiteColor];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
//    NSLog(@"textField:shouldChangeCharactersInRange:replacementString:");
    if ([string isEqualToString:@"#"]) {
        return NO;
    }
    else {
        return YES;
    }
}
#pragma mark OCCalendarDelegate Methods

- (void)completedWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"ICT"]];
    
    if ([tempDate isEqual:self.mStartTime]) {
        startTime = [dateFormatter stringFromDate:startDate];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString  = [dateFormatter stringFromDate:startDate];;
        [self.mStartTime setText:dateString];
    }
    else{
        stopTime = [dateFormatter stringFromDate:startDate];
        [dateFormatter setDateFormat:@"YYYY-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:endDate];
        [self.mStopTime setText:dateString];
    }
    [self completedWithNoSelection];
}

-(void) completedWithNoSelection{
    [btnOpiton removeFromSuperview];
    [btnOpiton1 removeFromSuperview];
    [bgView removeFromSuperview];
    [calVC.view removeFromSuperview];
    calVC.delegate = nil;
    calVC = nil;
}
-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGesture{
    [calVC performSelector:@selector(removeCalView) withObject:nil afterDelay:0.4f];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)btnCommit:(id)sender {
    [self loadData];
}
@end
