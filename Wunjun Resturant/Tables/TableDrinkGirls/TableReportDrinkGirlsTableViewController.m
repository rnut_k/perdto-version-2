//
//  TableReportDrinkGirlsTableViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableReportDrinkGirlsTableViewController.h"
#import "TableBillMenuViewController.h"
#import "TableSingleBillViewController.h"

#import "Check.h"
#import "GlobalBill.h"
#import "UIColor+HTColor.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Member.h"
#import "UIImageView+AFNetworking.h"
#import "PopupView.h"
#import "MenuDB.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"
#import "LoadProcess.h"
#import "NSString+GetString.h"


#define kCellReportStop (@"CellReportDrinkGirlsStop")
#define kCellReport     (@"CellReportDrinkGirls")

@interface TableReportDrinkGirlsTableViewController ()<WYPopoverControllerDelegate>{
    PopupView *popupView;
    WYPopoverController *popoverController;
    
    NSIndexPath *indexPathPop;
    NSMutableDictionary *drinkMenu;
}

@property (nonatomic ,strong) Member *member;
@end

@implementation TableReportDrinkGirlsTableViewController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    MenuDB *menuDB = [MenuDB getInstance];
    [menuDB loadData:@"menu_0"];
    NSDictionary *json = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    drinkMenu = [NSMutableDictionary dictionaryMutableWithObject:json[@"drink_menu"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.blClassSreach)
        return 40;
    
    return 0;
    
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (self.blClassSreach){
        NSString *title = self.dataOrderDrink[section][0][@"start_drink_time"];
        title = [Time getICTFormateDate:title];
        return title;
    }
    
    return @"";
    
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,40)];
    [headerView setBackgroundColor:[UIColor colorWithWhite:0.600 alpha:1.000]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,10,tableView.bounds.size.width-20,24)];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    [label setTextColor:[UIColor whiteColor]];
    label.font = [UIFont boldSystemFontOfSize:16];
    [headerView addSubview:label];
    
    return headerView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (self.blClassReport)
        return 1;
    else if (self.blClassSreach)
        return [self.dataOrderDrink count];
    
    return 0;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (self.blClassReport)
        return [self.dataOrderDrink count];
    else if (self.blClassSreach)
        return [self.dataOrderDrink[section] count];
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *type;
    NSString *dateStop = @"";
    if (self.blClassReport) {
        type = self.dataOrderDrink[indexPath.row][@"drinkgirl_type"];
        dateStop = [Time conventTime:self.dataOrderDrink[indexPath.row][@"stop_drink_time"]];
    }
    else if (self.blClassSreach){
        type = self.dataOrderDrink[indexPath.section][indexPath.row][@"drinkgirl_type"];
        dateStop = [Time conventTime:self.dataOrderDrink[indexPath.section][indexPath.row][@"stop_drink_time"]];
    }
    
    if ([type isEqual:@"2"] && [dateStop isEqual:@"00:00"])
        return 105;
    else
        return 65;
    
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataOrder = [NSDictionary dictionary];
    if (self.blClassReport) {
        dataOrder = self.dataOrderDrink[indexPath.row];
    }
    else if (self.blClassSreach){
        dataOrder = self.dataOrderDrink[indexPath.section][indexPath.row];
    }
    NSString *identitier = kCellReport;
    
    NSString *drinkID               = [NSString checkNull:dataOrder[@"shot_type"]];
    NSDictionary *dataDrinkGirlList = drinkMenu[[NSString stringWithFormat:@"id_%@",drinkID]];
    NSString *drinkgirlType =[NSString stringWithFormat:@"%@ ( %@ )",[self getTypeDrink:dataOrder[@"drinkgirl_type"]],dataDrinkGirlList[@"name"]];
    NSString *dateStop              = [Time conventTime:dataOrder[@"stop_drink_time"]];
    
    if ([[self getTypeDrink:dataOrder[@"drinkgirl_type"]] isEqual:kDrinkTwo] && [dateStop isEqual:@"00:00"]) {
        identitier = kCellReportStop;
    }
    
    CellReportDrinkGirls *cell = [tableView dequeueReusableCellWithIdentifier:identitier];
    if (cell == nil) {
        cell = [[CellReportDrinkGirls alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:identitier];
    }
    @try {
        NSString *dateStrat2      = [NSString stringWithFormat:@"เวลา %@",[Time conventTime:dataOrder[@"start_drink_time"]]];
        NSString *dateStrat1      = [NSString stringWithFormat:@"เวลา เริ่ม  %@",[Time conventTime:dataOrder[@"start_drink_time"]]];

        NSString *stop_drink_time = [Time conventTime:dataOrder[@"stop_drink_time"]];
        NSString *dateStop        = [NSString stringWithFormat:@"หยุด  %@",stop_drink_time];

        NSString *table_usage_id  = [self getDetailTable:dataOrder[@"table_usage_id"]];
        NSString *tableName       = [NSString stringWithFormat:@"โต๊ะ : %@",table_usage_id];
        
        NSString *timeD = [NSString conventCurrency:[NSString stringWithFormat:@"%@",dataOrder[@"total"]]];
        if ([dataOrder[@"deliver_status"] isEqual:@"4"]) {
            [cell.mStartTime setAttributedText:[NSString checkDeliverStatus:dateStrat1]];
            [cell.mPriceDrink setAttributedText:[NSString checkDeliverStatus:timeD]];
            [cell.mDrinkType setAttributedText:[NSString checkDeliverStatus:drinkgirlType]];
            [cell.mTableID setAttributedText:[NSString checkDeliverStatus:tableName]];
            if ([drinkgirlType isEqual:kDrinkTwo] && [stop_drink_time isEqual:@"00:00"]) {
                [cell.mDateTime setAttributedText:[NSString checkDeliverStatus:[Time getTime]]];
            }
            else{
                [cell.mStopTime setAttributedText:[NSString checkDeliverStatus:dateStop]];
            }
        }
        else{
            [cell.mDrinkType  setText:drinkgirlType];
            [cell.mPriceDrink setText:timeD];
            [cell.mTableID    setText:tableName];
            
            if ([[self getTypeDrink:dataOrder[@"drinkgirl_type"]] isEqual:kDrinkTwo]) {
                if ([stop_drink_time isEqual:@"00:00"]) {
                    [cell.mDateTime setText:[Time getTime]];
                    [cell.mBtnStop addTarget:self
                                      action:@selector(stopDate:)
                            forControlEvents:UIControlEventTouchUpInside];
                }
                else{
                    [cell.mStartTime  setText:dateStrat1];
                    [cell.mStopTime setText:dateStop];
                }
            }
            else{
                [cell.mStartTime  setText:dateStrat2];
                [cell.mStopTime  setText:@""];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"error exception %@",exception);
    }
    return cell;
}
-(NSString *)getDetailTable:(NSString *)key{
    NSString *table_usage_id = [NSString stringWithFormat:@"id_%@",key];
    NSDictionary *dataUsage = self.dataTableDrink[@"usage_table"][table_usage_id];

    if ([dataUsage count] != 0)
        return dataUsage[@"label"];
    else
        return @"ไม่มี";
}
-(NSString *)getTypeDrink:(NSString *)ind{
    if ([ind isEqual:@"1"]) {
        return kDrinkOne;
    }
    else if ([ind isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else if ([ind isEqual:@"3"]) {
        return kDrinkThree;
    }
    else{
        return nil;
    }
}
- (void)stopDate:(id)sender{
    UIButton *btn = (UIButton *)sender;
    UITableViewCell *buttonCell1 = (UITableViewCell*)[btn superview];
    UITableViewCell *buttonCell2 = (UITableViewCell*)[[btn superview] superview];
    UITableViewCell *buttonCell3 = (UITableViewCell*)[[[btn superview] superview]superview];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:buttonCell1];
    if (indexPath == nil) {
        indexPath = [self.tableView  indexPathForCell:buttonCell2];
        if (indexPath == nil) {
            indexPath = [self.tableView  indexPathForCell:buttonCell3];
        }
    }
    NSIndexPath *index = [NSIndexPath indexPathForRow:999 inSection:999];
    NSDictionary *dataDict = @{@"dataorder":self.dataOrderDrink[indexPath.row],
                               @"drinkGirl":self.dataDrinkGrink,
                               @"index":index};
    [self postNotificationName:@"checkStopDrink" and:dataDict];
}
-(void)postNotificationName:(NSString *)name and:(NSDictionary *)dataDict{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center postNotificationName:name
                          object:self
                        userInfo:dataDict];
}
-(IBAction)imageSingSubBill:(UILongPressGestureRecognizer *)sender {
    @try {
        if (sender.state == UIGestureRecognizerStateBegan){
            CGPoint p = [sender locationInView:self.tableView];
            indexPathPop = [self.tableView indexPathForRowAtPoint:p];
            NSDictionary *dataOrder = [NSDictionary dictionary];
            if (self.blClassReport) {
                dataOrder = self.dataOrderDrink[indexPathPop.row];
            }
            else if (self.blClassSreach){
                dataOrder = self.dataOrderDrink[indexPathPop.section][indexPathPop.row];
            }
            int tag1 = 11;
            NSString *drinkgirl_type = [self getTypeDrink:dataOrder[@"drinkgirl_type"]];
            if ([drinkgirl_type isEqual:kDrinkTwo]) {
                tag1 = 22;
            }
         
            popupView = [[PopupView alloc] init];
            [popupView setTanPopup:tag1];
            [popupView setTableReportDrinkGirls:self];
            [popupView showPopupWithStyle:CNPPopupStyleCentered];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@" error btnDetailMenuPop : %@",exception);
    }
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
    
    if (![Check checkNull:title] && ![title isEqual:AMLocalizedString(@"ไม่", nil)]) {
        UIViewController  *viewController = [[UIViewController alloc] init];
        viewController.title = AMLocalizedString(@"ลายเซ็นต์", nil);
        
        viewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-100);
        viewController.modalInPopover = NO;
        UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
        popoverController.delegate = self;
        [popoverController presentPopoverFromRect:CGRectZero
                                           inView:self.view
                         permittedArrowDirections:WYPopoverArrowDirectionNone
                                         animated:NO];
        NSLog(@"%f",popoverController.popoverContentSize.height);
        CGSize size = popoverController.popoverContentSize;
        
        UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnOpiton setFrame:CGRectMake(size.width - 40,3,30, 30)];
        [[btnOpiton imageView] setContentMode: UIViewContentModeScaleAspectFit];
        [btnOpiton setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [btnOpiton addTarget:self action:@selector(btnClosePop:) forControlEvents:UIControlEventTouchUpInside];
        btnOpiton.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        [contentViewController.view addSubview:btnOpiton];
        
        NSMutableDictionary *dataOrder;
        if (self.blClassReport) {
            dataOrder = self.dataOrderDrink[indexPathPop.row];
        }
        else if (self.blClassSreach){
            dataOrder = self.dataOrderDrink[indexPathPop.section][indexPathPop.row];
        }
        
        
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setFrame:CGRectMake(0,0,size.width,size.height)];
        NSString *_singImage = dataOrder[@"sign"];
        NSDictionary *dataSing = [NSDictionary dictionaryWithString:_singImage];
        NSString *pahtstart = dataSing[@"start"];
        NSString *pahtstop = dataSing[@"stop"];
        if ([title isEqual:AMLocalizedString(@"แสดงลายเซ็นต์ stop", nil)]) {
            [imageView setImageWithURL:[NSURL URLWithString:pahtstop]];
        }
        else {
            [imageView setImageWithURL:[NSURL URLWithString:pahtstart]];
        }
        
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [contentViewController.view addSubview:imageView];
    }
   
}
- (IBAction)btnClosePop:(id)sender {
    [popoverController dismissPopoverAnimated:YES];
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
