//
//  TableReportDrinkGirlsTableViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellReportDrinkGirls.h"
#import "CNPPopupController.h"

@interface TableReportDrinkGirlsTableViewController : UITableViewController<CNPPopupControllerDelegate>

@property (nonatomic,strong) NSArray *dataOrderDrink;
@property (nonatomic,strong) NSDictionary *dataTableDrink;
@property (nonatomic,strong) NSDictionary *dataDrinkGrink;

@property (nonatomic) BOOL blClassReport;
@property (nonatomic) BOOL blClassSreach;

-(IBAction)imageSingSubBill:(UILongPressGestureRecognizer *)sender;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title ;
@end
