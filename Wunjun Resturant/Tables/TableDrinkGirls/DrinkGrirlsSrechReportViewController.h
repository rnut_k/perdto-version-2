//
//  DrinkGrirlsSrechReportViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYStoryboardPopoverSegue.h"
#import "OCCalendarViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface DrinkGrirlsSrechReportViewController : UIViewController<UIGestureRecognizerDelegate,OCCalendarDelegate,WYPopoverControllerDelegate> {
    OCCalendarViewController *calVC;
}

@property (weak, nonatomic) IBOutlet UIView *mContainerView;
@property (weak, nonatomic) IBOutlet UITextField *mStartTime;
@property (weak, nonatomic) IBOutlet UITextField *mStopTime;
@property (weak, nonatomic) IBOutlet UILabel *mPriceDrink;
@property (weak, nonatomic) IBOutlet UILabel *mNameDrink;

@property (nonatomic ,strong) NSString *nameDrick;

- (IBAction)btnCommit:(id)sender;

@end
