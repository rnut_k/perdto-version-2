//
//  CellReportDrinkGirls.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellReportDrinkGirls : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *mDrinkType;
@property (weak, nonatomic) IBOutlet UILabel *mTableID;
@property (weak, nonatomic) IBOutlet UILabel *mStartTime;
@property (weak, nonatomic) IBOutlet UILabel *mStopTime;
@property (weak, nonatomic) IBOutlet UILabel *mPriceDrink;
@property (weak, nonatomic) IBOutlet UILabel *mDateTime;
@property (weak, nonatomic) IBOutlet UIButton *mBtnStop;


@end
