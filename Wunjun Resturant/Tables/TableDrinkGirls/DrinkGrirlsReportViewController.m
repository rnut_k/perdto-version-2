//
//  DrinkGrirlsReportViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/22/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "DrinkGrirlsReportViewController.h"
#import "TableReportDrinkGirlsTableViewController.h"
#import "TableBillMenuViewController.h"
#import "TableSingleBillViewController.h"
#import "CustomNavigationController.h"
#import "DrinkGrirlsSrechReportViewController.h"

#import "Check.h"
#import "GlobalBill.h"
#import "UIColor+HTColor.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Member.h"
#import "UIImageView+AFNetworking.h"
#import "PopupView.h"
#import "MenuDB.h"
#import "Time.h"
#import "NSDictionary+DictionaryWithString.h"
#import "LoadMenuAll.h"
#import "NSString+GetString.h"
#import "AppDelegate.h"

@interface DrinkGrirlsReportViewController (){
    PopupView *popupView;
    LoadProcess *loadProgress;
    NSString *blstatus ;
    
    NSDictionary *dataDrinkGirlList;
    NSMutableDictionary *drinkGirlList;
    NSMutableArray *menuList;
    
    CustomNavigationController *vc;
}
@property (nonatomic,strong) GlobalBill *globalBill;
@property (nonatomic,strong) Member *member;
@property (nonatomic,strong) MenuDB *menuDB;
@end

@implementation DrinkGrirlsReportViewController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

-(GlobalBill *)globalBill{
    if (!_globalBill) {
        _globalBill = [GlobalBill sharedInstance];
    }
    return _globalBill;
}

-(MenuDB *)menuDB{
    if (!_menuDB) {
        _menuDB = [MenuDB  getInstance];
        [_menuDB loadData:@"menu_3"];
    }
    return _menuDB;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigation];
    
    [self setTitle:AMLocalizedString(@"สั่งดริ้งค์", nil)];
    
    loadProgress = [LoadProcess sharedInstance];
    [self loadData];
    [self loadDataStatus];
    [self loadDataDrink];
    
    NSString *numberTable;
    if ([self.tableSubBillViewController isKindOfClass:[TableSubBillViewController class]]) {
        TableSubBillViewController *tableSub = (TableSubBillViewController *)self.tableSubBillViewController;
        numberTable = tableSub.detailTable[@"label"];
    }
    else{
        numberTable = self.tableSingleBillViewControll.numberTable;
    }
    
    NSString *titleBtn = [NSString stringWithFormat:@"%@ %@",self.mBtnAddDrink.titleLabel.text,numberTable];
    [self.mBtnAddDrink setTitle:titleBtn forState:UIControlStateNormal];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadData)
                                                 name:@"DrinkGirlsloadData"
                                               object:nil];
}
- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DrinkGirlsloadData" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)initNavigation{
    UIImage *image;
    CGSize size = self.navigationController.navigationBar.frame.size;
    if ([Check checkDevice]) { // iphione
        image = [UIImage imageNamed:@"top_drink@2x"];
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
            image = [UIImage imageNamed:@"top_drink@3x"];
        }
    }
    else{ // ipan
        image = [UIImage imageNamed:@"top_drink@3x"];
    }
    image = [self imageWithImage:image scaledToSize:CGSizeMake(size.width,size.height+22)];
    [self.navigationController.navigationBar setBackgroundImage:image
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)loadData{    
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"girl_id":self.member.girlID};
    
    NSString *url = [NSString stringWithFormat:GET_REPORT_DREINK_GIRL,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        [loadProgress.loadView showProgress];
        BOOL bl = [json[@"status"] boolValue];
        if (bl) {
            NSMutableDictionary *dataAll = json[@"data"];
            [self loadDataTable:dataAll];
        }
    }];
}
-(void)loadDataDrink{
    NSDictionary *json = [NSDictionary dictionaryWithString:self.menuDB.valueMenu];
    drinkGirlList = [NSMutableDictionary dictionaryMutableWithObject:json[@"drinkgirl"]];
    menuList = [NSMutableArray arrayWithArray:(NSMutableArray *)[Check checkObjectType:[json[@"menu"] allValues]]];
    self.drinkMenu = [NSMutableDictionary dictionaryMutableWithObject:json[@"drink_menu"]];
    NSString *girlID  = self.member.girlID;
    dataDrinkGirlList = drinkGirlList[[NSString stringWithFormat:@"id_%@",girlID]];
    NSString *name = [NSString stringWithFormat:@"%@ \n %@",AMLocalizedString(@"ยอดรวมของ", nil),dataDrinkGirlList[@"name"]];
    [self.mNameDrink setText:name];
}
-(void)loadDataTable:(NSDictionary *)data{
    [self.mContainerView setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
    TableReportDrinkGirlsTableViewController *tableReport = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableReportDrinkGirls"];
    [tableReport setBlClassReport:YES];
    
    NSDictionary *orders = [NSDictionary dictionaryWithObject:data[@"order_data"]];
    NSArray *orderList = [NSArray arrayWithArray:[Check checkObjectType:orders[@"orders"]]];
    NSDictionary *orderList1 = [NSDictionary dictionaryWithObject:data[@"table_data"]];
    NSString *total = [[NSNumber numberWithLong:[orders[@"total"] doubleValue]] stringValue];
    [self.mPriceDrink setText:[NSString conventCurrency:total]];
    
    [tableReport setDataOrderDrink:orderList];
    [tableReport setDataTableDrink:orderList1];
    [tableReport setDataDrinkGrink:dataDrinkGirlList];

    tableReport.view.frame = self.mContainerView.bounds;
    [self.mContainerView addSubview:tableReport.view];
    [self addChildViewController:tableReport];
    [tableReport didMoveToParentViewController:self];
}

#pragma mark - createPopover
-(void)createPopover{
    popupView = [[PopupView alloc] init];
    [popupView setStatus:blstatus];
    [popupView setDrinkGrirlsReportViewController:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
-(void)loadDataStatus{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_STATUS_DREINK_GIRL,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        BOOL bl = [json[@"status"] boolValue];
        if (bl) {
            NSString *idg = [NSString stringWithFormat:@"id_%@",self.member.girlID];
            if ([json[@"data"] count] != 0) {
                 blstatus = json[@"data"][idg];  // เช็กสถานะ
            }
        }
    }];
}
- (IBAction)getImageBtnPressed:(UIImage *)sender
{
    UIImage *image =  sender; //[signatureView getSignatureImage];
    if (image != nil) {
        [popupView btnClosePop:popupView.wyPopoverController];
        [self btnReply:image];
    }
    else{
        [loadProgress.loadView showWithStatusSignature];
    }
}
- (void)btnReply:(UIImage *)singImage{
    if(singImage) {
        NSString *girlID  = self.member.girlID;//[NSString stringWithFormat:@"id_%@",dataMenuList[@"girl_id"]];
        NSDictionary *dataDrinkGirlList1 = drinkGirlList[[NSString stringWithFormat:@"id_%@",girlID]];
        NSDictionary *dataMenuList = (NSMutableDictionary *)[self getDetailDrink:girlID];//menuList[tmpIndexPath.row];
        
        NSMutableDictionary *_data = [NSMutableDictionary dictionary];
        [_data setObject:@""      forKey:@"Option"];
        [_data setObject:@""      forKey:@"Comment"];
        [_data setObject:@"1"     forKey:@"Count"];
        if ([kDrinkTwo  isEqual:self.typeDr])
            [_data setObject:@"0"   forKey:@"Price"];
        else
            [_data setObject:dataMenuList[@"price"] forKey:@"Price"];
        
        [_data setObject:self.typeDr     forKey:@"TypeDrink"];
        [_data setObject:singImage       forKey:@"StarSing"];
        [_data setObject:@""             forKey:@"StopSing"];
        [_data setObject:dataMenuList    forKey:@"data"];
        [_data setObject:dataDrinkGirlList1  forKey:@"drinkGirl"];
        [_data setObject:self.typeDrinkMenu  forKey:@"shot_type"];
        
        if ([self.globalBill.billType  isEqual: kSingleBill]) {
            TableSingleBillViewController *myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
            if (![myController isKindOfClass:[TableSingleBillViewController class]]) {
                myController = (TableSingleBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
            }
            [myController.tableDataBillView addOrderDrink:_data];
            [self.navigationController popToViewController:myController animated:NO];
        }
        else if ([self.globalBill.billType  isEqual: kSubBill]) {            
            TableSubBillViewController *myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:1];
            if (![myController isKindOfClass:[TableSubBillViewController class]]) {
                myController = (TableSubBillViewController *)[self.navigationController.viewControllers objectAtIndex:2];
            }
            [myController.tableDataBillView addOrderDrink:_data];
            [self.navigationController popToViewController:myController animated:NO];
        }
        else{
            TableBillMenuViewController *myController = (TableBillMenuViewController *)[self.navigationController.viewControllers objectAtIndex:1];
            [self.navigationController popToViewController:myController animated:NO];
        }
    }
}

-(NSDictionary *)getDetailDrink:(NSString *)key{
    NSString *value = @"girl_id = %@";
    NSDictionary *data = (NSDictionary *)[NSDictionary searchDictionary:key andvalue:value andData:menuList];
    if ([data count] != 0)
        return data;
    else
        return nil;
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].reportSreach
    // Pass the selected object to the new view controller.
     NSLog(@"id %@",[segue identifier]);
    if ([[segue identifier] isEqualToString:@"reportSreach"]){
        NSLog(@"reportSreach");
        DrinkGrirlsSrechReportViewController *drink = (DrinkGrirlsSrechReportViewController *)segue.destinationViewController;
        [drink setNameDrick:self.mNameDrink.text];
    }
}


- (IBAction)btnAddDrinkGrils:(id)sender {
    if (blstatus) {
        [self createPopover];
    }
    
}

#pragma mark - UIInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self initNavigation];
}
@end
