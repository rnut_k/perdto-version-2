//
//  TableSumViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"

#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"

@interface TableSumViewController : UIViewController

@property(nonatomic,strong) IBOutlet UIView *mTableSumCollectionView;

@property (weak, nonatomic) IBOutlet UITextField *mCountWomen;
@property (weak, nonatomic) IBOutlet UITextField *mCountMen;
@property (weak, nonatomic) IBOutlet UILabel *mTimeTable;

- (IBAction)mTagKeybroad:(id)sender;
- (IBAction)btnTableOpen:(id)sender;
- (IBAction)btnBack:(id)sender;

@end
