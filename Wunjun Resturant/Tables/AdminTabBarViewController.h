//
//  AdminTabBarViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 9/22/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdminPendingViewController.h"
#import "CashSalesReportViewController.h"
#import "CashTableStatus.h"
#import "CheckerInvoiceOrderController.h"
#import "Member.h"
#import "AppDelegate.h"
#import "LoadMenuAll.h"
#import "LocalizationSystem.h"

@interface AdminTabBarViewController : UIViewController

@property (nonatomic ,weak) IBOutlet UIView *mContainer;
@property (nonatomic )          BOOL isFirst;
@property (nonatomic ,assign)   NSInteger index;
@property (nonatomic ,strong)   NSDictionary *dataAllZone;

@end
