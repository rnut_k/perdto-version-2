//
//  CellCollectionTableView.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "KRLCollectionViewGridLayout.h"
#import "TableCollectionViewCell.h"
#import "CNPPopupController.h"
#import "HTPressableButton.h"
#import "UIColor+HTColor.h"

@interface CellCollectionTableView : UICollectionView<THSegmentedPageViewControllerDelegate,CNPPopupControllerDelegate>

@end
