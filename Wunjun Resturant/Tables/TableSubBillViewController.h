//
//  TableSubBillViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 2/2/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "TableDataBillViewController.h"
#import "UIStoryboard+Main.h"

@interface TableSubBillViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mContainerTableBill;
@property (weak, nonatomic) IBOutlet UIView *mContainerMenu;

@property (strong,nonatomic) NSMutableDictionary *sectionSubBill;
@property (nonatomic,strong) NSString *titleTable;
@property (nonatomic,strong) NSString *numSubBill;
@property (nonatomic,strong) NSDictionary   *detailTable;

@property (nonatomic) BOOL isCashierTake;
@property (nonatomic) BOOL isStaffTake;

@property (nonatomic,strong) TableDataBillViewController *tableDataBillView;

- (IBAction)btnOK:(id)sender ;
- (IBAction)btnBackHome:(id)sender;

+(void)loadDataOpenTableSubBill:(NSString *)idTable;
@end
