//
//  TableOpenViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableOpenViewController.h"
#import "Time.h"
#import "Member.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "TableSubBillViewController.h"
#import "TableSingleBillViewController.h"
#import "GlobalBill.h"
#import "LoadMenuAll.h"
#import "SelectBill.h"
#import "NSString+GetString.h"

@interface TableOpenViewController(){
    Member *_member;
    GlobalBill *_globalBill;
    LoadProcess *loadProgress;
}
@end

@implementation TableOpenViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    loadProgress = [LoadProcess sharedInstance];
    
    [self.mTimeTable setText:[Time getTime]];
    [self.mCountMen setDelegate:self];
    [self.mCountWomen setDelegate:self];

    _member = [Member  getInstance];
    
    self.otpTableTxt.placeholder = self.dataTable[@"id"];
    self.otpValTxt.placeholder = [self randomStringWithLength:4];
}
-(void)loadDataOpenTable{
    [loadProgress.loadView showWithStatus];
    NSArray *listarr = [NSArray arrayWithObject:self.dataTable[@"id"]];
    NSError *error = nil;
    NSString *cartID = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:listarr
                                                                                        options:NSJSONWritingPrettyPrinted
                                                                                          error:&error]
                                               encoding:NSUTF8StringEncoding];
    
    NSString *otpTable = ([self.otpTableTxt.text isEqualToString:@""])?self.otpTableTxt.placeholder:self.otpTableTxt.text;
    NSString *otpVal = ([self.otpValTxt.text isEqualToString:@""])?self.otpValTxt.placeholder:self.otpValTxt.text;
    NSString *otp = [NSString stringWithFormat:@"%@%@",otpTable, otpVal];
    otp = [NSString stringWithFormat:@"%@-%@",_member.nre,[otp uppercaseString]];
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"action":@"in",
                                 @"table_data":@{
                                         @"id":cartID,
                                         @"n_male":self.mCountMen.text,
                                         @"n_female":self.mCountWomen.text
                                         },
                                 @"otp":otp
                                 };
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,_member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        NSDictionary *_data = dictionary[@"data"];
        if ([dictionary[@"status"] boolValue]) {
            [SelectBill setCheckOpenTable:NO]; // no = เปิดโต็ะ
            if (self.cashTableStatus != nil){
                [SelectBill setIscashTableStatus:YES];
            }
            else{
                [SelectBill setIscashTableStatus:NO];
            }
            [SelectBill setIsCashier:NO];
            [SelectBill setIsTake:3];
            [SelectBill selectBill:self andData:_data andDataTable:self.dataTable fromRole:nil];
        }
        else
            [loadProgress.loadView showError];
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing  %@  ",textField.text);
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@" textFieldDidEndEditing %@  ",textField.text);

}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    if (self.mCountMen != textField) {
        self.mCountSum.text = [@([number integerValue]) stringValue];
    }
    else{
        self.mCountSum.text = [@([self.mCountWomen.text  integerValue] + [number integerValue]) stringValue];
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char,"\b");
    if (isBackSpace == -8) {
        // is backspace
        if (self.mCountWomen == textField){
            self.mCountWomen.text = [self.mCountWomen.text substringToIndex:[textField.text length]-1];
            self.mCountSum.text = self.mCountMen.text;
        }
        else{
            self.mCountMen.text = [self.mCountMen.text substringToIndex:[textField.text length] -1];
            self.mCountSum.text = self.mCountWomen.text;
        }
    }
    if (number)
        return YES;
    else
        return NO;
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (IBAction)mTagKeybroad:(id)sender {
    [self.mCountMen resignFirstResponder];
    [self.mCountWomen resignFirstResponder];
}

- (IBAction)btnOpenTable:(id)sender {
    
    
    [self loadDataOpenTable];
    //if (![self.mCountWomen.text isEqual:@""] && ![self.mCountMen.text isEqual:@""] && !([self.mCountWomen.text isEqual:@"0"] && [self.mCountMen.text isEqual:@"0"])) {
   
   // }
    //else{
    //      [loadProgress.loadView showWithStatusTitle];
   // }
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((u_int32_t)[letters length])]];
    }
    
    return randomString;
}
@end
