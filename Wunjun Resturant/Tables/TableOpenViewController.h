//
//  TableOpenViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 1/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadProcess.h"
#import "CashTableStatus.h"

@interface TableOpenViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mCountMen;
@property (weak, nonatomic) IBOutlet UITextField *mCountWomen;
@property (weak, nonatomic) IBOutlet UILabel *mCountSum;
@property (weak, nonatomic) IBOutlet UILabel *mTimeTable;
@property (weak, nonatomic) IBOutlet UITextField *otpTableTxt;
@property (weak, nonatomic) IBOutlet UITextField *otpValTxt;

@property (nonatomic ,strong) NSDictionary *dataTable;

@property (nonatomic,strong) CashTableStatus *cashTableStatus;

- (IBAction)btnBack:(id)sender;
- (IBAction)mTagKeybroad:(id)sender;
- (IBAction)btnOpenTable:(id)sender;

-(void)loadDataOpenTable;
@end
