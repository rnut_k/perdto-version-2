//
//  TablesViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/23/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TablesViewController.h"
#import "TableAllZoneCollectionView.h"
#import "StaffStatusMenuItemViewController.h"

#import "GlobalBill.h"
#import "MenuDB.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Check.h"
#import "SelectedIndex.h"
#import "LoadProcess.h"
#import "PopupView.h"
#import "NSDictionary+DictionaryWithString.h"

@interface TablesViewController (){
    GlobalBill *_globalBill;
    Member *_member;
    MenuDB *_menuDB;
    PopupView *popupView;
    
    NSMutableArray *pages;
    NSMutableArray *dataZone;
    LoadProcess *loadProgress;
}

@end
@implementation TablesViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization 
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     _member = [Member getInstance];
    [_member loadData];
    [self setTitle:_member.titleRes];
    
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app setMainViewController:self];
    
//    float left = 40.0;
//    float right = self.view.frame.size.width-172.0;
//    UIView *navTitle1 = [[UIView alloc] initWithFrame:CGRectMake(left,0,right,44)];
//    UILabel *topTitle =  [[UILabel alloc] initWithFrame: CGRectMake(0,0,navTitle1.bounds.size.width, 44)];
//    topTitle.text = self.title;
//    [topTitle setTextAlignment:NSTextAlignmentCenter];
//    topTitle.font = [UIFont systemFontOfSize:18];
//    [topTitle setTextColor:[UIColor whiteColor]];
//    [navTitle1 addSubview:topTitle];
//    self.navigationItem.titleView = navTitle1;
    
//    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
//                                                                               target:nil action:nil];
//    fixedItem.width = 20.0f;  // or whatever you want
//    
//    UIImage *image = [UIImage imageNamed:@"ic_c_home"];
//    CGRect frame = CGRectMake(0, 0, image.size.width+5, image.size.height+5);
//    UIButton* button = [[UIButton alloc] initWithFrame:frame];
//    [button setBackgroundImage:image forState:UIControlStateNormal];
//    [button setShowsTouchWhenHighlighted:YES];
//    [button addTarget:self action:@selector(barButtonItemAction:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem* barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//    
//    NSArray *buttons = @[fixedItem,barButtonItem,fixedItem];
//    self.navigationItem.rightBarButtonItems = [self.navigationItem.rightBarButtonItems arrayByAddingObjectsFromArray:buttons];
//    
    loadProgress = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateTableLoad)
                                                 name:@"NotiupdataStatusTable"
                                               object:nil];
//    [self doCalculationsAndUpdateUIs];
    
    [self loadUpdata];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
}

-(void)updateTableLoad{
    SelectedIndex *selected = [SelectedIndex getInstance];
    int inse = (int)[selected selectedIndex];
    NSString *zoneID = dataZone[inse];

    [LoadMenuAll retrieveData:zoneID success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            TableAllZoneCollectionView *tableAllZone = (TableAllZoneCollectionView *)pages[inse];
            [tableAllZone setDataListTables:dataTables];
            [tableAllZone.collectionView reloadData];
        });
    }];
}
- (void)doCalculationsAndUpdateUIs {
    [LoadMenuAll checkDataMenu:@"0"];
}
-(void)loadUpdata{
    
    NSDictionary *parameters = @{@"code":_member.code};
    NSString *url = [NSString stringWithFormat:MAPI_URL,_member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSMutableDictionary *_dataAllZone = json[@"resturant"];
        if([_dataAllZone count] != 0){
            _globalBill = [GlobalBill sharedInstance];
            _globalBill.billResturant = _dataAllZone;
        }
        [self loadDataGetZone];
    }];
}
-(void)loadDataGetZone{
    
    NSDictionary *parameters = @{@"code":_member.code};
    NSString *url = [NSString stringWithFormat:GET_ZONE,_member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {        
        NSMutableArray *_dataAllZone = json[@"data"];
        if([_dataAllZone count] != 0){
            _globalBill = [GlobalBill sharedInstance];
            bool blu = [_globalBill.billResturant[@"use_sub_bill"] boolValue];
            _globalBill.billType = blu ? kSubBill:kSingleBill;
            [self createViewTableZone:_dataAllZone];
        }
    }];
}

-(void) createViewTableZone:(NSMutableArray *)_dataAllZone {
    [self.viewContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
    THSegmentedPager *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    pages = [NSMutableArray new];
    dataZone = [NSMutableArray new];
    NSDictionary *tableIndexData = [[[NSDictionary alloc]init] mutableCopy];
    [LoadMenuAll retrieveData:@"0" success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            _globalBill = [GlobalBill sharedInstance];
            [_globalBill setBillListTable:(NSMutableArray *)dataTables];
            for(NSDictionary *data in dataTables){
                NSString *key = [NSString stringWithFormat:@"zone_id_%@", data[@"zone_id"]];
                NSMutableArray *tableListInZone = (tableIndexData[key] != nil) ? (NSMutableArray*)tableIndexData[key]:[[NSMutableArray alloc]init];
                [tableListInZone addObject:data];
                [tableIndexData setValue:tableListInZone forKey:key];
            }
            
            for (int i = 0; i < [_dataAllZone count]; i++) {
                NSDictionary *dic = _dataAllZone[i];
                NSString *key = [NSString stringWithFormat:@"zone_id_%@", dic[@"id"]];
                [dataZone addObject:dic[@"id"]];
                TableAllZoneCollectionView *_tableAllZoneCollectionView = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableAllZone"];
                [pages addObject:_tableAllZoneCollectionView];
                [_tableAllZoneCollectionView setViewTitle:[dic objectForKey:@"name"]];
                [_tableAllZoneCollectionView setDataListZone:dic];
                [_tableAllZoneCollectionView setDataListZoneAll:_dataAllZone];
                [_tableAllZoneCollectionView setNumTable:[Check checkDevice]?3:6];
                [_tableAllZoneCollectionView setDataListTables:tableIndexData[key]];
                
                [_tableAllZoneCollectionView.collectionView reloadData];
            }
            [pager setPages:pages];
            
            pager.view.frame = self.viewContainer.bounds;
            [self.viewContainer addSubview:pager.view];
            [self addChildViewController:pager];
            [pager didMoveToParentViewController:self];

        });
    }];
    
}
- (void) getKitchenList
{
    
    MenuDB *menuDB = [MenuDB getInstance];
    [menuDB loadData:@"menu_0"];
    NSDictionary *data = [NSDictionary dictionaryWithString:menuDB.valueMenu];
    
    NSArray *list = [[data objectForKey:@"kitchen"] allValues];
    self.dataListItemOrder = [NSMutableArray arrayWithArray:list];
    self.dataListItemOrderPop = [NSMutableArray arrayWithArray:@[AMLocalizedString(@"สั่งกลับบ้าน", nil),AMLocalizedString(@"ออเดอร์ทั้งหมด", nil)]];
    
    for (NSDictionary *data in self.dataListItemOrder) {
        NSString *str = data[@"name"];
        [self.dataListItemOrderPop addObject:[NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ออเดอร์", nil),str]];
    }
    [self.dataListItemOrder insertObject:@{@"name":AMLocalizedString(@"สั่งกลับบ้าน", nil)} atIndex:0];
    [self.dataListItemOrder insertObject:@{@"name":AMLocalizedString(@"ออเดอร์ทั้งหมด", nil)} atIndex:1];
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSLog(@"name %@",[segue identifier]);
    if ([[segue identifier] isEqualToString:@"logout"]) {              //    logout
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(IBAction)barButtonItemAction:(id)sender{
    [self getKitchenList];
    
    popupView = [[PopupView alloc] init];
    [popupView setTablesViewController:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}

#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
    
    NSInteger num = [self.dataListItemOrderPop indexOfObject:title];
    switch (num) {
        case 0:{
            NSLog(@"0");
//            
            UIViewController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"NVTakeaway"];
            [[self navigationController] pushViewController:viewController_ animated:NO];
        }
            break;
        case 1:{
            NSLog(@"1");
//            
            StaffStatusMenuItemViewController *menuItem = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"StaffStatusMenuItemView"];
            [menuItem loadDataMenu:@"0"];
            [menuItem setTitle:title];
            [[self navigationController] pushViewController:menuItem animated:NO];
        }
            break;
        default:{
            if (![Check checkNull:title]) {
                NSLog(@"2");
//                
                NSDictionary *data = self.dataListItemOrder[num];
                StaffStatusMenuItemViewController *menuItem = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"StaffStatusMenuItemView"];
                [menuItem setTitle:title];
                [menuItem loadDataMenu:data[@"id"]];
                [menuItem setDataKitchen:data];
                [[self navigationController] pushViewController:menuItem animated:NO];
            }
        }
            break;
    }
//    if ([title isEqual:@"ขอส่วนลดเพิ่ม"]) {
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        CustomNavigationController *_viewController = [storyboard instantiateViewControllerWithIdentifier:@"requestDiscounts"];
//        [_viewController setDataListSingBill:self.detailTable];
//        [_viewController setFoodAmount:[@(foodTotal) stringValue]];
//        [_viewController setDrickAmount:[@(drickTotal) stringValue]];
//        [_viewController setCountAmount:[NSString stringWithFormat:@"%@",self.dataListSingBill[@"total"][@"total"]]];
//        [self  presentViewController:_viewController animated:YES completion:nil];
//    }
//    else if ([title isEqualToString:@"พิมพ์บิลเก็บเงิน"]){
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        PrintReceiptViewController *print = [storyboard instantiateViewControllerWithIdentifier:@"PrintReceiptViewController1"];
//        [print setTitle:self.title];
//        [print setTableDataBillView:self.tableDataBillView];
//        [print setTableSingleBillView:self];
//        [print setIsPrintAllSubBill:YES];
//        [print setIsPrintSubBill:NO];
//        
//        [self printCheckBillView:YES];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            dispatch_sync(dispatch_get_main_queue(), ^{
//                [self  presentViewController:print animated:YES completion:nil];
//            });
//        });
//    }
//    else if ([title isEqualToString:@"พิมพ์บิลย่อยและชำระเงิน"]){
//        NSMutableArray *arrList = [NSMutableArray arrayWithArray:self.listTitleSubBill];
//        self.listTitleSubBill = [NSMutableArray array];
//        self.isPrintSubBill = YES;
//        for (NSDictionary *dic in arrList) {
//            if (dic[@"subbill"] != nil) {
//                [self.listTitleSubBill addObject:dic];
//            }
//        }
//        [self didTapTitleView:self];
//    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
@end
