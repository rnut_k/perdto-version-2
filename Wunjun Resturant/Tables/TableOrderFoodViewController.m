//
//  TableOrderFoodViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 1/28/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TableOrderFoodViewController.h"
#import "WYPopoverController.h"
#import "TableBillMenuViewController.h"
#import "GlobalBill.h"
#import "TableSingleBillViewController.h"
#import "FoodItemsOrderViewController.h"
#import "UIImageView+AFNetworking.h"
#import "NSDictionary+DictionaryWithString.h"
#import "NSString+GetString.h"
#import "Check.h"
#import "FoodOptionSum.h"

#define kCellComment @("CellComment")
#define kCellOption  @("CellOption")
#define kCellCount   @("CellCount")
#define kCellPriceOrder @("cellPriceOrder")
#define kNoItems (@"ไม่มีรายการ")


@interface TableOrderFoodViewController()<CNPPopupControllerDelegate,WYPopoverControllerDelegate>{
    LoadProcess *loadProgress;
    WYPopoverController *popoverController;
    UITableViewController *viewControllerListOption;
    UITableView *tableListOption;
    NSMutableDictionary *detailListFood;
    
    NSMutableArray *listDataSecletor;
    GlobalBill *globalBill;
    
    BOOL tmpPrice;
    NSMutableArray *listFoodOption;
    BOOL bgTable;
}
@property (nonatomic ,strong) Member *member;
@property (nonatomic, strong) UITextView *tempKeybroad;
@property (nonatomic, strong) CNPPopupController *popupController;

@end
@implementation TableOrderFoodViewController
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    loadProgress   = [LoadProcess sharedInstance];
    
    self.mTableOrgerFood.allowsMultipleSelectionDuringEditing = YES;
    listDataSecletor = [NSMutableArray array];
    tableListOption  = [[UITableView alloc] init];
    globalBill = [GlobalBill sharedInstance];
    
    NSDictionary *datas = @{@"name":AMLocalizedString(@"ไม่มีรายการ", nil),
                           @"price":@"0"};
    [listDataSecletor addObject:datas];
    
    NSData *data = [self.dataMenuList[@"food_option"] dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error;
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    if ([json count] != 0) {
        listFoodOption = [NSMutableArray arrayWithArray:json];
    }
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.mTitleListFood setText:self.strTitleFood];

    NSString *url = [NSString checkNull:self.dataMenuList[@"image_url"]];
    NSDictionary *picture = [NSDictionary dictionaryWithString:url];
    [self.mImageListFood setImageWithURL:[NSURL URLWithString:picture[@"th_url"]]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cleckDeleteTable:)
                                                 name:@"cleckDeleteTable"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self setHeightTable];
}
- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewWillDisappear:animated];
}
-(void)setHeightTable{
    
    NSString *platform = [Check platformRawString];
    CGSize heightmTableInvoice = self.mTableOrgerFood.contentSize;
    if ([platform isEqualToString:@"iPhone3,3"] || [platform isEqualToString:@"iPhone3,1"] || [platform isEqualToString:@"x86_64"]){
        heightmTableInvoice.height += 60;
    }
    self.mTableOrgerFood.contentSize = heightmTableInvoice;
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.bounds.size.width, tableView.bounds.size.height)];
    [headerView setBackgroundColor:[UIColor ht_carrotColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,4, tableView.bounds.size.width, 24)];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    [label setTextColor:[UIColor whiteColor]];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:14];
    [headerView addSubview:label];
    
    if (section == 1) {
        UIButton *btnOpiton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [btnOpiton setFrame:CGRectMake(headerView.frame.size.width - 40, 0,30, 30)];
        [btnOpiton setTag:section];
        [btnOpiton setTintColor:[UIColor whiteColor]];
        [btnOpiton addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:btnOpiton];
    }
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableListOption == tableView) {
        return  0;
    }
    return 30;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableListOption == tableView) {
        return  @"";
    }
    switch (section) {
        case 0: return [NSString  stringWithFormat:@"   ราคา"];
            break;
        case 1: return [NSString  stringWithFormat:@"   Option"];
            break;
        case 2: return [NSString  stringWithFormat:@"   Comment"];
            break;
        default: return [NSString  stringWithFormat:@"  จำนวน"];
            break;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableListOption == tableView) {
        return  1;
    }
    return 4;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableListOption == tableView)
        return  [listFoodOption count];
    
    if (section == 1)
        return [listDataSecletor count];
    
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableListOption == tableView)
        return  tableView.rowHeight;

    if (indexPath.section == 2) {
        CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:kCellComment];
        CGSize size = [cell.mTextView sizeThatFits:CGSizeMake(cell.mTextView.frame.size.width,FLT_MAX)];
        return size.height;
    }
    else if (indexPath.section == 3)
        return 70.0;
    else if (indexPath.section == 1)
        return 44.0;
    else if (indexPath.section == 0)
        return 49.0;
    return 44.0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier ;
     CellTableViewAll *_cell = (CellTableViewAll*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (_cell == nil) {
        _cell = (CellTableViewAll*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                          reuseIdentifier:cellIdentifier];
    }
    NSLog(@"index %ld",(long)indexPath.row);
    if (tableListOption == tableView) {     // popup
        _cell = [tableView dequeueReusableCellWithIdentifier:kCellOptionFood];
        NSDictionary *data = listFoodOption[indexPath.row];
        _cell.mLabel.text = [[FoodOptionSum class] typeDicTostring:data[@"name"]];
        _cell.mCount.text = data[@"price"];
        return _cell;
    }
    if (indexPath.section == 0){
         _cell = [tableView dequeueReusableCellWithIdentifier:kCellPriceOrder];
        NSString *price = self.dataMenuList[@"price"];
        [_cell.mCountEdit setText:price];
        if (![self.member.editOr boolValue]) {
            [_cell.mCountEdit setUserInteractionEnabled:NO];
        }
        return _cell;
    }
    if (indexPath.section == 1){
        _cell = [tableView dequeueReusableCellWithIdentifier:kCellOption];
        NSDictionary *data = listDataSecletor[indexPath.row];
        _cell.mLabel.text = [[FoodOptionSum class] typeDicTostring:data[@"name"]];
        _cell.mCount.text = data[@"price"];
        return _cell;
    }
    else if(indexPath.section == 2){
        _cell = [tableView dequeueReusableCellWithIdentifier:kCellComment];
        _cell.mTextView.text = @"ข้อความ";
        _cell.mTextView.textColor = [UIColor ht_silverColor];
    }
    else if(indexPath.section == 3){
        _cell = [tableView dequeueReusableCellWithIdentifier:kCellCount];
        _cell.mLabel.text = @"1";
    }
    return _cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@" index %d",(int)[indexPath  row]);
    if (tableListOption == tableView) {     // popup
        NSDictionary *data = listFoodOption[indexPath.row];
        
        if (![[listDataSecletor firstObject][@"name"] isKindOfClass:[NSDictionary class]]) {
             [listDataSecletor replaceObjectAtIndex:0 withObject:data];
        }
        else{
            [listDataSecletor addObject:data];
        }
        [self.mTableOrgerFood reloadData];
    }
    [self closePopOver:nil];
}

- (IBAction)cleckDeleteTable:(NSNotification *)note {
    NSDictionary *theData = [note userInfo];
    NSIndexPath *indexPath;
    
    NSLog(@"listDataSecletor %@",listDataSecletor);
    NSLog(@"listDataSecletor");
    if (![listDataSecletor[0][@"name"][@"df"] isEqualToString:AMLocalizedString(@"ไม่มีรายการ", nil)]) {
        [self.mTableOrgerFood beginUpdates];
        if (theData != nil) {
            indexPath = [theData objectForKey:@"index"];
        }
        
        [listDataSecletor removeObjectAtIndex:indexPath.row];
        if ([listDataSecletor count] == 0) {
            NSDictionary *datas = @{@"name":AMLocalizedString(@"ไม่มีรายการ", nil),
                                    @"price":@"0"};
            [listDataSecletor addObject:datas];
            [[self mTableOrgerFood] reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        else{
            [[self mTableOrgerFood] deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        [self.mTableOrgerFood endUpdates];
    }
}
- (IBAction)buttonTap:(id)sender
{
//    
    viewControllerListOption = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ListOptionFood"];
    viewControllerListOption.title = @"Option";
    tableListOption = viewControllerListOption.tableView;
    tableListOption.delegate = self;
    tableListOption.dataSource = self;

    viewControllerListOption.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    viewControllerListOption.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewControllerListOption];
    
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}


- (IBAction)btnReply:(id)sender {
    NSIndexPath *indexPath1 = [NSIndexPath  indexPathForRow:0 inSection:0];
    NSIndexPath *indexPath2 = [NSIndexPath  indexPathForRow:0 inSection:2];
    NSIndexPath *indexPath3 = [NSIndexPath  indexPathForRow:0 inSection:3];
    
    CellTableViewAll *_cell1 = (CellTableViewAll *)[self.mTableOrgerFood cellForRowAtIndexPath:indexPath1];
    CellTableViewAll *_cell2 = (CellTableViewAll *)[self.mTableOrgerFood cellForRowAtIndexPath:indexPath2];
    CellTableViewAll *_cell3 = (CellTableViewAll *)[self.mTableOrgerFood cellForRowAtIndexPath:indexPath3];
    
    if (![_cell3.mLabel.text isEqual:@"0"]) {        
        NSString *priceSp = _cell1.mCountEdit.text;
        id option = [listDataSecletor mutableCopy];
        NSString *name = [[FoodOptionSum class] typeDicTostring:listDataSecletor[0][@"name"]];
        if ([name isEqualToString:AMLocalizedString(@"ไม่มีรายการ", nil)]) {
            option = @"";
        }
        else{
            int priceINT  = [priceSp intValue];
            for (NSDictionary *dic in listDataSecletor) {
                priceINT += [dic[@"price"] intValue];
//                NSString *tem = [NSString stringWithFormat:@"%@%@บาท,",[[FoodOptionSum class] typeDicTostring:dic[@"name"]],dic[@"price"]];
//                option = [option stringByAppendingString:tem];
            }
//            option = [option substringToIndex:[option length]-1];
//            priceINT *= [_cell3.mLabel.text intValue];
            priceSp = [@(priceINT) stringValue];
        }
        
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:self.dataMenuList];
        if (![Check checkNull:priceSp]) {
            [dic setObject:priceSp forKey:@"price"];
        
        }
        
        NSString *count = _cell3.mLabel.text;
        NSString *comment = [AMLocalizedString(_cell2.mTextView.text, nil) isEqualToString:AMLocalizedString(@"ข้อความ", nil)]?@"":_cell2.mTextView.text;

        [self.FoodItemsOrderView addOrderAppvoceoption:dic option:option comment:comment count:[count intValue]];
        [self.navigationController popToViewController:self.FoodItemsOrderView animated:NO];
    }
    else{
        [loadProgress.loadView showWithStatusTitle];
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)aPopoverController
{
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    _tempKeybroad = textView;
    textView.text = @"";
    textView.textColor = [UIColor blackColor];

    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:1];
    [self.mTableOrgerFood scrollToRowAtIndexPath:indexPath
                                atScrollPosition:UITableViewScrollPositionBottom
                                        animated:NO];
}
- (IBAction)tagKeybroad:(id)sender {
    [_tempKeybroad resignFirstResponder];
    NSLog(@"resignFirstResponder");
}
- (void)closePopOver:(id)sender
{
     [popoverController dismissPopoverAnimated:YES];
}
- (void)keyboardWillShow:(NSNotification *)notification {
    NSDictionary *keyInfo = [notification userInfo];
    CGRect keyboardFrame = [[keyInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];

    keyboardFrame = [self.mTableOrgerFood convertRect:keyboardFrame fromView:nil];
    CGRect intersect = CGRectIntersection(keyboardFrame, self.mTableOrgerFood.bounds);
    if (!CGRectIsNull(intersect)) {
        NSTimeInterval duration = [[keyInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
        [UIView animateWithDuration:duration animations:^{
            self.mTableOrgerFood.contentInset = UIEdgeInsetsMake(0, 0, intersect.size.height, 0);
            self.mTableOrgerFood.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, intersect.size.height, 0);
        }];
    }
}

- (void) keyboardWillHide:  (NSNotification *) notification{
    NSDictionary *keyInfo = [notification userInfo];
    NSTimeInterval duration = [[keyInfo objectForKey:@"UIKeyboardAnimationDurationUserInfoKey"] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        self.mTableOrgerFood.contentInset = UIEdgeInsetsZero;
        self.mTableOrgerFood.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnBackHome:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
@end
