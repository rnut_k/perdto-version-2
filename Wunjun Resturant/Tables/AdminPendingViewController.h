//
//  AdminPendingViewController.h
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+HTColor.h"
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "TablePendingViewController.h"

@interface AdminPendingViewController : UIViewController

@property (nonatomic,assign) NSInteger indexPage;
@end
