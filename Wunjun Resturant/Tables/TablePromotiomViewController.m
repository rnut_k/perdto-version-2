//
//  TablePromotiomViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 2/1/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TablePromotiomViewController.h"
#import "ListItemPromotiomViewController.h"
#import "TableComboSetViewController.h"

#import "Member.h"
#import "LoadMenuAll.h"
#import "Member.h"
#import "Constants.h"
#import "SearchPromotion.h"
#import "NSDictionary+DictionaryWithString.h"
#import "GlobalBill.h"
#import "Check.h"
#import "CustomNavigationBar.h"

@interface TablePromotiomViewController(){
    NSMutableArray *listAddData;
    NSDictionary *promotionInfo;
    NSArray *promotionPool ;
    
    GlobalBill *globalBill;
    int tagImage;
}
@property (nonatomic ,strong) Member *member;
@end

@implementation TablePromotiomViewController
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initNavigation];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super viewWillDisappear:animated];
}
-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initNavigation{
    UIImage *image;
    CGSize size = self.navigationController.navigationBar.frame.size;
    image = [UIImage imageNamed:@"top_promotion"];
//    if ([Check checkDevice]) { // iphione
//        image = [UIImage imageNamed:@"top_promotion@2x"];
//        if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
//            image = [UIImage imageNamed:@"top_promotion@3x"];
//        }
//    }
//    else{ // ipan
//        image = [UIImage imageNamed:@"top_promotion@3x"];
//    }
    image = [self imageWithImage:image scaledToSize:CGSizeMake(size.width,size.height+22)];
    [self.navigationController.navigationBar setBackgroundImage:image
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
}
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
-(void)loadDataPromotionFloder:(NSDictionary *)pmtFloder{
    NSString *pmtFid = pmtFloder[@"id"];
    NSDictionary *promotion;
    globalBill = [GlobalBill sharedInstance];
    if ([globalBill.billType isEqual:kSubBill]) {
        promotion = self.tableSubBillViewController.tableDataBillView.dataPromotion;
    }
    else{
        promotion = self.tableSingleBillViewControll.tableDataBillView.dataPromotion;
    }
    
    if ([promotion[@"is_promotion"] boolValue]) {
        promotionInfo = promotion[@"promotion_info"];
        listAddData = [NSMutableArray array];
        for (NSDictionary *dic in [promotionInfo allValues]) {
            NSString *folder_id = dic[@"folder_id"];
            BOOL bl = [dic[@"is_enabled"] boolValue];
            if ([folder_id isEqual:pmtFid] && bl) {
                [listAddData addObject:dic];
            }
        }
    }
    else{
        listAddData = [NSMutableArray arrayWithObjects:AMLocalizedString(@"ไม่มีโปรโมชั่น", nil),nil];
    }
    [self.mTableProductPromotion reloadData];
}
-(void)loadDataPromotion{
    NSDictionary *promotion;
    globalBill = [GlobalBill sharedInstance];
    if ([globalBill.billType isEqual:kSubBill]) {
        promotion = self.tableSubBillViewController.tableDataBillView.dataPromotion;
        if (self.tableSubBillViewController == nil) {
            promotion = self.tableSingleBillViewControll.tableDataBillView.dataPromotion;
        }
    }
    else{
        promotion = self.tableSingleBillViewControll.tableDataBillView.dataPromotion;
        if (self.tableSingleBillViewControll == nil) {
            promotion = self.tableSubBillViewController.tableDataBillView.dataPromotion;
        }
    }
    
    if ([promotion[@"is_promotion"] boolValue]) {
        NSDictionary *promotionFloder = [NSDictionary dictionaryWithObject:promotion[@"promotion_folder"]];
        promotionInfo = promotion[@"promotion_info"];
        promotionPool = [NSArray arrayWithArray:[Check checkObjectType:promotion[@"promotion_pool"]]];
        NSMutableArray *lv2  = [NSMutableArray arrayWithArray:[SearchPromotion getPromotionPool:promotionPool
                                                                                            and:(NSMutableArray *)[promotionFloder allValues]
                                                                                            and:promotionInfo]];
        listAddData = [NSMutableArray array];
        if ([lv2 count] != 0) {
            for (NSDictionary *dic in lv2) {
                int bl;
                if ([dic isKindOfClass:[NSArray class]]) {
                    NSArray *arr = (NSArray *)dic;
                    NSDictionary *arr1 = [NSDictionary dictionaryWithObject:arr[0]];
                    bl = [arr1[@"is_enabled"] intValue];
                }
                else
                    bl = [dic[@"is_enabled"] intValue];
                
                if (bl == 1) {
                    [listAddData addObject:dic];
                }
            }
        }
    }
    else{
        listAddData = [NSMutableArray arrayWithObjects:AMLocalizedString(@"ไม่มีโปรโมชั่น", nil),nil];
    }
    [self.mTableProductPromotion reloadData];
}
-(void)loadDataPromotionRequirePromtion{
    NSDictionary *promotion;
    globalBill = [GlobalBill sharedInstance];
    promotion = self.tableSingleBillViewControll.tableDataBillView.dataPromotion;
    if ([promotion[@"is_promotion"] boolValue]) {
        NSDictionary *promotionFloder = [NSDictionary dictionaryWithObject:promotion[@"promotion_folder"]];
        NSArray *promotionFloder1  = [promotionFloder allValues];
        promotionInfo = promotion[@"promotion_info"];
        promotionPool  = [NSArray arrayWithArray:[Check checkObjectType:promotion[@"promotion_pool"]]];
        NSMutableArray *lv2  = [NSMutableArray arrayWithArray:[SearchPromotion getPromotionRequire:promotionPool
                                                                                            and:(NSMutableArray *)promotionFloder1
                                                                                            and:promotionInfo]];
        listAddData = [NSMutableArray array];
        if ([lv2 count] != 0) {
            for (NSDictionary *dic in lv2) {
                int bl;
                if ([dic isKindOfClass:[NSArray class]]) {
                    NSArray *arr = (NSArray *)dic;
                    NSDictionary *arr1 = [NSDictionary dictionaryWithObject:arr[0]];
                    bl = [arr1[@"is_enabled"] intValue];
                }
                else
                    bl = [dic[@"is_enabled"] intValue];
                
                if (bl == 1) {
                    [listAddData addObject:dic];
                }
            }
        }
    }
    else{
        listAddData = [NSMutableArray arrayWithObjects:AMLocalizedString(@"ไม่มีโปรโมชั่น", nil),nil];
    }
    [self.mTableProductPromotion reloadData];
}
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,50)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10,15,tableView.bounds.size.width-20,24)];
    label.text = [tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    [label setTextColor:[UIColor ht_leadColor]];
    label.font = [UIFont boldSystemFontOfSize:16];
    [headerView addSubview:label];
    
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 50;
    return 0;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString  stringWithString:AMLocalizedString(@"โปรโมชั่นสินค้า", nil)];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listAddData count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return tableView.rowHeight;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    @try {
        id data = listAddData[indexPath.row];
        if ([data isKindOfClass:[NSDictionary class]])
            data = (NSDictionary *)data;
        else if ([data isKindOfClass:[NSString class]]){
            cell.textLabel.text = data;
            return cell;
        }
        else
            data = (NSDictionary *)data[0];
        
       cell.textLabel.text = data[@"name"];
        
        cell.layer.cornerRadius = 10.0f;
        cell.layer.borderWidth = 1.0f;
        cell.layer.borderColor = [UIColor ht_silverColor].CGColor;
        cell.clipsToBounds = NO;
        cell.backgroundColor = [UIColor whiteColor];
        cell.layer.masksToBounds = YES;
        cell.textLabel.font = [UIFont systemFontOfSize:14];
        bool bl = [data[@"protype_lv1"] boolValue];
        int protype_lv2 = [data[@"protype_lv2"] intValue];
        NSString *gift_type = data[@"gift_type"];
        if ((!bl || [gift_type isEqual:@"3"]) && protype_lv2 != 3) {
            if (!bl) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                cell.tag =  33;
            }
            else
                cell.tag =  11;
        }
        else if ((!bl || [gift_type isEqual:@"9"]) && protype_lv2 != 9) {
            cell.tag =  44;
        }
        else{
            cell.tag = 22;
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception cellForRowAtIndexPath : %@ ",exception);
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    @try {
        id promotion = listAddData[indexPath.row];
        if ([promotion isKindOfClass:[NSDictionary class]])
            promotion = (NSMutableDictionary *)promotion;
        else
            promotion = (NSMutableDictionary *)promotion[0];
        
        UITableViewCell *_cell = [self.mTableProductPromotion cellForRowAtIndexPath:indexPath];
        if (_cell.tag == 11) {
            NSString *key =    promotion[@"id"];
            NSString *value =  @"promotion_id = %@";
            NSDictionary *dataPool = (NSDictionary *)[NSDictionary searchDictionary:key  andvalue:value andData:promotionPool];
            NSString *keyInfo = [NSString stringWithFormat:@"id_%@",promotion[@"id"]];
            
            ListItemPromotiomViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"ListItemPromotiomView"];
            [_viewController setTableSingleBillViewController:self.tableSingleBillViewControll];
            [_viewController setTableSubBillViewController:self.tableSubBillViewController];
            [_viewController setLocTitle:promotion[@"name"]];
            [_viewController setDataPromotion:promotion];
            [_viewController setDataPromotionPool:dataPool];
            [_viewController setDataPromotionInfo:promotionInfo[keyInfo]];
            
            [[self navigationController] pushViewController:_viewController animated:NO];
        }
        else if ( _cell.tag == 22){
            id promotion = listAddData[indexPath.row];
            if ([promotion isKindOfClass:[NSDictionary class]])
                promotion = (NSMutableDictionary *)promotion;
            else
                promotion = (NSMutableDictionary *)promotion[0];
            
//            NSLog(@"%@",self.title);
            NSString *needPictureS = promotion[@"need_picture"];
            BOOL needPicture = ([Check checkNull:needPictureS] || [needPictureS isEqualToString:@"0"])?NO:YES;
//            NSString *title1 = [NSString stringWithFormat:@"คุณต้องการส่งคำขอโปรโมชั่น \n %@",promotion[@"name"]];
//            UIAlertView *alertView;
            if (needPicture) {
                
//                alertView = [[UIAlertView alloc]initWithTitle:@"แจ้งเตือน !"
//                                                       message:title1
//                                                      delegate:self
//                                             cancelButtonTitle:@"ไม่ใช่"
//                                             otherButtonTitles:@"ถ่ายรูป",@"ใช่",nil];
//                [alertView setTag:indexPath.row];
//                [alertView show];
                 [self selectCameraReply];
            }
            else{
//                alertView = [[UIAlertView alloc]initWithTitle:@"แจ้งเตือน !"
//                                                   message:title1
//                                                  delegate:self
//                                         cancelButtonTitle:@"ไม่ใช่"
//                                         otherButtonTitles:@"ใช่",nil];
                [self sendPromotion:(int)indexPath.row andImage:@""];
            }
         
           
//            id data = listAddData[indexPath.row];
//            NSString *key = [NSString stringWithFormat:@"id_%@",promotion[@"id"]];
//            NSDictionary *dic  = [promotionInfo objectForKey:key];
//            NSString *title1 = [NSString stringWithFormat:@"คุณต้องการส่งคำขอโปรโมชั่น %@",data[@"name"]];
//            
//            if (self.tableSingleBillViewControll != nil) {
//                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"แจ้งเตือน !"
//                                                                   message:title1
//                                                                  delegate:self
//                                                         cancelButtonTitle:@"ไม่ใช่"
//                                                         otherButtonTitles:@"ถ่ายรูป",@"ใช่",
//                                          nil];
//                [alertView show];
////                [self selectCameraReply:nil];
////                NSArray *arrList = [NSArray arrayWithObjects:self.tableSingleBillViewControll.tableDataBillView.promotionPoolID,dic,nil];
////                [self.tableSingleBillViewControll.tableDataBillView addPromotion:arrList];
////                [self.navigationController popToViewController:self.tableSingleBillViewControll animated:NO];
//            }
//            else{
//                NSArray *arrList = [NSArray arrayWithObjects:self.tableSubBillViewController.tableDataBillView.promotionPoolID,dic,nil];
//                [self.tableSubBillViewController.tableDataBillView addPromotion:arrList];
//                [self.navigationController popToViewController:self.tableSubBillViewController animated:NO];
//            }
        }
        else if ( _cell.tag == 33){
            if (promotion != nil) {
//                
                TablePromotiomViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TablePromotiomView"];
                
                [_viewController setTableSingleBillViewControll:self.tableSingleBillViewControll];
                [_viewController setTableSubBillViewController:self.tableSubBillViewController];
                [_viewController loadDataPromotionFloder:promotion];
                [[self navigationController] pushViewController:_viewController animated:NO];
            }
        }
        else if ( _cell.tag == 44){  // promotion combo set
            if (promotion != nil) {
                TableComboSetViewController *_viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableComboSetView"];
                [_viewController setTableSingleBillViewController:self.tableSingleBillViewControll];
                [_viewController setTableSubBillViewController:self.tableSubBillViewController];
                [_viewController setDataPromotionInfo:promotion];
                [_viewController setTitle:promotion[@"name"]];
                [[self navigationController] pushViewController:_viewController animated:NO];
            }
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception cellForRowAtIndexPath : %@ ",exception);
    }
}
- (IBAction) selectCameraReply{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = NO;
        
        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage, nil];
        picker.mediaTypes = mediaTypes;
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"camera on this device!"
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil,
                                  nil];
        [alertView show];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    NSData *imageData = UIImageJPEGRepresentation([info valueForKey:UIImagePickerControllerOriginalImage], 0.5);
    [self uploadImageSign:imageData success:^(NSString *paht){
        [self sendPromotion:tagImage andImage:paht];
    }];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - uploadImageSign
-(void)uploadImageSign:(NSData *)imageData success:(void (^)(NSString * paht))completion{
   
    NSDictionary *parameters = @{@"code": self.member.code,
                                 @"files":@"file"};
    
    NSString *url = [NSString stringWithFormat:UPLOAD_IMAGE,self.member.nre];
    [LoadMenuAll loadDataImage:imageData andParameter:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSString *paht = json[@"data"][@"th_url"];
            completion(paht);
        }
    }];
}
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
////    NSLog(@"alertview %d",buttonIndex);
//    NSString *option = [alertView buttonTitleAtIndex:buttonIndex];
//    if ([option isEqualToString:@"ถ่ายรูป"]) { // ถ่ายรูป
//        tagImage = (int)alertView.tag;
//        [self selectCameraReply];
//    }
//    else  if ([option isEqualToString:@"ใช่"]) { // ใช่
//        [self sendPromotion:(int)alertView.tag andImage:@""];
//    }
//}
-(void)sendPromotion:(int)tag andImage:(NSString *)image{ // ใช่
    id promotion = listAddData[tag];
    if ([promotion isKindOfClass:[NSDictionary class]])
        promotion = (NSMutableDictionary *)promotion;
    else
        promotion = (NSMutableDictionary *)promotion[0];
    
    NSString *key = [NSString stringWithFormat:@"id_%@",promotion[@"id"]];
    NSDictionary *dic  = [promotionInfo objectForKey:key];
    
    if (self.tableSingleBillViewControll != nil) {
        NSArray *arrList = [NSArray arrayWithObjects:self.tableSingleBillViewControll.tableDataBillView.promotionPoolID,dic,image,nil];
        [self.tableSingleBillViewControll.tableDataBillView addPromotion:arrList andDataOrder:nil];
        
        [self.navigationController popToViewController:self.tableSingleBillViewControll animated:NO];
    }
    else{
        NSArray *arrList = [NSArray arrayWithObjects:self.tableSubBillViewController.tableDataBillView.promotionPoolID,dic,image,nil];
        [self.tableSubBillViewController.tableDataBillView addPromotion:arrList andDataOrder:nil];
        [self.navigationController popToViewController:self.tableSubBillViewController animated:NO];
    }
    
}
- (IBAction)btnBack:(id)sender {
//    [KVNProgress showWithStatus:@"Loading..."];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIInterfaceOrientation
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [self initNavigation];
}
@end
