//
//  ListDataPrinter.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListDataPrinter : NSObject

@property(nonatomic,strong) NSString *brandPrinter;
@property(nonatomic,strong) NSString *namePrinter;
@property(nonatomic,strong) NSString *IP;
@property(nonatomic,strong) NSString *paper;
@property(nonatomic,strong) NSString *port;
@property(nonatomic) BOOL isSeclect;
@property(nonatomic) BOOL isMenuSuggestion;

@end
