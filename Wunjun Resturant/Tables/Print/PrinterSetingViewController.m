//
//  PrinterSetingViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "PrinterSetingViewController.h"
#import "CellPrinterSeting.h"
#import "PrinterCreateController.h"
#import "PrintQuenceTableViewController.h"

#import "PopupView.h"
#import "NSString+GetString.h"
#import "NSDictionary+DictionaryWithString.h"
#import "Check.h"
#import "LocalizationSystem.h"
#import "FMDBDataAccess.h"

#import "ShowMsg.h"
#import "ePOS-Print.h"
#import "PrinterEPSON.h"

#define SEND_TIMEOUT    10 * 1000

@interface PrinterSetingViewController(){
    SystemSoundID soundID;
    UIAlertView *av;
    NSMutableArray *audioFileList;
    AVAudioPlayer *avSound;
    WYPopoverController *popoverControllerSetting;
    
    PopupView *popupView;
    
    NSIndexPath *indexPathPop;
    
    BOOL blISON;
    NSString *Lang2;
    
    BOOL blPastView;
}

@property (nonatomic,strong) PrinterEPSON *printerEPSON;
@property (nonatomic,strong) Member *member;
//@property (strong,nonatomic) PrinterDB *printerDB;
@property (strong,nonatomic) LoadProcess *loadProcess;;
@end

static PrinterSetingViewController *selfPrinter;
@implementation PrinterSetingViewController
void AudioServicesPlaySystemSound (
                                   SystemSoundID inSystemSoundID
                                   );
OSStatus AudioServicesCreateSystemSoundID (
                                           CFURLRef       inFileURL,
                                           SystemSoundID  *outSystemSoundID
                                           );
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
-(LoadProcess *)loadProcess{
    if (!_loadProcess) {
        _loadProcess = [LoadProcess sharedInstance];
    }
    return _loadProcess;
}

//-(PrinterDB *)printerDB{
//    if (!_printerDB) {
//        _printerDB = [[PrinterDB alloc] init];
//    }
//    [_printerDB loadData:kPrinterDB];
//    
//    return _printerDB;
//}
-(void)viewDidLoad{
    [super viewDidLoad];
    blPastView = false;
    [self.loadProcess.loadView showWithStatus];
     self.listPrinter = [NSMutableArray array];
    [self loadViewPrinter];
    [self setUpLabel];
}
-(void)setUpLabel{
    _labelLanguage.text = AMLocalizedString(@"language", @"");
    _labelMenuSuggestion.text = AMLocalizedString(@"menuSuggesting", @"");
    _labelSound.text = AMLocalizedString(@"sound", @"");
    _labelPrinter.text = AMLocalizedString(@"printer", @"");
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    selfPrinter = self;
    NSString *lag = [self.member.lg isEqualToString:@"en"]?@"EN":[self.member.lg isEqualToString:@"th"]?@"TH":@"ZH";
    [self.mLanguage setTitle:lag forState:UIControlStateNormal];
    
    [self.mSound setText:[self.member.ss lastPathComponent]];
    
    [self.mTableViewPrinter reloadData];
    [LoadProcess dismissLoad];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    if (blPastView) {
        [self initPrinterDB];
    }
}

-(void)initPrinterDB{

    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    PrinterDB *customers = [[PrinterDB alloc] init];
    
    NSString *status = (self.mSwitchPrinter.on)?@"1":@"0";
    NSString *statusSug = (self.mSwitchMenuSuggestion.on)?@"1":@"0";
    customers.isOpen = status;
    customers.isMenuSug = statusSug;
    
    if ([self.listPrinter count] == 0 && ![status isEqualToString:@"1"]){
        customers.listPrinter = @"data print";       
        [db insertCustomer:customers];
    }
    else{
        NSError *writeError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:[self dictionaryReflectFromAttributes]
                                                           options:NSJSONWritingPrettyPrinted error:&writeError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData
                                                     encoding:NSUTF8StringEncoding];
        
        
        customers.listPrinter = jsonString;
        if (![db isKeyDB:kPrinterDB]){            
            [db updateCustomer:customers];
        }
        else{
            [db insertCustomer:customers];
        }
    }
}
- (NSDictionary *)dictionaryReflectFromAttributes
{
    @autoreleasepool
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        unsigned int count = (int)[self.listPrinter count];
        for (int i = 0; i < count; i++)
        {
            ListDataPrinter *attributes = self.listPrinter[i];
            NSDictionary *data = [NSDictionary dictionaryWithObjectsAndKeys:attributes.namePrinter,@"namePrinter",
                                   attributes.IP,@"IP",
                                   attributes.paper,@"paper",
                                   attributes.port,@"port",
                                   (attributes.isSeclect ? @"YES" : @"NO"),@"isSeclect",
                                   attributes.brandPrinter,@"brandPrinter",nil];
            [dict setObject:data forKey:[@(i) stringValue]];
        }
        return dict;
    }
}
-(void)loadViewPrinter{
    @try {
        FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
        PrinterDB *printerDB = [db getCustomers];
        
        NSString *isOp = [NSString checkNull:printerDB.isOpen];
        BOOL bl = [isOp isEqualToString:@"0"] || [isOp isEqualToString:@""]?false:true;
        [self.mSwitchPrinter setOn:bl animated:YES];
        
        NSString *isMenu = [NSString checkNull:printerDB.isMenuSug];
        BOOL blM = [isMenu isEqualToString:@"0"] || [isMenu isEqualToString:@""]?false:true;
        [self.mSwitchMenuSuggestion setOn:blM animated:YES];
        
        if (printerDB.listPrinter) {
            NSError* error;
            NSDictionary *dictList = [NSJSONSerialization JSONObjectWithData:[printerDB.listPrinter dataUsingEncoding:NSUTF8StringEncoding]
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&error];
            
            int i = 0;
            for (i = 0 ; i < [dictList  count] ; ++i) {
                NSDictionary *dic = dictList[[@(i) stringValue]];
                BOOL seclect = (bl && [dic[@"isSeclect"] boolValue])?true:false;
                
                ListDataPrinter *listStruct = [[ListDataPrinter alloc] init];
                listStruct.namePrinter      = dic[@"namePrinter"];
                listStruct.IP               = dic[@"IP"];
                listStruct.paper            = dic[@"paper"];
                listStruct.port             = dic[@"port"];
                listStruct.isSeclect        = seclect;
                listStruct.brandPrinter     = dic[@"brandPrinter"];
                
                [self.listPrinter addObject:listStruct];
            }
        }

    }
    @catch (NSException *exception) {
        DDLogError(@"error exception loadViewPrinter : %@ ",exception);
    }
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.IsAudio)
        return [audioFileList count];
    else
        return [self.listPrinter count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.IsAudio) {
        return tableView.rowHeight;
    }
    return 70.0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.IsAudio) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        UIView *item =[[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width, tableView.frame.size.height)];
        item.tag = 1;
        
        NSString *str = [ NSString stringWithFormat:@"%@",[[audioFileList objectAtIndex:indexPath.row] copy]];
        
        CGSize size = cell.frame.size;
        if ([str isEqual:self.member.ss]) {
            UIImage *btnImage = [UIImage imageNamed:@"send"];
            UIButton *addImage = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            addImage.frame = CGRectMake(size.width - 40, 8.0f, 20.0f, 20.0f);
            [addImage setBackgroundImage:btnImage forState:UIControlStateNormal];
            [item addSubview:addImage];
        }
        else{
            UIButton *addButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            addButton.frame = CGRectMake(size.width - 80, 5.0f, 75.0f, 30.0f);
            [addButton setTitle:@"Add" forState:UIControlStateNormal];
            [addButton setTintColor:[UIColor ht_peterRiverColor]];
            [addButton setTag:indexPath.row];
            [item addSubview:addButton];
            [addButton addTarget:self
                          action:@selector(addSound:)
                forControlEvents:UIControlEventTouchUpInside];
        }
        
        UILabel *Lbl = [[UILabel alloc]initWithFrame:CGRectMake(10.0f,5.0f,210.0f, 30.0f)];
        [Lbl setText:[[audioFileList objectAtIndex:indexPath.row] lastPathComponent]];
        [item addSubview:Lbl];
        
        [[cell viewWithTag:1] removeFromSuperview];
        [cell addSubview:item];
        return cell;
    }
    else{
        static NSString *idCell = @"CellPrinterSeting";
        CellPrinterSeting *cell = [tableView dequeueReusableCellWithIdentifier:idCell];
        if (!cell) {
            cell = [[CellPrinterSeting alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idCell];
        }
        
        ListDataPrinter *listStruct = (ListDataPrinter *)self.listPrinter[indexPath.row];
        NSString *strName = [NSString stringWithFormat:@"Brand Printer:%@   Name:%@",listStruct.brandPrinter,listStruct.namePrinter];
        [cell.mNamePrinter setText:strName];
        [cell.mNamePrinter setFont:[UIFont systemFontOfSize:14.0]];
        NSString *strIP = [NSString stringWithFormat:@"IP:%@  Port:%@  PageSize:%@",listStruct.IP,listStruct.port,listStruct.paper];
        [cell.mIP setText:strIP];
        
        NSString *strMode = [NSString stringWithFormat:@"Mode:Graphic"];
        [cell.mMode setText:strMode];
        
        if (listStruct.isSeclect) {
            UIImage *img = [UIImage imageNamed:@"send"];
            [cell.mImageConnect setImage:img];
            [cell.mTypeConnect setContentMode:UIViewContentModeScaleAspectFit];
        }
        else{
            [cell.mImageConnect setImage:nil];
        }
       return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.loadProcess.loadView showWithStatus];
    if (self.IsAudio) {
        if (indexPath.row == 0) {
            avSound = [[AVAudioPlayer alloc]
                       initWithContentsOfURL:[NSURL fileURLWithPath:
                                              [[NSBundle mainBundle] pathForResource:@"sounds-chime" ofType:@"mp3"]]
                       error:nil];
            
            [avSound prepareToPlay];
            [avSound setDelegate: self];
            [avSound play];
        }
        else{
            NSURL *URL = [[audioFileList objectAtIndex:indexPath.row] copy];
            CFURLRef url = (CFURLRef) CFBridgingRetain(URL);
            OSStatus errorCode  = AudioServicesCreateSystemSoundID(url,&soundID);
            NSLog(@"ERROR %d: Failed to initialize UI audio file %@", (int)errorCode, url);
            AudioServicesPlaySystemSound(soundID);
            AudioServicesPlayAlertSound(soundID);
        }
    }
    else{
//        [self.loadProcess.loadView showWithStatusTitleSet:AMLocalizedString(@"ตรวจสอบการเชื่อมต่อ", nil)];
//          [self.loadProcess.loadView showWithStatus];
        
        ListDataPrinter *data = (ListDataPrinter *)self.listPrinter[indexPath.row];
        for (ListDataPrinter *listStruct in self.listPrinter) {
            listStruct.isSeclect = NO;
//            if ([data.brandPrinter isEqualToString:@"EPSON"]) {
//                self.printerEPSON = [PrinterEPSON sharedInstance];
//                [self.printerEPSON withAddress:listStruct.IP];
//                
//            }
            if ([listStruct isEqual:data]){
                listStruct.isSeclect = YES;
            }
        }
        
        if ([data.brandPrinter isEqualToString:@"EPSON"]) {
             [self openPrinter:data andTest:NO sus:^(BOOL completion) {
                 if (completion) {
                     NSLog(@"test EPSON completiont");
                 }
                 else{
                     DDLogError(@" error test EPSON completiont");
                     data.isSeclect = NO;
                 }
             }];
        }
        else{
            [self setPrintQuenceTable:data suss:^(BOOL completion) {
                if (completion) {
                    NSLog(@"test completiont");
                }
                else{
                    DDLogError(@" error test  completiont");
                    data.isSeclect = NO;
                }
            }];
        }
        [self.mTableViewPrinter reloadData];
    }
  
}
+(void)setIsseclect{
    for (ListDataPrinter *listStruct in selfPrinter.listPrinter) {
        listStruct.isSeclect = NO;
    }
    [selfPrinter.mTableViewPrinter reloadData];
}
- (IBAction)btnLanguage:(UIButton *)sender {    
    self.isLanguage = YES;
    popupView = [[PopupView alloc] init];
    [popupView setPrinterSetingViewController:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}

- (IBAction)btnSwichConnect:(id)sender {
    if([sender isOn]){
        NSLog(@"Switch is ON");
//        if ([self.listPrinter count] != 0) {
//            PrintQuenceTableViewController *printQuenceTable = [[PrintQuenceTableViewController alloc] init];
//            [printQuenceTable setTypePrint:@"4"];
//            [printQuenceTable initNetworkCommunication:^(bool completion) {
//                if (completion) {
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//                        [printQuenceTable createViewPrint];
//                    });
//                }
//                else{
//                    [self dismissViewControllerAnimated:YES completion:nil];
//                }
//            }];
//        }
    }
    else{
        NSLog(@"Switch is OFF");
        for (ListDataPrinter *listStruct in self.listPrinter) {
            
            if ([listStruct.brandPrinter isEqualToString:@"EPSON"] && listStruct.isSeclect) {
                self.printerEPSON = [PrinterEPSON sharedInstance];
                if (self.printerEPSON != nil) {
                    [PrinterEPSON closePrinterEPSON];
                }
            }
            if (listStruct.isSeclect) {
                listStruct.isSeclect = NO;
            }
        }
        [self.mTableViewPrinter reloadData];
    }
}
- (IBAction)btnMenuSuggestion:(id)sender {
    [self initPrinterDB];
    if([sender isOn]){
        NSLog(@"btnMenuSuggestion is ON");
    }
    else{
        NSLog(@"btnMenuSuggestion is OFF");
    }
}
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender {
    @try {
        if (sender.state == UIGestureRecognizerStateBegan){
              self.isLanguage = NO;
            CGPoint p = [sender locationInView:self.mTableViewPrinter];
            indexPathPop = [self.mTableViewPrinter indexPathForRowAtPoint:p];
            
            popupView = [[PopupView alloc] init];
            [popupView setPrinterSetingViewController:self];
            [popupView showPopupWithStyle:CNPPopupStyleCentered];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@" error btnDetailMenuPop : %@",exception);
    }
}

- (IBAction)cilckSettingSound:(UITapGestureRecognizer *)sender {
    audioFileList = [[NSMutableArray alloc] init];
    self.IsAudio = YES;
    [self loadAudioFileList];
    
    UITableViewController* contentViewController = [[UITableViewController alloc] init];
    contentViewController.title = @"รายการเสียง";
    contentViewController.view.layer.masksToBounds = YES;
    contentViewController.preferredContentSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height);
    contentViewController.modalInPopover = NO;
    
    self.mTableViewPrinter = contentViewController.tableView;
    self.mTableViewPrinter.delegate = self;
    self.mTableViewPrinter.dataSource = self;
    
    UINavigationController* contentView = [[UINavigationController alloc] initWithRootViewController:contentViewController];
    UIImage *image = [[UIImage imageNamed:@"cic_close"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 40, 40)];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(dismissPopoverAnimated:)];
    contentViewController.navigationItem.rightBarButtonItem = rightBarButtonItem;

    popoverControllerSetting = [[WYPopoverController alloc] initWithContentViewController:contentView];
    popoverControllerSetting.delegate = self;

    [popoverControllerSetting presentPopoverFromRect:CGRectZero
                                              inView:self.view
                            permittedArrowDirections:WYPopoverArrowDirectionNone
                                            animated:NO];
}
- (void)dismissPopoverAnimated:(BOOL)animated{
     self.IsAudio = NO;
     [self.mSound setText:[self.member.ss lastPathComponent]];
    [popoverControllerSetting dismissPopoverAnimated:YES];
}
-(void)loadAudioFileList{
    [audioFileList addObject:@"sounds-chime.mp3"];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSURL *directoryURL = [NSURL URLWithString:@"/System/Library/Audio/UISounds"];
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             return YES;
                                         }];
    for (NSURL *url in enumerator) {
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [audioFileList addObject:url];
        }
    }
    
    av = [[UIAlertView alloc] initWithTitle:@"Ringtones"
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:@"close"
                          otherButtonTitles:nil];
}
- (IBAction)addSound:(UIButton *)sender
{
    NSString *sound = [[audioFileList objectAtIndex:sender.tag] copy];
    NSLog(@"Add friend. %@",sound);
    [self.member setSs:sound];
    [self.member updateDb:@{@"ss":sound} andId:self.member.id];
    [self.member loadData];
    [self.mTableViewPrinter reloadData];
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    if ([Check checkNull:title]) {
        title = @"";
    }
    else if ([title isEqualToString:@"Test Printer"]){
        ListDataPrinter *data = (ListDataPrinter *)self.listPrinter[indexPathPop.row];
        [self btnTestPrint:data];
    }
    else if ([title isEqualToString:@"Delete Printer"]){
        [self.listPrinter removeObjectAtIndex:indexPathPop.row];
        [self.mTableViewPrinter reloadData];
    }
    else if (![title isEqualToString:@""]){
        NSString *lang = [self selectLang:title];
        LocalizationSetIs(YES);
        LocalizationSetLanguage(lang);
        [self.mLanguage setTitle:title forState:UIControlStateNormal];
    }
}

-(NSString *)selectLang:(NSString *)lang{
    NSArray *items = @[@"TH", @"EN", @"ZH"];
    int item = (int)[items indexOfObject:lang];
    switch (item) {
        case 0:
             return @"th";
            break;
        case 1:
             return @"en";
            break;
        case 2:
             return @"zh";
            break;
        default:
            return [LocalizationSystem getLanguage];
            break;
    }
}
- (void)btnTestPrint:(ListDataPrinter *)dataPrinter{
    [self.loadProcess.loadView showWithStatus];
    
    NSLog(@"test btnTestPrint");
    [self setPrintQuenceTable:dataPrinter suss:^(BOOL completion) {
        if (completion) {
            NSLog(@"completion");
        }
        else{
            NSLog(@"no completion");
        }
    }];
}
-(void) setPrintQuenceTable:(ListDataPrinter *)dataPrinter suss:(void (^)(BOOL completion)) block{
    
    BOOL isPrintEPSON = ([dataPrinter.brandPrinter isEqualToString:@"EPSON"])?YES:NO;
    
    PrintQuenceTableViewController *printQuenceTable = [[PrintQuenceTableViewController alloc] init];    
    [printQuenceTable setTypePrint:@"4"];
    printQuenceTable.namePrint    = dataPrinter.namePrinter;
    printQuenceTable.IP           = dataPrinter.IP;
    printQuenceTable.Port         = dataPrinter.port;
    printQuenceTable.isPrintEPSON = isPrintEPSON;
    printQuenceTable.brandPrinter = dataPrinter.brandPrinter;
    [printQuenceTable setListDataPrinter:dataPrinter];
    if (isPrintEPSON) {
        BOOL bl =  [printQuenceTable createViewPrint];
    }
    else{
        [printQuenceTable initPrinterConnect:^(bool completion) {
            if (completion) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    BOOL bl =  [printQuenceTable createViewPrint];
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        block(bl);
                    });
                });
            }
            else{
                block(NO);
            }
        }];
    }
}
- (void)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL) blTest sus:(void (^)(BOOL completion)) block{
    BOOL bl = true;
    @try {
        NSLog(@"1 Setting open");
        //get open parameter
        if(textIp.IP.length == 0){
            [ShowMsg showError:@"errmsg_noaddress"];
            [LoadProcess dismissLoad];
            return ;
        }
        
        //open
        EposPrint *printer = [[EposPrint alloc] init];
        if(printer == nil){
            bl = false;
            return ;
        }
        [delegate_ setEventCallback:printer];
        
         int connectionType = EPOS_OC_DEVTYPE_TCP;
        // ต่อ printer
        int result = [printer openPrinter:connectionType DeviceName:textIp.IP Enabled:EPOS_OC_FALSE Interval:1000];
        if(result != EPOS_OC_SUCCESS){
            DDLogError(@"error open");
        }
        NSLog(@"2 connect %d",result);
        
        NSLog(@"3 add epos");
        int language = 0; //EPOS_OC_MODEL_THAI;;
        EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
        if(builder == nil){
            [LoadProcess dismissLoad];
            bl = false;
            return ;
        }
        
        result = [builder addText:[self getBuilderText]];
        if(result != EPOS_OC_SUCCESS){
            [LoadProcess dismissLoad];
            bl = false;
            return ;
        }
        
        //add command
        result = [builder addCut:EPOS_OC_CUT_FEED];
        if(result != EPOS_OC_SUCCESS){
            [LoadProcess dismissLoad];
            bl = false;
            return ;
        }
        
        //send builder data
        unsigned long status = 0;
        unsigned long battery = 0;
        result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
        
        //remove builder
        [builder clearCommandBuffer];        
        
        NSLog(@"4 close epos ");
        if(printer != nil){
            int result = [printer closePrinter];
            if(result != EPOS_OC_SUCCESS){
                DDLogError(@"closePrinter error");
                bl = false;
            }
            printer = nil;
        }
        NSLog(@"5 close epos");
        [LoadProcess dismissLoad];
    }
    @catch (NSException *exception) {
        DDLogError(@" error Setting openPrinter : %@",exception);
    }
    @finally {
        NSLog(@"6 block");
        block(bl);
    }
}
//- (void)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL) blTest sus:(void (^)(BOOL completion)) block{
//    BOOL bl = false;
//    @try {
//        NSLog(@"1 Setting open");
//        //get open parameter
//        if(textIp.IP.length == 0){
//            [ShowMsg showError:@"errmsg_noaddress"];
//            [LoadProcess dismissLoad];
//            return ;
//        }
//        
//        int result;
//        self.printerEPSON = [PrinterEPSON sharedInstance];
//        [PrinterEPSON withAddress:textIp.IP];
//        
//        if (!self.printerEPSON) {
//            [LoadProcess dismissLoad];
//            return;
//        }
//        result = self.printerEPSON.result;
//        NSLog(@"2 connect %d",result);
//        
//        if (self.printerEPSON != nil){
//            NSLog(@"3 add epos");
//            int language = 0; //EPOS_OC_MODEL_THAI;;
//            EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
//            if(builder == nil){
//                [LoadProcess dismissLoad];
//                return ;
//            }
//            
//            result = [builder addText:[self getBuilderText]];
//            if(result != EPOS_OC_SUCCESS){
//                [ShowMsg showExceptionEpos:result method:@"addText"];
//                [LoadProcess dismissLoad];
//                return ;
//            }
//            
//            //add command
//            result = [builder addCut:EPOS_OC_CUT_FEED];
//            if(result != EPOS_OC_SUCCESS){
//                [ShowMsg showExceptionEpos:result method:@"addCut"];
//                [LoadProcess dismissLoad];
//                return ;
//            }
//            
//            //send builder data
//            unsigned long status = 0;
//            unsigned long battery = 0;
//            result = [self.printerEPSON.printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
//            
//            //remove builder
//            [builder clearCommandBuffer];
//            NSLog(@"4 close epos ");
//            bl =  [PrinterEPSON closePrinterEPSON];
//            NSLog(@"5 close epos");
//        }
//        else{
//            [ShowMsg showError:@"errmsg_noaddress"];
//        }
//        [LoadProcess dismissLoad];
//    }
//    @catch (NSException *exception) {
//        DDLogError(@" error Setting openPrinter : %@",exception);
//    }
//    @finally {
//        NSLog(@"6 block");
//        block(bl);
//    }
//}

- (int)getStatusMonitorEnabled
{
    return EPOS_OC_FALSE;
}
- (NSString *)getBuilderText
{
    return NSLocalizedString(@"text_edit_text", @"");
}
- (void)onStatusChange:(NSString *)deviceName Status:(NSNumber *)status
{
    [ShowMsg showStatusChangeEvent:deviceName Status:[status unsignedLongValue]];
}

- (void)onBatteryStatusChange:(NSString *)deviceName Battery:(NSNumber *)battery
{
    [ShowMsg showBatteryStatusChangeEvent:deviceName Battery:[battery unsignedLongValue]];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
   
    if (![[segue identifier] isEqualToString:@"logout"]) {
        PrinterCreateController *obj = (PrinterCreateController *)[segue destinationViewController];
        [obj setPrinterSetingView:self];
    }
    else if ([[segue identifier] isEqualToString:@"logout"]) {
        blPastView = YES;
    }
}

@end
