//
//  CellPrinterSeting.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellPrinterSeting : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mImageConnect;
@property (weak, nonatomic) IBOutlet UILabel *mNamePrinter;
@property (weak, nonatomic) IBOutlet UILabel *mIP;
@property (weak, nonatomic) IBOutlet UILabel *mMode;
@property (weak, nonatomic) IBOutlet UILabel *mTypeConnect;

@end
