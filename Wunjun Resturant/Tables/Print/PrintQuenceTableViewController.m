//
//  PrintQuenceTableViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 6/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#define kCellNameTable  (@"cellNameTable")
#define kCellQuenceOrer (@"cellQuenceOrder")
#define kCellFood       (@"cellFood")

#import "PrintQuenceTableViewController.h"
#import "ConfigPrinter.h"
#import "PrinterSetingViewController.h"

#import "Reachability.h"
#import "ImageHelper.h"
#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "CellTableViewAll.h"
#import "GlobalBill.h"
#import "Member.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "Check.h"
#import "UIColor+HTColor.h"
#import "NSString+GetString.h"
#import "InvoiceDB.h"
#import "LoadProcess.h"
#import "KVNViewController.h"
#import "DeviceData.h"
#import "PrinterDB.h"
#import "AppDelegate.h"
#import "FoodOptionSum.h"
#import "GlobalBill.h"
#import "ShowMsg.h"
#import "ePOS-Print.h"
#import "PrinterEPSON.h"
#import "FMDBDataAccess.h"

static float fontKC = 50.0;
static float fontKT = 40.0;
static float fontK  = 30.0;

#define SEND_TIMEOUT    10 * 10000
#define IMAGE_WIDTH_MAX  512

@interface NSString (NSStringHexToBytes)
-(NSString*) NSDataToHex:(NSData*)data;
-(NSData*) hexToBytes;
@end

// Implement it here:
@implementation NSString(NSStringHexToBytes)

-(NSString*) NSDataToHex:(NSData*)data
{
    const unsigned char *dbytes = [data bytes];
    NSMutableString *hexStr =
    [NSMutableString stringWithCapacity:[data length]*2];
    int i;
    for (i = 0; i < [data length]; i++) {
        [hexStr appendFormat:@"%02x ", dbytes[i]];
    }
    return [NSString stringWithString: hexStr];
}
-(NSData*) hexToBytes {
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= self.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [self substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }
    return data;
}
@end

@interface PrintQuenceTableViewController ()<UIImagePickerControllerDelegate,UIAlertViewDelegate>{
    NSMutableData *dataResult;
    UIImage* imagePrint;
    NSArray *_binaryArrayClass;
    NSArray *_hexStrClass ;
   
    NSArray *arrList;
    
    GlobalBill *globalBill;
    DeviceData *deviceData;
    __block BOOL errorPrint;
    
    LoadProcess *loadProgress;
    
    
    BOOL fistTotal;
}
@property (nonatomic,strong) PrinterEPSON *printerEPSON;
//@property(strong,nonatomic) PrinterDB *printerDB;
@end
@implementation PrintQuenceTableViewController

@synthesize dataImagePrint;
@synthesize outputStream;

+ (NSArray *)binaryArray
{
    static NSArray *binaryArray;
    @synchronized(self) {
        binaryArray = @[@"0000",@"0001",@"0010",@"0011",
                        @"0100",@"0101",@"0110",@"0111",
                        @"1000",@"1001",@"1010",@"1011",
                        @"1100",@"1101",@"1110",@"1111"];
    }
    return binaryArray;
}
+ (NSArray *)hexStr
{
    static NSArray *hexStr ;
    @synchronized(self) {
        hexStr = @[@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",
                   @"A",@"B",@"C",@"D",@"E",@"F"];
    }
    return hexStr;
}
- (instancetype)init {
    self = [[super init] autorelease];
    if (self) {
        globalBill              = [GlobalBill sharedInstance];
        deviceData              = [DeviceData getInstance];
        _binaryArrayClass       = [[self class] binaryArray];
        _hexStrClass            = [[self class] hexStr];
        self.dataImagePrint     = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNetworkCommunication:^(bool completion) {
        NSLog(@"initNetworkCommunication");
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    loadProgress  = [LoadProcess sharedInstance];
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:YES];
    [LoadProcess dismissLoad];
}

- (void) initNetworkCommunication:(void (^)(bool completion)) block{
    [self getDataDB];
    if (self.isPrintEPSON) {
        block(true);
    }
    else{
        [self initPrinterConnect:^(bool completion) {
            //NSLog(@"comp");
            if (!completion) {
                if (self.isSalesPrint)
                    [self dismissViewControllerAnimated:YES completion:nil];
                else
                    [self.navigationController popViewControllerAnimated:YES];
            }
            block(completion);
        }];
    }
}
-(void)initPrinterConnect:(void (^)(bool completion)) block{
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    PrinterDB *printerDB = [db getCustomers];
    
    if (![printerDB.isOpen boolValue]) {
        block(YES);
    }
    else if ([Check checkNull:self.IP] || [Check checkNull:self.Port] || [self.IP isEqualToString:@""]) {
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                           message:AMLocalizedString(@"กรุณาตรวจสอบเครื่องปริ้นเตอร์", nil)
                                                          delegate:self
                                                 cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                                 otherButtonTitles:nil];
        [alerView show];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_sync(dispatch_get_main_queue(), ^{
                block(NO);
            });
        });
    }
    else{
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            CFReadStreamRef readStream;
            CFWriteStreamRef writeStream;
            CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)self.IP,[self.Port intValue], &readStream, &writeStream);
            
            outputStream = (__bridge NSOutputStream *)writeStream;
            [outputStream setDelegate:self];
            [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [outputStream open];
            dispatch_sync(dispatch_get_main_queue(), ^{
                block(YES);
            });
        });
    }
}

-(void) getDataDB{
    FMDBDataAccess *db = [[FMDBDataAccess alloc] init];
    PrinterDB *printerDB = [db getCustomers];
    
    self.listDataPrinter = [[ListDataPrinter alloc] init];
    
    if (printerDB.listPrinter) {
        NSDictionary *dictList = [NSJSONSerialization JSONObjectWithData:[printerDB.listPrinter dataUsingEncoding:NSUTF8StringEncoding]
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:nil];
        
        int i = 0;
        for (i = 0 ; i < [dictList  count] ; ++i) {
            NSDictionary *dic = dictList[[@(i) stringValue]];
            if ([dic[@"isSeclect"] boolValue]) {
                self.listDataPrinter.namePrinter      = dic[@"namePrinter"];
                self.listDataPrinter.IP               = dic[@"IP"];
                self.listDataPrinter.paper            = dic[@"paper"];
                self.listDataPrinter.port             = dic[@"port"];
                self.listDataPrinter.isSeclect        = [dic[@"isSeclect"] boolValue];
                self.listDataPrinter.brandPrinter     = dic[@"brandPrinter"];
                
                self.namePrint    = self.listDataPrinter.namePrinter;
                self.IP           = self.listDataPrinter.IP;
                self.Port         = self.listDataPrinter.port;
                self.isPrintEPSON = ([self.listDataPrinter.brandPrinter isEqualToString:@"EPSON"])?YES:NO;
                self.brandPrinter = self.listDataPrinter.brandPrinter;
                break;
            }
        }
    }
}

-(BOOL )createViewPrint{
    NSMutableArray *dataQ = [NSMutableArray array];
    BOOL blCut = ([self.typePrintCut isEqualToString:@"1"])?YES:NO;         // 2 == test all print
    BOOL blPrintTotal = ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คโต๊ะซ้ำ", nil)] && !blCut)?YES:NO;

    if ([self.typePrint isEqualToString:@"1"]) {  // กดปริ้น ที่ละใบ
        if (![Check checkNull:self.sortRepeat] && ![self.sortRepeat isEqualToString:@""] && ![self.sortRepeat isEqualToString:AMLocalizedString(@"ไม่เช็ค", nil)] && self.onePrint) {
            NSMutableArray *dataOrder = [[self sortDataOrder] mutableCopy];
            BOOL blTitle = YES;
            float total = 0.0;
            for (NSDictionary *data in dataOrder) {
                
                if (([dataQ count] == [dataOrder count]-1) && !blPrintTotal ) {
                    blCut = YES;
                }
                
                if ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คโต๊ะซ้ำ", nil)] && !blCut) {
                    if ([dataQ count] != 0) {
                          blTitle = NO;
                    }
                }
                
                NSMutableArray *dataSub = [self sortDataKitchenPrint:blTitle andCut:blCut andData:data];
                [dataQ addObject:dataSub];
                
                total += [NSString checkNullConvertFloat:data[@"n"]] * [NSString checkNullConvertFloat:data[@"price"]];
            }
            if (blPrintTotal) {
                NSString *line        = @"- - - - - - - - - - - - - - - - - - - - - - - -";
                NSString *totalF      = [NSString stringWithFormat:@"%@   %.2f",AMLocalizedString(@"ราคารวม", nil),total];
                NSMutableArray *arrL  = [NSMutableArray array];
                [arrL addObject:@"total"];
                [arrL addObject:totalF];
                [arrL addObject:line];
                [arrL addObject:@"cut"];
                [dataQ addObject:arrL];
            }
        }
        else{
           NSMutableArray *dataSub = [self sortDataKitchenPrint:YES andCut:YES andData:self.dataPrint];
           [dataQ addObject:dataSub];
        }
    }
    else if ([self.typePrint isEqualToString:@"2"]) {  // กดปริ้น ทั้งหมด printAll
        
        NSMutableArray *listMenu = [[self sortCategoryMenuTable:self.rowData] mutableCopy];
        if ([listMenu count] != 0) {
            
            NSString *tempTableID = [NSString checkNull:listMenu[0][@"table_id"]];
            NSString *tempTableID1 = @"";
            BOOL blTitle = YES;
          
            for (NSDictionary *data in listMenu) {
                int i = (int)[dataQ count];
                 tempTableID1 = [NSString checkNull:data[@"table_id"]];
                if (![tempTableID isEqualToString:tempTableID1] && !blCut) {
                    [dataQ[i-1] addObject:@"cut"];
                    tempTableID = tempTableID1;
                   
                    blTitle = YES;
                }
                else if ([dataQ count] != 0 && !blCut) {
                    blTitle = NO;
                }
                
                if ([dataQ count] == [listMenu count]-1) {
                    blCut = YES;
                }
                
                NSMutableArray *dataSub = [self sortDataKitchenPrint:blTitle andCut:blCut andData:data];
                [dataQ addObject:dataSub];
            }
        }
        else{
            return false;
        }
    }
    else if ([self.typePrint isEqualToString:@"3"]) {       // กดปริ้น ที่ละตามจำนวน  printCount
        int count = (int)[NSString checkNullConvertFloat:self.nPrint];
        if (count == 0) {
            UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                               message:AMLocalizedString(@"กรุณาป้อนจำนวนที่ต้องการปริ้น", nil)
                                                              delegate:self
                                                     cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                                     otherButtonTitles:nil];
            [alerView show];
            return false;
        }
        else{
            if (count > [self.rowData count])
                count = (int)[self.rowData count];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (NSInteger i = 0; i < count; ++i)
                [array addObject:self.rowData[i]];
            
            NSMutableArray *listMenu = [[self sortCategoryMenuTable:array] mutableCopy];
            if ([listMenu count] != 0) {
                BOOL blTitle = YES;
                NSString *tempTableID = [NSString checkNull:listMenu[0][@"table_id"]];
                NSString *tempTableID1 = @"";
                for (int i = 0 ; i < count; ++i) {
                    NSDictionary *data = [NSDictionary dictionaryWithObject:listMenu[i]];
                    
                     tempTableID1 = [NSString checkNull:data[@"table_id"]];
                    if (![tempTableID isEqualToString:tempTableID1] && !blCut) {
                        [dataQ[i-1] addObject:@"cut"];
                        
                        tempTableID = tempTableID1;
                        blTitle = YES;
                    }
                    else if ([dataQ count] != 0  && !blCut) {
                        blTitle = NO;
                    }
                    
                    if ([dataQ count] == [listMenu count]-1) {
                        blCut = YES;
                    }
                
                    NSMutableArray *dataSub = [self sortDataKitchenPrint:blTitle andCut:blCut andData:data];
                    [dataQ addObject:dataSub];
                }
            }
        }
    }
    else if ([self.typePrint isEqualToString:@"4"]){   // test printer
        NSDictionary *infoStaff = [NSDictionary dictionaryWithObject:deviceData.staff];
        NSString *nameStaff = [NSString checkNull:infoStaff[@"name"]];
        NSString *line        = @"- - - - - - - - - - - - - - - - - - - - - - - -";
        NSMutableArray *arrL  = [NSMutableArray array];
        [arrL addObject:@"Test Printer "];
        [arrL addObject:[NSString stringWithFormat:@"Brand Printer : %@",self.brandPrinter]];
        [arrL addObject:[NSString stringWithFormat:@"Name : %@",self.namePrint]];
        [arrL addObject:[NSString stringWithFormat:@"IP   : %@",self.IP]];
        [arrL addObject:[NSString stringWithFormat:@"By   : %@",nameStaff]];
        [arrL addObject:line];
        [arrL addObject:@"cut"];
        [dataQ addObject:arrL];
    }
    if (self.isPrintEPSON) {
        BOOL bl = [self.typePrint isEqualToString:@"4"]?YES:NO;
        [self openPrinter:self.listDataPrinter andTest:bl andDataText:dataQ sus:^(BOOL completio) {
            if (completio) {
                 DDLogError(@"createViewPrint openprinter");
            }
            else
                DDLogError(@"error createViewPrint openprinter");
        }];
        if (!bl) {
            DDLogError(@"error createViewPrint openprinter");
            loadProgress = [LoadProcess sharedInstance];
            [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบเครื่องปริ้นเตอร์"];
        }
        return bl;
    }
    else if ([dataQ count] != 0) {
        [self printQ:0 andQSub:0  andData:dataQ];
    }
    return true;
}

-(NSMutableArray *)sortDataOrder{

    NSString *key ;
    NSString *value ;
    if ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คเมนูซ้ำ", nil)]) {
        key = self.dataPrint[@"menu_id"];
        value =  @"menu_id = %@";
    }
    else if ([self.sortRepeat isEqualToString:AMLocalizedString(@"เช็คโต๊ะซ้ำ", nil)]) {
        key = self.dataPrint[@"table_id"];
        value =  @"table_id = %@";
    }
    
    NSMutableArray *dataOrder  = (NSMutableArray *)[NSDictionary searchDictionaryALL:key  andvalue:value andData:self.rowData];
    
    return dataOrder;
}

-(NSMutableArray *) sortCategoryMenuTable:(NSMutableArray *)data{
    NSMutableArray *tempArr = [NSMutableArray array];
    NSMutableArray *listData = [NSMutableArray arrayWithArray:data];
    
    while ([listData count] > 0) {
        NSDictionary *dix = [listData firstObject];
        NSString *key = dix[@"table_id"];
        NSString * value =  @"table_id = %@";
        NSMutableArray *dataOrder  = (NSMutableArray *)[NSDictionary searchDictionaryALL:key  andvalue:value andData:data];
        [tempArr addObjectsFromArray:dataOrder];

        [listData removeObjectsInArray:dataOrder];
//        NSLog(@"%@",listData);
    }
    return [tempArr mutableCopy];
}

-(NSMutableArray *) sortDataKitchenPrint:(BOOL)blTitle andCut:(BOOL)blCut andData:(NSDictionary *)data{
    NSMutableArray *dataSub = [NSMutableArray array];
    NSDictionary *menu = self.menuData[[NSString stringWithFormat:@"id_%@",data[@"menu_id"]]];
    NSDictionary *infoStaff = [NSDictionary dictionaryWithObject:deviceData.staff];
    NSString *nameStaff = [NSString checkNull:infoStaff[@"name"]];
    
    NSString *foodOption = @"";
    if (![Check checkNull:data[@"food_option"]]) {
        foodOption = [FoodOptionSum getFoodOption:data[@"food_option"]];
    }

    NSString *foodComment = [NSString checkNull:data[@"food_comment"]];
    NSString *price       = [NSString checkNull:data[@"price"]];
    price  = [NSString stringWithFormat:AMLocalizedString(@" ราคา %@ บาท ", nil),price];
    
    NSString *idOrder;
    if ([Check checkNull:data[@"order_id"]]) {
        idOrder     = [NSString stringWithFormat:@" # %@ ",data[@"id"]];
    }
    else{
        idOrder     = [NSString stringWithFormat:@" # %@ ",data[@"order_id"]];
    }

    NSString *menuID = @"";
    if (self.showIdMenu) {
        menuID = [NSString stringWithFormat:@" [%@]",[NSString checkNull:menu[@"id"]]];
    }
    
    NSString *name = [NSString stringWithFormat:@" %@ %@ (%@)",menuID,menu[@"name"],data[@"n"]];
    NSString *line = @"- - - - - - - - - - - - - - - - - - - - - - - -";
    
    NSString *deliverStatus    = [NSString checkNull:data[@"deliver_status"]];
    NSString *time        = [Time getTHTimeFromDate:[NSString checkNull:data[@"created_datetime"]]];
    NSString *tableID = data[@"table_id"];
    NSString *tableNum = @"";
    
    if (blTitle && ![deliverStatus isEqualToString:@"4"]) {
        if ([tableID isEqualToString:@"0"]) {
            float maxBill = [NSString checkNullConvertFloat:self.maxPrevBillId];
            float minBill = [NSString checkNullConvertFloat:data[@"bill_id"]];
            tableNum   = [NSString stringWithFormat:@" Q %f",maxBill - minBill];
            foodComment = [NSMutableString stringWithString:AMLocalizedString(@"กลับบ้าน", nil)];
            
        }
        else{
            tableNum = [NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"โต๊ะ", nil),self.tableData[[NSString stringWithFormat:@"id_%@",tableID]][@"label"]];
        }
        
        tableNum = [tableNum stringByAppendingString:@"   "];
        tableNum = [tableNum stringByAppendingString:time];
        [dataSub addObject:@"table"];
        [dataSub addObject:tableNum];
    }
    else if ([deliverStatus isEqualToString:@"4"]){
        NSString *tableNum = @"";
        NSString *tableID = data[@"table_id"];
        NSString *cancle  = [NSString stringWithString:AMLocalizedString(@" X ยกเลิก", nil)];
        tableNum   = [NSString stringWithFormat:@" %@ %@",AMLocalizedString(@" โต๊ะ", nil),self.tableData[[NSString stringWithFormat:@"id_%@",tableID]][@"label"]];
        
        tableNum = [tableNum stringByAppendingString:@"   "];
        tableNum = [tableNum stringByAppendingString:time];
        
        [dataSub addObject:@"cancle"];
        [dataSub addObject:cancle];
        [dataSub addObject:tableNum];
    }
    
    [dataSub addObject:idOrder];
    [dataSub addObject:name ];
    if (![Check checkNull:foodOption] && ![foodOption isEqualToString:@""] && ![foodOption isEqualToString:@" "]) {
        foodOption  = [NSString stringWithFormat:@"     ( %@ )",foodOption];
        [dataSub addObject:foodOption];
    }
    
    [dataSub addObject:price];
    
    if (![Check checkNull:foodComment] && ![foodComment isEqualToString:@""] && ![foodComment isEqualToString:@" "]) {
        foodComment  = [NSString stringWithFormat:@"    ( %@ ) ",foodComment];
        [dataSub addObject:foodComment];
    }
    
   [dataSub addObject:[NSString stringWithFormat:@" %@   : %@",AMLocalizedString(@"โดย", nil),nameStaff]];
   [dataSub addObject:line];
    if (blCut) {
        [dataSub addObject:@"cut"];
    }

    return dataSub;
}
-(void)printQ:(int)countQ andQSub:(int)countQSub andData:(NSMutableArray *)dataQ{
    
    @try {
        NSString *text = dataQ[countQ][countQSub];
        int font = fontK;
        
        __block  int countQBlock = countQ;
        __block  int countQSubBlock = countQSub;
        __block  BOOL blCut = NO;
        
        if ([text isEqualToString:@"cut"]) {
            text = @"I--";
            blCut = YES;
        }
        else if ([text isEqualToString:@"table"] || [text isEqualToString:@"total"]) {
            countQSubBlock ++;
            text = dataQ[countQ][countQSubBlock];
            font = fontKT;
        }
        else if ([text isEqualToString:@"cancle"]) {
            countQSubBlock ++;
            text = dataQ[countQ][countQSubBlock];
            font = fontKC;
        }
        
        if (blCut) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSData *data = [NSData dataWithBytes:CUT_PAPER_FEED length:sizeof(CUT_PAPER_FEED)];
                [outputStream write:[data bytes] maxLength:[data length]];
                
                countQSubBlock ++;
                
                NSArray *ddQ =  dataQ[countQBlock];
                if (countQSubBlock >= [ddQ count]) {
                    countQBlock++;
                    countQSubBlock = 0;
                }
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if (countQBlock < [dataQ count]) {
                        [UIView animateWithDuration:10.0 animations:^{
                            [self printQ:countQBlock andQSub:countQSubBlock andData:dataQ];
                        }];
                    }
                    [LoadProcess dismissLoad];
                });
            });
        }
        else if (countQBlock < [dataQ count]) {
            [self captureScreen:text font:font  success:^(BOOL completion) {
                if (completion) {
                    //NSLog(@"captureScreen   compele %@",text);
                    countQSubBlock ++;
                    
                    NSArray *ddQ =  dataQ[countQBlock];
                    if (countQSubBlock >= [ddQ count]) {
                        countQBlock++;
                        countQSubBlock = 0;
                    }
                    
                    if (countQBlock < [dataQ count]) {
                        [UIView animateWithDuration:7.0 animations:^{
                            [self printQ:countQBlock andQSub:countQSubBlock andData:dataQ];
                        }];
                    }
                }
                else
                    return ;
            }];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"printQ  exception %@",exception);
    }
}

-(NSString*)hexRepresentationWithSpaces_AS:(BOOL)spaces data:(NSData *)data
{
    @try {
        const unsigned char* bytes = (const unsigned char*)[data bytes];
        NSUInteger nbBytes = [data length];
        static const NSUInteger spaceEveryThisManyBytes = 4UL;
        static const NSUInteger lineBreakEveryThisManySpaces = 4UL;
        const NSUInteger lineBreakEveryThisManyBytes = spaceEveryThisManyBytes * lineBreakEveryThisManySpaces;
        NSUInteger strLen = 2*nbBytes + (spaces ? nbBytes/spaceEveryThisManyBytes : 0);
        
        NSMutableString* hex = [[NSMutableString alloc] initWithCapacity:strLen];
        for(NSUInteger i=0; i<nbBytes; ) {
            [hex appendFormat:@"%02X", bytes[i]];
            ++i;
            
            if (spaces) {
                if (i % lineBreakEveryThisManyBytes == 0) [hex appendString:@"\n"];
                else if (i % spaceEveryThisManyBytes == 0) [hex appendString:@" "];
            }
        }
        return hex;
    }
    @catch (NSException *exception) {
        DDLogError(@"hexRepresentationWithSpaces_AS  exception %@",exception);
    }
}
- (NSArray *)UIImageToByteArray:(UIImage*)image; {
    
    UIImage*imageOBJ;
    NSArray*arr;
    if (imageOBJ) {
        NSData *data = UIImagePNGRepresentation(image);
        
        NSUInteger len = data.length;
        uint8_t *bytes = (uint8_t *)[data bytes];NSMutableString *result1 = [NSMutableString  stringWithCapacity:len * 3];
        
        for (NSUInteger i = 0; i < len; i++) {
            if (i) {
                [result1 appendString:@","];
            }
            [result1 appendFormat:@"%d", bytes[i]];
        }
        
        arr = [result1 componentsSeparatedByString:@","];
    }
    return arr;
}
- (NSArray*)getRGBAsFromImage:(UIImage*)image atX:(int)x andY:(int)y count:(int)count
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];
    
    // First get the image into your data buffer
    CGImageRef imageRef = [image CGImage];
    NSUInteger width = CGImageGetWidth(imageRef);
    NSUInteger height = CGImageGetHeight(imageRef);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char *rawData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
    NSUInteger bytesPerPixel = 4;
    NSUInteger bytesPerRow = bytesPerPixel * width;
    NSUInteger bitsPerComponent = 8;
    CGContextRef context = CGBitmapContextCreate(rawData, width, height,
                                                 bitsPerComponent, bytesPerRow, colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
    CGContextRelease(context);
    
    NSUInteger byteIndex = (bytesPerRow * y) + x * bytesPerPixel;
    for (int i = 0 ; i < count ; ++i)
    {
        CGFloat red   = (rawData[byteIndex]     * 1.0) / 255.0;
        CGFloat green = (rawData[byteIndex + 1] * 1.0) / 255.0;
        CGFloat blue  = (rawData[byteIndex + 2] * 1.0) / 255.0;
        CGFloat alpha = (rawData[byteIndex + 3] * 1.0) / 255.0;
        byteIndex += bytesPerPixel;
        
        UIColor *acolor = [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
        [result addObject:acolor];
    }
    
    free(rawData);
    
    return result;
}
-(void)captureScreen:(NSString *)text font:(int )font success:(void (^)(BOOL completion)) block{
//    NSLog(@"captureScreen");
    NSDictionary *attributes = @{NSFontAttributeName            : [UIFont systemFontOfSize:font],
                                 NSForegroundColorAttributeName : [UIColor blackColor],
                                 NSBackgroundColorAttributeName : [UIColor clearColor]};
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self imageFromText:text attributes:attributes success:^(UIImage *completion) {
            completion = [UIImage imageWithData:UIImageJPEGRepresentation(completion,1.0)];
            __block  NSData *pectedData;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                pectedData = [self logPixelsOfImage:completion];
                dispatch_sync(dispatch_get_main_queue(), ^{
//                    NSLog(@"write data start");
                    @try {
                         [outputStream write:[pectedData bytes] maxLength:[pectedData length]];
                    }
                    @catch (NSException *exception) {
                        DDLogError(@"exception printTest: %@",exception);
                        block(YES);
                    }
                   
//                    NSLog(@"write data end");
                    if (errorPrint) {
                        block(NO);
                    }
                    else{
                        block(YES);
                    }
                });
            });
        }];
    });
}
-(void)printTest:(UIImage *)image success:(void (^)(BOOL completion)) block{
    UIImage *completion = [UIImage imageWithData:UIImageJPEGRepresentation(image,1.0)];
    __block  NSData *pectedData;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        pectedData = [self logPixelsOfImage:completion];
        dispatch_sync(dispatch_get_main_queue(), ^{
            @try {
                [outputStream write:[pectedData bytes] maxLength:[pectedData length]];
                if (errorPrint) {
                    block(NO);
                }
                else{
                    block(YES);
                }
            }
            @catch (NSException *exception) {
                DDLogError(@"exception printTest: %@",exception);
                block(NO);
            }
        });
    });
}
-(void)imageFromText:(NSString *)text attributes:(NSDictionary *)attributes success:(void (^)(UIImage *completion)) block
{
    @try {
        UIFont *font  = [attributes objectForKey:NSFontAttributeName];
        CGSize size = [text sizeWithAttributes:
                       @{NSFontAttributeName:font}];
        
        size.height += 15;
        int a = 700.0;
        int b = a+160.0;
        
        float actualHeight = size.height;
        float actualWidth = size.width;
        float imgRatio = actualWidth/actualHeight;
        float maxRatio = a/b;
        
        if(imgRatio!=maxRatio){
            if(imgRatio < maxRatio){
                imgRatio = b / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = size.height;
            }
            else{
                imgRatio = a / actualWidth;
                actualHeight = size.height;
                actualWidth = a;
            }
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            UIGraphicsBeginImageContext(CGSizeMake(actualWidth, actualHeight));
            [text drawAtPoint:CGPointMake(0.0, 0.0) withFont:font];
//            [text drawAtPoint:CGPointMake(0.0, 0.0) withAttributes:attributes];
            UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIGraphicsBeginImageContext(CGSizeMake(actualWidth, actualHeight));
            [img drawInRect:CGRectMake(0,0,actualWidth,actualHeight)];
            UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                block(newImage);
            });
        });
    }
    @catch (NSException *exception) {
        DDLogError(@"imageFromText exception :%@",exception);
    }
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {

    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:{
            DDLogError(@"Stream opened");
        }
            break;
        case NSStreamEventHasBytesAvailable:
             DDLogError(@"NSStreamEventHasBytesAvailable");
            break;
        case NSStreamEventErrorOccurred:{
            DDLogError(@"Can not connect to the host!");
            if (!errorPrint) {
                errorPrint = YES;
                [LoadProcess dismissLoad];
                UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                                   message:AMLocalizedString(@"กรุณาตรวจสอบเครื่องปริ้นเตอร์", nil)
                                                                  delegate:self
                                                         cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                                         otherButtonTitles:nil];
                [alerView show];
            }
        }
            break;
            
        case NSStreamEventEndEncountered:
             DDLogError(@"NSStreamEventEndEncountered ");
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [theStream release];
            theStream = nil;
            
            break;
        default:{
            DDLogError(@"Unknown event");
        }
    }
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSLog(@"printer");
    }
    else if (buttonIndex == 0){
        [PrinterSetingViewController setIsseclect];
    }
}
- (void) messageReceived:(NSString *)message {
    
    DDLogDebug(@"messageReceived %@",message);
    //	[self.messages addObject:message];
    //	[self.tView reloadData];
    //	NSIndexPath *topIndexPath = [NSIndexPath indexPathForRow:messages.count-1
    //												   inSection:0];
    //	[self.tView scrollToRowAtIndexPath:topIndexPath
    //					  atScrollPosition:UITableViewScrollPositionMiddle
    //							  animated:NO];
}
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (NSMutableData *)logPixelsOfImage:(UIImage*)image {
    @try {
//         NSLog(@"logPixelsOfImage");
        // 1. Get pixels of image
        CGImageRef inputCGImage = [image CGImage];
        NSUInteger width = CGImageGetWidth(inputCGImage);
        NSUInteger height = CGImageGetHeight(inputCGImage);
        
        NSUInteger bytesPerPixel = 4;
        NSUInteger bytesPerRow = bytesPerPixel * width;
        NSUInteger bitsPerComponent = 8;
        
        UInt32 * pixels;
        pixels = (UInt32 *) calloc(height * width, sizeof(UInt32));
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGContextRef context = CGBitmapContextCreate(pixels, width, height,
                                                     bitsPerComponent, bytesPerRow, colorSpace,
                                                     kCGImageAlphaPremultipliedLast|kCGBitmapByteOrder32Big);
        
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), inputCGImage);
        
        CGColorSpaceRelease(colorSpace);
        CGContextRelease(context);
        
    #define Mask8(x) ( (x) & 0xFF )
    #define R(x) ( Mask8(x) )
    #define G(x) ( Mask8(x >> 8 ) )
    #define B(x) ( Mask8(x >> 16) )
        
        int bitLen = (int)width / 8;
        int zeroCount = width % 8;
        NSString* zeroStr = @"";
        if (zeroCount > 0) {
            bitLen = (int)(width / 8 + 1);
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = [zeroStr stringByAppendingString:@"0"];
            }
        }
        NSMutableArray *listString = [NSMutableArray array];
    
        UInt32 * currentPixel = pixels;
        for (NSUInteger j = 0; j < height; j++) {
            NSString* sb = @"";
            for (NSUInteger i = 0; i < width; i++) {
                UInt32 color = *currentPixel;
                int rgb = (R(color)+G(color)+B(color))/3.0;
                if (rgb > 160)
                    sb = [sb stringByAppendingString:@"0"];
                else
                    sb = [sb stringByAppendingString:@"1"];
                
                currentPixel++;
            }
            if (zeroCount > 0) {
                sb = [sb stringByAppendingString:zeroStr];
            }
            [listString addObject:sb];
        }
        free(pixels);
        
    #undef R
    #undef G
    #undef B
        
        NSMutableArray *listHex1 = [self hexadecimalString:listString];
        NSString *commandHexString = @"1D763000";
        NSString *widthHexString = [NSString stringWithFormat:@"%lX",(width % 8 == 0 ? width / 8 : (width / 8 + 1))];
        
        if ([widthHexString length] > 2) {
            DDLogError(@"decodeBitmap error width is too large");
            return nil;
        }
        else if ([widthHexString length] == 1) {
            widthHexString = [@"0" stringByAppendingString:widthHexString];
        }
        widthHexString = [widthHexString stringByAppendingString:@"00"];
        
        
        NSString *heightHexString = [NSString stringWithFormat:@"%lX",(unsigned long)height];
        
        if ([heightHexString length] > 2) {
            DDLogError(@"decodeBitmap error width is too large");
            return nil;
        }
        else if ([heightHexString length] == 1) {
            heightHexString = [@"0" stringByAppendingString:heightHexString];
        }
        heightHexString = [heightHexString stringByAppendingString:@"00"];
        
        
        
        NSMutableArray *commandList = [NSMutableArray array];
        [commandList addObject:[NSString stringWithFormat:@"%@%@%@",commandHexString,widthHexString,heightHexString]];
        [commandList addObjectsFromArray:listHex1];
        
        uint8_t *result1 = (uint8_t *)malloc(sizeof(uint8_t) * ([commandList count] *8));
        dataResult = [NSMutableData data];
        result1 = [self hexList2Byte:commandList];
        return dataResult;
        }
    @catch (NSException *exception) {
        DDLogError(@"logPixelsOfImage  exception %@",exception);
    }
}
- (NSMutableArray *)hexadecimalString:(NSMutableArray *)listString {
    
    NSMutableArray *listHex = [NSMutableArray array];
    for (NSString *str in listString) {
        NSMutableString   *hexString  = [NSMutableString stringWithFormat:@""];
        for (int i = 0; i < [str length]; i += 8){
            NSString *str1 = [str substringWithRange:NSMakeRange(i,8)];
            str1 = [self stringToHex:str1];
            [hexString appendString:str1];
        }
        [listHex addObject:hexString];
    }
    
    return listHex;
}
- (NSString *) stringToHex:(NSString *)str
{
    
    @try {
        _binaryArrayClass = [[self class] binaryArray];
        _hexStrClass = [[self class] hexStr];

        NSString *hex = @"";
        NSString *f4 = [str substringWithRange:NSMakeRange(0,4)];
        NSString *b4 = [str substringWithRange:NSMakeRange(4,4)];

        int length = (int)[_hexStrClass count] ;
        for (int i = 0; i < length; i++) {
            NSString *binarr = [NSString stringWithFormat:@"%@",_binaryArrayClass[i]];
            if ([f4 isEqualToString:binarr]) {
                hex = [hex stringByAppendingString:_hexStrClass[i]];
            }
        }
        for (int i = 0; i < length; i++) {
            NSString *binarr = [NSString stringWithFormat:@"%@",_binaryArrayClass[i]];
            if ([b4 isEqualToString:binarr]) {
                hex = [hex stringByAppendingString:_hexStrClass[i]];
            }
        }
        return hex;
    }
    @catch (NSException *exception) {
        DDLogError(@"stringToHex  exception %@",exception);
    }
}

-(uint8_t *) hexList2Byte:(NSArray *)list{
    
    @try {
        int i = 0;
        int length = (int)[list count]*8;
        uint8_t *result = (uint8_t *)malloc(sizeof(uint8_t) * (length));
        for (NSString *hexstr in list) {
            NSString* str = [NSString stringWithFormat:@"%2@",hexstr];
            [self hexStringToBytes:str];
            i++;
        }
        free(result);
        return result;
    }
    @catch (NSException *exception) {
        DDLogError(@"hexList2Byte  exception %@",exception);
    }
}
-(void)hexStringToBytes:(NSString *)hexString{
    
    @try {
        if (hexString == nil || [hexString isEqualToString:@""]) {
            return;
        }
        hexString = [hexString uppercaseString];
        int length = (int)[hexString length] /2;
        
        unsigned char *buffer = (unsigned char *)calloc(length, sizeof(unsigned char));
        for(int i = 0; i < length ; i++) {
            int pos = i * 2;
            NSRange range = {pos, 2 };
            NSString *subString = [hexString substringWithRange:range];
     
            unsigned value;
            [[NSScanner scannerWithString:subString] scanHexInt:&value];
            uint8_t theInt = (uint8_t)value;
            int size = sizeof(theInt);
            [dataResult appendBytes:&theInt length:size];
        }
        free(buffer);
    }
    @catch (NSException *exception) {
        DDLogError(@"hexStringToBytes  exception %@",exception);
    }
}
-(void)blockVender:(int)numKey andS:(int)numSec suss:(void (^)(bool completion)) block{

   NSLog(@" %d : %d",numKey,numSec);
    if (self.isPrintEPSON) {
        [self openPrinter:self.listDataPrinter andTest:NO andDataText:self.dataImagePrint sus:^(BOOL completion) {
            [LoadProcess dismissLoad];
            if (completion) {
                [loadProgress.loadView showSuccess];
            }
            else{
                [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบเครื่องปริ้นเตอร์"];
            }
            block(completion);
        }];
    }
    else if ([self.dataImagePrint count] != 0) {
        __block int numI = numKey;
        __block int numS = numSec;
        NSMutableArray *dataSection = self.dataImagePrint[[@(numS) stringValue]];
        if ([dataSection count] != 0) {
            UIView *view = dataSection[numI];
            [self imageWithView:(UIView *)view suss:^(UIImage *completion) {
                [self printTest:completion success:^(BOOL completion) {
                    if (completion) {
                        if (numS <= [self.dataImagePrint count]) {
                            if (numI >= [dataSection count]-1) {
                                numI = -1;
                                numS++;
                            }
                            [self blockVender:++numI andS:numS suss:^(bool completion) {
                                //NSLog(@"completion");
                                block(YES);
                            }];
                        }
                    }
                    else{
                        DDLogError(@"blockVender not completion");
                        return ;
                    }
                    
                }];
            }];
        }
        else{
            NSData *data = [NSData dataWithBytes:CUT_PAPER_FEED length:sizeof(CUT_PAPER_FEED)];
            [outputStream write:[data bytes] maxLength:[data length]];
            [outputStream close];
            block(YES);
        }
    }
}
- (void )imageWithView:(UIView *)view suss:(void (^)(UIImage *completion)) block
{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        [self setFontSizeHight:view suss:^(UIView *completion) {
//            NSLog(@"s %f : %f",completion.bounds.size.width,completion.bounds.size.height);
            int a = 580.0;
            int b = a+160;
            CGRect frame = CGRectMake(0, 0,completion.bounds.size.width,completion.bounds.size.height);
            CGSize size = frame.size;
            
            float actualHeight = size.height;
            float actualWidth  = size.width;
            float imgRatio     = actualWidth/actualHeight;
            float maxRatio     = a/b;
            
            if(imgRatio!=maxRatio){
                if(imgRatio < maxRatio){
                    imgRatio     = b / actualHeight;
                    actualWidth  = imgRatio * actualWidth;
                    actualHeight = size.height;
                }
                else{
                    imgRatio = a / actualWidth;
                    actualHeight = size.height;
                    actualWidth = a;
                }
            }
            
            frame = CGRectMake(-10, 0,actualWidth+10,actualHeight);
            [view setFrame:frame];
            
            UIGraphicsBeginImageContext(frame.size);
            [completion drawViewHierarchyInRect:frame afterScreenUpdates:YES];
            UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
//            NSLog(@"e %f : %f",img.size.width,img.size.height);
            dispatch_async(dispatch_get_main_queue(), ^{
                 block(img);
////                dispatch_release(dispatch_get_main_queue());
            });
////            dispatch_release(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0));
        }];
//    });
}

-(void)setFontSizeHight:(UIView *)vi suss:(void (^)(UIView *completion)) block{
    CellTableViewAll *cell1 = (CellTableViewAll *)vi;
    if ([vi isKindOfClass:[CellTableViewAll class]]) {
        if (vi.tag != 999) {
            for (UIView *view in [cell1.contentView subviews]) {
                if ([view isKindOfClass:[UILabel class]]) {
                    UILabel *label = (UILabel *)view;
                    if ([label.text rangeOfString:AMLocalizedString(@"บิลย่อยที่", nil)].location != NSNotFound) {
                        float largestFontSize = 26;
                        label.font = [UIFont fontWithName:@"TimesNewRomanPS-BoldMT" size:largestFontSize];
                    }
                    else{
                        [label setFont:[UIFont systemFontOfSize:23]];
                    }
                }
            }
        }
    }
    block(cell1);
}

#pragma mark - Printer EPSON
//- (BOOL)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL)blTest andDataText:(id)listData
//{
//    @try {
//        NSLog(@" 1. Quence open");
//        
//        fistTotal = YES;
//        BOOL blPrint = false;
//        //get open parameter
//        if(textIp.IP.length == 0){
//            [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบไอพีเครื่องปริ้นเตอร์"];
//            return false;
//        }
//        
//        int result;
//        self.printerEPSON = [PrinterEPSON sharedInstance];
//        [PrinterEPSON withAddress:textIp.IP];          // ต่อ printer
//        
//        if (!self.printerEPSON) {
//            [LoadProcess dismissLoad];
//            return false;
//        }
//        result = self.printerEPSON.result;
//        NSLog(@" 2 connect %d",result);
//        
////        int result = 0;
////        if (self.printerEPSON == nil && textIp.isSeclect) {
////            NSLog(@" 2. connect");
////            
////            self.printerEPSON = [PrinterEPSON sharedInstance];            
////            [self.printerEPSON withAddress:textIp.IP];
////
////            if (!self.printerEPSON) {
////                [LoadProcess dismissLoad];
////                return false;
////            }
////            result = self.printerEPSON.result;
////        }
//        
//        if (self.printerEPSON != nil && result == 0){
//             NSLog(@" 3. printerEPSON");
//            
//            int language = 0; //EPOS_OC_MODEL_THAI;;
//            EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
//            if(builder == nil){
//                [LoadProcess dismissLoad];
//                return false;
//            }
//            
//            if (self.isCashierPrint) {
//                NSLog(@" 4. isCashierPrint");
//            
//                blPrint = [self getBillCashier:builder];
//                result = [builder addFeedLine:2];
//                result = [builder addCut:EPOS_OC_CUT_FEED];
//                if(result != EPOS_OC_SUCCESS){
//                    [ShowMsg showExceptionEpos:result method:@"addCut"];
//                    [LoadProcess dismissLoad];
//                    return false;
//                }
//            }
//            else{
//                NSLog(@" 4. not isCashierPrint");
//                blPrint = true;
//                for (NSArray *arr in listData) {
//                    for (int i = 0; i < [arr count]; i++) {
//                        NSString *str = [NSString checkNull:arr[i]];
//                        DDLogInfo(@"%@",str);
//                        if (![str isEqualToString:@"cut"]) {
//                            
//                            if ([str isEqualToString:@"table"] || [str isEqualToString:@"cancle"]) {
//                                result = [builder addTextSize:2 Height:2];
//                                str = [NSString checkNull:arr[++i]];
//                            }
//                            else{
//                                result = [builder addTextSize:1 Height:1];
//                            }
//                            
//                            str = [NSString stringWithFormat:@"%@ \n",str];
//                            result = [builder addText:str];
//                            result = [builder addFeedLine:1];
//                            if(result != EPOS_OC_SUCCESS){
//                                [ShowMsg showExceptionEpos:result method:@"addText"];
//                                [LoadProcess dismissLoad];
//                                blPrint = false;
//                                return false;
//                            }
//                        }
//                        else{
//                            result = [builder addCut:EPOS_OC_CUT_FEED];
//                            if(result != EPOS_OC_SUCCESS){
//                                [ShowMsg showExceptionEpos:result method:@"addCut"];
//                                [LoadProcess dismissLoad];
//                                blPrint = false;
//                                return false;
//                            }
//                        }
//                    }
//                }
//            }
//            
//            NSLog(@" 5.1 Print start");
//            unsigned long status = 0;
//            unsigned long battery = 0;
//            result = [self.printerEPSON.printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
//            
//            //remove builder
//            [builder clearCommandBuffer];
//            NSLog(@" 5.2  Print end ");
////            if (blPrint) {
////                //send builder data
////                unsigned long status = 0;
////                unsigned long battery = 0;
////                result = [self.printerEPSON.printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
////                
////                //remove builder
////                [builder clearCommandBuffer];
//////                if (blTest) {  // ต่อ printer
//////                    [self.printerEPSON closePrinterEPSON];
//////                }
////            }
////            else{
////                blPrint = false;
////            }
//            [self.printerEPSON.printer closePrinter];
//            [PrinterEPSON closePrinterEPSON];
//        
//            NSLog(@" 6. closePrinterEPSON");
//            [LoadProcess dismissLoad];
//        }
//        else{
//            [LoadProcess dismissLoad];
//            [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบเครื่องปริ้นเตอร์"];
//        }
//       
//        NSLog(@" 7. end");
//        return blPrint;
//       
//    }
//    @catch (NSException *exception) {
//        DDLogError(@" error Quence openPrinter : %@",exception);
//    }
//}
//- (BOOL)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL)blTest andDataText:(id)listData
//{
//    @try {
//        DDLogWarn(@" 1. Quence open");
//        
//        fistTotal = YES;
//        BOOL blPrint = false;
//        //get open parameter
//        if(textIp.IP.length == 0){
//            [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบไอพีเครื่องปริ้นเตอร์"];
//            return false;
//        }
//        
//        //open
//        EposPrint *printer = [[EposPrint alloc] init];
//        if(printer == nil){
//            return false;
//        }
//        [delegate_ setEventCallback:printer];
//        
//        int connectionType = EPOS_OC_DEVTYPE_TCP;
//        // ต่อ printer
//        int result = [printer openPrinter:connectionType DeviceName:textIp.IP Enabled:EPOS_OC_FALSE Interval:10000];
//        if(result != EPOS_OC_SUCCESS){
//            DDLogError(@"error open");
//        }
//        DDLogWarn(@" 2 connect %d",result);
//        
//        if (result == 0){
//            DDLogWarn(@" 3. printerEPSON");
//        
//            int language = 0; //EPOS_OC_MODEL_THAI;;
//            EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
//            if(builder == nil){
//                [LoadProcess dismissLoad];
//                return false;
//            }
//            
//            if (self.isCashierPrint) {
//                DDLogWarn(@" 4. isCashierPrint");
//                
//                blPrint = [self getBillCashier:builder];
//                result = [builder addFeedLine:2];
//                result = [builder addCut:EPOS_OC_CUT_FEED];
//                if(result != EPOS_OC_SUCCESS){
//                    [ShowMsg showExceptionEpos:result method:@"addCut"];
//                    [LoadProcess dismissLoad];
//                    return false;
//                }
//            }
//            else{
//                DDLogWarn(@" 4. not isCashierPrint");
//                blPrint = true;
//                for (NSArray *arr in listData) {
//                    for (int i = 0; i < [arr count]; i++) {
//                        NSString *str = [NSString checkNull:arr[i]];
//                        DDLogInfo(@"%@",str);
//                        if (![str isEqualToString:@"cut"]) {
//                            
//                            if ([str isEqualToString:@"table"] || [str isEqualToString:@"cancle"]) {
//                                result = [builder addTextSize:2 Height:2];
//                                str = [NSString checkNull:arr[++i]];
//                            }
//                            else{
//                                result = [builder addTextSize:1 Height:1];
//                            }
//                            
//                            str = [NSString stringWithFormat:@"%@ \n",str];
//                            result = [builder addText:str];
//                            result = [builder addFeedLine:1];
//                            if(result != EPOS_OC_SUCCESS){
//                                [ShowMsg showExceptionEpos:result method:@"addText"];
//                                [LoadProcess dismissLoad];
//                                blPrint = false;
//                                return false;
//                            }
//                        }
//                        else{
//                            result = [builder addCut:EPOS_OC_CUT_FEED];
//                            if(result != EPOS_OC_SUCCESS){
//                                [ShowMsg showExceptionEpos:result method:@"addCut"];
//                                [LoadProcess dismissLoad];
//                                blPrint = false;
//                                return false;
//                            }
//                        }
//                    }
//                }
//            }
//            
//            DDLogWarn(@" 5.1 Print start");
//            //send builder data
//            unsigned long status = 0;
//            unsigned long battery = 0;
//            result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
//            [ShowMsg showStatus:result status:status battery:battery];
//            
//            //remove builder
//            [builder clearCommandBuffer];
//            DDLogWarn(@" 5.2 Print end");
//            
//            DDLogWarn(@"6 close epos ");
//            if(printer != nil){
//                int result = [printer closePrinter];
//                if(result != EPOS_OC_SUCCESS){
//                    DDLogError(@"closePrinter error");
//                }
//                printer = nil;
//            }
//            DDLogWarn(@"7 close epos");
//            [LoadProcess dismissLoad];
//        }
//        else{
//            [LoadProcess dismissLoad];
//            [loadProgress.loadView showWithStatusAddError:@"กรุณาตรวจสอบเครื่องปริ้นเตอร์"];
//        }
//        
//        DDLogWarn(@" 8. end");
//        return blPrint;
//        
//    }
//    @catch (NSException *exception) {
//        DDLogError(@" error Quence openPrinter : %@",exception);
//    }
//}
- (void)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL) blTest andDataText:(id)listData sus:(void (^)(BOOL completion)) block{
    BOOL blPrint = true;
    fistTotal = YES;
    @try {
        NSLog(@"1 Setting open");
        //get open parameter
        if(textIp.IP.length == 0){
            block(false);
        }
        
        //open
        EposPrint *printer = [[EposPrint alloc] init];
        if(printer == nil){
            block(false);
        }
        [delegate_ setEventCallback:printer];
        
        int connectionType = EPOS_OC_DEVTYPE_TCP;
        // ต่อ printer
        int result = [printer openPrinter:connectionType DeviceName:textIp.IP Enabled:EPOS_OC_FALSE Interval:1000];
        if(result != EPOS_OC_SUCCESS){
            DDLogError(@"error open");
        }
        NSLog(@"2 connect %d",result);
        
        NSLog(@"3 add epos");
        int language = 0; //EPOS_OC_MODEL_THAI;;
        EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
        if(builder == nil){
            block(false);
        }
        
        if (self.isCashierPrint) {
            DDLogWarn(@" 4. isCashierPrint");
            
            blPrint = [self getBillCashier:builder];
            result = [builder addFeedLine:2];
            result = [builder addCut:EPOS_OC_CUT_FEED];
            if(result != EPOS_OC_SUCCESS){
                 block(false);
            }
        }
        else{
            DDLogWarn(@" 4. not isCashierPrint");
            blPrint = true;
            for (NSArray *arr in listData) {
                for (int i = 0; i < [arr count]; i++) {
                    NSString *str = [NSString checkNull:arr[i]];
                    DDLogInfo(@"%@",str);
                    if (![str isEqualToString:@"cut"]) {
                        
                        if ([str isEqualToString:@"table"] || [str isEqualToString:@"cancle"]) {
                            result = [builder addTextSize:2 Height:2];
                            str = [NSString checkNull:arr[++i]];
                        }
                        else{
                            result = [builder addTextSize:1 Height:1];
                        }
                        
                        str = [NSString stringWithFormat:@"%@ \n",str];
                        result = [builder addText:str];
                        result = [builder addFeedLine:1];
                        if(result != EPOS_OC_SUCCESS){
                             block(false);
                        }
                    }
                    else{
                        result = [builder addCut:EPOS_OC_CUT_FEED];
                        if(result != EPOS_OC_SUCCESS){
                           block(false);
                        }
                    }
                }
            }
        }
        
        //send builder data
        unsigned long status = 0;
        unsigned long battery = 0;
        result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
        
        //remove builder
        [builder clearCommandBuffer];
        
        NSLog(@"4 close epos ");
        if(printer != nil){
            int result = [printer closePrinter];
            if(result != EPOS_OC_SUCCESS){
                DDLogError(@"closePrinter error");
                block(false);
            }
            printer = nil;
        }
        NSLog(@"5 close epos");
        [LoadProcess dismissLoad];
    }
    @catch (NSException *exception) {
        DDLogError(@" error Setting openPrinter : %@",exception);
    }
    @finally {
        NSLog(@"6 block");
        block(blPrint);
    }
}
-(BOOL)getBillCashier:(EposBuilder *)builder {
    NSLog(@" 4.1  getBillCashier");
    __block int result = 0;
    for (NSArray *arr in [self.dataImagePrint allValues]) {
        for (id view in arr) {
            if ([view isKindOfClass:[UIImageView class]]) {
        
                UIImageView *img = (UIImageView *)view;
                UIImage *image = [self compressImage:img.image];
                long width  = MIN(IMAGE_WIDTH_MAX,image.size.width);
                long height = image.size.height;
                result = [builder addImage:image
                                         X:0
                                         Y:0
                                     Width:width
                                    Height:height
                                     Color:EPOS_OC_COLOR_1
                                      Mode:[self getBuilderMode]
                                  Halftone:[self getBuilderHalftone]
                                Brightness:[self getBuilderBrightness]];
                if(result != EPOS_OC_SUCCESS){
                    DDLogError(@"addImage");
                    return false;
                }
                result = [builder addFeedLine:1];
            }
            else if ([view isKindOfClass:[UILabel class]]){
                if(result != EPOS_OC_SUCCESS){
                    DDLogError(@"addTextStyle");
                    [LoadProcess dismissLoad];
                    return false;
                }
                
                UILabel *lab = (UILabel *)view;
                int tag = ((int)lab.tag == 0)?3:(int)lab.tag;
                NSString *text = [NSString stringWithFormat:@"%@",lab.text];
                text = [self stringByAddingSpace:40 andText:text andInsertForward:tag andNotReplacSpace:NO];
                text = [NSString stringWithFormat:@"   %@\n",text];
                NSLog(@"%@",text);
                
                result = [builder addText:text];
                if(result != EPOS_OC_SUCCESS){
                     DDLogError(@"addText");
                    [LoadProcess dismissLoad];
                    return false;
                }
                

            }
            else if ([view isKindOfClass:[CellTableViewAll class]]){
                CellTableViewAll *cell = (CellTableViewAll *)view;
                NSIndexPath *indexPath = [self.tableImagePrint indexPathForCell:cell];
                if (indexPath.row == 0 && indexPath.section == 0) {
                    NSString *mCountBill = [NSString stringWithFormat:@"%@",cell.mCountBill.text];
                    NSString *mNameTable = [NSString stringWithFormat:@"%@",cell.mNameTable.text];
                   
                    mCountBill = [self stringByAddingSpace:22 andText:mCountBill andInsertForward:3 andNotReplacSpace:NO];
                    mNameTable = [self stringByAddingSpace:22 andText:mNameTable andInsertForward:3 andNotReplacSpace:NO];
                    NSString *text = [NSString stringWithFormat:@"  %@%@\n",mCountBill,mNameTable];
                    NSLog(@"%@",text);
                    result = [builder addText:text];
                    
                    if(result != EPOS_OC_SUCCESS){
                        DDLogError(@"addText");
                        [LoadProcess dismissLoad];
                        return false;
                    }
                    
                }
                else if (indexPath.section == 1 || cell.tag == 44){
                    
                    if (fistTotal == YES) {
                        fistTotal = NO;
                        result =  [self addLineText:builder];
                    }
                    if(cell.tag == 44 && indexPath.section == 0){
                        NSUInteger index = [arr indexOfObject:cell];
                        if (index == [arr count]-1) {
                            NSString *total     = [NSString stringWithFormat:@"%@",AMLocalizedString(@"ยอดรวมชำระ", nil)];
                            NSString *mTotalAll = [NSString stringWithFormat:@"%@",cell.mTotalAllFood.text];
                            NSString *mNumVat   = [NSString stringWithFormat:@"%@",cell.mNumVat.text];
                            NSString *textVat   = [NSString stringWithFormat:@"%@",cell.mTextVat.text];
                            
                            NSArray *arrText = [NSArray arrayWithObjects:textVat,total,nil];
                            NSArray *arrTotal = [NSArray arrayWithObjects:mNumVat,mTotalAll,nil];
                            
                            for (int i = 0; i < [arrText count]; i++) {
                                NSString *text    = [self stringByAddingSpace:30 andText:arrText[i] andInsertForward:2 andNotReplacSpace:NO];
                                NSString *total   = [self stringByAddingSpace:15 andText:arrTotal[i] andInsertForward:1 andNotReplacSpace:NO];
                                NSString *textAdd = [NSString stringWithFormat:@"  %@%@\n",text,total];
                                NSLog(@"%@",textAdd);
                                result = [builder addText:textAdd];
                                if(result != EPOS_OC_SUCCESS){
                                    DDLogError(@"addText");
                                    [LoadProcess dismissLoad];
                                    return false;
                                }
                            }
                        }
                        else{
                            NSString *mCountBill = [NSString stringWithFormat:@"%@",cell.mTotalFood.text];
                            NSString *mNameTable = [NSString stringWithFormat:@"%@",cell.mNameOrder.text];
                            NSString *text    = [self stringByAddingSpace:30 andText:mNameTable andInsertForward:2 andNotReplacSpace:NO];
                            NSString *total   = [self stringByAddingSpace:15 andText:mCountBill andInsertForward:1 andNotReplacSpace:NO];
                            NSString *textAdd = [NSString stringWithFormat:@"  %@%@\n",text,total];
                            NSLog(@"%@",textAdd);
                            result = [builder addText:textAdd];
                            if(result != EPOS_OC_SUCCESS){
                                DDLogError(@"addText");
                                [LoadProcess dismissLoad];
                                return false;
                            }
                        }
                    }
                    else{
                        NSString *differenceText   = [NSString stringWithFormat:@"%@",AMLocalizedString(@"รับเงิน / ทอนเงิน", nil)];
                        NSString *difference   = [NSString stringWithFormat:@"%@",cell.mDifference.text];
                        
                        NSString *text    = [self stringByAddingSpace:30 andText:differenceText andInsertForward:2 andNotReplacSpace:NO];
                        NSString *total   = [self stringByAddingSpace:15 andText:difference andInsertForward:1 andNotReplacSpace:NO];
                        NSString *textAdd = [NSString stringWithFormat:@"  %@%@\n",text,total];
                        NSLog(@"%@",textAdd);
                        result = [builder addText:textAdd];
                        if(result != EPOS_OC_SUCCESS){
                            DDLogError(@"addText");
                            [LoadProcess dismissLoad];
                            return false;
                        }
                    }
                }
                else if (indexPath.section == 0){
                    NSString *nameOrder  = cell.mNameOrder.text;
                    NSRange range = [nameOrder rangeOfString:@"บิลหลักที่" options:NSBackwardsSearch];
                    if (range.location != NSNotFound) continue;
                    
                    NSString *countOrder = cell.mCountOrder.text;
                    NSString *totalFood  = cell.mTotalFood.text;
                    
                    nameOrder  = [self stringByAddingSpace:25 andText:nameOrder andInsertForward:2 andNotReplacSpace:YES];
                    countOrder = [self stringByAddingSpace:7 andText:countOrder andInsertForward:1 andNotReplacSpace:YES];
                    totalFood  = [self stringByAddingSpace:13 andText:totalFood andInsertForward:1 andNotReplacSpace:YES];
                    
                    NSString *text = @"";
                    if (cell.mLabel.tag == -789) {
                        text = [NSString stringWithFormat:@"  --%@--%@%@%@\n",AMLocalizedString(@"ไม่", nil),nameOrder,countOrder,totalFood];
                    }
                    else{
                        text = [NSString stringWithFormat:@"  %@%@%@\n",nameOrder,countOrder,totalFood];
                    }
                   
                    NSLog(@"%@",text);
                    result = [builder addText:text];
                    if(result != EPOS_OC_SUCCESS){
                        DDLogError(@"addText");
                        [LoadProcess dismissLoad];
                        return false;
                    }
                }
            }
            else if ([view isKindOfClass:[UITableViewCell class]]){
//                CellTableViewAll *cell = (CellTableViewAll *)view;
//                NSIndexPath *indexPath = [self.tableImagePrint indexPathForCell:cell];
//                NSLog(@"index %ld : %ld",(long)indexPath.row,(long)indexPath.section);
                result =  [self addLineText:builder];
                
                NSString *nameOrder  = [self stringByAddingSpace:20 andText:AMLocalizedString(@"รายการ", nil) andInsertForward:2 andNotReplacSpace:NO];
                nameOrder = [NSString stringWithFormat:@"   %@",nameOrder];
                NSString *countOrder = [self stringByAddingSpace:10 andText:AMLocalizedString(@"จำนวน", nil) andInsertForward:1 andNotReplacSpace:NO];
                NSString *totalFood  = [self stringByAddingSpace:11 andText:AMLocalizedString(@"ราคา", nil) andInsertForward:1 andNotReplacSpace:NO];
                NSString *text = [NSString stringWithFormat:@"  %@%@%@\n",nameOrder,countOrder,totalFood];
                NSLog(@"%@",text);
                result = [builder addText:text];
                if(result != EPOS_OC_SUCCESS){
                    DDLogError(@"addText");
                    [LoadProcess dismissLoad];
                    return false;
                }
                
                if(result != EPOS_OC_SUCCESS){
                    DDLogError(@"addText");
                    [LoadProcess dismissLoad];
                    return false;
                }
                result = [self addLineText:builder];
            }
        }
    }
     NSLog(@" 4.2.  sus getBillCashier");
    return true;
}
-(UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 320.0;
    float maxWidth = 520.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5; //50 percent compression
    
    if(imgRatio < maxRatio){
        //adjust width according to maxHeight
        imgRatio = maxHeight / actualHeight;
        actualWidth = imgRatio * actualWidth;
        actualHeight = maxHeight;
    }
    else if(imgRatio > maxRatio){
        //adjust height according to maxWidth
        imgRatio = maxWidth / actualWidth;
        actualHeight = imgRatio * actualHeight;
        actualWidth = maxWidth;
    }
    else{
        actualHeight = maxHeight;
        actualWidth = maxWidth;
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}

-(NSString*)stringByAddingSpace:(NSInteger )lengStr andText:(NSString *)stringToAddSpace andInsertForward:(int)type andNotReplacSpace:(BOOL) bl{
    
    NSString *stringResult;
    if (!bl) {
        stringResult = stringToAddSpace;
    }
    else{
        stringResult = [stringToAddSpace stringByReplacingOccurrencesOfString:@" "
                                                                             withString:@""];
    }

    
    NSUInteger wordCount = [self wordCount:stringResult];
    NSInteger  lengWord = stringResult.length - wordCount;
    NSInteger leng;
    if (type == 3) {
        leng = ((lengStr)/2) -  (lengWord/2);
    }
    else{
       leng = lengStr - lengWord;
    }
    
    
    NSString *space = [@" " stringByPaddingToLength:leng
                                         withString:@" "
                                    startingAtIndex:0];
    NSString *result = @"";
    result = [self addSpace:type and:stringResult andText:space];
    return result;
}
-(NSString *)addSpace:(int)type and:(NSString *)result andText:(NSString *)space{
    
    switch (type) {
        case 1:{
            return [NSString stringWithFormat:@"%@%@",space,result];
        }
            break;
        case 2:{
            return [NSString stringWithFormat:@"%@%@",result,space];
        }
            break;
        case 3:{
            return [NSString stringWithFormat:@"%@%@%@",space,result,space];
        }
            break;
        default:
            return @"";
            break;
    }
}
-(int) addLineText:(EposBuilder *)builder{
    int result;
    NSString *line = @" -----------------------------------------------\n";
    NSLog(@"%@",line);
    result =  [builder addText:line];
    if(result != EPOS_OC_SUCCESS){
        DDLogError(@"addLineText");
        [LoadProcess dismissLoad];
        return false;
    }
    return result;
}
- (NSUInteger)wordCount:(NSString *)stringToAddSpace
{
    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:stringToAddSpace.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:stringToAddSpace];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@" ิ ี ึ ื ุ ู ั ํ ็ ์ ๊ ่ ๋ ้"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    
    return strippedString.length;
}

-(void) closeInStrem{
    [outputStream close];
    [outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream release];
    outputStream = nil;
}
-(void)dealloc{
//    [outputStream release];
}
- (int)getBuilderMode
{
    return EPOS_OC_MODE_MONO;
//    switch(modeList_.selectIndex){
//        case 1:
//            return EPOS_OC_MODE_GRAY16;
//        case 0:
//        default:
//            return EPOS_OC_MODE_MONO;
//    }
}

- (int)getBuilderHalftone
{
    return EPOS_OC_HALFTONE_DITHER;
    
//    switch(halftoneList_.selectIndex){
//        case 1:
//            return EPOS_OC_HALFTONE_ERROR_DIFFUSION;
//        case 2:
//            return EPOS_OC_HALFTONE_THRESHOLD;
//        case 0:
//        default:
//            return EPOS_OC_HALFTONE_DITHER;
//    }
}

- (double)getBuilderBrightness
{
    return 1.0 ; //[textBrightness_.text doubleValue];
}
@end

