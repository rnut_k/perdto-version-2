//
//  PrinterCreateController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "PrinterCreateController.h"

#import "Check.h"
#import "PopupView.h"


@interface PrinterCreateController()<UIAlertViewDelegate>{
    ListDataPrinter *listDataStructCreate;
    PopupView *popupView;
}

@end
@implementation PrinterCreateController

-(void)viewDidLoad{
    [super viewDidLoad];
    listDataStructCreate = [[ListDataPrinter alloc] init];
    self.automaticallyAdjustsScrollViewInsets = NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([self.mTextPaper isEqual:textField] || [self.mBrandPrinter isEqual:textField]) {
        [self selectModePrint:textField];
        [self resignFirstResponderText];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *text = textField.text;
    [textField resignFirstResponder];
    if (![text isEqualToString:@""]) {
        if ([self.mTextPaper isEqual:textField]) {
            listDataStructCreate.paper = text;
            NSLog(@"1 %@",listDataStructCreate.paper);
        }
        else if ([self.mNamePrinter isEqual:textField]) {
            listDataStructCreate.namePrinter = text;
            NSLog(@"2 %@",listDataStructCreate.namePrinter);
        }
        else if ([self.mIPAddress isEqual:textField]) {
            listDataStructCreate.IP = text;
            NSLog(@"3 %@",listDataStructCreate.IP);
        }
        else if ([self.mPort isEqual:textField]) {
            listDataStructCreate.port = text;
            NSLog(@"4 %@",listDataStructCreate.port);
        }
        else if ([self.mBrandPrinter isEqual:textField]) {
            listDataStructCreate.brandPrinter = text;
            NSLog(@"5 %@",listDataStructCreate.brandPrinter);
        }
    }
}

- (IBAction)btnAddPrinter:(id)sender {
    NSLog(@"btnAddPrinter");
    
    if (listDataStructCreate.IP == nil) {
        listDataStructCreate.IP = kIP;
    }
    if (listDataStructCreate.port == nil) {
        listDataStructCreate.port = kPort;
    }
    listDataStructCreate.isSeclect = NO;
    if ([self.printerSetingView.listPrinter count] == 0) {
        [self.printerSetingView.listPrinter addObject:listDataStructCreate];
    }
    else{
        [self.printerSetingView.listPrinter insertObject:listDataStructCreate atIndex:0];
    }
    [self.navigationController popViewControllerAnimated:YES];
    [self.printerSetingView.mTableViewPrinter reloadData];
}

- (IBAction)btnTestPrint:(id)sender {
    NSLog(@"btnTestPrint");
    if (listDataStructCreate.IP == nil) {
        listDataStructCreate.IP = kIP;
    }
    if (listDataStructCreate.port == nil) {
        listDataStructCreate.port = kPort;
    }
    
    if (listDataStructCreate.namePrinter == nil || listDataStructCreate.paper == nil) {
        UIAlertView *aler = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"แจ้งเตือน", nil)
                                                       message:AMLocalizedString(@"กรุณาตรวจสอบข้อมูล", nil)
                                                      delegate:self
                                             cancelButtonTitle:AMLocalizedString(@"ใช่", nil)
                                             otherButtonTitles:nil];
        [aler show];
    }
    else{
        [self.printerSetingView btnTestPrint:listDataStructCreate];
    }
}

-(void)selectModePrint:(id)sender{
    UITextField *textField = (UITextField *)sender;
    
    if (popupView == nil) {
        popupView = [[PopupView alloc] init];
        [popupView setPrinterCreateController:self];
    }
    
    [popupView setTanPopup:textField.tag];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {

    if ([Check checkNull:title]) {
        title = @"";
    }
    
    if (popupView.tanPopup == 1) {
//            arrBut = @[@"EPSON",@"Other"];
        [self.mTextPaper setText:title];
        listDataStructCreate.paper = title;
    }
    else if  (popupView.tanPopup == 2){
        [self.mBrandPrinter setText:title];
        listDataStructCreate.brandPrinter = title;
    }
}
-(void) resignFirstResponderText{
    
    [self.mPort resignFirstResponder];
    [self.mTextPaper resignFirstResponder];
    [self.mNamePrinter resignFirstResponder];
    [self.mIPAddress resignFirstResponder];
    [self.mBrandPrinter resignFirstResponder];
}
@end
