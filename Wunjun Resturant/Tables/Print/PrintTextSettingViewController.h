//
//  PrintTextSettingViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 11/7/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//
#import <UIKit/UIKit.h>
//#import "PickerTableView.h"
#import "UIViewControllerCustom.h"

@class EposPrint;

@interface PrintTextSettingViewController : UIViewControllerCustom {
    EposPrint *printer_;
    NSString* printername_;
    int language_;
//    PickerTableView *fontList_;
//    PickerTableView *alignList_;
//    PickerTableView *languageList_;
//    PickerTableView *sizeWidthList_;
//    PickerTableView *sizeHeightList_;
    
    UIView *activeViewItem;
}
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView_;
@property (retain, nonatomic) IBOutlet UIView *itemView_;
@property (retain, nonatomic) IBOutlet UITextView *textData_;
@property (retain, nonatomic) IBOutlet UIButton *buttonFont_;
@property (retain, nonatomic) IBOutlet UIButton *buttonAlign_;
@property (retain, nonatomic) IBOutlet UIButton *buttonLanguage_;
@property (retain, nonatomic) IBOutlet UITextField *textLineSpace_;
@property (retain, nonatomic) IBOutlet UIButton *buttonSizeWidth_;
@property (retain, nonatomic) IBOutlet UIButton *buttonSizeHeight_;
@property (retain, nonatomic) IBOutlet UISwitch *switchStyleBold_;
@property (retain, nonatomic) IBOutlet UISwitch *switchStyleUnderline_;
@property (retain, nonatomic) IBOutlet UITextField *textXPosition_;
@property (retain, nonatomic) IBOutlet UITextField *textFeedUnit_;

- (id)initWithPrinter:(EposPrint*)printer
          printername:(NSString*)printername
             language:(int)language;

@end
