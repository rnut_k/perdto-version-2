//
//  PrintReceiptViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSingleBillViewController.h"

@interface PrintReceiptViewController : UIViewController

@property (weak, nonatomic  ) IBOutlet UILabel     *mTitle;
@property (weak, nonatomic  ) IBOutlet UILabel     *mNameStore;
@property (weak, nonatomic  ) IBOutlet UILabel     *mDate;
@property (weak, nonatomic  ) IBOutlet UITableView *mTableOrder;
@property (weak, nonatomic  ) IBOutlet UIView      *mContainerView;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *layoutHightTableOrder;

@property (strong, nonatomic) NSString *amountReceived;
@property (strong, nonatomic) NSString *amountRemaininMoney;

@property (strong,nonatomic) NSMutableDictionary *sectionBill;
@property (nonatomic,strong) NSDictionary *dataListSingBill;
@property (nonatomic,strong) NSDictionary *dataSubbill;
@property (nonatomic,strong) NSString *indexSubBill;

@property (nonatomic,strong) TableDataBillViewController  *tableDataBillView;
@property (nonatomic,strong) TableSingleBillViewController  *tableSingleBillView;

@property (nonatomic,strong) NSMutableArray *sectionDataIndex;
@property (nonatomic       ) BOOL           isPrintSubBill;
@property (nonatomic       ) BOOL           isPrintAllSubBill;
@property (nonatomic       ) BOOL           isPrintCheckBill;

-(void)blockPrintReceiptView:(int)numKey;
@end
