#import <UIKit/UIKit.h>

@interface UIViewControllerCustom : UIViewController {
    UIView *activeViewItem_;
    UIView *defaultActiveViewItem_;
    UIScrollView *scrollBaseView_;
}

- (void)registerForKeyboardNotifications;
- (void)unregisterForKeyboardNotifications;

@end
