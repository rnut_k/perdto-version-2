//
//  PrintReceiptViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 6/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#define kCellTitle     (@"cellTitle")
#define kCellOrderMenu (@"cellOrderMenu")
#define kCellTotal     (@"cellTotal")
#define kCellTitleBill (@"cellTitleBill")

#import "PrintReceiptViewController.h"
#import "CellTableViewAll.h"
#import "TableDataBillViewController.h"
#import "PrintQuenceTableViewController.h"
#import "CustomNavigationController.h"
#import "AppDelegate.h"
#import "CashTableStatus.h"

#import "UIImageView+AFNetworking.h"
#import "UIViewController+ECSlidingViewController.h"
#import "WYPopoverController.h"
#import "GlobalBill.h"
#import "Member.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Constants.h"
#import "Check.h"
#import "UIColor+HTColor.h"
#import "NSString+GetString.h"
#import "InvoiceDB.h"
#import "LoadProcess.h"
#import "KVNViewController.h"
#import "DeviceData.h"

@interface PrintReceiptViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSArray *listDataOrder;
    GlobalBill *globalBill;
    DeviceData *deviceDate;
    
    int heightmTableInvoice;
    
    NSMutableDictionary *dataTableTile;
    NSMutableArray *dataTableList;
    NSMutableArray *dataSection;
    BOOL isAddDataPrint;
    
    NSMutableDictionary *dataPrintTemp;
    LoadProcess *loadProgress;
    
    UIImageView *imageLogoPrint;
    
    NSDictionary *infoResturant;
}
@property (nonatomic,strong)Member *member;
@end

@implementation PrintReceiptViewController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
    }
    return _member;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    globalBill = [GlobalBill sharedInstance];
    deviceDate = [DeviceData getInstance];
    
    heightmTableInvoice = 0;
    listDataOrder = @[@"1",@"1",@"1",@"1",@"1"];
    dataTableTile = [NSMutableDictionary dictionary];
    [self.mNameStore setText:self.member.titleRes];
    self.dataListSingBill = [NSDictionary dictionaryWithObject:[self.tableDataBillView.selfSuper dataListSingBill]];
    NSDictionary *usageInfo = [self.tableDataBillView.selfSuper dataListSingBill][@"usage_info"];
    if ([usageInfo count] != 0) {
        NSArray *arr = [usageInfo allKeys];
        NSDictionary *dataIn = usageInfo[arr[0]][0];
        
        NSString *date_ = [Time covnetTimeDMY1:dataIn[@"open_datetime"]];
        [self.mDate setText:date_];
    }
    else{
        NSString *date_ = [Time covnetTimeDMY];
        [self.mDate setText:date_];
    }
    
    infoResturant = [NSDictionary dictionaryWithObject:globalBill.billResturant];
    NSString *logoPrint = [NSString checkNull:infoResturant[@"logo_print"]];
    CGRect rect = self.mTableOrder.frame;
    
    if (![Check checkNull:logoPrint]) {
        imageLogoPrint = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,rect.size.width, 138)];
        [imageLogoPrint setContentMode:UIViewContentModeScaleAspectFit];
        [imageLogoPrint setImageWithURL:[NSURL URLWithString:logoPrint]];
    }
    

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    loadProgress  = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self setHeightTable];
    NSString *key = listDataOrder[0];
    [self captureScreenBlockSync:key andVevnder:nil success:^(BOOL completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self blockPrintReceiptView:0];
        });
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [LoadProcess dismissLoad];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)blockPrintReceiptView:(int)numKey{
    NSDictionary *infoStaff = [NSDictionary dictionaryWithObject:deviceDate.staff];
    NSDictionary *config    = [NSDictionary dictionaryWithString:infoResturant[@"config"]];
    NSDictionary *bill_info = [NSDictionary dictionaryWithObject:self.dataListSingBill[@"bill_info"]];
    NSString *idBill        = [NSString stringWithFormat:@"id_%@",globalBill.billList[@"bill_id"]];
    NSDictionary *billData  = [NSDictionary dictionaryWithObject:bill_info[idBill]];
    
    NSString *address = [NSString checkNull:infoResturant[@"address"]];
    UILabel *addressLab = [self createLabelPrint:address andCenter:YES];
    [addressLab setTag:3];
    
    NSString *number = [NSString checkNull:config[@"vat"][@"number"]];
    number = [NSString stringWithFormat:@"%@   : %@",AMLocalizedString(@"เลขที่ใบกำกับภาษี", nil),number];
    UILabel *numberVat = [self createLabelPrint:number andCenter:YES];
    [numberVat setTag:3];
    
    NSString *datetime = [Time getTHFullFromDate:[Time getTimeandDate]];
    datetime = [NSString stringWithFormat:@"%@   : %@",AMLocalizedString(@"ประจำวันที่", nil),datetime];
    UILabel *date = [self createLabelPrint:datetime andCenter:NO];
    [date setTag:2];
    
    NSString *created_datetime = [NSString checkNull:billData[@"created_datetime"]];
    NSString *time = [Time getTHFullFromDate:created_datetime];
    time = [NSString stringWithFormat:@"%@   : %@",AMLocalizedString(@"เปิดโต๊ะ", nil),time];
    UILabel *dateOpen = [self createLabelPrint:time andCenter:NO];
    [dateOpen setTag:2];
    
    NSString *userID = [NSString checkNull:infoStaff[@"user_id"]];
    userID = [NSString stringWithFormat:@"%@   : #%@",AMLocalizedString(@"แคชเชียร์", nil),userID];
    UILabel *casiherNumber = [self createLabelPrint:userID andCenter:NO];
    [casiherNumber setTag:2];
    
    PrintQuenceTableViewController *printQuenceTable = [[PrintQuenceTableViewController alloc] init];
    [printQuenceTable initNetworkCommunication:^(bool completion) {
        if (completion) {
            
            int indexName = 2;
            NSMutableArray *dataSection1 = [NSMutableArray arrayWithArray:dataTableTile[@"0"]];
            
            [dataSection1 insertObject:self.mTitle     atIndex:0];
            [dataSection1 insertObject:self.mNameStore atIndex:1];
            if (![Check checkNull:imageLogoPrint]) {
                [dataSection1 insertObject:imageLogoPrint    atIndex:0];
                indexName ++;
            }
            
            [dataSection1 insertObject:addressLab   atIndex:indexName++];
            [dataSection1 insertObject:numberVat    atIndex:indexName++];
            [dataSection1 insertObject:date         atIndex:indexName++];
            [dataSection1 insertObject:dateOpen     atIndex:indexName++];
            [dataSection1 insertObject:casiherNumber   atIndex:indexName++];
            
            [dataTableTile setObject:dataSection1 forKey:@"0"];
            [printQuenceTable setDataImagePrint:dataTableTile];
            [printQuenceTable setIsCashierPrint:YES];
            [printQuenceTable setTableImagePrint:self.mTableOrder];
            [printQuenceTable blockVender:0 andS:0 suss:^(bool completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (self.isPrintCheckBill) {
                      [self performSelectorOnMainThread:@selector(receiveEvent) withObject:nil waitUntilDone:YES];
                    }
                    else{
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }
                });
            }];
        }
        else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}
- (void)receiveEvent {
//    
    CashTableStatus *navCon = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashTableStatusMain"];
    [self presentViewController:navCon animated:YES completion:NULL];
}
- (void) captureScreenBlockSync:(NSString *)key andVevnder:(NSMutableDictionary *)dataVenderTemp success:(void (^)(BOOL completion)) block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0 animations:^{
                    dataPrintTemp = [NSMutableDictionary dictionaryMutableWithObject:self.tableDataBillView.dataImagePrint];
                    isAddDataPrint = YES;
                    [self.mTableOrder reloadData];
                } completion:^(BOOL finished) {
                    block(YES);
                }];
            });
    });
}
-(void)setHeightTable{
    heightmTableInvoice = 300;
    heightmTableInvoice += self.tableDataBillView.mTableViewBill.contentSize.height + self.tableDataBillView.mTableViewBill.frame.origin.y;
    CGRect frame = CGRectMake(0, 0,self.tableDataBillView.mTableViewBill.frame.size.width,heightmTableInvoice);
    [self.mTableOrder setFrame:frame];
    [self.layoutHightTableOrder setConstant:heightmTableInvoice];
}
-(void)captureScreen:(void (^)(UIImage *completion)) block{

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGRect frame = CGRectMake(0, 0,screenRect.size.width,heightmTableInvoice);
        CGRect frameWindow = self.view.bounds;
        self.view.bounds = frame;
        
        UIGraphicsBeginImageContextWithOptions(frame.size,NO,[UIScreen mainScreen].scale);
        [self.tableDataBillView.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageWriteToSavedPhotosAlbum(image, nil,nil, nil);
        self.view.bounds = frameWindow;        
        dispatch_sync(dispatch_get_main_queue(), ^{
             block(image);
        });
    });
}
- (void ) imageWithView:(UIView *)view suss:(void (^)(UIImage *completion)) block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        CGSize size = view.frame.size;
        
        size.height += 15;
        int a = 500;//700.0;
        int b = a+160.0;
        
        float actualHeight = size.height;
        float actualWidth = size.width;
        float imgRatio = actualWidth/actualHeight;
        float maxRatio = a/b;
        
        if(imgRatio!=maxRatio){
            if(imgRatio < maxRatio){
                imgRatio = b / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = size.height;
            }
            else{
                imgRatio = a / actualWidth;
                actualHeight = size.height;
                actualWidth = a;
            }
        }
        
        UIGraphicsBeginImageContextWithOptions(view.frame.size, view.opaque, 0.0);
        [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
        [view.layer renderInContext:UIGraphicsGetCurrentContext()];
        
        UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();

        UIGraphicsBeginImageContext(CGSizeMake(actualWidth, actualHeight));
        [img drawInRect:CGRectMake(0,0,actualWidth,actualHeight)];
        UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            block(newImage);
        });
    });
}
#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    int num = (int)[dataPrintTemp count];
    return num;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    NSMutableArray *arrList = dataPrintTemp[[@(section+1) stringValue]];
    int num = (int)[arrList count];
    if (section == 0) {
        if (self.isPrintAllSubBill) {
//            num += 2;
            num = num; // test print text
        }
        else{
            num += 1;
        }
        
    }
    return num;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if (indexPath.row == 1 || indexPath.row == 0){
            return 50.0;
        }
        return 44.0;
    }
    else if (indexPath.section == 1)
        return 156.0;
    
    return 44.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"%ld  :S %ld",(long)indexPath.row,(long)indexPath.section);
    NSString *iden = @"";
   
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        iden = kCellTitleBill;
    }
    else if (indexPath.section == 1){
        iden = kCellTotal;
    }
    else{
        iden = kCellOrderMenu;
    }
    
    CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:iden];
    if (!cell) {
        cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
    }
    NSMutableArray *dataList = dataPrintTemp[[@(indexPath.section+1) stringValue]];
    if ([dataList count] == 0) {
        return cell;
    }
    
    if (indexPath.row == 0 && indexPath.section == 0) {
        cell =  [self setCellTitleBill:cell];
    }
    else if (indexPath.section == 1){
        NSString *index = @"1";
        if (self.isPrintAllSubBill) {
            index = @"2";
        }
        NSMutableArray *dataList1 = dataPrintTemp[index];
        CellTableViewAll *dataTotal = [dataList1 lastObject];
        cell =  [self cellTotal:cell and:dataTotal andDataTotal:nil];
    }
    else if (indexPath.section == 0){
//        int num = (int)indexPath.row-2;  test print text
        int num = (int)indexPath.row;
        CellTableViewAll *dataOrderMenu = dataList[num];
        cell = [self cellOrderMenu:cell and:dataOrderMenu];
        [cell setTag:dataOrderMenu.tag];
    }

     if (indexPath.row == 1 && indexPath.section == 0){
         UITableViewCell *cell1 = [tableView dequeueReusableCellWithIdentifier:kCellTitle];
        [dataTableList addObject:cell1];
    }
    if (isAddDataPrint) {
        NSString *secIdx = [@(indexPath.section) stringValue];
        if (dataTableTile[secIdx] == nil) {
            dataTableList  = [NSMutableArray array];
            [dataTableList addObject:cell];
            [dataTableTile setObject:dataTableList forKey:secIdx];
        }
        else{
            [dataTableList addObject:cell];
        }
    }

    return cell;
}
-(CellTableViewAll *) setCellTitleBill:(CellTableViewAll *)cell{
    
    NSDictionary *billInfo = [self.dataListSingBill[@"bill_info"] copy];
    if ([billInfo count] != 0) {
        NSDictionary *data  = [[billInfo allValues][0]  copy];
        NSString *date = [Time covnetTimeDMY1:data[@"created_datetime"]];
        date = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"วันที่", nil),date];
        [self.mDate setText:date];
        
        NSString *nameTa = @"[ ";
        NSString *billTa = @"[ ";
        if (/* DISABLES CODE */ (NO)){//(isStaffTake || isCashierTake) {
            nameTa = [ NSString stringWithFormat:@" Q %@",data[@"no"]];
        }
        else{
            NSDictionary *dataUsage = [NSDictionary dictionaryWithObject:self.dataListSingBill[@"usage_info"]];
            NSArray *dataListTables = [[NSDictionary dictionaryWithObject:self.dataListSingBill[@"tables_info"]] allValues];
            
            NSArray *listKey = [dataUsage allKeys];
            for (NSString *key in listKey) {
                NSArray *arr = (NSArray *)dataUsage[key];
                if ([arr count] != 0) {
                    NSDictionary *dic = [NSDictionary dictionaryWithObject:arr[0]];
                    NSDictionary *filteredArray = (NSDictionary *)[NSDictionary searchDictionary:dic[@"table_id"]
                                                                                        andvalue:@"(id = %@)"
                                                                                         andData:dataListTables];
                    nameTa = [NSString stringWithFormat:@"%@ %@ ,",nameTa, filteredArray[@"label"]];
                    billTa = [NSString stringWithFormat:@"%@ %@ ,",billTa, dic[@"bill_id"]];
                }
                
            }
            nameTa = [nameTa substringToIndex:[nameTa length]-1];
            nameTa = [nameTa stringByAppendingString:@" ]"];
            
            billTa = [billTa substringToIndex:[billTa length]-1];
            billTa = [billTa stringByAppendingString:@" ]"];
        }
        [cell.mCountBill setText:[NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"บิล", nil),billTa]];
        [cell.mNameTable setText:[NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"โต๊ะ", nil),nameTa]];
    }
    return cell;
}
-(CellTableViewAll *)cellTotal:(CellTableViewAll *)cell and:(CellTableViewAll *)dataTotal andDataTotal:(NSDictionary *)_total{
    
    NSDictionary *dicres = [NSDictionary dictionaryWithObject:globalBill.billResturant];
    NSDictionary *vat1   = [NSDictionary dictionaryWithString:dicres[@"config"]];
    NSDictionary *vat2   = [NSDictionary dictionaryWithObject:vat1[@"vat"]];
    NSString *percent    = [NSString checkNull:vat2[@"percent"]];
    
    NSString *discount = @"";
    NSString *total    = @"";
    NSString *vat      = @"";
    NSString *diff     = @"";
    
    if (dataTotal.mTotalFood.tag != 0) {
        NSString *keyBill = [@(dataTotal.mTotalFood.tag) stringValue];
        NSString *value = [NSString stringWithFormat:@"subbill_id_%@",keyBill];
        NSDictionary *dic = self.dataSubbill[value];
        if ([dic  count] != 0) {
            diff     = [NSString conventCurrency:@"0"];
            discount = [NSString conventCurrency:@"0"];
            total    = [NSString conventCurrency:dic[@"total"]];
            vat      = [NSString conventCurrency:dic[@"vat"]];
        }
    }
    else{
        discount = dataTotal.mDiscountFood.text;
        total    = dataTotal.mTotalAllFood.text;
        vat      = dataTotal.mNumVat.text;
        diff     = @"0.00/0.00";
    }
    
    if ([Check checkNull:vat]) {
        vat = @"0.00";
    }
    
    if (self.isPrintCheckBill) {
        diff = [NSString stringWithFormat:@"%@ / %@",self.amountReceived,self.amountRemaininMoney];
    }
    
    cell.mTotalAllFood.text = total;
    cell.mDiscountFood.text = discount;
    cell.mDifference.text   = diff;
    [cell.mTextVat setText:[NSString stringWithFormat:@"Vat %@ %@",percent,@"%"]];
    cell.mNumVat.text       = vat;
    
    return cell;
}
-(CellTableViewAll *)cellOrderMenu:(CellTableViewAll *)cell and:(CellTableViewAll *)dataTotal{
    
    if (dataTotal.tag == 44) {
        return dataTotal;
    }
    else{
        NSString *statusEmblem  = @"●";
        NSMutableAttributedString *nameOrder = [dataTotal.mLabel.attributedText mutableCopy];
        [[nameOrder mutableString] replaceOccurrencesOfString:statusEmblem withString:@""
                                                      options:NSCaseInsensitiveSearch
                                                        range:NSMakeRange(0,nameOrder.string.length)];
        
        
        dataTotal.mLabel.attributedText = nameOrder;
        cell.mNameOrder.attributedText  = dataTotal.mLabel.attributedText;
        cell.mCountOrder.attributedText = dataTotal.mCountOrder.attributedText;
        cell.mTotalFood.attributedText  = dataTotal.mCount.attributedText;
    }
    return cell;
}
-(BOOL)checkDeliverStatus:(NSString *)status{
    if ([status isEqual:@"4"]) {
        return YES;
    }
    return NO;
}
-(UILabel *)createLabelPrint:(NSString *)title andCenter:(BOOL)bl {
    CGRect rect = self.mTableOrder.frame;
    UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(10,0,rect.size.width,50)];
    [lab setText:title];
    [lab setFont:[UIFont systemFontOfSize:26.0]];
    [lab setTextAlignment:(bl)?NSTextAlignmentCenter:NSTextAlignmentLeft];
    
    return lab;
}
@end
