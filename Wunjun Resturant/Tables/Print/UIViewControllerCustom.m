#import "UIViewControllerCustom.h"

@interface UIViewControllerCustom ()

@end

@implementation UIViewControllerCustom

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeViewItem_ = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeViewItem_ = nil;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keybaordWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];}

- (void)keyboardDidShow:(NSNotification*)aNotification
{
    if(activeViewItem_ == nil){
        activeViewItem_ = defaultActiveViewItem_;
    }
    if(scrollBaseView_ == nil || activeViewItem_ == nil){
        return ;
    }
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey]
                     CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollBaseView_.contentInset = contentInsets;
    scrollBaseView_.scrollIndicatorInsets = contentInsets;

    CGRect aRect = self.view.frame;
    aRect.origin.y += scrollBaseView_.contentOffset.y;
    aRect.size.height -= kbSize.height + activeViewItem_.frame.size.height;
    if (!CGRectContainsPoint(aRect, activeViewItem_.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0,
                                          activeViewItem_.frame.origin.y - (aRect.origin.y + aRect.size.height));
        scrollPoint.y += scrollBaseView_.contentOffset.y;
        [scrollBaseView_ setContentOffset:scrollPoint animated:YES];
    }
}

- (void)keybaordWillHide:(NSNotification*)aNotification
{
    scrollBaseView_.contentInset = UIEdgeInsetsZero;
    scrollBaseView_.scrollIndicatorInsets = UIEdgeInsetsZero;
}

@end
