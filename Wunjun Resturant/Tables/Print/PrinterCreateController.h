//
//  PrinterCreateController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrinterSetingViewController.h"
#import "CNPPopupController.h"

@interface PrinterCreateController : UIViewController<UITextFieldDelegate,CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mPort;
@property (weak, nonatomic) IBOutlet UITextField *mTextPaper;
@property (weak, nonatomic) IBOutlet UITextField *mNamePrinter;
@property (weak, nonatomic) IBOutlet UITextField *mIPAddress;
@property (weak, nonatomic) IBOutlet UITextField *mBrandPrinter;

@property (nonatomic ,strong) PrinterSetingViewController *printerSetingView;

- (IBAction)btnAddPrinter:(id)sender;
- (IBAction)btnTestPrint:(id)sender;


- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title ;

@end
