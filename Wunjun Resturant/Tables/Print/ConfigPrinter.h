//
//  ConfigPrinter.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/9/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#ifndef Wunjun_Resturant_ConfigPrinter_h
#define Wunjun_Resturant_ConfigPrinter_h

const char BYTE[]                                  = {0x27,0x64};
const char FEED_LINE[]                             = {10};
const char SELECT_FONT_A[]                         = {27, 33, 0};
const char SET_BAR_CODE_HEIGHT[]                   = {29, 104, 100};
const char PRINT_BAR_CODE_1[]                      = {29, 107, 2};
const char SEND_NULL_BYTE[]                        = {0x00};
const char SELECT_PRINT_SHEET[]                    = {0x1B, 0x63, 0x30, 0x02};
const char FEED_PAPER_AND_CUT[]                    = {0x1D, 0x56, 66, 0x00};
const char SELECT_CYRILLIC_CHARACTER_CODE_TABLE[]  = {0x1B, 0x74, 0x11};
const char SET_LINE_SPACING_24[]                   = {0x1B, 0x33, 24};
const char SET_LINE_SPACING_30[]                   = {0x1B, 0x33, 30};
const char TRANSMIT_DLE_PRINTER_STATUS[]           = {0x10, 0x04, 0x01};
const char TRANSMIT_DLE_OFFLINE_PRINTER_STATUS[]   = {0x10, 0x04, 0x02};
const char TRANSMIT_DLE_ERROR_STATUS[]             = {0x10, 0x04, 0x03};
const char TRANSMIT_DLE_ROLL_PAPER_SENSOR_STATUS[] = {0x10, 0x04, 0x04};
const char ESC_CHAR                                = {0x1B};
const char GS                                      = {0x1D};
const char LINE_FEED[]                             = {0x0A};
const char CUT_PAPER[]                             = {GS, 0x56, 0x00};
const char CUT_PAPER_FEED[]                        = {0x0A,0x0A, 0x0A, 0x0A,0x1D,0x56, 0x00};
const char INIT_PRINTER[]                          = {ESC_CHAR, 0x40};
const char SET_LINE_SPACE_24[]                     = {ESC_CHAR, 0x33, 24};
const char SELECT_BIT_IMAGE_MODE[]                 = {0x1B, 0x2A, 33, -128, 0};
const char FONT_A[]                                = {0x1B,50};
const char FONT_SIZE_9[]                           = {0x1B,0x4D,9};
const char FONT_SI[]                               = {-95};

#endif
