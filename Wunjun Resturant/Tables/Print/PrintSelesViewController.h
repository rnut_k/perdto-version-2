//
//  PrintSelesViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 9/8/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrintSelesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic ,weak) IBOutlet UITableView *mTablePrintSeles;
@property (weak, nonatomic  ) IBOutlet NSLayoutConstraint *layoutHightTableSales;

@property (nonatomic,strong) NSMutableDictionary *dataImagePrint;
@property (nonatomic,strong) NSMutableArray *dataPrintList;
@property (nonatomic,strong) NSMutableArray *dataCumulativeSales;
@property (nonatomic,strong) NSDictionary  *dataPromotion;

@property (nonatomic ,strong)  NSDictionary *dataAllZone;


//// cumulative sales
//@property (nonatomic ,assign)  NSInteger priceAccumelated;
//@property (nonatomic ,assign)  NSInteger priceSales;
//
//// sales summary
//@property (nonatomic ,assign)  NSInteger countGood;
//@property (nonatomic ,assign)  NSInteger countBill;
//@property (nonatomic ,assign)  NSInteger countCustomer;
//@property (nonatomic ,assign)  NSInteger countMan;
//@property (nonatomic ,assign)  NSInteger countWoman;
//@property (nonatomic ,assign)  NSInteger countTotalCirculation;
//@property (nonatomic ,assign)  NSInteger countTAX;
//@property (nonatomic ,assign)  NSInteger countTatalSold;
//
//// summary fo financial
//@property (nonatomic ,assign)  NSInteger countCash;
//@property (nonatomic ,assign)  NSInteger priceCash;
//@property (nonatomic ,assign)  NSInteger countCredit;
//@property (nonatomic ,assign)  NSInteger priceCredit;

@end
