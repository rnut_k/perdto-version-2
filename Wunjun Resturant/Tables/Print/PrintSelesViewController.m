//
//  PrintSelesViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 9/8/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "PrintSelesViewController.h"
#import "PrintQuenceTableViewController.h"

#import "UIImageView+AFNetworking.h"
#import "CustomNavigationController.h"
#import "WYPopoverController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "UIColor+HTColor.h"
#import "Check.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "Member.h"
#import "Constants.h"
#import "Time.h"
#import "SearchPromotion.h"
#import "NSString+GetString.h"
#import "LoadProcess.h"
#import "CellTableViewAll.h"
#import "GlobalBill.h"


typedef NS_ENUM(NSUInteger, CellModel){
    HCellImagePrintSeles = 200,
    HCellTitlePrintSeles = 50,
    HCellDateSeles       = 88,
    HCellDataSeles       = 50,
    HCellSummarySeles    = 50,
    HCellCross           = 55,
    HCellEndReportSeles  = 50,
    HCellOneReportSeles  = 50
};

typedef NS_ENUM(NSUInteger, IdenCell){
    CellImagePrintSeles = 1,
    CellTitlePrintSeles ,
    CellDateSeles       ,
    CellDataSeles       ,
    CellSummarySeles    ,
    ellCross            ,
    CellEndReportSeles  ,
    CellOneReportSeles
};

extern NSString * NSStringGetIdentifireCellTable(IdenCell state);

static NSString *cellNames[] = {@"CellImagePrintSeles",  // 0
                                @"CellTitlePrintSeles",  // 1
                                @"CellDateSeles",        // 2
                                @"CellDataSeles",        // 3
                                @"CellSummarySeles",     // 4
                                @"CellCross",            // 5
                                @"CellEndReportSeles",   // 6
                                @"CellOneReportSeles"    // 7
};

static NSArray *textTitleBill;
static NSArray *listTitleBill;
static NSArray *listTitleSummary;

@interface PrintSelesViewController (){
    GlobalBill *globalBill;
    LoadProcess *loadProcess;
    
    NSInteger rowIndex;
    NSInteger dataIndex;
    NSMutableArray *dataSeles;
    
    NSMutableArray *infoSection; // เก็บข้อมูล section ที่จะจะโชว์
    NSDictionary *infoResturant;
    NSDictionary *infoStaff;
    DeviceData *deviceData;
    
// call printer
    int heightmTableInvoice;
    NSMutableDictionary *dataAddprint;
    NSMutableDictionary *dataPrintTemp;
    NSMutableArray *dataTableList;
    BOOL isAddDataPrint;
}
@property (nonatomic,strong) Member *member;
@end

@implementation PrintSelesViewController
+ (void)initialize
{
    // do not run for derived classes
    if (self != [PrintSelesViewController class])
        return;
    listTitleBill = @[@"logo", // index
                    @"titleBill",
                    @"date",
                    @"title",
                    @"num",
                    @"line",
                    @"lineEnd",
                    @"order",
                    @"titleOrder"
                    ];
    
    textTitleBill = @[@"รายงานสรุปยอดขาย",
                    @"ชื่อร้าน",
                    @"รายงายยอดขายตามประเภท",
                    @"สรุปยอดขายสะสม",
                    @"สรุปยอดขาย",
                    @"สรุปยอดการรับเงิน",
                    @"เงินสด",
                    @"เครดิต",
                    @"พิมพ์โดย",
                    @"ขอส่วนลด",
                    @"คูปอง"
                    ];
    
    listTitleSummary = @[@"ยอดขายสะสมสุทธิ",
                      @"ยอดขายเดือน ",
                      @"จำนวนสินค้า",
                      @"จำนวนบิล",
                      @"จำนวนลูกค้า",
                      @"ชาย",
                      @"หญิง",
                      @"ยอดขายรวม",
                      @"ภาษี",
                      @"ยอดขายรวมทุกอย่าง",
                      @"จำนวนบิลยกเลิก",
                      @"จำนวนบิลแก้ไข"
                      ];
}


-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    loadProcess  = [LoadProcess sharedInstance];
    [loadProcess.loadView showWithStatus];
    
    heightmTableInvoice = 0;
    dataAddprint = [NSMutableDictionary dictionary];
    infoSection =  [NSMutableArray array];
    deviceData = [DeviceData getInstance];
    globalBill = [GlobalBill sharedInstance];
    
    rowIndex = 0;
    dataIndex = 0;
    
    [self setDataPrinter];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self setHeightTable];

    [self captureScreenBlockSync:nil andVevnder:nil success:^(BOOL completion) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self blockPrintReceiptView:0];
        });
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    [LoadProcess dismissLoad];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setHeightTable{
    heightmTableInvoice = 300;
    heightmTableInvoice += self.mTablePrintSeles.contentSize.height + self.mTablePrintSeles.frame.origin.y;
    CGRect frame = CGRectMake(0, 0,self.mTablePrintSeles.frame.size.width,heightmTableInvoice);
    [self.mTablePrintSeles setFrame:frame];
    [self.layoutHightTableSales setConstant:heightmTableInvoice];
}
-(void) setDataPrinter{
    dataSeles = [NSMutableArray array];
    infoResturant = [NSDictionary dictionaryWithObject:globalBill.billResturant];
    infoStaff = [NSDictionary dictionaryWithObject:deviceData.staff];
    
    NSMutableArray *temp = [NSMutableArray array];
    NSString *logoPrint = [NSString checkNull:infoResturant[@"logo_print"]];
    if (![Check checkNull:logoPrint]) {
        [temp addObject:@[listTitleBill[0],logoPrint]];
    }
    
    NSString *title = [NSString checkNull:infoResturant[@"title"]];
    [temp addObject:@[@"titleBill",[NSNumber numberWithInt:0]]];
    [temp addObject:@[@"titleBill",[NSNumber numberWithInt:1],title]];
    NSString *date = [Time covnetTimeDMY];
    NSString *date1 = [NSString stringWithFormat:@" %@ : %@",date,[Time getTime]];
    [temp addObject:@[listTitleBill[2],date,date1]];
    [temp addObject:@[@"title",[NSNumber numberWithInt:8]]];
    [temp addObject:@[@"line"]];
    [infoSection addObject:temp];
    
    temp = [NSMutableArray array];
    [temp addObject:@[@"title",[NSNumber numberWithInt:2]]];
    [temp addObjectsFromArray:self.dataPrintList];
    [temp addObject:@[@"line"]];
    [infoSection addObject:temp];
    
    temp = [NSMutableArray array];
    [temp addObjectsFromArray:self.dataCumulativeSales];
    [infoSection addObject:temp];
}

-(void)blockPrintReceiptView:(int)numKey{
    PrintQuenceTableViewController *printQuenceTable = [[PrintQuenceTableViewController alloc] init];
    [printQuenceTable initNetworkCommunication:^(bool completion) {
        if (completion) {
            [printQuenceTable setDataImagePrint:dataAddprint];
            [printQuenceTable setIsSalesPrint:YES];
            [printQuenceTable blockVender:0 andS:0 suss:^(bool completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }];
        }
        else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}
- (void) captureScreenBlockSync:(NSString *)key andVevnder:(NSMutableDictionary *)dataVenderTemp success:(void (^)(BOOL completion)) block
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0 animations:^{
                isAddDataPrint = YES;
                [self.mTablePrintSeles reloadData];
            } completion:^(BOOL finished) {
                isAddDataPrint = NO;
                block(YES);
            }];
        });
    });
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [infoSection count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = indexPath.row;
    NSInteger sec = indexPath.section;
    
    NSArray *dataSec = [NSArray arrayWithArray:infoSection[sec]];
    switch (sec) {
        case 0:{
            NSArray *dataRow = [NSArray arrayWithArray:dataSec[row]];
            NSUInteger index = [listTitleBill indexOfObject:dataRow[0]];
            CGFloat hight    = [self getHightCellTable:(int)index+1];
            return hight;
        }
            break;
        case 1:
            return HCellDataSeles;
            break;
        case 2:
            return HCellSummarySeles;
            break;
        default:
             return tableView.rowHeight;
            break;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *dataSec = [NSArray arrayWithArray:infoSection[section]];
    return [dataSec count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {        
        NSInteger row      = indexPath.row;
        NSInteger sec      = indexPath.section;
        NSArray *dataOrder = [NSArray arrayWithArray:infoSection[sec][row]];
        NSString *text     = [NSString checkNull:[dataOrder firstObject]];
        NSUInteger index   = [listTitleBill indexOfObject:text];
        NSString *idenCell = [self getIdentifire:(int)index];
        
        CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:idenCell];
        if (!cell)
            cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenCell];
        
        switch (index) {
            case 0:{  // @"logo"
                [cell.mImageBackgroud setImageWithURL:[NSURL URLWithString:[dataOrder lastObject]]];
                tableView.rowHeight = HCellImagePrintSeles;
            }break;
            case 2:{  // @"date"
                NSString *daily = [NSString checkNull:dataOrder[1]];
                NSString *date  = [NSString checkNull:dataOrder[2]];
                [cell.mDaily setText:[NSString stringWithFormat:cell.mDaily.text,daily]];
                [cell.mDate setText:[NSString stringWithFormat:cell.mDate.text,date]];
                tableView.rowHeight = HCellDateSeles;
            } break;
            case 1:{  //  @"ซื่อร้าน"
                NSInteger index = [dataOrder[1] integerValue];
                NSString *name = @"";
                if (index == 1) {   // ชื่อร้าน
                    name = [NSString stringWithFormat:@"%@ %@",textTitleBill[1],[NSString checkNull:dataOrder[2]]];
                }
                else if (index == 2) {   // พิมพ์โดย
                    name = [NSString stringWithString:[NSString checkNull:dataOrder[2]]];
                }
                else{ // หัวข้อ
                    name = [NSString checkNull:[textTitleBill objectAtIndex:index]];
                }
                [cell.mNameTable setText:name];
                 cell.mNameTable.font = [UIFont boldSystemFontOfSize:26];
                [cell setTag:999];
                
            }break;
            case 3:{  //  @"title"
                NSInteger index = [dataOrder[1] integerValue];
                NSString *title = @"";
                if (index == 8)
                    title = [NSString stringWithFormat:@" %@ : %@ %@",textTitleBill[index],infoStaff[@"username"],infoStaff[@"lastname"]];
                else
                    title = [NSString checkNull:textTitleBill[index]];
                
                [cell.mNameOrder setText:title];
                cell.mNameOrder.font = [UIFont boldSystemFontOfSize:26];
                [cell setTag:999];
                
            }break;
            case 4:{    // @"num"
                NSInteger index = [dataOrder[1] integerValue];
                NSInteger price = [dataOrder[2] integerValue];
                NSString *title = [NSString checkNull:[listTitleSummary objectAtIndex:index]];
                if (index == 1) {
                    NSArray *myComponents = [[Time getTimeALL] componentsSeparatedByString:@"-"];
                    NSString *monthName   = [Time getMountTH:[myComponents[1] intValue]];
                    title = [listTitleSummary[index] stringByAppendingString:monthName];
                }
                [cell.mNameOrder setText:title];
                BOOL bl = [self isNotConvert:index];
                NSString  *priceCon = (bl)?[NSString  conventCurrency:[@(price) stringValue]]:[@(price) stringValue];
                [cell.mTotalFood setText:priceCon];
            }break;
            case 5:   // @"line"
            case 6:   // @"lineEnd"
                  break;
            case 7:{    // @"order"
                NSDictionary *order = [NSDictionary dictionaryWithObject:[dataOrder lastObject]];
                cell = [self setDetailTableOrder:cell andDate:order];
            } break;
            case 8:{   // title order
                id dataNum = dataOrder[1];
                if ([dataNum isKindOfClass:[NSArray class]]) {  // 6,7
                    NSInteger index = [dataNum[0] integerValue];
                    NSInteger count = [dataNum[1] integerValue];
                    NSInteger price = [dataNum[2] integerValue];
                    NSString *title = [NSString checkNull:[textTitleBill objectAtIndex:index]];
                    [cell.mNameOrder setText:title];
                    [cell.mTotalFood setText:[NSString  conventCurrency:[@(price) stringValue]]];
                    [cell.mCount setText:[NSString stringWithFormat:@" ( %ld )",(long)count]];
                }
                else{
                    NSDictionary *order = [NSDictionary dictionaryWithObject:dataOrder[1]];
                    [cell.mNameOrder setText:order[@"title"]];
                    [cell.mTotalFood setText:[NSString  conventCurrency:order[@"total"]]];
                    [cell.mCount setText:[NSString stringWithFormat:@" ( %@ )",order[@"count"]]];
                }
                
                cell.mNameOrder.font = [UIFont boldSystemFontOfSize:26];
                cell.mTotalFood.font = [UIFont boldSystemFontOfSize:26];
                cell.mCount.font = [UIFont boldSystemFontOfSize:26];
                [cell setTag:999];
                
            }
            default:
                break;
        }
        
        if (isAddDataPrint) {
             NSString *secIdx = [@(indexPath.section) stringValue];
            if (dataAddprint[secIdx] == nil) {
                dataTableList  = [NSMutableArray array];
                [dataTableList addObject:cell];
                [dataAddprint setObject:dataTableList forKey:secIdx];
            }
            else{
                [dataTableList addObject:cell];
            }
        }
        
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"exception  cellForRowAtIndexPath :%@",exception);
    }
}

-(CellTableViewAll *) setDetailTableOrder:(CellTableViewAll *)cell  andDate:(NSDictionary *)dataOrder {
    NSString *sales ;
    NSString *nameFood;
    NSString  *price = @"";
    NSDictionary *addFoodBill = dataOrder[@"dataorder"];
    BOOL blCon = true;
    if ([addFoodBill count] != 0) {
        int cu = [addFoodBill[@"price"]  floatValue] * [addFoodBill[@"n"] intValue];
        sales = [@(cu) stringValue];
        
        NSString *typeDrink = [self getTypeDrink:dataOrder[@"dataorder"][@"drinkgirl_type"]];
        if (typeDrink == nil) {
            nameFood = [NSString stringWithFormat:@"%@",dataOrder[@"data"][@"name"]];
            price = [NSString stringWithFormat:@"( %@ * %@ )",addFoodBill[@"n"],addFoodBill[@"price"]];
        }
        else{
            nameFood = [NSString stringWithFormat:@"%@",dataOrder[@"data"][@"name"]];
            price = [NSString stringWithFormat:@"( %@ )",typeDrink];
        }
    }
    else if (dataOrder[@"promotion_id"] != nil && ![dataOrder[@"promotion_id"] isEqual:@"0"]){
        NSString *idPro = [NSString stringWithFormat:@"id_%@",dataOrder[@"promotion_id"]];
        int cu = [dataOrder[@"price"]  floatValue] * [dataOrder[@"n"] intValue];
        sales = [@(cu) stringValue];
        
        NSDictionary *tempPro = self.dataPromotion[idPro];
        if ([tempPro count] != 0) {
            nameFood = [NSString stringWithFormat:@"%@",tempPro[@"name"]];
            price = [NSString stringWithFormat:@"( %@ * %@ )",dataOrder[@"n"],dataOrder[@"price"]];
        }
    }
    else if (dataOrder[@"promotion_id"] != nil && [dataOrder[@"promotion_id"] isEqual:@"0"]){
        float total  = [dataOrder[@"total"] floatValue];
        float price1 = [dataOrder[@"price"] floatValue];
        int  typePro  = [dataOrder[@"type"] intValue];
        if (typePro == 4){
            nameFood = [NSString stringWithFormat:@"%@ %.02f",AMLocalizedString(@"คูปอง", nil),fabsf(price1)];
            sales = [NSString stringWithFormat:@"%.02f",fabsf(total)];
            price = [NSString stringWithFormat:@"( %@ * %.02f )",dataOrder[@"n"],fabsf(price1)];
        }
        else {
        nameFood = [NSString stringWithFormat:@"%@",AMLocalizedString(@"ขอส่วนลด", nil)];
        price = [NSString stringWithFormat:@"( %@ )",dataOrder[@"n"]];
        sales = [NSString stringWithFormat:@"%@",dataOrder[@"total"]];
        }
    }
    
    [cell.mNameOrder setText:[NSString stringWithFormat:@" %@",nameFood]];
    [cell.mTotalFood setText:(blCon)?[NSString  conventCurrency:sales]:sales];
    [cell.mCount setText:price];
    
    return cell;
}
-(NSString *)getTypeDrink:(NSString *)ind{
    if ([ind isEqual:@"1"]) {
        return kDrinkOne;
    }
    else if ([ind isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else if ([ind isEqual:@"3"]) {
        return kDrinkThree;
    }
    else{
        return nil;
    }
}
-(CGFloat)getHightCellTable:(int)num{
    
    switch (num) {
        case 1:
            return HCellImagePrintSeles;
            break;
        case 2:
            return HCellTitlePrintSeles;
            break;
        case 3:
            return HCellDateSeles;
            break;
        case 4:
            return HCellDataSeles;
            break;
        case 5:
            return HCellSummarySeles;
            break;
        case 6:
            return HCellCross;
            break;
        case 7:
            return HCellEndReportSeles;
            break;
        case 8:
            return HCellOneReportSeles;
            break;
        default:
                return 0;
            break;
    }
}

-(NSString *) getIdentifire:(int)index{
    NSString *idenCell =@"";
    switch (index) {
        case 0:{
            idenCell = NSStringGetIdentifireCellTable(0);  // @"logo"
        } break;
        case 1:{
            idenCell = NSStringGetIdentifireCellTable(1);  //  @"ซื่อร้าน"
        } break;
        case 2:{
            idenCell = NSStringGetIdentifireCellTable(2);  // @"date"
        } break;
        case 3:{
            idenCell = NSStringGetIdentifireCellTable(7);  //  @"title"
        } break;
        case 4:{
            idenCell = NSStringGetIdentifireCellTable(4);  // @"num"
        } break;
        case 5:{
            idenCell = NSStringGetIdentifireCellTable(5);  // @"line"
        } break;
        case 6:{
            idenCell = NSStringGetIdentifireCellTable(6);  // @"lineEnd"
        } break;
        case 7:
        case 8:{
            idenCell = NSStringGetIdentifireCellTable(3);   // @"order"
        } break;
        default:
            break;
    }
    return idenCell;
}

NSString * NSStringGetIdentifireCellTable(IdenCell state) {
    return cellNames[state];
}
-(BOOL) isNotConvert:(NSInteger)num{
    switch (num) {
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 10:
        case 11:
            return false;
        case 0:
        case 1:
        case 7:
        case 8:
        case 9:
            return true;
        default:
             return false;
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
