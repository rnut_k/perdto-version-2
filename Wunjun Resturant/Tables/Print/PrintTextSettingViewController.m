//
//  PrintTextSettingViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 11/7/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "PrintTextSettingViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ePOS-Print.h"
#import "ShowMsg.h"

#define SEND_TIMEOUT    10 * 1000
#define SIZEWIDTH_MAX   8

@interface PrintTextSettingViewController ()
- (void)printText;
- (int)getBuilderFont;
- (int)getBuilderAlign;
- (long)getBuilderLineSpace;
- (int)getBuilderLanguage;
- (int)getBuilderStyleBold;
- (int)getBuilderStyleUnderline;
- (long)getBuilderXPosition;
- (NSString *)getBuilderText;
- (long)getBuilderFeedUnit;
@end

@implementation PrintTextSettingViewController
@synthesize scrollView_;
@synthesize textData_;
@synthesize textLineSpace_;
@synthesize buttonSizeWidth_;
@synthesize buttonSizeHeight_;
@synthesize switchStyleBold_;
@synthesize switchStyleUnderline_;
@synthesize textXPosition_;
@synthesize textFeedUnit_;
@synthesize itemView_;
@synthesize buttonFont_;
@synthesize buttonLanguage_;
@synthesize buttonAlign_;

- (id)initWithPrinter:(EposPrint*)printer
          printername:(NSString*)printername
             language:(int)language
{
    self = [super init];
    if (self) {
//        printer_ = [printer retain];
//        printername_ = [printername retain];
        language_ = language;
        activeViewItem = nil;
    }
    return self;
}

//- (void)dealloc
//{
////    [scrollView_ release];
////    [itemView_ release];
////    [textData_ release];
////    [buttonFont_ release];
////    [buttonAlign_ release];
////    [buttonLanguage_ release];
////    [textLineSpace_ release];
////    [buttonSizeWidth_ release];
////    [buttonSizeHeight_ release];
////    [switchStyleBold_ release];
////    [switchStyleUnderline_ release];
////    [textXPosition_ release];
////    [textFeedUnit_ release];
////    
////    [printer_ release];
////    [printername_ release];
////    [fontList_ release];
////    [alignList_ release];
////    [languageList_ release];
////    [sizeWidthList_ release];
////    [sizeHeightList_ release];
//    [super dealloc];
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set multiline edit style
    textData_.layer.borderWidth = 1;
    textData_.layer.borderColor = [[UIColor grayColor] CGColor];
    textData_.layer.cornerRadius = 8;
    
    //set multiline edit keyboard accessory
//    UIToolbar *doneToolbar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)] autorelease];
//    doneToolbar.barStyle = UIBarStyleBlackTranslucent;
//    [doneToolbar sizeToFit];
//    UIBarButtonItem *space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil] autorelease];
//    UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(eventDidPushDone:)] autorelease];
//    
//    NSMutableArray *items = [NSMutableArray arrayWithObjects:space, doneButton, nil];
//    [doneToolbar setItems:items animated:YES];
//    textData_.inputAccessoryView = doneToolbar;
    
//    //init font list
//    fontList_ = [[PickerTableView alloc] init];
//    items = [[[NSMutableArray alloc] init] autorelease];
//    [items addObject:NSLocalizedString(@"font_a", @"")];
//    [items addObject:NSLocalizedString(@"font_b", @"")];
//    [items addObject:NSLocalizedString(@"font_c", @"")];
//    [items addObject:NSLocalizedString(@"font_d", @"")];
//    [items addObject:NSLocalizedString(@"font_e", @"")];
//    [buttonFont_ setTitle:[items objectAtIndex:0] forState:UIControlStateNormal];
//    [fontList_ setItemList:items];
//    fontList_.delegate = self;
    
    //init align list
//    alignList_ = [[PickerTableView alloc] init];
//    items = [[[NSMutableArray alloc] init] autorelease];
//    [items addObject:NSLocalizedString(@"align_left", @"")];
//    [items addObject:NSLocalizedString(@"align_center", @"")];
//    [items addObject:NSLocalizedString(@"align_right", @"")];
//    [buttonAlign_ setTitle:[items objectAtIndex:0] forState:UIControlStateNormal];
//    [alignList_ setItemList:items];
//    alignList_.delegate = self;
//    
//    //init language list
//    languageList_ = [[PickerTableView alloc] init];
//    items = [[[NSMutableArray alloc] init] autorelease];
//    [items addObject:NSLocalizedString(@"language_ank", @"")];
//    [items addObject:NSLocalizedString(@"language_japanese", @"")];
//    [items addObject:NSLocalizedString(@"language_simplified_chinese", @"")];
//    [items addObject:NSLocalizedString(@"language_traditional_chinese", @"")];
//    [items addObject:NSLocalizedString(@"language_korean", @"")];
//    [items addObject:NSLocalizedString(@"language_thai", @"")];
//    [items addObject:NSLocalizedString(@"language_vietnamese", @"")];
//    [buttonLanguage_ setTitle:[items objectAtIndex:0] forState:UIControlStateNormal];
//    [languageList_ setItemList:items];
//    languageList_.delegate = self;
    
    //init size list
//    sizeWidthList_ = [[PickerTableView alloc] init];
//    items = [[[NSMutableArray alloc] init] autorelease];
//    for(int i = 1; i <= SIZEWIDTH_MAX; i++){
//        [items addObject:[NSString stringWithFormat:@"%d", i]];
//    }
//    [buttonSizeWidth_ setTitle:[items objectAtIndex:0] forState:UIControlStateNormal];
//    [sizeWidthList_ setItemList:items];
//    sizeWidthList_.delegate = self;
//    
//    sizeHeightList_ = [[PickerTableView alloc] init];
//    [buttonSizeHeight_ setTitle:[items objectAtIndex:0] forState:UIControlStateNormal];
//    [sizeHeightList_ setItemList:items];
//    sizeHeightList_.delegate = self;
    
    //default setting
    textData_.text = NSLocalizedString(@"text_edit_text", @"");
    textLineSpace_.text = @"30";
    textXPosition_.text = @"0";
    textFeedUnit_.text = @"30";
    switchStyleBold_.on = NO;
    switchStyleUnderline_.on = NO;
    
    //show back button(navigationbar)
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    //auto scroll setting
    scrollBaseView_ = scrollView_;
    defaultActiveViewItem_ = textData_;
    [scrollView_ setContentSize:itemView_.frame.size];
    [self registerForKeyboardNotifications];
}

- (void)viewDidUnload
{
    [self unregisterForKeyboardNotifications];
    [self setScrollView_:nil];
    [self setButtonFont_:nil];
    [self setButtonLanguage_:nil];
    [self setButtonAlign_:nil];
    [self setTextData_:nil];
    [self setTextLineSpace_:nil];
    [self setButtonSizeWidth_:nil];
    [self setButtonSizeHeight_:nil];
    [self setSwitchStyleBold_:nil];
    [self setSwitchStyleUnderline_:nil];
    [self setTextXPosition_:nil];
    [self setTextFeedUnit_:nil];
    [self setItemView_:nil];
    [super viewDidUnload];
}

- (void)eventDidPushDone:(id)sender
{
    [textData_ resignFirstResponder];
}

//- (void)onSelectPickerItem:(NSInteger)position obj:(id)obj {
//    if(obj == fontList_){
//        [buttonFont_ setTitle:[fontList_ getItem:position] forState:UIControlStateNormal];
//    }else if(obj == alignList_){
//        [buttonAlign_ setTitle:[alignList_ getItem:position] forState:UIControlStateNormal];
//    }else if(obj == languageList_){
//        [buttonLanguage_ setTitle:[languageList_ getItem:position] forState:UIControlStateNormal];
//    }else if(obj == sizeWidthList_){
//        [buttonSizeWidth_ setTitle:[sizeWidthList_ getItem:position] forState:UIControlStateNormal];
//    }else if(obj == sizeHeightList_){
//        [buttonSizeHeight_ setTitle:[sizeHeightList_ getItem:position] forState:UIControlStateNormal];
//    }
//}

- (IBAction)eventDidPush:(id)sender
{
    [self printText];
    
//    switch(((UIView*)sender).tag){
//        case 0:
//            [self printText];
//            break;
//        case 1:
//            [fontList_ show];
//            break;
//        case 2:
//            [alignList_ show];
//            break;
//        case 3:
//            [languageList_ show];
//            break;
//        case 4:
//            [sizeWidthList_ show];
//            break;
//        case 5:
//            [sizeHeightList_ show];
//            break;
//        default:
//            break;
//    }
}

- (void)printText
{
    if(textData_.text.length == 0){
        [ShowMsg showError:@"errmsg_notext"];
        return ;
    }
    
    //create builder
    EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:printername_ Lang:language_];
    if(builder == nil){
        return ;
    }
    
    //add command
    int result = [builder addTextFont:[self getBuilderFont]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextFont"];
        return ;
    }
    
    result = [builder addTextAlign:[self getBuilderAlign]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextAlign"];
        return ;
    }
    
    result = [builder addTextLineSpace:[self getBuilderLineSpace]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextLineSpace"];
        return ;
    }
    
    result = [builder addTextLang:[self getBuilderLanguage]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextLang"];
        return ;
    }
    
//    result = [builder addTextSize:[self getBuilderSizeW] Height:[self getBuilderSizeH]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextSize"];
        return ;
    }
    
    result = [builder addTextStyle:EPOS_OC_FALSE Ul:[self getBuilderStyleUnderline] Em:[self getBuilderStyleBold] Color:EPOS_OC_COLOR_1];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextStyle"];
        return ;
    }
    
    result = [builder addTextPosition:[self getBuilderXPosition]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addTextPosition"];
        return ;
    }
    
    result = [builder addText:[self getBuilderText]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addText"];
        return ;
    }
    
    result = [builder addFeedUnit:[self getBuilderFeedUnit]];
    if(result != EPOS_OC_SUCCESS){
        [ShowMsg showExceptionEpos:result method:@"addFeedUnit"];
        return ;
    }
    
    //send builder data
    unsigned long status = 0;
    unsigned long battery = 0;
    result = [printer_ sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery];
    [ShowMsg showStatus:result status:status battery:battery];
    
    //remove builder
    [builder clearCommandBuffer];
}

- (int)getBuilderFont
{
//    switch(fontList_.selectIndex){
    switch(0){
        case 1:
            return EPOS_OC_FONT_B;
        case 2:
            return EPOS_OC_FONT_C;
        case 3:
            return EPOS_OC_FONT_D;
        case 4:
            return EPOS_OC_FONT_E;
        case 0:
        default:
            return EPOS_OC_FONT_A;
    }
}

- (int)getBuilderAlign
{
//    switch(alignList_.selectIndex){
    switch(1){
        case 1:
            return EPOS_OC_ALIGN_CENTER;
        case 2:
            return EPOS_OC_ALIGN_RIGHT;
        case 0:
        default:
            return EPOS_OC_ALIGN_LEFT;
    }
}

- (long)getBuilderLineSpace
{
    return [textLineSpace_.text intValue];
}

- (int)getBuilderLanguage
{
//    switch(languageList_.selectIndex){
        switch(5){
        case 1:
            return EPOS_OC_LANG_JA;
        case 2:
            return EPOS_OC_LANG_ZH_CN;
        case 3:
            return EPOS_OC_LANG_ZH_TW;
        case 4:
            return EPOS_OC_LANG_KO;
        case 5:
            return EPOS_OC_LANG_TH;
        case 6:
            return EPOS_OC_LANG_VI;
        case 0:
        default:
            return EPOS_OC_LANG_EN;
    }
}

//- (long)getBuilderSizeW
//{
//    return [[sizeWidthList_ getItem:sizeWidthList_.selectIndex] intValue];
//}
//
//- (long)getBuilderSizeH
//{
//    return [[sizeHeightList_ getItem:sizeHeightList_.selectIndex] intValue];
//}

- (int)getBuilderStyleBold
{
    if(switchStyleBold_.on){
        return EPOS_OC_TRUE;
    }else{
        return EPOS_OC_FALSE;
    }
}

- (int)getBuilderStyleUnderline
{
    if(switchStyleUnderline_.on){
        return EPOS_OC_TRUE;
    }else{
        return EPOS_OC_FALSE;
    }
}

- (long)getBuilderXPosition
{
    return [textXPosition_.text intValue];
}

- (NSString *)getBuilderText
{
    return textData_.text;
}

- (long)getBuilderFeedUnit
{
    return [textFeedUnit_.text intValue];
}
//- (void)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL) blTest
//{
//    //    [self.loadProcess.loadView showWithStatus];
//    //get open parameter
//    if(textIp.IP.length == 0){
//        [ShowMsg showError:@"errmsg_noaddress"];
//        [LoadProcess dismissLoad];
//        return ;
//    }
//    
//    //open device type
//    int connectionType = EPOS_OC_DEVTYPE_TCP;
//    //    switch(deviceType_.selectedSegmentIndex){
//    //        case 0:
//    //            connectionType = EPOS_OC_DEVTYPE_TCP;
//    //            break;
//    //        default:
//    //            connectionType = EPOS_OC_DEVTYPE_BLUETOOTH;
//    //            break;
//    //    }
//    
//    //open
//    EposPrint *printer = [[EposPrint alloc] init];
//    if(printer == nil){
//        [LoadProcess dismissLoad];
//        return ;
//    }
//    
//    //    [delegate_ setEventCallback:printer];
//    //    if(printer != nil){
//    //        [printer setStatusChangeEventCallback:@selector(onStatusChange:Status:) Target:self];
//    //        [printer setBatteryStatusChangeEventCallback:@selector(onBatteryStatusChange:Battery:) Target:self];
//    //    }
//    // ต่อ printer
//    int result = [printer openPrinter:connectionType DeviceName:textIp.IP Enabled:[self getStatusMonitorEnabled] Interval:1000];
//    if(result != EPOS_OC_SUCCESS){
//        [LoadProcess dismissLoad];
//        [ShowMsg showExceptionEpos:result method:@"openPrinter"];
//        
//        return;
//    }
//    else{
//        [LoadProcess dismissLoad];
//    }
//    
//    int language = 0; //EPOS_OC_MODEL_THAI;;
//    //    if (!blTest) {
//    //        [delegate_ onOpenPrinter:printer
//    //                  connectionType:connectionType
//    //                       ipaddress:textIp.IP
//    //                     printername:@"TM-m10"
//    //                        language:language
//    //                 ListDataPrinter:textIp];
//    //    }
//    
//    EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
//    if(builder == nil){
//        return ;
//    }
//    
//    result = [builder addText:[self getBuilderText]];
//    if(result != EPOS_OC_SUCCESS){
//        [ShowMsg showExceptionEpos:result method:@"addText"];
//        return ;
//    }
//    
//    //send builder data
//    unsigned long status = 0;
//    unsigned long battery = 0;
//    result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
//    [ShowMsg showStatus:result status:status battery:battery];
//    
//    //remove builder
//    [builder clearCommandBuffer];
//    
//    //return settings
//    //    if(result != nil){
//    //        int language = EPOS_OC_MODEL_THAI;;
//    ////        switch(languageList_.selectIndex){
//    ////            case 1:
//    ////                language = EPOS_OC_MODEL_JAPANESE;
//    ////                break;
//    ////            case 2:
//    ////                language = EPOS_OC_MODEL_CHINESE;
//    ////                break;
//    ////            case 3:
//    ////                language = EPOS_OC_MODEL_TAIWAN;
//    ////                break;
//    ////            case 4:
//    ////                language = EPOS_OC_MODEL_KOREAN;
//    ////                break;
//    ////            case 5:
//    ////                language = EPOS_OC_MODEL_THAI;
//    ////                break;
//    ////            case 6:
//    ////                language = EPOS_OC_MODEL_SOUTHASIA;
//    ////                break;
//    ////            case 0:
//    ////            default:
//    ////                language = EPOS_OC_MODEL_ANK;
//    ////                break;
//    ////        }
//    //        int connectionType = EPOS_OC_DEVTYPE_TCP;
//    ////        switch(deviceType_.selectedSegmentIndex){
//    ////            case 0:
//    ////                connectionType = EPOS_OC_DEVTYPE_TCP;
//    ////                break;
//    ////            default:
//    ////                connectionType = EPOS_OC_DEVTYPE_BLUETOOTH;
//    ////                break;
//    ////        }
//    //        if (!blTest) {
//    //            [delegate_ onOpenPrinter:printer
//    //                      connectionType:connectionType
//    //                           ipaddress:textIp.IP
//    //                         printername:@"TM-m10"
//    //                            language:language
//    //                     ListDataPrinter:textIp];
//    //        }
//    //
//    //        EposBuilder *builder = [[EposBuilder alloc] initWithPrinterModel:@"TM-m10" Lang:language];
//    //        if(builder == nil){
//    //            return ;
//    //        }
//    //
//    //        //send builder data
//    //        unsigned long status = 0;
//    //        unsigned long battery = 0;
//    //        result = [printer sendData:builder Timeout:SEND_TIMEOUT Status:&status Battery:&battery]; // ส่งปริ้น
//    //        [ShowMsg showStatus:result status:status battery:battery];
//    //        
//    //        //remove builder
//    //        [builder clearCommandBuffer];
//    //
//    //    }
//    //return main activity
//    //    [self.navigationController popViewControllerAnimated:YES];
//}
@end
