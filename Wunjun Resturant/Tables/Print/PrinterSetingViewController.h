//
//  PrinterSetingViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 7/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrinterDB.h"
#import "ListDataPrinter.h"
#import "CNPPopupController.h"
#import "AppDelegate.h"

#import "ePOS-Print.h"
#import "WYPopoverController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioServices.h>
#import <AVFoundation/AVFoundation.h>

//#import "PickerTableView.h"
#import "UIViewControllerCustom.h"

@class EposPrint;
@protocol OpenPrinterDelegate
- (void)onOpenPrinter:(EposPrint*)prn
       connectionType:(int)connectionType
            ipaddress:(NSString*)ipaddress
          printername:(NSString*)printername
             language:(int)language
      ListDataPrinter:(ListDataPrinter *)listDataPrinter;

- (void)setEventCallback:(EposPrint*)prn;

@end

@interface PrinterSetingViewController : UIViewControllerCustom<UITableViewDataSource,UITableViewDelegate,AVAudioPlayerDelegate,WYPopoverControllerDelegate,CNPPopupControllerDelegate> {
    id<OpenPrinterDelegate> delegate_;
}


@property (weak, nonatomic) IBOutlet UITableView *mTableViewPrinter;
@property (strong,nonatomic) NSMutableArray *listPrinter;
@property (weak, nonatomic) IBOutlet UILabel *mSound;
@property (weak, nonatomic) IBOutlet UISwitch *mSwitchPrinter;
@property (weak, nonatomic) IBOutlet UISwitch *mSwitchMenuSuggestion;

@property (weak, nonatomic) IBOutlet UILabel *labelLanguage;
@property (weak, nonatomic) IBOutlet UILabel *labelMenuSuggestion;
@property (weak, nonatomic) IBOutlet UILabel *labelSound;
@property (weak, nonatomic) IBOutlet UILabel *labelPrinter;



@property (weak, nonatomic) IBOutlet UIButton *mLanguage;
- (IBAction)btnLanguage:(UIButton *)sender;
- (IBAction)btnSwichConnect:(id)sender;
- (IBAction)btnMenuSuggestion:(id)sender;
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender;
- (IBAction)cilckSettingSound:(UITapGestureRecognizer *)sender;

@property (assign, nonatomic) id<OpenPrinterDelegate> delegate;

@property (nonatomic, retain) MPMusicPlayerController *musicPlayer;
@property (strong, nonatomic) NSMutableArray *songsList;
@property (strong, nonatomic) AVPlayer *audioPlayer;
@property (nonatomic) BOOL IsAudio;

@property (nonatomic) BOOL isLanguage;

- (void)btnTestPrint:(ListDataPrinter *)dataPrinter;
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title ;

+(void)setIsseclect;

- (void)openPrinter:(ListDataPrinter *)textIp andTest:(BOOL) blTest sus:(void (^)(BOOL completion)) block;
@end
