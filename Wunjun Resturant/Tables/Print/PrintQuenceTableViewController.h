//
//  PrintQuenceTableViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ConfigPrinter.h"
#import "ListDataPrinter.h"
#import "PrinterSetingViewController.h"

#define RGBA 4
#define RGBA_8_BIT 8

typedef struct ARGBPixel {
    unsigned char alpha;
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} ARGBPixel;

@class EposPrint;

@interface PrintQuenceTableViewController : UIViewController <NSStreamDelegate> {

    NSOutputStream	*outputStream;
    NSMutableDictionary *dataImagePrint;
     id<OpenPrinterDelegate> delegate_;
}

@property (nonatomic, retain) NSDictionary   *tableData;
@property (nonatomic, retain) NSDictionary   *dataPrint;
@property (nonatomic, retain) NSDictionary   *menuData;
@property (nonatomic, retain) NSMutableArray *rowData;
@property (nonatomic, retain) NSString       *typePrint;
@property (nonatomic, strong) NSString       *sortRepeat;
@property (nonatomic        ) BOOL           *showIdMenu;
@property (strong, nonatomic) NSString       *maxPrevBillId;
@property (nonatomic,strong ) NSString       *typePrintCut;
@property (strong, nonatomic) NSString       *nPrint;
@property (strong, nonatomic) NSString       *kitchenType;
@property (nonatomic        ) BOOL           isCashierPrint;
@property (nonatomic        ) BOOL           onePrint;
@property (nonatomic        ) BOOL           isSalesPrint;
@property (nonatomic        ) BOOL           isPrintEPSON;

@property (strong, nonatomic)  NSString        *namePrint;
@property (strong, nonatomic)  NSString        *IP;
@property (strong, nonatomic)  NSString        *Port;
@property (strong, nonatomic)  NSString        *brandPrinter;
@property (strong, nonatomic)  UITableView     *tableImagePrint;

@property (strong,nonatomic)   NSMutableDictionary *dataImagePrint;
@property (nonatomic, retain)  NSOutputStream *outputStream;
@property(strong,nonatomic)  ListDataPrinter *listDataPrinter;
@property (assign, nonatomic) id<OpenPrinterDelegate> delegate;

- (void) initNetworkCommunication:(void (^)(bool completion)) block;
- (void) messageReceived:(NSString *)message;
-(void)initPrinterConnect:(void (^)(bool completion)) block;

-(BOOL )createViewPrint;
-(void)blockVender:(int)numKey andS:(int)numSec suss:(void (^)(bool completion)) block;
@end
