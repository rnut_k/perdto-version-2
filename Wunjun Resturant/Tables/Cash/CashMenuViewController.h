//
//  CashMenuViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashMenuViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mtableMenuCash;

@end
