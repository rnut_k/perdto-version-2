//
//  CashTableStatus.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashTableStatus.h"
#import "TableAllZoneCollectionView.h"
#import "GlobalBill.h"
#import "MenuDB.h"
#import "Time.h"
#import "LoadMenuAll.h"
#import "Check.h"
#import "CustomNavigationController.h"

#import "WYPopoverController.h"
#import "UIViewController+ECSlidingViewController.h"
#import "MEDynamicTransition.h"
#import "METransitions.h"

@interface CashTableStatus ()<WYPopoverControllerDelegate>{
    
    WYPopoverController *popoverControllerStatus;
    UITextField *textFieldNum;
    CustomNavigationController *vc;
    
    LoadProcess *loadProgress;
    UIButton *leftBarNoti;
    
    NSMutableArray *pendingList;
    NSMutableDictionary *usageData;
    NSMutableDictionary *billData;
    
    THSegmentedPager *pager;
    UITableViewController  *tableViewNotiCheckBill;
}
@property (nonatomic,strong)  Member *member;
@property (nonatomic, strong) METransitions *transitions;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation CashTableStatus{
    GlobalBill *_globalBill;
    MenuDB *_menuDB;
}
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self initEcsliding];
    _globalBill = [GlobalBill sharedInstance];
    self.listNotiCheckBill = [NSMutableArray array];
    [self createButtonBarLeft];
    
    loadProgress = [LoadProcess sharedInstance];
    [self setTitle:[NSString stringWithFormat:@"%@ cash ",self.member.titleRes]];
    
    vc = (CustomNavigationController *)self.slidingViewController.topViewController;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [loadProgress.loadView showProgress];
    
    [self loadDataGetPendingNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadDataGetPendingNotification)
                                                 name:@"updateCheckBillNoti" object:nil];
    
    [self loadUpdata];
   
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    if([Check checkNull:_globalBill.moneyIn] && ![self.role isEqualToString:@"admin"]){
        [self beginViewCash];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)initEcsliding{
    self.transitions.dynamicTransition.slidingViewController = self.slidingViewController;
    self.slidingViewController.topViewAnchoredGesture = ECSlidingViewControllerAnchoredGestureTapping | ECSlidingViewControllerAnchoredGesturePanning;
    self.slidingViewController.customAnchoredGestures = @[];
    [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
    [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
}
#pragma mark - Properties

- (METransitions *)transitions {
    if (_transitions) return _transitions;
    
    _transitions = [[METransitions alloc] init];
    
    return _transitions;
}
- (UIPanGestureRecognizer *)dynamicTransitionPanGesture {
    if (_dynamicTransitionPanGesture) return _dynamicTransitionPanGesture;
    
    _dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.transitions.dynamicTransition
                                                                           action:@selector(handlePanGesture:)];
    return _dynamicTransitionPanGesture;
}
-(void)createButtonBarLeft{
    leftBarNoti =  [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBarNoti addTarget:self action:@selector(createPopoverController:) forControlEvents:UIControlEventTouchUpInside];
    [leftBarNoti setFrame:CGRectMake(0,0,30,30)];
    [leftBarNoti  setBackgroundImage:[UIImage imageNamed:@"bg_tab_noti"] forState:UIControlStateNormal];
    
    UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarNoti];
    NSMutableArray *items = [NSMutableArray arrayWithArray:self.navigationItem.leftBarButtonItems];
    [items addObject:leftBarButtonItem];
    [self.navigationItem setLeftBarButtonItems:items];
}
-(void)loadUpdata{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:MAPI_URL,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSMutableDictionary *_dataAllZone = json[@"resturant"];
        if([_dataAllZone count] != 0){
            _globalBill.billResturant = _dataAllZone;
        }
        [self loadDataGetZone];
    }];
}
-(void)loadDataGetZone{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_ZONE,self.member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSMutableArray *_dataAllZone = json[@"data"];
        if([_dataAllZone count] != 0){
            _globalBill = [GlobalBill sharedInstance];
            bool blu = [_globalBill.billResturant[@"use_sub_bill"] boolValue];
            _globalBill.billType = blu ? kSubBill:kSingleBill;
            [self createViewTableZone:_dataAllZone];
        }
    }];
}

-(void)loadDataGetPendingNotification{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_PENDING_NOTI,self.member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        BOOL bl = [json[@"status"] boolValue];
        if(bl){
            NSDictionary *data = [NSDictionary dictionaryWithObject:json[@"data"]];
            if ([data count] > 0) {
                pendingList        = [NSMutableArray arrayWithArray:data[@"pending_list"]];
                usageData          = [NSDictionary dictionaryMutableWithObject:data[@"usage_data"]];
                billData           = [NSDictionary dictionaryMutableWithObject:data[@"bill_data"]];
            }
        }
        [leftBarNoti setTitle:[NSString stringWithFormat:@"%lu",(unsigned long)[pendingList count]]  forState:UIControlStateNormal];
        [tableViewNotiCheckBill.tableView reloadData];
    }];
}
-(void)loadDataReadPendingNotification:(NSString *)idNoti{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    NSString *url = [NSString stringWithFormat:GET_READ_PENDING_NOTI,self.member.nre,idNoti];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        BOOL bl = [json[@"status"] boolValue];
        DDLogDebug(@"%d",bl);
    }];
}
#define GET_PENDING_NOTI        (MAPI_URL @"getPendingNotification")
#define GET_READ_PENDING_NOTI   (MAPI_URL @"readPendingNotification/%@") // id

- (void)doCalculationsAndUpdateUIs {
    [LoadMenuAll checkDataMenu:@"0"];
}
-(void) createViewTableZone:(NSMutableArray *)_dataAllZone {
    
    pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
    NSMutableArray *pages = [NSMutableArray new];
    
    NSDictionary *tableIndexData = [[[NSDictionary alloc]init] mutableCopy];
    [LoadMenuAll retrieveData:@"0" success:^(NSArray *dataTables){
        dispatch_async(dispatch_get_main_queue(), ^{
            for(NSDictionary *data in dataTables){
                NSString *key = [NSString stringWithFormat:@"zone_id_%@", data[@"zone_id"]];
                NSMutableArray *tableListInZone = (tableIndexData[key] != nil)?(NSMutableArray*)tableIndexData[key]:[[NSMutableArray alloc]init];
                [tableListInZone addObject:data];
                [tableIndexData setValue:tableListInZone forKey:key];
            }
            
            for (int i = 0; i < [_dataAllZone count]; i++) {
                NSDictionary *dic = _dataAllZone[i];
                NSString *key = [NSString stringWithFormat:@"zone_id_%@", dic[@"id"]];
                TableAllZoneCollectionView *_tableAllZoneCollectionView = [pager.storyboard instantiateViewControllerWithIdentifier:@"TableAllZone"];
                [pages addObject:_tableAllZoneCollectionView];
                [_tableAllZoneCollectionView setViewTitle:[dic objectForKey:@"name"]];
                [_tableAllZoneCollectionView setDataListZone:dic];
                [_tableAllZoneCollectionView setDataListZoneAll:_dataAllZone];
                [_tableAllZoneCollectionView setNumTable:[Check checkDevice]?3:6];
                [_tableAllZoneCollectionView setCashTableStatus:self];
                [_tableAllZoneCollectionView setDataListTables:tableIndexData[key]];
                
                //admin role
                if([self.role isEqualToString:@"admin"]){
                    [_tableAllZoneCollectionView setRole:@"admin"];
                }
                [_tableAllZoneCollectionView.collectionView reloadData];
            }
            [pager setPages:pages];
            
            pager.view.frame = self.viewContainer.bounds;
            [self.viewContainer addSubview:pager.view];
            [self addChildViewController:pager];
            [pager didMoveToParentViewController:self];
        });
    }];
}


- (void)beginViewCash{

    UIViewController  *settingsViewController = [[UIViewController alloc] init];
    settingsViewController.preferredContentSize = CGSizeMake(300,200);
    settingsViewController.title = AMLocalizedString(@"ยอดเงินก่อนชำระ", nil);
    settingsViewController.modalInPopover = NO;
    
    UINavigationController *contentViewController = [[UINavigationController alloc] initWithRootViewController:settingsViewController];
    
    popoverControllerStatus = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverControllerStatus.delegate = self;
    popoverControllerStatus.popoverLayoutMargins = UIEdgeInsetsMake(10, 10, 10, 10);
    popoverControllerStatus.wantsDefaultContentAppearance = NO;

    [popoverControllerStatus presentPopoverFromRect:CGRectZero
                                             inView:self.view
                           permittedArrowDirections:WYPopoverArrowDirectionNone
                                           animated:YES
                                            options:WYPopoverAnimationOptionFadeWithScale];

    CGSize size = popoverControllerStatus.popoverContentSize;
    
    textFieldNum = [[UITextField alloc] initWithFrame:CGRectMake(20,10,size.width-40,50)];
    [textFieldNum setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    textFieldNum.textAlignment = NSTextAlignmentCenter;
    [textFieldNum setBackgroundColor:[UIColor redColor]];
    textFieldNum.layer.cornerRadius = 10.0f;
    textFieldNum.layer.borderWidth = 1.0f;
    textFieldNum.layer.borderColor = [UIColor ht_silverColor].CGColor;
    textFieldNum.clipsToBounds = NO;
    textFieldNum.backgroundColor = [UIColor whiteColor];
    textFieldNum.layer.masksToBounds = YES;
    [textFieldNum setKeyboardType:UIKeyboardTypeNumberPad];
    [settingsViewController.view addSubview:textFieldNum];
    
    UIButton *btnReply = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnReply setFrame:CGRectMake(5,90,(size.width/2)-10,40)];
    [[btnReply imageView] setContentMode: UIViewContentModeScaleToFill];
    [btnReply setTitle:AMLocalizedString(@"ยืนยัน", nil)  forState:UIControlStateNormal];
    [btnReply setBackgroundColor:[UIColor colorWithRed:0.4431 green:0.6275 blue:0.2431 alpha:1.0]];
    [btnReply.layer setCornerRadius:20.0];;
    [btnReply addTarget:self action:@selector(btnCbtnReply:) forControlEvents:UIControlEventTouchUpInside];
    [settingsViewController.view addSubview:btnReply];
    
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnClose setFrame:CGRectMake((btnReply.frame.origin.x + btnReply.frame.size.width) +10,90,(size.width/2)-10,40)];
    [[btnClose imageView] setContentMode: UIViewContentModeScaleToFill];
    [btnClose setTitle:AMLocalizedString(@"ไม่", nil)  forState:UIControlStateNormal];
    [btnClose setBackgroundColor:[UIColor colorWithRed:0.6784 green:0.6706 blue:0.6588 alpha:1.0]];
    [btnClose.layer setCornerRadius:20.0];
    [btnClose addTarget:self action:@selector(btnCbtnClose:) forControlEvents:UIControlEventTouchUpInside];
    [settingsViewController.view addSubview:btnClose];
}
#pragma mark - WYPopoverControllerDelegate

- (void)popoverControllerDidPresentPopover:(WYPopoverController *)controller
{
    NSLog(@"popoverControllerDidPresentPopover");
}

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController{
    NSLog(@"%@",textFieldNum.text);
    if ([_globalBill.moneyIn isEqual:@""]) {
        [self beginViewCash];
    }
    else{
        popoverControllerStatus.delegate = nil;
        popoverControllerStatus = nil;
    }
}

- (BOOL)popoverControllerShouldIgnoreKeyboardBounds:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverController:(WYPopoverController *)popoverController willTranslatePopoverWithYOffset:(float *)value
{
    // keyboard is shown and the popover will be moved up by 163 pixels for example ( *value = 163 )
    *value = 0; // set value to 0 if you want to avoid the popover to be moved
}

- (void)createPopoverController:(id)sender{
    UIButton *btn = (UIButton *)sender;
    
    tableViewNotiCheckBill = [[UITableViewController alloc] init];
    tableViewNotiCheckBill.title = AMLocalizedString(@"แจ้งเตือน เซ็กบิล !", nil);
    tableViewNotiCheckBill.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    tableViewNotiCheckBill.modalInPopover = NO;
    [tableViewNotiCheckBill.tableView setDelegate:self];
    [tableViewNotiCheckBill.tableView setDataSource:self];
    
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:tableViewNotiCheckBill];
    popoverControllerStatus = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverControllerStatus.delegate = self;
    [popoverControllerStatus presentPopoverFromRect:btn.bounds
                                       inView:btn
                     permittedArrowDirections:WYPopoverArrowDirectionAny
                                     animated:YES
                                      options:WYPopoverAnimationOptionFadeWithScale];
}

-(void)btnCbtnReply:(id )sender{
    if (![textFieldNum.text isEqual:@""]) {
        [self dismissPopUP];
        [_globalBill setMoneyIn:textFieldNum.text];
    }
}
-(void)btnCbtnClose:(id )sender{
    [self dismissPopUP];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void)dismissPopUP{
     [popoverControllerStatus dismissPopoverAnimated:YES];
}
- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}


#pragma mark - delete table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [pendingList count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenfine = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenfine];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:idenfine];
    }
    
    NSInteger row = [indexPath row];
    NSDictionary *dataPendingList = [NSDictionary dictionaryWithObject:pendingList[row]];
    NSString *key = [NSString stringWithFormat:@"object_id_%@:%@",dataPendingList[@"object_id"],dataPendingList[@"object_relate_id"]];
    NSDictionary *dataUsage = [NSDictionary dictionaryWithObject:usageData[key]];
    
    NSString *label = @"";
    NSString *created_datetime = [Time getTimeandDate];
    if ([dataUsage count] > 0) {
        NSDictionary *tables = [NSDictionary dictionaryWithObject:dataUsage[@"tables"]];
        label = tables[@"label"];
        created_datetime = dataPendingList[@"created_datetime"];
    }
    
    NSString *time = [Time covnetTimeDMY1:[Time getICTFormateDate:created_datetime]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"เช็กบิล โต็ะ", nil),label];
    cell.detailTextLabel.text = time;
    cell.textLabel.textColor = [UIColor ht_leadDarkColor];
    cell.detailTextLabel.textColor = [UIColor ht_leadDarkColor];
    [[cell imageView] setImage:[UIImage imageNamed:@"ic_checkbill"]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = indexPath.row;
    NSDictionary *dataPendingList = [NSDictionary dictionaryWithObject:pendingList[row]];
    NSString *key = [NSString stringWithFormat:@"object_id_%@:%@",dataPendingList[@"object_id"],dataPendingList[@"object_relate_id"]];
    NSDictionary *dataUsage = [NSDictionary dictionaryWithObject:usageData[key]];
    [self loadDataReadPendingNotification:dataPendingList[@"id"]];
    
    TableAllZoneCollectionView *obj = (TableAllZoneCollectionView *)pager.selectedController;
    [obj loadDataOpenTable:dataUsage[@"tables"] andType:@"out"];
    [self dismissPopUP];
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"logout"]) {  //    logout
//        DeviceData *device = [DeviceData getInstance];
//        if(device.token == nil)
//            device.token = @"";
//        [self.member deleteLoginDb];
//        
//        MenuDB *menuDB = [MenuDB getInstance];
//        [menuDB deleteLoginDb];
        [LoadMenuAll logoutView:@{@"error":LOGIN_ERROR}];
        
//        UIViewController *vc_ = segue.destinationViewController;
//        [self presentViewController:vc_ animated:YES completion:nil];

    }
    else if ([[segue identifier] isEqualToString:@"NVTakeaway"]) {  //    logout
        NSLog(@"NVTakeaway");
//        DeviceData *device = [DeviceData getInstance];
//        if(device.token == nil)
//            device.token = @"";
//        [self.member deleteLoginDb];
//        
//        MenuDB *menuDB = [MenuDB getInstance];
//        [menuDB deleteLoginDb];
//        
//        UIViewController *vc_ = segue.destinationViewController;
//        [self presentViewController:vc_ animated:YES completion:nil];
    }
}

@end
