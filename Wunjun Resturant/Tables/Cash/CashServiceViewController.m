//
//  CashServiceViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 10/1/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "CashServiceViewController.h"
#import "CustomNavigationController.h"

#import "NSString+GetString.h"
#import "NSDictionary+DictionaryWithString.h"
#import "Check.h"
#import "LoadProcess.h"
#import "LocalizationSystem.h"
#import "Constants.h"
#import "LoadMenuAll.h"
#import "Member.h"
#import "PopupView.h"

@interface CashServiceViewController ()<CNPPopupControllerDelegate>{
    CustomNavigationController *vc;
    LoadProcess *loadProcess;
    PopupView *popupView;
    NSDictionary *detailTable;
    NSDictionary *dataListSingBill;
    NSDictionary *billInfo;
    NSArray *billList;
    NSDictionary *serviceInfo;
    NSDictionary *totalInfo;
    
    NSInteger servicePer;
    NSInteger serviceTotal;
    
    int totalBill;
}
@property (nonatomic ,strong) Member *member;
@end

@implementation CashServiceViewController
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    vc = (CustomNavigationController *)self.parentViewController;
    self.tableSingleBillView = (TableSingleBillViewController *)vc.dataSingleBillViewController;
    
    detailTable = [NSDictionary dictionaryWithObject:vc.detailTable];
    dataListSingBill = [NSDictionary dictionaryWithObject:vc.dataListSingBill];
    loadProcess = [LoadProcess sharedInstance];
    
    billInfo = [NSDictionary dictionaryWithObject:dataListSingBill[@"bill_info"]];
    billList = [NSArray arrayWithArray:dataListSingBill[@"bill_list"]];
    serviceInfo = [NSDictionary dictionaryWithObject:dataListSingBill[@"service"]];
    totalInfo = [NSDictionary dictionaryWithObject:dataListSingBill[@"total"]];
    
    [self selectSubbill:[billList firstObject]];
    
    NSString *idBill = [@"id_" stringByAppendingString:[billList firstObject]];
    NSString *idBillService = [@"bill_id_" stringByAppendingString:[billList firstObject]];
    NSDictionary *dataBill = [NSDictionary dictionaryWithObject:billInfo[idBill]];
    servicePer = [dataBill[@"service_per"] integerValue];
    serviceTotal = [serviceInfo[idBillService] integerValue];
    
    totalBill = [totalInfo[@"total"] intValue];
    [self.mTotalBill setText:[NSString conventCurrency:[@(totalBill) stringValue]]];
    [self.mPersent setText:[@(servicePer) stringValue]];
    [self.mTotal setText:[@(serviceTotal) stringValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma - mark TextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //     NSLog(@"%@",textField.text);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //    NSLog(@"%@",textField.text);
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    
    if ([self.mPersent isEqual:textField]) { // จำนวนบาท
        int num1 = ([number floatValue] / 100.0 ) * totalBill;
        [self.mTotal setText:[@(num1) stringValue]];
    }
    else{ // จำนวนเปอร์เซ็นต์
        int num1 =  ([number floatValue] / totalBill) * 100.0;
        [self.mPersent setText:[@(num1) stringValue]];
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char,"\b");
    if (isBackSpace == -8) {
        // is backspace
        if (self.mTotal == textField){
            self.mPersent.text = [self.mPersent.text substringToIndex:[self.mPersent.text length]-1];
            if ([Check checkNull:self.mPersent.text]) {
                self.mPersent.text = @"0";
            }
        }
        else{
            self.mTotal.text = [self.mTotal.text substringToIndex:[self.mTotal.text length] -1];
            if ([Check checkNull:self.mTotal.text]) {
                self.mTotal.text = @"0";
            }
        }
    }
    
    if (number)
        return YES;
    else
        return NO;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //    NSLog(@"%@",textField.text);
    [textField resignFirstResponder];
    return YES;
    
}

-(void)loadDataSubmitCoupon{
    [loadProcess.loadView showWithStatus];
    
    NSString *n    = self.mPersent.text;
    NSError *error = nil;
    NSString *cartID = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:billList
                                                                                      options:NSJSONWritingPrettyPrinted
                                                                                        error:&error]
                                             encoding:NSUTF8StringEncoding];
    
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"bill_list":cartID,
                                 @"service":n,  // %
                                 };
    NSString *url = [NSString stringWithFormat:SET_SERVICE_RATE,self.member.nre];    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        BOOL staus = [dictionary[@"status"] boolValue];
        if (staus) {
            [self cilckBack:nil];
        }
        else
            [loadProcess.loadView showError];
    }];
}
- (IBAction)cilckSelectBill:(id)sender {
    popupView = [[PopupView alloc] init];
    [popupView setTableSingleBillViewController:self.tableSingleBillView];
    [popupView setCashServiceViewController:self];
    [popupView createPopoverDrinkMenu:self];
}

- (IBAction)cilckSubmit:(id)sender {
    NSString *n    = self.mPersent.text;
    if (![Check checkNull:n]) {
        [self loadDataSubmitCoupon];
    }
}
- (void)selectSubbill:(id)data{
    NSString *idS = @"";
    if([data isKindOfClass:[NSString class]]){
        idS = [NSString checkNull:data];
    }
    else{
        idS = [NSString checkNull:data[@"id"]];
    }
  
    NSString *idBill = [@"id_" stringByAppendingString:idS];
    NSString *idBillService = [@"bill_id_" stringByAppendingString:idS];
    NSDictionary *dataBill = [NSDictionary dictionaryWithObject:billInfo[idBill]];
    servicePer = [dataBill[@"service_per"] integerValue];
    serviceTotal = [serviceInfo[idBillService] integerValue];
    
    int total = [totalInfo[@"total"] intValue];
    [self.mTotalBill setText:[NSString conventCurrency:[@(total) stringValue]]];
    [self.mPersent setText:[@(servicePer) stringValue]];
    [self.mTotal setText:[@(serviceTotal) stringValue]];
}
- (IBAction)cilckBack:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
