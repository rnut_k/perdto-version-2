//
//  CashTableNotCheckBill.h
//  Wunjun Resturant
//
//  Created by AgeNt on 5/12/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocalizationSystem.h"

@interface CashTableNotCheckBill : UIViewController

@property (nonatomic,weak) IBOutlet UITableView *mTableNotBill;
@property (nonatomic,weak) IBOutlet UIButton *mButtonClose;
@property (nonatomic,weak) IBOutlet UILabel *mTitleNotBill;

@property (nonatomic,strong) NSArray *dataNotCheckBill;

@end
