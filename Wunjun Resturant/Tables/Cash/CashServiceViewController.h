//
//  CashServiceViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 10/1/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableSingleBillViewController.h"

@interface CashServiceViewController : UIViewController<UITextFieldDelegate,CNPPopupControllerDelegate>


@property (weak, nonatomic) IBOutlet UIButton *cilckCancel;
@property (weak, nonatomic) IBOutlet UITextField *mPersent;
@property (weak, nonatomic) IBOutlet UITextField *mTotal;
@property (weak, nonatomic) IBOutlet UILabel *mTotalBill;
@property (weak, nonatomic) IBOutlet UILabel *mTitleBill;

- (IBAction)cilckSelectBill:(id)sender;
- (IBAction)cilckSubmit:(id)sender;
- (IBAction)cilckBack:(id)sender;

@property (nonatomic,strong) TableSingleBillViewController  *tableSingleBillView;

- (void)selectSubbill:(id)data;
@end
