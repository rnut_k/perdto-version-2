//
//  CashReportBillViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KRLCollectionViewGridLayout.h"
#import "TableDataBillViewController.h"
#import "UIStoryboard+Main.h"

@interface CashReportBillViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *mCollectionViewBill;
@property (weak, nonatomic) IBOutlet UILabel *mSortBillDate;
@property (weak, nonatomic) IBOutlet UILabel *mSortBillAll;
@property (weak, nonatomic) IBOutlet UILabel *mtotal;

@property (weak, nonatomic) IBOutlet UIView *mContainerTableBill;

- (IBAction)btnSortBillDate:(id)sender;
- (IBAction)btnSortBillAll:(id)sender;

- (IBAction)menuButtonTapped:(id)sender;
@end
