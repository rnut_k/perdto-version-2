//
//  CashTakeawayTableViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 6/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "THSegmentedPageViewControllerDelegate.h"
#import "OrdersKitchen.h"
#import "UIColor+HTColor.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "Time.h"
#import "Text.h"
#import "NSDictionary+DictionaryWithString.h"

@interface CashTakeawayTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)  Member *member;

@property (weak, nonatomic) IBOutlet UITableView *mTableTakeaway;

@property (nonatomic) BOOL isCashierTake;
@property (nonatomic) BOOL isStaffTake;

- (IBAction)btnCheckBill:(id)sender;
- (IBAction)btnCreateBill:(id)sender;
@end
