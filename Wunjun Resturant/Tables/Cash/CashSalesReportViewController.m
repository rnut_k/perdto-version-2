//
//  CashSalesReportViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashSalesReportViewController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "CellTableViewAll.h"
#import "PrintQuenceTableViewController.h"
#import "PrintSelesViewController.h"

#import "CustomNavigationController.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"
#import "WYPopoverController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TableCollectionViewCell.h"
#import "UIColor+HTColor.h"
#import "Check.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "Member.h"
#import "Constants.h"
#import "Time.h"
#import "SearchPromotion.h"
#import "NSString+GetString.h"
#import "LoadProcess.h"

#define kCellBillSale    (@"cellBillSales")
#define kCellDrinkSales  (@"cellDrinkSales")
#define kCellDrinkSubmit (@"cellDrinkSubmit")
#define kPrintBillReport (@"printBillReport")


@interface DateTimeClss : NSObject{
    NSString* textDate;
    NSString* dateTime;
    NSDate  * date;
}
@property NSString* textDate;
@property NSString* dateTime;
@property NSDate  * date;
@end
@implementation DateTimeClss
@synthesize textDate,dateTime,date;
@end
@interface CashSalesReportViewController ()<WYPopoverControllerDelegate>{
    CustomNavigationController *vc;
    Member *member;
    WYPopoverController *popoverController;
    LoadProcess *loadProcess;
    
    NSMutableArray *dataBill;
    NSDictionary *dataTotal;
    NSDictionary *dataSubbill;
    NSArray *dataUsage ;
    NSDictionary *dataTables ;
    NSDictionary *dataOrders ;
    NSDictionary *dataMenu   ;
    NSDictionary *dataPromotion ;
    NSDictionary *dataDiscount ;
    NSDictionary *dataLogEditBill;
    NSArray *dateTimeSubmit;
    
    NSMutableArray *listDateTime;
    NSMutableArray *dataBillShow;
    int totalAll;
    UITableView *tableViewPop;
    
    float totalAll1;
    int countGood;
    NSMutableArray *dataBillPrint;
    
    BOOL isSeclectDate;
}

@property (nonatomic, copy)    NSMutableArray *selectedIndexPaths;
@property (nonatomic, strong)  UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation CashSalesReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{ 
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithCoder:(NSCoder*)aDecoder
{
    if(self = [super initWithCoder:aDecoder])
    {
        loadProcess = [LoadProcess sharedInstance];
        [loadProcess.loadView showWaitProgress];
        
        member = [Member getInstance];
        [member loadData];
    }
    return self;
}

- (NSArray *)selectedIndexPaths
{
    if (!_selectedIndexPaths)
    {
         self.mtbvCashSalesReport.SKSTableViewDelegate = self;
        [self sortDataOrdersInSection];
        
//       0 = CellFoodSeles
//       1 = CellDrinkSeles
//       2 = CellDrinkGirlsSeles
//       3 = CellProSeles
//       4 = CellTotalSeles
    }
    
    return _selectedIndexPaths;
}

#pragma mark - UIViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    [self setTitle:AMLocalizedString(@"รายงานยอดขาย", nil)];
    totalAll1 = 0;

    vc = (CustomNavigationController *)self.parentViewController;
    if(vc != nil ){
        [self setDataAllZone:vc.dataAllZone];
    }
    
    if (!isSeclectDate) {
        [self.mDate setText:[Time covnetTimeDMY]];
        listDateTime = [NSMutableArray arrayWithArray:[self getDateTime]];
        [self loadDataReportBill];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
    
    if (self.slidingViewController != nil) {
        if ([(NSObject *)self.slidingViewController.delegate isKindOfClass:[MEDynamicTransition class]]) {
            MEDynamicTransition *dynamicTransition = (MEDynamicTransition *)self.slidingViewController.delegate;
            if (!self.dynamicTransitionPanGesture) {
                self.dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:dynamicTransition action:@selector(handlePanGesture:)];
            }
            
            [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
            [self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
        }
        else {
            [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
            [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
        }
    }
}

-(void)loadDataReportSeles:(NSString *)date{

    NSDictionary *parameters = @{@"code":member.code,
                                 @"type":@"2",
                                 @"get_summary":@"1",
                                 @"datetime":date};
    
    NSString *url = [NSString stringWithFormat:GET_BILL_DETAIL,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            self.dataAllZone = [NSDictionary dictionaryWithObject:json[@"data"]];
            isSeclectDate = YES;
            [self loadDataReportBill];
            [self reloadTableViewWithData];
            
            [LoadProcess dismissLoad];
        }
        else{
            [loadProcess.loadView showWithStatusAddError:@"โหดลข้อมูลไม่สำเร็จ"];
        }
    }];
}
-(void)loadDataReportBill{
    dataBill      = [NSMutableArray arrayWithArray:[Check checkObjectType:self.dataAllZone[@"bill"]]];
    dataBillShow  = [NSMutableArray arrayWithArray:[dataBill copy]];
    dataTotal     = [NSDictionary dictionaryWithObject:self.dataAllZone[@"total"]];
    dataSubbill   = [NSDictionary dictionaryWithObject:self.dataAllZone[@"subbill"]];
    dataUsage     = [NSArray arrayWithArray:[Check checkObjectType:self.dataAllZone[@"usage"]]];
    dataTables    = [NSDictionary dictionaryWithObject:self.dataAllZone[@"tables"]];
    dataOrders    = [NSDictionary dictionaryWithObject:self.dataAllZone[@"orders"]];
    dataMenu      = [NSDictionary dictionaryWithObject:self.dataAllZone[@"menu"]];
    dataPromotion = [NSDictionary dictionaryWithObject:self.dataAllZone[@"promotion"]];
    dataLogEditBill = [NSDictionary dictionaryWithObject:self.dataAllZone[@"log"]];
    dateTimeSubmit  = [NSArray arrayWithArray:[Check checkObjectType:self.dataAllZone[@"submit_date"]]];
}

-(void) sortDataOrdersInSection{
    NSMutableArray *addFood    = [NSMutableArray array];
    NSMutableArray *addDrink   = [NSMutableArray array];
    NSMutableArray *addDrinkGirl = [NSMutableArray array];
    NSMutableArray *addPro = [NSMutableArray array];
    NSMutableArray *addDiscount = [NSMutableArray array];
    NSMutableArray *addCoupon    = [NSMutableArray array];
    
    dataBillPrint = [NSMutableArray array];
    totalAll1 = 0;
    
    NSDictionary *groupAddFood      = [[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *groupAddDrink     = [[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *groupAddDrinkGirl = [[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *groupAddPro       = [[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *groupAddDiscount  = [[[NSDictionary alloc]init] mutableCopy];
    NSDictionary *groupAddCoupon    = [[[NSDictionary alloc]init] mutableCopy];
    
    float totalFood      = 0.0;
    float totalDrink     = 0.0;
    float totalDrinkGirl = 0.0;
    float totalPro       = 0.0;
    float totalDiscount  = 0.0;
    float totalVat       = 0.0;
    float totalCoupon    = 0.0;
    float totalServiceCharge  = 0.0;
    float totalPaidCash    = 0.0;
    float totalPaidCard    = 0.0;
    int countPaidCash    = 0;
    int countPaidCard    = 0;
    
    @try {
        NSArray *billList = [NSArray arrayWithArray:dataBill];
        if ([billList count] != 0) {
            for (NSDictionary *bill in billList) {
                NSString *orderKey = [NSString stringWithFormat:@"bill_id_%@", bill[@"id"]];
                NSArray *dic =  [NSArray arrayWithArray:dataOrders[orderKey]];
                totalVat += [bill[@"vat"] floatValue];
                for (NSDictionary *dicType in dic) {
                    if([dicType[@"deliver_status"] intValue] == 4){
                        continue;
                    }
                    
                    NSInteger type = (![Check checkNull:dicType[@"type"]]?[dicType[@"type"] integerValue]:0);
                    NSInteger menu_type = (![Check checkNull:dicType[@"menu_type"]]?[dicType[@"menu_type"] integerValue]:0);
                    NSString *key = [NSString stringWithFormat:@"%@_%@", dicType[@"menu_id"], dicType[@"price"]];
                    switch (type) {
                        case 1:{
                            switch (menu_type) {
                                case 1:{
                                    if(groupAddFood[key] == nil){
                                        [groupAddFood setValue:dicType forKey:key];
                                    }else{
                                        NSDictionary *gDic = [groupAddFood[key] mutableCopy];
                                        NSString *updateN = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                        NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                        [gDic setValue:updateN forKey:@"n"];
                                        [gDic setValue:updateTotal forKey:@"total"];
                                        [groupAddFood setValue:gDic forKey:key];
                                    }
                                    
                                    totalFood += [dicType[@"total"] floatValue];
                                }
                                    break;
                                case 2:{
                                    if(groupAddDrink[key] == nil){
                                        [groupAddDrink setValue:dicType forKey:key];
                                    }else{
                                        NSDictionary *gDic    = [groupAddDrink[key] mutableCopy];
                                        NSString *updateN     = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                        NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                        [gDic setValue:updateN forKey:@"n"];
                                        [gDic setValue:updateTotal forKey:@"total"];
                                        [groupAddDrink setValue:gDic forKey:key];
                                    }
                                    
                                    totalDrink += [dicType[@"total"] floatValue];
                                }
                                    break;
                                case 3:{
                                    if(groupAddDrinkGirl[key] == nil){
                                        [groupAddDrinkGirl setValue:dicType forKey:key];
                                    }else{
                                        NSDictionary *gDic    = [groupAddDrinkGirl[key] mutableCopy];
                                        NSString *updateN     = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                        NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                        [gDic setValue:updateN forKey:@"n"];
                                        [gDic setValue:updateTotal forKey:@"total"];
                                        [groupAddDrinkGirl setValue:gDic forKey:key];
                                    }
                                    
                                    totalDrinkGirl += [dicType[@"total"] floatValue];
                                }
                                    break;
                                case 4:{
                                    if(groupAddFood[key] == nil){
                                        [groupAddFood setValue:dicType forKey:key];
                                    }
                                    else{
                                        NSDictionary *gFood   = [groupAddFood[key] mutableCopy];
                                        NSString *updateN     = [NSString stringWithFormat:@"%d", [gFood[@"n"] intValue] + [dicType[@"n"] intValue]];
                                        NSString *updateTotal = [NSString stringWithFormat:@"%f", [gFood[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                        [gFood setValue:updateN forKey:@"n"];
                                        [gFood setValue:updateTotal forKey:@"total"];
                                        [groupAddFood setValue:gFood forKey:key];
                                    }
                                    
                                    totalFood += [dicType[@"total"] floatValue];
                                }
                                    break;
                                default:
                                    break;
                            }
                        }
                            break;
                        case 2:{
                            if(groupAddPro[key] == nil){
                                [groupAddPro setValue:dicType forKey:key];
                            }
                            else{
                                NSDictionary *gDic = [groupAddPro[key] mutableCopy];
                                NSString *updateN = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                [gDic setValue:updateN forKey:@"n"];
                                [gDic setValue:updateTotal forKey:@"total"];
                                [groupAddPro setValue:gDic forKey:key];
                            }
                            
                            totalDrinkGirl += [dicType[@"total"] floatValue];
                            
                            totalPro += [dicType[@"total"] floatValue];
                        }
                            break;
                        case 3:{
                            
                            if(groupAddDiscount[key] == nil){
                                [groupAddDiscount setValue:dicType forKey:key];
                            }else{
                                NSDictionary *gDic = [groupAddDiscount[key] mutableCopy];
                                NSString *updateN = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                [gDic setValue:updateN forKey:@"n"];
                                [gDic setValue:updateTotal forKey:@"total"];
                                [groupAddDiscount setValue:gDic forKey:key];
                            }
                            totalDiscount += [dicType[@"total"] floatValue];
                        }
                        case 4:{
                            if(groupAddCoupon[key] == nil){
                                [groupAddCoupon setValue:dicType forKey:key];
                            }else{
                                NSDictionary *gDic = [groupAddCoupon[key] mutableCopy];
                                NSString *updateN = [NSString stringWithFormat:@"%d", [gDic[@"n"] intValue] + [dicType[@"n"] intValue]];
                                NSString *updateTotal = [NSString stringWithFormat:@"%f", [gDic[@"total"] floatValue] + [dicType[@"total"] floatValue]];
                                [gDic setValue:updateN forKey:@"n"];
                                [gDic setValue:updateTotal forKey:@"total"];
                                [groupAddCoupon setValue:gDic forKey:key];
                            }
                            totalCoupon += fabs([dicType[@"total"] floatValue]);
                        }
                            break;
                        default:
                            break;
                    }
                }
                
                float paidCard = [bill[@"paid_card"] floatValue];
                float total = [bill[@"total"] floatValue];
                total -= paidCard;
                
                if (paidCard != 0) {
                    totalPaidCard  += paidCard;
                    countPaidCard ++;
                }
                if (total != 0) {
                    totalPaidCash += [bill[@"total"] floatValue];
                    countPaidCash ++;
                }
                
                totalServiceCharge += [bill[@"service"] floatValue];
            }
            
            //set array value group by list
            if([groupAddFood count] > 0){
                for(NSDictionary *obj in groupAddFood){
                    [addFood addObject:groupAddFood[obj]];
                }
            }
            if([groupAddDrink count] > 0){
                for(NSDictionary *obj in groupAddDrink){
                    [addDrink addObject:groupAddDrink[obj]];
                }
            }
            if([groupAddDrinkGirl count] > 0){
                for(NSDictionary *obj in groupAddDrinkGirl){
                    [addDrinkGirl addObject:groupAddDrinkGirl[obj]];
                }
            }
            if([groupAddPro count] > 0){
                for(NSDictionary *obj in groupAddPro){
                    [addPro addObject:groupAddPro[obj]];
                }
            }
            if([groupAddDiscount count] > 0){
                for(NSDictionary *obj in groupAddDiscount){
                    [addDiscount addObject:groupAddDiscount[obj]];
                }
            }
            if([groupAddCoupon count] > 0){
                for(NSDictionary *obj in groupAddCoupon){
                    [addCoupon addObject:groupAddCoupon[obj]];
                }
            }
            
        }
 
    }
    @catch (NSException *exception) {
        DDLogError(@"error sortDataOrdersInSection exception %@",exception);
    }
    
    NSMutableArray *addFoodBill = [NSMutableArray array];
    if ([addFood count] != 0) {
         addFoodBill  = [SearchPromotion addBillData:addFood andPop:NO];
        [self setdataPrinter:[addFoodBill mutableCopy] andTotal:totalFood and:1];
        [addFoodBill    insertObject:[NSNumber numberWithFloat:totalFood] atIndex:0];
        countGood += [addFoodBill count] -1;
    }
    else{
        [self setdataPrinter:[addFoodBill mutableCopy] andTotal:0 and:1];
        [addFoodBill    addObject:[NSNumber numberWithInt:0]];
        [addFoodBill    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }
    
    if ([addDrink count] != 0) {
         addDrink  = [SearchPromotion addBillData:addDrink andPop:NO];
        [self setdataPrinter:[addDrink mutableCopy] andTotal:totalDrink and:2];
        [addDrink    insertObject:[NSNumber numberWithFloat:totalDrink] atIndex:0];
        
        countGood += [addDrink count] -1;
    }
    else{
        [self setdataPrinter:[addDrink mutableCopy] andTotal:0 and:2];
        [addDrink    addObject:[NSNumber numberWithInt:0]];
        [addDrink    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }
    
    if ([addDrinkGirl count] != 0) {
        addDrinkGirl  = [SearchPromotion addBillData:addDrinkGirl andPop:NO];
         [self setdataPrinter:[addDrinkGirl mutableCopy] andTotal:totalDrinkGirl and:3];
        [addDrinkGirl    insertObject:[NSNumber numberWithFloat:totalDrinkGirl] atIndex:0];
        
        countGood += [addDrinkGirl count] -1;
    }
    else{
        [self setdataPrinter:[addDrinkGirl mutableCopy] andTotal:0 and:3];
        [addDrinkGirl    addObject:[NSNumber numberWithInt:0]];
        [addDrinkGirl    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }
    
    if ([addPro count] != 0) {
        [self setdataPrinter:[addPro mutableCopy] andTotal:totalPro and:4];
        [addPro    insertObject:[NSNumber numberWithFloat:totalPro] atIndex:0];
        
        countGood += [addPro count] -1;
    }
    else{
        [self setdataPrinter:[addPro mutableCopy] andTotal:0 and:4];
        [addPro    addObject:[NSNumber numberWithInt:0]];
        [addPro    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }
    
    if ([addDiscount count] != 0) {
        [self setdataPrinter:[addDiscount mutableCopy] andTotal:totalDiscount and:5];
        [addDiscount    insertObject:[NSNumber numberWithFloat:totalDiscount] atIndex:0];
        
        countGood += [addDiscount count] -1;
    }
    else{
        [self setdataPrinter:[addDiscount mutableCopy] andTotal:0 and:5];
        [addDiscount    addObject:[NSNumber numberWithInt:0]];
        [addDiscount    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }

    if ([addCoupon count] != 0) {
        [self setdataPrinter:[addCoupon mutableCopy] andTotal:totalCoupon and:6];
        [addCoupon    insertObject:[NSNumber numberWithFloat:totalCoupon] atIndex:0];
        countGood += [addCoupon count] -1;
    }
    else{
        [self setdataPrinter:[addCoupon mutableCopy] andTotal:0 and:6];
        [addCoupon    addObject:[NSNumber numberWithInt:0]];
        [addCoupon    addObject:@{@"data":AMLocalizedString(@"ไม่มีรายการ", nil)}];
    }
    totalAll1 = totalCoupon;
    for (NSString *tem in [dataTotal allValues]) {
        totalAll1  += [tem floatValue];
    }
    
    NSDictionary *dataCash = @{@"title":[NSNumber numberWithFloat:8],
                               @"total":[NSNumber numberWithFloat:totalPaidCash],
                               @"count":[NSNumber numberWithFloat:countPaidCash]};
    NSDictionary *dataCredit = @{@"title":[NSNumber numberWithFloat:9],
                                 @"total":[NSNumber numberWithFloat:totalPaidCard],
                                 @"count":[NSNumber numberWithFloat:countPaidCard]};
    
    self.selectedIndexPaths = [NSMutableArray arrayWithArray:@[
                      addFoodBill,
                      addDrinkGirl,
                      addDrink,
                      addPro,
                      addDiscount,
                      addCoupon,
                      @[[NSNumber numberWithFloat:totalServiceCharge]],
                      @[[NSNumber numberWithFloat:totalVat]],
                      @[[NSNumber numberWithFloat:totalAll1]],
                      dataCash,
                      dataCredit
                      ]
                  ];
}

-(void) setdataPrinter:(NSMutableArray *)data andTotal:(float)total and:(int )title{
    
    [dataBillPrint addObject:@[@"titleOrder",@{@"title":[self getTitleSales:title],
                                               @"total":[NSNumber numberWithFloat:total],
                                               @"count":[NSNumber numberWithFloat:[data count]]}
                               ]];
    if ([data count] != 0) {
        for (NSDictionary *dic in data) {
            [dataBillPrint addObject:@[@"order",dic]];
        }
    }
}
-(NSString *) getTitleSales:(NSInteger)num{
    switch (num) {
        case 1:
            return  AMLocalizedString(@"ยอดขายอาหาร", nil);
            break;
        case 2:
            return  AMLocalizedString(@"เด็กดริ้งค์", nil);
            break;
        case 3:
            return  AMLocalizedString(@"ยอดขายเครื่องดื่ม", nil);
            break;
        case 4:
            return  AMLocalizedString(@"การใช้โปรโมชั่น", nil);
            break;
        case 5:
            return  AMLocalizedString(@"ขอส่วนลด", nil);
            break;
        case 6:
            return  AMLocalizedString(@"คูปอง", nil);
            break;
        case 7:
            return  AMLocalizedString(@"ค่าบริการ", nil);
            break;
        case 8:
            return  AMLocalizedString(@"ยอดภาษีรวม", nil);
            break;
        case 9:
            return  AMLocalizedString(@"ยอดรวมทั้งหมด", nil);
            break;
        case 10:
            return  AMLocalizedString(@"เงินสด", nil);
            break;
        case 11:
            return  AMLocalizedString(@"บัตรเครดิต", nil);
            break;
        default:
            return @"";
            break;
    }
}
-(UIColor *) getColorCell:(NSInteger)num{
    switch (num) {
        case 1:
            return  [UIColor ht_carrotColor];
            break;
        case 2:
            return  [UIColor ht_belizeHoleColor];
            break;
        case 3:
           return  [UIColor ht_alizarinColor];
            break;
        case 4:
            return  [UIColor ht_pinkRoseColor];
            break;
        case 5:
            return  [UIColor ht_sunflowerColor];
            break;
        case 6:
            return  [UIColor ht_nephritisColor];
            break;
        case 7:
        case 8:
        case 9:
        case 10:
        case 11:
            return [UIColor ht_cloudsColor];
            break;
        default:
            return [UIColor whiteColor];
            break;
    }
}
#pragma mark - UITextField
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    UITableViewController* contentViewController = [[UITableViewController alloc] init];
    contentViewController.title = AMLocalizedString(@"เลือกวันที่ ", nil);
    contentViewController.view.layer.masksToBounds = YES;
    contentViewController.preferredContentSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height);
    contentViewController.modalInPopover = NO;
    [contentViewController.tableView setTag:999];
    [contentViewController.tableView setDataSource:self];
    [contentViewController.tableView setDelegate:self];
    
    UINavigationController* contentView = [[UINavigationController alloc] initWithRootViewController:contentViewController];
    [contentView.navigationBar setBarTintColor:[UIColor colorWithRed:19.0/255.0 green:23.0/255.0 blue:68.0/255.0 alpha:1]];
    UIImage *image = [[UIImage imageNamed:@"close"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 40, 40)];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(btnClosePop:)];
    contentViewController.navigationItem.rightBarButtonItem = rightBarButtonItem;
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentView];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
}
-(IBAction)btnClosePop:(id)sender{
    [popoverController dismissPopoverAnimated:YES];
}


#pragma mark - Actions
- (void)reloadTableViewWithData
{
    [self sortDataOrdersInSection];
    
    // Refresh data not scrolling
    //    [self.tableView refreshData];
    
    [self.mtbvCashSalesReport refreshDataWithScrollingToIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"2 * numberOfRowsInSection %ld",[self.selectedIndexPaths count]);
    if (tableView.tag == 999) {
        return [listDateTime count];
    }
    else
        return [self.selectedIndexPaths count];
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"3 * numberOfSubRowsAtIndexPath %lu : %lu : %lu ",indexPath.section,indexPath.row,[self.selectedIndexPaths[indexPath.row] count] - 1);
    NSInteger count = [self.selectedIndexPaths[indexPath.row] count]-1;
    return count;
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}
- (UITableViewCell *)tableView:(SKSTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        NSLog(@"cellForRow Section: %ld, Row:%ld, Subrow:%ld",indexPath.section,indexPath.row,(long)indexPath.subRow);
        if (tableView.tag == 999) {
            NSString *CellIdentifier = @"cell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
            NSInteger row = indexPath.row;;
            DateTimeClss *dayDate = [[DateTimeClss alloc] init];
            dayDate = listDateTime[row];
            [cell.textLabel setText:dayDate.textDate];
            UIImage *cellImage = [UIImage imageNamed:@"cic_close"];
            BOOL isNot = [dateTimeSubmit containsObject:dayDate.dateTime];
            if (isNot) {
                cellImage = [UIImage imageNamed:@"send"];
            }
            [cell.imageView setImage:cellImage];
            [cell.imageView setContentMode:UIViewContentModeScaleAspectFit];
            return cell;
        }
        else{
            NSInteger row = indexPath.row;
            NSString *CellIdentifier = [self gerIdentifine:row];     //@"SKSTableViewCell";
            SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell){
                cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            
            id data = self.selectedIndexPaths[row];
            NSNumber *num2;
            NSNumber *count;
                
            if ([data isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dic = (NSDictionary *)data;
                num2 = [NSNumber numberWithFloat:[dic[@"total"] floatValue]];
                count = [NSNumber numberWithFloat:[dic[@"count"] floatValue]];
            }
            else{
                num2 = [NSNumber numberWithDouble:[[data firstObject] floatValue]];
                count = [NSNumber numberWithFloat:[data count] - 1];
                
                if ([num2 intValue] == 0) {
                    if ([data count] == 2) {
                        int subRow =  (int)indexPath.subRow;
                        NSDictionary *dataOrder = data[subRow+1];
                        bool bl = ([dataOrder[@"data"]isEqual:AMLocalizedString(@"ไม่มีรายการ", nil)] ? true:false);
                        if (bl) {
                            count = @(count.intValue - 1);
                        }
                    }
                }
            }
            
            row += 1;
            [cell.selesCell setText:[NSString conventCurrency:[num2 stringValue]]];
            [cell.mCount setText:[NSString stringWithFormat:@" ( %d )",[count intValue]]];
            [cell.titleCell setText:[self getTitleSales:row]];
            [cell setBackgroundColor:[self getColorCell:row]];
            
            if (row > 6) {
                cell.expandable = NO;
                [cell.titleCell setTextColor:[UIColor colorWithRed:0.298 green:0.298 blue:0.298 alpha:1.0]];
                [cell.selesCell setTextColor:[UIColor colorWithRed:0.298 green:0.298 blue:0.298 alpha:1.0]];
                [cell.mCount setTextColor:[UIColor colorWithRed:0.298 green:0.298 blue:0.298 alpha:1.0]];
            }
            else{
                cell.expandable = YES;
                [cell.titleCell setTextColor:[UIColor whiteColor]];
                [cell.selesCell setTextColor:[UIColor whiteColor]];
                [cell.mCount setTextColor:[UIColor whiteColor]];
            }
            return cell;
        }
    }
    @catch (NSException *exception) {
        DDLogError(@"exception  SKSTableViewCell :%@",exception);
    }
}

- (UITableViewCell *)tableView:(SKSTableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
        NSLog(@"cellForSub Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
        
        NSString *cellIden = kCellBillSale;
        SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIden];
        
        if (!cell)
            cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIden];
        
        NSDictionary *dataOrder = self.selectedIndexPaths[indexPath.row][indexPath.subRow];
        bool bl = ([dataOrder[@"data"]isEqual:AMLocalizedString(@"ไม่มีรายการ", nil)] ? true:false);
        NSString *sales ;
        NSString *nameFood;
        NSString  *price = @"";
        if (bl) {
            sales = @"-";
            nameFood = AMLocalizedString(@"ไม่มีรายการ", nil);
        }
        else{
            NSDictionary *addFoodBill = dataOrder[@"dataorder"];
            if ([addFoodBill count] != 0) {
                int cu = [addFoodBill[@"price"]  floatValue] * [addFoodBill[@"n"] intValue];
                sales = [@(cu) stringValue];
                
                NSString *typeDrink = [self getTypeDrink:dataOrder[@"dataorder"][@"drinkgirl_type"]];
                if (typeDrink == nil) {
                    nameFood = [NSString stringWithFormat:@"%@",dataOrder[@"data"][@"name"]];
                    price = [NSString stringWithFormat:@"( %@ * %@ )",addFoodBill[@"price"],addFoodBill[@"n"]];
                }
                else{
                    nameFood = [NSString stringWithFormat:@"%@",dataOrder[@"data"][@"name"]];
                    price = [NSString stringWithFormat:@"( %@ )",typeDrink];
                }
            }
            else if (dataOrder[@"promotion_id"] != nil && ![dataOrder[@"promotion_id"] isEqual:@"0"]){
                NSString *idPro = [NSString stringWithFormat:@"id_%@",dataOrder[@"promotion_id"]];
                int cu = [dataOrder[@"price"]  floatValue] * [dataOrder[@"n"] intValue];
                sales = [@(cu) stringValue];
                
                NSDictionary *tempPro = dataPromotion[idPro];
                if ([tempPro count] != 0) {
                    nameFood = [NSString stringWithFormat:@"%@",tempPro[@"name"]];
                    price = [NSString stringWithFormat:@"( %@ * %@ )",dataOrder[@"price"],dataOrder[@"n"]];
                }
            }
            else if (dataOrder[@"promotion_id"] != nil && [dataOrder[@"promotion_id"] isEqual:@"0"]){
                int typePro = [dataOrder[@"type"] intValue];
                int count   = [dataOrder[@"n"] intValue];
                NSString *totalPro = [NSString stringWithFormat:@"%@ ",dataOrder[@"total"]];
                if (typePro == 4){
                    float price1  = [dataOrder[@"price"] floatValue];
                    nameFood = [NSString stringWithFormat:@"%@ %.02f",AMLocalizedString(@"คูปอง", nil),fabs(price1)];
                    price = [NSString stringWithFormat:@"( %d * %.02f )",count,fabs(price1)];
                    float tot   = [dataOrder[@"total"] floatValue];
                    sales = [NSString stringWithFormat:@"%.02f",fabs(tot)];
                }
                else{
                nameFood = [NSString stringWithFormat:@"%@",AMLocalizedString(@"ขอส่วนลด", nil)];
                    price = [NSString stringWithFormat:@"( %d )",count];
                    sales = [NSString stringWithFormat:@"%@",totalPro];
                }
            }
        }
        [cell.mLabel setText:nameFood];
        [cell.mCount setText:[NSString  conventCurrency:sales]];
        [cell.mCountAndPrice setText:price];
        cell.accessoryType = UITableViewCellAccessoryNone;
        [cell setBackgroundColor:[UIColor ht_cloudsColor]];
        return cell;
    }
    @catch (NSException *exception) {
        DDLogError(@"exception  UITableViewCell :%@",exception);
    }
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"UITableView Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
    if (tableView.tag == 999 ) {
        [loadProcess.loadView showWaitProgress];
        [self btnClosePop:nil];
        NSInteger row = indexPath.row;
        DateTimeClss *date = [[DateTimeClss alloc] init];
        date = listDateTime[row];
        [self.mDate setText:date.textDate];
        [self loadDataReportSeles:date.dateTime];
    }
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"SKSTableView Section: %ld, Row:%ld, Subrow:%ld", (long)indexPath.section, (long)indexPath.row, (long)indexPath.subRow);
}

-(NSMutableArray *) getDateTime{
    NSMutableArray *listDate = [NSMutableArray array];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-dd";
    NSDate *yesterday = [NSDate date];
    int daysToAdd = -1;
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    for (int i = 0; i < 31; i++) {
        NSString *Date = [dateFormatter stringFromDate:yesterday];
        NSString *time = [Time covnetTimeDMY1:Date];
        DateTimeClss *yesterdayDate = [[DateTimeClss alloc] init];
        yesterdayDate.textDate = time;
        yesterdayDate.dateTime = Date;
        yesterdayDate.date = yesterday;
        [listDate addObject:yesterdayDate];
        NSLog(@"Yesterday: %@ and %@", yesterday,Date);
        yesterday = [gregorian dateByAddingComponents:components toDate:yesterday options:0];
        
        
    }
    return listDate;
}
-(NSString *)gerIdentifine:(NSInteger )num{
    switch (num) {
        case 0:
        case 5:
        case 9:
        case 10:
            return @"CellFoodSeles";
        break;
        case 1:
            return @"CellDrinkSeles";
        break;
        case 2:
            return @"CellDrinkGirlsSeles";
        break;
        case 3:
            return @"CellProSeles";
        break;
        case 4:
            return @"CellDiscountSeles";
        break;
        case 7:
            return @"CellTotalVat";
            break;
        case 6:
        case 8:
            return @"CellTotalSeles";
            break;
        
        default:
            return @"";
            break;
    }
}

-(NSMutableArray *)sortPromotionOrders:(NSDictionary *)datapro{
    
    NSMutableArray *promotionBill = [NSMutableArray array];
    NSMutableArray *pendingPrders = [NSMutableArray arrayWithArray:self.dataAllZone[@"pending_orders"]];
    NSDictionary *promotionFloder  = [NSDictionary dictionaryWithObject:datapro[@"promotion_folder"]];
    NSArray *promotionFloder1  = [promotionFloder allValues];
    NSDictionary *promotionInfo = [NSDictionary dictionaryWithObject:(NSDictionary *)datapro[@"promotion_info"]];
    NSArray *promotionPool = [NSArray arrayWithArray:datapro[@"promotion_pool"]];
    NSDictionary *promotionApproval =  [NSDictionary dictionaryWithObject:([datapro[@"promotion_approval"] count] != 0)?datapro[@"promotion_approval"]:nil];
    
    [SearchPromotion setStaticValel:promotionInfo];
    [SearchPromotion setStaticValel:1 and:2];
    NSMutableArray *promotionList = (NSMutableArray *)[SearchPromotion getPromotionPoolAttachBill:promotionPool
                                                                                              and:(NSMutableArray *)promotionFloder1
                                                                                              and:promotionInfo and:promotionApproval];
    NSArray *addFoodBillPENDING  = [SearchPromotion addBillData:pendingPrders andPop:NO];  // 9999
    if ([promotionList count] != 0 && ([pendingPrders count] != 0)) {
        if ([addFoodBillPENDING count] != 0) {
            for (NSDictionary *dic in promotionList) {
                NSString *key = dic[@"id"];
                NSString *value = @"promotion_id = %@";
                NSArray *data = [NSDictionary searchDictionary:key andvalue:value andData:[addFoodBillPENDING valueForKey:@"dataorder"]];
                if (data == nil) {
                    [promotionBill addObject:dic];
                }
            }
        }
    }
    else if ( ([pendingPrders count] == 0)){
        [promotionBill addObjectsFromArray:promotionList];
    }
    return promotionBill;
}
#pragma mark - IBActions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

//- (IBAction)clickPrintBill:(id)sender {
//    
////    PrintQuenceTableViewController *printQuenceTable = [[PrintQuenceTableViewController alloc] init];
////    [printQuenceTable initNetworkCommunication:^(bool completion) {
////        if (completion) {
////            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//////                [self loadDataPrintCheckBillCash:sender];
////            });
////        }
////        else{
////            [self dismissViewControllerAnimated:YES completion:nil];
////        }
////    }];
//}


-(NSString *)getTypeDrink:(NSString *)ind{
    if ([ind isEqual:@"1"]) {
        return kDrinkOne;
    }
    else if ([ind isEqual:@"2"]) {
        return kDrinkTwo;
    }
    else if ([ind isEqual:@"3"]) {
        return kDrinkThree;
    }
    else{
        return nil;
    }
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

     if ([[segue identifier] isEqualToString:kPrintBillReport]) {
         PrintSelesViewController *viewPrint = (PrintSelesViewController *)[segue destinationViewController];
         
         NSInteger male = 0;
         NSInteger woman = 0;
         NSInteger vat = 0;
         NSInteger cash = 0;
         NSInteger credit = 0;
         NSInteger diccount = 0;
         NSInteger coupon = 0;
         
         int countCash = 0;
         int countCredit = 0;
         int countdiccount = 0;
         int countcoupon = 0;
         
         for (NSDictionary *tem in dataBill) {
             vat  += [[NSString checkNull:tem[@"vat"]] integerValue];
             male += [[NSString checkNull:tem[@"n_male"]] integerValue];
             woman += [[NSString checkNull:tem[@"n_female"]] integerValue];
             
             int total =  [[NSString checkNull:tem[@"total"]] intValue];
             
             if ([tem[@"type"] isEqualToString:@"1"]) {  // type = 1  เงินสด  , 2 = เครดิต
                 cash += total;
                 countCash ++;
             }
             else{
                 credit += total;
                 countCredit ++;
             }
         }
         
         NSArray *listDiscount =  self.selectedIndexPaths[4];
         diccount = [[listDiscount firstObject] intValue];
         id strDis = [listDiscount[1] objectForKey:@"data"];
         if (![strDis isKindOfClass:[NSString class]]) {
             countdiccount = (int)[listDiscount count] - 1;
         }
         NSArray *listCoupon = self.selectedIndexPaths[5];
         coupon = [[listCoupon firstObject] intValue];
         id strCou = [listCoupon[1] objectForKey:@"data"];
         if (![strCou isKindOfClass:[NSString class]]) {
             countcoupon = (int)[listCoupon count] -1;
         }
         NSMutableArray *listCum = [NSMutableArray array];
         
         NSInteger priceAccumelated = 0;
         NSInteger priceSales = 0;
         float voidbill_counter = 0;
         priceAccumelated = [[NSString checkNullCurrency:self.dataAllZone[@"summary"][@"total_month"]] integerValue];
         priceSales = [[NSString checkNullCurrency:self.dataAllZone[@"summary"][@"total_year"]] integerValue];
         voidbill_counter = [NSString checkNullConvertFloat:self.dataAllZone[@"voidbill_counter"]];
         
         NSInteger countBill = [Check checkNull:dataTotal]?0:[dataTotal count];
         listCum = [NSMutableArray arrayWithArray:@[@[@"title",[NSNumber numberWithInt:3]],
                                                    @[@"num",[NSNumber numberWithInt:0],[NSNumber numberWithInteger:priceAccumelated]],
                                                    @[@"num",[NSNumber numberWithInt:1],[NSNumber numberWithInteger:priceSales]],
                                                    @[@"line"],
                                                    @[@"title",[NSNumber numberWithInt:4]],
                                                    @[@"num",[NSNumber numberWithInt:2],[NSNumber numberWithInteger:countGood]],
                                                    @[@"num",[NSNumber numberWithInt:3],[NSNumber numberWithInteger:countBill]],
                                                    @[@"num",[NSNumber numberWithInt:10],[NSNumber numberWithInteger:voidbill_counter]],
                                                    @[@"num",[NSNumber numberWithInt:11],[NSNumber numberWithInteger:[[dataLogEditBill allValues] count]]],
                                                    @[@"num",[NSNumber numberWithInt:4],[NSNumber numberWithInteger:(male + woman)]],
                                                    @[@"num",[NSNumber numberWithInt:5],[NSNumber numberWithInteger:male]],
                                                    @[@"num",[NSNumber numberWithInt:6],[NSNumber numberWithInteger:woman]],
                                                    @[@"num",[NSNumber numberWithInt:7],[NSNumber numberWithInteger:(totalAll1 - vat)]],
                                                    @[@"num",[NSNumber numberWithInt:8],[NSNumber numberWithInteger:vat]],
                                                    @[@"num",[NSNumber numberWithInt:9],[NSNumber numberWithInteger:totalAll1]],
                                                    @[@"title",[NSNumber numberWithInt:5]],
                                                    @[@"titleOrder",@[[NSNumber numberWithInt:6],[NSNumber numberWithInt:countCash],[NSNumber numberWithInteger:cash]]],
                                                    @[@"titleOrder",@[[NSNumber numberWithInt:7],[NSNumber numberWithInt:countCredit] ,[NSNumber numberWithInteger:credit]]],
                                                    @[@"titleOrder",@[[NSNumber numberWithInt:9],[NSNumber numberWithInt:countdiccount],[NSNumber numberWithInteger:diccount]]],
                                                    @[@"titleOrder",@[[NSNumber numberWithInt:10],[NSNumber numberWithInt:countcoupon],[NSNumber numberWithInteger:coupon]]],
                                                    @[@"lineEnd"],
                     ]];
         
         [viewPrint setDataCumulativeSales:listCum];
         [viewPrint setDataPrintList:dataBillPrint];
         [viewPrint setDataPromotion:dataPromotion];
     }
 }

@end
