//
//  CouponViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 10/1/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import "CouponViewController.h"
#import "CustomNavigationController.h"

#import "NSString+GetString.h"
#import "NSDictionary+DictionaryWithString.h"
#import "Check.h"
#import "LoadProcess.h"
#import "LocalizationSystem.h"
#import "Constants.h"
#import "LoadMenuAll.h"
#import "Member.h"

@interface CouponViewController (){
    CustomNavigationController *vc;
    LoadProcess *loadProcess;
    
    NSDictionary *detailTable;
}
@property (nonatomic ,strong) Member *member;
@end

@implementation CouponViewController
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    vc = (CustomNavigationController *)self.parentViewController;
    detailTable = [NSDictionary dictionaryWithObject:vc.dataListSingBill];
    loadProcess = [LoadProcess sharedInstance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cilckSubmitCoupon:(id)sender {
    NSString *value   = self.mPrice.text;
    NSString *n       = self.mCount.text;
    
    if ([Check checkNull:value] || [Check checkNull:n] || [n isEqualToString:@"0"] || [value isEqualToString:@"0"]) {
        [loadProcess.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาป้อนจำนวน", nil)];
    }
    else{
        [self loadDataSubmitCoupon];
    }
}
-(void)loadDataSubmitCoupon{
   [loadProcess.loadView showWithStatus];
    NSArray *arr = [LoadMenuAll requirebillID:detailTable[@"id"]];
    
    NSString *value   = self.mPrice.text;
    NSString *n       = self.mCount.text;
    NSString *idBill  = [NSString  checkNull:arr[1]];
    NSString *idTable = [NSString checkNull:arr[0]];

    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"table_usage_id":idTable,
                                 @"bill_id":idBill,
                                 @"value":value,
                                 @"n":n
                                 };
    NSString *url = [NSString stringWithFormat:GET_SUBMIT_COUPON,self.member.nre];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        BOOL staus = [dictionary[@"status"] boolValue];
        if (staus) {
            [self cilckBack:nil];
        }
        else
            [loadProcess.loadView showError];
    }];
}
- (IBAction)cilckBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
