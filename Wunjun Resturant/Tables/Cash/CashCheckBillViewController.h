//
//  CashCheckBillViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIStoryboard+Main.h"

@interface CashCheckBillViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mContainerTableBill;

//@property (nonatomic,strong) NSDictionary *dataListSingBill;
//@property (nonatomic,strong) NSString *billTitle;
//@property (nonatomic,strong) NSString *countAmount;
- (IBAction)btnBack:(id)sender;

@end
