//
//  CashSalesReportViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKSTableView.h"

@interface CashSalesReportViewController : UIViewController<SKSTableViewDelegate,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITextField *mDate;
@property (nonatomic ,weak) IBOutlet SKSTableView *mtbvCashSalesReport;

@property (nonatomic ,strong)  NSDictionary *dataAllZone;

- (IBAction)menuButtonTapped:(id)sender;
//- (IBAction)clickPrintBill:(id)sender;
@end
