//
//  CashCheckBillViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashCheckBillViewController.h"
#import "THSegmentedPager.h"
#import "CashSpecifyTheAmount.h"
#import "CustomNavigationController.h"
#import "TableAllZoneCollectionView.h"


@interface CashCheckBillViewController(){
    CustomNavigationController *vc;
}

@end


@implementation CashCheckBillViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    vc = (CustomNavigationController *)self.parentViewController;
    self.navigationItem.title = vc.billTitle;
    
    [self createViewTableZone];
}

-(void) createViewTableZone {
    
//    UIStoryboard *storyboard = self.storyboard;
//    
//    THSegmentedPager *pager = [storyboard instantiateViewControllerWithIdentifier:@"THSegmentedPager"];
//    NSMutableArray *pages = [NSMutableArray new];
//    
//    for (int i = 0; i < 2; i++) {
//        CashSpecifyTheAmount *_cashSpecifyTheAmount = [pager.storyboard instantiateViewControllerWithIdentifier:@"CashSpecifyTheAmount"];
//        [pages addObject:_cashSpecifyTheAmount];
//        [_cashSpecifyTheAmount setViewTitle:(i == 0) ?AMLocalizedString(@"เงินสด", nil) :AMLocalizedString(@"บัตรเครดิต", nil)];
//        
//        [_cashSpecifyTheAmount setCountAmount:vc.countAmount];
//        [_cashSpecifyTheAmount setBillTitle:vc.billTitle];
//        [_cashSpecifyTheAmount setSelfClass:vc.selfClass];
//        [_cashSpecifyTheAmount setTableDataBillView:vc.tableDataBillView];
//    }
//    
//    [pager setPages:pages];
    
    CashSpecifyTheAmount *_cashSpecifyTheAmount = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashSpecifyTheAmount"];
    [_cashSpecifyTheAmount setViewTitle:AMLocalizedString(@"เงินสด", nil)];
    
    [_cashSpecifyTheAmount setCountAmount:vc.countAmount];
    [_cashSpecifyTheAmount setBillTitle:vc.billTitle];
    [_cashSpecifyTheAmount setSelfClass:vc.selfClass];
    [_cashSpecifyTheAmount setTableDataBillView:vc.tableDataBillView];
    
    _cashSpecifyTheAmount.view.frame = self.mContainerTableBill.bounds;
    [self.mContainerTableBill addSubview:_cashSpecifyTheAmount.view];
    [self addChildViewController:_cashSpecifyTheAmount];
    [_cashSpecifyTheAmount didMoveToParentViewController:self];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
