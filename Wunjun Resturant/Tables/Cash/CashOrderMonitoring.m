//
//  CashOrderMonitoring.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashOrderMonitoring.h"
#import "OrdersListViewController.h"

#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"

@interface CashOrderMonitoring(){
    
}

@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end
@implementation CashOrderMonitoring


#pragma mark - UIViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setViewTab];
    [self setTitle:AMLocalizedString(@"รายการทั้งหมด", nil)];
}
-(void)setViewTab{
//    
    OrdersListViewController *pager = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"OrdersListView"];
    [pager setRole:@"cashier"];
    [pager setBlClikeChecker:YES];
    [pager setKitchenId:@"0"];
    [pager setKitchenName:@"ครัว"];
    
    pager.view.frame = self.viewContainer.bounds;
    [self.viewContainer addSubview:pager.view];
    [self addChildViewController:pager];
    [pager didMoveToParentViewController:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([(NSObject *)self.slidingViewController.delegate isKindOfClass:[MEDynamicTransition class]]) {
        MEDynamicTransition *dynamicTransition = (MEDynamicTransition *)self.slidingViewController.delegate;
        if (!self.dynamicTransitionPanGesture) {
            self.dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:dynamicTransition action:@selector(handlePanGesture:)];
        }
        
        [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
        [self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
    } else {
        [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}
#pragma mark - IBActions

- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}

@end
