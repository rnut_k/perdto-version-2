//
//  CashTableStatus.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPager.h"
#import "UIImageView+AFNetworking.h"
#import "AFNetworking.h"
#import "Member.h"
#import "Constants.h"
#import "DeviceData.h"
#import "MenuDB.h"
#import "LoadProcess.h"
#import "ECSlidingViewController.h"
#import "UIStoryboard+Main.h"

@interface CashTableStatus : UIViewController<ECSlidingViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>{

}

@property (strong, nonatomic) IBOutlet UIView *viewContainer;

@property (nonatomic,strong ) NSMutableArray *listNotiCheckBill;
@property (nonatomic,strong)  NSString *role;
@property (nonatomic) BOOL notBegin;

- (IBAction)menuButtonTapped:(id)sender;
@end
