//
//  CashTableNotCheckBill.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/12/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashTableNotCheckBill.h"

@implementation CashTableNotCheckBill
-(void)viewDidLoad{
    [super viewDidLoad];
    
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dataNotCheckBill count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIden  =  @"Cell";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:cellIden];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIden];
    }
    NSDictionary *data = self.dataNotCheckBill[indexPath.row];
    [cell.textLabel setText:[NSString stringWithFormat:@"%ld.  %@ %@",(long)[indexPath row],data[@"label"],AMLocalizedString(@"โต๊ะ", nil)]];
    return cell;
}

@end
