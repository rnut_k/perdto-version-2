//
//  CashTakeawayTableViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 6/26/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashTakeawayTableViewController.h"

#import "Time.h"
#import "CellTableViewAll.h"
#import "SelectBill.h"
#import "NSString+GetString.h"

@interface CashTakeawayTableViewController (){
   LoadProcess *loadProgress;
    
NSArray *listDataBill;
}

@end


@implementation CashTakeawayTableViewController
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(Member *)member{
    if(!_member){
        _member = [Member getInstance];
    }
    return _member;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = YES;
    loadProgress = [LoadProcess sharedInstance];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self loadDataCreateBill];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)loadDataCreateBill{
    
    NSDictionary *parameters = @{@"code":self.member.code};
    
    NSString *url = [NSString stringWithFormat:GET_ORDER_TO_GO_LIST,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue]){
            NSDictionary *data = [NSDictionary dictionaryWithObject:json];
            listDataBill = [NSArray arrayWithArray:data[@"data"]];
            [self.mTableTakeaway reloadData];
        }
    }];
}

 #pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [listDataBill count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *iden = @"cell";
    CellTableViewAll *cell = [tableView dequeueReusableCellWithIdentifier:iden forIndexPath:indexPath];
    if (!cell) {
        cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:iden];
    }
    
    NSDictionary *data = listDataBill[indexPath.row];
    [cell.mTimeDrink setText:[Time conventTime:[NSString checkNull:data[@"open_datetime"]]]];
    NSString *total = [NSString conventCurrency:[NSString checkNull:data[@"total"]]];
    [cell.mTotalFood setText:total];
    [cell.mCountOrder setText:[NSString checkNull:data[@"no"]]];
    
    [cell setBackgroundColor:[UIColor clearColor]];
    return cell;
}
-(void)setLine:(UIView *)obj{
    obj.layer.cornerRadius = 10.0f;
    obj.layer.borderWidth = 1.0f;
    obj.layer.borderColor = [UIColor ht_silverColor].CGColor;
    obj.clipsToBounds = NO;
    obj.backgroundColor = [UIColor whiteColor];
    obj.layer.masksToBounds = YES;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"index %ld",indexPath.row);
    NSDictionary *data = listDataBill[indexPath.row];
    self.isCashierTake = NO;
    [self loadDataOpenTable:data];
}

- (IBAction)btnCheckBill:(id)sender{
    NSLog(@"btnCheckBill");
    UIButton *btn = (UIButton *)sender;
    CellTableViewAll *cell = (CellTableViewAll *)[btn.superview superview];
    NSIndexPath *indexPath = [self.mTableTakeaway indexPathForCell:cell];
    self.isCashierTake = YES;
    NSDictionary *data = listDataBill[indexPath.row];
    [self loadDataOpenTable:data];
}
- (IBAction)btnCreateBill:(id)sender{
    NSLog(@"btnCheckBill");
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"สั่งกลับบ้าน", nil)
                                                            message:AMLocalizedString(@"คุณต้องการสั่งอาหารกลับบ้าน", nil)
                                                           delegate:self
                                                  cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                                  otherButtonTitles:AMLocalizedString(@"ใช่", nil), nil];
    [alertView show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
       [self loadDataOpenTable];
    }
}
-(void)loadDataOpenTable{
    [loadProgress.loadView showWithStatus];
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"in",
                                 @"table_data":@{
                                         @"id":@"0",
                                         @"n_male":@"1",
                                         @"n_female":@"1"
                                         }
                                 };
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,self.member.nre];    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        NSDictionary *_data = dictionary[@"data"];
        if ([dictionary[@"status"] boolValue]) {
            [SelectBill setCheckOpenTable:NO];  // no = เปิดโต็ะ
            int num = 0;  // take staff
            if (self.isCashierTake) {
                num = 1;
            }
            
            [SelectBill setIsTake:num];
            [SelectBill setIscashTableStatus:NO];
            [SelectBill setIsCashier:NO];
            NSDictionary *dataUsage = [NSDictionary dictionaryWithObject:_data[@"usage"]];
            NSDictionary *dataTable = (NSDictionary *)[NSDictionary dictionaryWithObject:[dataUsage allValues][0]];
            [SelectBill selectBill:self andData:_data andDataTable:dataTable fromRole:nil];
        }
        else
            [loadProgress.loadView showError];
    }];
}
-(void)loadDataOpenTable:(NSDictionary *)data{
    [loadProgress.loadView showWithStatus];
    NSString *idTable = data[@"id"];
    NSString *billID = data[@"bill_id"];
    
    NSDictionary *parameters = @{@"code":self.member.code,
                                 @"action":@"in",
                                 @"table_id":idTable};
    
    NSString *getBill = [NSString stringWithFormat:GET_BILL,self.member.nre,@"0"];
    NSString *url = [NSString stringWithFormat:@"%@/%@",getBill,billID];
    
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        NSDictionary *_data = json[@"data"];
        if ([_data count] != 0) {
            NSLog(@"\n loadDataOpenTable");
            [SelectBill setCheckOpenTable:YES];
            int num = 0; // take staff
            if (self.isCashierTake) {
                num = 1;
            }
            
            [SelectBill setIsCashier:NO];
            [SelectBill setIscashTableStatus:NO];
            [SelectBill setIsTake:num];
            [SelectBill selectBill:self andData:_data andDataTable:data fromRole:nil];
        }
    }];
}
@end
