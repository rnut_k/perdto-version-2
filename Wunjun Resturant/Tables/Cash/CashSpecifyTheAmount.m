//
//  CashSpecifyTheAmount.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashSpecifyTheAmount.h"
#import "CustomNavigationController.h"
#import "CashTableStatus.h"
#import "AppDelegate.h"
#import "UIViewController+ECSlidingViewController.h"
#import "PrintReceiptViewController.h"
#import "PrintQuenceTableViewController.h"

#import "PopupView.h"
#import "Constants.h"
#import "Member.h"
#import "NSString+GetString.h"
#import "LoadMenuAll.h"
#import "LoadProcess.h"
#import "UIColor+HTColor.h"
#import "Text.h"

@interface CashSpecifyTheAmount(){
    PrintQuenceTableViewController *printQuenceTable;
    PopupView  *popupView;
    LoadProcess *loadProgress;
    
    int inputNum;
    
    BOOL isCash;
}
@property (nonatomic ,strong) PrinterDB *printerDB;
@property (nonatomic ,strong) Member *member;
@end
@implementation CashSpecifyTheAmount
-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}
//-(PrinterDB *)printerDB{
//    if (!_printerDB) {   
//        _printerDB = [PrinterDB getInstance];
//    }
//     [_printerDB loadData:kPrinterDB]; 
//    return _printerDB;
//}

-(void)viewDidLoad{
    [super viewDidLoad];
    inputNum = 0;
    isCash = YES;
    
    loadProgress  = [LoadProcess sharedInstance];
    [loadProgress.loadView showWaitProgress];
    
    [self.mNumTable setText:self.billTitle];
    float num = [self.countAmount floatValue];
    NSString *total = [NSString stringWithFormat:@" ราคารวมทั้งหมด  %@",[Text numberFormatt:num]];
    [self.mTotal setText:total];
    
    
    if ([self.member.cac boolValue]) {
        if (self.isCredit) {
            [self.sizeViewCash setConstant:0.0];
            [self setViewCash:NO];
            [self setViewCredit:YES];
        }
    }
    else{
        if (self.isCredit) {
            [self.sizeViewCash setConstant:0.0];
            [self setViewCash:NO];
            [self setViewCredit:YES];
            [self.mAmountCredit setText:self.countAmount];
            [self.mAmountCredit setUserInteractionEnabled:NO];
        }
        else{
            [self.sizeViewCredit setConstant:0.0];
            [self setViewCash:YES];
            [self setViewCredit:NO];
        }
    }    
    self.navigationItem.title = self.billTitle;
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
     textField.layer.borderWidth = 2.0f;
    
    if (![textField isEqual:self.mAmountCredit]) {
        self.mAmountCredit.layer.borderColor = [UIColor ht_silverColor].CGColor;
        [self.mAmountCredit setTag:0];
        [self.mAmountReceived setTag:11];
    }
    else{
        self.mAmountReceived.layer.borderColor = [UIColor ht_silverColor].CGColor;
        [self.mAmountCredit setText:self.countAmount];
        [self.mAmountCredit setTag:11];
        [self.mAmountReceived setTag:0];
    }
   
    textField.layer.borderColor = [UIColor ht_jayDarkColor].CGColor;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
//    NSLog(@" textFieldDidEndEditing %@  ",textField.text);
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle]; // แปลง string to nsnumber
    
    NSNumber *numTotal = [nf numberFromString:self.countAmount];
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    
    if ([self.mAmountReceived isEqual:textField] || [self.mAmountCredit isEqual:textField]) {
        self.mAmountRemaininMoney.text = [@([number integerValue]-[numTotal integerValue]) stringValue];
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char,"\b");  // เช็กช่องว่าง
    if (isBackSpace == -8) {
        if (self.mAmountReceived == textField || [self.mAmountCredit isEqual:textField]){
            self.mAmountRemaininMoney.text = [@([number integerValue]-[numTotal integerValue]) stringValue];
        }
    }
    
   return YES;
}
-(IBAction)btnPrintBill:(id)sender{
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    numberFormatter.locale = [NSLocale currentLocale]; 
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.usesGroupingSeparator = YES;
    
    NSNumber *amountRec ;
    if (self.isCredit) {
      amountRec = [numberFormatter numberFromString:self.mAmountCredit.text];
    }
    else{
       amountRec = [numberFormatter numberFromString:self.mAmountReceived.text];
    }
    NSNumber *total = [numberFormatter numberFromString:self.countAmount];
    
    if ([amountRec integerValue] < [total integerValue]) {
        [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"จำนวนเงินเซ็คบิลไม่พอ", nil)];
    }
    else{
        printQuenceTable = [[PrintQuenceTableViewController alloc] init];
        [printQuenceTable initNetworkCommunication:^(bool completion) {
            if (completion) {
                [self loadDataPrintCheckBillCash:sender];
            }
            else{
               [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
}
-(void) loadDataPrintCheckBillCash:(id)sender{
    TableSingleBillViewController *tableSingleBill = (TableSingleBillViewController *)self.selfClass;
    Member *member = [Member getInstance];
    [member loadData];
    
    [CashSpecifyTheAmount getDetailCheckBill:@{@"code":member.code}
                                     andTale:tableSingleBill.dataListSingBill[@"table_id"]
                                     success:^(NSDictionary* dictionary) {
                                         
                                         NSArray *usageList;
                                         NSArray *bill_split_list;
                                         NSArray *tables_list;
                                         if (tableSingleBill.isCashierTake) {
                                             usageList = tableSingleBill.dataListSingBill[@"usage_list"];
                                             bill_split_list = tableSingleBill.dataListSingBill[@"bill_list"];
                                             tables_list = tableSingleBill.dataListSingBill[@"tables_list"];
                                         }
                                         else{
                                             usageList = dictionary[@"usage_list"];
                                             bill_split_list = dictionary[@"bill_list"];
                                             tables_list = dictionary[@"tables_list"];
                                         }
                                         
                                         NSDictionary *table_data = @{@"bill_list":bill_split_list,
                                                                      @"usage_list":usageList,
                                                                      @"tables_list":tables_list};
                                         
                                         NSDictionary *parameters = @{@"code":member.code,
                                                                      @"action":@"out",
                                                                      @"paid":self.mAmountReceived.text,
                                                                      @"table_data":table_data,
                                                                      @"type":isCash?@"1":@"2"
                                                                      };
                                         [CashSpecifyTheAmount btnPrintBill:parameters success:^(bool status) {
                                             NSLog(@"status  %d",status);
                                             if (!status) {
                                                 [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"เช็กบิลไม่สำเร็จ", nil)];
                                             }
                                             else{
                                                 UIButton *btn = (UIButton *)sender;
                                                 [btn setTag:123];
                                                 if ([self.printerDB.isOpen boolValue]) {
                                                      [self loadDataPrinter:btn];
                                                 }
                                                 else{
                                                    [self.navigationController popToRootViewControllerAnimated:YES];
                                                 }
                                             }
                                         }];
                                     }];
}
-(IBAction)btnPrintBillNotCheckBill:(id)sender{
    printQuenceTable = [[PrintQuenceTableViewController alloc] init];
    [printQuenceTable initNetworkCommunication:^(bool completion) {
        if (completion) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                [self loadDataPrinter:sender];
            });
        }
        else{
             [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}

-(IBAction)loadDataPrinter:(id)sender{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle]; // แปลง string to nsnumber
    NSNumber *amountRec = [nf numberFromString:self.mAmountReceived.text];
    NSNumber *amountRemaininMoney = [nf numberFromString:self.mAmountRemaininMoney.text];
    
    TableSingleBillViewController *tableSingleBill = (TableSingleBillViewController *)self.selfClass;
    
    PrintReceiptViewController *print = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"PrintReceiptViewController1"];
    [print setTitle:AMLocalizedString(@"ใบเสร็จ", nil)];
    [print setTableDataBillView:self.tableDataBillView];
    [print setTableSingleBillView:tableSingleBill];
    [print setIsPrintAllSubBill:YES];
    [print setIsPrintSubBill:NO];
    [print setAmountReceived:[amountRec stringValue]];
    [print setAmountRemaininMoney:[amountRemaininMoney stringValue]];
    
    UIButton *btn = (UIButton *)sender;
    if (btn.tag == 123) {
        [print setIsPrintCheckBill:YES];
    }
    
    [print setAmountReceived:self.mAmountReceived.text];
    [print setAmountRemaininMoney:self.mAmountRemaininMoney.text];
    [tableSingleBill printCheckBillView:NO];

    
    NSString *idBill = tableSingleBill.detailTable[@"id"];
    [TableSingleBillViewController resetBillSeclectPassivePro:idBill];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self  presentViewController:print animated:YES completion:nil];
        });
    });
}
+(void)btnPrintBill:(NSDictionary *)parameters  success:(void (^)(bool status))completion{
    Member *member = [Member getInstance];
    [member loadData];
    NSString *url = [NSString stringWithFormat:GET_MANAGEUSAGE_TABLE,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        bool bl = [dictionary[@"status"] boolValue];
        completion(bl);
    }];
}
+(void)getDetailCheckBill:(NSDictionary *)parameters  andTale:(NSString *)idTable success:(void (^)(NSDictionary* dictionary))completion{
    Member *member = [Member getInstance];
    [member loadData];
    NSString *url = [NSString stringWithFormat:GET_BILL_CASHIER,member.nre,idTable];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *dictionary) {
        completion(dictionary[@"data"]);
    }];
}
- (NSString *)viewControllerTitle{
    return _viewTitle ? _viewTitle : self.title;
}

- (IBAction)clickNumeric:(id)sender {
   
    if (self.mAmountReceived.tag != 11 && self.mAmountCredit.tag != 11) return;
    else if (self.isCredit) return;
    
    UIButton *num = (UIButton *)sender;
    NSString *numStr = [NSString checkNull:num.titleLabel.text];
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterDecimalStyle];
    [nf setMaximumFractionDigits:2];
    [nf setRoundingMode:NSNumberFormatterRoundDown];
    
    NSString * newString = @"";
    NSInteger recived = 0;
    NSInteger credit = 0;
    if (self.mAmountReceived.tag == 11) {
        newString = [NSString stringWithFormat:@"%@%@",self.mAmountReceived.text,numStr];
        recived = [newString integerValue];
        credit = [self.mAmountCredit.text integerValue];
    }
    else{
        newString = [NSString stringWithFormat:@"%@%@",self.mAmountCredit.text,numStr];
        credit = [newString integerValue];
        recived = [self.mAmountReceived.text integerValue];
    }
    
    newString = [@(recived + credit) stringValue];
    NSNumber *numTotal = [nf numberFromString:self.countAmount];
    NSNumber * number = [nf numberFromString:newString];
    
    if (self.mAmountReceived.tag == 11) {
        self.mAmountReceived.text = [@(recived) stringValue];
    }
    else{
        self.mAmountCredit.text = [@(credit) stringValue];
    }
    
    NSString *total;
    if ([numTotal integerValue] % 2){ // even
//        total = [@([number integerValue]) stringValue];
        total = [@([number integerValue]-[numTotal integerValue]) stringValue];
    }
    else{   // add
        total = [@([number integerValue]) stringValue];
//        total = [@([number integerValue]-[numTotal integerValue]) stringValue];
    }
    [self.mAmountRemaininMoney setText:total];
}

- (IBAction)cilckDelete:(id)sender {
    
    if (self.mAmountReceived.tag != 11 && self.mAmountCredit.tag != 11) return;
    else if (self.isCredit) return;
    NSString *num;
    
    if (self.mAmountReceived.tag == 11) {
        num = [NSString checkNull:self.mAmountReceived.text];
    }
    else{
        num = [NSString checkNull:self.mAmountCredit.text];
    }
    if (![num isEqualToString:@""]) {
        NSString *truncatedString = [num substringToIndex:[num length]-1];
        NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
        [nf setNumberStyle:NSNumberFormatterNoStyle];                   // แปลง string to nsnumber
        NSNumber *numTotal = [nf numberFromString:self.countAmount];
        
        const char * _char = [truncatedString cStringUsingEncoding:NSUTF8StringEncoding];
        int isBackSpace = strcmp(_char,"\b");  // เช็กช่องว่าง
        if (isBackSpace == -8) {
            self.mAmountRemaininMoney.text = @"";
        }
        else{
            self.mAmountRemaininMoney.text = [@([truncatedString integerValue]-[numTotal integerValue]) stringValue];
        }
        if (self.mAmountReceived.tag == 11) {
            self.mAmountReceived.text = truncatedString;
        }
        else{
            self.mAmountCredit.text = truncatedString;
        }
    }
}

- (IBAction)cilckSeleteCash:(id)sender {
    popupView = [[PopupView alloc] init];
    [popupView setCashSpecifyTheAmount:self];
    [popupView showPopupWithStyle:CNPPopupStyleCentered];
}

- (IBAction)cilckChangeModeBillPayments:(id)sender {
    if (![self.member.cac boolValue] && !self.isCredit) {
        self.isPopupChangeMode = YES;
        popupView = [[PopupView alloc] init];
        [popupView setCashSpecifyTheAmount:self];
        [popupView showPopupWithStyle:CNPPopupStyleCentered];
    }
}
-(void)setViewCash:(BOOL) bl{
    [self.mAmountCredit setHidden:bl];
    [self.mImageCredit setHidden:bl];
    [self.mTextCredit setHidden:bl];
}
-(void) setViewCredit:(BOOL)bl{
    [self.mAmountReceived setHidden:bl];
    [self.mImageReceived setHidden:bl];
    [self.mTextCash setHidden:bl];
}
#pragma mark - CNPPopupController Delegate
- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    if (self.isPopupChangeMode) {
        if ([title isEqualToString:@"Cash"]) {
            [self.sizeViewCash setConstant:77.0];
            [self.sizeViewCredit setConstant:0.0];
            [self setViewCash:YES];
            [self setViewCredit:NO];
        }
        else{
            [self.sizeViewCash setConstant:0.0];
            [self.sizeViewCredit setConstant:77.0];
            [self setViewCash:NO];
            [self setViewCredit:YES];
        }
    }
    else{
        if ([title isEqualToString:@"Cash"]) {
            isCash = YES;
        }
        else{
            isCash = NO;
        }
        [self.mCash setTitle:title forState:UIControlStateNormal];
    }
    self.isPopupChangeMode = NO;
}
@end
