//
//  CouponViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 10/1/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *mPrice;
@property (weak, nonatomic) IBOutlet UITextField *mCount;

- (IBAction)cilckSubmitCoupon:(id)sender;
- (IBAction)cilckBack:(id)sender;
@end
