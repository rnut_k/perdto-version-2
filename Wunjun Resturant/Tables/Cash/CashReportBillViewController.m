//
//  CashReportBillViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashReportBillViewController.h"
#import "TableSingleBillViewController.h"
#import "CustomNavigationController.h"

#import "CNPPopupController.h"
#import "WYPopoverController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "TableCollectionViewCell.h"
#import "UIColor+HTColor.h"
#import "Check.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "Member.h"
#import "Constants.h"
#import "Time.h"
#import "LoadProcess.h"

@interface CashReportBillViewController ()<WYPopoverControllerDelegate,CNPPopupControllerDelegate>{
    Member *member;
    WYPopoverController *popoverController;
    LoadProcess *loadProgress;
    
    NSMutableArray *dataBill ;
    NSMutableDictionary *dataTotal ;
    NSDictionary *dataSubbill ;
    NSArray *dataUsage  ;
    NSDictionary *dataTables ;
    NSDictionary *dataOrders ;
    NSDictionary *dataMenu  ;
    NSDictionary *dataPromotion ;
    
    NSMutableArray *dataBillShow;
    int totalAll;
    UITableView *tableViewPop;
    NSIndexPath *indexPathPop;
    UITextField *passwordText;
}

@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation CashReportBillViewController

#pragma mark - UIViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {        // Custom initialization
    }
    return self;
}
- (KRLCollectionViewGridLayout *)layout
{
    return (id)self.mCollectionViewBill.collectionViewLayout;
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.layout.numberOfItemsPerLine = [[actionSheet buttonTitleAtIndex:buttonIndex] integerValue];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self setTitle:AMLocalizedString(@"รายงานบิล", nil)];
    totalAll = 0;
    member = [Member getInstance];
    [member loadData];
    
    loadProgress   = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
    
    self.layout.numberOfItemsPerLine = [Check checkDevice]?2:4;
    self.layout.aspectRatio = 1;
    self.layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    self.layout.interitemSpacing = 5;
    self.layout.lineSpacing = 5;
    self.layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setLine:self.mSortBillDate];
    [self setLine:self.mSortBillAll];
    [self loadDataReportBill];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([(NSObject *)self.slidingViewController.delegate isKindOfClass:[MEDynamicTransition class]]) {
        MEDynamicTransition *dynamicTransition = (MEDynamicTransition *)self.slidingViewController.delegate;
        if (!self.dynamicTransitionPanGesture) {
            self.dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:dynamicTransition action:@selector(handlePanGesture:)];
        }
        
        [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
        [self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
    } else {
        [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
        [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
    }
}

-(void)loadDataReportBill{
    NSDictionary *parameters = @{@"code":member.code,
                                 @"type":@"2"};
    
    NSString *url = [NSString stringWithFormat:GET_BILL_DETAIL,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSDictionary *dataAllZone = [NSDictionary dictionaryWithObject:json[@"data"]];
            dataBill    = dataAllZone[@"bill"];
           
            NSMutableArray *arr = [NSMutableArray arrayWithArray:dataAllZone[@"bill"]];
            [self sortDate:arr];
            dataBillShow = [NSMutableArray arrayWithArray:[dataBill copy]];
            dataTotal   = dataAllZone[@"total"];
            dataSubbill = dataAllZone[@"subbill"];
            dataUsage   = dataAllZone[@"usage"];
            dataTables  = dataAllZone[@"tables"];
            dataOrders  = dataAllZone[@"orders"];
            dataMenu    = dataAllZone[@"menu"];
            dataPromotion = dataAllZone[@"promotion"];
        }
        [self.mCollectionViewBill reloadData];

        NSInteger total = 0;
        if ([dataTotal count] != 0) {
            for (NSString *dic in [dataTotal allValues]) {
                total += [dic integerValue];
            }
        }
        
        [self.mtotal setText:[@(total) stringValue]];
        [LoadProcess dismissLoad];
    }];
}
#pragma mark - CollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [dataBillShow count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TableCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    [self setLine:cell.mViewCollection];
    
    @try {
        if ([dataBillShow count] != 0) {
            int num = (int)indexPath.row;
            cell.mCountBill.text = [NSString stringWithFormat:@"%@ %d",AMLocalizedString(@"บิลที่", nil),num+1];
            NSDictionary *detailBill = dataBillShow[indexPath.row];
            NSString *keyBill = detailBill[@"id"];
            NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(bill_id = %@)" andData:dataUsage];
            NSString *nameTable = @"";
            if ([listUsaage count] != 0) {
                NSString *keyTable;
                nameTable = @"[";
                for (NSDictionary *dic in listUsaage) {
                    if (![keyTable isEqual:dic[@"table_id"]]) {
                        keyTable = dic[@"table_id"];
                        NSDictionary *valel = dataTables[[NSString stringWithFormat:@"id_%@",keyTable]];
                        nameTable = [[nameTable stringByAppendingString:valel[@"label"]] stringByAppendingString:@","];
                    }
                }
                nameTable = [[nameTable substringToIndex:[nameTable length]-1] stringByAppendingString:@"]"];
            }
            [cell.mNameTable setText:[NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"โต๊ะที่", nil),nameTable]];
            [cell.mTimeOpen setText:[Time conventTime:listUsaage[0][@"open_datetime"]]];
            [cell.mTimeClose setText:[Time conventTime:listUsaage[0][@"close_datetime"]]];
            
            NSString *idTotal = [NSString stringWithFormat:@"bill_id_%@",keyBill];
            NSString *totalBillID = @" $ 0";
            if (dataTotal[idTotal] != nil) {
                totalBillID = [NSString stringWithFormat:@"$ %@",dataTotal[idTotal]];
            }
            [cell.mSumTotal setText:totalBillID];
            UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self
                                                                                                                  action:@selector(btnDetailMenuPop:)];
            longPressGestureRecognizer.numberOfTouchesRequired = 1;
            longPressGestureRecognizer.minimumPressDuration    = 0.5f;
            longPressGestureRecognizer.delegate  = self;
            [cell addGestureRecognizer:longPressGestureRecognizer];
        }
    }
    @catch (NSException *exception) {
        DDLogInfo(@"error exception %@",exception);
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"%ld",(long)indexPath.row);
    
    NSDictionary *detailBill = dataBillShow[indexPath.row];
    NSString *keyBill = detailBill[@"id"];
    NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(bill_id = %@)" andData:dataUsage];
    NSDictionary *detailTable = listUsaage[0];
    NSDictionary *valel = dataTables[[NSString stringWithFormat:@"id_%@",detailTable[@"table_id"]]];
    [self createPopBill:valel and:detailTable];

}
- (void)createPopBill:(NSDictionary *)dataTable and:(NSDictionary *)dataBill1{ 
//    
    TableSingleBillViewController *tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSubBill"];
    [tableSingleBill setTitleTable:[NSString stringWithFormat:@" %@ %@",AMLocalizedString(@"บิลหลักโต๊ะ",nil),dataTable[@"label"]]];
    [tableSingleBill setNumberTable:dataTable[@"label"]];
    [tableSingleBill setDetailTable:dataBill1];
    [tableSingleBill setCheckOpenTable:YES];
    [tableSingleBill setIsCashier:YES];
    [tableSingleBill setIsPopupCashier:YES];
    
    tableSingleBill.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    tableSingleBill.modalInPopover = NO;
    
    CustomNavigationController* contentViewController = [[CustomNavigationController alloc] initWithRootViewController:tableSingleBill];
    [contentViewController setColorBar];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    UIImage *image = [[UIImage imageNamed:@"close"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(btnClosePop:)];
    [tableSingleBill.navigationItem setRightBarButtonItem:rightBarButtonItem];
    [tableSingleBill.navigationItem setLeftBarButtonItem:nil];
}
-(void)setLine:(UIView *)obj{
    obj.layer.cornerRadius = 10.0f;
    obj.layer.borderWidth = 1.0f;
    obj.layer.borderColor = [UIColor ht_silverColor].CGColor;
    obj.clipsToBounds = NO;
    obj.backgroundColor = [UIColor whiteColor];
    obj.layer.masksToBounds = YES;
}
#pragma mark - IBActions

- (IBAction)btnSortBillDate:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.tag = 111;
    [self createViewPopBillSortIDTable:sender];
}
- (IBAction)btnSortBillAll:(id)sender {
    UIButton *btn = (UIButton *)sender;
    btn.tag = 222;
    [self createViewPopBillSortIDTable:sender];
}
-(void)createViewPopBillSortIDTable:(id)sender{
    UIButton *btn = (UIButton *)sender;
    UITableViewController *viewController = [[UITableViewController alloc] init];
    viewController.title = (btn.tag == 111)?AMLocalizedString(@"รายการบิล",nil):AMLocalizedString(@"รายการเรียง",nil);
    tableViewPop =  viewController.tableView;
    tableViewPop.delegate = self;
    tableViewPop.dataSource = self;
    [tableViewPop setTag:btn.tag];
    
    viewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-50,self.view.frame.size.height-50);
    viewController.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:viewController];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:btn.bounds
                             inView:btn
           permittedArrowDirections:WYPopoverArrowDirectionAny
                           animated:YES
                            options:WYPopoverAnimationOptionFadeWithScale];
}
- (IBAction)menuButtonTapped:(id)sender {
    [self.slidingViewController anchorTopViewToRightAnimated:YES];
}
- (IBAction)btnClosePop:(id)sender {
    [popoverController dismissPopoverAnimated:YES];
}
#pragma mark - delete table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  (tableView.tag == 111)?2:[dataTables count]+1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenfine = @"cell";
    
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:idenfine];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenfine];
    }
    
    if (tableView.tag == 222) {
        if (indexPath.row == 0) {
            [cell.textLabel setText:AMLocalizedString(@"บิลทั้งหมด",nil)];
        }
        else{
            NSArray *listKey = [dataTables allKeys];
            NSDictionary *data = dataTables[listKey[indexPath.row-1]];
            [cell.textLabel setText:data[@"label"]];
        }
    }
    else if (tableView.tag == 111){
        if (indexPath.row == 0) {
            [cell.textLabel setText:AMLocalizedString(@"เรียงตามเวลาเปิดโต๊ะ",nil)];
        }
        else{
            [cell.textLabel setText:AMLocalizedString(@"เรียงตามยอดบิล",nil)];
        }
    }
    cell.textLabel.textColor = [UIColor ht_leadDarkColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"index %ld",(long)indexPath.row);
    [self btnClosePop:nil];
    
    if (tableView.tag == 222) {
        if (indexPath.row == 0) {
           dataBillShow = [NSMutableArray arrayWithArray:[dataBill copy]];
        }
        else{
            NSArray *listKey = [dataTables allKeys];
            NSDictionary *data = dataTables[listKey[indexPath.row-1]];
            NSString *keyTable = data[@"id"];
            NSArray *listUsaage = [NSDictionary searchDictionary:keyTable andvalue:@"(table_id = %@)" andData:dataUsage];
            if ([listUsaage count] != 0) {
                NSDictionary *dic = (NSDictionary *)listUsaage;
                keyTable = dic[@"bill_id"];
                NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyTable andvalue:@"(id = %@)" andData:dataBill];
                dataBillShow = [NSMutableArray arrayWithArray:listUsaage];
            }
        }
    }
    else if (tableView.tag == 111){
        if (indexPath.row == 0) { // เรียงตามเวลา
            dataBillShow = [NSMutableArray arrayWithArray:[dataBill copy]];
            NSMutableArray *arr = [self sortDate:dataBillShow];
            dataBillShow = [NSMutableArray arrayWithArray:arr];
        }
        else{ // เรียงตามยอด
            dataBillShow = [NSMutableArray arrayWithArray:[dataBill copy]];
            NSMutableArray *arr =  [self sortArrayTotal:dataBillShow];
            dataBillShow = [NSMutableArray arrayWithArray:arr];
        }
    }
    [self.mCollectionViewBill reloadData];
}


#pragma mark - sort Array

-(NSMutableArray *) sortDate:(NSMutableArray *)data{
    NSLog(@"%@",data);
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"created_datetime"
                                                                 ascending:YES];
    [data sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [data sortUsingComparator:^(id dict1, id dict2) {
        NSDate *date1 = dict1[@"created_datetime"];// create NSDate from dict1's Date;
        NSDate *date2 = dict2[@"created_datetime"];// create NSDate from dict2's Date;
        return [date1 compare:date2];
    }];
    NSLog(@"%@",data);
    return data;
}

-(NSMutableArray *) sortTotal:(NSMutableArray *)data{
    NSLog(@"%@",data);
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"total"
                                                                 ascending:YES];
    [data sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [data sortUsingComparator:^(id dict1, id dict2) {
        NSDate *date1 = dict1[@"total"];// create NSDate from dict1's Date;
        NSDate *date2 = dict2[@"total"];// create NSDate from dict2's Date;
        return [date1 compare:date2];
    }];
    NSLog(@"%@",data);
    return data;
}
-(NSMutableDictionary *) sortArray:(NSMutableDictionary *)data{
    [data keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        NSInteger value1 = [[obj1 objectForKey: @"id"] intValue];
        NSInteger value2 = [[obj2 objectForKey: @"id"] intValue];
        if (value1 > value2)
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if (value1 < value2)
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return data;
}
-(NSMutableArray *) sortArrayTotal:(NSMutableArray *)data{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"total"
                                                                 ascending:YES];
    [data sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    [data sortUsingComparator:^(id dict1, id dict2) {
        NSInteger value1 = [dict1[@"total"] intValue];
        NSInteger value2 = [dict2[@"total"] intValue];
        if (value1 > value2)
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if (value1 < value2)
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    return data;
}
-(NSArray *) sortDictionary:(NSMutableDictionary *)data{
    //sort by values
    NSArray * keys = [data keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2]; //ascending
    }];
//    for(NSString *key in keys)
//    {
//        NSLog(@"%@:%@",key,[data objectForKey:key]);
//    }
//    NSArray * keys2 = [data keysSortedByValueUsingSelector:@selector(compare:)];
//    for(NSString *key in keys2)
//    {
//        NSLog(@"%@:%@",key,[data objectForKey:key]);
//    }
    return keys;
}
- (IBAction)btnDetailMenuPop:(UILongPressGestureRecognizer *)sender {
    @try {
        if (sender.state == UIGestureRecognizerStateBegan){
            CGPoint p = [sender locationInView:self.mCollectionViewBill];
            indexPathPop = [self.mCollectionViewBill indexPathForItemAtPoint:p];
            [self showPopupWithStyle:CNPPopupStyleCentered];
        }
    }
    @catch (NSException *exception) {
        DDLogError(@" error btnDetailMenuPop : %@",exception);
    }
}
- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title  = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ยืนยัน",nil)];
    CNPPopupButton *btnOK  = [self createButton:title];
    
    title = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ไม่",nil)];
    CNPPopupButton *btnCancel  = [self createButton:title];
    
    title = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"ต้องการลบบิลโต๊ะ %@  ใช่ หรือ ไม่", nil)];
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    title = [self createNSAttributedString:paragraphStyle and:AMLocalizedString(@"กรุณาป้อนรห้สผ่าน", nil)];
    UILabel *textPassword = [[UILabel alloc] init];
    textPassword.numberOfLines = 0;
    textPassword.attributedText = title;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 55)];
    customView.backgroundColor = [UIColor lightGrayColor];
    
    passwordText = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 230, 35)];
    passwordText.borderStyle = UITextBorderStyleRoundedRect;
    passwordText.placeholder = AMLocalizedString(@"รห้สผ่าน", nil);
    [customView addSubview:passwordText];
    
    NSMutableArray *arr = [NSMutableArray arrayWithObject:titleLabel];
    [arr addObjectsFromArray:@[textPassword,customView,btnOK,btnCancel]];
    self.popupController = [[CNPPopupController alloc] initWithContents:arr];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromBottom;
    [self.popupController presentPopupControllerAnimated:YES];
}
-(NSAttributedString *)createNSAttributedString:(NSMutableParagraphStyle *)paragraphStyle  and:(NSString *) title{
    NSAttributedString *buttonTitle = [[NSAttributedString alloc] initWithString:title
                                                                      attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:14], NSForegroundColorAttributeName : [UIColor ht_leadColor], NSParagraphStyleAttributeName : paragraphStyle}];
    return buttonTitle;
}
-(CNPPopupButton *)createButton:(NSAttributedString *)title{
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0,0,200,40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:[title string] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.6784 green:0.6706 blue:0.6588 alpha:1.0];
    button.titleLabel.textColor = [UIColor whiteColor];
    [button.layer setCornerRadius:10];
    
    button.selectionHandler = ^(CNPPopupButton *button){
        if ([button.titleLabel.text isEqualToString:AMLocalizedString(@"ยืนยัน",nil)]) {
            [self loadDataDelete];
        }
        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    return button;
}
//#pragma mark - CNPPopupController Delegate
//
//- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
//    NSLog(@"Dismissed with button title: %@", title);
//    
//    if ([title isEqualToString:AMLocalizedString(@"ยืนยัน",nil)]) {
//        [self loadDataDelete];
//    }
//}
//
//- (void)popupControllerDidPresent:(CNPPopupController *)controller {
//    NSLog(@"Popup controller presented.");
//}

-(void)loadDataDelete{
    [loadProgress.loadView showWithStatus];
    @try {

        NSDictionary *detailBill = dataBillShow[indexPathPop.row];
        NSString *keyBill = detailBill[@"id"];
        NSArray *listUsaage = [NSDictionary searchDictionaryALL:keyBill andvalue:@"(bill_id = %@)" andData:dataUsage];
        NSString *nameTable = @"";
        if ([listUsaage count] != 0) {
            NSString *keyTable;
            nameTable = @"[";
            for (NSDictionary *dic in listUsaage) {
                if (![keyTable isEqual:dic[@"table_id"]]) {
                    keyTable = dic[@"table_id"];
                    NSDictionary *valel = dataTables[[NSString stringWithFormat:@"id_%@",keyTable]];
                    nameTable = [[nameTable stringByAppendingString:valel[@"label"]] stringByAppendingString:@","];
                }
            }
            nameTable = [[nameTable substringToIndex:[nameTable length]-1] stringByAppendingString:@"]"];
        }

        NSString *strPassword = [[passwordText text] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSDictionary *parameters = @{@"code":member.code,
                                     @"user":@{
                                             @"password":strPassword,
                                             @"username":member.n}
                                     };
        NSString *url = [NSString stringWithFormat:SET_DELETE_BILL,member.nre,keyBill];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            BOOL bl = [json[@"status"] boolValue];
            if(bl){
                NSString *str = AMLocalizedString(@"ลบบิลโต๊ะที่ %@ สำเร็จ", nil);
                str = [NSString stringWithFormat:str,nameTable];
                [loadProgress.loadView showWithStatusTitleSet:str];
            }
            else{
                NSString *str = AMLocalizedString(@"ลบบิลโต๊ะที่ %@ ไม่สำเร็จ", nil);
                str = [NSString stringWithFormat:str,nameTable];
                [loadProgress.loadView showWithStatusAddError:str];
            }
        }];
    }
    @catch (NSException *exception) {
        DDLogError(@" error loadDataDelete : %@",exception);
    }
}
@end
