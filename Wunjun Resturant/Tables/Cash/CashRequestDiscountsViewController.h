//
//  CashRequestDiscountsViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 4/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNPPopupController.h"

@interface CashRequestDiscountsViewController : UIViewController<UIScrollViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView  *mImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UITextView   *mDetailRequest;
@property (weak, nonatomic) IBOutlet UITextField  *mAmount;
@property (weak, nonatomic) IBOutlet UITextField  *mPercent;
@property (weak, nonatomic) IBOutlet UIButton     *mSelectDiscounts;

@property (strong, nonatomic)  NSString *mPrice;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mLayoutHeigtImage;

- (IBAction)btnReply:(id)sender;
- (IBAction)leftBtnAction:(id)sender;
- (IBAction)btnCamera:(id)sender;
- (IBAction)btnSelectDiscounts:(id)sender;

@end
