//
//  CashAmountDailyViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashAmountDailyViewController : UIViewController<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollViewAmount;
@property (weak, nonatomic) IBOutlet UIView *mContentView;

@property (weak, nonatomic) IBOutlet UILabel *mTotalCoupon;
@property (weak, nonatomic) IBOutlet UILabel *mSalesTotal;
@property (weak, nonatomic) IBOutlet UITextField *mSalesRealNumbers;
@property (weak, nonatomic) IBOutlet UITextField *mSalesCounts;
@property (weak, nonatomic) IBOutlet UITextField *mSalesCoupon;
@property (weak, nonatomic) IBOutlet UITextField *mSalesCerdit;
@property (weak, nonatomic) IBOutlet UILabel *mSalesDislocation;
@property (weak, nonatomic) IBOutlet UITextView *mTextDislocation;
@property (weak, nonatomic) IBOutlet UILabel *mMoneyIn;

@property (weak, nonatomic) IBOutlet UIButton *mSubmitButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarBtn;



//label
@property (weak, nonatomic) IBOutlet UILabel *labelCashBefore;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalCalculation;
@property (weak, nonatomic) IBOutlet UILabel *labelCashCalculated;
@property (weak, nonatomic) IBOutlet UILabel *labelTheAmountThatCounts;
@property (weak, nonatomic) IBOutlet UILabel *labelError;
@property (weak, nonatomic) IBOutlet UILabel *labelErrorComment;


- (IBAction)menuButtonTapped:(id)sender ;
- (IBAction)btnApprove:(id)sender;
@end
