//
//  CashAmountDailyViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashAmountDailyViewController.h"
#import "MEDynamicTransition.h"
#import "UIViewController+ECSlidingViewController.h"
#import "CustomNavigationController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import "WYPopoverController.h"
#import "CashTableNotCheckBill.h"
#import "UIViewController+ECSlidingViewController.h"

#import "UIColor+HTColor.h"
#import "Check.h"
#import "LoadMenuAll.h"
#import "MenuDB.h"
#import "Member.h"
#import "Constants.h"
#import "Time.h"
#import "GlobalBill.h"
#import "LoadMenuAll.h"
#import "DeviceData.h"
#import "LoadProcess.h"

@interface CashAmountDailyViewController ()<UITextFieldDelegate,WYPopoverControllerDelegate>{
    GlobalBill *_globalBill;
    CustomNavigationController *vc;
    Member *_member;
    LoadProcess *loadProcess;
    
    NSDictionary *dataOrder;
    NSDictionary *dataTotal;
    
    WYPopoverController *popupTableNotCheckBill;
    NSMutableArray *dataNotCheckBill;
}
@property (nonatomic, strong) UIPanGestureRecognizer *dynamicTransitionPanGesture;
@end

@implementation CashAmountDailyViewController

#pragma mark - UIViewController
-(void)viewDidLoad{
    [super viewDidLoad];
    _globalBill = [GlobalBill sharedInstance];
    _member = [Member getInstance];
    [_member loadData];
    loadProcess  = [LoadProcess sharedInstance];
    [self getTableZone];

    [self setTotalSales];
    
    [self setUpLabel];
    NSString *pre;
    if([vc.role isEqualToString:@"admin"]){
        [self setTitle:vc.title];
        pre = vc.viewInfo[@"pre"];
        [self.mSalesCounts setText:[NSString stringWithFormat:@"%.02f", [vc.viewInfo[@"post"] floatValue]]];
        [self.mTextDislocation setText:vc.viewInfo[@"description"]];
        [self.mSalesDislocation setText:[NSString stringWithFormat:@"%.02f", [vc.viewInfo[@"amount_effect"] floatValue]]];
        
        [self.mMoneyIn setEnabled:NO];
        [self.mSalesCounts setEnabled:NO];
        [self.mTextDislocation setEditable:NO];
        [self.mSalesDislocation setEnabled:NO];
        [self.mSalesRealNumbers setEnabled:NO];
        [self.mSubmitButton setEnabled:NO];
        [self.mSubmitButton setHidden:YES];
        
        [self.leftBarBtn setImage:[UIImage imageNamed:@"ic_back"]];
        [self.rightBarBtn setImage:nil];
        [self.rightBarBtn setEnabled:NO];
        
    }else{
        [self setTitle:AMLocalizedString(@"ส่งยอดประจำวัน", nil)];
        pre = _globalBill.moneyIn;
        [self setLine:self.mMoneyIn];
        [self setLine:self.mSalesCounts];
        [self setLine:self.mSalesCoupon];
        [self setLine:self.mSalesCerdit];
        [self setLine:self.mSalesRealNumbers];
        [self.mSalesRealNumbers setUserInteractionEnabled:NO];
    }
}

-(void)setUpLabel{

    _labelCashBefore.text = AMLocalizedString(@"cash_before", nil);
    _labelTotalCalculation.text = AMLocalizedString(@"total_calculation", nil);
    _labelCashCalculated.text = AMLocalizedString(@"cash_calculated", nil);
    _labelTheAmountThatCounts.text = AMLocalizedString(@"the_amount_that_counts", nil);
    _labelError.text = AMLocalizedString(@"error", nil);
    _labelErrorComment.text = AMLocalizedString(@"error_comment", nil);
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.slidingViewController != nil) {
        if ([(NSObject *)self.slidingViewController.delegate isKindOfClass:[MEDynamicTransition class]]) {
            MEDynamicTransition *dynamicTransition = (MEDynamicTransition *)self.slidingViewController.delegate;
            if (!self.dynamicTransitionPanGesture) {
                self.dynamicTransitionPanGesture = [[UIPanGestureRecognizer alloc] initWithTarget:dynamicTransition action:@selector(handlePanGesture:)];
            }
            
            [self.navigationController.view removeGestureRecognizer:self.slidingViewController.panGesture];
            [self.navigationController.view addGestureRecognizer:self.dynamicTransitionPanGesture];
        }
        else {
            [self.navigationController.view removeGestureRecognizer:self.dynamicTransitionPanGesture];
            [self.navigationController.view addGestureRecognizer:self.slidingViewController.panGesture];
        }
    }
    
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
-(void)setTotalSales{
    vc = (CustomNavigationController *)self.parentViewController;
    if([vc.role isEqualToString:@"admin"])
    {
        NSDictionary *dataIncome = [NSDictionary dictionaryWithObject:vc.dataIncome];
        [self.mSalesTotal setText:[NSString stringWithFormat:@"%.02f",[dataIncome[@"income_amount"] floatValue]]];
        [self.mSalesRealNumbers setText:[NSString stringWithFormat:@"%.02f", [vc.viewInfo[@"post"] floatValue]-[vc.viewInfo[@"amount_effect"] floatValue]]];
        [self.mMoneyIn setText:[NSString stringWithFormat:@"%.02f",[dataIncome[@"pre"] floatValue]]];
    }
    else
    {
//        T = ยอดรวมของแต่ละ bill
//        P = ยอดเงินก่อนเข้ากะ
//        Q = ยอดขายรวม ?
//        C = ยอดเงินสดที่ควรจะเป็น ?
//        N = คูปองยอดรวมจากแต่ละ bill
//        K = เงินจากบัตร ได้จากแต่ละ bill
//        R = เงินสดที่ได้รับ
//        E = ความคลาดเคลื่อน ?
//        
//        Q = T – N
//        C = T+P-K
//        E = R – (T+P-K)
        
        NSInteger momeyIN      = [_globalBill.moneyIn integerValue];
        NSInteger totalAll     = 0;// Q
        NSInteger totalAllBill = 0;// T
        NSInteger countCoupon  = 0;
        NSInteger totalCoupon  = 0;// N
        NSInteger totalCerdit  = 0;// N
        NSDictionary *dataAllZone = [NSDictionary dictionaryWithObject:vc.dataAllZone];
        NSDictionary *dataOrder1 =  [NSDictionary dictionaryWithObject:dataAllZone[@"orders"]];
        NSArray *bill =  [NSArray arrayWithArray:dataAllZone[@"bill"]];
        
        for (NSDictionary *dic in bill) {
            NSString *billID = [NSString stringWithFormat:@"bill_id_%@",dic[@"id"]];
            NSArray *listOrder = [NSArray arrayWithArray:dataOrder1[billID]];
            for (NSDictionary *dataBill in listOrder) {
                NSString *type = dataBill[@"type"];
                if ([type isEqualToString:@"4"]) {
                    countCoupon ++;
                    totalCoupon += fabs([dataBill[@"total"] floatValue]);
                }
                else if ([type isEqualToString:@"2"]) {
                    totalCerdit += fabs([dataBill[@"total"] floatValue]);
                }
            }
        }
     
        
        dataTotal   = [NSDictionary dictionaryWithObject:dataAllZone[@"total"]];
        for (NSString *tem in [dataTotal allValues]) {
            totalAllBill  += [tem floatValue];
        }
        totalAll = totalAllBill + totalCoupon;
        
        [self.mSalesTotal setText:[NSString stringWithFormat:@"%.02f",[@(totalAll) doubleValue]]];
        NSString *num;
        num = [NSString stringWithFormat:@"%.02f",[@((momeyIN + totalAllBill)) doubleValue]];
        
        [self.mSalesRealNumbers setText:num];
        [self.mSalesCoupon setText:[NSString stringWithFormat:@"%.02f",[@(totalCoupon) doubleValue]]];
        [self.mTotalCoupon setText:[NSString stringWithFormat:self.mTotalCoupon.text,[@(countCoupon) stringValue]]];
        [self.mMoneyIn setText:[NSString stringWithFormat:@"%.02f",[@(momeyIN) doubleValue]]];
        [self.mSalesCerdit setText:[NSString stringWithFormat:@"%.02f",[@(totalCerdit) doubleValue]]];
    }
}
#pragma - mark TextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//     NSLog(@"%@",textField.text);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//    NSLog(@"%@",textField.text);
    NSInteger num = [textField.text integerValue];
    NSInteger numTotal = [self.mSalesRealNumbers.text integerValue];
    num -= numTotal;
    [self.mSalesDislocation setText:[@(num) stringValue]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    NSLog(@"%@",textField.text);
    [textField resignFirstResponder];
    return YES;
    
}
#pragma mark - IBActions

- (IBAction)menuButtonTapped:(id)sender {
    if([vc.role isEqualToString:@"admin"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.slidingViewController anchorTopViewToRightAnimated:YES];
    }
}

- (IBAction)btnApprove:(id)sender {
    
    if (![self.mSalesCounts.text isEqual:@""]) {
        NSString *momeyIN      = [@([_globalBill.moneyIn integerValue]) stringValue];
        NSDictionary *parameters = @{@"code":_member.code,
                                     @"action":@"req",
                                     @"pre":momeyIN,
                                     @"post":self.mSalesCounts.text,
                                     @"ideal_income":self.mSalesRealNumbers.text,
                                     @"comment":self.mTextDislocation.text
                                     };
        NSString *url = [NSString stringWithFormat:GET_MANAGE_INCOME,_member.nre];
        
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            bool bl  = [json[@"status"] boolValue];
            if(bl){
                [loadProcess.loadView showWithStatus];
                [self checkOut];
            }
            [LoadProcess dismissLoad];
        }];
    }
    else{
        [loadProcess.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาป้อนจำนวนยอดบิล", nil)];
    }
}
-(void)checkOut{    
    [LoadMenuAll logoutView:@{@"error":LOGIN_ERROR}];
}
-(void) getTableZone{
    [loadProcess.loadView showProgress];
    
    NSMutableArray *tableIndexData = [NSMutableArray array];
    [LoadMenuAll retrieveData:@"0" success:^(NSArray *dataTables){
        NSLog(@"%@",dataTables);
        for (NSDictionary *dic in dataTables) {
            if (![dic[@"status"] boolValue]) {
                [tableIndexData addObject:dic];
            }
        }
        dataNotCheckBill = [NSMutableArray arrayWithArray:tableIndexData];
        if ([dataNotCheckBill count] != 0 && ![vc.role isEqualToString:@"admin"]){
            [self createTablePopup];
        }
    }];
}
- (void)createTablePopup {
   
    CashTableNotCheckBill  *viewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashTableNotCheckBill"];
    viewController.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height);
    viewController.modalInPopover = NO;
    
    popupTableNotCheckBill = [[WYPopoverController alloc] initWithContentViewController:viewController];
    popupTableNotCheckBill.delegate = self;
    [popupTableNotCheckBill presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    
    NSString *title_ = [NSString stringWithFormat:@"%lu",(unsigned long)[dataNotCheckBill count]];
    viewController.mTitleNotBill.text = [NSString stringWithFormat:viewController.mTitleNotBill.text,title_];
    [viewController setDataNotCheckBill:dataNotCheckBill];
    [viewController.mButtonClose addTarget:self
                                    action:@selector(btnClosePopup:)
                          forControlEvents:UIControlEventTouchUpInside];
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController{
    [self createTablePopup];
}
-(IBAction)btnClosePopup:(id)sender{
    
    CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashMain"];
    self.slidingViewController.topViewController = cashSalesReportView;
    [self.slidingViewController resetTopViewAnimated:YES];
    [popupTableNotCheckBill dismissPopoverAnimated:YES];
}
-(void)setLine:(UIView *)obj{
    obj.layer.cornerRadius = 10.0f;
    obj.layer.borderWidth = 1.0f;
    obj.layer.borderColor = [UIColor ht_silverColor].CGColor;
    obj.clipsToBounds = NO;
    obj.backgroundColor = [UIColor whiteColor];
    obj.layer.masksToBounds = YES;
}
@end
