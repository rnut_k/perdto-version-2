//
//  CashMenuViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 3/18/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashMenuViewController.h"
#import "CustomNavigationController.h"

#import "UIViewController+ECSlidingViewController.h"
#import "DeviceData.h"
#import "Member.h"
#import "MenuDB.h"
#import "Constants.h"
#import "LoadMenuAll.h"

@interface CashMenuViewController (){
    Member *_member;
    MenuDB *_menuDB;
    NSDictionary *dataAllZone;
}
@property (nonatomic, strong) NSArray *menuItems;
@property (nonatomic, strong) UINavigationController *transitionsNavigationController;
@end

@implementation CashMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.transitionsNavigationController = (UINavigationController *)self.slidingViewController.topViewController;
    _member = [Member getInstance];
    [_member loadData];
    [self loadDataReportSeles];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
  	
}

-(void)loadDataReportSeles{
    NSDictionary *parameters = @{@"code":_member.code,
                                 @"type":@"2",
                                 @"get_summary":@"1",
                                 @"date_time":@""};
    NSString *url = [NSString stringWithFormat:GET_BILL_DETAIL,_member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            dataAllZone = [NSDictionary dictionaryWithObject:json[@"data"]];
        }
    }];
}

#pragma mark - UITableViewDataSource
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel * sectionHeader = [[UILabel alloc] initWithFrame:CGRectZero];
    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    sectionHeader.font = [UIFont boldSystemFontOfSize:16];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.text = AMLocalizedString(@"เมนู", nil);
    return sectionHeader;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *menuItem = [@(indexPath.row) stringValue];
    self.slidingViewController.topViewController.view.layer.transform = CATransform3DMakeScale(1, 1, 1);
//    
    if ([menuItem isEqualToString:@"1"]) {
        self.slidingViewController.topViewController = self.transitionsNavigationController;
    }
    else if ([menuItem isEqualToString:@"2"]) {
        self.slidingViewController.topViewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashOrderMonitouing"];
    }
    else if ([menuItem isEqualToString:@"3"]) {
        self.slidingViewController.topViewController = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashReportBill"];
    }
    else if ([menuItem isEqualToString:@"4"]) {
        CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashSalesReport"];
        [cashSalesReportView setDataAllZone:dataAllZone];
        self.slidingViewController.topViewController = cashSalesReportView;
    }
    else if ([menuItem isEqualToString:@"5"]) {
        CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashAmountDaily"];
        [cashSalesReportView setDataAllZone:dataAllZone];
        self.slidingViewController.topViewController = cashSalesReportView;
    }
    else if ([menuItem isEqualToString:@"6"]) {    // web ทำรายการย้อนหลัง
        [self openSheu:self];
    }
    else if ([menuItem isEqualToString:@"0"]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    [self.slidingViewController resetTopViewAnimated:YES];
}
- (IBAction)openSheu:(id)sender
{
    NSString *text = [NSString stringWithFormat:GET_BACK_BiLL,_member.nre,_member.code];
    NSURL* url  = [[NSURL alloc] initWithString:text];
    [[UIApplication sharedApplication] openURL:url];
}

#pragma mark - create table container
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender // ยกเลิก prepareForSegue
{
//    if(![identifier isEqualToString:@"logout"])  {
//        return NO;
//    }
    return YES;
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    NSLog(@"name %@",[segue identifier]);
    if ([[segue identifier] isEqualToString:@"logout"]) {  //    logout
//        Member *member = [Member getInstance];
//        
//        DeviceData *device = [DeviceData getInstance];
//        if(device.token == nil)
//            device.token = @"";
//        [member deleteLoginDb];
//        
//        MenuDB *menuDB = [MenuDB getInstance];
//        [menuDB deleteLoginDb];
        
         [LoadMenuAll logoutView:@{@"error":@"101"}];
        
//        UIViewController *vc = segue.destinationViewController;
//        [self presentViewController:vc animated:YES completion:nil];
    }
}
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
