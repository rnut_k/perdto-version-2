//
//  CashOrderMonitoring.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/30/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CashOrderMonitoring : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (nonatomic ,strong) NSDictionary *dataKitchen;
@end
