//
//  CashRequestDiscountsViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 4/27/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "CashRequestDiscountsViewController.h"
#import "CustomNavigationController.h"
#import "WYPopoverController.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import "UIImageView+AFNetworking.h"
#import "LoadMenuAll.h"
#import "LoadProcess.h"
#import "Check.h"
#import "NSString+GetString.h"
#import "Member.h"
#import "Constants.h"

 @interface CashRequestDiscountsViewController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,WYPopoverControllerDelegate>{

    CustomNavigationController *vc;
    WYPopoverController *popoverController;
    
    NSString *imagePro;
    LoadProcess *loadProcess;
     
     NSArray *listSelect;
//     UITableViewController *tableViewSelect;
     UITableView *tableViewSelect;
     int numSelectDiscounts;
}
@end

@implementation CashRequestDiscountsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    imagePro = @"";
    vc = (CustomNavigationController *)self.parentViewController;
    if (![Check checkNull:vc.countAmount]) {
        self.mPrice = vc.countAmount;
    }
    listSelect = @[AMLocalizedString(@"ทั้งหมด", nil) ,AMLocalizedString(@"อาหาร", nil),AMLocalizedString(@"เครื่องดื่ม", nil)];
    numSelectDiscounts = 0;
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - loadData OpenTable
-(void)loadData:(NSString *)idTable{
    Member *member = [Member getInstance];
    [member loadData];
    NSArray *arr = [LoadMenuAll requirebillID:vc.dataListSingBill[@"id"]];
    NSString *comment = [NSString checkNull:self.mDetailRequest.text];
    if (![self.mPercent.text isEqualToString:@""]) {
        NSString *str = [NSString stringWithFormat:@"%@%@ %@ %@",AMLocalizedString(@"ลดเฉพราะ", nil),self.mSelectDiscounts.titleLabel.text,self.mPercent.text,@"%"];
        comment = [comment stringByAppendingString:str];
    }

    NSDictionary *parameters = @{@"code":member.code,
                                 @"usage_id":arr[0],
                                 @"bill_id":arr[1],
                                 @"table_id":vc.dataListSingBill[@"id"],
                                 @"discount":self.mAmount.text,
                                 @"comment":comment,
                                 @"picture":imagePro};
    
    NSString *url = [NSString stringWithFormat:GET_REQUIRE_DISCOUNT,member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        @try {
            bool bl = [json[@"status"] boolValue];
            if (bl) {
                [self leftBtnAction:nil];
            }
            else
                NSLog(@"error loadDataOpenTableSing");
            [LoadProcess dismissLoad];
        }
        @catch (NSException *exception) {
            DDLogError(@"Caught exception loadData : %@", exception);
        }
    }];
}
- (IBAction)btnReply:(id)sender {
    LoadProcess *loadProgress  = [LoadProcess sharedInstance];
    if (![self.mAmount.text isEqual:@""]) {
        [loadProgress.loadView showWithStatus];
        [self loadData:@""];
    }
    else{
        [loadProgress.loadView showWithStatusAddError:AMLocalizedString(@"กรุณาระบุ", nil)];
    }
}
- (IBAction)btnSelectDiscounts:(id)sender{
    UIButton *btn = (UIButton *)sender;
    UITableViewController *tableView = [[UITableViewController alloc] init];
    tableView.title = AMLocalizedString(@"เลือกรายการที่ต้องการส่วนลด", nil);
    tableViewSelect = tableView.tableView;
    [tableViewSelect setDataSource:self];
    [tableViewSelect setDelegate:self];
    
    CGSize size;
    if (![Check  checkDevice]) {
        size = CGSizeMake(320,200);
    }
    else{
        size = CGSizeMake(self.view.frame.size.width-50,200);
    }
    
    tableView.preferredContentSize = CGSizeMake(size.width,size.height);
    tableView.modalInPopover = NO;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:tableView];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:btn.bounds
                                       inView:btn
                     permittedArrowDirections:WYPopoverArrowDirectionAny
                                     animated:YES
                                      options:WYPopoverAnimationOptionFadeWithScale];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [listSelect count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idTable = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idTable];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idTable];
    }
    
    NSString *text = listSelect[indexPath.row];
    [cell.textLabel setText:text];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld",(long)indexPath.row);
    numSelectDiscounts = (int)indexPath.row;
    
    if (numSelectDiscounts == 0) {
        self.mPrice = vc.countAmount;
    }
    else if (numSelectDiscounts == 1) {
        self.mPrice = vc.foodAmount;
    }
    else if (numSelectDiscounts == 2) {
        self.mPrice = vc.drickAmount;
    }
    
    [self.mSelectDiscounts setTitle:listSelect[numSelectDiscounts] forState:UIControlStateNormal];
    [self.mAmount reloadInputViews];
    [self.mPercent reloadInputViews];
    [popoverController dismissPopoverAnimated:YES];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSNumberFormatter * nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterNoStyle];
    
    NSString * newString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    NSNumber * number = [nf numberFromString:newString];
    
    if ([self.mPercent isEqual:textField]) { // จำนวนบาท
        int num1 = ([number floatValue] / 100.0 ) * [self.mPrice floatValue];
        [self.mAmount setText:[@(num1) stringValue]];
    }
    else{ // จำนวนเปอร์เซ็นต์
        int num1 =  ([number floatValue] / [self.mPrice floatValue]) * 100.0;
        [self.mPercent setText:[@(num1) stringValue]];
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char,"\b");
    if (isBackSpace == -8) {
        // is backspace
        if (self.mAmount == textField){
            self.mPercent.text = [self.mPercent.text substringToIndex:[textField.text length]-1];
        }
        else{
            self.mAmount.text = [self.mAmount.text substringToIndex:[textField.text length] -1];
        }
    }
    if (number)
        return YES;
    else
        return NO;
}

- (IBAction)leftBtnAction:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnCamera:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc]init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.allowsEditing = NO;
        
        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeImage, nil];
        picker.mediaTypes = mediaTypes;
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"camera on this device!"
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil,
                                  nil];
        [alertView show];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [loadProcess.loadView showWithStatus];
    NSData *imageData = UIImageJPEGRepresentation([info valueForKey:UIImagePickerControllerOriginalImage], 0.5);
    [self uploadImageSign:imageData success:^(NSString *paht){
        imagePro = paht;
        [self.mImageView setImageWithURL:[NSURL URLWithString:paht]];
//        [self.mLayoutHeigtImage setConstant:150];
        [LoadProcess dismissLoad];
    }];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark - uploadImageSign
-(void)uploadImageSign:(NSData *)imageData success:(void (^)(NSString * paht))completion{
    Member *member = [Member getInstance];
    [member loadData];
    NSDictionary *parameters = @{@"code":member.code,
                                 @"files":@"file"};
    
    NSString *url = [NSString stringWithFormat:UPLOAD_IMAGE,member.nre];
    [LoadMenuAll loadDataImage:imageData andParameter:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            NSString *paht = json[@"data"][@"th_url"];
            completion(paht);
        }
        [LoadProcess dismissLoad];
    }];
}
@end
