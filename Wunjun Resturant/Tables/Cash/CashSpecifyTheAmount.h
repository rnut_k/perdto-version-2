//
//  CashSpecifyTheAmount.h
//  Wunjun Resturant
//
//  Created by AgeNt on 3/20/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "THSegmentedPageViewControllerDelegate.h"
#import "TableDataBillViewController.h"
#import "CNPPopupController.h"

@interface CashSpecifyTheAmount : UIViewController<THSegmentedPageViewControllerDelegate,UITextFieldDelegate,UIScrollViewDelegate,CNPPopupControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *mScrollViewSpecify;
@property (weak, nonatomic) IBOutlet UILabel *mNumTable;
@property (weak, nonatomic) IBOutlet UILabel *mTotal;
@property (weak, nonatomic) IBOutlet UITextField *mAmountCredit;
@property (weak, nonatomic) IBOutlet UITextField *mAmountReceived;
@property (weak, nonatomic) IBOutlet UITextField *mAmountRemaininMoney;
@property (weak, nonatomic) IBOutlet UIButton *mCash;
@property (weak, nonatomic) IBOutlet UIImageView *mImageReceived;
@property (weak, nonatomic) IBOutlet UIImageView *mImageCredit;

@property (weak, nonatomic) IBOutlet UILabel *mTextCash;
@property (weak, nonatomic) IBOutlet UILabel *mTextCredit;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeViewCredit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeViewCash;

- (IBAction)clickNumeric:(id)sender;
- (IBAction)cilckDelete:(id)sender;
- (IBAction)cilckSeleteCash:(id)sender;
- (IBAction)cilckChangeModeBillPayments:(id)sender;

@property (nonatomic,strong) NSString *viewTitle;
@property (nonatomic,strong) NSString *countAmount;
@property (nonatomic,strong) NSString *billTitle;
@property (nonatomic,strong) TableDataBillViewController  *tableDataBillView;
@property (nonatomic )       BOOL isCredit;
@property (nonatomic )       BOOL isPopupChangeMode;

@property (nonatomic,strong) id selfClass;

-(IBAction)btnPrintBill:(id)sender;
-(IBAction)btnPrintBillNotCheckBill:(id)sender;
+(void)btnPrintBill:(NSDictionary *)parameters  success:(void (^)(bool status))completion;

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title;
@end
