//
//  StaffStatusMenuItemViewController.m
//  Wunjun Resturant
//
//  Created by AgeNt on 5/12/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "StaffStatusMenuItemViewController.h"
#import "CellTableViewAll.h"

#import "Text.h"
#import "Member.h"
#import "Constants.h"
#import "LoadProcess.h"
#import "LoadMenuAll.h"
#import "NSString+GetString.h"
#import "Time.h"

@interface StaffStatusMenuItemViewController (){
    NSMutableArray *dataOrders;
    LoadProcess *loadProgress;
    NSDictionary *tablesList;
}

@property (nonatomic ,strong) Member *member;
@end

@implementation StaffStatusMenuItemViewController

-(Member *)member{
    if (!_member) {
        _member = [Member getInstance];
        [_member loadData];
    }
    return _member;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    loadProgress  = [LoadProcess sharedInstance];
    [loadProgress.loadView showWithStatus];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) refresh:(UIRefreshControl *)sender{
    [loadProgress.loadView showWithStatus];
    [self loadDataMenu:self.dataKitchen[@"id"]];
    [sender endRefreshing];
}
-(void)loadDataMenu:(NSString *)kitchenID{
    NSDictionary *parameters = @{
                                 @"code":self.member.code,
                                 @"kitchen_id":kitchenID,
                                 @"deliver_status":@"0"
                                 };
    NSString *url = [NSString stringWithFormat:GET_ORDERS_KITCHEN,self.member.nre];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        if([json[@"status"]boolValue])
        {
            NSDictionary *data = [NSDictionary dictionaryWithObject:json[@"data"]];
            NSDictionary *orders = [NSMutableDictionary dictionaryWithObject:data[@"orders"]];
            NSDictionary *menus = [NSMutableDictionary dictionaryWithObject:data[@"menu"]];
            tablesList = [NSMutableDictionary dictionaryWithObject:data[@"tables"]];
            if ([tablesList count] != 0) {
                tablesList = [NSMutableDictionary dictionaryWithObject:tablesList[@"tables"]];
            }
            dataOrders = [NSMutableArray array];
            if ([orders count] != 0) {
                NSMutableArray *arrList = [NSMutableArray arrayWithArray:[orders allValues]];
                NSArray *orders1 = [NSArray arrayWithArray:[Time sortDateDescriptorsDeliverDatetime:arrList]];
    
                for (NSDictionary *dic in orders1) {
                    if ([self checkDeliver:dic[@"deliver_status"]]) {
                        NSString *idMenu = [NSString stringWithFormat:@"id_%@",dic[@"menu_id"]];
                        NSDictionary *menuSort = menus[idMenu];
                        if ([menuSort count] != 0) {
                            [dataOrders addObject:@{@"order":dic,
                                                    @"menu":menuSort}];
                        }
                    }
                }
//                for (NSDictionary *dic in dataOrders) {
//                    NSDate *date2 = dic[@"order"][@"deliver_datetime"];       // create NSDate from dict2's Date;
//                    NSLog(@"date : %@ ",date2);
//                }
                if ([dataOrders count] != 0) {
                    [self.tableView reloadData];
                }
            }
        }
        [LoadProcess dismissLoad];
    }];
}
-(BOOL)checkDeliver:(NSString *)num{
    int num_ = [num intValue];
    switch (num_) {
//        case 1:
//        case 2:
        case 5: // รายการ อาหารที่ทำเสร็จ
            return true;
        default:
            return false;
            break;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [dataOrders count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIden = @"cellBillCommit";
    CellTableViewAll *cell =  [tableView dequeueReusableCellWithIdentifier:cellIden];
    if (cell == nil) {
        cell = [[CellTableViewAll alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIden];
    }
    NSDictionary *data     = dataOrders[indexPath.row];
    if ([data count] == 0) return cell;
    NSDictionary *orders   = [NSDictionary dictionaryWithObject:data[@"order"]];
    NSDictionary *menus    = [NSDictionary dictionaryWithObject:data[@"menu"]];

    NSString *tableID = [NSString checkNull:orders[@"table_id"]];
    NSString *nameTable = tablesList[[@"id_" stringByAppendingString:tableID]][@"label"];
    NSString *count        = [NSString stringWithFormat:@"(%@)",orders[@"n"]];
    NSString *price        = [NSString checkNull:orders[@"price"]];
    NSString *name         = [NSString stringWithFormat:@"%@ %@",[NSString checkNull:menus[@"name"]],count];
    NSString *statusEmblem = @"●";
    UIColor *color         = [self getColorEmblem:[orders[@"deliver_status"] intValue]];
    
    [cell.mLabel setAttributedText:[Text setColor:statusEmblem and:name andColor:color]];
    [cell.mCountOrder setText:nameTable];
    [cell.mCount setText:[NSString conventCurrency:price]];
    
    return cell;
}
-(UIColor *)getColorEmblem:(int)num{
    switch (num) {
        case 1:
        case 6:
            return  [UIColor colorWithRed:0.918 green:0.751 blue:0.171 alpha:1.000];
            break;
        case 2:
            return [UIColor colorWithRed:0.253 green:0.174 blue:0.920 alpha:1.000];
            break;
        case 3:
            return [UIColor colorWithRed:0.918 green:0.753 blue:0.332 alpha:1.000];
            break;
        case 4:
            return [UIColor colorWithRed:0.467 green:0.465 blue:0.399 alpha:1.000];
            break;
        case 0:
        case 5:
            return [UIColor colorWithRed:0.150 green:0.759 blue:0.068 alpha:1.000];
            break;
        default:
            return [UIColor whiteColor];
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
