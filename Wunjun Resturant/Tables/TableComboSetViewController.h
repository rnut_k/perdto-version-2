//
//  TableComboSetViewController.h
//  Wunjun Resturant
//
//  Created by AgeNt on 10/15/2558 BE.
//  Copyright © 2558 AgeNt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TableSingleBillViewController.h"
#import "TableSubBillViewController.h"

@interface TableComboSetViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mTableSeclectComboset;
@property (weak, nonatomic) IBOutlet UIButton *mDone;

@property (nonatomic ,strong) NSMutableArray *dataComboSetSeclect;
@property (nonatomic ,strong) NSMutableArray *dataComboSetList;
@property (nonatomic ,strong) NSDictionary *dataPromotionInfo;
@property (nonatomic ,strong) TableComboSetViewController *tableComboSet;
@property (nonatomic ) BOOL isPopUP;
@property (nonatomic ,assign) NSInteger indexSeclect;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mSizeBtnDone;


@property (nonatomic,strong)  TableSingleBillViewController *tableSingleBillViewController;
@property (nonatomic,strong)  TableSubBillViewController *tableSubBillViewController;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnDone:(id)sender;
@end
