//
//  AdminMainViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/24/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "AdminMainViewController.h"
#import "LoadProcess.h"
#import "Check.h"

@interface AdminMainViewController ()<UITabBarControllerDelegate>
{
    AdminNavigationController *adv;
    LoadProcess *loadProcess;
    Member *member;

    NSDictionary *dataAllZone;
}
@end

@implementation AdminMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loadProcess = [LoadProcess sharedInstance];
    member = [Member getInstance];
    adv = (AdminNavigationController *)self.parentViewController;
    
    if (![Check checkVersioniOS8]) {
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    
    self.title = AMLocalizedString(@"หน้าหลัก", nil);
    [self loadDatacashSalesReportView];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self initSetViewTab:0];
}

-(void) initSetViewTab:(int)index{
    AdminTabBarViewController *viewController = self.viewControllers[index];
    if (!viewController.isFirst) {
        [viewController setIndex:index];
        viewController.isFirst = YES;
        
        if (index == 2) {
            [viewController setDataAllZone:dataAllZone];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
     NSLog(@"didSelectItem");
    [loadProcess.loadView showWaitProgress];
    
    if (item.tag == 1) {
        self.title = AMLocalizedString(@"หน้าหลัก", nil);
        [self initSetViewTab:0];
    }
    else if (item.tag == 2) {
        self.title = AMLocalizedString(@"โต๊ะ", nil);
        [self initSetViewTab:1];
    }
    else if (item.tag == 3) {
        self.title = AMLocalizedString(@"ภาพรวมยอดขาย", nil);
        [self initSetViewTab:2];
    }
    else if (item.tag == 4) {
        self.title = AMLocalizedString(@"รายงานสต็อก", nil);
       [self initSetViewTab:3];
    }
    else if (item.tag == 5) {
         [LoadMenuAll logoutView:@{@"error":@"101"}];
    }
}
-(void) loadDatacashSalesReportView{
    
    NSDictionary *parameters = @{
                                 @"code":member.code,
                                 @"type":@"2"
                                 };
    NSString *url = [NSString stringWithFormat:GET_BILL_DETAIL,member.nre];
    KVNViewController *loadProgress  = [[KVNViewController alloc] init];
    [loadProgress showWithStatus];
    [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
        bool bl = [json[@"status"] boolValue];
        if (bl) {
            dataAllZone = [NSDictionary dictionaryWithObject:json[@"data"]];
        }
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
