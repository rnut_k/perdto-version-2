//
//  TablePendingViewController.m
//  Wunjun Resturant
//
//  Created by Sakarat Kaewwchain on 3/25/2558 BE.
//  Copyright (c) 2558 AgeNt. All rights reserved.
//

#import "TablePendingViewController.h"
#import "Time.h"
#import "CheckerInvoiceOrderController.h"
#import "PopupView.h"
#import "NSString+GetString.h"
#import "Check.h"
   
@interface TablePendingViewController ()<WYPopoverControllerDelegate>
{
    WYPopoverController *popoverController;
    PopupView *popupView;
    LoadProcess *loadProgress;
    NSMutableArray *pendingData;
    Member *member;
    
    NSDictionary *promotionInfo;
    NSDictionary *tableData;
    NSDictionary *billData;
    NSDictionary *orderInfo;
    
    NSDictionary *invoiceData;
    
    NSDictionary *incomeData;
    NSDictionary *otherExpData;
    
    NSString *lastId;
    BOOL isFinal;
    
    NSMutableArray *hilightList;
    
    NSDictionary *listStaff;
    NSArray *invoiceStatus;
}
@end

@implementation TablePendingViewController
@synthesize type, counterData;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initInvoiceStatus];
    loadProgress   = [LoadProcess sharedInstance];
    
    member = [Member getInstance];
    DeviceData *deviceData = [DeviceData getInstance];
    listStaff = [NSDictionary dictionaryWithDictionary:deviceData.staffList];
    
    self.view.backgroundColor = [UIColor ht_cloudsColor];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [loadProgress.loadView showProgress];
    hilightList = [[NSMutableArray alloc]init];
    for(NSDictionary*data in counterData[@"list"]){
        [hilightList addObject:data[@"id"]];
    }
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged:)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self updataPending:nil];
//    if (self.showShare) {
//        [self ceatePopupSharcLine];
//    }
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updataPending:)
                                                 name:@"NotiupdataPending" object:nil];
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)initInvoiceStatus{
    invoiceStatus = @[AMLocalizedString(@"รายการสินค้ายังไม่ขออนุมัติ", nil),
                      AMLocalizedString(@"รอการอนุมัติใบสั่งซื้อ", nil),
                      AMLocalizedString(@"อนุมัติใบสั่งซื้อ กำลังดำเนินการ", nil),
                      AMLocalizedString(@"รอตรวจรายการสินค้าจากใบสั่งซื้อ", nil),
                      AMLocalizedString(@"เสร็จเรียบร้อย", nil),
                      AMLocalizedString(@"ยกเลิกใบสั่งซื้อ", nil)];
}
- (void) refresh:(UIRefreshControl *)sender{
    isFinal = NO;
    lastId = @"";
    [self getPending];
    
    [sender endRefreshing];
}
- (void) orientationChanged:(NSNotification *)note{
    UIDevice * device = [UIDevice currentDevice];
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            NSLog(@"u size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            NSLog(@"d size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"l size: %f", self.view.frame.size.width);
            break;
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"r size: %f", self.view.frame.size.width);
            break;
            
        default:
            break;
    };
    [self.tableView reloadData];
}


- (NSString *)viewControllerTitle{
    return _viewTitle ? _viewTitle : self.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)updataPending:(NSNotification *)data{
    isFinal = NO;
    lastId = @"";
    [self getPending];
    [self.tableView reloadData];
}
#pragma mark req http
- (void) getPending
{
    if(type == 1) // staff
    {
        NSDictionary *parameters = @{
                                     @"code":member.code
                                     };
        NSString *url = [NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:GET_PENDING_PROMOTION,member.nre], lastId];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if([json[@"status"]boolValue]){
                if(lastId == nil || [lastId isEqualToString:@""]){
                    pendingData = [[NSMutableArray alloc]init];
                    promotionInfo = [[[NSDictionary alloc]init] mutableCopy];
                    tableData = [[[NSDictionary alloc]init] mutableCopy];
                    billData = [[[NSDictionary alloc]init] mutableCopy];
                    orderInfo = [[[NSDictionary alloc]init] mutableCopy];
                }
                
                isFinal = [json[@"data"][@"final"] boolValue];
                lastId = json[@"data"][@"last_id"];
                NSDictionary *promotion = json[@"data"][@"promotion_info"];
                NSDictionary *table = json[@"data"][@"tables"];
                NSDictionary *bill = json[@"data"][@"bill"];
                NSDictionary *pending = json[@"data"][@"pending"];
                NSDictionary *orders = json[@"data"][@"order_info"];
                if([pending count] > 0){
                    for (NSDictionary *row in pending) {
                        [pendingData addObject:row];
                    }
                }
                if([promotion count] > 0){
                    for(NSString *k in promotion){
                        [promotionInfo setValue:promotion[k] forKey:k];
                    }
                }
                if([table count] > 0){
                    for(NSString *k in table){
                        [tableData setValue:table[k] forKey:k];
                    }
                }
                if([bill count] > 0){
                    for(NSString *k in bill){
                        [billData setValue:bill[k] forKey:k];
                    }
                }
                if([orders count] > 0){
                    for(NSString *k in orders){
                        [orderInfo setValue:orders[k] forKey:k];
                    }
                }
                
                [self.tableView reloadData];
            }
            
        }];
    }
    else if (type == 2) // checker
    {
        NSDictionary *parameters = @{
                                     @"code":member.code,
                                     };
        NSString *url = [NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:GET_PENDING_INVOICE,member.nre], lastId];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if([json[@"status"]boolValue]){
                if(lastId == nil || [lastId isEqualToString:@""]){
                    pendingData = [[NSMutableArray alloc]init];
                    invoiceData = [[[NSDictionary alloc]init] mutableCopy];
                }
                
                isFinal = [json[@"data"][@"final"] boolValue];
                lastId = json[@"data"][@"last_id"];
                NSDictionary *invoice = json[@"data"][@"invoice"];
                NSDictionary *pending = json[@"data"][@"pending"];
                if([pending count] > 0){
                    for (NSDictionary *row in pending) {
                        [pendingData addObject:row];
                    }
                }
                if([invoice count] > 0){
                    for(NSString *k in invoice){
                        [invoiceData setValue:invoice[k] forKey:k];
                    }
                }
                [self.tableView reloadData];
            }
            
        }];
    }
    else if (type == 3) // cashier
    {
        NSDictionary *parameters = @{@"code":member.code};
        NSString *url = [NSString stringWithFormat:@"%@%@",[NSString stringWithFormat:GET_PENDING_INCOME,member.nre], lastId];
        [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
            if([json[@"status"]boolValue]){
                if(lastId == nil || [lastId isEqualToString:@""]){
                    pendingData = [[NSMutableArray alloc]init];
                    incomeData = [[[NSDictionary alloc]init] mutableCopy];
                    otherExpData = [[[NSDictionary alloc]init] mutableCopy];
                }
                
                isFinal = [json[@"data"][@"final"] boolValue];
                lastId = json[@"data"][@"last_id"];
                NSDictionary *income = json[@"data"][@"income"];
                NSDictionary *otherExp = json[@"data"][@"other_exp"];
                NSDictionary *pending = json[@"data"][@"pending"];
                if([pending count] > 0){
                    for (NSDictionary *row in pending) {
                        [pendingData addObject:row];
                    }
                }
                if([income count] > 0){
                    for(NSString *k in income){
                        [incomeData setValue:income[k] forKey:k];
                    }
                }
                if([otherExp count] > 0){
                    for(NSString *k in otherExp){
                        [otherExpData setValue:otherExp[k] forKey:k];
                    }
                }
                [self.tableView reloadData];
            }
        }];
    }
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 20;
    
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headTable = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    headTable.alpha = 0.5;
    headTable.backgroundColor = [UIColor ht_cloudsColor];
    headTable.layer.borderWidth = 0;
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, headTable.frame.size.width - 20, headTable.frame.size.height)];
    NSString *title = @"";
    if(type == 1){
        title = AMLocalizedString(@"โปรโมชั่นและส่วนลด",nil);
    }
    else if(type == 2){
        title = AMLocalizedString(@"รายงานใบสั่งสินค้า",nil);
    }
    else if(type == 3){
        title = AMLocalizedString(@"รายงานส่งยอดรายรับ",nil);
    }
    lbl.font = [UIFont systemFontOfSize:12];
    lbl.text = title;
    [headTable addSubview:lbl];
    
    return headTable;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [pendingData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    NSDictionary *info = pendingData[indexPath.row];
    
    UIView *tableContent = [[UIView alloc]initWithFrame:CGRectMake(0, 0 ,tableView.frame.size.width, 70)];
    tableContent.tag = 1;
    
    NSString *imgName = @"";
    NSString *titleText = @"";
    NSString *titleText2 = @"";
    NSString *datetimeText = @"";
    NSString *discountText = @"";
    
    UIColor *bgColor = [UIColor clearColor];
    
    if([hilightList containsObject:info[@"id"]]){
        bgColor = [UIColor colorWithRed:1.000 green:1.000 blue:0.400 alpha:0.590];
    }
    
    if(type == 1){
        if([info[@"status"] intValue] == 0){
            bgColor = [UIColor colorWithRed:1.000 green:1.000 blue:0.400 alpha:0.590];
        }
        else if ([info[@"status"] intValue] == 3 || [info[@"status"] intValue] == 1){
            bgColor = [UIColor clearColor];
        }
        if([info[@"status"] intValue] == 0){
            imgName = @"a_img_pro_in";
        }else if ([info[@"status"] intValue] == 1)
            imgName = @"check";
        else if ([info[@"status"] intValue] == 2)
            imgName = @"a_ic_cancel";
        
        NSString *tableLbl = tableData[[NSString stringWithFormat:@"id_%@", info[@"table_id"]]][@"label"];
        NSString *proName;
        if (info[@"object_id"] == nil) {
            proName = info[@"promotion_data"][@"name"];
        }
        else{
            if([info[@"type"] intValue] == 7){
                NSDictionary *orders = orderInfo[[NSString stringWithFormat:@"id_%@", info[@"object_id"]]];
                if(orders != nil){
                    discountText = [NSString stringWithFormat:@"ขอส่วนลด %@ บาท", orders[@"price"]];
                    proName = orders[@"food_comment"];
                }
                else{
                    proName = ([info[@"comment"] isKindOfClass:[NSNull class]])?@"":info[@"comment"];
                }
            }
            else{
                proName = promotionInfo[[NSString stringWithFormat:@"id_%@", info[@"object_id"]]][@"name"]; // ให้มา
            }
            
        }
        titleText = [NSString stringWithFormat:@"%@ %@ : %@",AMLocalizedString(@"โต๊ะ", nil),tableLbl, proName];
       
        datetimeText = (info[@"created_datetime"]==nil)?[Time getTimeandDate]:info[@"created_datetime"];
        titleText2 = @"";
    }
    else if(type == 2){
        NSLog(@" %ld : %@",(long)indexPath.row,info[@"status"]);
        if([info[@"status"] intValue] == 0){
            bgColor = [UIColor colorWithRed:1.000 green:1.000 blue:0.400 alpha:0.590];
        }
        else if ([info[@"status"] intValue] == 3 || [info[@"status"] intValue] == 1){
            bgColor = [UIColor clearColor];
        }

        NSString *typeText = @"";
        if([info[@"type"] intValue] == 5)
        {
            NSString *idInvioce = [NSString stringWithFormat:@"id_%@",info[@"object_id"]];
            NSDictionary *invioceInfo = invoiceData[idInvioce];
            
            imgName = @"a_img_invoice";
            typeText = AMLocalizedString(@"ใบสั่งซื้อ", nil);
            datetimeText = invoiceStatus[[invioceInfo[@"status"]intValue]];
        }
        else if([info[@"type"] intValue] == 8)
        {
            imgName = @"a_img_invoice_report";
            typeText = AMLocalizedString(@"รายงานสต็อก", nil);
            datetimeText = info[@"created_datetime"];
        }
        
        NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:info[@"created_datetime"]]];
        NSString *idC = [NSString stringWithFormat:@"id_%@",info[@"object_id"]];
        NSDictionary *objectID = invoiceData[idC];
        NSString *idStaff = [NSString stringWithFormat:@"id_%@",objectID[@"creator"]];
        NSString *idCreate = [NSString stringWithFormat:@"ID:%@",info[@"object_id"]];
        NSString *nameCreate = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"โดย", nil),listStaff[idStaff][@"name"]];
        if ([typeText isEqualToString:AMLocalizedString(@"รายงานสต็อก", nil)]) {
            titleText = [NSString stringWithFormat:@"%@ %@ ",typeText,date_];
        }
        else
            titleText = [NSString stringWithFormat:@"%@ %@ %@ %@",typeText,date_,idCreate,nameCreate];
        
    }
    else if(type == 3){
        if([info[@"status"] intValue] == 0){
            bgColor = [UIColor colorWithRed:1.000 green:1.000 blue:0.400 alpha:0.590];
        }
        
        if ([info[@"type"] integerValue] == 9) {
            titleText = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ยกเลิกบิล", nil),info[@"object_id"]];
            datetimeText = [NSString stringWithFormat:@"%@ %@", [Time getTHFullFromDate:info[@"created_datetime"]],AMLocalizedString(@"น.", nil)];
            titleText2 = @"";
        }
        else{
            NSDictionary *income = incomeData[[NSString stringWithFormat:@"id_%@", info[@"object_id"]]];
            NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:info[@"created_datetime"]]];
            titleText = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"รายงานยอดเงิน", nil),date_];
            datetimeText = [NSString stringWithFormat:@"%@ %@", [Time conventTime:info[@"created_datetime"]],AMLocalizedString(@"น.", nil)];
            titleText2 = [Text numberFormatt:[income[@"income_amount"] floatValue]];
        }
    }

    UIImageView *imgView;
    if(![imgName isEqualToString:@""]){
        imgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 15, 30, 30)];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.image = [UIImage imageNamed:imgName];
        [tableContent addSubview:imgView];
    }
    
    
    UILabel *lbl2;
    float lbl2W = 0;
    if(![titleText2 isEqualToString:@""]){
        lbl2W = tableContent.frame.size.width * 0.3;
        lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(tableContent.frame.size.width - 5 - lbl2W, 5, lbl2W, 25)];
        lbl2.textColor = [UIColor ht_jayColor];
        lbl2.textAlignment = NSTextAlignmentRight;
        lbl2.text = titleText2;
        [tableContent addSubview:lbl2];
    }
    
    float lblX = imgView.frame.origin.x + imgView.frame.size.width + 5;
    float lblW = tableContent.frame.size.width - lblX - 5 - lbl2W;
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(lblX, 5, lblW, 25)];
    lbl.tag = 101;
    lbl.text = titleText;
    [lbl setFont:[UIFont systemFontOfSize:14]];
    [tableContent addSubview:lbl];
    lbl.alpha = 0.4;
    
    UILabel *datelbl1 = [[UILabel alloc]initWithFrame:CGRectMake(lblX, lbl.frame.origin.y + lbl.frame.size.height, lblW, 15)];
    datelbl1.text = discountText;
    datelbl1.font = [UIFont boldSystemFontOfSize:12];
    datelbl1.alpha = 0.4;
    datelbl1.textColor = [UIColor redColor];
    [tableContent addSubview:datelbl1];
    
    UILabel *datelbl = [[UILabel alloc]initWithFrame:CGRectMake(lblX, datelbl1.frame.origin.y + datelbl1.frame.size.height, lblW, 15)];
    datelbl.text = datetimeText;
    datelbl.font = [UIFont systemFontOfSize:12];
    datelbl.alpha = 0.4;
    [tableContent addSubview:datelbl];
    
    tableContent.backgroundColor = bgColor;
    
    [[cell viewWithTag:1] removeFromSuperview];
    
    [cell addSubview:tableContent];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSDictionary *pendingDataInfo = pendingData[indexPath.row];
//    NSString *idInvioce = [NSString  stringWithFormat:@"id_%@",pendingDataInfo[@"object_id"]];
//    NSDictionary *invioceInfo = invoiceData[idInvioce];
    
    if(type == 1 && [pendingDataInfo[@"status"] intValue] == 0)
    {
        NSDictionary *info = pendingData[indexPath.row];
        NSString *msg = @"";
        if([info[@"type"] intValue] == 7)
        {
            if(![info[@"comment"] isKindOfClass:[NSNull class]] && ![info[@"comment"] isEqualToString:@""])
            {
                msg = info[@"comment"];
            }
            else
            {
                NSDictionary *orders = orderInfo[[NSString stringWithFormat:@"id_%@", info[@"object_id"]]];
                if(orders != nil){
                    msg = [NSString stringWithFormat:@"%@ %@ %@",AMLocalizedString(@"ขอส่วนลด", nil),orders[@"price"],AMLocalizedString(@"บาท", nil)];
                }
            }
        }
        else
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UILabel *lbl = (UILabel*)[cell viewWithTag:101];
            msg = lbl.text;
        }
        
//        proName = ([info[@"comment"] isKindOfClass:[NSNull class]])?@"":info[@"comment"];
        
        UIAlertView *alertViewOption = [[UIAlertView alloc]
                                        initWithTitle:AMLocalizedString(@"อนุมัติโปรโมชั่น", nil)
                                        message:msg
                                        delegate:self
                                        cancelButtonTitle:AMLocalizedString(@"ไม่", nil)
                                        otherButtonTitles:AMLocalizedString(@"อนุมัติ", nil),AMLocalizedString(@"ปฎิเสธ", nil) , nil
                                        ];
        alertViewOption.tag = 2;
        alertViewOption.backgroundColor = [UIColor whiteColor];
        
        UILabel *row = [[UILabel alloc]init];
        row.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
        row.tag = 101;
        [alertViewOption addSubview:row];
        
        [alertViewOption show];
    }
    else if(type == 2){
        NSDictionary *info = [pendingData[indexPath.row] mutableCopy];

        BOOL editNotApproved = NO;
        BOOL editApproved = NO;
        BOOL viewStatus = NO;
        if([info[@"type"] intValue] == 5)
        {
            NSString *idInvioce = [NSString stringWithFormat:@"id_%@",info[@"object_id"]];
            NSDictionary *invioceInfo = invoiceData[idInvioce];
            if([invioceInfo[@"status"]intValue] == 0){
//                datetimeText = @"รายการสินค้ายังไม่ขออนุมัติ";
                editNotApproved = YES;
            }
            else if([invioceInfo[@"status"]intValue] == 2){
//                datetimeText = @"รอการอนุมัติใบสั่งซื้อ";
                viewStatus = YES;
                [loadProgress.loadView showWithStatusAddError:@"อนุมัติใบสั่งซื้อ กำลังดำเนินการ"];
                return;
            }
            else if([invioceInfo[@"status"]intValue] == 1){
//                datetimeText = @"อนุมัติใบสั่งซื้อ กำลังดำเนินการ";
                editApproved = YES;
            }
            else if([invioceInfo[@"status"]intValue] == 3){
//                datetimeText = @"รอตรวจรายการสินค้าจากใบสั่งซื้อ";
                viewStatus = YES;
            }
            else if([invioceInfo[@"status"]intValue] == 4){
//                datetimeText = @"เสร็จเรียบร้อย";
                viewStatus = YES;
            }
            else{ //manage with bug aprove in success invoice
//                datetimeText = ยกเลิกใบสั่งซื้อ
                viewStatus = YES;
            }
            
            [loadProgress.loadView showWaitProgress];
            NSString *idStaff = [NSString stringWithFormat:@"id_%@",invioceInfo[@"creator"]];
            NSString *idCreate = [NSString stringWithFormat:@"ID:%@",info[@"object_id"]];
            NSString *nameCreate = [NSString stringWithFormat:@"โดย %@",listStaff[idStaff][@"name"]];
            
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
            CheckerInvoiceOrderController *viewController_ = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CheckerInvoiceStockCheck"];
            NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:info[@"created_datetime"]]];
            NSString *titleText = [NSString stringWithFormat:@"%@ %@",AMLocalizedString(@"ใบสั่งสินค้า", nil),date_];
            [viewController_ setTitle:titleText];
            [viewController_ setNameVendor:[NSString stringWithFormat:@"%@ %@",idCreate,nameCreate]];
            [viewController_ setRole:@"admin"];
            [viewController_ setPendingData:info];
            [viewController_ setDataEditItem:invioceInfo];
            [viewController_ setBlEditNotApproved:editNotApproved];
            [viewController_ setBlEditApproved:editApproved];
            [viewController_ setBlViewStatusInvoice:viewStatus];
//            [viewController_ loadDataReportSeles];
            [viewController_ setTablePending1:self];
            
            CustomNavigationController *customNav = [[CustomNavigationController alloc]initWithRootViewController:viewController_];
            customNav.navigationBar.translucent = NO;
            customNav.navigationBar.barTintColor = [UIColor colorWithRed:23/255.0f green:20/255.0f blue:66/255.0f alpha:1];
            
            [self presentViewController:customNav animated:YES completion:nil];
        }
        else if([info[@"type"] intValue] == 8)
        {
            [loadProgress.loadView showWithStatus];
            
            NSString *pendingId = ([info[@"status"] intValue] == 0)?info[@"id"]:@"";
            NSDictionary *parameters = @{
                                         @"code":member.code
                                         };
            NSString *url = [NSString stringWithFormat:GET_REPORT_STOCK,member.nre, info[@"object_id"], pendingId];
            [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
                if([json[@"status"] boolValue]){
                    if([info[@"status"] intValue] == 0){
                        [info setValue:@"1" forKey:@"status"];
                        [pendingData setObject:info atIndexedSubscript:indexPath.row];
                        [self.tableView reloadData];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationSetCounter"
                                                                            object:self
                                                                          userInfo:nil];
                    }
                    
//                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:LocalizationgetNSBundle];
                    CustomNavigationController *stockSubmitReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"AdminStockSubmit"];
                    [stockSubmitReportView setRole:@"admin"];
                    [stockSubmitReportView setDataStockSubmit:json[@"data"]];
                    NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:info[@"created_datetime"]]];
                    [stockSubmitReportView setTitle:date_];
                    [self presentViewController:stockSubmitReportView animated:YES completion:nil];
                     [KVNProgress dismiss];
                }
                
            }];
        }
        
    }
    else if(type == 3){

        NSDictionary *info = [pendingData[indexPath.row] mutableCopy];
       if ([info[@"type"] integerValue] == 9) {
           [loadProgress.loadView showWithStatus];
           [self createPopBill:info];
       }
       else{
           NSDictionary *income = incomeData[[NSString stringWithFormat:@"id_%@", info[@"object_id"]]];
           NSDictionary *otherExp = otherExpData[[NSString stringWithFormat:@"income_id_%@", income[@"id"]]];
           float amount_effect = 0;
           NSString *description = @"";
           if(otherExp != nil){
               amount_effect = [otherExp[@"amount_effect"] floatValue];
               description = otherExp[@"description"];
           }
           
           CustomNavigationController *cashSalesReportView = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"CashAmountDailyView"];
           NSString *date_ = [Time covnetTimeDMY1:[Time getICTFormateDate:info[@"created_datetime"]]];
           NSString *titleText = [NSString stringWithFormat:@"รายงานยอดเงิน %@", date_];
           [cashSalesReportView setTitle:titleText];
           [cashSalesReportView setRole:@"admin"];
           [cashSalesReportView setDataIncome:income];
           [cashSalesReportView setViewInfo:@{
                                              @"pre":income[@"pre"],
                                              @"post":income[@"post"],
                                              @"description":description,
                                              @"amount_effect":[NSString stringWithFormat:@"%f", amount_effect]
                                              }];
           [self presentViewController:cashSalesReportView animated:YES completion:nil];
           
           
           if([info[@"status"]intValue] == 0)
           {
               [info setValue:@"1" forKey:@"status"];
               [pendingData setObject:info atIndexedSubscript:indexPath.row];
               [self.tableView reloadData];
               
               NSString *pendingId = info[@"id"];
               NSDictionary *parameters = @{
                                            @"code":member.code,
                                            @"pending_id":pendingId,
                                            @"status":@"1",
                                            @"action":@"approve"
                                            };
               NSString *url = [NSString stringWithFormat:GET_MANAGE_INCOME,member.nre];
               [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
                   [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationSetCounter"
                                                                       object:self
                                                                     userInfo:nil];
               }];
           }
       }
    }
}

#pragma mark scroll detect
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    if(!isFinal)
    {
        CGPoint offset = scrollView.contentOffset;
        CGRect bounds = scrollView.bounds;
        CGSize size = scrollView.contentSize;
        UIEdgeInsets inset = scrollView.contentInset;
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        float reload_distance = 50;
        if(y > h + reload_distance) {
            [self getPending];
        }
    }
    
}

#pragma mark uialerview
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(type == 1){
        if(buttonIndex != 0){
            UILabel *row = (UILabel*)[alertView viewWithTag:101];
            NSDictionary *info = [pendingData[[row.text intValue]]mutableCopy];
            NSString *pendingId = info[@"id"];
            NSString *tableId = info[@"table_id"];
            NSString *pendingType = info[@"type"];
            NSDictionary *parameters = @{
                                         @"code":member.code,
                                         @"pending_id":pendingId,
                                         @"status":(buttonIndex == 1)?@"1":@"2",
                                         @"type":pendingType,
                                         @"table_id":tableId
                                         };
            NSString *url = [NSString stringWithFormat:APPROVE_PROMOTION,member.nre];
            [LoadMenuAll loadData:parameters andURL:url success:^(NSDictionary *json) {
                if([json[@"status"]boolValue]){
                    [info setValue:parameters[@"status"] forKey:@"status"];
                    [pendingData setObject:info atIndexedSubscript:[row.text intValue]];
                    [self.tableView reloadData];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationSetCounter"
                                                                    object:self
                                                                  userInfo:nil];
                
            }];
        }
    }
}
- (void)createPopBill:(NSDictionary *)data {
    
    TableSingleBillViewController *tableSingleBill = [UIStoryboardMain instantiateViewControllerWithIdentifier:@"TableSubBill"];
     NSString *title =  [NSString stringWithFormat:@" %@ ",AMLocalizedString(@"บิลหลักโต๊ะ ...", nil)];
    [tableSingleBill setTitleTable:title];
    [tableSingleBill setDetailTable:data];
    [tableSingleBill setIsDeleteBill:YES];
    [tableSingleBill setCheckOpenTable:NO];//no
    [tableSingleBill setIsCashier:NO];//no
    [tableSingleBill setIsCashTableStatus:NO];//yes
    [tableSingleBill setIsCashierTake:NO];//no
    [tableSingleBill setIsStaffTake:NO];//no
    [tableSingleBill setIsPopupCashier:YES];
        
    tableSingleBill.preferredContentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height+200);
    tableSingleBill.modalInPopover = NO;
    
    CustomNavigationController* contentViewController = [[CustomNavigationController alloc] initWithRootViewController:tableSingleBill];
    [contentViewController setColorBar];
    popoverController = [[WYPopoverController alloc] initWithContentViewController:contentViewController];
    popoverController.delegate = self;
    [popoverController presentPopoverFromRect:CGRectZero
                                       inView:self.view
                     permittedArrowDirections:WYPopoverArrowDirectionNone
                                     animated:NO];
    UIImage *image = [[UIImage imageNamed:@"close"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)
                                                                   resizingMode:UIImageResizingModeStretch];
    UIBarButtonItem  *rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:image
                                                                            style:UIBarButtonItemStyleDone
                                                                           target:self
                                                                           action:@selector(btnClosePop:)];
    [tableSingleBill.navigationItem setRightBarButtonItem:rightBarButtonItem];
    [tableSingleBill.navigationItem setLeftBarButtonItem:nil];
}
- (IBAction)btnClosePop:(id)sender {
    [popoverController dismissPopoverAnimated:YES];
    popoverController = nil;
}
//- (void)ceatePopupSharcLine{
//    popupView = [[PopupView alloc] init];
//    [popupView setTablePendingViewController:self];
//    [popupView showPopupWithStyle:CNPPopupStyleCentered];
//}
//#pragma mark - CNPPopupController Delegate
//- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
//    if ([title  isEqual: @"ใช่"]) {
//        [self showShareButtons:YES];
//    }
//}
//-(void)showShareButtons:(BOOL)animated
//{
//    UIImage *image = self.imageScreenInvioce;
//    if (image != nil) {
//        NSArray *lineActivity = @[[[AQSLINEActivity alloc] init]];
//        NSArray *objectsToShare = @[image];
//        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare
//                                                                                 applicationActivities:lineActivity];
//        activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypePostToFacebook,UIActivityTypeMessage];
//        [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
//            if (completed) {
//                NSLog(@"การแชร์เรียนร้อย");
//            }
//            else{
//                if (activityType == NULL) {
//                    NSLog(@"ไม่ได้เลือกอะไร");
//                }
//                else{
//                    NSLog(@"ไม่");
//                }
//            }
//            
//        }];
//        
//        UINavigationController *nav = (UINavigationController *)self.view.window.rootViewController;
//        [nav presentViewController:activityVC animated:YES completion:nil];
//    }
//}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
